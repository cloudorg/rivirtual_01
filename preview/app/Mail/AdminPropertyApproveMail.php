<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminPropertyApproveMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data = [];
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data = [])
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   $data['propertyData'] = $this->data;
        $subject = 'Your property approved successfully';
        return $this->view('mail.admin_property_approve_mail',$data)
        ->subject($subject)
        ->to($this->data->email)
        ->from(env('MAIL_USERNAME'), env('APP_NAME'));
    }
}

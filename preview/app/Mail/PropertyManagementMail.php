<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PropertyManagementMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data = [];
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data = [])
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   $data['data'] = $this->data;
        // dd($data);
        // $subject = 'Your property approved successfully';
        return $this->view('mail.property_management_mail',$data)
        ->subject('Property Management Mail')
        ->to('pm@rivirtual.com')
        // ->to('pujabanerjee373@gmail.com')
        ->from(env('MAIL_USERNAME'), env('APP_NAME'));
    }
}

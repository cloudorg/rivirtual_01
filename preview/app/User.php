<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    // /**
    //  * The attributes that are mass assignable.
    //  *
    //  * @var array
    //  */
    // protected $fillable = [
    //     'name', 'email', 'password',
    // ];

    // /**
    //  * The attributes that should be hidden for arrays.
    //  *
    //  * @var array
    //  */
    protected $table = 'users';
    protected $guarded = [];

    // protected $hidden = [
    //     'password', 'remember_token',
    // ];

    // /**
    //  * The attributes that should be cast to native types.
    //  *
    //  * @var array
    //  */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];

    public function category(){

          return $this->hasOne('App\Models\Category','id','category_id')->where('status','A');
    }

    public function providerSkill(){

          return $this->hasMany('App\Models\ProviderToSkills','user_id','id');
    }

    public function providerToLanguage(){

      return $this->hasMany('App\Models\ProviderToLanguage','user_id','id');
    }
    public function userCountry(){

      return $this->hasOne('App\Country','id','country');
    }
    public function userState(){

      return $this->hasOne('App\Models\State','id','state');
    }
    public function userCity(){

      return $this->hasOne('App\Models\City','id','city');
    }

    public function proToCategory(){

      return $this->hasOne('App\Models\ProToCategory','user_id','id')->where('master_id','=',0);
    }

    public function proToSubCategory(){

      return $this->hasOne('App\Models\ProToCategory','user_id','id')->where('master_id','!=',0);
    }
    public function totalProperty(){

      return $this->hasMany('App\Models\Property','user_id','id');
  }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageDetail extends Model
{
    protected $table = 'message_details';
    protected $guarded = [];

    public function getUser()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function getReceiver()
    {
        return $this->hasOne('App\User', 'id', 'receiver_id')->where('status','A');
    }
    public function getMessageMaster()
    {
        return $this->hasOne('App\Models\MessageMaster', 'id', 'message_master_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserToSaveSearch extends Model
{
    protected $table = 'user_to_save_searches';
    protected $guarded = [];

    public function propertyDetails(){

          return $this->hasOne('App\Models\Property','id','property_id');
    }

    public function agentDetails(){

        return $this->hasOne('App\User','id','agent_id');
    }

    public function totalProperty(){

          return $this->hasMany('App\Models\Property','user_id','agent_id')->where('status','A')->where('aprove_by_admin','Y');
    }

    public function proDetails(){

         return $this->hasOne('App\User','id','pro_id');
    }

    public function proToLanguage(){

         return $this->hasMany('App\Models\ProviderToLanguage','user_id','pro_id');
    }

    public function proToCategory(){

        return $this->hasOne('App\Models\ProToCategory','user_id','pro_id');
    }
}

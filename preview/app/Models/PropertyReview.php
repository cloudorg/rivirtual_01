<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyReview extends Model
{
    protected $table = 'property_reviews';
    protected $guarded = [];

    public function userDetails(){

         return $this->hasOne('App\User','id','posted_by_id');
    }

    public function propertyDetails(){

         return $this->hasOne('App\Models\Property','id','property_id');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobToProReview extends Model
{
    protected $table = 'job_to_pro_reviews';
    protected $guarded = [];

    public function jobDetails(){

         return $this->hasOne('App\Models\PostJob','id','job_id');
    }

    public function userDetails(){

         return $this->hasOne('App\User','id','posted_by_id');
    }
}

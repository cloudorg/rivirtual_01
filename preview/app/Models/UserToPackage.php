<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserToPackage extends Model
{
    protected $table = 'user_to_package';
    protected $guarded = [];

    public function UserPackageName(){

           return $this->hasOne('App\Models\AgentMemberShipPackage','id','agent_package_id');
    }
    public function getPackage(){
      return $this->hasOne('App\Models\ProPackage','id','pro_package_id');
    }
}

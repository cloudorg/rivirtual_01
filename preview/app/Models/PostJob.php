<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostJob extends Model
{
    protected $table = 'jobs';
    protected $guarded = [];

    public function categoryName(){

          return $this->hasOne('App\Models\Category','id','category');
    }

    public function subCategoryName(){

        return $this->hasOne('App\Models\SubCategory','id','sub_category');
    }


    public function getUser(){
      return $this->hasOne('App\User','id','user_id');
    }

    
    public function userDetails(){

         return $this->hasOne('App\User','id','user_id')->where('status','!=','D');
    }

    public function jobToCategory(){

         return $this->hasOne('App\Models\JobToCategory','post_id','id')->where('master_id','=',0);
    }

    public function jobToSubCategory(){

        return $this->hasOne('App\Models\JobToCategory','post_id','id')->where('master_id','!=',0);
    }

    public function quoteDetails(){

          return $this->hasOne('App\Models\Quotes','post_id','id')->where('status','Awarded');;
    }

    public function jobState(){

        return $this->hasOne('App\Models\State','id','state');
    }
    public function jobCity(){

        return $this->hasOne('App\Models\City','id','city');
    }
    public function jobCountry(){

        return $this->hasOne('App\Country','id','country');
    }
    
    public function proDetails(){

           return $this->hasOne('App\Models\Quotes','post_id','id')->where('status','Awarded');
    }
    
    public function reviewDetails(){

          return $this->hasOne('App\Models\JobToProReview','job_id','id');
    }
    
    public function getMilestone(){

        return $this->hasOne('App\Models\Milestone','job_id','id');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
class AgentMemberShipPackage extends Model
{
    protected $table = 'agent_membership_package';
    protected $guarded = [];

    public function userToPackage(){

          return $this->hasOne('App\Models\UserToPackage','agent_package_id','id')->where('user_id',Auth::user()->id);
    }
}

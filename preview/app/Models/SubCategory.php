<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $table = 'sub_categories';
    protected $guarded = [];

    public function categoryName(){

         return $this->hasOne('App\Models\Category','id','category_id');
    }
}

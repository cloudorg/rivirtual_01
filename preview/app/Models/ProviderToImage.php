<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProviderToImage extends Model
{
    protected $table = 'provider_to_image';
    protected $guarded = [];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Quotes extends Model
{
    protected $table = 'jobs_quotes';
    protected $guarded = [];


     public function proDetails(){

          return $this->hasOne('App\User','id','user_id');
     }
     public function jobDetails(){

         return $this->hasOne('App\Models\PostJob','id','post_id');
     }
    
}

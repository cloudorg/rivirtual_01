<?php

namespace App\Models;
use Auth;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $table = 'properties';
    protected $guarded = [];

    public function propertyImageMain()
    {
        return $this->hasOne('App\Models\PropertyToImage','property_id','id')->where('is_default','1');
    }
    public function propertyUser()
    {
        return $this->hasOne('App\User','id','user_id');
    }
    public function countryName(){
        return $this->hasMany('App\Country','id','country');
    }
    
    public function countName(){
        return $this->hasOne('App\Country','id','country');
    }

    public function localityName(){
        return $this->hasOne('App\Models\Locality','id','locality');
    }
    public function stateName(){
        return $this->hasOne('App\Models\State','id','state');
    }
    public function cityName(){
        return $this->hasOne('App\Models\City','id','city');
    }
     public function landPropertyImageMain(){

        return $this->hasOne('App\Models\PropertyToImage','property_id','id')->where('image_for','FP')->where('is_default','1');
    }
   public function propertySave(){

        return $this->hasOne('App\Models\UserToSaveSearch','property_id','id')->where('user_id',@Auth::user()->id);
    }
}

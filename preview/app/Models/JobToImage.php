<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobToImage extends Model
{
    protected $table = 'job_to_image';
    protected $guarded = [];
}

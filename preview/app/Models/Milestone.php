<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Milestone extends Model
{
    protected $table = 'milestones';
    protected $guarded = [];

    public function userDetails(){

          return $this->hasOne('App\User','id','user_id');
    }
    public function proDetails(){

        return $this->hasOne('App\User','id','pro_id');
  }
  public function jobDetails(){

        return $this->hasOne('App\Models\PostJob','id','job_id');
  }
}

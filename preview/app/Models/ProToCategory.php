<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProToCategory extends Model
{
    protected $table = 'pro_to_category';
    protected $guarded = [];

    public function categoryName(){

          return $this->hasOne('App\Models\Category','id','category_id');
    }

    public function subCategoryName(){

        return $this->hasOne('App\Models\SubCategory','id','category_id');
  }
}

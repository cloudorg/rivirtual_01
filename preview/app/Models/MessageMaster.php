<?php

namespace App\Models;
use Auth;

use Illuminate\Database\Eloquent\Model;

class MessageMaster extends Model
{
    protected $table = 'message_master';
    protected $guarded = [];

    public function getSeeker()
    {
        return $this->hasOne('App\User', 'id', 'seeker_id');
    }
    public function getProvider()
    {
        return $this->hasOne('App\User', 'id', 'provider_id');
    }

    public function getMessageDetailLastMessage()
    {
        return $this->hasOne('App\Models\MessageDetail', 'message_master_id', 'id')->orderBy('id','desc');
    }

    public function getUnreadMessage()
    {
        return $this->hasMany('App\Models\MessageDetail', 'message_master_id', 'id')->where('is_read','N')->where('user_id','!=',@Auth::id());
    }

    public function getProperty()
    {
        return $this->hasOne('App\Models\Property','id','property_id');
    }
}

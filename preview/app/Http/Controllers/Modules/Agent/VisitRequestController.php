<?php

namespace App\Http\Controllers\Modules\Agent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\PropertyVisitRequest;
use Mail;
use App\Mail\AgentVisitCancelMail;
use App\User;
class VisitRequestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function visitRequest(Request $request){
         $allVisitRequest = PropertyVisitRequest::with(['propertyDetails','userDetails'])->where('agent_id',Auth::user()->id)->where('status','!=','C');
         if($request->all()){

              if($request->property_name){

                $allVisitRequest = $allVisitRequest->whereHas('propertyDetails', function($q) use($request){

                      $q->where('name','like','%'.$request->property_name.'%')
                      ->orWhere('slug','like','%'.str_slug($request->property_name).'%');
                });
            }
                if($request->start_date){

                    $allVisitRequest = $allVisitRequest->where(DB::raw("DATE(visit_date)"),'=',date('Y-m-d',strtotime($request->start_date)));
                }
                if($request->time){

                    $allVisitRequest = $allVisitRequest->where(DB::raw("DATE(visit_date)"),'=',date('h:i',strtotime($request->time)));
                }

             $data['key'] = $request->all();

         }
          $allVisitRequest = $allVisitRequest->orderBy('id','desc')->paginate(10);
          $data['allVisitRequest'] = $allVisitRequest;
          return view('modules.agent.manage_visit_request')->with($data);
    }

    public function visitRequestCancel($id=null){


    $check= PropertyVisitRequest::with('propertyDetails','userDetails','agentDetails')->where('id',$id)->first();
    if($check==null){
        session()->flash('error','Something went wrong');
        return redirect()->route('agent.visit.request');
    }
    $data['name'] = $check->userDetails->name;
    $data['agent_name'] = $check->agentDetails->name;
    $data['email'] = $check->userDetails->email;
    $data['property_name'] = $check->propertyDetails->name;
    $data['visit_date'] = $check->visit_date;
     Mail::send(new AgentVisitCancelMail($data));
    PropertyVisitRequest::where('id',$id)->update(['status'=>'CA']);
    session()->flash('success','Property visit request cancel successfully');
    return redirect()->route('agent.visit.request');
    }


    public function viewProfile($v_id=null,$u_id=null){
       
       
       $data['user'] = User::where('id',$u_id)->first();
       
       return view('modules.agent.client_details')->with($data);
    }
}

<?php

namespace App\Http\Controllers\Modules\Agent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Mail;
use App\Country;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\AgentMemberShipPackage;
use App\Models\UserToPackage;
class SubcriptionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function agentSubcription(){

         $check = UserToPackage::where('user_id',Auth::user()->id)->count();
         if(@$check == 0){

              $ins = [];
              $ins['user_id'] = Auth::user()->id;
              $ins['agent_package_id'] = 1;
              $create =  UserToPackage::create($ins);
              
              $upd = [];
              $NewDate=Date('Y-m-d', strtotime('+30 days'));
              $upd['package_expiry_date'] = $NewDate;
              $upd['payment_freq'] = 'M';

              User::where('id',Auth::user()->id)->update($upd);
         }
         $data['allSubscription'] = AgentMemberShipPackage::with('userToPackage')->whereIn('id',[2,3,4])->get();
         $data['defaultPackage'] = AgentMemberShipPackage::where('id',1)->first();
         $data['currentPackage'] = UserToPackage::with('UserPackageName')->where('user_id',Auth::user()->id)->first();
         $data['user'] = User::where('id',Auth::user()->id)->first();
        
        return view('modules.agent.subcription')->with($data);
    }

    public function agentSetSubscription($id=null){

           $check = AgentMemberShipPackage::where('id',$id)->first();
           if(@$check == null){

               session()->flash('error','something went wrong');
               return redirect()->back();
           }
           $upd = [];
           if(@$check->type == 'M'){

               $upd['payment_freq'] = 'M';
               $startDate = date('Y-m-d');
               $upd['package_expiry_date'] = date('Y-m-d',strtotime(date('Y-m-d',strtotime($startDate))."+30 day"));
           }
           else if(@$check->type == 'Y'){

            $upd['payment_freq'] = 'Y';
            $startDate = date('Y-m-d');
            $upd['package_expiry_date'] = date('Y-m-d',strtotime(date('Y-m-d',strtotime($startDate))."+365 day"));

           }
           User::where('id',Auth::user()->id)->update($upd);
           
          $update =  UserToPackage::where('user_id',Auth::user()->id)->update(['agent_package_id'=>@$check->id]);

          if(@$update){

            session()->flash('success','Your Package updated successfully');
            return redirect()->back();
          }
    }
}

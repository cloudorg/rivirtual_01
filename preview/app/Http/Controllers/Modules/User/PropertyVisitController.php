<?php

namespace App\Http\Controllers\Modules\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Mail;
use App\Country;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\AgentToAvailability;
use App\Models\PropertyVisitRequest;
use App\Models\Property;


class PropertyVisitController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *   Method      : visitRequestStore
     *   Description : for user visit Request Store
     *   Author      : Soumojit
     *   Date        : 2021-Nov-24
     **/
   public function visitRequestStore(Request $request){
        
        $request->validate([
            'property' => 'required',
            'date' => 'required',
            'time' => 'required',
            // 'address' => 'required',
            'phonenumber' => 'required',
        ]);

        $property=Property::where('id',$request->property)->first();
        if($property==null){
            session()->flash('error', 'Something went wrong');
            return redirect()->back();
        }
        $ins=[];
        $ins['user_id']=Auth::user()->id;
        $ins['property_id']=$request->property;
        $ins['visit_date']=date('Y-m-d H:i:s',strtotime($request->date.$request->time));
        $ins['address']=$request->address;
        $ins['phone']=$request->phonenumber;
        $ins['agent_id']=$property->user_id;
        $check = PropertyVisitRequest::where('user_id',Auth::user()->id)->where('property_id',$request->property)->whereDay('visit_date','=',date('d',strtotime($request->date)))->whereMonth('visit_date','=',date('m',strtotime($request->date)))->whereYear('visit_date','=',date('Y',strtotime($request->date)))->first();
        $property_check = Property::where('user_id',Auth::user()->id)->where('id',$request->property)->first();
        if(@$check){

            session()->flash('error', 'Visit Request already exist');
            return redirect()->back();

        }else if($property_check){

            session()->flash('error', 'Agent can not send Visit Request to his own property');
            return redirect()->back(); 
        }
        else{
            $user = User::where('id',Auth::user()->id)->first();
            $send_property_request_sms_user = $this->sendPropertyVisitRequestUser(@$user->mobile_number,@$request->date,$ins['property_id']);
            $agent = User::where('id',@$property->user_id)->first();
            
            $send_property_request_sms_agent = $this->sendPropertyVisitRequestAgent(@$agent->mobile_number,@$request->date,$user->name);
           
            $create =PropertyVisitRequest::create($ins);
            if( @$create){
                session()->flash('success', 'Visiting request send to agent');
                return redirect()->back();
            }    
        }
        
        session()->flash('error', 'Something went wrong');
        return redirect()->back();

    }

    /**
     *   Method      : visitRequestCheck
     *   Description : for user visit Request Check
     *   Author      : Soumojit
     *   Date        : 2021-Nov-25
     **/

    public function visitRequestCheck(Request $request){

        $from_time=date('Y-m-d H:i:s',strtotime($request->date.$request->time));
        $to_time=date('Y-m-d H:i:s',strtotime("-1 seconds",strtotime($from_time)));
        $to_time=date('Y-m-d H:i:s',strtotime("30 minutes",strtotime($to_time)));
        $check=PropertyVisitRequest::whereBetween('visit_date',[$from_time,$to_time])->where('agent_id',$request->user_id)->count();
        if($check>0){
            return response('false');
        }else{
            return response('true');
        }
        return response('no Slot');
    }


    public function userVisitRequest(Request $request){

        $allVisitRequest = PropertyVisitRequest::with(['propertyDetails','agentDetails'])->where('user_id',Auth::user()->id)->whereNotIn('status',['CA','C']);
        if($request->all()){

             if($request->property_name){

               $allVisitRequest = $allVisitRequest->whereHas('propertyDetails', function($q) use($request){

                     $q->where('name','like','%'.$request->property_name.'%')
                     ->orWhere('slug','like','%'.str_slug($request->property_name).'%');
               });
           }
               if($request->start_date){

                   $allVisitRequest = $allVisitRequest->where(DB::raw("DATE(visit_date)"),'=',date('Y-m-d',strtotime($request->start_date)));
               }
               if($request->time){

                   $allVisitRequest = $allVisitRequest->where(DB::raw("DATE(visit_date)"),'=',date('h:i',strtotime($request->time)));
               }

            $data['key'] = $request->all();

        }
         $allVisitRequest = $allVisitRequest->orderBy('id','desc')->paginate(10);
         $data['allVisitRequest'] = $allVisitRequest;
          return view('modules.user.property_visit_request_list')->with($data);
    }

    public function userVisitRequestCancel($id=null){


    $check= PropertyVisitRequest::where('id',$id)->first();
    if($check==null){
        session()->flash('error','Something went wrong');
        return redirect()->route('user.property.visit.request');
    }

    PropertyVisitRequest::where('id',$id)->update(['status'=>'CA']);
    session()->flash('success','Property visit request cancel successfully');
    return redirect()->route('user.property.visit.request');
    }
    
    public function sendPropertyVisitRequestUser($number,$date,$prop_id){
        $apiKey = urlencode('NDEzODdhNjM0YTc3NDk2OTQ5MzI3OTY5NmI0YzM4NmY=');
        
        $num = '91'.$number;
        $numbers = urlencode($num); 
        $sender = urlencode('ITREAL');
        // $message = 'Use '.$otp.' OTP to verify your mobile number.';
        $message = rawurlencode('You requested a property visit on '.@$date.' to Property id ' .@$prop_id.'.');
       
        $data = 'apikey=' . $apiKey . '&numbers=' . $numbers . "&sender=" . $sender . "&message=" . $message;
        
        $ch = curl_init('https://api.textlocal.in/send/?' . $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        
        return $response;
    }

    public function sendPropertyVisitRequestAgent($number,$date,$name){
        $apiKey = urlencode('NDEzODdhNjM0YTc3NDk2OTQ5MzI3OTY5NmI0YzM4NmY=');
        
        $num = '91'.$number;
        $numbers = urlencode($num); 
        $sender = urlencode('ITREAL');
        // $message = 'Use '.$otp.' OTP to verify your mobile number.';
        $message = rawurlencode('You have a property visit request on '.@$date.' by user '.@$name.'.
RiVirtual - iTester team');

       
        $data = 'apikey=' . $apiKey . '&numbers=' . $numbers . "&sender=" . $sender . "&message=" . $message;
        
        $ch = curl_init('https://api.textlocal.in/send/?' . $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        
        return $response;

    }

}

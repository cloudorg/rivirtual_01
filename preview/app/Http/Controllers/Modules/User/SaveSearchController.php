<?php

namespace App\Http\Controllers\Modules\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Mail;
use App\Country;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\City;
use App\Models\State;
use App\Models\PropertyVisitRequest;
use App\Models\Property;
use App\Models\PostJob;
use App\Models\UserToSaveSearch;
class SaveSearchController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function userSaveProperty(){

         $data['saveProperty'] = UserToSaveSearch::with('propertyDetails')->where('user_id',Auth::user()->id)
         ->where('property_id','!=',null)->orderBy('id','desc')->paginate(6);

          return view('modules.user.save_search_property')->with($data);
    }

    public function userSaveAgent(){
      $data['saveAgentData'] = UserToSaveSearch::with(['agentDetails','totalProperty'])->where('user_id',Auth::user()->id)
      ->where('agent_id','!=',null)->orderBy('id','desc')->paginate(6);    
          return view('modules.user.save_search_agent')->with($data);
    }

    public function userSavePro(){

      $data['saveProData'] = UserToSaveSearch::with(['proDetails','proToLanguage','proToCategory'])->where('user_id',Auth::user()->id)
      ->where('pro_id','!=',null)->orderBy('id','desc')->paginate(6);

          return view('modules.user.save_search_pro')->with($data);
    }
      public function userSavePropertyDelete($id=null){

          $check = UserToSaveSearch::where('id',$id)->first();

          if(@$check == null){

              session()->flash('error','Something went wrong');
              return redirect()->route('user.save.property');
          }

          UserToSaveSearch::where('id',$id)->delete();
          session()->flash('success','This property has been deleted successfully');
          return redirect()->route('user.save.property');
    }
      public function userSaveAgentDelete($id=null){

        $check = UserToSaveSearch::where('id',$id)->first();

        if(@$check == null){

            session()->flash('error','Something went wrong');
            return redirect()->route('user.save.agent');
        }

        UserToSaveSearch::where('id',$id)->delete();
        session()->flash('success','This agent has been deleted successfully');
        return redirect()->route('user.save.agent');
    }
      public function userSaveProDelete($id=null){

        $check = UserToSaveSearch::where('id',$id)->first();

        if(@$check == null){

            session()->flash('error','Something went wrong');
            return redirect()->route('user.save.pro');
        }

        UserToSaveSearch::where('id',$id)->delete();
        session()->flash('success','This pro has been deleted successfully');
        return redirect()->route('user.save.pro');
    }
    
}

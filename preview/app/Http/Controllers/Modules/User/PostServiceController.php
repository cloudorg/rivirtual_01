<?php

namespace App\Http\Controllers\Modules\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use DB;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\PostJob;
use App\Models\Quotes;
use App\Models\State;
use App\Models\City;
use Mail;
use App\Mail\JobEditMail;
use App\Mail\JobCreateMail;
use App\Mail\AwardMail;

use App\User;
use App\Models\JobToProReview;
use App\Models\JobToImage;
use Storage;
use App\Http\Controllers\Modules\Message\MessageController;
class PostServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *   Method      : addPostJob
     *   Description : for user post job add
     *   Author      : sudipta
     *   Date        : 2021-DEC-11
     **/
   
    public function addPostJob($id=null){
        $data['category'] = Category::where('status','A')->get();
        $data['states'] = State::where('country_id',101)->get();
        $data['cites'] = [];
        $data['allSubCategory']= [];
         if($id==null){

              return view('modules.user.post_job')->with($data);
         }

         $data['postDetails']=PostJob::where('id',$id)->where('user_id',auth()->user()->id)->first();
        if($data['postDetails']==null){
            session()->flash('error', 'Something went wrong');
            return redirect()->route('user.post.job');
        }
        $data['cites'] = City::where('state_id',$data['postDetails']->state)->get();
        $data['allSubCategory'] = SubCategory::where('category_id',$data['postDetails']->category)->get();
        $data['jobImages'] = JobToImage::where('post_id',$data['postDetails']->id)->get();
        return view('modules.user.post_job')->with($data);
    }

      /**
     *   Method      : addPostJobSave
     *   Description : for user post job save
     *   Author      : sudipta
     *   Date        : 2021-DEC-11
     **/

    public function addPostJobSave(Request $request,$id=null){

          $request->validate([

               'job_title'=>'required',
               'category'=>'required',
               'day'=>'required',
               'start_date'=>'required',
               'time'=>'required',
               //'budget_type'=>'required',
               //'budget'=>'required',
               'description'=>'required'
          ]);
          if(@$request->start_date && (date('Y-m-d',strtotime(@$request->start_date))) < (date('Y-m-d'))){
            session()->flash('error', 'Please insert valid date');
            return redirect()->back();
        }
          if($id == null){

               $ins = [];
               $ins['job_name'] = $request->job_title;
               $ins['category'] = $request->category;
               $ins['sub_category'] = $request->sub_category;
               $ins['user_id'] = Auth::user()->id;
               $ins['duration'] = $request->day;
               $ins['job_start_date'] = date('Y-m-d',strtotime($request->start_date));
               $ins['budget_range_from'] = $request->budget_range_from;
               $ins['budget_range_to'] = $request->budget_range_to;
               $ins['time'] = date('H:i:s',strtotime($request->time));
               $ins['address'] = $request->address;
               $ins['country'] = $request->country;
               $ins['state'] = $request->state;
               $ins['city'] = $request->city;
               $ins['description'] = $request->description;
               $ins['status'] = 'A';
               $ins['job_status'] = 'O';
               
              
            //    if (@$request->service_image) {
            //     // $image = $request->profile_pic;
            //      // $filename = time() . '-' . rand(1000, 9999) . '.' . $image->getClientOriginalExtension();
            //      // Storage::putFileAs('public/profile_picture', $image, $filename);
            //      // $upd['profile_img'] = $filename;
            //      // @unlink(storage_path('app/public/profile_picture/' . auth()->user()->profile_img));
            //     // @unlink(storage_path('app/public/profile_picture/' . $existuser->profile_pic));
            //      $destinationPath = "storage/app/public/service_image/";
            //      $img1 = str_replace('data:image/png;base64,', '', @$request->service_image);
            //      $img1 = str_replace(' ', '+', $img1);
            //      $image_base64 = base64_decode($img1);
            //      $img = time() . '-' . rand(1000, 9999) . '.png';
            //      $file = $destinationPath . $img;
            //      file_put_contents($file, $image_base64);
            //      chmod($file, 0755);
            //      $ins['image'] = $img;
            //  }
               $create = PostJob::create($ins);
              
               if($create){

                $upd=[];
                $slug= str_slug(@$request->job_title, "-");
                $upd['slug'] = $slug. "-".$create->id;
                PostJob::where('id',$create->id)->update($upd);
                $files1 = $request->file('file-1');
                
                   
                if($request->hasFile('file-1'))
                {
                    foreach ($files1 as $file) {
                        $filename = time() . '-' . rand(1000, 9999) . '.' . $file->getClientOriginalExtension();
                        Storage::putFileAs('public/service_image', $file, $filename);
                        $insImage=[];
                        $insImage['image']=$filename;
                        $insImage['post_id']=@$create->id;
                        
                        JobToImage::create($insImage);
                    }
                }
                $user = User::where('id',@$ins['user_id'])->first();
                $job =  PostJob::where('id',$create->id)->first();
                $send_job_live_sms = $this->sendJobLiveSms(@$user->mobile_number,@$job->job_name,@$job->id);
                $data['user_name'] = Auth::user()->name;
                $data['job_name'] = $job->job_name;
                $data['email'] = Auth::user()->email;
                
                Mail::send(new JobCreateMail($data));
                
                session()->flash('success', 'Post service details added successfully');
                return redirect()->route('user.post.job');
               }
                
               session()->flash('error', 'Post service details not added');
            return redirect()->route('user.post.job');
          }

          $postDetails=PostJob::where('id',$id)->where('user_id',auth()->user()->id)->first();
          if($postDetails==null){
              session()->flash('error', 'Something went wrong');
              return redirect()->back();
          }

          



          $upd = [];
          $upd['job_name'] = $request->job_title;
          $upd['category'] = $request->category;
          $upd['sub_category'] = $request->sub_category;
          $upd['user_id'] = Auth::user()->id;
          $upd['duration'] = $request->day;
          $upd['job_start_date'] = date('Y-m-d',strtotime($request->start_date));
          $upd['budget_range_from'] = $request->budget_range_from;
          $upd['budget_range_to'] = $request->budget_range_to;
          $upd['time'] = date('H:i:s',strtotime($request->time));
          $upd['address'] = $request->address;
          $upd['country'] = $request->country;
          $upd['state'] = $request->state;
          $upd['city'] = $request->city;
          $upd['description'] = $request->description;
          $slug = str_slug($request->job_title,"-");
          $upd['slug'] = $slug."-".@$postDetails->id;
          $upd['status'] = 'A';
          $upd['job_status'] = 'O';

        //   if (@$request->service_image) {
        //     // $image = $request->profile_pic;
        //      // $filename = time() . '-' . rand(1000, 9999) . '.' . $image->getClientOriginalExtension();
        //      // Storage::putFileAs('public/profile_picture', $image, $filename);
        //      // $upd['profile_img'] = $filename;
        //      // @unlink(storage_path('app/public/profile_picture/' . auth()->user()->profile_img));
        //      @unlink(storage_path('app/public/service_image/' . $postDetails->image));
        //      $destinationPath = "storage/app/public/service_image/";
        //      $img1 = str_replace('data:image/png;base64,', '', @$request->service_image);
        //      $img1 = str_replace(' ', '+', $img1);
        //      $image_base64 = base64_decode($img1);
        //      $img = time() . '-' . rand(1000, 9999) . '.png';
        //      $file = $destinationPath . $img;
        //      file_put_contents($file, $image_base64);
        //      chmod($file, 0755);
        //      $upd['image'] = $img;
        //  }
        $files1 = $request->file('file-1');
        if($request->hasFile('file-1'))
        {
            foreach ($files1 as $file) {
                $filename = time() . '-' . rand(1000, 9999) . '.' . $file->getClientOriginalExtension();
                Storage::putFileAs('public/service_image', $file, $filename);
                $insImage=[];
                $insImage['image']=$filename;
                $insImage['post_id']=@$postDetails->id;
                
                JobToImage::create($insImage);
            }
        }
          
         $updated=PostJob::where('id',$postDetails->id)->update($upd);
         $bidder = Quotes::where('post_id',$id)->get();
          foreach(@$bidder as $bid){
            $data['user_name'] = Auth::user()->name;
            $data['name'] = $bid->proDetails->name;
            $data['job_name'] = $bid->jobDetails->job_name;
            $data['email'] = $bid->proDetails->email;
            $data['slug'] = $bid->jobDetails->slug;
            Mail::send(new JobEditMail($data));
          }
        if(@$updated){
            session()->flash('success', 'Post service details updated successfully');
            return redirect()->route('user.post.job',['id'=>$postDetails->id]);
        }
        session()->flash('error', 'Post service details not updated');
        return redirect()->back();
    }

    public function jobImageRemove($id=null){

      $insImage=JobToImage::where('id',$id)->first();
      if($insImage==null){
          session()->flash('error', 'Something went wrong image not removed');
          return redirect()->back();
      }
      $postDetails=PostJob::where('id',$insImage->post_id)->where('user_id',auth()->user()->id)->first();
      if($postDetails==null){
          session()->flash('error', 'Something went wrong image not removed');
          return redirect()->back();
      }

    //   $image= JobToImage::where('post_id',$insImage->post_id)->count();

    //   if($image==1){
    //     session()->flash('error', 'Image not removed minimum one image require');
    //     return redirect()->back();
    // }
    @unlink(storage_path('app/public/service_image/' .$insImage->image));
    JobToImage::where('id', $insImage->id)->delete();
    session()->flash('success', 'Job image removed');
    return redirect()->back();

    }

      /**
     *   Method      : myPostJob
     *   Description : for user post job listing page
     *   Author      : sudipta
     *   Date        : 2021-DEC-11
     **/

 
   public function myPostJob(Request $request){

       $data['category'] = Category::where('status','A')->get();
       $data['postData'] = $this->getBids($request);
       $data['jobProgressData '] = $this->getBids($request);
       $data['jobClosedData'] = $this->getBids($request);
       $data['allJobData'] = $this->getBids($request);
       $postData =$data['postData']->where('job_status','O')->where('status','A');
       $jobProgressData =$data['jobProgressData ']->where('job_status','IP')->where('status','A');
       $jobClosedData = $data['jobClosedData']->with(['proDetails','reviewDetails'])->where('job_status','C')->where('status','A');
       $allJobData =$data['allJobData']->where('status','A');
       $data['posts'] = $postData->orderBy('id','desc')->paginate(10,['*'],'page1');
       $data['jobProgressData'] = $jobProgressData->orderBy('id','desc')->paginate(10,['*'],'page2');
       $data['jobClosedData'] = $jobClosedData->orderBy('id','desc')->paginate(10,['*'],'page3');
       $data['allJobData'] = $allJobData->orderBy('id','desc')->paginate(10,['*'],'page4');
       if($request->all()){ 
         $data['key'] = @$request->all();
       }
       return view('modules.user.my_post_job')->with($data);
   }

   public function getBids($request){
    //$data['category'] = Category::where('status','A')->get();
    $postData = PostJob::where('user_id',Auth::user()->id)->with('categoryName');
        if($request->category){

              $postData = $postData->where(function($q) use($request){

                  $q->where('category',$request->category);
              });
         }
         if($request->job_title){

          $postData = $postData->where(function($q) use($request){

              $q->where('job_name','like','%'.$request->job_title.'%')
              ->orWhere('slug','like','%'.str_slug($request->job_title));
          });
     }
     if($request->start_date){

      $postData = $postData->where(function($q) use($request){

           $q->where(DB::raw("DATE(job_start_date)"),'=',date('Y-m-d',strtotime($request->start_date)));
      });
 }
        // $data['key'] = @$request->all();
        return $postData;
}

     /**
     *   Method      : myPostJobDelete
     *   Description : for user post job delete
     *   Author      : sudipta
     *   Date        : 2021-DEC-11
     **/

   public function myPostJobDelete($id=null){

        
    $check= PostJob::where('id',$id)->first();
    if($check==null){
        session()->flash('error','Something went wrong');
        return redirect()->route('user.my.post.job');
    }

    PostJob::where('id',$id)->update(['status'=>'D']);
    session()->flash('success','Post service deleted successfully');
    return redirect()->route('user.my.post.job');
   }

   public function proposalList(Request $request,$id=null){
        $data['jobDetails'] = PostJob::with('categoryName','getMilestone')->where('id',$id)->where('status','A')->first();
        $data['allProposal'] = Quotes::with(['proDetails','jobDetails','jobDetails.getMilestone'])->where('post_id',$id);
        
        if($request->all()){

            if(@$request->sort_by_value){
                if(@$request->sort_by_value == 'H'){
                    $data['allProposal'] = $data['allProposal']->orderBy('cost','desc');
                    
                }
                else if(@$request->sort_by_value == 'L'){
                    $data['allProposal'] = $data['allProposal']->orderBy('cost','asc');
                }
                else if($request->sort_by_value == 'N'){

                     $data['allProposal'] = $data['allProposal']->orderBy('id','desc');
                }
                 
                // else if(@$request->sort_by_value == 'RH'){
                //     $allJobs = $allJobs->orderBy('tot_review','desc');
                // }
                // else if(@$request->sort_by_value == 'RL'){
                //     $allJobs = $allJobs->orderBy('tot_review','asc');
                // }
            }
            $data['request'] = $request->all();
        }
        if(empty($request->sort_by_value)){

             $data['allProposal'] = $data['allProposal']->orderBy('is_recommended','desc');
            
        }
        

        $data['allProposal'] = $data['allProposal']->paginate(10);
        
        return view('modules.user.view_proposal_list')->with($data);
   }

    public function jobAward($id=null,$socket_id=null){

          $check = Quotes::where('id',$id)->whereIn('status',['Bid Placed','Awarded','Revoked'])->first();
          $all_quotes = Quotes::where('post_id',$check->post_id)->where('id','!=',$check->id)->get();
          
          if($check == null){

            session()->flash('error','Something went wrong');
            return redirect()->back();
          }

          if(@$check->status == 'Bid Placed'){

            Quotes::where('id',$id)->update(['status'=>'Awarded']);
            PostJob::where('id',@$check->post_id)->update(['job_status'=>'IP']);
            $job = PostJob::where('id',@$check->post_id)->first();
            foreach(@$all_quotes as $q){
                $up['status'] = 'Closed';
                $q->update($up);
            }
            $messageController = new MessageController;
                $re = new Request();
                $re->user_id=$check->user_id;
                $re->property_id=0;
                $re->provider_id=$check->user_id;
                $re->type='';
                // $re->bid_id = $check->id;
                // $re->u_type = 'P';
                // $re->msgtyp = 'Pr';
                $re->from=Auth::user()->name;
                $re->name=Auth::user()->name;
                if($socket_id){
                    $re->socket_id=$socket_id;
                }else{
                    $soc_id = mt_rand(4,999999)*0.007;
                    $re->socket_id=strval($soc_id);
                }
                $re->message=auth()->user()->name. " has awarded the project ".@$job->job_name;
            
                $messageController->sendAjax($re);
                $pro = User::where('id',@$check->user_id)->first();
                $data['email'] = @$pro->email;
                $data['job_name'] = @$job->job_name;
                $data['user_name'] = Auth::user()->name;
                $data['bidder_name'] = $pro->name;
                Mail::send(new AwardMail($data));
            session()->flash('success','This job has been awarded successfully');
            return redirect()->back();
          }
          if(@$check->status == 'Awarded'){

            Quotes::where('id',$id)->update(['status'=>'Bid Placed']);
            PostJob::where('id',@$check->post_id)->update(['job_status'=>'O']);
            foreach(@$all_quotes as $q){
                $up['status'] = 'Bid Placed';
                $q->update($up);
            }
            session()->flash('success','job is revoked successfully');
            return redirect()->back();
          }
          if(@$check->status == 'Revoked'){

            Quotes::where('id',$id)->update(['status'=>'Awarded']);
            PostJob::where('id',@$check->post_id)->update(['job_status'=>'IP']);
            session()->flash('success','This job has been awarded successfully');
            return redirect()->back();
          }
          

         
    }

    public function jobRatingSave(Request $request){

          $job_check = PostJob::where('id',$request->job_id)->where('status','A')->first();

          if(@$job_check == null){

               session()->flash('error','something went wrong');
               return redirect()->back();
          }
          $existing_rating = JobToProReview::where('job_id',$request->job_id)->where('posted_by_id',Auth::user()->id)->first();

          if($existing_rating){
              
            session()->flash('error','Already placed review');
            return redirect()->back();
            //   $upd = [];
            //   $upd['review_point'] = $request->property_rating;
            //   $upd['review_text'] = $request->description;

            //  $update =  JobToProReview::where('job_id',$request->job_id)->where('posted_by_id',Auth::user()->id)->update($upd);
            //  if($update){

                
            //     $pro_rating = JobToProReview::where('provider_id',$request->pro_id)->get();
            //     $pro_rating_sum = JobToProReview::where('provider_id',$request->pro_id)->sum('review_point');
                
            //    if($pro_rating->count()>0){

            //         $pro_avg_review = $pro_rating_sum/$pro_rating->count(); 
            //    }else{

            //        $pro_avg_review = 0.00;
            //    }
            //    $agentrating = [];
            //    $agentrating['avg_review'] = $pro_avg_review;
            //    $agentrating['tot_review'] = $pro_rating->count();
            //    User::where('id',$request->pro_id)->update($agentrating);
            //    session()->flash('success','Job review updated successfully');
            //     return redirect()->back();
            //  }
          }
          else{

               $ins = [];
               $ins['job_id'] = $request->job_id;
               $ins['provider_id'] = $request->pro_id;
               $ins['posted_by_id'] = Auth::user()->id;
               $ins['review_point'] = $request->property_rating;
               $ins['review_text'] = $request->description;

              $create =  JobToProReview::create($ins);
            
              if($create){

                $pro_rating = JobToProReview::where('provider_id',$request->pro_id)->get();
                $pro_rating_sum = JobToProReview::where('provider_id',$request->pro_id)->sum('review_point');                
               
                if($pro_rating->count()>0){

                     $pro_avg_review = $pro_rating_sum/$pro_rating->count();
                }else{

                    $pro_avg_review = 0.00; 
                }
                $ratingupd = [];
                $ratingupd['avg_review'] = $pro_avg_review;
                $ratingupd['tot_review'] = $pro_rating->count();

                User::where('id',$request->pro_id)->update($ratingupd);
               
                session()->flash('success','Job review posted successfully');
                return redirect()->back();
              }
          }
    }

    public function getRivirtualAssist($id=null){

             
    $check= PostJob::where('id',$id)->first();
    if($check==null){
        session()->flash('error','Something went wrong');
        return redirect()->route('user.my.post.job');
    }

    if(@$check->rivirtual_assistance != 'Y'){

         $update['rivirtual_assistance'] = 'Y';
    }else{

      $update['rivirtual_assistance'] = 'N'; 
    }

    PostJob::where('id',$id)->update($update);
    session()->flash('success','Rivirtual assistance get successfully');
    return redirect()->route('user.my.post.job');
    }
    
    
    public function sendJobLiveSms($number,$job_name,$job_id){
        $apiKey = urlencode('NDEzODdhNjM0YTc3NDk2OTQ5MzI3OTY5NmI0YzM4NmY=');
        $num = '91'.$number;
        $numbers = urlencode($num); 
        $sender = urlencode('ITREAL');
        // $message = 'Use '.$otp.' OTP to verify your mobile number.';
        $message = rawurlencode('Your Job id '.@$job_id.'- '.@$job_name.' is live! RIVirtual and iTester Team');
          
        $data = 'apikey=' . $apiKey . '&numbers=' . $numbers . "&sender=" . $sender . "&message=" . $message;
        
        $ch = curl_init('https://api.textlocal.in/send/?' . $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        
        return $response;

    }

    
}

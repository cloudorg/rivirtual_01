<?php

namespace App\Http\Controllers\Modules\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use DB;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\PostJob;
use App\Models\Quotes;
use App\Models\State;
use App\Models\City;
use Mail;
use App\Mail\JobEditMail;
use App\Mail\ReleaseMail;
use App\User;
use App\Models\JobToProReview;
use App\Models\JobToImage;
use Storage;
use App\Models\Milestone;
use App\Models\UserTransaction;
class MilestoneController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *   Method      : createMilestone
     *   Description : Create Milestone
     *   Author      : Puja
     *   Date        : 2022-Feb-01
     **/
   
    public function getMilestone($id=null){
        
        $data['quotes'] = Quotes::where('id',$id)->first();
        $data['jobDetails'] = PostJob::where('id',$data['quotes']->post_id)->first();
        
        
        $data['createdM']   = Milestone::where('job_id',$data['jobDetails']->id)
                                        ->whereIn('status',['F','R'])
                                        ->get();
        $data['createdAmt']   = Milestone::where(['job_id'=>$data['jobDetails']->id])
                                        ->whereIn('status',['F'])
                                        ->sum('amount');

                                        
       $data['requestAmt'] = 0;

        $data['releaseAmt']   = Milestone::where(['job_id'=>$data['jobDetails']->id])
                                        ->where('status','R')
                                        ->sum('amount');
        $amt = $data['quotes']->cost;
        $data['yetToPay'] = $amt-($data['releaseAmt'] + $data['createdAmt']);

        if(@$data['yetToPay']){
           
            $data['max_limit']=$data['yetToPay'];

        }elseif(@$data['yetToPay'] == 0){
            $data['max_limit']= 0;
        }
        else{
            $data['max_limit'] = $data['quotes']->cost;   
        }
        
        
        if($data['max_limit'] < 0){
            
            $data['max_limit']=0; 
        }
        // if($data['yetToPay'] < 0){
        //     $data['yetToPay']=0; 
        // }
        


        return view('modules.user.view_milestone')->with($data);


    }


    public function makePayment(Request $request){
        // dd($request->all());
        $data['provider'] = User::find($request->pro_id);
        $data['post'] = PostJob::find($request->post_id);
        $data['bid'] = Quotes::where('post_id',$request->post_id)->where('user_id',$request->pro_id)->first();
        $BIDAMT = $amount = $request->amount;
        
        $amount_INR = $request->amount;
        if(@$request->bidamt){
            $BIDAMT = $request->bidamt;
        }

        $getMisAmt = Milestone::where(['job_id'=>@$request->post_id,'pro_id'=>@$request->pro_id])->whereIn('status',['Funded','Released'])->sum('amount');

        $getFrBid = Quotes::where(['post_id'=>@$request->post_id,'user_id'=>@$request->pro_id])->first();

        $amt = @$getFrBid->amount;

        $input['job_id'] = $request->post_id;
        $input['bid_id'] = $request->bid_id;
        $input['pro_id'] = $request->pro_id;
        $input['user_id'] = $request->user_id;
        $input['amount'] = $request->amount;
        $input['status'] = 'F';
        $input['description'] = $request->description;

        $m_id = Milestone::insertGetId($input);

        $insert['milestone_id'] = $m_id;
        $insert['job_id'] = $request->post_id;
        $insert['amount'] = $request->amount;
        $insert['type'] = 'OUT';
        $insert['user_id'] = Auth::user()->id;
        $insert['category'] = 'MC';
        UserTransaction::insertGetId($insert);


        
        // dd($getMisAmt);
        // $projectFee = ProjectFees::first();
        // $data['getaway'] = $projectFee->payment_gateway_charges*$amount/100;
        // $service_fee = $projectFee->service_fees*$amount/100;
        // if($projectFee->minimum_fixed_amount_INR>=$service_fee){
        //     $data['service_fee'] = $projectFee->minimum_fixed_amount_INR;
        // } else{
        //     $data['service_fee'] = $service_fee;
        // }
        //  $data['pay_pal_gat'] = $projectFee->payment_gateway_charges;
        
        // $data['pay_pal_chrg'] = (($amount +  $data['service_fee'] ) * $projectFee->payment_gateway_charges/100);

        // $data['service_fee_getaway'] = $data['service_fee'] + (($amount + $data['service_fee']) * $projectFee->payment_gateway_charges/100);
        // // $data['service_fee_getaway'] = $data['service_fee'] + $data['getaway'];

        // $data['currency_id'] = @$currency;
        // $data['bid_id'] = $data['bid']->id;
        // $data['project_type'] = @$request->project_type;
        // $data['milestone_hours'] = @$request->milestone_hours;
        // $data['amount'] = $amount;
        // $data['page_type'] = 'M';
        
        // $data['wallat_amount'] = $amount+$data['service_fee'];
        // $data['gataway_amount'] = $amount+$data['service_fee_getaway'];
        // $data['project_id'] = $request->project_id;
        // $data['freelancer_id'] = $request->freelancer_id;
        // $data['description'] = $request->description;

        // $data['wallet'] = UserWallet::where('user_id',Auth::id())->first();
        // if(@$data['wallet']->balance && @$data['wallet']->balance>0 && @$data['wallet']->balance <$data['amount'] ){
        //     $data['reamining_payble']= $data['amount']-@$data['wallet']->balance;
        //     $data['payable_from_wallet']=$data['amount']-$data['reamining_payble'];
        //     $data['getaway'] = $projectFee->payment_gateway_charges *  $data['reamining_payble']/100;
        //     $data['service_fee_getaway'] = $data['service_fee'] + $data['getaway'];
        //     $data['gataway_amount'] = $amount+$data['service_fee_getaway'];
        //     // dd($data);
        // }
        // Session::put('first_mis', $data);
        
        return redirect()->back()->with('success','Milestone created Successfully');
        
    }


    public function cancelMilestone(Request $request){
        $milestone = Milestone::where('id',$request->id)->first();
        $response=array();
        if($milestone){
            $result = $milestone->delete();
        }
        if($result){
            $response['status'] = 1;   
        }else{
            $response['status'] = 0;  
        }
        
        
        return response()->json($response);
    }
    public function releaseMilestoneRequest(Request $request){
        $milestone = Milestone::where('id',$request->id)->first();

        $upd['is_requested_release'] = 'Y';

        $result = $milestone->update($upd);
        $response=array();
        if($result){
            $response['status'] = 1;   
        }else{
            $response['status'] = 0;  
        }
        
        
        return response()->json($response);



    }

    public function releaseMilestone(Request $request){
        $milestone = Milestone::where('id',$request->id)->first();
        $createdAmt   = Milestone::where(['job_id'=>@$milestone->job_id])
                                        ->whereIn('status',['F'])
                                        ->sum('amount');
        
        $releaseAmt   = Milestone::where(['job_id'=>@$milestone->job_id])
                                        ->where('status','R')
                                        ->sum('amount');
                                        
        

        $pro = User::where('id',@$milestone->pro_id)->first();
        $pro->tot_earning = $pro->tot_earning + $milestone->amount;
        $pro->tot_due = $pro->tot_due + $milestone->amount;
        $pro->update(['tot_earning' => $pro->tot_earning,'tot_due' => $pro->tot_due]);
        $insert['milestone_id'] = $milestone->id;
        $insert['job_id'] = $milestone->jobDetails->id;
        $insert['amount'] = $milestone->amount;
        $insert['user_id'] = $pro->id;
        $insert['type'] = 'IN';
        $insert['category'] = 'MR';
        $response=array();
        if($milestone){
            UserTransaction::insertGetId($insert);
            $result = $milestone->update(['status'=> 'R']);
            $post = PostJob::where('id',@$milestone->jobDetails->id)->first();
            $data['email'] = @$pro->email;
            $data['bidder_name'] = @$pro->name;
            $data['user_name'] = Auth::user()->name;
            $data['des'] = @$milestone->description;
            $data['amount'] = @$milestone->com_amount;
            $data['job_name'] = @$post->job_name;
            Mail::send(new ReleaseMail($data));
            if($createdAmt <= $releaseAmt){
                
                PostJob::where('id',@$milestone->job_id)->update(['job_status'=>'C']);
            }


        }
        if($result){
            $response['status'] = 1;   
        }else{
            $response['status'] = 0;  
        }
        
        
        return response()->json($response);
    }


      
    
}

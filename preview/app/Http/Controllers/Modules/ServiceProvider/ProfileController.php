<?php

namespace App\Http\Controllers\Modules\ServiceProvider;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Mail;
use App\Country;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Storage;
use App\Models\Category;
use App\Models\Language;
use App\Models\Skill;
use App\Models\ProviderToLanguage;
use App\Models\ProviderToSkills;
use App\Models\ProviderToImage;
use App\Models\City;
use App\Models\State;
use App\Models\SubCategory;
use App\Models\ProToCategory;
use App\Models\PostJob;
use App\Models\Quotes;
use App\Models\Job;
use App\Models\ProPackage;
use App\Models\UserToPackage;
use App\Models\JobToProReview;
use App\Models\Milestone;
use App\Models\MessageMaster;
use App\Mail\QuotesCreateMail;
use App\Mail\QuotesPostedMail;





class ProfileController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *   Method      : dashboard
     *   Description : for service provider Dashboard
     *   Author      : Soumojit
     *   Date        : 2021-OCT-28
     **/

    public function dashboard(){
        $data['allJobs'] = Quotes::with('jobDetails')->where('user_id',Auth::user()->id)->where('status','Awarded')->get();
        $data['f_mile_amt'] = Milestone::where('pro_id',Auth::user()->id)->where('status','F')->sum('amount');
        $data['r_mile_amt'] = Milestone::where('pro_id',Auth::user()->id)->where('status','R')->sum('amount');

    
        return view('modules.service_provider.dashboard')->with($data);
    }
    /**
     *   Method      : editProfile
     *   Description : for service provider editProfile view page
     *   Author      : Soumojit
     *   Date        : 2021-OCT-28
     **/

    public function editProfile(){
        $data['allCountry']=Country::get();
        $data['allCategory']=Category::where('status','A')->get();
        $data['allLanguage']=Language::where('status','!=','D')->get();
        $data['allSkill']=Skill::where('status','!=','D')->get();
        $data['allUserLanguage']=ProviderToLanguage::where('user_id', Auth::user()->id)->get();
        $data['allUserSkill']=ProviderToSkills::where('user_id', Auth::user()->id)->get();
        $data['allWorkImageCount']=ProviderToImage::where('user_id', Auth::user()->id)->count();
        $data['states'] = State::where('country_id',auth()->user()->country)->get();
        $data['cites'] = City::where('state_id',auth()->user()->state)->get();
        // $data['allSubCategory'] = SubCategory::where('category_id',auth()->user()->category_id)->get();
        $data['myCategory']=ProToCategory::where('user_id', Auth::user()->id)->where('master_id','=', 0)->first();
        $data['allSubCategory']=[];
        $data['mySubCategory']=ProToCategory::where('user_id', Auth::user()->id)->where('master_id','!=', 0)->get();
        if($data['myCategory']!==null){
            $data['allSubCategory'] = SubCategory::where('category_id',$data['myCategory']->category_id)->where('status','A')->get();
        }


        // return $data['allUserSkill'];

        return view('modules.service_provider.edit_profile')->with($data);
    }
     /**
     *   Method      : editProfileSave
     *   Description : for service provider edit Profile Save
     *   Author      : Soumojit
     *   Date        : 2021-OCT-28
     **/

    public function editProfileSave(Request $request){

        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'address' => 'required',
            'about' => 'required',
            'category' => 'required',
            'budget' => 'required',
            'budget_type' => 'required',
            'experience' => 'required',
            'whatsapp_no' => 'required',
        ]);
        $name = $request->first_name . ' ' . $request->last_name;
        $upd = [];
        $upd['name'] = @$name;
        $upd['city'] = @$request->city;
        $upd['state'] = @$request->state;
        $upd['country'] = @$request->country;
        $upd['address'] = @$request->address;
        $upd['about'] = @$request->about;
        $upd['website'] = @$request->website;
        $upd['whatsapp_no'] = @$request->whatsapp_no;
        // $upd['category_id'] = @$request->category;
        $upd['budget'] = @$request->budget;
        $upd['budget_type'] = @$request->budget_type;
        $upd['experience'] = @$request->experience;
         $slug = str_slug($name, "-");
        $cityname = City::where('id',$request->city)->value('name');
        $new_slug = $slug."-".$cityname;
        $check_slug = User::where('id','!=',Auth::user()->id)->where('slug',$new_slug)->where('status','!=','D')->first();
        if($check_slug == null){

             $upd['slug'] = $new_slug;
        }else{

             $upd['slug'] = $slug."-".Auth::user()->id."-".$cityname;
        }
        //$upd['slug'] = $slug . "-" . Auth::user()->id;
        if (@$request->profile_picture) {
            // $image = $request->profile_pic;
            // $filename = time() . '-' . rand(1000, 9999) . '.' . $image->getClientOriginalExtension();
            // Storage::putFileAs('public/profile_picture', $image, $filename);
            // $upd['profile_img'] = $filename;
            // @unlink(storage_path('app/public/profile_picture/' . auth()->user()->profile_img));
            @unlink(storage_path('app/public/profile_picture/' . auth()->user()->profile_pic));
            $destinationPath = "storage/app/public/profile_picture/";
            $img1 = str_replace('data:image/png;base64,', '', @$request->profile_picture);
            $img1 = str_replace(' ', '+', $img1);
            $image_base64 = base64_decode($img1);
            $img = time() . '-' . rand(1000, 9999) . '.png';
            $file = $destinationPath . $img;
            file_put_contents($file, $image_base64);
            chmod($file, 0755);
            $upd['profile_pic'] = $img;
        }
        if (@$request->gov_id) {
            $image = $request->gov_id;
            $filename = time() . '-' . rand(1000, 9999) . '.' . $image->getClientOriginalExtension();
            Storage::putFileAs('public/gov_id_image', $image, $filename);
            $upd['gov_id_image'] = $filename;
            @unlink(storage_path('app/public/gov_id_image/' . auth()->user()->gov_id_image));
        }
        if (@$request->certificate) {
            $image = $request->certificate;
            $filename = time() . '-' . rand(1000, 9999) . '.' . $image->getClientOriginalExtension();
            Storage::putFileAs('public/certificate_image', $image, $filename);
            $upd['certificate_image'] = $filename;
            @unlink(storage_path('app/public/certificate_image/' . auth()->user()->certificate_image));
        }
        if (@$request->old_password || @$request->new_password || @$request->new_password) {
            $request->validate([
                'old_password' => 'required',
                'new_password' => 'required|confirmed',
                'new_password_confirmation' => 'required',
            ]);
            $check_user = User::where('id', auth()->user()->id)->first();
            if (!Hash::check($request->old_password, $check_user->password)) {
                session()->flash('error', 'Old password wrong');
                return redirect()->back();
            } else {
                $upd['password'] = \Hash::make($request->new_password);
            }
        }



        $skill = explode(",", @$request->skill);
        // return $skill;
        foreach ($skill as $item) {
            $insSkill = [];
            $insSkill['user_id'] = Auth::user()->id;
            $insSkill['skills_id'] = $item;
            if(@$item){
                $checkAvailable =  ProviderToSkills::where('user_id', Auth::user()->id)->where('skills_id', $item)->first();
                if ($checkAvailable == null) {
                    ProviderToSkills::create($insSkill);
                }
            }

        }
        ProviderToSkills::where('user_id', Auth::user()->id)->whereNotIn('skills_id', $skill)->delete();

        $language= explode(",", @$request->language);
        // return $language;
        foreach ($language as $item1) {
            $insLanguage = [];
            $insLanguage['user_id'] = Auth::user()->id;
            $insLanguage['language_id'] = $item1;
            if(@$item1){
                $checkAvailable =  ProviderToLanguage::where('user_id', Auth::user()->id)->where('language_id', $item1)->first();
                if ($checkAvailable == null) {
                    ProviderToLanguage::create($insLanguage);
                }
            }

        }
        ProviderToLanguage::where('user_id', Auth::user()->id)->whereNotIn('language_id', $language)->delete();

        $insCat=[];
        $insCat['category_id']=$request->category;
        $insCat['master_id'] = 0;
        $insCat['user_id'] = Auth::user()->id;
        ProToCategory::where('user_id', Auth::user()->id)->where('master_id', 0)->delete();
        ProToCategory::create($insCat);

        $sub_category = explode(",", @$request->sub_category);
        // return $sub_category;
        foreach ($sub_category as $item2) {
            $insSubCategory = [];
            $insSubCategory['user_id'] = Auth::user()->id;
            $insSubCategory['category_id'] = $item2;
            $insSubCategory['master_id'] = $request->category;
            if(@$item1){
                $checkAvailable =  ProToCategory::where('user_id', Auth::user()->id)->where('category_id', $item2)->where('master_id','!=', 0)->first();
                if ($checkAvailable == null) {
                    ProToCategory::create($insSubCategory);
                }
            }

        }
        ProToCategory::where('user_id', Auth::user()->id)->where('master_id','!=', 0)->whereNotIn('category_id', $sub_category)->delete();


        $user = User::where('id', Auth::user()->id)->update($upd);

        $userData =User::where('id', Auth::user()->id)->first();
        $userLanguage =ProviderToLanguage::where('user_id', Auth::user()->id)->count();
        $userDataSkills =ProviderToSkills::where('user_id', Auth::user()->id)->count();
        $userDataImage =ProviderToImage::where('user_id', Auth::user()->id)->count();
        if($userData->approval_status==null){
            if($userData->profile_pic!=null && $userLanguage>0 && $userDataSkills>0 && $userDataImage>0){
                User::where('id', Auth::user()->id)->update(['approval_status'=>'N']);
            }
        }
        if( @$user ){
            session()->flash('success', 'Profile updated successfully');
            return redirect()->route('service.provider.profile');
        }
        session()->flash('error', 'Profile not updated');
        return redirect()->route('service.provider.profile');

    }
    /**
     *   Method      : editWorkImage
     *   Description : for service provider edit Profile Save
     *   Author      : Soumojit
     *   Date        : 2021-OCT-29
     **/

    public function editWorkImage(){

        $data['allWorkImage']=ProviderToImage::where('user_id', Auth::user()->id)->get();
        $data['allWorkImageCount']=ProviderToImage::where('user_id', Auth::user()->id)->count();

        return view('modules.service_provider.work_image')->with($data);
    }
    /**
     *   Method      : editWorkImageSave
     *   Description : for service provider edit Profile Save
     *   Author      : Soumojit
     *   Date        : 2021-OCT-29
     **/

    public function editWorkImageSave(Request $request){
        $ins=[];
        if (@$request->work_image) {
            // @unlink(storage_path('app/public/work_image/' . auth()->user()->profile_pic));
            $destinationPath = "storage/app/public/work_image/";
            $img1 = str_replace('data:image/png;base64,', '', @$request->work_image);
            $img1 = str_replace(' ', '+', $img1);
            $image_base64 = base64_decode($img1);
            $img = time() . '-' . rand(1000, 9999) . '.png';
            $file = $destinationPath . $img;
            file_put_contents($file, $image_base64);
            chmod($file, 0755);
            $ins['image'] = $img;
            $ins['user_id'] = Auth::user()->id;
        }
        $create = ProviderToImage::create($ins);

        $userData =User::where('id', Auth::user()->id)->first();
        $userLanguage =ProviderToLanguage::where('user_id', Auth::user()->id)->count();
        $userDataSkills =ProviderToSkills::where('user_id', Auth::user()->id)->count();
        $userDataImage =ProviderToImage::where('user_id', Auth::user()->id)->count();
        if($userData->approval_status==null){
            if($userData->profile_pic!=null && $userLanguage>0 && $userDataSkills>0 && $userDataImage>0){
                User::where('id', Auth::user()->id)->update(['approval_status'=>'N']);
            }
        }
        if( @$create ){
            session()->flash('success', ' Work image upload successfully');
            return redirect()->route('service.provider.work.image');
        }
        session()->flash('error', 'Work image not upload');
        return redirect()->route('service.provider.work.image');
    }

    /**
     *   Method      : removeWorkImage
     *   Description : for service provider edit Profile Save
     *   Author      : Soumojit
     *   Date        : 2021-OCT-29
     **/

    public function removeWorkImage($id=null){
        $data= ProviderToImage::where('id',$id)->where('user_id', Auth::user()->id)->first();
        $userDataImage =ProviderToImage::where('user_id', Auth::user()->id)->count();
        if($userDataImage==1){
            session()->flash('error', 'Image not removed minimum one image image require');
            return redirect()->route('service.provider.work.image');
        }
        if(@$data){
            @unlink(storage_path('app/public/work_image/' .$data->image));
            ProviderToImage::where('id', $data->id)->delete();
            session()->flash('success', ' Image removed');
            return redirect()->route('service.provider.work.image');
        }
        session()->flash('error', 'Image not removed');
        return redirect()->route('service.provider.work.image');
    }
    /**
     *   Method      : changeEmail
     *   Description : for service provider Email change
     *   Author      : Soumojit
     *   Date        : 2021-NOV-10
     **/

    public function changeEmail(Request $request){
        $request->validate([
            'email' => 'required',
        ]);

        $upd = [];
        $upd['temp_email'] = @$request->email;
        if(auth()->user()->email_mob_change==null){
            $upd['email_mob_change']='E';
        }
        if(auth()->user()->email_mob_change=='M'){
            $upd['email_mob_change']='B';
        }
        if(auth()->user()->email_mob_change=='E'){
            $upd['email_mob_change']='E';
        }
        if(auth()->user()->email_mob_change=='B'){
            $upd['email_mob_change']='B';
        }
        User::where('id', Auth::user()->id)->update($upd);
        // $user = User::where('id', Auth::user()->id)->first();
        // $send_email_approval = $this->sendEmailApproval(@$user->mobile_number,@$user->id);
        session()->flash('success', 'Email saved waiting for admin approval');
        return redirect()->route('service.provider.profile');
    }
    /**
     *   Method      : changeMobile
     *   Description : for service provider Mobile change
     *   Author      : Soumojit
     *   Date        : 2021-NOV-10
     **/

    public function changeMobile(Request $request){
        $request->validate([
            'mobile' => 'required',
        ]);

        $upd = [];
        $upd['temp_mobile'] = @$request->mobile;
        if(auth()->user()->email_mob_change==null){
            $upd['email_mob_change']='M';
        }
        if(auth()->user()->email_mob_change=='E'){
            $upd['email_mob_change']='B';
        }
        if(auth()->user()->email_mob_change=='M'){
            $upd['email_mob_change']='M';
        }
        if(auth()->user()->email_mob_change=='B'){
            $upd['email_mob_change']='B';
        }
        User::where('id', Auth::user()->id)->update($upd);
        session()->flash('success', 'Mobile no saved waiting for admin approval');
        return redirect()->route('service.provider.profile');
    }

    /**
     *   Method      : saveQuotes
     *   Description : for service provider save quotes
     *   Author      : Puja
     *   Date        : 2021-DEC-14
     **/

    public function saveQuotes(Request $request){
        
        $chk = Quotes::where('user_id',@$request->user_id)->where('post_id',@$request->id)->first();
        $post = PostJob::where('id',@$request->id)->first();

        if(@$chk){
            if(@$request->quote_id){

                
                $ins['cost'] = @$request->cost;
                $ins['timeline'] =  @$request->timeline;
                $ins['quote_description'] = @$request->description;
                $result = Quotes::where('id',@$request->quote_id)->update($ins);
                if(@$result){
                    session()->flash('success', 'Quotation updated successfully');
                    return redirect()->back();
                }else{
                    session()->flash('error', 'Quotation not updated');
                    return redirect()->back();
                }

            }
            

        }else{
            $ins['user_id'] = @$request->user_id;
            $ins['post_id'] = @$request->id;
            $ins['cost'] = @$request->cost;
            $ins['timeline'] =  @$request->timeline;
            $ins['quote_description'] = @$request->description;
            $result = Quotes::insert($ins);

            $post->tot_bids = $post->tot_bids +  1;
            $total = $post->tot_bids;
            $post->update(['tot_bids' => $total ]);
            if(@$result){
                $job = PostJob::where('id',@$ins['post_id'])->first();
                $pro = User::where('id',Auth::user()->id)->first();
                $user = User::where('id',@$job->user_id)->first();
                $send_quote_sms = $this->sendQuoteSms($user->mobile_number,$pro->name,$job->id);
                $data['user_name'] = $user->name;
                $data['bidder_name'] = Auth::user()->name;
                $data['job_name'] = $job->job_name;
                $data['email'] = $user->email;
                $data['bidder_email'] = Auth::user()->email;
                Mail::send(new QuotesCreateMail($data));
                Mail::send(new QuotesPostedMail($data));
                
                session()->flash('success', 'Quotation placed successfully');
                return redirect()->back();
            }else{
                session()->flash('error', 'Quotation not placed');
                return redirect()->back();
            }
        }
    }

    /**
     *   Method      : myQuotes
     *   Description : for service provider showing quotes
     *   Author      : Puja
     *   Date        : 2021-DEC-21
     **/
    public function myQuotes(Request $request){

        $data['open'] = $this->getBids($request);
        $data['awarded'] = $this->getBids($request);
        $data['closed'] = $this->getBids($request);
        $data['all'] = $this->getBids($request);
        $data['open'] = $data['open']->where('status', 'Bid Placed')->paginate(10,['*'],'page1');
        $data['awarded'] = $data['awarded']->where('status', 'Awarded')->paginate(10,['*'],'page2');
        $data['closed'] = $data['closed']->where('status','Closed')->paginate(10,['*'],'page3');
        $data['all'] = $data['all']->paginate(10,['*'],'page4');

        if(@$request->all()){
            $data['key'] = @$request->all();
        }
        
        return view('modules.service_provider.my_quotes')->with($data);
    }

    /**
     *   Method      : deleteQuotes
     *   Description : for service provider delete quotes
     *   Author      : Puja
     *   Date        : 2021-DEC-21
     **/

    public function deleteQuotes($id=null){
    $check= Quotes::where('id',$id)->first();
    if($check==null){
        session()->flash('error','Something went wrong');
        return redirect()->route('service.provider.my.quotes');
    }
    $result = $check->delete();
     
        if($result){
            session()->flash('success','Quote deleted successfully');
            return redirect()->route('service.provider.my.quotes');
        }
    
    }

    /**
     *   Method      : getMembership
     *   Description : membership for service provider.
     *   Author      : Puja
     *   Date        : 2021-DEC-22
     **/

    public function getMembership(){

        $data['free'] = ProPackage::where('package_name','BRONZE')->where('status','A')->first();
        $data['premium'] = ProPackage::where('package_name','PREMIUM')->where('status','A')->first();
        $data['pro_package'] = ProPackage::where('status','A')->get();
        $data['package'] = UserToPackage::where('user_id',Auth::user()->id)->first();
        $data['user'] = User::where('id',Auth::user()->id)->first();
        return view('modules.service_provider.membership')->with($data);

    }

    /**
     *   Method      : updatePackage
     *   Description : update Package for service provider.
     *   Author      : Puja
     *   Date        : 2021-DEC-22
     **/

    public function updatePackage(Request $request){
        $data = ProPackage::where('id',$request->id)->first();
        $update['user_id'] = Auth::user()->id;
        $update['pro_package_id'] = @$data->id;
        $update1['payment_freq'] = @$data->type;
        if(@$data->type == 'M'){
            $NewDate=Date('Y-m-d', strtotime('+30 days'));
            $update1['package_expiry_date'] = $NewDate;
        }else{
            $NewDate=Date('Y-m-d', strtotime('+365 days'));
            $update1['package_expiry_date'] = $NewDate; 
        }
        $chk = UserToPackage::where('user_id',Auth::user()->id)->first();
        if($chk){
            
            $result = UserToPackage::where('id',@$chk->id)->update($update);
            User::where('id',Auth::user()->id)->update($update1);
        
        }else{
            
            $result = UserToPackage::insert($update);
            User::where('id',Auth::user()->id)->update($update1); 
        }
        $response=array();
        if(@$result){
            $response['status']='success';
        }
        return response()->json($response);
    }

    /**
     *   Method      : viewQuote
     *   Description : view Quote for service provider.
     *   Author      : Puja
     *   Date        : 2021-DEC-22
     **/ 
    public function viewQuote($id){
        
        $data['quote'] = Quotes::with('jobDetails')->where('id',$id)->first();
        $data['milestones'] = Milestone::where('job_id',$data['quote']->post_id)->get();
        $data['chat'] = MessageMaster::where('bid_id',@$data['quote']->id)->where('provider_id',Auth::user()->id)->first();
        return view('modules.service_provider.view_quotes')->with($data);

    }


    public function getBids($request){
        $bids = Quotes::with('jobDetails')->where('user_id',Auth::user()->id)->orderBy('created_at','DESC');
            if(@$request->keyword){
                $bids =  $bids->whereHas('jobDetails',function($q) use($request){
                    $q->where('job_name','like','%'.$request->keyword.'%');
                });
            }
            if(@$request->date){
               
                $bids =  $bids->whereHas('jobDetails',function($q1) use($request){
                    $q1->where('job_start_date',date('Y-m-d H:i:s',strtotime($request->date)));
                });
            }
            if(@$request->status){
               
                $bids =  $bids->where('status',@$request->status);
            }
            // $data['key'] = @$request->all();
            return $bids;
    }

        
    

    public function proDeleteJob($id=null){

        $check= Quotes::where('id',$id)->first();
        if($check==null){
            session()->flash('error','Something went wrong');
            return redirect()->back();
        }
        $result = $check->delete();
         
            if($result){
                session()->flash('success','Job deleted successfully');
                return redirect()->back();
            }

    }

    public function proRatingReview(){

        $data['allReviews'] = JobToProReview::where('provider_id',Auth::user()->id)->paginate(10);
        $data['proDetails'] = User::where('id',Auth::user()->id)->first();
      
       // return $data['allReviews'];
        return view('modules.service_provider.review_rating')->with($data);
    }
    
    public function sendQuoteSms($number,$pro_name,$job_id){
        $apiKey = urlencode('NDEzODdhNjM0YTc3NDk2OTQ5MzI3OTY5NmI0YzM4NmY=');
        $num = '91'.$number;
        $numbers = urlencode($num); 
        $sender = urlencode('ITREAL');
        // $message = 'Use '.$otp.' OTP to verify your mobile number.';
        $message = rawurlencode(@$pro_name.' has sent a quote on your Job id '.@$job_id.'.
RiVirtual - iTester team');
          
        $data = 'apikey=' . $apiKey . '&numbers=' . $numbers . "&sender=" . $sender . "&message=" . $message;
        
        $ch = curl_init('https://api.textlocal.in/send/?' . $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        
        return $response;
    }

    public function sendEmailApproval($number,$user_id){
        $apiKey = urlencode('NDEzODdhNjM0YTc3NDk2OTQ5MzI3OTY5NmI0YzM4NmY=');
        $num = '91'.$number;
        $numbers = urlencode($num); 
        $sender = urlencode('ITREAL');
        // $message = 'Use '.$otp.' OTP to verify your mobile number.';
        $message = rawurlencode('Your email has been updated by Admin on your request for user id '.@$user_id.'.
RIVirtual from iTester');
          
        $data = 'apikey=' . $apiKey . '&numbers=' . $numbers . "&sender=" . $sender . "&message=" . $message;
        
        $ch = curl_init('https://api.textlocal.in/send/?' . $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        
        return $response;


}
}
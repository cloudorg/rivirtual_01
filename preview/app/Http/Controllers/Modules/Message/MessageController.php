<?php

namespace App\Http\Controllers\Modules\Message;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Mail;
use App\Country;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Storage;
use App\Models\AgentToAvailability;
use App\Models\Language;
use App\Models\ProviderToLanguage;
use App\Models\City;
use App\Models\State;
use App\Models\PropertyVisitRequest;
use App\Mail\AgentRescheduleMail;
use Pusher\Pusher;
use App\Models\MessageMaster;
use App\Models\MessageDetail;
use App\Models\Property;
use DB;
use App\Models\Quotes;
use App\Models\PostJob;


class MessageController extends Controller{

     public function __construct()
    {
        $this->middleware('auth');
    }

    // public function showMessage(Request $request){
    //     $userId = auth()->user()->id;
    //     // DB::connection()->enableQueryLog();

    //     $m_id=MessageMaster::with('getMessageDetailLastMessage')->where(function($q2) use($userId) {
    //         $q2->where('seeker_id',$userId)
    //         ->orWhere('provider_id',$userId);
    //    })->pluck('id');
    //     $msgmaster=MessageMaster::with('getMessageDetailLastMessage')->where(function($q1) use($userId) {
    //          $q1->where('seeker_id',$userId)
    //         ->orWhere('provider_id',$userId);
    //     })->whereNotIn('id',$m_id);
    //     $msgmaster1=$msgmaster->withCount('getUnreadMessage')->get();
    //     $msgmaster2=$msgmaster1->sortByDesc(function($msgmaster, $key) {
    //         return $msgmaster['getMessageDetailLastMessage']['id'];
    //     })->all();
    //     // $queries = DB::getQueryLog();
    //     $data['message']=[];  
    //     foreach($msgmaster2 as $res){
    //         $data['message'][]=$res;
    //     }
    //     $data['messageDetail']=MessageDetail::whereIn('message_master_id',@$m_id)->get();

    //    // $data['messageDetail']=MessageDetail::where('message_master_id',@$data['message'][0]->id)->get();
        
    //     //return $data['messageDetail'];
    //     $data['users'] = MessageMaster::with('getSeeker')->where('provider_id',Auth::user()->id)->where('seeker_id','!=',Auth::user()->id);
    //     if($request->all()){
    //         $data['users'] = $data['users']->whereHas('getSeeker' ,function($q) use($request){

    //               $q->where('name','like','%'.$request->search.'%');
    //         });

    //    // $data['users'] = $data['users']->where('name','like','%'.$request->search.'%')->orWhere('description','like','%'.$request->search.'%');
    //     }
    //     $data['users'] = $data['users']->get();
        
    //     foreach($data['message'] as $msg){
    //         if(Auth::user()->id==$msg->seeker_id){
    //             $user=$msg->getProvider;

    //         }else{
    //             $user=$msg->getSeeker;
    //         }
    //         if(!Cache::has('user-activity-status-' . $user->id)){
    //             User::where('id',$user->id)->where('online_status','Y')->update(['online_status' => 'N']);
    //         }
    //     }
        
       
    //     return view('modules.message_box.message')->with($data);
    // }

    public function showMessage(Request $request){
        $userId = auth()->user()->id;
        // DB::connection()->enableQueryLog();
        $m_id=MessageMaster::with('getMessageDetailLastMessage')->where(function($q2) use($userId) {
            $q2->where('seeker_id',$userId)
            ->orWhere('provider_id',$userId);
       })->pluck('id');
        $msgmaster=MessageMaster::with('getMessageDetailLastMessage')->where(function($q1) use($userId) {
             $q1->where('seeker_id',$userId)
            ->orWhere('provider_id',$userId);
        })->get();
        
        // $msgmaster1=$msgmaster->withCount('getUnreadMessage')->get();
        // $msgmaster2=$msgmaster1->sortByDesc(function($msgmaster, $key) {
            // return $msgmaster['getMessageDetailLastMessage']['id'];
        // })->all();
        // $queries = DB::getQueryLog();
        // dd($msgmaster2);
        $data['message']=[];  
        foreach($msgmaster as $res){
            $data['message'][]=$res;
        }
        $data['messageDetail']=MessageDetail::where('message_master_id',@$data['message'][0]->id)->get();
        $data['users'] = User::where('id','!=',Auth::user()->id);
        
        if($request->all()){
        $data['users'] = $data['users']->where('fname','like','%'.$request->search.'%')->orWhere('lname','like','%'.$request->search.'%')->orWhere('description','like','%'.$request->search.'%');
        }
        $data['users'] = $data['users']->get();
        
        foreach($data['message'] as $msg){
            if(Auth::user()->id==$msg->seeker_id){
                $user=$msg->getProvider;

            }else{
                $user=$msg->getSeeker;
            }
            // if(!Cache::has('user-activity-status-' . $user->id)){
            //     User::where('id',$user->id)->where('online_status','Y')->update(['online_status' => 'N']);
            // }
        }
        
        
        return view('modules.message_box.message')->with($data);
    }
    
    public function searchAjax(Request $request){
        $response = [
            'jsonrpc' => '2.0'
        ];
        $userId = auth()->user()->id;
        $m_id=MessageMaster::with('getMessageDetailLastMessage')->where(function($q2) use($userId) {
                $q2->where('seeker_id',$userId)
                ->orWhere('provider_id',$userId);
        })->pluck('id');
        if(@$request->data['type']){
            $msgmaster=MessageMaster::with('getMessageDetailLastMessage','getSeeker:id,fname,lname,slug,image,online_status','getProvider:id,fname,lname,slug,image,online_status','getService:id,request_title,slug,user_id')->withCount('getUnreadMessage')->where(function($q1) use($userId) {
                $q1->where('seeker_id',$userId)
               ->orWhere('provider_id',$userId);
           })->whereIn('id',$m_id);
        }else{
            $msgmaster=MessageMaster::with('getMessageDetailLastMessage','getSeeker:id,fname,lname,slug,image,online_status','getProvider:id,fname,lname,slug,image,online_status','getService:id,request_title,slug,user_id')->withCount('getUnreadMessage')->where(function($q1) use($userId) {
                $q1->where('seeker_id',$userId)
               ->orWhere('provider_id',$userId);
           })->whereNotIn('id',$m_id);
        }
        // dd($request->data);
        if(@$request->data['keyword']){
            // DB::connection()->enableQueryLog();
            $user_id=User::where(DB::raw('concat(fname, " ", lname)'),'LIKE','%'.$request->data['keyword'].'%')->where('id','!=',Auth::id())->pluck('id');
            // $queries = DB::getQueryLog();
            // dd($queries);
            $m_id=MessageMaster::with('getMessageDetailLastMessage')->where(function($q2) use($user_id) {
                $q2->whereIn('seeker_id',$user_id)
                ->orWhereIn('provider_id',$user_id);
            })->pluck('id');
            $service_id=UserToPostedServiceRequest::where('request_title', 'like', '%' . $request->data['keyword'] . '%')->pluck('id');
            $msgmaster=$msgmaster->where(function($q) use ($service_id,$m_id){
                $q->whereIn('service_id',$service_id);
                $q->orwhereIn('id',$m_id);
            });
            // dd($user_id);
        }
        $msgmaster1=$msgmaster->get();
        $msgmaster2=$msgmaster1->sortByDesc(function($msgmaster, $key) {
            return $msgmaster['getMessageDetailLastMessage']['id'];
        })->all();
        $response['message']=[];  
        foreach($msgmaster2 as $res){
            $response['message'][]=$res;
        }      
        $response['msgcnt']=MessageDetail::where('is_read','N')->where('user_id','!=',@Auth::user()->id)->whereHas('getMessageMaster',function($q1) use($userId){
            $q1->where('seeker_id',$userId)
            ->orWhere('provider_id',$userId);    
        })->count(); 
        if(!@$response['msgcnt']){
            $response['msgcnt']='0';
        }
        return response()->json($response);
    }
    
    public function sendAjax(Request $request)
    {   
       
        $pusher = new Pusher(config("services.pusher.pusher_app_key"),config("services.pusher.pusher_app_secret"),config("services.pusher.pusher_app_id"), [
            'cluster' => config("services.pusher.pusher_app_cluster")
        ]);
        
        $userId = auth()->user()->id;
        $user=User::find($userId);
        $user_type = $user->user_type;
              
        if(@$request->message){

            $hv_https = stripos(@$request->message,'https://');
            $hv_http = stripos(@$request->message,'http://');
           
            if(@$hv_http != 0){
                $d = substr(@$request->message,$hv_http);
                $a = strpos($d,' ');
                if(@$a){

                    $hrf = substr(@$request->message,$hv_http,$a);
                    $a1 = "<a href=";
                    $a2 = ">";
                    $a3 = "</a>";
                    $ancr = str_replace($hrf,$a1.$hrf.$a2.$hrf.$a3,@$request->message);

                    $request->message = $ancr;

                }else{
                    $a1 = "<a href=";
                    $a2 = ">";
                    $a3 = "</a>";
                    $ancr = str_replace($d,$a1.$d.$a2.$d.$a3,@$request->message);
                    

                    $request->message = $ancr;
                }
                
            }else if(@$hv_https != 0){
                $d = substr(@$request->message,$hv_https);
                $a = strpos($d,' ');
                
                if(@$a){

                    $hrf = substr(@$request->message,$hv_https,$a);
                    $a1 = "<a href=";
                    $a2 = ">";
                    $a3 = "</a>";
                    $ancr = str_replace($hrf,$a1.$hrf.$a2.$hrf.$a3,@$request->message);
                   
                    $request->message = $ancr;

                }else{
                    $a1 = "<a href=";
                    $a2 = ">";
                    $a3 = "</a>";
                    $ancr = str_replace($d,$a1.$d.$a2.$d.$a3,@$request->message);
                    
                    
                    $request->message = $ancr;
                }
                
            }else if(@$hv_http == 0){
                
                if(str_contains(@$request->message,'https://') == true || str_contains(@$request->message,'http://') == true){
                    
                        $d = substr(@$request->message,$hv_http);
                
                        $a = strpos($d,' ');
                
                    if($a){
                        $hrf = substr(@$request->message,$hv_https,$a);
                        $a1 = "<a href=";
                        $a2 = ">";
                        $a3 = "</a>";
                        $ancr = str_replace($hrf,$a1.$hrf.$a2.$hrf.$a3,@$request->message);
                        $request->message = $ancr;
                         
                    }else{

                        $a1 = "<a href=";
                        $a2 = ">";
                        $a3 = "</a>";
                        $ancr = str_replace($d,$a1.$d.$a2.$d.$a3,@$request->message);
                        $request->message = $ancr;
                    
                    }   
                }
                
               
                
            }

        }
            
        if(@$request->u_type == 'P'){

            if(@$request->message_master_id){

                $msgMstrID=$request->message_master_id;
            }
            if(@$request->message_master_id == 0){

                        if(@$request->property_id == 0 && @$request->bid_id == 0){

                            if(@$user_type == 'U'){
                                    $msgmaster=MessageMaster::where('provider_id',@$request->provider_id)->where('seeker_id',$userId)->where('property_id',0)->where('bid_id',0)->first();
                            }
                    
                            if(@$msgmaster){
                                $msgMstrID=$msgmaster->id;
                            }else{
                                $property=User::where('id',@$request->provider_id)->first();
                                $msgMstrID= MessageMaster::insertGetId([
                                    'property_id'=>0,
                                    'seeker_id'=>@$userId,
                                    'provider_id'=>@$property->id,
                                    // 'bid_id' => @$request->bid_id,
                                    'bid_id' => 1,
                                ]);
                            }
                        }
                        elseif(@$request->property_id == 0 && @$request->bid_id != 0){
                
                if(@$user_type == 'U'){
                    $msgmaster=MessageMaster::where('provider_id',@$request->provider_id)->where('seeker_id',$userId)->where('property_id',0)->where('bid_id',@$request->bid_id)->first();
                }
                
                if(@$msgmaster){
                    $msgMstrID=$msgmaster->id;
                }else{
                    $property=User::where('id',@$request->provider_id)->first();
                    $msgMstrID= MessageMaster::insertGetId([
                        'property_id'=>0,
                        'seeker_id'=>@$userId,
                        'provider_id'=>@$property->id,
                        // 'bid_id' => @$request->bid_id,
                        'bid_id' => 2,
                    ]);
                }
                    }

            }
            
            if(@$request->file){
                $file = $request->file;
                $filename = time() . '-' . rand(1000, 9999) .'_'. $file->getClientOriginalName();
                Storage::putFileAs('public/message_files', $file, $filename);
            }
            $msgmaster=MessageMaster::where('id',$msgMstrID)->first();

            // MessageMaster::where('id',$msgMstrID)->update(['archive_seeker_id'=>0,'archive_provider_id'=>0]);
            if($msgmaster->seeker_id==$userId){
                $reciver_id=$msgmaster->provider_id;
            }else{
                $reciver_id=$msgmaster->seeker_id;
            }
            MessageDetail::where(['message_master_id'=>@$msgmaster->id])->where('user_id','!=',@Auth::id())->update(['is_read'=>'Y']);  

            $response = $pusher->trigger('rivirtual', 'receive-event', [
                'message_master_id'   => @$msgmaster->id,
                'message'       =>  @$request->message ? $request->message : ' ',
                'title'         => @$msgmaster->getProvider->name,
                's_slug'          =>  @$msgmaster->getProvider->slug,
                'file'          =>  @$filename,
                'reciver_id'    =>  @$reciver_id,
                'sender_name'   =>  @$user->name,
                'sender_type'   =>  @$user->user_type,
                'sender_slug'   =>  @$user->slug,
                'online_status' =>  @$user->online_status,
                'sender_image'  =>  @$user->profile_pic ? url('storage/app/public/profile_picture/'.@$user->profile_pic) : url('public/frontend/images/blank.png'),
                'socket_id' => $request->socket_id,
            ]);

                
       
                $createData['message_master_id']  =  @$msgMstrID;
                $createData['property_id']         =  @$msgmaster->property_id ;
                $createData['user_id']            =  $userId;
                $createData['reciver_id']         =   @$reciver_id;
                $createData['message']            =  @$request->message ? $request->message : ' ';
                $createData['file']               =  @$filename;
                $msg_create = MessageDetail::create($createData);
                $res = $pusher->trigger('rivirtual', 'receive-event-app', 
                MessageDetail::with('getUser','getMessageMaster')->where('id',@$msg_create->id)->first(),[
                    'socket_id' => $request->socket_id
                ]);
               
                if ($response) {  
                    if(@$request->property_id || @$request->property_id == '0'){              
                        $user=User::find(@$reciver_id);
                        return response()->json([
                            'message_master_id' => @$msgMstrID,
                            'online_status' =>  @$user->online_status,
                            'user_type'     =>  @$request->u_type,
                            'bid_id'        =>  @$request->bid_id,
                            'title'=> @$msgmaster->getProvider->name,
                            's_slug' =>  @$msgmaster->getProvider->slug,
                            'u_name'    =>  @$user->name,
                            'u_slug'    =>  @$user->slug,
                            'u_image'   =>  @$user->image ? url('storage/app/public/profile_picture/'.@$user->image) : url('public/images/blank.png'), 
                        ]);
                    }
                    
                    return response()->json([
                        'result' => [
                            'code' => 200,
                            'file'=>@$filename,
                            'message'=>@$request->message ? $request->message : '',
                        ]
                    ]);
                } else {
                    return response()->json([
                        'error' => [
                            'message' => 'Cound\'nt send message.'
                        ]
                    ]);
                }

        }else{

            
            
            if(@$request->m_type == "PM"){


                   if(@$request->message_master_id){
                        $msgMstrID=$request->message_master_id;
                    }
                    if(@$request->property_id){
                        if(@$user_type == 'U'){
                            $msgmaster=MessageMaster::where('provider_id',@$request->property_id)->where('seeker_id',$userId)->where('property_id',0)->first();
                        }
                        
                        if(@$msgmaster){
                            $msgMstrID=$msgmaster->id;
                        }else{
                            $property=User::where('id',@$request->property_id)->first();
                            $msgMstrID= MessageMaster::insertGetId([
                                'property_id'=>0,
                                'seeker_id'=>@$userId,
                                'provider_id'=>@$property->id,
                                'bid_id' => 3,
                            ]);
                        }
                    }
                    if(@$request->file){
                        $file = $request->file;
                        $filename = time() . '-' . rand(1000, 9999) .'_'. $file->getClientOriginalName();
                        Storage::putFileAs('public/message_files', $file, $filename);
                    }
                    $msgmaster=MessageMaster::where('id',$msgMstrID)->first();
                    // MessageMaster::where('id',$msgMstrID)->update(['archive_seeker_id'=>0,'archive_provider_id'=>0]);
                    if($msgmaster->seeker_id==$userId){
                        $reciver_id=$msgmaster->provider_id;
                    }else{
                        $reciver_id=$msgmaster->seeker_id;
                    }
                    MessageDetail::where(['message_master_id'=>@$msgmaster->id])->where('user_id','!=',@Auth::id())->update(['is_read'=>'Y']);   
                    $response = $pusher->trigger('rivirtual', 'receive-event', [
                        'message_master_id'   => @$msgmaster->id,
                        'message'       =>  @$request->message ? $request->message : ' ',
                        'title'         => @$msgmaster->getProvider->name,
                        's_slug'          =>  @$msgmaster->getProvider->slug,
                        'file'          =>  @$filename,
                        'reciver_id'    =>  @$reciver_id,
                        'sender_name'   =>  @$user->name,
                        'sender_type'   =>  @$user->user_type,
                        'sender_slug'   =>  @$user->slug,
                        'online_status' =>  @$user->online_status,
                        'sender_image'  =>  @$user->profile_pic ? url('storage/app/public/profile_picture/'.@$user->profile_pic) : url('public/frontend/images/blank.png'),
                        'socket_id' => $request->socket_id,
                    ]);
               
                        $createData['message_master_id']  =  @$msgMstrID;
                        $createData['property_id']         =  @$msgmaster->property_id ;
                        $createData['user_id']            =  $userId;
                        $createData['reciver_id']         =   @$reciver_id;
                        $createData['message']            =  @$request->message ? $request->message : ' ';
                        $createData['file']               =  @$filename;
                        $msg_create = MessageDetail::create($createData);
                        $res = $pusher->trigger('rivirtual', 'receive-event-app', 
                        MessageDetail::with('getUser','getMessageMaster')->where('id',@$msg_create->id)->first(),[
                            'socket_id' => $request->socket_id
                        ]);
                        // dd($response);
                        if ($response) {                
                            if(@$request->property_id){
                                $user=User::find(@$reciver_id);
                                return response()->json([
                                    'message_master_id' => @$msgMstrID,
                                    'online_status' =>  @$user->online_status,
                                    'title'=> @$msgmaster->getProvider->name,
                                    's_slug' =>  @$msgmaster->getProvider->slug,
                                    'u_name'    =>  @$user->name,
                                    'u_slug'    =>  @$user->slug,
                                    'u_image'   =>  @$user->image ? url('storage/app/public/profile_picture/'.@$user->image) : url('public/images/blank.png'), 
                                ]);
                            }
                            return response()->json([
                                'result' => [
                                    'code' => 200,
                                    'file'=>@$filename,
                                    'message'=>@$request->message ? $request->message : '',
                                ]
                            ]);
                        } else {
                            return response()->json([
                                'error' => [
                                    'message' => 'Cound\'nt send message.'
                                ]
                            ]);
                        }

            }else{
                    

                   //Property thread mrssage
                   if(@$request->message_master_id){
                        $msgMstrID=$request->message_master_id;
                    }else{

                        if(@$request->property_id == 0 || @$request->property_id != 0 ){ // New changes for single thread
                        if(@$user_type == 'U'){

                            if(@$request->property_id != 0){
                              $msgmaster=MessageMaster::where('property_id',@$request->property_id)->where('seeker_id',$userId)->first();   
                            }else if(@$request->property_id == 0){
                                $msgmaster=MessageMaster::where('seeker_id',$userId)->where('provider_id', @$request->provider_id)->first();

                            }
                        }
                        
                        if(@$msgmaster){
                            $msgMstrID=$msgmaster->id;
                        }else{
                            if(@$request->property_id != 0){

                                $property=Property::where('id',@$request->property_id)->first();
                                $msgMstrID= MessageMaster::insertGetId([
                                'property_id'=>@$property->id,
                                'seeker_id'=>@$userId,
                                'provider_id'=>@$property->user_id,
                            ]);

                            }else{

                                $property=User::where('id',@$request->provider_id)->first();
                                
                                $msgMstrID= MessageMaster::insertGetId([
                                'property_id'=>0,
                                'seeker_id'=>@$userId,
                                'provider_id'=>@$property->id,
                                'bid_id' => 4,
                            ]);

                            }
                            
                        }
                    }
                    }
                    
                    if(@$request->file){
                        $file = $request->file;
                        $filename = time() . '-' . rand(1000, 9999) .'_'. $file->getClientOriginalName();
                        Storage::putFileAs('public/message_files', $file, $filename);
                    }
                    $msgmaster=MessageMaster::where('id',$msgMstrID)->first();
                    // MessageMaster::where('id',$msgMstrID)->update(['archive_seeker_id'=>0,'archive_provider_id'=>0]);
                     
                    if($msgmaster->seeker_id==$userId){
                        $reciver_id=$msgmaster->provider_id;
                    }else{
                        $reciver_id=$msgmaster->seeker_id;
                    }
                    MessageDetail::where(['message_master_id'=>@$msgmaster->id])->where('user_id','!=',@Auth::id())->update(['is_read'=>'Y']);   
                    $response = $pusher->trigger('rivirtual', 'receive-event', [
                        'message_master_id'   => @$msgmaster->id,
                        'message'       =>  @$request->message ? $request->message : ' ',
                        'title'         => @$msgmaster->getProperty->name,
                        's_slug'          =>  @$msgmaster->getProperty->slug,
                        'file'          =>  @$filename,
                        'reciver_id'    =>  @$reciver_id,
                        'sender_name'   =>  @$user->name,
                        'sender_type'   =>  @$user->user_type,
                        'sender_slug'   =>  @$user->slug,
                        'online_status' =>  @$user->online_status,
                        'sender_image'  =>  @$user->profile_pic ? url('storage/app/public/profile_picture/'.@$user->profile_pic) : url('public/frontend/images/blank.png'),
                        'socket_id' => $request->socket_id,
                    ]);
               
                        $createData['message_master_id']  =  @$msgMstrID;
                        $createData['property_id']         =  @$msgmaster->property_id ;
                        $createData['user_id']            =  $userId;
                        $createData['reciver_id']         =   @$reciver_id;
                        $createData['message']            =  @$request->message ? $request->message : ' ';
                        $createData['file']               =  @$filename;
                        $msg_create = MessageDetail::create($createData);
                        $res = $pusher->trigger('rivirtual', 'receive-event-app', 
                        MessageDetail::with('getUser','getMessageMaster')->where('id',@$msg_create->id)->first(),[
                            'socket_id' => $request->socket_id
                        ]);
                        // dd($response);
                        if ($response) {                
                            if(@$request->property_id){
                                $user=User::find(@$reciver_id);
                                return response()->json([
                                    'message_master_id' => @$msgMstrID,
                                    'online_status' =>  @$user->online_status,
                                    'title'=> @$msgmaster->getProperty->name,
                                    's_slug' =>  @$msgmaster->getProperty->slug,
                                    'u_name'    =>  @$user->name,
                                    'u_slug'    =>  @$user->slug,
                                    'u_image'   =>  @$user->image ? url('storage/app/public/profile_picture/'.@$user->image) : url('public/images/blank.png'), 
                                ]);
                            }
                            return response()->json([
                                'result' => [
                                    'code' => 200,
                                    'file'=>@$filename,
                                    'message'=>@$request->message ? $request->message : '',
                                ]
                            ]);
                        } else {
                            return response()->json([
                                'error' => [
                                    'message' => 'Cound\'nt send message.'
                                ]
                            ]);
                        }
                        // End Property thread message
                    }

            }
        
    }
    public function getMessageMaster(Request $request)
    {
        $params = $request->all();
        
        $user_id=auth()->user()->id;
        $response = [
            'jsonrpc' => '2.0'
        ];
        $response['is_block']='';
        $response['type']="";


    
        if(@$request->usertype == 'P'){

            if(@$params['property_id']){
                
                if(@$params['msgtyp'] == 'Pr'){

                    $msgmaster= MessageMaster::where(['provider_id'=>@$params['property_id'],'bid_id' => @$params['bid_id'],'property_id'=> 0])->first();
                }else{
                    // $msgmaster= MessageMaster::where(['provider_id'=>@$params['property_id'],'bid_id'=> 0,'property_id'=>0,'seeker_id' => Auth::user()->id])->first(); 

                    $msgmaster= MessageMaster::where(['provider_id'=>@$params['property_id'],'bid_id'=> 0,'property_id'=>0])->first();  
                    
                }
                
            }elseif(@$params['message_master_id'])
                $msgmaster= MessageMaster::where(['id'=>@$params['message_master_id']])->first();
            
            if(!@$msgmaster)
                $msgmaster= MessageMaster::where(['provider_id'=>@$params['property_id'],'property_id' =>0,'seeker_id'=>$user_id,'bid_id' => @$params['bid_id']])->first();

            if(@$msgmaster){
                $response['message_master_id']=$msgmaster->id;
                $tot_msg = MessageDetail::where('message_master_id',@$msgmaster->id)->count();
                $response['tot_msg'] = @$tot_msg;
                $response['is_block']=$msgmaster->is_block;
                $response['title']=$msgmaster->getProvider->name;
                $response['s_slug']=$msgmaster->getProvider->slug;
                $response['user_type'] = @$request->usertype;
                $response['bid_id'] = @$request->bid_id;
                if($msgmaster->seeker_id==$user_id)
                    $u_id=$msgmaster->provider_id;
                else
                $u_id=$msgmaster->seeker_id;
                $response['block_by']=$msgmaster->seeker_id==$user_id?'You':$msgmaster->getSeeker->fname.' '.$msgmaster->getSeeker->lname;  
             
            }else{

                
                if(@$params['msgtyp'] == 'Pr'){
                     // $property=User::where('id',@$params['property_id'])->first();
                     $bid = Quotes::where('id',@$params['bid_id'])->first();
                     
                     $property = User::where('id',@$bid->user_id)->first();
                 }else{
                     $property=User::where('id',@$params['provider_id'])->first();
                 }
                 
               
                if(!@$property)
                    $property=User::where('id',@$params['message_master_id'])->first();
                    // dd($property);
                if(@$params['msgtyp'] == 'Pr'){
                    $bid = Quotes::where('id',@$params['bid_id'])->first();
                    $response['message_master_id']=@$bid->user_id;
                }else{
                   $response['message_master_id']=$property->id; 
                }

                $tot_msg = MessageDetail::where('message_master_id',@$response['message_master_id'])->count();
                $response['tot_msg'] = @$tot_msg;

                
                $response['type']="B";
                $response['user_type'] = @$request->usertype;
                if(@$params['msgtyp'] == 'Pr'){
                    $response['bid_id'] = @$params['bid_id'];
                    $response['title']=$property->job_name;
                }else{
                  $response['title']=$property->name;  
                }
                $response['s_slug']=$property->slug;
                if(@$params['msgtyp'] == 'Pr'){
                    $bid = Quotes::where('id',@$params['bid_id'])->first();
                    if($bid->user_id!=$user_id)
                    $u_id=$bid->user_id;
                else
                    $u_id=$user_id;

                }else{
                  if($property->id!=$user_id)
                    $u_id=$property->id;
                else
                    $u_id=$user_id;  
                }
                
            }
            $user=User::where('id',$u_id)->first();
            if(@$user){
                $response['u_name'] = @$user->name;

                $response['u_slug'] = @$user->slug;
                $response['u_image'] =@$user->profile_pic ? url('storage/app/public/profile_picture/'.@$user->profile_pic) : url('public/images/blank.png');
                // $response['online_status'] = @$user->online_status;
            }
        }
        else{
            
            if(@$params['property_id'] == 0){
                
                if(@$params['provider_id']){
                    
                    $msgmaster= MessageMaster::where(['provider_id'=>@$params['provider_id'],'property_id'=>0])->where('seeker_id',Auth::user()->id)->first();
                    
                 
                }elseif(@$params['message_master_id'])
                    
                    $msgmaster= MessageMaster::where(['id'=>@$params['message_master_id']])->first();
                    
                
                
                if(!@$msgmaster)
                    $msgmaster= MessageMaster::where(['provider_id'=>@$params['message_master_id'],'property_id'=>0])->where('seeker_id',Auth::user()->id)->first();

                    
                        
                
                if(@$msgmaster){
                    $tot_msg = MessageDetail::where('message_master_id',@$msgmaster->id)->count();
                    $response['tot_msg'] = @$tot_msg;
                    if(@$msgmaster->property_id == 0){
                        $response['title']=$msgmaster->getProvider->name;
                        $response['s_slug']=$msgmaster->getProvider->slug;
                        $response['m_type'] = "PM";
                    }
                    else
                    {
                        $response['title']=$msgmaster->getProperty->name;
                        $response['s_slug']=$msgmaster->getProperty->slug;
                        $response['m_type'] = "A";
                    }
                    $response['message_master_id']=$msgmaster->id;
                    $response['is_block']=$msgmaster->is_block;
                    $response['user_type'] = "A";
                    
                    
                    if($msgmaster->seeker_id==$user_id)
                        $u_id=$msgmaster->provider_id;
                    else
                    $u_id=$msgmaster->seeker_id;
                    $response['block_by']=$msgmaster->seeker_id==$user_id?'You':$msgmaster->getSeeker->fname.' '.$msgmaster->getSeeker->lname;  
                 
                }else{
                    
                    
                    
                    
                        $property=User::where('id',@$params['provider_id'])->first();
                        
                        $response['message_master_id']=@$property->id;

                        if($response['message_master_id'] != null){
                            $tot_msg = MessageDetail::where('message_master_id',@$response['message_master_id'])->count();
                            $response['tot_msg'] = $tot_msg;
                        }
                    
                        if(!@$property){
                            $property=Property::where('id',@$params['message_master_id'])->first();
                            
                        
                            
                            if(@$property->user_id!=$user_id){
                                $u_id=@$property->user_id;
                            }else{
                                $u_id=$user_id;
                                $response['m_type'] = 'A';
                            }
                        }else{
                            if(@$property->id!=$user_id)
                            $u_id=@$property->id;
                            else
                            $u_id=$user_id;
                            $response['m_type'] = 'PM';
                        }
                           
                        
                        // $response['message_master_id']=@$property->id;
                        
                        $response['type']="B";
                        $response['user_type'] = "A";
                        $response['title']=@$property->name;
                        $response['s_slug']=@$property->slug;
                        // if(@$property->id!=$user_id)
                        //     $u_id=@$property->id;
                        //     else
                        //     $u_id=$user_id;
                    }
                
                
                    $user=User::where('id',$u_id)->first();
                    if(@$user){
                        $response['u_name'] = @$user->name;
                        $response['u_slug'] = @$user->slug;
                        $response['u_image'] =@$user->image ? url('storage/app/public/profile_picture/'.@$user->image) : url('public/images/blank.png');
                        // $response['m_type'] = 'PM';
                        // $response['online_status'] = @$user->online_status;
                    
                }
                

                
            }else{

                
                //Without Property Id == 0
                if(@$params['property_id'])
                    $msgmaster= MessageMaster::where(['property_id'=>@$params['property_id']])->where('seeker_id',Auth::user()->id)->orwhere('provider_id',Auth::user()->id)->first();
                elseif(@$params['message_master_id'])
                    $msgmaster= MessageMaster::where(['id'=>@$params['message_master_id']])->first();
                
                    
                
                if(!@$msgmaster)
                    $msgmaster= MessageMaster::where(['property_id'=>@$params['message_master_id']])->where('seeker_id',Auth::user()->id)->orwhere('provider_id',Auth::user()->id)->first();

                if(@$msgmaster){
                    if(@$msgmaster->property_id == 0){
                        $response['title']=$msgmaster->getProvider->name;
                        $response['s_slug']=$msgmaster->getProvider->slug;
                    }
                    else
                    {
                        $response['title']=$msgmaster->getProperty->name;
                        $response['s_slug']=$msgmaster->getProperty->slug;
                    }
                    $response['message_master_id']=$msgmaster->id;
                    $response['is_block']=$msgmaster->is_block;
                    
                    if($msgmaster->seeker_id==$user_id)
                        $u_id=$msgmaster->provider_id;
                    else
                    $u_id=$msgmaster->seeker_id;
                    $response['block_by']=$msgmaster->seeker_id==$user_id?'You':$msgmaster->getSeeker->fname.' '.$msgmaster->getSeeker->lname;  
                 
                }else{
                    $property=Property::where('id',@$params['property_id'])->first();
                    if(!@$property)
                        $property=Property::where('id',@$params['message_master_id'])->first();
                    $response['message_master_id']=$property->id;
                    $response['type']="B";
                    $response['user_type'] = "A";
                    $response['title']=$property->name;
                    $response['s_slug']=$property->slug;
                    if($property->user_id!=$user_id)
                        $u_id=$property->user_id;
                    else
                        $u_id=$user_id;
                }
                $user=User::where('id',$u_id)->first();
                if(@$user){
                    $response['u_name'] = @$user->name;
                    $response['u_slug'] = @$user->slug;
                    $response['u_image'] =@$user->image ? url('storage/app/public/profile_picture/'.@$user->image) : url('public/images/blank.png');
                    $response['m_type'] = 'A';
                    // $response['online_status'] = @$user->online_status;
                }
                //End Without Property Id == 0
            }
        }
       
        return response()->json($response);
    }

    public function userMessage(Request $request)
    {
        $params = $request->all();
        $user_id=@auth()->user()->id;
        $response = [
            'jsonrpc' => '2.0'
        ];
        MessageDetail::where(['message_master_id'=>@$params['message_master_id']])->where('user_id','!=',@Auth::id())->update(['is_read'=>'Y']);   
        $response['msgcnt']=MessageDetail::where('is_read','N')->where('user_id','!=',@Auth::id())->whereHas('getMessageMaster',function($q1) use($user_id){
            $q1->where('seeker_id',$user_id)
            ->orWhere('provider_id',$user_id);    
        })->count(); 
        $response['tot_msg'] = MessageDetail::where('message_master_id',@$params['message_master_id'])->count();
        if(!@$response['msgcnt']){
            $response['msgcnt']='0';
        }
        $message=MessageMaster::where(['id'=>@$params['message_master_id']])->first();   
        $response['result']= MessageDetail::with('getUser:id,name,online_status,slug,profile_pic,user_type')->where('message_master_id',@$params['message_master_id'])->get();
        return response()->json($response);
    }

    public function typingAjax(Request $request)
    {
        $pusher = new Pusher(config("services.pusher.pusher_app_key"),config("services.pusher.pusher_app_secret"),config("services.pusher.pusher_app_id"), [
            'cluster' => config("services.pusher.pusher_app_cluster")
        ]);
        $user_id=@auth()->user()->id;
        $params = $request->all();
        $msgmaster= MessageMaster::where(['id'=>@$params['message_master_id']])->first();
        if(@$msgmaster->seeker_id==$user_id)
            $reciver_id=@$msgmaster->provider_id;
        else
            $reciver_id=@$msgmaster->seeker_id;
        $response = $pusher->trigger('rivirtual', 'start-end-typing', [
            'reciver_id' => $reciver_id,
            'from' => @auth()->user()->fname.' '.@auth()->user()->lname,
            'typing' => $params['typing'],
            'sender_id'=>$user_id,
            'message_master_id'    =>  (int) @$params['message_master_id'],
            'socket_id' => $params['socket_id'],
        ]);
        if ($response) {
            return response()->json([
                'result' => [
                    'code' => 200
                ]
            ]);
        } else {
            return response()->json([
                'error' => [
                    'message' => 'Cound\'nt send message.'
                ]
            ]);
        }
    }
    public function blockMessage(Request $request)
    {
        $pusher = new Pusher(config("services.pusher.pusher_app_key"),config("services.pusher.pusher_app_secret"),config("services.pusher.pusher_app_id"), [
            'cluster' => config("services.pusher.pusher_app_cluster")
        ]);
        $userId = auth()->user()->id;
        $msgMstrID=$request->message_master_id;
        $msgmaster=MessageMaster::where('id',$msgMstrID)->first();
        if(@$request->type=='B'){
            MessageMaster::where('id',$msgMstrID)->update(['is_block'=>'Y']);
              $isBlock='Y';
        }else{
            MessageMaster::where('id',$msgMstrID)->update(['is_block'=>'N']);
              $isBlock='N';
        }
        $name=auth()->user()->fname.' '.auth()->user()->lname;
        if($msgmaster->seeker_id==$userId){
            $reciver_id=$msgmaster->provider_id;
        }else{
            $reciver_id=$msgmaster->seeker_id;
        }
        $response = $pusher->trigger('rivirtual', 'receive-event', [
            'reciver_id'         =>  $reciver_id,
            'type'          =>  @$request->type,
            'sender_name'      => $name,
            'message_master_id' => (int)  @$msgMstrID,
        ], $request->socket_id);
          $res = $pusher->trigger('rivirtual', 'block-event', [
          'seeker_id' => $msgmaster->seeker_id,
          'provider_id' =>$msgmaster->provider_id,
          'is_block' => $isBlock,
          'message_master_id' => (int) @$msgMstrID,
          ], @$reqData['socket_id']);
        if ($response) {                 
            return response()->json([
                'result' => [
                    'code' => 200,
                ]
            ]);
        } else {
            return response()->json([
                'error' => [
                    'message' => 'Something went wrong.',
                ]
            ]);
        }
    }
    public function achive($id = null)
    {
        if($id){
            $userId=auth()->user()->id;
            $msgmaster=MessageMaster::where('id',$id)->first();
            if($msgmaster->seeker_id==$userId){
                $chk=MessageMaster::where('id',$id)->where('archive_seeker_id',$userId)->first();
                if($chk)
                    $msgmaster->update(['archive_seeker_id'=>0]);
                else
                    $msgmaster->update(['archive_seeker_id'=>$userId]);
            }else if($msgmaster->provider_id==$userId){
                $chk=MessageMaster::where('id',$id)->where('archive_provider_id',$userId)->first();
                if($chk)
                    $msgmaster->update(['archive_provider_id'=>0]);
                else
                    $msgmaster->update(['archive_provider_id'=>$userId]);
            }
        }
        return redirect()->route('show.message');
    }
}

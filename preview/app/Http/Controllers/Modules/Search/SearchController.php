<?php

namespace App\Http\Controllers\Modules\Search;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Mail;
use App\Models\Category;
use App\Models\Property;
use App\Models\PropertyToImage;
use App\Models\PropertyToFacilitiesAmenities;
use App\Models\ProviderToImage;
use App\Models\State;
use App\Models\City;
use App\Models\SubCategory;
use App\Models\UserToSaveSearch;
use App\Models\PropertyReview;
use App\Models\JobToProReview;
use App\Models\LoanRequest;
use App\Mail\LoanRequestMail;
use App\Mail\PropertyManagementMail;
class SearchController extends Controller
{
    /**
    *   Method      : index
    *   Description : service provider search listing and filtering
    *   Author      : sudipta
    *   Date        : 2021-NOV-12
    **/
    public function index(Request $request,$slug=null){
        $data['category'] = Category::where('status','A')->get();
        $data['states'] = State::where('country_id',101)->orderBy('name','ASC')->get();
        $data['subcategory'] = [];
        $data['cites'] =[];
        $data['maxPrice']=User::where('user_type','S')->orderby('budget','desc')->first();
        // $experience = User::where('user_type','S')->where('approval_status','Y')->pluck('experience')->toArray();
        $allProviders = User::with(['proToCategory.categoryName','providerToLanguage.userLanguage','proToSubCategory'])->where('user_type','S')->where('status','A')->where('approval_status','Y');
        if(@$slug != null){
            $subcat = [];
           $subcat = SubCategory::where('slug',$slug)->first();
            
            $allProviders = $allProviders->whereHas('proToSubCategory',function($q) use($subcat){
                $q->where('category_id', @$subcat->id);
            });
            $data['subcategory'] = SubCategory::orderBy('name','ASC')->get();
            $data['catId'] = SubCategory::where('slug',$slug)->first();
            $data['subcatId'] = SubCategory::where('slug',$slug)->first();  
        }
        if($request->all()){
            
            if($request->pop_category){
                $data['subcategory'] = SubCategory::orderBy('name','ASC')->get();
                $allProviders = $allProviders->whereHas('proToCategory', function($q) use($request){
                    $q->where('category_id',$request->pop_category);
                });
            }
            if($request->category){
                $data['subcategory'] = SubCategory::orderBy('name','ASC')->get();
                $allProviders = $allProviders->whereHas('proToCategory', function($q) use($request){
                    $q->where('category_id',$request->category);
                });
            }
            if($request->sub_category){
                $data['subcategory'] = SubCategory::orderBy('name','ASC')->get();
                $data['catId'] = SubCategory::where('id',$request->sub_category)->first();
                $allProviders = $allProviders->whereHas('proToSubCategory', function($q) use($request){
                    $q->where('category_id',$request->sub_category);
                });
            }
            if($request->state){

                $data['cites'] = City::orderBy('name','ASC')->get();
                $allProviders=$allProviders->where('state',$request->state);
            }
            if($request->city){

                $allProviders=$allProviders->where('city',$request->city);
            }
            if($request->amount1!=null && $request->amount2!=null ){
                $allProviders=$allProviders->whereBetween('budget',[$request->amount1 , $request->amount2]);
            }
            if(@$request->rating){

                foreach(@$request->rating as $key=>$rate){
                    if($rate == 3){

                        $allProviders = $allProviders->where('avg_review','>=',2.75);

                    }
                   if($rate == 4){

                        $allProviders = $allProviders->where('avg_review','>=',3.75);

                    }
                    if($rate == 2){

                        $allProviders = $allProviders->where('avg_review','>=',2.75);

                    }

                }

            }
            if(@$request->year){

                    foreach(@$request->year as $yr){
                        if(@$yr == 2){

                            $allProviders = $allProviders->whereBetween('experience',[0,2]);

                        }
                        if(@$yr == 5){

                            $allProviders = $allProviders->whereBetween('experience',[3,5]);

                        }
                        if(@$yr == 10){

                        $allProviders = $allProviders->whereBetween('experience',[6,10]);

                        }
                       if(@$yr == 11){

                            $allProviders = $allProviders->where('experience','>=',11);

                        }


            }
        }
            if(@$request->sort_by_value){
                if(@$request->sort_by_value == 'H'){
                    $allProviders = $allProviders->orderBy('budget','desc');
                }
                else if(@$request->sort_by_value == 'L'){
                    $allProviders = $allProviders->orderBy('budget','asc');
                }
                else if(@$request->sort_by_value == 'RH'){
                    $allProviders = $allProviders->orderBy('tot_review','desc');
                }
                else if(@$request->sort_by_value == 'RL'){
                    $allProviders = $allProviders->orderBy('tot_review','asc');
                }
            }
            $data['request'] = $request->all();
        }
        if(empty(@$request->sort_by_value)){
            $allProviders = $allProviders->orderBy('id','desc');
        }
        $data['total_pro'] = $allProviders->count();
        $allProviders = $allProviders->paginate(10);
        $data['providers'] = $allProviders;
        return view('modules.search.find_service_pro')->with($data);
    }

     /**
     *   Method      : proProfileShow
     *   Description : service provider profile show
     *   Author      : sudipta
     *   Date        : 2021-NOV-12
     **/

    public function proProfileShow($slug=null){
        $data['provider'] = User::with('proToCategory.categoryName')->where('slug',$slug)->first();
        $workImages = ProviderToImage::where('user_id',$data['provider']->id)->get();
        $new=[];
        $i=0;
        foreach($workImages as $key=>$item){
            $new[$i][]=$item;
            if(($key+1)%2==0){
                $i=$i+1;
            }
        }
        $data['workImages']=$new;
        $data['allReviews'] = JobToProReview::with('userDetails')->where('provider_id',$data['provider']->id)->take(6)->get();
        $data['hideReviews'] = JobToProReview::with('userDetails')->where('provider_id',$data['provider']->id)->get();
        $data['proSave'] = [];
        if(Auth::user()){

            $data['proSave'] = UserToSaveSearch::where('user_id',Auth::user()->id)->where('pro_id',$data['provider']->id)->first();
        }
        
        
        return view('modules.search.pro_profile')->with($data);
    }

     /**
     *   Method      : agentProfileShow
     *   Description : agent profile show
     *   Author      : sudipta
     *   Date        : 2021-NOV-12
     **/

    public function agentProfileShow($slug=null){

     $data['agent'] = User::where('slug',$slug)->first();
     if($data['agent']==null){
          return redirect()->route('search.property');
      }
     $data['property'] = Property::with('propertyImageMain')->where('user_id',$data['agent']->id)->where('status','A')->where('aprove_by_admin','Y')->get();
     $data['allReviews'] = PropertyReview::with(['userDetails','propertyDetails'])->where('owner_id',$data['agent']->id)->take(3)->get();
     $data['hideReviews'] = PropertyReview::with(['userDetails','propertyDetails'])->where('owner_id',$data['agent']->id)->get();
     $data['agentSave'] = [];
     if(Auth::user()){

         $data['agentSave'] = UserToSaveSearch::where('user_id',Auth::user()->id)->where('agent_id',$data['agent']->id)->first();
     }
     return view('modules.search.agent_profile')->with($data);
}

 public function hirePro(){
       $data['category'] = Category::where('status','A')->get();
       $data['pop_cat'] = Category::where('status','A')->where('make_popular','Y')->get();
       $data['states'] = State::where('country_id',101)->orderBy('name','ASC')->get();
       $data['cites'] = [];
       $data['categories'] = Category::with('subCategoryDetails')->where('status','A')->get();
       $data['pop_categories'] = SubCategory::where('status','A')->where('make_popular','Y')->get();
       //return $data['pop_categories'];
      return view('modules.search.hire_pro')->with($data);
 }

 public function sellPage(){

      return view('modules.search.sell');
 }

 public function helpPage(){

    return view('modules.search.help');
 }

  public function agentSave($id=null){

    $check = User::where('id',$id)->first();

    if(@$check == null){

        session()->flash('error','something went wrong');
        return redirect()->back();

    }

    $saveCheck = UserToSaveSearch::where('user_id',Auth::user()->id)->where('agent_id',$id)->first();

    if(@$saveCheck){
        $saveCheck->delete();
        session()->flash('error','Agent unsaved');
        return redirect()->back();
    }
    $ins = [];
    $ins['user_id'] = Auth::user()->id;
    $ins['agent_id'] = $id;

    $save = UserToSaveSearch::create($ins);

    if(@$save){

      session()->flash('success','Agent saved');
      return redirect()->back();
    }
  }

   public function proSave($id=null){

    $check = User::where('id',$id)->first();

    if(@$check == null){

        session()->flash('error','something went wrong');
        return redirect()->back();

    }

    $saveCheck = UserToSaveSearch::where('user_id',Auth::user()->id)->where('pro_id',$id)->first();

    if(@$saveCheck){
        $saveCheck->delete();
        session()->flash('error','PRO UNSAVED');
        return redirect()->back();
    }
    $ins = [];
    $ins['user_id'] = Auth::user()->id;
    $ins['pro_id'] = $id;

    $save = UserToSaveSearch::create($ins);

    if(@$save){

      session()->flash('success','PRO SAVED');
      return redirect()->back();
    }
   }
    public function saveProperty($id=null){

    $check = Property::where('id',$id)->first();

    if(@$check == null){

        session()->flash('error','something went wrong');
        return redirect()->back();

    }

    $saveCheck = UserToSaveSearch::where('user_id',Auth::user()->id)->where('property_id',$id)->first();

    if(@$saveCheck){
        @$saveCheck->delete();
        session()->flash('error','Property unsaved');
        return redirect()->back();
    }
    $ins = [];
    $ins['user_id'] = Auth::user()->id;
    $ins['property_id'] = $id;

    $save = UserToSaveSearch::create($ins);

    if(@$save){

      session()->flash('success','Property saved');
      return redirect()->back();
    }
}

     /**
     *   Method      : find agent
     *   Description : agent listing
     *   Author      : sudipta
     *   Date        : 2022-JAN-06
     **/

   public function findAgent(Request $request){
       
    $data['states'] = State::where('country_id',101)->orderBy('name','ASC')->get();
    $data['cites'] =[];
    
    $allProviders = User::with(['providerToLanguage.userLanguage'])->where('user_type','A')->where('status','A')->where('approval_status','Y');
    
    

    if($request->all()){
      
         if($request->keyword){

              $allProviders = $allProviders->where(function($q) use($request){

                   $q->where('name','like','%'.$request->keyword.'%')
                   ->orWhereHas('userCountry', function($q) use($request){

                    $q->where('name','LIKE','%'.$request->keyword.'%');
               });
                  
              });
         }
         if($request->post_code){

            $allProviders = $allProviders->where(function($q) use($request){

                  $q->where('postal_code',$request->post_code);
            });
      }

         if($request->agent_for){

               $allProviders = $allProviders->where(function($q) use($request){

                     $q->where('agent_for','like','%'.$request->agent_for.'%');
               });
         }
        
        if($request->state){

            $data['cites'] = City::where('state_id',$request->state)->orderBy('name','ASC')->get();
            $allProviders=$allProviders->where('state',$request->state);
        }
        if($request->city){

            $allProviders=$allProviders->where('city',$request->city);
        }
        
       
        if(@$request->rating){

            foreach(@$request->rating as $key=>$rate){
                if($rate == 4){

                    $allProviders = $allProviders->where('owner_avg_review','>=',3.75);

                }
               if($rate == 3){

                    $allProviders = $allProviders->where('owner_avg_review','>=',2.75);

                }
                if($rate == 2){

                    $allProviders = $allProviders->where('owner_avg_review','>=',1.75);

                }
                if($rate == 1){

                    $allProviders = $allProviders->where('owner_avg_review','>=',0.75);
                }

            }

        }
        
        if(@$request->sort_by_value){
             if(@$request->sort_by_value == 'N'){

                 $allProviders = $allProviders->orderBy('id','desc');
             }
             else if(@$request->sort_by_value == 'RH'){
                $allProviders = $allProviders->orderBy('owner_total_review','desc');
            }
            else if(@$request->sort_by_value == 'RL'){
                $allProviders = $allProviders->orderBy('owner_total_review','asc');
            }
        }
        $data['request'] = $request->all();
    }
    if(empty(@$request->sort_by_value)){
        $allProviders = $allProviders->orderBy('id','desc');
    }
    $data['total_agent'] = $allProviders->count();
    $allProviders = $allProviders->paginate(10);
    $data['providers'] = $allProviders;
    
    return view('modules.search.find_agent')->with($data);
   }

   public function searchAgent(){

       return view('modules.search.agent_search');
   }

   public function propertyManagement(){

       return view('modules.search.property_management');
   }

   public function loanPage(){

    return view('modules.search.loan');
   }
    public function commercialPage(){

    return view('modules.search.commercial');
   }
    public function residentialPage(){

    return view('modules.search.residential');
   }
   
   public function saveLoanRequest(Request $request){
     $data['name'] = @$request->name;
     $data['phone'] = @$request->phone;
     $data['email'] = @$request->email;
     $data['service_city'] = @$request->service_city;
     $data['user_type'] = @$request->user_type;

     // $result = LoanRequest::insertGetId($data);
     Mail::send(new LoanRequestMail($data));
     return redirect()->back()->with('success','Loan Request Placed Successfully');
     
    }
    
    public function savePropertyManagement(Request $request){
     $data['name'] = @$request->name;
     $data['phone'] = @$request->phone;
     $data['email'] = @$request->email;
     $data['service_city'] = @$request->service_city;
     $data['user_type'] = @$request->user_type;

     // $result = LoanRequest::insertGetId($data);
     Mail::send(new PropertyManagementMail($data));
     return redirect()->back()->with('success','Property Management Request Placed Successfully');
    }

}

<?php

namespace App\Http\Controllers\Modules\Search;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\PostJob;
use DB;
use Auth;
use App\Models\SubCategory;
use App\Models\State;
use App\Models\City;
class JobSearchController extends Controller
{
    public function __construct(){

          return $this->middleware('auth');
    }

   public function index(Request $request){

        $data['category'] = Category::where('status','A')->get();
        $data['subcategory'] = [];
        $data['states'] = State::where('country_id',101)->get();
        $data['cites'] = [];
        $data['totaljobs'] = PostJob::where('status','!=','D')->count();
        $data['maxPrice'] = PostJob::orderBy('budget_range_from','desc')->first();
        $allJobs = PostJob::with(['categoryName','userDetails','subCategoryName','jobState','jobCity'])->where('status','A');
        if($request->all()){
            if($request->category){
                $data['subcategory'] = SubCategory::orderBy('name','ASC')->get();
                  $allJobs = $allJobs->where(function($q) use($request){

                       $q->where('category',$request->category);
                  });
            }
            if($request->sub_category){
                $data['subcategory'] = SubCategory::orderBy('name','ASC')->get();
                $data['catId'] = SubCategory::where('id',$request->sub_category)->first();
                $allJobs = $allJobs->where(function($q) use($request){
                    $q->where('sub_category',$request->sub_category);
                });
            }
            if($request->job_type){

                 $allJobs = $allJobs->where(function($q) use($request){

                      $q->where('job_type',$request->job_type);
                 });
            }
            if($request->rate_type){

                 $allJobs = $allJobs->where(function($q) use($request){

                     $q->where('budget_type',$request->rate_type);
                 });
            }
            if($request->amount1!=null && $request->amount2!=null ){
                $allJobs=$allJobs->whereBetween('budget_range_from',[$request->amount1 , $request->amount2]);
            }
            if($request->state){
                  $data['cites'] = City::where('state_id',$request->state)->orderBy('name','asc')->get();
                  $allJobs = $allJobs->where(function($q) use($request){

                      $q->where('state',$request->state);
                  });
            }
            if($request->city){

                $allJobs = $allJobs->where(function($q) use($request){

                    $q->where('state',$request->city);
                });
          }
            if(@$request->sort_by_value){
                if(@$request->sort_by_value == 'H'){
                    $allJobs = $allJobs->orderBy('budget_range_from','desc');
                }
                else if(@$request->sort_by_value == 'L'){
                    $allJobs = $allJobs->orderBy('budget_range_from','asc');
                }
                // else if(@$request->sort_by_value == 'RH'){
                //     $allJobs = $allJobs->orderBy('tot_review','desc');
                // }
                // else if(@$request->sort_by_value == 'RL'){
                //     $allJobs = $allJobs->orderBy('tot_review','asc');
                // }
            }

            $data['request'] = $request->all();
        }
        if(empty(@$request->sort_by_value)){
            $allJobs = $allJobs->orderBy('id','desc');
        }
        $allJobs = $allJobs->paginate(10);
        $data['allJobs'] = $allJobs;

        return view('modules.service_provider.find_job')->with($data);

   }
}

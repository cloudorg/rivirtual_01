<?php

namespace App\Http\Controllers\Modules\Search;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Mail;
use App\Country;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Storage;
use App\Models\FacilitiesAmenities;
use App\Models\Property;
use App\Models\PropertyToImage;
use App\Models\PropertyToFacilitiesAmenities;
use App\Models\AgentToAvailability;
use App\Models\PropertyVisitRequest;
use App\Models\UserToSaveSearch;
use App\Models\PropertyReview;

class PropertySearchController extends Controller
{
     public function __construct()
    {
        // $this->middleware('auth');
    }


    /**
     *   Method      : searchProperty
     *   Description : for search Property
     *   Author      : Soumojit
     *   Date        : 2021-NOv-13
     **/

    public function searchProperty(Request $request){
        
        $myAllProperty= Property::with(['propertyImageMain','propertyUser'])->where('status','=','A')->where('is_sold','N')->where('aprove_by_admin','Y');
        $data['maxPrice']=Property::where('status','!=','I')->orderby('budget_range_from','desc')->first();
        // $allDetails= Property::select('status','bathroom','no_of_bedrooms')->where('status','=','A')->pluck('bathroom','no_of_bedrooms');
        // return $allDetails;
        if(@$request->all()){
            // return $request;
            
            if(@$request->property_type){
                if(@$request->master_property_type){
                    $myAllProperty=$myAllProperty->where('master_property_type',@$request->master_property_type)->whereIn('property_type',$request->property_type);
                }else{
                    $myAllProperty=$myAllProperty->whereIn('property_type',$request->property_type);
                }
                
            }else if(@$request->master_property_type){

                $myAllProperty=$myAllProperty->where('master_property_type',@$request->master_property_type);
            }
            if(@$request->property_for){
                $myAllProperty=$myAllProperty->whereIn('property_for',$request->property_for);
            }
            if(@$request->no_of_bedrooms){
                $myAllProperty=$myAllProperty->where('no_of_bedrooms','=',$request->no_of_bedrooms);
            }
            if(@$request->bathroom){
                $myAllProperty=$myAllProperty->where('bathroom','=',$request->bathroom);
            }
            if(@$request->furnishing){
                $myAllProperty=$myAllProperty->where('furnishing',$request->furnishing);
            }
            if(@$request->construction_status){
                $myAllProperty=$myAllProperty->where('construction_status',$request->construction_status);
            }
            if($request->amount1!=null && $request->amount2!=null ){
                $myAllProperty=$myAllProperty->whereBetween('budget_range_from',[$request->amount1 , $request->amount2]);
            }
            if(@$request->keyword){
                $myAllProperty=$myAllProperty->where(function($query) use ($request){
                    $query->where('name','LIKE','%'.$request->keyword.'%')
                    ->orWhere('state','LIKE','%'.$request->keyword.'%')
                    ->orWhere('city','LIKE','%'.$request->keyword.'%')
                    ->orWhereHas('countryName',function($q) use ($request){
                        $q->where('name','LIKE','%'.$request->keyword.'%');
                    });
                });
        			// ->orWhere('profile_headline','LIKE','%'.request('keyword').'%')
        			// 	  ->orWhereHas('companyName',function($q){
        			// 		$q->where('name','LIKE','%'.request('keyword').'%');
        			// 	});
                // ->where('name',[$property_type]);
            }
            // if(@$request->lat && $request->long){
            //     $query =  Property::selectRaw("*,(6371 * acos ( cos ( radians('".$request->lat."') ) * cos( radians( address_lat ) ) * cos( radians( address_long ) - radians('".$request->long."') ) + sin ( radians('".$request->lat."') ) * sin( radians( address_lat ) ))) as distance");
            //     $query = $query->having("distance", "<", $request->distance)->pluck('id')->toArray();
            //     $myAllProperty=$myAllProperty->whereIn('id',$query);

            // }
            if(@$request->location){
                if(stripos(@$request->location,'₹') ===  false){
                    
                    $myAllProperty=$myAllProperty->where(function($query) use ($request){
                        $query->whereHas('localityName',function($q) use ($request){
                            $q->where('locality_name','LIKE','%'.$request->location.'%');
                        })->orWhereHas('cityName',function($q) use ($request){
                            $q->where('name','LIKE','%'.$request->location.'%');
                        });
                    });
                }
            }
            if($request->sqFtFrom!=null && $request->sqFtTo!=null ){
                $myAllProperty=$myAllProperty->whereBetween('area',[$request->sqFtFrom , $request->sqFtTo]);
            }

            if(@$request->short_by_value==0 ){
                $myAllProperty=$myAllProperty->orderBy('id','desc');
            }
            if(@$request->short_by_value==1 ){
                $myAllProperty=$myAllProperty->orderBy('budget_range_from','asc');
            }
            if(@$request->short_by_value==2 ){
                $myAllProperty=$myAllProperty->orderBy('budget_range_from','desc');
            }
            $data['key']=$request->all();
        }
        if($request->short_by_value==null){
            $myAllProperty=$myAllProperty->orderBy('id','desc');
        }
        $data['totalProperty']=$myAllProperty->count();
        $myAllProperty=$myAllProperty->paginate(9);


        $data['allProperty']=$myAllProperty;
        // return $data['allProperty'];
        return view('modules.search.search_property')->with($data);
    }

    /**
     *   Method      : searchPropertyDetails
     *   Description : for Property Details
     *   Author      : Soumojit
     *   Date        : 2021-NOv-15
     **/
    public function searchPropertyDetails($slug){
        $propertyDetails=Property::with(['propertyImageMain','propertyUser.userCountry','localityName','cityName','stateName'])->where('status','!=','I')->where('slug',$slug)->first();
        $data['agent'] = User::where('id',@$propertyDetails->propertyUser->id)->first();
        if($propertyDetails==null){
            session()->flash('error', 'Something went wrong');
            return redirect()->route('search.property');
        }
        $data['propertyDetails']=$propertyDetails;
        $data['propertyFacilitiesAmenities']=PropertyToFacilitiesAmenities::with(['facilitiesAmenitiesName'])->where('property_id',$propertyDetails->id)->get();
        $data['propertyImageExterior']=PropertyToImage::where('image_for','EP')->where('property_id',$propertyDetails->id)->get();
        $data['propertyImageInterior']=PropertyToImage::where('image_for','IP')->where('property_id',$propertyDetails->id)->get();
        $data['propertyImageFloor']=PropertyToImage::where('image_for','FP')->where('property_id',$propertyDetails->id)->get();
        $data['similarProperty']=Property::with(['propertyImageMain','propertyUser'])->where('status','A')->take(10)->get();

        $dayAvailability= AgentToAvailability::where('user_id',$propertyDetails->user_id)->pluck('day')->toArray();
        $dayAvailability=array_unique($dayAvailability);
        if(@$dayAvailability == null){

             $dayAvailability = [1,2,3,4,5,6,7];
        }

        $replacements = array(
            '7' => '0',
        );
        foreach ($dayAvailability as $key => $value) {
            if (isset($replacements[$value])) {
                $dayAvailability[$key] = $replacements[$value];
            }
        }
        // $dayAvailability=array_unique($dayAvailability);
        $data['dayAvailability']= $dayAvailability;
       
        $data['allReviews'] = PropertyReview::with('userDetails')->where('property_id',@$propertyDetails->id)->take(6)->get();
        if(Auth::user()){
            $data['user_rating'] = PropertyReview::where('property_id',@$propertyDetails->id)->where('posted_by_id',Auth::user()->id)->first();
        }
       $data['hideReviews'] = PropertyReview::with('userDetails')->where('property_id',@$propertyDetails->id)->get();
        return view('modules.search.search_property_details')->with($data);

    }

     /**
     *   Method      : getTimeSlot
     *   Description : for get Time Slot
     *   Author      : Soumojit
     *   Date        : 2021-Nov-23
     **/
    public function getTimeSlot(Request $request){

        $response = [
            'jsonrpc' => 2.0
        ];
        $params = $request->params;
        $day = date('N',strtotime($params['date']));
        $allTimeSlot=AgentToAvailability::where('user_id',$params['user_id'])->where('day',$day)->get();

        $new=[];
        $html='';
        $html.='<option value="" selected>Select Time Slot</option>';
        foreach($allTimeSlot as $friday){

            $timeDifference=(strtotime($friday->to_time)-strtotime($friday->from_time))/60;
            if($timeDifference>30){
                $i=0;
                $num=($timeDifference/30);
                for($i=0; $i<$num ; $i++){
                    $timeAdd=30*$i;
                    $from_time=date('H:i:s',strtotime($timeAdd." minutes",strtotime($friday->from_time)));
                    $to_time=date('H:i:s',strtotime("30 minutes",strtotime($from_time)));
                    $check=PropertyVisitRequest::whereBetween('visit_date',[date('Y-m-d H:i:s',strtotime($params['date'].$from_time)),date('Y-m-d H:i:s',strtotime("-1 seconds",strtotime($params['date'].$to_time)))])
                    ->where('agent_id',$params['user_id'])->count();
                    if($check==0){
                        $new[]=[
                            'day'=>$friday->day,
                            'from_time'=>$from_time,
                            'to_time'=>$to_time,
                        ];
                        $html.='<option value='.date('H:i',strtotime($from_time)).'>'.date('H:i',strtotime($from_time)).'</option>';
                    }
                }
            }else{
            
                $new[]=[
                    'day'=>$friday->day,
                    'from_time'=>$friday->from_time,
                    'to_time'=>$friday->to_time,
                ];
                $html.='<option value='.date('H:i',strtotime($friday->from_time)).'>'.date('H:i',strtotime($friday->from_time)).'</option>';
            }
        }
        if($allTimeSlot->isEmpty()){
            $allTimeSlot = ['00:00','00:15','00:30','00:45','01:00','01:15','01:30','01:45','02:00','02:15','02:30','02:45','03:00','03:15','03:30','03:45','04:00','04:15','04:30','04:45','05:00','05:15','05:30','05:45','06:00','06:15','06:30','06:45','07:00','07:15','07:30','07:45','08:00','08:15','08:30','08:45','09:00','09:15','09:30','09:45','10:00','10:15','10:30','10:45','11:00','11:15','11:30','11:45','12:00','12:15','12:30','12:45','13:00','13:15','13:30','13:45','14:00','14:15','14:30','14:45','15:00','15:15','15:30','15:45','16:00','16:15','16:30','16:45','17:00','17:15','17:30','17:45','18:00','18:15','18:30','18:45','19:00','19:15','19:30','19:45','20:00','20:15','20:30','20:45','21:00','21:15','21:30','21:45','22:00','22:15','22:30','22:45','23:00','23:15','23:30','23:45']; 
            foreach($allTimeSlot as $val){

                $html.='<option value="'.$val.'">'.$val.'</option>';
            }
           
            
        }
        $response['result']['data'] = $new;
        $response['result']['html'] = $html;
        return response()->json($response);
    }

    public function saveProperty($id=null){

          $check = Property::where('id',$id)->first();

          if(@$check == null){

              session()->flash('error','something went wrong');
              return redirect()->back();

          }

          $saveCheck = UserToSaveSearch::where('user_id',Auth::user()->id)->where('property_id',$id)->first();

          if(@$saveCheck){
             
             @$saveCheck->delete();
              session()->flash('error','Property unsave');
              return redirect()->back();
          }
          $ins = [];
          $ins['user_id'] = Auth::user()->id;
          $ins['property_id'] = $id;

          $save = UserToSaveSearch::create($ins);

          if(@$save){

            session()->flash('success','Property saved');
            return redirect()->back();
          }
    }

    public function saveReting(Request $request){

          $property_check = Property::where('id',$request->property_id)->where('status','A')->first();

          if(@$property_check == null){

               session()->flash('error','something went wrong');
               return redirect()->back();
          }
          $existing_rating = PropertyReview::where('property_id',$request->property_id)->where('posted_by_id',Auth::user()->id)->first();

          if($existing_rating){

              $upd = [];
              $upd['review_point'] = $request->property_rating;
              $upd['review_text'] = $request->description;

             $update =  PropertyReview::where('property_id',$request->property_id)->where('posted_by_id',Auth::user()->id)->update($upd);
             if($update){

                $rating = PropertyReview::where('property_id',$request->property_id)->get();
                $agent_rating = PropertyReview::where('owner_id',$request->owner_id)->get();
                $agent_rating_sum = PropertyReview::where('owner_id',$request->owner_id)->sum('review_point');
                $rating_sum = PropertyReview::where('property_id',$request->property_id)->sum('review_point');

                if($rating->count()>0){

                    $avg_review = $rating_sum/$rating->count();
               }else{

                   $avg_review = 0.00; 
               }
               $ratingupd = [];
               $ratingupd['avg_review'] = $avg_review;
               $ratingupd['tot_review'] = $rating->count();

               Property::where('id',$request->property_id)->update($ratingupd);
               if($agent_rating->count()>0){

                    $agent_avg_review = $agent_rating_sum/$agent_rating->count(); 
               }else{

                   $agent_avg_review = 0.00;
               }
               $agentrating = [];
               $agentrating['owner_avg_review'] = $agent_avg_review;
               $agentrating['owner_total_review'] = $agent_rating->count();
               User::where('id',$request->owner_id)->update($agentrating);
               session()->flash('success','Property review updated successfully');
                return redirect()->back();
             }
          }
          else{

               $ins = [];
               $ins['property_id'] = $request->property_id;
               $ins['owner_id'] = $request->owner_id;
               $ins['posted_by_id'] = Auth::user()->id;
               $ins['review_point'] = $request->property_rating;
               $ins['review_text'] = $request->description;

              $create =  PropertyReview::create($ins);
            
              if($create){

                $rating = PropertyReview::where('property_id',$request->property_id)->get();
                $agent_rating = PropertyReview::where('owner_id',$request->owner_id)->get();
                $agent_rating_sum = PropertyReview::where('owner_id',$request->owner_id)->sum('review_point');
                $rating_sum = PropertyReview::where('property_id',$request->property_id)->sum('review_point');
                if($rating->count()>0){

                     $avg_review = $rating_sum/$rating->count();
                }else{

                    $avg_review = 0.00; 
                }
                $ratingupd = [];
                $ratingupd['avg_review'] = $avg_review;
                $ratingupd['tot_review'] = $rating->count();

                Property::where('id',$request->property_id)->update($ratingupd);
                if($agent_rating->count()>0){

                     $agent_avg_review = $agent_rating_sum/$agent_rating->count(); 
                }else{

                    $agent_avg_review = 0.00;
                }
                $agentrating = [];
                $agentrating['owner_avg_review'] = $agent_avg_review;
                $agentrating['owner_total_review'] = $agent_rating->count();
                User::where('id',$request->owner_id)->update($agentrating);
                session()->flash('success','Property review posted successfully');
                return redirect()->back();
              }
          }
    }

}

<?php

namespace App\Http\Controllers\Modules\Razorpay;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Mail;
use App\Country;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Storage;
use Razorpay\Api\Api;
use Session;
use Exception;
use App\Models\PostJob;
use App\Models\Quotes;
use App\Models\Milestone;
use App\Models\UserTransaction;
use App\Models\Payment;
use App\Models\AgentMemberShipPackage;
use App\Models\UserToPackage;
use App\Models\ProPackage;
use App\Mail\MilestoneCreateMail;



class RazorpayController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(Request $request)
    {   
        if(@$request->page_type == 'M'){
            $data['provider'] = User::find($request->pro_id);
            $data['post'] = PostJob::find($request->post_id);
            $data['bid'] = Quotes::with('jobDetails')->where('post_id',$request->post_id)->where('user_id',$request->pro_id)->first();
            $data['m_des'] = $request->description;
            if($data['post']->rivirtual_assistance == 'Y'){

                    $data['org_amt'] = @$request->amount;

                if($request->amount < 100000){ //Less than 1L

                    $data['amt1'] = (@$request->amount + (20*@$request->amount)/100);
                    $data['amt'] = @$data['amt1']*100 ;   

                }else if($request->amount >= 100000 && $request->amount <= 500000){ // Between 1L and 5L
                    $data['amt1'] = (@$request->amount + (15*@$request->amount)/100);
                    $data['amt'] = @$data['amt1']*100 ;
                }else if($request->amount > 500000){ // More than 5L
                    $data['amt1'] = (@$request->amount + (10*@$request->amount)/100);
                    $data['amt'] = @$data['amt1']*100 ; 
                }

            }else{

                $data['org_amt'] = @$request->amount;
                $data['amt'] = $request->amount*100;
                $data['amt1'] = $request->amount; 
            }


            $BIDAMT = $amount = $request->amount;
        
            $amount_INR = $request->amount;
            if(@$request->bidamt){
                $BIDAMT = $request->bidamt;
            }

            $getMisAmt = Milestone::where(['job_id'=>@$request->post_id,'pro_id'=>@$request->pro_id])->whereIn('status',['Funded','Released'])->sum('amount');

            $getFrBid = Quotes::where(['post_id'=>@$request->post_id,'user_id'=>@$request->pro_id])->first();

            $amt = @$getFrBid->amount;
        

            $insert['user_id'] = $data['post']->user_id;
            $insert['bider_id'] = $data['bid']->user_id;
            $insert['bid_id'] = $data['bid']->id;
            $insert['amount'] = $data['amt1'];
            $insert['job_id'] = $data['post']->id;
            $insert['page_type'] = 'M';
            $data['payment_id'] = Payment::insertGetId($insert);


            return view('modules.razorpay.razorpayview')->with($data);

        }else if(@$request->page_type == 'S' && @$request->mem_type == 'Pro'){
        
        
           $check = ProPackage::where('id',@$request->subscription_id)->first();
           
           if(@$check == null){

               session()->flash('error','something went wrong');
               return redirect()->back();
           }
           $data['package'] =$check;
           $data['page_type'] = @$request->page_type;
           $data['mem_type'] = @$request->mem_type;
           if(@$check->type == 'M'){
                $data['amt1'] = @$check->monthly_price;
            }else if(@$check->type == 'Y'){
                $data['amt1'] = @$check->yearly_price;

            }

            $insert['user_id'] = Auth::user()->id;
            if(@$check->type == 'M'){
                $insert['amount'] = @$check->monthly_price;
            }else if(@$check->type == 'Y'){
                $insert['amount'] = @$check->yearly_price;

            }
            $insert['page_type'] = 'S';
            $data['payment_id'] = Payment::insertGetId($insert);

           return view('modules.razorpay.razorpayview_subs')->with($data);
        }else if(@$request->page_type == 'S'){
            
           $check = AgentMemberShipPackage::where('id',@$request->subscription_id)->first();
           
           if(@$check == null){

               session()->flash('error','something went wrong');
               return redirect()->back();
           }
           $data['package'] =$check;
           $data['page_type'] = @$request->page_type;
           if(@$check->type == 'M'){
                $data['amt1'] = @$check->monthly_price;
            }else if(@$check->type == 'Y'){
                $data['amt1'] = @$check->yearly_price;

            }

            $insert['user_id'] = Auth::user()->id;
            if(@$check->type == 'M'){
                $insert['amount'] = @$check->monthly_price;
            }else if(@$check->type == 'Y'){
                $insert['amount'] = @$check->yearly_price;

            }
            $insert['page_type'] = 'S';
            $data['payment_id'] = Payment::insertGetId($insert);

           return view('modules.razorpay.razorpayview_subs')->with($data);
        }
    }

     public function store(Request $request)
    {

        if(@$request->page_type != 'S'){

                $input = $request->all();
                $api = new Api(env('RAZORPAY_KEY'), env('RAZORPAY_SECRET'));         
                $payment = $api->payment->fetch($input['razorpay_payment_id']);
          
                if(count($input)  && !empty($input['razorpay_payment_id'])) {
                    try {
                        $response = $api->payment->fetch($input['razorpay_payment_id'])->capture(array('amount'=>$payment['amount'])); 

                        
                    if($response->status == 'captured'){

                        $insert2['txn_id'] = @$request->razorpay_payment_id;
                        $insert2['mode'] = 'RAZORPAY';
                        $insert2['status'] = 'S';
                        Payment::where('id',@$request->payment_id)->update($insert2);
                        
                        $insert['job_id'] = @$request->job_id;
                        $insert['bid_id'] = @$request->bid_id;
                        $insert['pro_id'] = @$request->pro_id;
                        $insert['user_id'] = @$request->user_id;
                        $insert['amount'] = @$request->amount;
                        $insert['status'] = 'F';
                        $insert['description'] = @$request->description;
                        $insert['razorpay_payment_id'] = @$request->razorpay_payment_id;
                        $insert['com_amount'] = @$request->com_amount;

                        $m_id = Milestone::insertGetId($insert); 
                        $prov = User::where('id',@$request->pro_id)->first();
                        $post = PostJob::where('id',@$request->job_id)->first();
                        $milestone = Milestone::where('id',@$m_id)->first();
                        $data['email'] = $prov->email;
                        $data['bidder_name'] = $prov->name;
                        $data['job_name'] = $post->job_name;
                        $data['user_name'] = Auth::user()->name;
                        $data['des'] = @$milestone->description;
                        $data['amount'] = @$request->com_amount;


                        Mail::send(new MilestoneCreateMail($data));

                        $insert1['milestone_id'] = $m_id;
                        $insert1['job_id'] = $request->job_id;
                        $insert1['amount'] = $request->amount;
                        $insert1['type'] = 'OUT';
                        $insert1['user_id'] = Auth::user()->id;
                        $insert1['category'] = 'MC';
                        UserTransaction::insertGetId($insert1);

                        if(@$request->com_amount){
                            $org_amount = @$request->com_amount - @$request->amount;
                            if($org_amount > 0){

                                $insert1['milestone_id'] = $m_id;
                                $insert1['job_id'] = $request->job_id;
                                $insert1['amount'] = $org_amount;
                                $insert1['type'] = 'OUT';
                                $insert1['user_id'] = Auth::user()->id;
                                $insert1['category'] = 'C';
                                UserTransaction::insertGetId($insert1);

                            }
                            
                        }
                    }
                        
          
                    } catch (Exception $e) {
                        return  $e->getMessage();
                        Session::put('error',$e->getMessage());
                        // return redirect()->back();
                        return redirect()->route('user.get.milestone',$request->bid_id);
                    }
                }
                  
                Session::put('success', 'Payment successful');
                // return redirect()->back();
                return  redirect()->route('user.get.milestone',$request->bid_id);

        }else if(@$request->page_type == 'S' && @$request->mem_type == 'Pro'){
                
                $input = $request->all();
                $api = new Api(env('RAZORPAY_KEY'), env('RAZORPAY_SECRET'));         
                $payment = $api->payment->fetch($input['razorpay_payment_id']);
          
                if(count($input)  && !empty($input['razorpay_payment_id'])) {
                    try {
                        $response = $api->payment->fetch($input['razorpay_payment_id'])->capture(array('amount'=>$payment['amount'])); 

                        
                    if($response->status == 'captured'){

                        $insert2['txn_id'] = @$request->razorpay_payment_id;
                        $insert2['mode'] = 'RAZORPAY';
                        $insert2['status'] = 'S';
                        Payment::where('id',@$request->payment_id)->update($insert2);
                        $data = ProPackage::where('id',$request->subscription_id)->first();
                        $update['user_id'] = Auth::user()->id;
                        $update['pro_package_id'] = @$data->id;
                        $update1['payment_freq'] = @$data->type;
                        if(@$data->type == 'M'){
                            $NewDate=Date('Y-m-d', strtotime('+30 days'));
                            $update1['package_expiry_date'] = $NewDate;
                        }else{
                            $NewDate=Date('Y-m-d', strtotime('+365 days'));
                            $update1['package_expiry_date'] = $NewDate; 
                        }
                        $chk = UserToPackage::where('user_id',Auth::user()->id)->first();
                        if($chk){
                            
                            $result = UserToPackage::where('id',@$chk->id)->update($update);
                            User::where('id',Auth::user()->id)->update($update1);
                        
                        }else{
                            
                            $result = UserToPackage::insert($update);
                            User::where('id',Auth::user()->id)->update($update1); 
                        }
                        

                        // if(@$update){

                        //     session()->flash('success','Your Package updated successfully');
                        //     return redirect()->back();
                        // }
                        


                        
                        
                    }
                        
          
                    } catch (Exception $e) {
                        return  $e->getMessage();
                        Session::put('error',$e->getMessage());
                        // return redirect()->back();
                        return redirect()->route('service.provider.membership');
                    }
                }
                  
                Session::put('success', 'Payment successful');
                // return redirect()->back();
                return  redirect()->route('service.provider.membership');



        }else if(@$request->page_type == 'S'){


                $input = $request->all();
                $api = new Api(env('RAZORPAY_KEY'), env('RAZORPAY_SECRET'));         
                $payment = $api->payment->fetch($input['razorpay_payment_id']);
          
                if(count($input)  && !empty($input['razorpay_payment_id'])) {
                    try {
                        $response = $api->payment->fetch($input['razorpay_payment_id'])->capture(array('amount'=>$payment['amount'])); 

                        
                    if($response->status == 'captured'){

                        $insert2['txn_id'] = @$request->razorpay_payment_id;
                        $insert2['mode'] = 'RAZORPAY';
                        $insert2['status'] = 'S';
                        Payment::where('id',@$request->payment_id)->update($insert2);
                        $check = AgentMemberShipPackage::where('id',@$request->subscription_id)->first();
           
                        if(@$check == null){

                               session()->flash('error','something went wrong');
                               return redirect()->back();
                        }
                        $upd = [];
                        if(@$check->type == 'M'){

                               $upd['payment_freq'] = 'M';
                               $startDate = date('Y-m-d');
                               $upd['package_expiry_date'] = date('Y-m-d',strtotime(date('Y-m-d',strtotime($startDate))."+30 day"));
                        }
                        else if(@$check->type == 'Y'){

                            $upd['payment_freq'] = 'Y';
                            $startDate = date('Y-m-d');
                            $upd['package_expiry_date'] = date('Y-m-d',strtotime(date('Y-m-d',strtotime($startDate))."+365 day"));

                       }
                        User::where('id',Auth::user()->id)->update($upd);
           
                        $update =  UserToPackage::where('user_id',Auth::user()->id)->update(['agent_package_id'=>@$check->id]);

                        // if(@$update){

                        //     session()->flash('success','Your Package updated successfully');
                        //     return redirect()->back();
                        // }
                        


                        
                        
                    }
                        
          
                    } catch (Exception $e) {
                        return  $e->getMessage();
                        Session::put('error',$e->getMessage());
                        // return redirect()->back();
                        return redirect()->route('user.get.milestone',$request->bid_id);
                    }
                }
                  
                Session::put('success', 'Payment successful');
                // return redirect()->back();
                return  redirect()->route('agent.subcription');



        }
        

    }





}

<?php

namespace App\Http\Controllers\Admin\Modules\Language;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use App\Models\Language;
use App\Models\ProviderToLanguage;

class LanguageController extends Controller
{
    public function __construct(){

        return $this->middleware('admin.auth:admin');
  }

   /**
     *   Method      : index
     *   Description : listing and searching language
     *   Author      : sudipta
     *   Date        : 2021-OCT-26
     **/

  public function index(Request $request){
           
    if($request->all()){
     $data['language'] = Language::where('status','A');
       if($request->language){

         $data['language'] = $data['language']->where(function($q) use($request){

                $q->where('name','like','%'.$request->language.'%');
         });
       }

       $data['language'] = $data['language']->orderBy('id','desc')->paginate(10);
       return view('admin.modules.language.list_language',$data);
    }
    $data['language'] = Language::where('status','A')->orderBy('id','desc')->paginate(10);
    return view('admin.modules.language.list_language',$data);
}

 /**
     *   Method      : addLanguage
     *   Description : add language
     *   Author      : sudipta
     *   Date        : 2021-OCT-26
     **/

public function addLanguage(Request $request){

    if($request->all()){

         $category['name'] = $request->language;
         $category['status'] = 'A';

         $save = Language::create($category);

         if($save){

              session()->flash('success','Language added successfully!');
              return redirect()->back();
         }else{

            session()->flash('error','Something went wrong');
            return redirect()->back();
         }
    }
    return view('admin.modules.language.add_language');
}

 /**
     *   Method      : checkLanguage
     *   Description : duplicate checking
     *   Author      : sudipta
     *   Date        : 2021-OCT-26
     **/

public function checkLanguage(Request $request){

    if($request->language){

          $checkSkill = Language::where('status','A')->where('name',$request->language)->where('status','A')->count();

          if($checkSkill>0){

                return response('false');
          }else{

               return response('true');
          }
    }

    return response('no language');
}

 /**
     *   Method      : editLanguage
     *   Description : edit language
     *   Author      : sudipta
     *   Date        : 2021-OCT-26
     **/

public function editLanguage(Request $request,$id=null){

    if($id!=''){

           if($request->all()){

            $check = Language::where('id','!=',$id)->where('name',$request->language)->first();
            $language = Language::where('id',$id)->where('status','A')->first(); 
              if($check){
                    
                    session()->flash('error','This language name already taken.please enter another language');
                    return redirect()->back();
              }

                 $update['name'] = $request->language;

                 $save = Language::where('id',$id)->update($update);

                 if($save){

                      session()->flash('success','Language update successfully!');
                      return redirect()->back();
                 }else{
                    $data['language'] = $language;
                    session()->flash('error','Something went wrong');
                    return redirect()->back()->with($data);  
                      
                 }
           }
           
           $data['language'] = Language::where('id',$id)->where('status','A')->first();

           return view('admin.modules.language.edit_language',$data);
    }
}

 /**
     *   Method      : deleteLanguage
     *   Description : delete a language
     *   Author      : sudipta
     *   Date        : 2021-OCT-26
     **/

public function deleteLanguage($id=null){

    $check= Language::where('id',$id)->first();
    if($check==null){
        session()->flash('error','Something went wrong');
        return redirect()->route('admin.manage.language');
    }
    $user_language_check=ProviderToLanguage::where('language_id',$id)->count();
    if(@$user_language_check>0)
    {
        session()->flash('error','Language can not be deleted as it is associated with a user.');
             return redirect()->route('admin.manage.language');
    }
    Language::where('id',$id)->update(['status'=>'D']);
    session()->flash('success','Language deleted successfully');
    return redirect()->route('admin.manage.language');
}


}

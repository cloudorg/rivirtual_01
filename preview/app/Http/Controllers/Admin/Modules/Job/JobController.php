<?php

namespace App\Http\Controllers\Admin\Modules\Job;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Auth;
use App\User;
use App\Country;
use Storage;
use App\Models\Category;
use App\Models\Language;
use App\Models\Skill;
use App\Models\ProviderToLanguage;
use App\Models\ProviderToSkills;
use App\Models\ProviderToImage;
use App\Models\ProToCategory;
use App\Models\SubCategory;
use App\Models\City;
use App\Models\State;
use App\Models\PostJob;
use App\Models\Quotes;
use App\Models\Property;
use App\Models\JobToCategory;
use App\Models\JobToProReview;
use App\Models\JobToImage;
use App\Models\Milestone;
use Mail;
use App\Mail\JobApprovalMail;
class JobController extends Controller
{
    public function __construct(){

        return $this->middleware('admin.auth:admin');
    }

    public function manageJob(Request $request){
    $data['category'] = Category::where('status','A')->get();
    $data['states'] = State::where('country_id',101)->get();
    $data['cites'] = City::orderBy('name','ASC')->get();
    $data['maxPrice'] = PostJob::orderBy('budget_range_from','desc')->first();
    if($request->all()){
     $data['jobs'] = PostJob::with(['getUser','categoryName','subCategoryName'])->where('status','!=','D')->orderBy('id','desc');

      if($request->keyword){

          $data['jobs'] = $data['jobs']->where(function($q) use($request){

               $q->where('job_name','like','%'.$request->keyword.'%')
               ->orwhere('slug','like','%'.str_slug($request->keyword).'%');
          })->orWhereHas('getUser', function($q) use($request){

               $q->where('name','like','%'.$request->keyword.'%');
          });
      }
      if($request->location){

          $data['jobs'] = $data['jobs']->where(function($q) use($request){

              $q->where('address','like','%'.$request->location.'%');
          });
      }
      if($request->state){

        $data['jobs'] = $data['jobs']->where(function($q) use($request){

            $q->where('state',$request->state);
        });
      }
      if($request->city){

        $data['jobs'] = $data['jobs']->where(function($q) use($request){

            $q->where('city',$request->city);
        });
      }

      if($request->category){

          $data['jobs'] = $data['jobs']->where(function($q) use($request){

              $q->where('category',$request->category);
          });
      }
     
     if($request->from_date){

         $data['jobs'] = $data['jobs']->where(function($q) use($request){

             $q->where(DB::raw("DATE(created_at)") ,'>=',date('Y-m-d',strtotime($request->from_date)));
       });
     }
     if($request->to_date){

         $data['jobs'] = $data['jobs']->where(function($q) use($request){

             $q->where(DB::raw("DATE(created_at)") ,'<=',date('Y-m-d',strtotime($request->to_date)));
       });

     }
     

     if($request->status){

        $data['jobs'] = $data['jobs']->where(function($q) use($request){

             $q->where('status','like','%'.$request->status.'%');
        });
     }
     if($request->rivirtual_assist){

        $data['jobs'] = $data['jobs']->where(function($q) use($request){

            $q->where('rivirtual_assistance',$request->rivirtual_assist);
        });
      }

     if($request->budget_type){

          $data['jobs'] = $data['jobs']->where(function($q) use($request){

              $q->where('budget_type','like','%'.$request->budget_type.'%');
          });
     }
     if($request->amount1!=null && $request->amount2!=null ){
        $data['jobs']=$data['jobs']->whereBetween('budget_range_from',[$request->amount1 , $request->amount2]);
    }
     
    $data['key'] = $request->all();
    

     $data['jobs'] = $data['jobs']->paginate(10);
     return view('admin.modules.job.manage_job',$data);

    }
    

        $data['jobs'] = PostJob::with('getUser')->where('status','!=','D')->orderBy('id','desc')->paginate(10);

        return view('admin.modules.job.manage_job',$data);
    }

    public function jobStatusUpdate(Request $request){

        $existjob =  PostJob::where('id',$request->id)->whereIn('status',['A','I'])->first();
        if($existjob){
             if($existjob->status == 'A'){
    
                 $update['status'] = 'I';
             }
             else if($existjob->status == 'I'){
    
               $update['status'] = 'A';
             }
             $existjob->update($update);
    
             $jobupdate = PostJob::where('id',$request->id)->where('status', '!=','D')->first();
    
           return response()->json($jobupdate);
        }
    }

    public function jobDetails($id=null){

          $data['jobDetails'] = PostJob::where('id',$id)->first();
          $data['review'] = JobToProReview::where('job_id',$data['jobDetails']->id)->first(); 
          $data['job_image'] = JobToImage::where('post_id',$data['jobDetails']->id)->first();
          return view('admin.modules.job.job_details')->with($data);
    }

    public function deleteJob($id=null){

         
        $check= PostJob::where('id',$id)->first();
        if($check==null){
            session()->flash('error','Something went wrong');
            return redirect()->route('admin.manage.job');
        }

        PostJob::where('id',$id)->update(['status'=>'D']);
        session()->flash('success','Job deleted successfully');
        return redirect()->route('admin.manage.job');
    }

    public function jobProposalList(Request $request,$id=null){

         $data['jobId'] = $id;
         $data['job_name'] = PostJob::where('id',$id)->first();
         $allProposal = Quotes::with(['proDetails','jobDetails'])->where('post_id',$id);
         $allProposal = $allProposal->orderBy('id','desc')->get();
         $data['allProposal'] = $allProposal;
         return view('admin.modules.job.job_proposal_list')->with($data);
    }

    /**
     *   Method      : editJob
     *   Description : admin edit job
     *   Author      : Puja
     *   Date        : 2021-DEC-15
     **/

    public function editJob($id){
        $data['category'] = Category::where('status','A')->get();
        $data['states'] = State::where('country_id',101)->get();
        $data['cites'] = [];
        $data['allSubCategory']= [];

        $data['postDetails']=PostJob::where('id',$id)->first();
       
        if($data['postDetails']==null){
            session()->flash('error', 'Something went wrong');
            return redirect()->back();
        }
       $data['postDetails']  = PostJob::where('id',$id)->first();

       $data['cites'] = City::where('state_id',$data['postDetails']->state)->get();
       $data['allSubCategory'] = SubCategory::where('category_id',$data['postDetails']->category)->get();
       $data['jobImages'] = JobToImage::where('post_id',$data['postDetails']->id)->get();
     // $data['allCountry'] = Country::all();
    // $data['states'] = State::where('country_id',101)->orderBy('name','ASC')->get();
     
    // $data['cites'] = City::orderBy('name','ASC')->get();
    
       return view('admin.modules.job.edit_job',$data);


    }


    /**
     *   Method      : addPostJobSave
     *   Description : for user post job save
     *   Author      : Puja
     *   Date        : 2021-DEC-15
     **/

    public function addPostJobSave(Request $request,$id=null){
        
          $request->validate([

               'job_title'=>'required',
               'category'=>'required',
               'day'=>'required',
               'start_date'=>'required',
               'time'=>'required',
               //'budget_type'=>'required',
               //'budget'=>'required',
               'address'=>'required',
               'description'=>'required'
          ]);

          
          $postDetails=PostJob::where('id',$id)->first();
          if($postDetails==null){
              session()->flash('error', 'Something went wrong');
              return redirect()->back();
          }
          if(@$request->start_date && (date('Y-m-d',strtotime(@$request->start_date))) < (date('Y-m-d'))){
            session()->flash('error', 'Please insert valid date');
            return redirect()->back();
        }
          $upd = [];
          $upd['job_name'] = $request->job_title;
          $upd['category'] = $request->category;
          $upd['sub_category'] = $request->sub_category;
          $upd['duration'] = $request->day;
          $upd['job_start_date'] = date('Y-m-d',strtotime($request->start_date));
          $upd['budget_range_from'] = $request->budget_range_from;
          $upd['budget_range_to'] = $request->budget_range_to;
          $upd['time'] = date('H:i:s',strtotime($request->time));
          $upd['address'] = $request->address;
          $upd['country'] = $request->country;
          $upd['state'] = $request->state;
          $upd['city'] = $request->city;
          $upd['description'] = $request->description;
          $slug = str_slug($request->job_title,"-");
          $upd['slug'] = $slug."-".@$postDetails->id;
          $upd['status'] = 'A';

        //   if (@$request->service_image) {
        //     // $image = $request->profile_pic;
        //      // $filename = time() . '-' . rand(1000, 9999) . '.' . $image->getClientOriginalExtension();
        //      // Storage::putFileAs('public/profile_picture', $image, $filename);
        //      // $upd['profile_img'] = $filename;
        //      // @unlink(storage_path('app/public/profile_picture/' . auth()->user()->profile_img));
        //      @unlink(storage_path('app/public/service_image/' . $postDetails->image));
        //      $destinationPath = "storage/app/public/service_image/";
        //      $img1 = str_replace('data:image/png;base64,', '', @$request->service_image);
        //      $img1 = str_replace(' ', '+', $img1);
        //      $image_base64 = base64_decode($img1);
        //      $img = time() . '-' . rand(1000, 9999) . '.png';
        //      $file = $destinationPath . $img;
        //      file_put_contents($file, $image_base64);
        //      chmod($file, 0755);
        //      $upd['image'] = $img;
        //  }

        $files1 = $request->file('file-1');
        if($request->hasFile('file-1'))
        {
            foreach ($files1 as $file) {
                $filename = time() . '-' . rand(1000, 9999) . '.' . $file->getClientOriginalExtension();
                Storage::putFileAs('public/service_image', $file, $filename);
                $insImage=[];
                $insImage['image']=$filename;
                $insImage['post_id']=@$postDetails->id;
                
                JobToImage::create($insImage);
            }
        }

         $updated=PostJob::where('id',$postDetails->id)->update($upd);
        if(@$updated){
            session()->flash('success', 'Post service details updated successfully');
            return redirect()->back();
        }
        session()->flash('error', 'Post service details not updated');
        return redirect()->back();
    }

    public function jobImageRemove($id=null){

        $insImage=JobToImage::where('id',$id)->first();
        if($insImage==null){
            session()->flash('error', 'Something went wrong image not removed');
            return redirect()->back();
        }
        $postDetails=PostJob::where('id',$insImage->post_id)->first();
        if($postDetails==null){
            session()->flash('error', 'Something went wrong image not removed');
            return redirect()->back();
        }
  
        $image= JobToImage::where('post_id',$insImage->post_id)->count();
  
        if($image==1){
          session()->flash('error', 'Image not removed minimum one image require');
          return redirect()->back();
      }
      @unlink(storage_path('app/public/service_image/' .$insImage->image));
      JobToImage::where('id', $insImage->id)->delete();
      session()->flash('success', 'Job mage removed');
      return redirect()->back();
    }

    public function saveRivirtualAssist(Request $request){

           $upd = [];
           $upd['assistance_name'] = $request->assist_name;
           $upd['assistance_phone'] = $request->assist_phone;
           $upd['assistance_email'] = $request->assist_email;

           $save = PostJob::where('id',$request->postId)->update($upd);

           if($save){

               session()->flash('success','Assistance added successfully');
                return redirect()->route('admin.manage.job');
           }else{

            session()->flash('error','something went wrong');
            return redirect()->route('admin.manage.job');
           }

    }

    public function jobBidRecommended($id=null){

        $check= Quotes::where('id',$id)->first();
        if($check==null){
            session()->flash('error','Something went wrong');
            return redirect()->back();
        }

        if(@$check->is_recommended == 0){

              $update['is_recommended'] = 1;
              session()->flash('success','Bid recommended successfully');
        }else{

            $update['is_recommended'] = 0;
            session()->flash('success','Bid recommended cancel successfully');
        }

        Quotes::where('id',$id)->update($update);
       
        return redirect()->back();
    }

    public function viewMilestone(Request $request,$id=null){

        $check= PostJob::where('id',$id)->first();
        if($check==null){
            session()->flash('error','Something went wrong');
            return redirect()->route('admin.manage.job');
        }
        $data['postId'] = $id;
        $data['bidDetails'] = Quotes::with('proDetails')->where('post_id',$id)->where('status','Awarded')->first();
        //return $data['bidDetails'];
        $data['allMilestone'] = Milestone::with(['userDetails','proDetails'])->where('job_id',$id);
        if($request->all()){

               if($request->status){
                
                $data['allMilestone'] = $data['allMilestone']->where(function($q) use($request){

                      $q->where('status','like','%'.$request->status.'%');
                });
                   
               }
        }
        $data['allMilestone'] = $data['allMilestone']->orderBy('id','desc')->get();
        //return $data['allMilestone'];
        return view('admin.modules.job.view_milestone')->with($data);
    }
    public function jobDeleteAll(Request $request){

        if($request->status && $request->allid){
    
            PostJob::whereIn('id',$request->allid)->update(['status'=>'I']);
            session()->flash('success','Job inactive successfully');
            return redirect()->route('admin.manage.job');
        }
        else{
    
            PostJob::whereIn('id',$request->allid)->update(['status'=>'D']);
            session()->flash('success','Job deleted successfully');
            return redirect()->route('admin.manage.job');
        }
           
           session()->flash('error','Something went wrong.please select at least one job.');
           return redirect()->route('admin.manage.job');
     }
     
     public function jobApproval(Request $request){

           $jobcheck = PostJob::where('id',$request->id)->where('status','!=','D')->first();

           if(@$jobcheck){

                 $upd['approval_by_admin'] = 'Y';
           }

           PostJob::where('id',@$jobcheck->id)->update($upd);
           $upd_data = PostJob::with('getUser')->where('id',@$jobcheck->id)->first();

           $data['email'] = @$upd_data->getUser->email;
           $data['name'] = @$upd_data->getUser->name;
           $data['job_name'] = @$upd_data->job_name;
           $send_job_live_sms = $this->sendJobLiveSms(@$upd_data->getUser->mobile_number,@$upd_data->job_name,@$upd_data->id);
           
           Mail::send(new JobApprovalMail($data));
           $response = array();
           if(@$upd_data){

               $response['data']['message'] = 'Job approved successfully';
           }
           
           return response()->json($response);
     }
     
    public function sendJobLiveSms($number,$job_name,$job_id){
        $apiKey = urlencode('NDEzODdhNjM0YTc3NDk2OTQ5MzI3OTY5NmI0YzM4NmY=');
        $num = '91'.$number;
        $numbers = urlencode($num); 
        $sender = urlencode('ITREAL');
        // $message = 'Use '.$otp.' OTP to verify your mobile number.';
        $message = rawurlencode('Your Job id '.@$job_id.'- '.@$job_name.' is live! RIVirtual and iTester Team');
          
        $data = 'apikey=' . $apiKey . '&numbers=' . $numbers . "&sender=" . $sender . "&message=" . $message;
        
        $ch = curl_init('https://api.textlocal.in/send/?' . $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        
        return $response;

    }
}

<?php

namespace App\Http\Controllers\Admin\Modules\Payment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Models\UserTransaction;
class PaymentController extends Controller
{
    public function __construct(){

        return $this->middleware('admin.auth:admin');
  }

    public function index(){
          $data['allPaymentData'] = User::where('user_type','S')->where('tot_due','>',0.00)->get();
          //return  $data['allPaymentData'];
          return view('admin.modules.payment.pro_payment_list')->with($data);
    }

     public function proSavePayment(Request $request){

          $request->validate([

              'amount'=>'required',
              'description'=>'required'
          ]);

           $ins = [];
           $ins['user_id'] = $request->proId;
           $ins['amount'] = $request->amount;
           $ins['description'] = $request->description;
           $ins['type'] = 'OUT';
           $ins['category'] = 'PA';

           $create = UserTransaction::create($ins);
           if($create){

               $pro_tot_paid = User::where('id',$request->proId)->value('tot_paid');
               $pro_tot_due = User::where('id',$request->proId)->value('tot_due');
               $upd = [];
               $upd['tot_paid'] = $pro_tot_paid + $request->amount;
               $upd['tot_due'] = $pro_tot_due - $request->amount;

               User::where('id',$request->proId)->update($upd);

               session()->flash('success','Payment added successfully');
               return redirect()->back();

           }
           session()->flash('error','Something went wrong');
           return redirect()->back();

     }
}

<?php

namespace App\Http\Controllers\Admin\Modules\Category;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Category;
use App\User;
use App\Models\ProToCategory;
class CategoryController extends Controller
{
    public function __construct(){

          return $this->middleware('admin.auth:admin');
    }

     /**
     *   Method      : index
     *   Description : listing and searching  category
     *   Author      : sudipta
     *   Date        : 2021-OCT-26
     **/

    public function index(Request $request){
           
           if($request->all()){
            $data['category'] = Category::where('status','A');
              if($request->category){

                $data['category'] = $data['category']->where(function($q) use($request){

                       $q->where('category_name','like','%'.$request->category.'%');
                });
              }

              $data['category'] = $data['category']->orderBy('id','desc')->paginate(10);
              return view('admin.modules.category.list_category',$data);
           }
           $data['category'] = Category::where('status','A')->orderBy('id','desc')->paginate(10);
           return view('admin.modules.category.list_category',$data);
    }

     /**
     *   Method      : addCategory
     *   Description : add category
     *   Author      : sudipta
     *   Date        : 2021-OCT-26
     **/

    public function addCategory(Request $request){

            if($request->all()){

                 $category['category_name'] = $request->category;
                 $category['slug'] = str_slug($request->category);
                 $category['status'] = 'A';

                 $save = Category::create($category);

                 if($save){

                      session()->flash('success','Category added successfully!');
                      return redirect()->back();
                 }else{

                    session()->flash('error','Something went wrong');
                    return redirect()->back();
                 }
            }
            return view('admin.modules.category.add_category');
    }

     /**
     *   Method      : checkCategory
     *   Description : duplicate checking
     *   Author      : sudipta
     *   Date        : 2021-OCT-26
     **/

    public function checkCategory(Request $request){

      if($request->category){
  
            $checkSkill = Category::where('status','A')->where('category_name',$request->category)->count();
  
            if($checkSkill>0){
  
                  return response('false');
            }else{
  
                 return response('true');
            }
      }
  
      return response('no category');
  }

   /**
     *   Method      : editCategory
     *   Description : edit category
     *   Author      : sudipta
     *   Date        : 2021-OCT-26
     **/

    public function editCategory(Request $request,$id=null){

            if($id!=''){

                   if($request->all()){

                    $check = Category::where('id','!=',$id)->where('category_name',$request->category)->first();
                    $category = Category::where('id',$id)->where('status','A')->first(); 
                      if($check){
                            
                            session()->flash('error','This category name already taken.please enter another category');
                            return redirect()->back();
                      }

                         $update['category_name'] = $request->category;
                         $update['slug'] = str_slug($request->category);

                         $save = Category::where('id',$id)->update($update);

                         if($save){

                              session()->flash('success','Category update successfully!');
                              return redirect()->back();
                         }else{
                            $category['category'] = $category;
                            session()->flash('error','Something went wrong');
                            return redirect()->back()->with($category);  
                              
                         }
                   }
                   
                   $data['category'] = Category::where('id',$id)->where('status','A')->first();

                   return view('admin.modules.category.edit_category',$data);
            }
    }

     /**
     *   Method      : deleteCategory
     *   Description : delete a category
     *   Author      : sudipta
     *   Date        : 2021-OCT-26
     **/

    public function deleteCategory($id=null){

        $check= Category::where('id',$id)->first();
        if($check==null){
            session()->flash('error','Something went wrong');
            return redirect()->route('admin.manage.category');
        }
        $user_category_check=ProToCategory::where('category_id',$id)->where('master_id','=',0)->count();
		if(@$user_category_check>0)
		{
			session()->flash('error','Category can not be deleted as it is associated with a user.');
                 return redirect()->route('admin.manage.category');
		}

        Category::where('id',$id)->update(['status'=>'D']);
        session()->flash('success','Category deleted successfully');
        return redirect()->route('admin.manage.category');
    }

    /**
     *   Method      : makePopular
     *   Description : make popular a category
     *   Author      : Puja
     *   Date        : 2022-JAN-25
     **/
    public function makePopular($id=null){

        $check= Category::where('id',$id)->first();
        if($check==null){
            session()->flash('error','Something went wrong');
            return redirect()->route('admin.manage.category');
        }
        if($check->make_popular != 'Y' ){
            $update['make_popular'] = 'Y';
        }else{
            $update['make_popular'] = 'N';
        }


        Category::where('id',$id)->update($update);
        session()->flash('success','Category added in Popular list successfully');
        return redirect()->route('admin.manage.category');

    }
}

<?php

namespace App\Http\Controllers\Admin\Modules\SubCategory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Auth;
use App\Models\Category;
use App\Models\SubCategory;
use Storage;
class SubCategoryController extends Controller
{
    public function __construct(){

         return $this->middleware('admin.auth:admin');
    }

     /**
     *   Method      : index
     *   Description : listing and searching sub category
     *   Author      : sudipta
     *   Date        : 2021-DEC-07
     **/

    public function index(Request $request){

          $data['category'] = Category::where('status','A')->get();
          if($request->all()){
               $data['subcategories'] = SubCategory::with('categoryName')->where('status','A');

               if($request->sub_category){

                    $data['subcategories'] = $data['subcategories']->where(function($q) use($request){

                          $q->where('name','like','%'.$request->sub_category.'%')
                          ->orWhere('slug','like','%'.str_slug($request->sub_category).'%');
                    });  
               }
               if($request->category){

                    $data['subcategories'] = $data['subcategories']->where(function($q) use($request){

                          $q->where('category_id',$request->category);
                    });
               }
                if($request->popular){

                    $data['subcategories'] = $data['subcategories']->where(function($q) use($request){

                          $q->where('make_popular',$request->popular);
                    });
               }
               
          $data['subcategories'] = $data['subcategories']->orderBy('id','desc')->paginate(10);

          return view('admin.modules.sub_category.sub_category_list')->with($data);
               
          }
          $data['subcategories'] = SubCategory::with('categoryName')->where('status','A')->orderBy('id','desc')->paginate(10);

          return view('admin.modules.sub_category.sub_category_list')->with($data);
    }

     /**
     *   Method      : addSubCategory
     *   Description : for add sub category
     *   Author      : sudipta
     *   Date        : 2021-DEC-07
     **/

    public function addSubCategory(Request $request){
        $data['category'] = Category::where('status','A')->get();

        if($request->all()){
             $category['category_id'] = $request->category;
             $category['name'] = $request->sub_category;
             $category['make_popular'] = 'N';
             $category['status'] = 'A';
              
             if (@$request->category_image) {
                // $image = $request->profile_pic;
                 // $filename = time() . '-' . rand(1000, 9999) . '.' . $image->getClientOriginalExtension();
                 // Storage::putFileAs('public/profile_picture', $image, $filename);
                 // $upd['profile_img'] = $filename;
                 // @unlink(storage_path('app/public/profile_picture/' . auth()->user()->profile_img));
                // @unlink(storage_path('app/public/profile_picture/' . $existuser->profile_pic));
                 $destinationPath = "storage/app/public/category_image/";
                 $img1 = str_replace('data:image/png;base64,', '', @$request->category_image);
                 $img1 = str_replace(' ', '+', $img1);
                 $image_base64 = base64_decode($img1);
                 $img = time() . '-' . rand(1000, 9999) . '.png';
                 $file = $destinationPath . $img;
                 file_put_contents($file, $image_base64);
                 chmod($file, 0755);
                 $category['image'] = $img;
             }
             $create = SubCategory::create($category);

             if($create){
                  $upd = [];
                  $slug = str_slug($request->sub_category,"-");
                  $upd['slug'] = $slug."-".$create->id;
                  SubCategory::where('id',$create->id)->update($upd);
                  session()->flash('success','Sub category added successfully!');
                  return redirect()->back();
             }else{

                session()->flash('error','Something went wrong');
                return redirect()->back();
             }
        }
        return view('admin.modules.sub_category.add_sub_category')->with($data);
}

 /**
     *   Method      : checkSubCategory
     *   Description : for duplicate checking sub category
     *   Author      : sudipta
     *   Date        : 2021-DEC-07
     **/
 
public function checkSubCategory(Request $request){

    if($request->category){

          $checkSkill = SubCategory::where('status','A')->where('name',$request->category)->count();

          if($checkSkill>0){

                return response('false');
          }else{

               return response('true');
          }
    }

    return response('no sub category');
}

/**
     *   Method      : editSubCategory
     *   Description : for edit  sub category
     *   Author      : sudipta
     *   Date        : 2021-DEC-07
     **/
  
  public function editSubCategory(Request $request,$id=null){
      $data['category'] =  Category::where('status','A')->get();
       if($id != null){

          if($request->all()){

               $check = SubCategory::where('id','!=',$id)->where('name',$request->sub_category)->where('status','A')->first();
               $subcategory = SubCategory::where('id',$id)->where('status','A')->first(); 
                 if($check){
                       
                       session()->flash('error','This sub category name already taken.please enter another sub category');
                       return redirect()->back();
                 }

                    $update['category_id'] = $request->category;
                    $update['name'] = $request->sub_category;
                    $slug = str_slug($request->sub_category,"-");
                    $update['slug'] = $slug."-".$subcategory->id;
                     
                    if (@$request->category_image) {
                         // $image = $request->profile_pic;
                          // $filename = time() . '-' . rand(1000, 9999) . '.' . $image->getClientOriginalExtension();
                          // Storage::putFileAs('public/profile_picture', $image, $filename);
                          // $upd['profile_img'] = $filename;
                          // @unlink(storage_path('app/public/profile_picture/' . auth()->user()->profile_img));
                         @unlink(storage_path('app/public/category_image/' . $subcategory->image));
                          $destinationPath = "storage/app/public/category_image/";
                          $img1 = str_replace('data:image/png;base64,', '', @$request->category_image);
                          $img1 = str_replace(' ', '+', $img1);
                          $image_base64 = base64_decode($img1);
                          $img = time() . '-' . rand(1000, 9999) . '.png';
                          $file = $destinationPath . $img;
                          file_put_contents($file, $image_base64);
                          chmod($file, 0755);
                          $update['image'] = $img;
                      }


                    $save = SubCategory::where('id',$id)->update($update);

                    if($save){

                         session()->flash('success','Sub category update successfully!');
                         return redirect()->back();
                    }else{
                       $data['subcategory'] = $subcategory;
                       session()->flash('error','Something went wrong');
                       return redirect()->back()->with($data);  
                         
                    }
              }
              
              $data['subcategory'] = SubCategory::where('id',$id)->where('status','A')->first();

              return view('admin.modules.sub_category.edit_sub_category')->with($data);
       }
  }

  /**
     *   Method      : deleteSubCategory
     *   Description : for delete  sub category
     *   Author      : sudipta
     *   Date        : 2021-DEC-07
     **/

  public function deleteSubCategory($id=null){

     $check= SubCategory::where('id',$id)->first();
     if($check==null){
         session()->flash('error','Something went wrong');
         return redirect()->route('admin.manage.sub.category');
     }
     // $user_category_check=User::where('category_id',$id)->count();
     //   if(@$user_category_check>0)
     //   {
     //        session()->flash('error','Category can not be deleted as it is associated with a user.');
     //          return redirect()->route('admin.manage.category');
     //   }

     SubCategory::where('id',$id)->update(['status'=>'D']);
     session()->flash('success','Sub category deleted successfully');
     return redirect()->route('admin.manage.sub.category');
 }
  public function makePopular($id=null){

     $check= SubCategory::where('id',$id)->first();
     if($check==null){
         session()->flash('error','Something went wrong');
         return redirect()->route('admin.manage.sub.category');
     }
     if($check->make_popular != 'Y' ){
         $update['make_popular'] = 'Y';
     }else{
         $update['make_popular'] = 'N';
     }


     SubCategory::where('id',$id)->update($update);
     session()->flash('success','sub category added in Popular list successfully');
     return redirect()->route('admin.manage.sub.category');

 }
}

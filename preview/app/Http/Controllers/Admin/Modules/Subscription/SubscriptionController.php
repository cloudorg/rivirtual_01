<?php

namespace App\Http\Controllers\Admin\Modules\Subscription;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Auth;
use App\User;
use App\Country;
use Storage;
use App\Models\Category;
use App\Models\Language;
use App\Models\Skill;
use App\Models\ProviderToLanguage;
use App\Models\ProviderToSkills;
use App\Models\ProviderToImage;
use App\Models\ProToCategory;
use App\Models\SubCategory;
use App\Models\City;
use App\Models\State;
use App\Models\PostJob;
use App\Models\Quotes;
use App\Models\Property;
use App\Models\AgentMemberShipPackage;
use App\Models\ProPackage;



class SubscriptionController extends Controller
{
    public function __construct(){

        return $this->middleware('admin.auth:admin');
    }

    public function manageSubscription(Request $request){
        if(@$request->all()){
            $data["subscription"] = AgentMemberShipPackage::where('package_name','like','%'.@$request->keyword.'%')->get();

            $data['key'] = @$request->all();

        }else{
            $data["subscription"] = AgentMemberShipPackage::get();
        }
        
    
        return view('admin.modules.subscription.manage_subscription',$data);
    }

    public function changeStatus(Request $request){

    $ex_sub =  AgentMemberShipPackage::where('id',$request->id)->whereIn('status',['A','I'])->first();
        if($ex_sub){
            if($ex_sub->status == 'A'){

                 $update['status'] = 'I';
            }
            else if($ex_sub->status == 'I'){

               $update['status'] = 'A';
            }
            $ex_sub->update($update);

            $sub_update = AgentMemberShipPackage::where('id',$request->id)->first();

           return response()->json($sub_update);
        }
    }

    public function editSubscription($id){
        $data['subscription'] = AgentMemberShipPackage::where('id',$id)->first();

        return view('admin.modules.subscription.edit_subscription',$data);
    }

    public function saveSubscription(Request $request){

        $update['monthly_price'] = @$request->monthly_price;
        $update['yearly_price'] = @$request->yearly_price;
        $update['package_desc'] = @$request->description;
        $result = AgentMemberShipPackage::where('id',@$request->id)->update($update);

        if($result){

                session()->flash('success','Price updated successfully!');
                return redirect()->back();
        }else{

               session()->flash('error','Something went wrong');
               return redirect()->back();
        }

    }

    public function manageProviderSubscription(Request $request){
        if(@$request->all()){
            $data["pro_package"] = ProPackage::where('package_name','like','%'.@$request->keyword.'%')->get();

            $data['key'] = @$request->all();

        }else{
            $data["pro_package"] = ProPackage::get();
        }
        
        return view('admin.modules.subscription.manage_provider_subscription',$data);
    } 

    public function changePackageStatus(Request $request){

        $ex_sub =  ProPackage::where('id',$request->id)->whereIn('status',['A','I'])->first();
        if($ex_sub){
            if($ex_sub->status == 'A'){

                 $update['status'] = 'I';
            }
            else if($ex_sub->status == 'I'){

               $update['status'] = 'A';
            }
            $ex_sub->update($update);

            $sub_update = ProPackage::where('id',$request->id)->first();

           return response()->json($sub_update);
        }

    }

    public function changeTypeStatus(Request $request){

        $ex_sub =  ProPackage::where('id',$request->id)->whereIn('status',['A','I'])->whereIn('type',['Y','M'])->first();
        if($ex_sub){
            if($ex_sub->type == 'Y'){

                 $update['type'] = 'M';
            }
            else if($ex_sub->type == 'M'){

               $update['type'] = 'Y';
            }
            $ex_sub->update($update);

            $sub_update = ProPackage::where('id',$request->id)->first();

           return response()->json($sub_update);
        }

    } 

    public function changeSubStatus(Request $request){

        $ex_sub =  AgentMemberShipPackage::where('id',$request->id)->whereIn('status',['A','I'])->first();
        if($ex_sub){
            if($ex_sub->status == 'A'){

                 $update['status'] = 'I';
            }
            else if($ex_sub->status == 'I'){

               $update['status'] = 'A';
            }
            $ex_sub->update($update);

            $sub_update = AgentMemberShipPackage::where('id',$request->id)->first();

           return response()->json($sub_update);
        }

    }

    public function changeSubTypeStatus(Request $request){

        $ex_sub =  AgentMemberShipPackage::where('id',$request->id)->whereIn('status',['A','I'])->whereIn('type',['Y','M'])->first();
        if($ex_sub){
            if($ex_sub->type == 'Y'){

                 $update['type'] = 'M';
            }
            else if($ex_sub->type == 'M'){

               $update['type'] = 'Y';
            }
            $ex_sub->update($update);

            $sub_update = AgentMemberShipPackage::where('id',$request->id)->first();

           return response()->json($sub_update);
        }

    }

    public function editProviderSubscription($id){

        $data['pro_package'] = ProPackage::where('id',$id)->first();

        return view('admin.modules.subscription.edit_provider_subscription',$data);

    } 

    public function saveProviderSubscription(Request $request){

        $update['monthly_price'] = @$request->monthly_price;
        $update['yearly_price'] = @$request->yearly_price;
        
        $update['package_desc'] = @$request->description;
        $result = ProPackage::where('id',@$request->id)->update($update);

        if($result){

                session()->flash('success','Price updated successfully!');
                return redirect()->back();
        }else{

               session()->flash('error','Something went wrong');
               return redirect()->back();
        }

    } 
}

<?php

namespace App\Http\Controllers\Admin\Modules\Others;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Auth;
use App\User;
class AprovalController extends Controller
{
    
    public function __construct(){

        return $this->middleware('admin.auth:admin');
  }

   public function userApprovalStatusUpdate($id=null){

         $existuser = User::where('id',$id)->where('status','!=','D')->first();
            
         if($existuser->temp_email != null && $existuser->temp_mobile != null){

               $checkEmail = User::where('id','!=',$id)->where('email',$existuser->temp_email)->where('status','!=','D')->first();
               $checkMobile = User::where('id','!=',$id)->where('mobile_number',$existuser->temp_mobile)->where('status','!=','D')->first();
               if($checkEmail){

                    if($checkMobile == null){

                        $update = User::where('id',$id)
                        ->update(['mobile_number'=>$existuser->temp_mobile,'temp_mobile'=>null,'email_mob_change'=>'E']);

                    }

                    session()->flash('error','This Email already exist in our platform');
                            return redirect()->back(); 
               } 
               else if($checkMobile){

                if($checkEmail == null){

                    $update = User::where('id',$id)
                    ->update(['email'=>$existuser->temp_email,'temp_email'=>null,'email_mob_change'=>'M']);

                }
               
                session()->flash('error','This Mobile number already exist in our platform');
                return redirect()->back(); 
                
               }
               
               else{

                    $update = User::where('id',$id)
                    ->update(['email'=>$existuser->temp_email,'mobile_number'=>$existuser->temp_mobile,'temp_email'=>null,'temp_mobile'=>null,'email_mob_change'=>null]);
                       
                    if($update){

                         session()->flash('success','Profile approved successful!');
                         return redirect()->back();
                    }
               }
         }else if($existuser->temp_email != null){

            $checkEmail = User::where('id','!=',$id)->where('email',$existuser->temp_email)->first();
            
            if($checkEmail){

                session()->flash('error','This Email already exist in our platform');
                return redirect()->back();
            }else{

                $update = User::where('id',$id)
                ->update(['email'=>$existuser->temp_email,'temp_email'=>null,'email_mob_change'=>null]);
                   
                if($update){
                    $user = User::where('id', $id)->first();
                    $send_email_approval = $this->sendEmailApproval(@$user->mobile_number,@$user->user_id);
                     session()->flash('success','Profile approved successful!');
                     return redirect()->back();
                }
            }
         }else if($existuser->temp_mobile != null){

            $checkMobile = User::where('id','!=',$id)->where('mobile_number',$existuser->temp_mobile)->where('status','!=','D')->first();

            if($checkMobile){

                session()->flash('error','This Mobile number already exist in our platform');
                return redirect()->back();
               }else{

                $update = User::where('id',$id)
                ->update(['mobile_number'=>$existuser->temp_mobile,'temp_mobile'=>null,'email_mob_change'=>null]);
                   
                if($update){

                     session()->flash('success','Profile approved successful!');
                     return redirect()->back();
                }
               }
         }
   }
   
   
   public function sendEmailApproval($number,$user_id){
        $apiKey = urlencode('NDEzODdhNjM0YTc3NDk2OTQ5MzI3OTY5NmI0YzM4NmY=');
        $num = '91'.$number;
        $numbers = urlencode($num); 
        $sender = urlencode('ITREAL');
        // $message = 'Use '.$otp.' OTP to verify your mobile number.';
        $message = rawurlencode('Your email has been updated by Admin on your request for user id '.@$user_id.'.
RIVirtual from iTester');
          
        $data = 'apikey=' . $apiKey . '&numbers=' . $numbers . "&sender=" . $sender . "&message=" . $message;
        
        $ch = curl_init('https://api.textlocal.in/send/?' . $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        
        return $response;

    }
}

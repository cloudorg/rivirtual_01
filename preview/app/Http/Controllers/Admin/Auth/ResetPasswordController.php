<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use App\Admin;
class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.guest:admin');
    }

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
   public function showResetForm(Request $request,$email = null, $token = null)
    {
        $user = Admin::where('email', @$email)->where('remember_token',@$token)->first();
        
        if($user)
        {
            return view('admin.auth.passwords.reset')->with(
            ['email' => $user->email]
        );
        }
        else
        {
            session()->flash("error",'Something Went Wrong.');
            return redirect()->route('home');
        }

        
    }
    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker('admins');
    }

    /**
     * Get the guard to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('admin');
    }
    
    public function reset(Request $request){

        $request->validate([
            'password' => 'required|string|min:8',
            'password_confirmation'=> 'required|string|min:8|same:password'
        ]);
        $user = Admin::where('email', $request->email)->first();
        if($user)
        {
            $user->update(['password' => \Hash::make($request->password ),'remember_token' =>null]);
             session()->flash("success",'Password changed successfully! Please login.');
            return redirect()->route('admin.dashboard');
        }
        else
        {
            session()->flash("error",'Something Went Wrong.');
            return redirect()->back();
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Property;
use App\Models\State;
use App\Models\City;
use App\Models\Locality;
use App\Models\SubCategory;
use App\Models\PostJob;
use Auth;
use App\Models\Quotes;
use App\Models\JobToImage;
use App\Models\MessageMaster;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
         $propertyDetails = Property::with('propertyImageMain')->where('status','A')->where('show_in_home','Y')->get();
        $new=[];
        $i=0;
        foreach($propertyDetails as $key=>$item){
            $new[$i][]=$item;
            if(($key+1)%2==0){
                $i=$i+1;
            }
        }
        $data['propertyDetails'] = $new;
        return view('home')->with($data);
    }

    public function checkEmail(Request $request)
    {
        if ($request->email) {
            $checkEmail = User::whereIn('status', ['A', 'I', 'U'])->where('email', $request->email)->count();
            if ($checkEmail > 0) {
                return response('false');
            } else {
                return response('true');
            }
        }
        return response('no email');
    }

    public function checkMobile(Request $request)
    {
        if ($request->mobile) {
            $checkMobile = User::whereIn('status', ['A', 'I', 'U','D'])->where('mobile_number', $request->mobile)->count();
            if ($checkMobile > 0) {
                return response('false');
            } else {
                return response('true');
            }
        }
        return response('no mobile');
    }


    public function getState(Request $request)
    {
        $data = State::where('country_id',$request->country)->get();
        $response=array();
        $result="<option value=''>Select State</option>";
        if(@$data->isNotEmpty())
        {
            foreach($data as $rows)
            {
                // if(@$request->id==$rows->id)
                // {
                //     $result.="<option value='".$rows->id."' selected >".$rows->name."</option>";
                // }

                // else
                // {
                    $result.="<option value='".$rows->id."' >".$rows->name."</option>";
                // }

            }
        }
        $response['state']=$result;
        return response()->json($response);
    }

    public function getCity(Request $request)
    {
        $data = City::where('state_id',$request->state)->get();
        $response=array();
        $result="<option value='' >Select City</option>";
        if(@$data->isNotEmpty())
        {
            foreach($data as $rows)
            {
                // if(@$request->id==$rows->id)
                // {
                //     $result.="<option value='".$rows->id."' selected >".$rows->name."</option>";
                // }

                // else
                // {
                    $result.="<option value='".$rows->id."' >".$rows->name."</option>";
                // }

            }
        }
        $response['city']=$result;
        return response()->json($response);
    }


    /**
     *   Method      : searchLocality
     *   Description : Locality name Search
     *   Author      : Soumojit
     *   Date        : 2021-DEC-07
     **/

    public function searchLocality(Request $request)
    {
        if ($request->name) {
            $html = '';
            $allLocality= Locality::where('locality_name', 'like', $request->name . '%')->get();
            if ($allLocality) {
                foreach ($allLocality as $key => $value) {
                    $html .= '<li onclick="add_searchbar(this)" ><a href="javascript:void(0)">' . $value->locality_name . '</a></li>';
                }
            }
            return $html;
        }
    }
    /**
     *   Method      : searchLocalityAvailable
     *   Description : Locality name Search available
     *   Author      : Soumojit
     *   Date        : 2021-DEC-07
     **/

    public function searchLocalityAvailable(Request $request)
    {   

        if(stripos(@$request->name,'₹') ===  false){
            if ($request->name) {
                $html = '';
                $allLocality= Locality::where('locality_name', 'like', $request->name . '%')->get();
                $allCity= City::where('name', 'like', $request->name . '%')->get();
                if(@$allCity){
                    foreach ($allCity as $key1 => $value1) {
                        $check=Property::where('city',$value1->id)->first();
                        if($check){
                            $html .= '<li onclick="add_searchbar(this)" ><a href="javascript:void(0)">' . $value1->name . '</a></li>';
                        }
                    }
                }
                if ($allLocality) {
                    foreach ($allLocality as $key => $value) {
                        $check=Property::where('locality',$value->id)->first();
                        if($check){
                            $html .= '<li onclick="add_searchbar(this)" ><a href="javascript:void(0)">' . $value->locality_name . '</a></li>';
                        }
                    }
                }
                return $html;
            }
        }
    }

    public function getSubCategory(Request $request){

        $data = SubCategory::where('category_id',$request->category)->get();
        $response=array();
        $result="<option value='' >Select Sub Category</option>";
        if(@$data->isNotEmpty())
        {
            foreach($data as $rows)
            {
                // if(@$request->id==$rows->id)
                // {
                //     $result.="<option value='".$rows->id."' selected >".$rows->name."</option>";
                // }

                // else
                // {
                    $result.="<option value='".$rows->id."' >".$rows->name."</option>";
                // }

            }
        }
        $response['subcategory']=$result;
        return response()->json($response);
    }

    public function getSubcategoryUser(Request $request){
        $data = SubCategory::where('category_id',$request->category)->where('status','A')->get();
        $response=array();
        $result=$data ;
        $response['sub_category']=$result;
        return response()->json($response);
    }

    /**
     *   Method      : jobDetails
     *   Description : for service provider job details.
     *   Author      : Puja
     *   Date        : 2021-Dec-14
     **/

    public function jobDetails($slug){

        $data["job_data"] = PostJob::with('getUser')->where('slug',@$slug)->first();
        $data['total_jobs'] = PostJob::where('user_id',$data['job_data']->user_id)->where('status','A')->count();
        $data['job_image'] = JobToImage::where('post_id',$data['job_data']->id)->first();

        $data['similar_jobs'] = PostJob::where('id','!=',$data['job_data']->id)->where('category',$data['job_data']->category)->get();

        if(Auth::user()){
            $data['quote']=Quotes::where('user_id',Auth::user()->id)->where('post_id',$data['job_data']->id)->first();
            
            $data['chat'] = MessageMaster::where('bid_id',@$data['quote']->id)->where('provider_id',Auth::user()->id)->first();
        }

        return view('modules.service_provider.job_details')->with($data);

    }

    /**
     *   Method      : OnlineAway
     *   Description : foronlineawaystatus
     *   Author      : Puja
     *   Date        : 2021-Dec-28
     **/


    public function onlineAway(Request $request)
    {
        $userId = @Auth::user()->id;
        // dd($request->all());
        $slug=@Auth::user()->slug;
        if(@$userId){
            $is_online='N';
            if($request->online_status=="true"){
                $is_online='Y';
            }
            // dd($is_online);
            User::where('id',$userId)->update(['online_status'=>$is_online]);
            $pusher = new Pusher(config("services.pusher.pusher_app_key"),config("services.pusher.pusher_app_secret"),config("services.pusher.pusher_app_id"), [
                'cluster' => config("services.pusher.pusher_app_cluster")
            ]);
            $response=$pusher->trigger('rivirtual', 'receive-event', [
                'type'          =>  @$is_online,
                'slug'      => $slug,
            ], $request->socket_id);
            if ($response) {                 
                return response()->json([
                    'result' => [
                        'type' => @$is_online,
                    ]
                ]);
            } else {
                return response()->json([
                    'error' => [
                        'message' => 'Something went wrong.',
                    ]
                ]);
            }
        }
    }
}

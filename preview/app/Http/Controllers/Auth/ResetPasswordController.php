<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\User;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showResetForm(Request $request,$id = null, $vcode = null)
    {
        $user = User::where('id', @$id)->where('vcode_email', @$vcode)->first();
        if($user)
        {
            return view('auth.passwords.reset')->with(
            ['id' => $user->id]
        );
        }
        else
        {
            session()->flash("error",'Something Went Wrong.');
            return redirect()->route('home');
        }

        
    }

    public function reset(Request $request)
    {
        $request->validate([
            'password' => 'required|string|min:8',
            'confirm_password'=> 'required|string|min:8|same:password'
        ]);
        $user = User::where('id', $request->id)->first();
        if($user)
        {
            $user->update(['password' => Hash::make($request->password ),'vcode_email' =>null]);
             session()->flash("success",'Password changed successfully! Please login.');
            return redirect()->route('home');
        }
        else
        {
            session()->flash("error",'Something Went Wrong.');
            return redirect()->back();
        }
    }
}

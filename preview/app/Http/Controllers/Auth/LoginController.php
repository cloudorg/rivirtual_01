<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\User;
use Mail;
use App\Mail\EmailVerification;
use App\Mail\ResetPasswordUserMail;
use Laravel\Socialite\Two\FacebookProvider;
use Socialite;
use Laravel\Socialite\Two\GoogleProvider;
use Cookie;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     *   Method      : customLogin
     *   Description : for log in
     *   Author      : Soumojit
     *   Date        : 2021-OCT-27
     **/

    public function customLogin(Request $request)
    {
        $response = [
            'jsonrpc' => 2.0
        ];
        $params = $request->params;

        // check username match with email
        $userDataEmail = User::whereIn('status', ['A', 'I', 'U'])->where('email', $params['username'])->first();

        if(@$userDataEmail){
            if (!Hash::check($params['password'], $userDataEmail->password)) {
                $response['error']['message'] = 'These credentials do not match our records';
                return response()->json($response);
            }
            if($userDataEmail->user_type!=$params['userType']){
                $response['error']['message'] = 'These credentials do not match our records';
                return response()->json($response);
            }
            if($userDataEmail->status=='U'){
                //$upd['vcode_email']=str_random(60);
                //User::where('id',$userDataEmail->id)->update($upd);
                //$user = User::where('id', $userDataEmail->id)->first();
                //Mail::send(new EmailVerification($user));
                $response['error']['message'] = 'Email account unverified please verify email';
                return response()->json($response);
            }
            if($userDataEmail->is_email_verified=='N'){
                //$upd['vcode_email']=str_random(60);
                //User::where('id',$userDataEmail->id)->update($upd);
                //$user = User::where('id', $userDataEmail->id)->first();
                //Mail::send(new EmailVerification($user));
                $response['error']['message'] = 'Email account unverified please verify email';
                return response()->json($response);
            }
            if($userDataEmail->status=='I'){
                $response['error']['message'] = 'Account Inactive please contact admin ';
                return response()->json($response);
            }
            $upd['last_login']=date('Y-m-d H:i:s');
            $upd['online_status']= 'Y';
            User::where('id',$userDataEmail->id)->update($upd);

            Auth::login($userDataEmail);

            $response['result']['data'] = $userDataEmail;
            return response()->json($response);
        }

        // check username match with phone number

        $userDataPhone = User::whereIn('status', ['A', 'I', 'U'])->where('mobile_number', $params['username'])->first();

        if(@$userDataPhone){

            if (!Hash::check($params['password'], $userDataPhone->password)) {
                $response['error']['message'] = 'These credentials do not match our records';
                return response()->json($response);
            }
            if($userDataPhone->user_type!=$params['userType']){
                $response['error']['message'] = 'These credentials do not match our records';
                return response()->json($response);
            }
            if($userDataPhone->status=='U'){
                $upd['otp_mobile']= mt_rand(100000,999999);
                User::where('id',$userDataPhone->id )->update($upd);
                $response['error']['otp']=$upd['otp_mobile'];
                $response['error']['data']=$userDataPhone;
                $response['error']['message'] = 'Mobile no not verified please verify';
                return response()->json($response);
            }
            if($userDataPhone->is_mobile_verified=='N'){
                $upd['otp_mobile']= mt_rand(100000,999999);
                User::where('id',$userDataPhone->id )->update($upd);
                $response['error']['otp']=$upd['otp_mobile'];
                $response['error']['data']=$userDataPhone;
                $response['error']['message'] = 'Mobile no not verified please verify';
                return response()->json($response);
            }
            if($userDataPhone->status=='I'){
                $response['error']['message'] = 'Account Inactive please contact admin ';
                return response()->json($response);
            }
            $upd['last_login']=date('Y-m-d H:i:s');
            $upd['online_status']= 'Y';
            User::where('id',$userDataPhone->id)->update($upd);
            Auth::login($userDataPhone);
            $response['result']['data'] = $userDataPhone;
            return response()->json($response);
        }
        $response['error']['message'] = 'User not register in our platform';
        return response()->json($response);


    }

    public function logout(Request $request)
    {  
        if(@auth()->user()){ 
            $user=User::where('id',auth()->user()->id)->first();
            User::where('id',auth()->user()->id)->update(['online_status' => 'N']);
        }

        $this->guard()->logout();
        $request->session()->invalidate();
        return $this->loggedOut($request) ?: redirect('/');

    }

    /**
     *   Method      : forgotPassword
     *   Description : forgot Password
     *   Author      : Soumojit
     *   Date        : 2021-OCT-29
     **/
    public function forgotPassword(Request $request){

        $response = [
            'jsonrpc' => 2.0
        ];
        $params = $request->params;

        $userDataEmail = User::whereIn('status', ['A', 'I', 'U'])->where('email', $params['username'])->first();
        if($userDataEmail){
            $upd['vcode_email']=str_random(60);
            User::where('id',$userDataEmail->id)->update($upd);
            $user = User::where('id', $userDataEmail->id)->first();
            Mail::send(new ResetPasswordUserMail($user));
            $response['result']['message'] = 'Reset password link send your email address';
            $response['result']['email'] = '1';
            return response()->json($response);

        }

        $userDataPhone = User::whereIn('status', ['A', 'I', 'U'])->where('mobile_number', $params['username'])->first();
        if($userDataPhone){
            $upd['otp_mobile']= mt_rand(100000,999999);
            $send_recovery_otp = $this->sendRecoveryOtp($userDataPhone->user_id,$userDataPhone->mobile_number,$upd['otp_mobile']);
            User::where('id',$userDataPhone->id )->update($upd);
            $userData=User::where('id',$userDataPhone->id )->first();
            $response['result']['otp']=$userData->otp_mobile;
            $response['result']['data']=$userDataPhone;
            $response['result']['message'] = 'Reset password Otp send your mobile no';
            return response()->json($response);
        }
        $response['error']['message'] = 'User not register in our platform';
        return response()->json($response);

    }


    /**
     *   Method      :  verifyOtp
     *   Description :  mobile number verification
     *   Author      : Soumojit
     *   Date        : 2021-OCT-26
     **/
    public function verifyOtp(Request $request)
    {
        $response = [
            'jsonrpc' => 2.0
        ];
        $params = $request->params;
        $otp= @$params['codeBox1'] . @$params['codeBox2'] . @$params['codeBox3'] . @$params['codeBox4'] . @$params['codeBox5'] . @$params['codeBox6'];
        $userData = User::where('id', $params['id'])->where('status', '!=', 'D')->first();
        if (@$userData) {
            if(@$userData->otp_mobile==$otp){
                $upd=[];
                if (@$userData->status == 'U') {
                    $upd['status'] = 'A';
                    if (@$userData->user_type =='U' && @$userData->user_id==null) {
                        $allCustomer= User::where('user_id','!=',null)->where('user_type','U')->get();
                        $code='U';
                        $sum=str_pad($allCustomer->count()+1, 7, '0', STR_PAD_LEFT);
                        $upd['user_id']=$code.$sum;
                    }
                    if (@$userData->user_type =='A' && @$userData->user_id==null) {
                        $allCustomer= User::where('user_id','!=',null)->where('user_type','A')->get();
                        $code='A';
                        $sum=str_pad($allCustomer->count()+1, 7, '0', STR_PAD_LEFT);
                        $upd['user_id']=$code.$sum;
                    }
                    if (@$userData->user_type =='S' && @$userData->user_id==null) {
                        $allCustomer= User::where('user_id','!=',null)->where('user_type','S')->get();
                        $code='PC';
                        $sum=str_pad($allCustomer->count()+1, 6, '0', STR_PAD_LEFT);
                        $upd['user_id']=$code.$sum;
                    }
                }
                $upd['otp_mobile'] = null;
                $upd['is_mobile_verified'] = 'Y';
                User::where('id', $userData->id)->update($upd);
                $response['result']['message'] = 'Mobile number verified login your account';
                $response['result']['data'] = $userData;
                return response()->json($response);
            }
            $response['error']['message'] = 'Invalid OTP entered.';
            return response()->json($response);
        }
        $response['error']['message'] = 'Something went wrong';
        return response()->json($response);

    }


    public function changePassword(Request $request){
        $response = [
            'jsonrpc' => 2.0
        ];
        $params = $request->params;
        $userData = User::where('id', $params['id'])->where('status', '!=', 'D')->first();
        if($userData){
            $upd=[];
            $upd['password'] = \Hash::make($params['newpassword']);
            User::where('id', $userData->id)->update($upd);
            $response['result']['message'] = 'Password reset successfully';
            $response['result']['data'] = $userData;
            return response()->json($response);
        }
        $response['error']['message'] = 'Something went wrong';
        return response()->json($response);
    }






    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($user_type,$provider_type)
    {
        $last_url = url()->previous();
        session(['last_url' => $last_url]);
        if ($provider_type == 'facebook') {
            $config = [
                'client_id' => env('FACEBOOK_CLIENT_ID'),
                'client_secret' => env('FACEBOOK_CLIENT_SECRET'),
                'redirect' => route('login.social.callback', ['user_type'=>$user_type,'provider_type' => $provider_type]),
            ];

            $provider = Socialite::buildProvider(
                    FacebookProvider::class,
                    $config
                )->stateless();
        } else if ($provider_type == 'google') {
            $config = [
                'client_id' => env('GOOGLE_CLIENT_ID'),
                'client_secret' => env('GOOGLE_CLIENT_SECRET'),
                'redirect' => route('login.social.callback', ['user_type'=>$user_type,'provider_type' => $provider_type
                ]),
            ];

            $provider = Socialite::buildProvider(
                GoogleProvider::class,

                $config
            )->stateless();
        }


        return $provider->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($user_type,$provider_type)
    {
        try {
            if ($provider_type == 'facebook') {
                $config = [
                    'client_id' => env('FACEBOOK_CLIENT_ID'),
                    'client_secret' => env('FACEBOOK_CLIENT_SECRET'),
                    'redirect' => route('login.social.callback',
                        ['user_type'=>$user_type,'provider_type' => $provider_type]
                    ),
                ];

                $provider = Socialite::buildProvider(
                        FacebookProvider::class,
                        $config
                    )->stateless();
            } else if ($provider_type == 'google') {
                $config = [
                    'client_id' => env('GOOGLE_CLIENT_ID'),
                    'client_secret' => env('GOOGLE_CLIENT_SECRET'),
                    'redirect' => route('login.social.callback', ['user_type'=>$user_type,'provider_type' => $provider_type]),
                ];

                $provider = Socialite::buildProvider(
                    GoogleProvider::class,

                    $config
                )->stateless();
            }
            $user = $provider->user();
        } catch (\Exception $e) {
            session()->flash('error', 'Something went wrong with social login');
            return redirect('/');
        }
        // dd($provider);

        // dd ($user);

        if(@$user->email==null){
            session()->flash('error', 'Email not added with this account');
            return redirect()->route('home');
        }

         // dd ($user);
        $upd = [];
        $ins = [];
        // dd(session()->get('last_url'));

        if (@$provider_type == 'facebook') {
            $social_id_check = User::where('facebook_id', $user->id)->whereIn('status', ['U', 'A'])->first();
            $social_id_check_inactive = User::where('facebook_id', $user->id)->whereIn('status', ['I'])->first();
            if(@$social_id_check_inactive){
                session()->flash('error', 'Account Inactive please contact admin');
                return redirect()->route('home');
            }
            if (@$social_id_check) {
                $upd['last_login']=date('Y-m-d H:i:s');
                $upd['online_status']= 'Y';
                User::where('id',$social_id_check->id)->update($upd);
                Auth::login($social_id_check);
                // if (Session::get('customurl')) {
                //   $val = Session::get('customurl');
                //   $exp = explode('/',$val);
                //   if(end($exp)!=$socal_id_ckeck->slug)
                //   {
                //     // session()->flash('success', 'login successfully');
                //      return redirect(Session::get('customurl'));
                //   }
                //  else
                //   {
                //     // session()->flash('success', 'login successfully');
                //     return redirect()->route('user.dashboard');
                //   }
                // }
                if($social_id_check->user_type=='U'){

                    // return redirect()->route('user.dashboard');
                    return redirect(session()->get('last_url'));
                }
                if($social_id_check->user_type=='A'){

                    return redirect()->route('agent.dashboard');
                }
                if($social_id_check->user_type=='S'){

                    return redirect()->route('service.provider.dashboard');
                }
            }
        }
        if (@$provider_type == 'google') {
            $social_id_check = User::where('google_id', $user->id)->whereIn('status', ['U', 'A'])->first();
            $social_id_check_inactive= User::where('google_id', $user->id)->whereIn('status', ['I'])->first();
            if(@$social_id_check_inactive){
                session()->flash('error', 'Account Inactive please contact admin');
                return redirect()->route('home');
            }
            if (@$social_id_check) {
                $upd['last_login']=date('Y-m-d H:i:s');
                $upd['online_status']= 'Y';
                User::where('id',$social_id_check->id)->update($upd);
                Auth::login($social_id_check);
                if($social_id_check->user_type=='U'){
                    return redirect(session()->get('last_url'));

                    // return redirect()->route('user.dashboard');
                }
                if($social_id_check->user_type=='A'){

                    return redirect()->route('agent.dashboard');
                }
                if($social_id_check->user_type=='S'){

                    return redirect()->route('service.provider.dashboard');
                }
            }
        }

        // email check
        $user_check = User::where('email', @$user->email)->whereIn('status', ['U','A'] )->first();
        $user_check_inactive = User::where('email', @$user->email)->whereIn('status', ['I'] )->first();
        if(@$user_check_inactive){
            session()->flash('error', 'Account Inactive please contact admin');
            return redirect()->route('home');
        }
        if (@$user_check) {
            if ($provider_type == 'facebook') {
                $upd['facebook_id'] = $user->id;
                $upd['status'] = 'A';
                $upd['last_login']=date('Y-m-d H:i:s');
                $upd['online_status']= 'Y';
                User::where('email', $user->email)->whereIn('status', ['U', 'A'])->update($upd);
                Auth::login($user_check);
                if($user_check->user_type=='U'){
                    return redirect()->route('user.dashboard');
                }
                if($user_check->user_type=='A'){
                    return redirect()->route('agent.dashboard');
                }
                if($user_check->user_type=='S'){
                    return redirect()->route('service.provider.dashboard');
                }
            }
            if ($provider_type == 'google') {
                $upd['google_id'] = $user->id;
                $upd['status'] = 'A';
                $upd['last_login']=date('Y-m-d H:i:s');
                $upd['online_status']= 'Y';
                User::where('email', $user->email)->whereIn('status', ['U', 'A'])->update($upd);
                Auth::login($user_check);
                if($user_check->user_type=='U'){
                    return redirect()->route('user.dashboard');
                }
                if($user_check->user_type=='A'){
                    return redirect()->route('agent.dashboard');
                }
                if($user_check->user_type=='S'){
                    return redirect()->route('service.provider.dashboard');
                }
            }
        }




        $data['user']=$user;
        $data['provider_type']=$provider_type;
        $data['user_type']=$user_type;
        return view('auth.social_login')->with($data);


    }



    public function socialRegistation(Request $request){
        // return $request;
        $ins['name']= @$request->social_user_name;
        $ins['email']=@$request->social_user_email;
        $ins['mobile_number']=@$request->phone;
        $ins['status']='A';
        $ins['is_email_verified']='Y';
        $ins['otp_mobile']= mt_rand(100000,999999);
        $ins['singup_date']=date('Y-m-d H:i:s');
        if (@$request->provider_type == 'facebook') {
            $ins['facebook_id']=@$request->social_user_id;
            $ins['signup_from']='F';
        }
        if (@$request->provider_type == 'google') {
            $ins['google_id']=@$request->social_user_id;
            $ins['signup_from']='G';
        }
        if(@$request->user_type=='user'){

            $ins['user_type']='U';
        }
        if(@$request->user_type=='agent'){

            $ins['user_type']='A';
        }
        if(@$request->user_type=='service'){

            $ins['user_type']='S';
        }
        // return $ins;
        $user= User::create($ins);
        $userData= User::where('id',$user->id)->first();
        if($user){
            if (@$userData->user_type =='U' && @$userData->user_id==null) {
                $allCustomer= User::where('user_id','!=',null)->where('user_type','U')->get();
                $code='U';
                $sum=str_pad($allCustomer->count()+1, 7, '0', STR_PAD_LEFT);
                $upd['user_id']=$code.$sum;
            }
            if (@$userData->user_type =='A' && @$userData->user_id==null) {
                $allCustomer= User::where('user_id','!=',null)->where('user_type','A')->get();
                $code='A';
                $sum=str_pad($allCustomer->count()+1, 7, '0', STR_PAD_LEFT);
                $upd['user_id']=$code.$sum;
            }
            if (@$userData->user_type =='S' && @$userData->user_id==null) {
                $allCustomer= User::where('user_id','!=',null)->where('user_type','S')->get();
                $code='PC';
                $sum=str_pad($allCustomer->count()+1, 6, '0', STR_PAD_LEFT);
                $upd['user_id']=$code.$sum;
            }
            $slug= str_slug(@$user->name, "-");
            $upd['slug'] = $slug. "-".$user->id;
            $upd['last_login']=date('Y-m-d H:i:s');
            $upd['online_status']= 'Y';
            $userUpdate = User::where('id',$user->id)->update($upd);
            Auth::login($user);

            if(@$userData->user_type=='U'){
                return redirect(session()->get('last_url'));

                // return redirect()->route('user.dashboard');
            }
            if(@$userData->user_type=='A'){

                return redirect()->route('agent.dashboard');
            }
            if(@$userData->user_type=='S'){

                return redirect()->route('service.provider.dashboard');
            }
        }
        return redirect()->route('home');
    }
    
    public function sendRecoveryOtp($user_id,$number,$otp){
        
        $apiKey = urlencode('NDEzODdhNjM0YTc3NDk2OTQ5MzI3OTY5NmI0YzM4NmY=');
        $num = '91'.$number;
        $numbers = urlencode($num); 
        $sender = urlencode('ITREAL');
        // $message = 'Use '.$otp.' OTP to verify your mobile number.';
        $message = rawurlencode('OTP for password recovery of user id '.@$user_id.' is '.@$otp);

          
        $data = 'apikey=' . $apiKey . '&numbers=' . $numbers . "&sender=" . $sender . "&message=" . $message;
        
        $ch = curl_init('https://api.textlocal.in/send/?' . $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        
        return $response;
    }
}

@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
<link href="{{ URL::asset('public/frontend/croppie/croppie.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('public/frontend/croppie/croppie.min.css') }}" rel="stylesheet" />
@endsection

@section('title')
<title> RiVirtual | Chat History</title>
@endsection




@section('header')
@include('includes.header')
@endsection

@section('content')

<div class="haeder-padding"></div>


<div class="user_chat_sec">
	<div class="container">
	@if(!@$message)
	<div class="user_chat_inr nw_cls">
		<div class="text-center">
			<img src="{{ asset('public/frontend/images/no_msg.png') }}">
			<p class="err_mesg_msg_page">You do not have any messages in inbox.</p>
		</div>		
	</div>
	@else
		<div class="user_chat_inr">
			<div class="user_chat_left">
				<div class="u_chat_le_inr">
					<div class="user_chat_top">
						<form action="{{route('show.message')}}" onsubmit="return false" id="myForm" method="POST">
						   @csrf
							<div class="sharch_input">
								<input type="text" placeholder="Search" name="search">
								<input type="submit" value="" class="msg-submit">
							</div>
						</form>
					</div>
					<input type="hidden" name="" id="user_type" value="{{@Auth::user()->user_type}}">
					<input type="hidden" class="message_master_id" value="{{@$message[0]->id}}">
						<div class="user_chat_list">
							
											@if(!@$message)
	                 			<p class="err_mesg_msg_page">You do not have any messages in inbox.</p>
			                @elseif(@$message)
			                @foreach($message as  $key=>$msg)
			                 	@php
				                  if(@auth()->user()->id==$msg->seeker_id){
				                    $user=$msg->getProvider;
				                  }else{
				                    $user=$msg->getSeeker;
				                  }
			                	@endphp
			                
				            		<div class="chat-holders user_chat_item {{@$key==0?'chat-list-active':''}}" data-id="{{@$msg->id}}" data-name="{{@$user->name}}" data-online_status="{{@$user->online_status}}" data-slug="{{@$user->slug}}" data-title="{{ strlen(@$msg->getService->request_title) > 25?substr(@$msg->getService->request_title, 0, 25).'..':@$msg->getService->request_title}}"
				                  
				                  @if(auth()->user()->id == @$msg->seeker_id && @$msg->is_block=='Y')
				                      data-block="Y"
				                  @elseif(auth()->user()->id == @$msg->seeker_id &&  @$msg->is_block=='N')
				                      data-block="N"
				                  @elseif(@$msg->is_block=='Y')
				                      data-block="BL"
				                  @else 
				                      data-block="A"
				                  @endif
				                  >
				                <span class="chat_time">{{date('d-m-Y')==date('d-m-Y',strtotime($msg->getMessageDetailLastMessage->created_at))?date('H:i A',strtotime($msg->getMessageDetailLastMessage->created_at)):date('d-F-Y H:i A',strtotime($msg->getMessageDetailLastMessage->created_at))}}</span>
				                <div class="media">
													<em>
														<span class="holder-image">
															@if(@$user->profile_pic)
						                    					<img src="{{ URL::to('storage/app/public/profile_picture') }}/{{ @$user->profile_pic }}" alt="">
						                    				@else
						                    					<img src="{{ asset('public/frontend/images/avatar.png') }}" alt="">
						                    				@endif
						                    				@if(@$user->online_status == 'Y')
															<small class="on_li"></small>
															@else
															<small class="on_li" style="background:red !important"></small>
															@endif
														</span>
														
													</em>
										            
													<div class="media-body">
															<h4>{{ @$user->name }}</h4>
															@if(@$user->online_status == 'Y')
															<p class="user-active">Active</p>
															@else
															<p class="user-active" style="color:red !important">Offline</p>
															@endif
															<!-- <p></p> -->
													</div>
												</div>
											</div>
	                    @endforeach
	                	@endif
	              		</div>
            	</div>
            </div>
           	

			
		
				<div class="user_chat_right">
					<div class="chat_rig_inr">
						<div class="chat_rig_top">
							<div class="chat_rig_top_le chat-date">

								<h1 class="msg-user-details"></h1>
								<!-- <span>{{ date('jS M,Y',strtotime(date('Y-m-d'))) }}</span> -->
								<p></p>
							</div>
							<!-- <div class="chat_rig_top_rig">
								<a href="#url" class="see_cc">View Profile</a>
							</div> -->
						</div>
						<div class="all-conversation chat_rig_body chat-box">
	                      @foreach (@$messageDetail as $val)

	                      @if(@auth()->user()->id!=@$val->user_id)

							<div class="chat_mass_itm sender-div">
								<div class="media">
	                              
									<em>
	                                	@if(@$val->getUser->profile_pic)
	                                    <img src="{{ URL::to('storage/app/public/profile_picture') }}/{{ @$val->getUser->profile_pic }}" alt="">
	                                    @else
	                                    <img src="{{ url('public/frontend/images/avatar.png') }}" alt="">
	                                    @endif
	                                </em>
									<div class="media-body">
										@if(@auth()->user()->user_type == 'U')
											@if(@$val->getUser->user_type == 'A')
											<h5><a href="{{route('agent.public.profile',@$val->getUser->slug)}}">{{ @$val->getUser->name }}</a></h5>
											@elseif(@$val->getUser->user_type == 'S')
											<h5><a href="{{route('pro.profile',@$val->getUser->slug)}}">{{ @$val->getUser->name }}</a></h5>
											@endif
										@else
										<h5>{{ @$val->getUser->name }}</h5>
										@endif
											<div class="chat_mass_bx">
													@if(@$val->file)

																			
													@php
			                                      		$filename = explode('.',@$val->file);  
			                                      		$n=count($filename)-1;
			                                		@endphp
                                					@if($filename[$n] == 'jpg'|| $filename[$n] == 'png'||$filename[$n] == 'JPG'||$filename[$n] == 'gif'|| $filename[$n] == 'jpeg' || $filename[$n] == 'jfif')
                                						<br><a href="{{url('storage/app/public/message_files/')}}/{{@$val->file}}"  target=_blank><img width="100" src="{{ url('storage/app/public/message_files/'.@$val->file) }}" alt=""></a>
                                            @elseif($filename[$n] == 'mp4')
                                         <br><a href="{{url('storage/app/public/message_files/')}}/{{@$val->file}}"  target=_blank><img width="100" src="{{ url('public/frontend/images/vedio_icon.jpg') }}" alt=""></a>
                                  					@else
                                    					<br><a href="{{url('storage/app/public/message_files/')}}/{{@$val->file}}"  target=_blank><img width="100" src="{{ url('public/frontend/images/document_imge.png') }}" alt=""></a>

                                					@endif

													<!-- <br><a href="{{url('storage/app/public/message_files/')}}/{{@$val->file}}"  target=_blank>{{substr(@$val->file,strpos(@$val->file,'_')+1)}}
													</a> -->
													@endif
													<p>
												@php
												if(@$val->message){

            									$hv_https = stripos(@$val->message,'<a');
            									
            									
									            
									            if(@$hv_https){
									                $d = substr(@$val->message,$hv_https);
									              
									                $ancr = str_replace($d,$d,@$val->message);
									                
									                $val->message = $ancr;
									                if($ancr){
									                	echo @$ancr	;
									                }else{
									                	echo @$val->message;
									                }
									            }
									            else{
									            	echo @$val->message;
									            }
									        }

																
											@endphp
											</p>
											</div>
											<span>{{date('d-m-Y')==date('d-m-Y',strtotime($val->created_at))?date('H:i A',strtotime($val->created_at)):date('d-F-Y H:i:A',strtotime($val->created_at))}}</span>
									</div>
								</div>
							</div>
	                      @else
							<div class="chat_mass_itm chat_mass_itm_rig reciever-div">
								
								
								
								
								

								<div class="media">
									<em>
						            @if(@$val->getUser->profile_pic)
						                <img src="{{ URL::to('storage/app/public/profile_picture') }}/{{ @$val->getUser->profile_pic }}" alt="">
						            @else
						                <img src="{{ url('public/frontend/images/avatar.png') }}" alt="">
						            @endif
						            </em>
						                                    
						                                
									<div class="media-body">
										<h5>{{ Auth::user()->name }}</h5>
										<div class="chat_mass_bx">
										    @if(@$val->file)
											@php
                                      		$filename = explode('.',@$val->file);  
                                      		$n=count($filename)-1;
                                			@endphp
                                				@if($filename[$n] == 'jpg'|| $filename[$n] == 'png'||$filename[$n] == 'JPG'||$filename[$n] == 'gif'|| $filename[$n] == 'jpeg' || $filename[$n] == 'jfif')
                                						<br><a href="{{url('storage/app/public/message_files/')}}/{{@$val->file}}"  target=_blank><img width="100" src="{{ url('storage/app/public/message_files/'.@$val->file) }}" alt=""></a>
                                         @elseif($filename[$n] == 'mp4')
                                         <br><a href="{{url('storage/app/public/message_files/')}}/{{@$val->file}}"  target=_blank><img width="100" src="{{ url('public/frontend/images/vedio_icon.jpg') }}" alt=""></a>   
                                  				@else
                                    					<br><a href="{{url('storage/app/public/message_files/')}}/{{@$val->file}}"  target=_blank><img width="100" src="{{ url('public/frontend/images/document_imge.png') }}" alt=""></a>

                                				@endif
																<!-- <br><a href="{{url('storage/app/public/message_files/')}}/{{@$val->file}}"  target=_blank>{{substr(@$val->file,strpos(@$val->file,'_')+1)}}</a>  -->
												@endif
												<p>
												@php
												if(@$val->message){

            									$hv_https = stripos(@$val->message,'<a');
            									
            									
									            
									            if(@$hv_https){
									                $d = substr(@$val->message,$hv_https);
									              
									                $ancr = str_replace($d,$d,@$val->message);
									                
									                $val->message = $ancr;
									                if($ancr){
									                	echo @$ancr	;
									                }else{
									                	echo @$val->message;
									                }
									            }
									            else{
									            	echo @$val->message;
									            }
									        }

																
											@endphp
											</p>
										</div>
										<span>{{date('d-m-Y')==date('d-m-Y',strtotime($val->created_at))?date('H:i A',strtotime($val->created_at)):date('d-F-Y H:i:A',strtotime($val->created_at))}}</span>
									</div>
								</div>
							</div>
	                      @endif
	                      @endforeach	
						</div>
						<div class="chat_rig_foot">
							<div class="chat_rig_inputs">
								<!-- <input type="text" placeholder="Type your message here.." name="message" id="message"> -->
								<textarea class="hire-type message" style="height: 112px;" placeholder="Type your message here..."></textarea>
								<span class="upload-image-pgrs upload-message-file" style="padding-top:100px; display:none"><i class="fa fa-upload" aria-hidden="true"></i> Uploading...</span>
								<span class="file_in">
								    <input type="file" name="file" id="file">
								    
								    <label for="file" class="file_name"><img src="{{ url('public/frontend/images/atac.png') }}" alt=""></label>
								  </span>
							</div>
							<div class="uploadImg1">
                              <em><img id="blah"  alt="" width="100"></em>
             				</div>
							<div class="inputs_send">
								<input type="submit" value="" class="send rp_btn">						
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	@endif
	</div>
</div>
@endsection


@section('footer')
@include('includes.footer')
@endsection



@section('script')
@include('includes.script')

<script>
$('.uploadImg1').hide();
  pusher.connection.bind('connected', function() {
    socketId = pusher.connection.socket_id;
});
$(document).ready(function(){
  scrollChat();
  $('.send').click(function() {
  	var user_type = $('#user_type').val();
  	if(user_type == 'S'){
  		var u_type = 'P';
  	}else if(user_type == 'A'){
  		var u_type = 'A';
  	}else if(user_type == 'U'){
  		var u_type = 'U';
  	}
    const message = $('.message').val().trim();
    var files = $('#file').prop('files');
    message_master_id = $('.message_master_id').val();
    var filecnt = Object.keys(files).length;
    console.log("message");
    if ((message != ''||filecnt>0) && message_master_id !='') {
        if(filecnt>0){
              $('.upload-message-file').show();
        }
    data = new FormData();
    data.append('_token', "{{ csrf_token() }}");
    data.append('message_master_id',message_master_id);
    data.append('message',message);
    data.append('socket_id',socketId);
    data.append('u_type',u_type);
    console.log(data);
    $.each(files, function(k,file){
        data.append('file', file);
    });
        $.ajax({
            url: '{{ route("send.ajax") }}',
            type: 'POST',
            dataType: 'JSON',
            data:data,
            enctype: 'multipart/form-data',
            processData: false,  
            contentType: false
        })
        .done(result => {
          if (result.result) {
            attach_file=``; 
            replymsg=``; 
            if(result.result.file !== null){
                file = "{{url('storage/app/public/message_files/')}}";
                file = file+'/'+result.result.file;

                var res = result.result.file.split('.');
                var flname = res.slice(-1);
                if(flname == 'jpg'|| flname == 'png'||flname == 'JPG'||flname == 'gif'|| flname == 'jpeg' || flname == 'jfif'){

                	filename=result.result.file.substr(result.result.file.lastIndexOf("_")+1)
                	attach_file=`<br><a href="${file}" class="msg-link-color" target=_blank><img width="100" src="${file}" alt=""></a>`
                }else if(flname == 'mp4'){
                	filename=result.result.file.substr(result.result.file.lastIndexOf("_")+1)
                	attach_file = `<br><a href="${file}"  target=_blank><img width="100" src="{{ url('public/frontend/images/vedio_icon.jpg') }}" alt=""></a>`

                }else{

                	filename=result.result.file.substr(result.result.file.lastIndexOf("_")+1)
                	attach_file=`<br><a href="${file}" class="msg-link-color" target=_blank><img width="100" src="{{ url('public/frontend/images/document_imge.png') }}" alt=""></a>`
                	
                }

                                      
                
            }
            if(result.result.message !== null){
                    replymsg=result.result.message;								
            }
            var msgg = `<div class="chat_mass_itm chat_mass_itm_rig reciever-div">
													<div class="media">
														<em>
						                  <img src="{{ @Auth::user()->profile_pic ? url('storage/app/public/profile_picture/'.@Auth::user()->profile_pic) : url('public/frontend/images/avatar.png') }}" alt="">           
						                </em>
						                                    
						                                
														<div class="media-body">
															<h5>{{@Auth::user()->name}}</h5>
															<div class="chat_mass_bx">

																<p>${nl2br(replymsg)}${attach_file}</p>
															</div>
															<span>${getTimeDiff(new Date())}</span>
														</div>
													</div>
												</div>`;
            // var msgg = `<div class="media w-100 ml-auto mb-3 reciever-div">
            //                 <div class="media-body mr-3">
            //                     <div class="bg-primary rounded py-2 px-3 mb-1">
            //                         <p class="text-small mb-0 text-white">${nl2br(replymsg)}${attach_file}</p>
            //                     </div>
            //                     <div class="w-100"></div>
            //                     <p class="small">${getTimeDiff(new Date())}</p>
            //                 </div>
            //                 <img width="30" class="rounded-circle" src="{{ @Auth::user()->image ? url('storage/app/public/user/'.@Auth::user()->image) : url('public/images/blank.png') }}" alt="">
            //             </div>`;
            $('.chat-box').append(msgg);
            $('.message').val('');
            $('#file').val('');
            $('#blah').val('');
            $('.uploadImg1').hide();

            $('.upload-message-file').hide();
            $('.file_name').html(`<img src="{{ url('public/frontend/images/atac.png') }}" alt="">`);
            scrollChat();
            refreshListing();
        } else {
            alert(result.error.message);
        }
        });
    } else {
         alert("please select user &  type some message or insert file");
    }
  });
  $('body').on('click','.msg-submit',function(e){
    refreshListing();
  });
  $('.active-message-list').click(function(){
    if($('.achrive-message-list').hasClass("active")){
        var keyword = $('.hasDatepicker').val('');
    }
    $('.achrive-message-list').removeClass('active');
    $('.active-message-list').addClass('active');
    refreshListing();
  });
  $('.achrive-message-list').click(function(){
      var keyword = $('.hasDatepicker').val('');
      $('.achrive-message-list').addClass('active');
      $('.active-message-list').removeClass('active');
      refreshListing();
  });
  // $('#file').change(function(){
  //   var filename = $('#file').val().split('\\').pop();
  //   $('.file_name').html(filename);
  //    readURL(this);


  // });
  function readURL(input) {
    if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('.uploadImg1').show();
      $('#blah').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]);
    }
  }


  $("#file").change(function () {
            $('.uploadImg1').html('');
            var filename = $('#file').val().split('\\').pop();
    				// $('.file_name').html(filename);
            let files = this.files;
            let img  = new Image();
            if (files.length > 0) {
                let exts = ['image/jpeg', 'image/png', 'image/gif','application/pdf','application/vnd.openxmlformats-officedocument.wordprocessingml.document','video/mp4'];
                let imgExts = ['image/jpeg', 'image/png', 'image/gif'];
                let vExts = ['video/mp4'];
                console.log(exts);
                let valid = true;
                $.each(files, function(i, f) {
                    if (exts.indexOf(f.type) <= -1) {
                        valid = false;
                        return false;
                    }
                });
                if (! valid) {
                    alert('Please insert valid file');
                    $("#file").val('');
                    return false;
                }
                // img.src = window.URL.createObjectURL(event.target.files[0])
                // img.onload = function () {
                //     if(this.width > 250 || this.height >160) {
                //         flag=0;
                //         alert('Please upload proper image size less then : 250px x 160px');
                //         $("#customFile").val('');
                //         $('.uploadImg').hide();
                //         return false;
                //     } 
                // };
                
                $.each(files, function(i, f) {
                	console.log(vExts.indexOf(f.type));
                    var reader = new FileReader();
                    reader.onload = function(e){
                    	
                        if (imgExts.indexOf(f.type) <= -1 && vExts.indexOf(f.type) <= -1 ) {

                            $('.uploadImg1').append('<img width="100"  src="{{ url('public/frontend/images/document_imge.png') }}">');
                        }else if(vExts.indexOf(f.type) == 0){
                        	
                        	$('.uploadImg1').append('<img width="100" src="{{ url('public/frontend/images/vedio_icon.jpg') }}" alt="">');
                        }else{
                            $('.uploadImg1').append('<img width="100"  src="' + e.target.result + '"><a href="javascript:;" class="delete_image1"><img src="{{ URL::to('public/frontend/images/w-cross.png')}}"></a>');
                        }
                    };
                    reader.readAsDataURL(f);
                });
                $('.uploadImg1').css('display','flex');
                $('.uploadImg1').show();
            }
            
        });






  $('body').on('click','.block',function(){
    if (confirm("Are you sure you want to block this contact?")) {
      message_master_id = $('.message_master_id').val();
      $.ajax({
        url: '{{ route("block.message") }}',
        type: 'POST',
        dataType: 'JSON',
        data:{
            '_token'  : '{{csrf_token()}}',
            'socket_id' :socketId,
            'message_master_id':message_master_id,
            'type':'B'
          } 
      })
      .done(result => {
          if(result.result){
            //   $('.block-btn').html('Unblock');
            $('.block-btn').html('<i class="fa fa-ban" aria-hidden="true" ></i>');
            
              $('.block-btn').addClass('unblock');
              $('.block-btn').removeClass('block');
              $('.attachedd-type').hide();
              $('.message-box-div').append('<h2 class="block-msg">You have block the conversation</h2>');
              refreshListing();
          }else{
            alert(result.error.message);
          }
      }); 
    }     
  });
  $('body').on('click','.unblock',function(){
    if (confirm("Are you sure you want to unblock this contact?")) {
      message_master_id = $('.message_master_id').val();
      $.ajax({
        url: '{{ route("block.message") }}',
        type: 'POST',
        dataType: 'JSON',
        data:{
          '_token'  : '{{csrf_token()}}',
          'socket_id' :socketId,
          'message_master_id':message_master_id,
          'type':'U'
        } 
      })
      .done(result => {
        if(result.result){
        //   $('.block-btn').html('Block');
          $('.block-btn').html('<i class="fa fa-ban" aria-hidden="true" ></i>');
          
          $('.block-btn').removeClass('unblock');
          $('.block-btn').addClass('block');
          $('.attachedd-type').show();
          $('.block-msg').remove();
          refreshListing();
        }else{
          alert(result.error.message);
        }
      });  
    }
  });
$('body').on('click','.archive-flag',function(){
    if($(this).data('type')=='A'){
        msg="archive";
    }else{
        msg="Unarchive";
    }
   
    if (confirm("Are you sure you want to "+msg+" this chat?")) {
      message_master_id = $('.message_master_id').val();
      window.location.href ='{{route('message.archive')}}/'+message_master_id;
    }
  })
  $('.active-message-list').click(function(){
    if($('.achrive-message-list').hasClass("active")){
        var keyword = $('.hasDatepicker').val('');
    }
    $('.achrive-message-list').removeClass('active');
    $('.active-message-list').addClass('active');
    refreshListing();
  });

  $('.achrive-message-list').click(function(){
      var keyword = $('.hasDatepicker').val('');
      $('.achrive-message-list').addClass('active');
      $('.active-message-list').removeClass('active');
      refreshListing();
  });
  $(".mobile_filter").click(function(){
      $(".left-list").slideToggle();
  });
})

$(document).on('click','.delete_image1',function(){
    
    $(this).parent('.uploadImg1').remove();
    
}); 
</script>
@endsection
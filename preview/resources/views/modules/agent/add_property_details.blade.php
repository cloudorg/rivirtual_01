@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
        margin-top: 10px;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
<link href="{{ URL::asset('public/frontend/croppie/croppie.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('public/frontend/croppie/croppie.min.css') }}" rel="stylesheet" />
@endsection

@section('title')
<title> RiVirtual | Add Property Details</title>
@endsection




@section('header')
@include('includes.header')
@endsection


@section('content')
<!----header--->
<div class="haeder-padding"></div>

<div class="dashboard-agent">
	<div class="container">
		<div class="row">
			@include('includes.sidebar')
            <div class="col-lg-9 col-md-12 col-sm-12">
				<div class="cus-dashboard-right">
				   <h2>Add Property</h2>
				</div>
				<div class="agent-dash-right">
					<div class="agent-das-tab">
						<ul>
                            @if(@$propertyDetails)
                            <li class="active"><a @if($propertyDetails->status=='I') href="javascript:;" @else href="{{route('agent.add.property',['id'=>$propertyDetails->id])}}" @endif>Property Details</a></li>
							<li ><a @if($propertyDetails->status!=null) href="javascript:;" @else href="{{route('agent.add.property.image',['id'=>$propertyDetails->id])}}" @endif>Property Images</a></li>
                            @else
                            <li class="active"><a href="javascript:;">Property Details</a></li>
							<li><a href="javascript:;">Property Images</a></li>
                            @endif
						</ul>
					</div>
                    @include('includes.message')
                    <form action="{{@$propertyDetails?route('agent.add.property.save',['id'=>$propertyDetails->id]):route('agent.add.property.save')}}" id="addPropertyForm" method="POST">
                        @csrf
                        <div class="agent-right-body new-upcon">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="redio_bx prop">
                                        <span>Property Type :</span>
                                        <ul class="category-ul ul_radio">
                                            <li>
                                                <label class="radio">
                                                    <input type="radio" name="master_property_type" value="RES"  @if(old('master_property_type')=='RES' || @$propertyDetails->master_property_type=='RES') checked @endif class="master_property_type">
                                                    <span class="outer"><span class="inner"></span></span>Residential
                                                </label>
                                            </li>
                                            <li>
                                                <label class="radio">
                                                        <input type="radio" name="master_property_type" value="COM" @if(old('master_property_type')=='COM'|| @$propertyDetails->master_property_type=='COM') checked @endif class="master_property_type">
                                                        <span class="outer"><span class="inner"></span></span>Commercial
                                                </label>
                                            </li>
                                           
                                        </ul>
                                        <label id="master_property_type-error" class="error" for="master_property_type" style="display: none"></label>
                                    </div>
                                </div>
                                <div class="col-md-8 residential_property"  @if(@$propertyDetails->master_property_type=='RES') style="display:block;" @else style="display:none;" @endif>
                                    <div class="redio_bx prop">
                                        
                                        <ul class="category-ul ul_radio">
                                            <li>
                                                <label class="radio">
                                                    <input type="radio" name="property_type" value="F" @if(old('property_type')=='F' || @$propertyDetails->property_type=='F') checked @endif class="property_type">
                                                    <span class="outer"><span class="inner"></span></span>Flat
                                                </label>
                                            </li>
                                            <li>
                                                <label class="radio">
                                                        <input type="radio" name="property_type" value="H" @if(old('property_type')=='H'|| @$propertyDetails->property_type=='H') checked @endif class="property_type">
                                                        <span class="outer"><span class="inner"></span></span>House
                                                </label>
                                            </li>
                                            <li>
                                                <label class="radio">
                                                        <input type="radio" name="property_type" value="GTCM" @if(old('property_type')=='GTCM'|| @$propertyDetails->property_type=='GTCM') checked @endif class="property_type">
                                                        <span class="outer"><span class="inner"></span></span>Gated Community
                                                </label>
                                            </li>
                                            <li>
                                                <label class="radio">
                                                        <input type="radio"  name="property_type" value="L" @if(old('property_type')=='L' || @$propertyDetails->property_type=='L') checked @endif class="property_type">
                                                        <span class="outer"><span class="inner"></span></span>Land
                                                </label>
                                            </li>
                                            <li>
                                                <label class="radio">
                                                        <input type="radio"  name="property_type" value="FRMH" @if(old('property_type')=='FRMH' || @$propertyDetails->property_type=='FRMH') checked @endif class="property_type">
                                                        <span class="outer"><span class="inner"></span></span>Farmhouse
                                                </label>
                                            </li>
                                            {{--<li>
                                                <label class="radio">
                                                        <input id="radio4" type="radio"  name="property_type" value="R" @if(old('property_type')|| @$propertyDetails->property_type=='R') checked @endif class="property_type">
                                                        <span class="outer"><span class="inner"></span></span>Residential
                                                </label>
                                            </li>--}}
                                            {{--<li>
                                                <label class="radio">
                                                        <input id="radio5" type="radio"  name="property_type" value="O" @if(old('property_type')=='O'|| @$propertyDetails->property_type=='O') checked @endif class="property_type">
                                                        <span class="outer"><span class="inner"></span></span>Office
                                                </label>
                                            </li>--}}
                                        </ul>
                                        <label id="property_type-error" class="error" for="property_type" style="display: none"></label>
                                    </div>
                                </div>
                                <div class="col-md-8 commercial_property"  @if(@$propertyDetails->master_property_type=='COM') style="display:block;" @else style="display:none;" @endif>
                                    <div class="redio_bx prop">
                                        <ul class="category-ul ul_radio">
                                             <li>
                                                <label class="radio">
                                                    <input type="radio" name="property_type" value="OSP" @if(old('property_type')=='OSP' || @$propertyDetails->property_type=='OSP') checked @endif class="property_type">
                                                    <span class="outer"><span class="inner"></span></span>Office Space
                                                </label>
                                              </li>
                                            <li>
                                                <label class="radio">
                                                    <input type="radio" name="property_type" value="WRH" @if(old('property_type')=='WRH' || @$propertyDetails->property_type=='WRH') checked @endif class="property_type">
                                                    <span class="outer"><span class="inner"></span></span>Warehouse
                                                </label>
                                            </li>
                                            <li>
                                                <label class="radio">
                                                        <input type="radio" name="property_type" value="SHR" @if(old('property_type')=='SHR'|| @$propertyDetails->property_type=='SHR') checked @endif class="property_type">
                                                        <span class="outer"><span class="inner"></span></span>Showrooms
                                                </label>
                                            </li>
                                            <li>
                                                <label class="radio">
                                                        <input type="radio"  name="property_type" value="INDT" @if(old('property_type')=='INDT' || @$propertyDetails->property_type=='INDT') checked @endif class="property_type">
                                                        <span class="outer"><span class="inner"></span></span>Industrial
                                                </label>
                                            </li>
                                            <li>
                                                <label class="radio">
                                                        <input id="radio4" type="radio"  name="property_type" value="RET" @if(old('property_type')=='RET'|| @$propertyDetails->property_type=='RET') checked @endif class="property_type">
                                                        <span class="outer"><span class="inner"></span></span>Retail
                                                </label>
                                            </li>
                            
                                        </ul>
                                        <label id="property_type-error" class="error" for="property_type" style="display: none"></label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="redio_bx ">
                                        <span>Property For :</span>
                                        <ul class="category-ul ul_radio">
                                            <li>
                                                <label class="radio">
                                                    <input type="radio" name="property_for" value="B" @if(old('property_for')=='B' || @$propertyDetails->property_for=='B') checked @endif class="property_for">
                                                    <span class="outer"><span class="inner"></span></span>For Buy
                                                </label>
                                            </li>
                                            <li>
                                                <label class="radio">
                                                        <input  type="radio" name="property_for" value="R" @if(old('property_for')=='R' || @$propertyDetails->property_for=='R') checked @endif class="property_for">
                                                        <span class="outer"><span class="inner"></span></span>For Rent
                                                </label>
                                            </li>
                                        </ul>
                                        <label id="property_for-error" class="error" for="property_for" style="display: none"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="das_input">
                                        <label>Name of the Property</label>
                                        <input class="required" type="text" placeholder="Enter property name.." value="{{old('property_name',@$propertyDetails->name)}}" name="property_name" >
                                    </div>
                                </div>
                                {{-- {{numberchange((int)$propertyDetails->budget_range_from)}}
                                {{numberchange((int)$propertyDetails->budget_range_to)}} --}}
                            </div>
                            @php
                            function count_digit($number) {
                                return strlen($number);
                            }
                            function divider($number_of_digits) {
                                $tens="1";
                                if($number_of_digits>8)
                                return 10000000;
                                while(($number_of_digits-1)>0)
                                {
                                    $tens.="0";
                                    $number_of_digits--;
                                }
                                return $tens;
                            }
                            //function call
                            function numberchange($num){
                                $num = $num;
                                $ext="";//thousand,lac, crore
                                $number_of_digits = count_digit($num); //this is call :)
                                if($number_of_digits>3)
                                {
                                    if($number_of_digits%2!=0)
                                    $divider=divider($number_of_digits-1);
                                    else
                                    $divider=divider($number_of_digits);
                                }else
                                $divider=1;
                                $fraction=$num/$divider;
                                $fraction=number_format($fraction,2);
                                $fraction;
                                if($number_of_digits==4 ||$number_of_digits==5)
                                $ext="k";
                                if($number_of_digits==6 ||$number_of_digits==7)
                                $ext="Lakhs";
                                if($number_of_digits==8 ||$number_of_digits==9||$number_of_digits==10)
                                $ext="Cr";
                                return $fraction." ".$ext;
                            }
                            @endphp

                            <div class="row">

                                <div class="col-md-4 new-office-class" @if(@$propertyDetails->property_type!='OSP') style="display: none" @endif>
                                    <div class="das_input">
                                        <label>Office Class</label>
                                        <select name="office_class" id="office_class" @if(@$propertyDetails->property_type=='OSP') class="required" @endif>
                                            <option value="">Select Office Class</option>
                                             <option value="A" @if(@$propertyDetails->office_class=='A') selected @endif >Class A</option>
                                             <option value="B"  @if(@$propertyDetails->office_class=='B') selected @endif>Class B</option>
                                             <option value="C"  @if(@$propertyDetails->office_class=='C') selected @endif>Class C</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 new-remove-office-class" @if(@$propertyDetails->property_type=='OSP' || @$propertyDetails->property_type=='L' || @$propertyDetails->master_property_type=='COM' || @$propertyDetails->property_type=='FRMH') style="display: none" @endif>
                                    <div class="das_input">
                                        <label>Bedrooms</label>
                                        <input @if(@$propertyDetails->property_type=='F'|| @$propertyDetails->property_type=='GTCM' || @$propertyDetails->property_type=='H') class="required" @endif type="text" placeholder="Enter here.." value="{{old('no_of_bedrooms',@$propertyDetails->no_of_bedrooms)}}" name="no_of_bedrooms" id="no_of_bedrooms">
                                    </div>
                                </div>
                                <div class="col-md-3 new-remove-room-class" @if(@$propertyDetails->master_property_type=='COM' && @$propertyDetails->property_type!='OSP') style="display:block" @else style="display:none;" @endif>
                                    <div class="das_input">
                                        <label>Rooms</label>
                                        <input @if(@$propertyDetails->property_type!='OSP' && @$propertyDetails->property_type!='L' && @$propertyDetails->property_type!='FRMH') class="required" @endif type="text" placeholder="Enter here.." value="{{old('no_of_room',@$propertyDetails->no_of_bedrooms)}}" name="no_of_room" id="no_of_room">
                                    </div>
                                </div>
                                <div class="col-md-3 property_type_land" @if(@$propertyDetails->property_type=='L' || @$propertyDetails->property_type=='FRMH') style="display: none" @endif>
                                    <div class="das_input">
                                        <label>Bathroom</label>
                                        <input @if(@$propertyDetails->property_type!='L' && @$propertyDetails->property_type!='FRMH') class="required" @endif type="text" placeholder="Enter here.." value="{{old('bathroom',@$propertyDetails->bathroom)}}" name="bathroom" id="bathroom">
                                    </div>
                                </div>

                                {{-- <div class="col-md-6">
                                    <div class="das_input">
                                        <label>Budget Range</label>
                                        <div class="bud-input">
                                            <div class="bud_sets">
                                                <input class="required" type="text" placeholder="From" value="{{@$propertyDetails->budget_range_from?(int)$propertyDetails->budget_range_from:''}}" name="budget_range_from" id="budget_range_from" maxlength="10">
                                                <span class="budget_iconn02"><i class="fa fa-inr" aria-hidden="true"></i></span>
                                                <p id="budget_range_from_p">@if(@$propertyDetails->budget_range_from)<span>₹</span> {{numberchange((int)$propertyDetails->budget_range_from)}} @endif</p>
                                            </div>
                                            <div class="bud_sets">
                                                <input class="required" type="text" placeholder="To" value="{{@$propertyDetails->budget_range_to?(int)$propertyDetails->budget_range_to:''}}" name="budget_range_to" id="budget_range_to" maxlength="10">
                                                <span class="budget_iconn02"><i class="fa fa-inr" aria-hidden="true"></i></span>
                                                <p id="budget_range_to_p">@if(@$propertyDetails->budget_range_to)<span>₹</span> {{numberchange((int)$propertyDetails->budget_range_to)}}@endif</p>
                                            </div>


                                        </div>
                                        <div class="bud-input" >
                                            <div style="width: 46.5%;">

                                                <label id="budget_range_from-error" class="error" for="budget_range_from" style="display:none;"></label>
                                            </div>
                                            <div style="width: 46.5%;">

                                                <label id="budget_range_to-error" class="error" for="budget_range_to" style="display:none;"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div> --}}
                                <div class="col-md-6 price_tab">
                                    
                                    <div class="das_input price_tab">

                                        <label>Price</label>
                                        <div class="bud-input neg_bx">
                                            <div class="bud_sets">
                                                <input class="required" type="text" placeholder="Price" value="{{@$propertyDetails->budget_range_from?(int)$propertyDetails->budget_range_from:''}}" name="budget_range_from" id="budget_range_from" maxlength="10">
                                                <span class="budget_iconn02"><i class="fa fa-inr" aria-hidden="true"></i></span>
                                                <p id="budget_range_from_p">@if(@$propertyDetails->budget_range_from)<span>₹</span> {{numberchange((int)$propertyDetails->budget_range_from)}} @endif</p>
                                            </div>
                                            
                                            <div class="radiobx ">
                                                <input type="checkbox" id="nego" name="nego" value="Y" @if(@$propertyDetails->is_negotiable == 'Y') checked @endif>
                                                <label for="nego" >Rent Negotiable</label>
                                            </div>


                                        </div>
                                        <div class="bud-input" >
                                            <div style="width: 46.5%;">

                                                <label id="budget_range_from-error" class="error" for="budget_range_from" style="display:none;"></label>
                                            </div>
                                            <div style="width: 46.5%;">

                                                <label id="budget_range_to-error" class="error" for="budget_range_to" style="display:none;"></label>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="col-md-4 property_type_land" @if(@$propertyDetails->property_type=='L' || @$propertyDetails->property_type=='FRMH') style="display: none" @endif>
                                    <div class="das_input">
                                        <label>Floor No.</label>
                                        <input type="text" placeholder="Enter floor" name="floor" @if(@$propertyDetails->property_type!='L' && @$propertyDetails->property_type!='FRMH') class="required" @endif value="{{old('floor',@$propertyDetails->floor)}}" id="floor"  maxlength="6">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                            <div class="col-md-4 new-rent-class" @if(@$propertyDetails->property_for!='R') style="display:none;" @elseif((@$propertyDetails->property_type == 'L' || @$propertyDetails->property_type == 'FRMH' || @$propertyDetails->property_type == 'OSP') && @$propertyDetails->property_for =='R' )  style="display:none;" @endif>
                                <div class="das_input">
                                    <label>Rent payment</label>
                                    <select name="rent_payment" id="rent_payment" @if(@$propertyDetails->property_for=='R' && @$propertyDetails->property_type != 'L' && @$propertyDetails->property_type != 'FRMH' && @$propertyDetails->property_type != 'OSP') class="required" @endif>
                                        <option value="">Select rent payment</option>
                                        <option value="M" @if(@$propertyDetails->rent_payment=='M') selected @endif >Monthly</option>
                                        <option value="Y"  @if(@$propertyDetails->rent_payment=='Y') selected @endif>yearly</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 new-rent-class" @if(@$propertyDetails->property_for!='R') style="display:none;" @elseif((@$propertyDetails->property_type == 'L' || @$propertyDetails->property_type == 'FRMH' || @$propertyDetails->property_type == 'OSP') && @$propertyDetails->property_for =='R' )  style="display:none;" @endif>
                                <div class="das_input">
                                    <label>Available from</label>
                                    <input type="text" readonly="true"  maxlength="10" id="available_from" name="available_from" placeholder="Available from" @if(@$propertyDetails->property_for=='R' && @$propertyDetails->property_type != 'OSP') class="required" @endif value={{@$propertyDetails->available_from?date('m/d/Y',strtotime($propertyDetails->available_from)):''}} >
                                </div>
                            </div>
                            <div class="col-md-4 new-rent-class" @if(@$propertyDetails->property_for!='R') style="display:none;" @elseif((@$propertyDetails->property_type == 'L' || @$propertyDetails->property_type == 'FRMH' || @$propertyDetails->property_type == 'OSP') && @$propertyDetails->property_for =='R' )  style="display:none;" @endif>
                                <div class="das_input">
                                    <label>Preference</label>
                                    <select name="preference" id="preference" @if(@$propertyDetails->property_for=='R' && @$propertyDetails->property_type != 'L' && @$propertyDetails->property_type != 'FRMH' && @$propertyDetails->property_type != 'OSP') class="required" @endif>
                                        <option value="">Select preference</option>
                                        <option value="N" @if(@$propertyDetails->preference=='N') selected @endif >No preference</option>
                                        <option value="F"  @if(@$propertyDetails->preference=='F') selected @endif>Families</option>
                                        <option value="B"  @if(@$propertyDetails->preference=='B') selected @endif>Bachelors</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 new-rent-class" @if(@$propertyDetails->property_for!='R') style="display:none;" @elseif((@$propertyDetails->property_type == 'L' || @$propertyDetails->property_type == 'FRMH' || @$propertyDetails->property_type == 'OSP') && @$propertyDetails->property_for =='R' )  style="display:none;" @endif>
                                <div class="das_input">
                                    <label>Deposit amount</label>
                                    <div class="bud_sets">
                                        <input @if(@$propertyDetails->property_for=='R' && @$propertyDetails->property_type != 'L' && @$propertyDetails->property_type != 'FRMH' && @$propertyDetails->property_type != 'OSP') class="required" @endif type="text" placeholder="Deposit amount" value="{{@$propertyDetails->deposit_amount?(int)$propertyDetails->deposit_amount:''}}" name="deposit_amount" id="deposit_amount" maxlength="10">

                                        <span class="budget_iconn02"><i class="fa fa-inr" aria-hidden="true"></i></span>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-4 new-rent-class" @if(@$propertyDetails->property_for!='R') style="display:none;" @elseif((@$propertyDetails->property_type == 'L' || @$propertyDetails->property_type == 'FRMH' || @$propertyDetails->property_type == 'OSP') && @$propertyDetails->property_for =='R' )  style="display:none;" @endif>
                                <div class="das_input">

                                    <label>Maintenance Charges</label>
                                    <div class="bud_sets">
                                        <input @if(@$propertyDetails->property_for=='R' && @$propertyDetails->property_type != 'L' && @$propertyDetails->property_type != 'FRMH' && @$propertyDetails->property_type != 'OSP') class="required" @endif type="text" placeholder="Maintenance Charges" value="{{@$propertyDetails->maintenance_charge?(int)$propertyDetails->maintenance_charge:''}}" name="maintenance_charge" id="maintenance_charge" maxlength="10">
                                        <span class="budget_iconn02"><i class="fa fa-inr" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 land-area-class" @if(@$propertyDetails->property_type != 'L' && @$propertyDetails->property_type != 'FRMH') style="display:none" @endif>
                                    <div class="das_input">
                                        <label>Land Area</label>
                                        <input @if(@$propertyDetails->property_type == 'L' || @$propertyDetails->property_type == 'FRMH') class="required" @endif type="text" placeholder="Enter here.." value="{{old('land_area',intval(@$propertyDetails->land_area))}}" name="land_area" id="land_area" maxlength="10">
                                    </div>
                                </div>
                                <div class="col-md-4 land-area-class" @if(@$propertyDetails->property_type != 'L' && @$propertyDetails->property_type != 'FRMH') style="display:none" @endif>
                                    <div class="das_input">
                                        <label>Land Area Unit(Acres/Sq Yards)</label>
                                        <select @if(@$propertyDetails->property_type == 'L' || @$propertyDetails->property_type == 'FRMH') class="required" @endif name="land_area_unit" id="land_area_unit">
                                            <option value="">Select</option>
                                            <option value="A" @if(old('land_area_unit')=='A' || @$propertyDetails->land_area_unit == 'A' ) selected @endif>Acres</option>
                                            <option value="Y" @if(old('land_area_unit')=='Y' || @$propertyDetails->land_area_unit == 'Y' ) selected @endif>Sq Yards</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 t-area-class" @if(@$propertyDetails->property_type == 'L' || @$propertyDetails->property_type == 'FRMH') style="display:none" @endif>
                                    <div class="das_input">
                                        <label> Area in Sqft</label>
                                        <input @if(@$propertyDetails->property_type != 'L' && @$propertyDetails->property_type != 'FRMH') class="required" @endif type="text" placeholder="Enter here.." value="{{@$propertyDetails->area?$propertyDetails->area:''}}" name="area" id="area" @if(@$propertyDetails->property_type == 'L') maxlength="100"@endif  >
                                    </div>
                                </div>
                                <div class="col-md-4 area-class" @if(@$propertyDetails->property_type=='L' || @$propertyDetails->property_type=='FRMH' || @$propertyDetails->property_type=='WRH' || @$propertyDetails->property_type=='SHR' || @$propertyDetails->property_type=='INDT' || @$propertyDetails->property_type=='RET') style="display: none" @endif >
                                    <div class="das_input">
                                        <label>Super area in Sqft</label>
                                        <input @if(@$propertyDetails->property_type=='H' || @$propertyDetails->property_type=='F' || @$propertyDetails->property_type=='GTCM'  || @$propertyDetails->property_type=='OSP')  class="required" @endif type="text" placeholder="Enter here.." value="{{old('super_area',@$propertyDetails->super_area)}}" name="super_area" id="super_area" maxlength="10">
                                    </div>
                                </div>
                                <div class="col-md-4 area-class" @if(@$propertyDetails->property_type=='L' || @$propertyDetails->property_type=='FRMH' || @$propertyDetails->property_type=='WRH' || @$propertyDetails->property_type=='SHR' || @$propertyDetails->property_type=='INDT' || @$propertyDetails->property_type=='RET') style="display: none" @endif>
                                    <div class="das_input">
                                        <label>Carpet area in Sqft</label>
                                        <input @if(@$propertyDetails->property_type=='H' || @$propertyDetails->property_type=='F' || @$propertyDetails->property_type=='GTCM'  || @$propertyDetails->property_type=='OSP')  class="required" @endif type="text" placeholder="Enter here.." value="{{old('carpet_area',@$propertyDetails->carpet_area)}}" name="carpet_area" maxlength="10" id="carpet_area">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 property_type_land pro_commercial"  @if(@$propertyDetails->property_type=='L' || @$propertyDetails->property_type=='FRMH' || @$propertyDetails->master_property_type=='COM') style="display: none" @endif>
                                    <div class="redio_bx furn">
                                        <span>Construction Status :</span>
                                        
                                        <ul class="category-ul ul_radio">
                                            <li>
                                                <label class="radio">
                                                    <input id="radio21" type="radio" class="construction_status @if(@$propertyDetails->property_type!='L' && @$propertyDetails->property_type!='FRMH') required @endif" name="construction_status" value="RM" @if(old('construction_status')=='RM' || @$propertyDetails->construction_status=='RM') checked @endif>
                                                    <span class="outer"><span class="inner"></span></span>Ready to Move
                                                </label>
                                            </li>
                                            <li>
                                                <label class="radio">
                                                        <input id="radio22" type="radio" class="construction_status @if(@$propertyDetails->property_type!='L' && @$propertyDetails->property_type!='FRMH') required @endif" name="construction_status" value="UC" @if(old('construction_status')=='UC'|| @$propertyDetails->construction_status=='UC') checked @endif>
                                                        <span class="outer"><span class="inner"></span></span>Under Construction
                                                </label>
                                            </li>
                                            <li>
                                                <label class="radio">
                                                        <input id="radio23" type="radio" class="construction_status @if(@$propertyDetails->property_type!='L' && @$propertyDetails->property_type!='FRMH') required @endif" name="construction_status" value="PL" @if(old('construction_status')=='PL'|| @$propertyDetails->construction_status=='PL') checked @endif>
                                                        <span class="outer"><span class="inner"></span></span>Pre-Launch
                                                </label>
                                            </li>

                                        </ul>
                                        <label id="construction_status-error" class="error" for="construction_status" style="display: none"></label>
                                    </div>
                                </div>
                                <div class="col-md-12 commercial_construction_status" @if(@$propertyDetails->master_property_type=='COM') style="display:block;" @else style="display:none;" @endif>
                                    <div class="redio_bx furn">
                                        <span>Construction Status :</span>
                                        <ul class="category-ul ul_radio">
                                            <li>
                                                <label class="radio">
                                                    <input id="radio21" type="radio" class="construction_status @if(@$propertyDetails->property_type!='L' && @$propertyDetails->property_type!='FRMH') required @endif" name="construction_status" value="RM" @if(@$propertyDetails->master_property_type=='COM')@if(old('construction_status')=='RM' || @$propertyDetails->construction_status=='RM') checked @endif @endif>
                                                    <span class="outer"><span class="inner"></span></span>Ready to Move
                                                </label>
                                            </li>
                                            <li>
                                                <label class="radio">
                                                        <input id="radio22" type="radio" class="construction_status @if(@$propertyDetails->property_type!='L' && @$propertyDetails->property_type!='FRMH') required @endif" name="construction_status" value="UC" @if(@$propertyDetails->master_property_type=='COM')@if(old('construction_status')=='UC'|| @$propertyDetails->construction_status=='UC') checked @endif @endif>
                                                        <span class="outer"><span class="inner"></span></span>Under Construction
                                                </label>
                                            </li>
                                            <li>
                                                <label class="radio">
                                                        <input id="radio23" type="radio" class="construction_status @if(@$propertyDetails->property_type!='L' && @$propertyDetails->property_type!='FRMH') required @endif" name="construction_status" value="CR" @if(@$propertyDetails->master_property_type=='COM')@if(old('construction_status')=='CR'|| @$propertyDetails->construction_status=='CR') checked @endif @endif>
                                                        <span class="outer"><span class="inner"></span></span>Construction Required
                                                </label>
                                            </li>

                                        </ul>
                                        <label id="construction_status-error" class="error" for="construction_status" style="display: none"></label>
                                    </div>
                                </div>
                                <div class="col-md-12 property_type_land" @if(@$propertyDetails->property_type=='L' || @$propertyDetails->property_type=='FRMH') style="display: none" @endif>
                                    <div class="redio_bx furn ">
                                        <span>Furnishing :</span>
                                        <ul class="category-ul ul_radio">
                                            <li>
                                                <label class="radio">
                                                    <input id="radio31" type="radio" class="furnishing @if(@$propertyDetails->property_type!='L' && @$propertyDetails->property_type!='FRMH') required @endif" name="furnishing" value="SF" @if(old('furnishing')=='SF' || @$propertyDetails->furnishing=='SF') checked @endif>
                                                    <span class="outer"><span class="inner"></span></span> Semi Furnished
                                                </label>
                                            </li>
                                            <li>
                                                <label class="radio">
                                                        <input id="radio32" type="radio" class="furnishing @if(@$propertyDetails->property_type!='L' && @$propertyDetails->property_type!='FRMH') required @endif" name="furnishing" value="FF" @if(old('furnishing')=='FF'|| @$propertyDetails->furnishing=='FF') checked @endif>
                                                        <span class="outer"><span class="inner"></span></span>Full Furnished
                                                </label>
                                            </li>
                                            <li>
                                                <label class="radio">
                                                        <input id="radio23" type="radio" class="furnishing @if(@$propertyDetails->property_type!='L' && @$propertyDetails->property_type!='FRMH') required @endif" name="furnishing" value="NF" @if(old('furnishing')=='NF'|| @$propertyDetails->furnishing=='NF') checked @endif>
                                                        <span class="outer"><span class="inner"></span></span>Not Furnished
                                                </label>
                                            </li>

                                        </ul>
                                        <label id="furnishing-error" class="error" for="furnishing" style="display: none"></label>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="bodar"></div>
                                </div>


                                <div class="col-md-4">
                                    <div class="das_input">
                                        <label>Country</label>
                                        <select name="country" id="country" class="required">
                                            <option value="">Select Country</option>
                                             <option value="101" selected>India</option>
                                             {{-- <option value="101" @if(old('country')==101 || @$propertyDetails->country==101 ) selected @endif selected>India</option> --}}
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="das_input">
                                        <label>State</label>

                                        <select name="state" id="states" class="required">
                                            <option value="">Select State</option>
                                            @foreach ( $states as $state)
                                            <option value="{{$state->id}}" @if(old('state')==$state->id || @$propertyDetails->state==$state->id ) selected @endif>{{$state->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="das_input">
                                        <label>City</label>
                                        <select name="city" id="city" class="required">
                                            <option value="">Select City</option>
                                            @foreach ( $cites as $city)
                                            <option value="{{$city->id}}" @if(old('city')==$city->id || @$propertyDetails->city==$city->id ) selected @endif>{{$city->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="das_input">
                                        <label>Locality</label>
                                        <input type="text" placeholder="Enter here.." name="locality" class="required" value="{{old('locality',@$propertyDetails->localityName->locality_name)}}" id="locality" onkeyup="search_result_check(this);" autocomplete="off">
                                        <label id="locality-error" class="error" for="locality" style="display: none"></label>
                                        <ul id="serch_result" class="search_sajasation" style="display: none"></ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="das_input">
                                        <label>Address</label>
                                        <input type="text" placeholder="Enter here.." name="address" class="required" value="{{old('address',@$propertyDetails->address)}}" id="address" autocomplete="off">
                                        {{--<input type="hidden" name="lat" id="lat" value="{{old('lat',@$propertyDetails->address_lat)}}">
                                        <input type="hidden" name="long" id="long" value="{{old('long',@$propertyDetails->address_long)}}">--}}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="das_input">
                                        <label>Post Code</label>
                                        <input type="text" placeholder="Enter here.." name="post_code" class="required" value="{{old('post_code',@$propertyDetails->post_code)}}" id="post_code"  maxlength="6">
                                    </div>
                                </div>
                                <div class="col-md-4 build-class" @if(@$propertyDetails->property_type == 'L' || @$propertyDetails->property_type == 'FRMH') style="display:none" @endif>
                                    <div class="das_input">
                                        <label>Build Year</label>
                                        @php
                                        $now = date('Y');
                                        $last= date('Y')-50;
                                        @endphp

                                        <select name="build_year" id="build_year" @if(@$propertyDetails->property_type != 'L' && @$propertyDetails->property_type != 'FRMH')
                                        class="required" @endif>
                                            <option value="">Select year</option>
                                            @for ($i = $now; $i >= $last; $i--)
                                            <option value="{{ $i }}"  @if(old('build_year')==$i || @$propertyDetails->build_year==$i ) selected @endif>{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="das_input">
                                        <label>Youtube Video Link</label>
                                        <input type="text" placeholder="Enter here.." name="video_link" class="" id="video_link" @if(@$propertyDetails->video_link) value="https://www.youtube.com/watch?v={{@$propertyDetails->video_link}}" @endif>
                                    </div>
                                    <label class="intro_video_error error error1" style="color: red"></label>
                                </div>

                                <div class="col-md-12">
                                    <div class="das_input">
                                        <label>Description </label>
                                        <textarea placeholder="Enter here.." name="description" class="required">{{old('description',@$propertyDetails->description)}}</textarea>

                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="bodar">
                                    </div>
                                </div>
                                <input type="hidden" name="" id="prop_for" value="{{@$propertyDetails->property_for}}">
                                <input type="hidden" name="" id="prop_type" value="{{@$propertyDetails->property_type}}">
                                <input type="hidden" name="" id="master_prop_type" value="{{@$propertyDetails->master_property_type}}">
                                <div class="col-md-12">
                                    <div class="usei_sub text-right">
                                        <input type="submit" value="Save & Continue " class="see_cc">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>

				</div>

			</div>


		</div>

	</div>
</div>


@endsection


@section('footer')
@include('includes.footer')
@endsection



@section('script')
@include('includes.script')
 {{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRZMuXnvy3FntdZUehn0IHLpjQm55Tz1E&libraries=places&callback=initAutocomplete" async defer></script>
<script>
    function initAutocomplete() {
        // Create the search box and link it to the UI element.
        var input = document.getElementById('address');

        var options = {
          types: ['establishment']
        };

        var input = document.getElementById('address');
        var autocomplete = new google.maps.places.Autocomplete(input, options);

        autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);

        autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();
            console.log(place)
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            $('#lat').val(place.geometry.location.lat());
            $('#long').val(place.geometry.location.lng());
            lat = place.geometry.location.lat();
            lng = place.geometry.location.lng();
            $('.exct_btn').show();

            initMap();
        });
        initMap();
    }
</script>

<script>

    function initMap() {
        geocoder = new google.maps.Geocoder();
        var lat = $('#lat').val();
        var lng = $('#long').val();
        var myLatLng = new google.maps.LatLng(lat, lng);
        // console.log(myLatLng);
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 16,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'Choose hotel location',
          draggable: true
        });

        google.maps.event.addListener(marker, 'dragend', function(evt,status){
        $('#lat').val(evt.latLng.lat());
        $('#long').val(evt.latLng.lng());
        var lat_1 = evt.latLng.lat();
        var lng_1 = evt.latLng.lng();
        var latlng = new google.maps.LatLng(lat_1, lng_1);
            geocoder.geocode({'latLng': latlng}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    $('#address').val(results[0].formatted_address);
                }
            });


        });
    }
    </script>--}} 
    {{-- <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC1A0Zjdpb5eWY6MCTp_8ZOVAlDkUB4MTY&callback=initMap">
    </script> --}}

<script>
    $(document).ready(function(){
        jQuery.validator.addMethod("validate_name", function(value, element) {
            if (/^([a-zA-Z0-9 ])+$/.test(value)) {
                 return true;
            } else {
                 return false;
            }
        }, "Allow only (a-z, A-Z, 0-9, )");
        jQuery.validator.addMethod("validate_address", function(value, element) {
            if (/^([a-zA-Z0-9./_ ,-])+$/.test(value)) {
                 return true;
            } else {
                 return false;
            }
        }, "Allow only (a-z, A-Z, 0-9, ./_ ,-)");
        jQuery.validator.addMethod("validate_location", function(value, element) {
            if (/^([a-zA-Z0-9./_ ,-])+$/.test(value)) {
                 return true;
            } else {
                 return false;
            }
        }, "Allow only (a-z, A-Z, 0-9, ./_ ,-)");
        jQuery.validator.addMethod("validate_description", function(value, element) {
            if (/^([a-zA-Z0-9./'_ ,-])+$/.test(value)) {
                return true;
            } else {
                return false;
            }
        }, "Allow only (a-z, A-Z, 0-9, ./'_ ,-)");
        $.validator.addMethod('greaterThan', function (value, element, param) {
            return this.optional(element) || parseInt(value) >= parseInt($(param).val());
        }, 'Budget to not small budget from');
        jQuery.validator.addMethod("validate_area", function(value, element) {
            var testEmail =   /^[0-9]{0,5}.[0-9]{0,2}$/
            if (testEmail.test(value)) {
                return true;
            } else {
                return false;
            }
        }, "Area maximum 5 digits and 2 decimal places.E.g. 12345.67");
        jQuery.validator.addMethod("validate_super_area", function(value, element) {
            var testEmail =   /^[0-9]{0,5}.[0-9]{0,2}$/
            if (testEmail.test(value)) {
                return true;
            } else {
                return false;
            }
        }, "Super Area maximum  5 digits and 2 decimal places.E.g. 12345.67");
        jQuery.validator.addMethod("validate_carpet_area", function(value, element) {
            var testEmail =   /^([0-9]{0,5}).[0-9]{0,2}$/
            if (testEmail.test(value)) {
                return true;
            } else {
                return false;
            }
        }, "Carpet Area maximum  5 digits and 2 decimal places.E.g. 12345.67");
        
        $('#addPropertyForm').validate({
            rules: {

                property_type:{
                    required:true
                },
                master_property_type:{
                    required:true
                },
                property_for:{
                    required:true
                },
                property_name:{
                    required: true,
                    validate_name:true
                },
                no_of_bedrooms:{
                    // required: true,
                    digits: true ,
                    min:0,
                    max:99,
                },
                bathroom:{
                    // required: true,
                    digits: true ,
                    min:0,
                    max:99,
                },
                floor:{
                    // required: true,
                    digits: true ,
                    // min:0,
                    max:99,
                },
                post_code:{
                    required: true,
                    digits: true ,
                    minlength:6,
                },
                budget_range_from:{
                    // required: true,
                    digits: true ,
                    min:1,
                    // max:function(){
                    //     if($('#budget_range_to').val()!=''){
                    //         return $('#budget_range_to').val();
                    //     }
                    // },
                },
                
                maintenance_charge:{
                    digits: true ,
                },
                deposit_amount:{
                    digits: true ,
                },
                area:{
                    // required: true,
                    number: true ,

                    // validate_number: true,
                   // digits: true,

                    // validate_area:true,
                    // min:0,
                },
                super_area:{
                    // required: true,
                    number: true ,

                    // validate_number: true,

                   // digits: true,

                    // validate_super_area:true,
                    // min:0,
                },
                carpet_area:{
                    // required: true,
                    number: true ,

                    // validate_number: true,

                    //digits: true,

                    // validate_carpet_area:true,
                    // min:0,
                },land_area:{
                    number:true,
                },
                address:{
                    required: true,
                    validate_address:true
                },
                description:{
                    required: true,
                   // validate_description:true,
                },
                location:{
                    required: true,
                    validate_location:true,
                },
            },
            messages: {
                construction_status:{
                    required: 'Select property construction Status',
                },
                furnishing:{
                    required: 'Select property furnishing Status',
                },
                property_type:{
                    required: 'Select property Type',
                },
                property_for:{
                    required: 'Select property For',
                },
                property_name:{
                    required: 'Enter property name',
                },
                no_of_bedrooms:{
                    required: 'Enter number of bedrooms',
                    digits: 'Bedroom only number ',
                    min:'Minimum bedroom 0 ',
                    max:'Maximum bedroom 99',
                },
                bathroom:{
                    required: 'Enter number of bathrooms',
                    digits: 'Bathroom only number ',
                    min:'Minimum bathroom 0 ',
                    max:'Maximum bathroom 99',
                },
                floor:{
                    required: 'Enter number of floor',
                    digits: 'Floor only number ',
                    // min:'Minimum floor 0 ',
                    max:'Maximum floor 99',
                },
                post_code:{
                    required: 'Enter post code',
                    digits: 'post code only number ',
                    minlength:'Post code exactly 6 digits',
                },
                budget_range_from:{
                    required: 'Enter budget from ',
                    digits: 'Budget from only number ',
                    min: 'Budget from start from 1',
                    max: 'Budget from not gater budget to',
                },
                budget_range_to:{
                    required: 'Enter budget to',
                    digits: 'Budget to only number ',
                    min: 'Budget to not small budget from',
                },
                deposit_amount:{
                    required: 'Enter deposit amount',
                    digits: 'Deposit amount only number ',
                },
                maintenance_charge:{
                    required: 'Enter maintenance charge',
                    digits: 'Maintenance charge only number ',
                },
                area:{
                    required: 'Enter area ',
                    number: 'Area only number ',
                    min:'Minimum area 0 ',
                    
                },
                super_area:{
                    required: 'Enter super area ',
                    number: 'Super area number ',
                    min:'Minimum super area 0 ',
                },
                carpet_area:{
                    required: 'Enter carpet area ',
                    number: 'Carpet area only number ',
                    min:'Minimum carpet area 0 ',
                },
                country:{
                    required:'Select Country',
                },
                build_year:{
                    required:'Select build year',
                },
                state:{
                    required:'Enter state',
                },
                city:{
                    required:'Enter city',
                },
                address:{
                    required:'Enter address',
                },
                description:{
                    required:'Enter property description',
                },
                locality:{
                    required:'Enter locality',
                },
            },
            ignore: [],
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
    //   $('#country').on('change',function(e){
    //     e.preventDefault();
    //     var id = $(this).val();

    //     $.ajax({
    //       url:'{{route('get.state')}}',
    //       type:'GET',
    //       data:{country:id,id:'{{auth()->user()->state}}'},
    //       success:function(data){
    //         console.log(data);
    //         $('#states').html(data.state);
    //       }
    //     })
    //   });

      $('#states').on('change',function(e){
        e.preventDefault();
        var id = $(this).val();
        $.ajax({
          url:'{{route('get.city')}}',
          type:'GET',
          data:{state:id,id:'{{auth()->user()->state}}'},
          success:function(data){
            console.log(data);
            $('#city').html(data.city);
          }
        })
      });
      $('#city').on('change',function(e){
        $('#locality-error').text('');
        $('#locality-error').css('display','none');
      })


    });
  </script>
  <script>
    function search_result_check(that) {
        var city =$('#city').val();
        if(city==null||city==''){
            $('#locality-error').text('Please select state & city first');
            $('#locality-error').css('display','block');
            $('#locality').val('');
        }else{
            $('#locality-error').text('');
            $('#locality-error').css('display','none');
            var $this = that;
            var name = $($this).val();
            console.log($($this).val());
        $.ajax({
            type: "POST",
            url:"{{route('get.locality')}}",
            data: {
                'name': name,
                '_token':'{{@csrf_token()}}'
            },
            success: function(data) {
              if(data){
                console.log(data);
                $('#serch_result').show();
                $('#serch_result').fadeIn();
                $('#serch_result').last().html(data);

              }
              else
              {
                $('#serch_result').children().remove();
              }
            }
        });
        }

    }
    function add_searchbar(that) {
        var $this = that;
        var name = $($this).text();
        console.log(name);
        $('#locality').val(name);
        $($this).remove();
        $('#serch_result').children().remove();
        $('#serch_result').hide();
    }
</script>
<script>

    $('.property_type:radio').on('change', function() {
        var property_type=$(this).val();
        $('#prop_type').val(property_type);
        var prop_type = $('#prop_type').val();
        var prop_for = $('#prop_for').val();
       
        if((prop_type == 'L' || prop_type == 'FRMH') && prop_for == 'R'){
            $('.new-remove-office-class').hide();
            $('.new-office-class').hide();
            $('#office_class').removeClass('required');
            $('#no_of_bedrooms').removeClass('required');
            $('#no_of_room').removeClass('required'); 
            $('#bathroom').removeClass('required');
            $('.property_type_land').hide();
            $('input[name=furnishing]').removeClass('required');
            $('input[name=construction_status]').removeClass('required');
            $('input[name=bathroom]').removeClass('required');
            $('input[name=floor]').removeClass('required');
            $('.new-rent-class').slideUp();
            $('#rent_payment').removeClass('required');
            $('#preference').removeClass('required');
            $('#available_from').removeClass('required');
            $('#deposit_amount').removeClass('required');
            $('#maintenance_charge').removeClass('required');
            $('.area-class').slideUp();
            $('#super_area').removeClass('required');
            $('#carpet_area').removeClass('required');
            $('.build-class').slideUp();
            $('#build_year').removeClass('required');
            $('.land-area-class').slideDown();
            $('#land_area').addClass('required');
            $('#land_area_unit').addClass('required');
            $('.t-area-class').slideUp();
            $('#area').removeClass('required');
            // $('#budget_range_from').removeClass('required');
            // $('#budget_range_from').slideUp();
            // $('.price_tab').hide();
        }
        else if(prop_type == 'WRH' || prop_type == 'SHR' || prop_type == 'INDT' || prop_type == 'RET'){

            $('.area-class').slideUp();
            $('#super_area').removeClass('required');
            $('#carpet_area').removeClass('required');
            $('.new-remove-room-class').show();
            $('#no_of_room').addClass('required');
            $('.new-remove-office-class').hide();
            $('#no_of_bedrooms').removeClass('required');
            $('.new-remove-office-class').hide();
            $('.new-office-class').hide();
            $('#office_class').removeClass('required');
        }
        else if(prop_type == 'OSP' && prop_for == 'R'){
            $('.new-remove-office-class').hide();
            $('.new-office-class').slideDown();
            $('#office_class').addClass('required');
            $('.new-rent-class').slideUp();
            $('#rent_payment').removeClass('required');
            $('#preference').removeClass('required');
            $('#available_from').removeClass('required');
            $('#deposit_amount').removeClass('required');
            $('#maintenance_charge').removeClass('required');
            $('.build-class').slideDown();
            $('#build_year').addClass('required');
            $('.land-area-class').slideUp();
            $('#land_area').removeClass('required');
            $('#land_area_unit').removeClass('required');
            $('.t-area-class').slideDown();
            $('#area').addClass('required');
            $('#area').attr('maxlength','10');
            // $('#budget_range_from').addClass('required');
            // $('#budget_range_from').slideDown();
            // $('.price_tab').show();
            $('#no_of_bedrooms').removeClass('required');
            $('.area-class').slideDown();
            $('#super_area').addClass('required');
            $('#carpet_area').addClass('required');
            $('.new-remove-room-class').hide();
            $('#no_of_room').removeClass('required'); 
        }else if((prop_type == 'H' && prop_for == 'R') || (prop_type == 'F' && prop_for == 'R') || (prop_type == 'GTCM' && prop_for == 'R')){
            $('.new-rent-class').slideDown();
            $('#rent_payment').addClass('required');
            $('#preference').addClass('required');
            $('#available_from').addClass('required');
            $('#deposit_amount').addClass('required');
            $('#maintenance_charge').addClass('required');
            $('.area-class').slideDown();
            $('#super_area').addClass('required');
            $('#carpet_area').addClass('required'); 
            $('.new-remove-office-class').show();
            $('.new-remove-office-class').slideDown();
            $('.property_type_land').slideDown();
            $('#no_of_bedrooms').addClass('required');
            $('.new-office-class').slideUp();
            $('#office_class').removeClass('required');
            $('#bathroom').addClass('required');
            $('.build-class').slideDown();
            $('#build_year').addClass('required');
            $('.land-area-class').slideUp();
            $('#land_area').removeClass('required');
            $('#land_area_unit').removeClass('required');
            $('.t-area-class').slideDown();
            $('#area').addClass('required');
            $('#area').attr('maxlength','10');
            // $('#budget_range_from').removeClass('required');
            // $('#budget_range_from').slideUp();
            // $('.price_tab').hide();
            $('.new-remove-room-class').hide();
            $('#no_of_room').removeClass('required');

        }
        else if((prop_type == 'WRH' && prop_for == 'R') || (prop_type == 'SHR' && prop_for == 'R') || (prop_type == 'INDT' && prop_for == 'R') || (prop_type == 'RET' && prop_for == 'R')){

            $('.new-rent-class').slideDown();
            $('#rent_payment').addClass('required');
            $('#preference').addClass('required');
            $('#available_from').addClass('required');
            $('#deposit_amount').addClass('required');
            $('#maintenance_charge').addClass('required');
            
            $('.new-remove-office-class').hide();
            $('.new-remove-office-class').slideUp();
            $('.property_type_land').slideDown();
            $('#no_of_bedrooms').removeClass('required');
            $('.new-office-class').slideUp();
            $('#office_class').removeClass('required');
            $('#bathroom').addClass('required');
            $('.build-class').slideDown();
            $('#build_year').addClass('required');
            $('.land-area-class').slideUp();
            $('#land_area').removeClass('required');
            $('#land_area_unit').removeClass('required');
            $('.t-area-class').slideDown();
            $('#area').addClass('required');
            $('#area').attr('maxlength','10');
            // $('#budget_range_from').removeClass('required');
            // $('#budget_range_from').slideUp();
            // $('.price_tab').hide();
            //$('.new-remove-room-class').hide();
            $('#no_of_room').addClass('required');
        }

        else if(property_type=='OSP'){
            $('.new-remove-office-class').hide();
            $('.new-office-class').slideDown();
            $('#office_class').addClass('required');
            $('.property_type_land').slideDown();
            $('#no_of_bedrooms').removeClass('required');
            // $('#bathroom').removeClass('required');
            $('#bathroom').addClass('required');
            $('input[name=furnishing]').addClass('required');
            $('input[name=construction_status]').addClass('required');
            $('input[name=floor]').addClass('required');
            $('.new-rent-class').slideUp();
            $('#rent_payment').removeClass('required');
            $('#preference').removeClass('required');
            $('#available_from').removeClass('required');
            $('#deposit_amount').removeClass('required');
            $('#maintenance_charge').removeClass('required');
            $('.build-class').slideDown();
            $('#build_year').addClass('required');
            $('.land-area-class').slideUp();
            $('#land_area').removeClass('required');
            $('#land_area_unit').removeClass('required');
            $('.t-area-class').slideDown();
            $('#area').addClass('required');
            $('#area').attr('maxlength','10');

            $('#budget_range_from').addClass('required');
            $('#budget_range_from').slideDown();
            $('.price_tab').show();
            $('.commercial_construction_status').show();
            $('.pro_commercial').hide();
            $('.area-class').slideDown();
            $('#super_area').addClass('required');
            $('#carpet_area').addClass('required');
            $('.new-remove-room-class').hide();
            $('#no_of_room').removeClass('required'); 

        }else if(property_type=='L' || property_type=='FRMH')
        {
            $('.new-remove-office-class').hide();
            $('.new-office-class').hide();
            $('#office_class').removeClass('required');
            $('#no_of_bedrooms').removeClass('required');
            $('#bathroom').removeClass('required');
            $('.property_type_land').hide();
            $('input[name=furnishing]').removeClass('required');
            $('input[name=construction_status]').removeClass('required');
            $('input[name=bathroom]').removeClass('required');
            $('input[name=floor]').removeClass('required');
            $('.new-rent-class').slideUp();
            $('#rent_payment').removeClass('required');
            $('#preference').removeClass('required');
            $('#available_from').removeClass('required');
            $('#deposit_amount').removeClass('required');
            $('#maintenance_charge').removeClass('required');
            $('.area-class').slideUp();
            $('#super_area').removeClass('required');
            $('#carpet_area').removeClass('required');
            $('.build-class').slideUp();
            $('#build_year').removeClass('required');
            $('.land-area-class').slideDown();
            $('#land_area').addClass('required');
            $('#land_area_unit').addClass('required');
            $('.t-area-class').slideUp();
            $('#area').removeClass('required');
            $('#area').attr('maxlength','100');

            $('#budget_range_from').addClass('required');
            $('#budget_range_from').slideDown();
            $('.price_tab').show();
            $('.new-remove-room-class').hide();
            $('#no_of_room').removeClass('required');

        }
        else{
            $('.new-office-class').hide();
            $('.new-remove-office-class').slideDown();
            $('.property_type_land').slideDown();
            $('#office_class').removeClass('required');
            $('#no_of_bedrooms').addClass('required');
            $('#bathroom').addClass('required');
            $('input[name=furnishing]').addClass('required');
            $('input[name=construction_status]').addClass('required');
            $('input[name=floor]').addClass('required');
            $('.area-class').slideDown();
            $('#super_area').addClass('required');
            $('#carpet_area').addClass('required');
            $('.build-class').slideDown();
            $('#build_year').addClass('required');
            $('.land-area-class').slideUp();
            $('#land_area').removeClass('required');
            $('#land_area_unit').removeClass('required');
            $('.t-area-class').slideDown();
            $('#area').addClass('required');
            $('#area').attr('maxlength','10');
            $('#budget_range_from').addClass('required');
            $('#budget_range_from').slideDown();
            $('.price_tab').show();
            $('#no_of_room').removeClass('required'); 
            
        }
    });
    $('.property_for:radio').on('change', function() {
        var property_for=$(this).val();
        $('#prop_for').val(property_for);
        var prop_type = $('#prop_type').val();
        var prop_for = $('#prop_for').val();
        
        
        if((prop_type == 'L' || prop_type == 'FRMH') && prop_for == 'R'){
            $('.new-rent-class').slideUp();
            $('#rent_payment').removeClass('required');
            $('#preference').removeClass('required');
            $('#available_from').removeClass('required');
            $('#deposit_amount').removeClass('required');
            $('#maintenance_charge').removeClass('required');
            $('.build-class').slideUp();
            $('#build_year').removeClass('required');
            $('.land-area-class').slideDown();
            $('#land_area').addClass('required');
            $('#land_area_unit').addClass('required');
            $('.t-area-class').slideUp();
            $('#area').removeClass('required');
            // $('#budget_range_from').removeClass('required');
            // $('#budget_range_from').slideUp();
            // $('.price_tab').hide();
            $('.new-remove-room-class').hide();
            $('#no_of_room').removeClass('required');

        }else if(prop_type == 'OSP' && prop_for == 'R'){
            $('.new-rent-class').slideUp();
            $('#rent_payment').removeClass('required');
            $('#preference').removeClass('required');
            $('#available_from').removeClass('required');
            $('#deposit_amount').removeClass('required');
            $('#maintenance_charge').removeClass('required');
            $('.build-class').slideDown();
            $('#build_year').addClass('required');
            $('.land-area-class').slideUp();
            $('#land_area').removeClass('required');
            $('#land_area_unit').removeClass('required');
            $('.t-area-class').slideDown();
            $('#area').addClass('required');
            $('#area').attr('maxlength','10');
            $('#office_class').addClass('required');
            // $('#budget_range_from').addClass('required');
            // $('#budget_range_from').slideDown();
            // $('.price_tab').show();
            $('#no_of_bedrooms').removeClass('required');
            $('.area-class').slideDown();
            $('#super_area').addClass('required');
            $('#carpet_area').addClass('required');
            $('#no_of_room').removeClass('required');  
        }
        else if((prop_type == 'WRH' && prop_for == 'R') || (prop_type == 'SHR' && prop_for == 'R') || (prop_type == 'INDT' && prop_for == 'R') || (prop_type == 'RET' && prop_for == 'R')){

            $('.new-rent-class').slideDown();
            $('#rent_payment').addClass('required');
            $('#preference').addClass('required');
            $('#available_from').addClass('required');
            $('#deposit_amount').addClass('required');
            $('#maintenance_charge').addClass('required');
           
            $('.new-remove-office-class').hide();
            $('.new-remove-office-class').slideUp();
            $('.property_type_land').slideDown();
            $('#no_of_bedrooms').removeClass('required');
            $('.new-office-class').slideUp();
            $('#office_class').removeClass('required');
            $('#bathroom').addClass('required');
            $('.build-class').slideDown();
            $('#build_year').addClass('required');
            $('.land-area-class').slideUp();
            $('#land_area').removeClass('required');
            $('#land_area_unit').removeClass('required');
            $('.t-area-class').slideDown();
            $('#area').addClass('required');
            $('#area').attr('maxlength','10');
            // $('#budget_range_from').removeClass('required');
            // $('#budget_range_from').slideUp();
            // $('.price_tab').hide();
            //$('.new-remove-room-class').hide();
            $('#no_of_room').addClass('required');
            $('.commercial_construction_status').show();
            $('.pro_commercial').hide();
        }
        else if(property_for=='R'){
            $('.new-rent-class').slideDown();
            $('#rent_payment').addClass('required');
            $('#preference').addClass('required');
            $('#available_from').addClass('required');
            $('#deposit_amount').addClass('required');
            $('#maintenance_charge').addClass('required');
            $('#no_of_bedrooms').addClass('required');
            $('#area').attr('maxlength','10');
            // $('#budget_range_from').removeClass('required');
            // $('#budget_range_from').slideUp();
            // $('.price_tab').hide();

        }else{
            $('.new-rent-class').slideUp();
            $('#rent_payment').removeClass('required');
            $('#preference').removeClass('required');
            $('#available_from').removeClass('required');
            $('#deposit_amount').removeClass('required');
            $('#maintenance_charge').removeClass('required');
            $('#budget_range_from').addClass('required');
            $('#budget_range_from').slideDown();
            $('.price_tab').show();
        }
    });

    function addCommas(numberString) {
        numberString += '';
  var x = numberString.split('.'),
      x1 = x[0],
      x2 = x.length > 1 ? '.' + x[1] : '',
      rgxp = /(\d+)(\d{3})/;

  while (rgxp.test(x1)) {
    x1 = x1.replace(rgxp, '$1' + ',' + '$2');
  }

  return x1 + x2;
}

function divider1(number_of_digits) {
    var tens="1";
    if(number_of_digits>8)
    return 10000000;
    while((number_of_digits-1)>0)
    {
        tens+="0";
        number_of_digits--;
    }
    return tens;
}

    $('#budget_range_from').keyup(function(){
        var from_value=$(this).val();
        var from_length=from_value.length;
        if(from_length<4){
            $('#budget_range_from_p').html('<span>₹</span>'+ from_value)
            console.log(from_value);
        }

        if(from_length==4||from_length==5){
            if(from_value%1000!=0){
                var fraction=from_value/1000;
                $('#budget_range_from_p').html('<span>₹</span>'+ fraction.toFixed(2)+'k')
                console.log(fraction.toFixed(2)+'k');
            }else{
                var fraction=from_value/1000;
                $('#budget_range_from_p').html('<span>₹</span>'+ fraction+'k')
                console.log(fraction+'k');
            }
        }
        if(from_length==6||from_length==7){
            if(from_value%100000!=0){
                var fraction=from_value/100000;
                $('#budget_range_from_p').html('<span>₹</span>'+ fraction.toFixed(2)+'Lakhs')
                console.log(fraction.toFixed(2)+'Lakhs');
            }else{
                var fraction=from_value/100000;
                $('#budget_range_from_p').html('<span>₹</span>'+ fraction.toFixed(2)+'Lakhs')
                console.log(fraction+'Lakhs');
            }
        }
        if(from_length==8||from_length==9||from_length==10){

            if(from_value%10000000!=0){
                var fraction=from_value/10000000;
                $('#budget_range_from_p').html('<span>₹</span>'+ fraction.toFixed(2)+'Cr')
                console.log(fraction.toFixed(2)+'Cr');
            }else{
                var fraction=from_value/10000000;
                $('#budget_range_from_p').html('<span>₹</span>'+ fraction+'Cr')
                console.log(fraction+'Cr');
            }
        }

    })
    // $('#budget_range_to').keyup(function(){
    //     var to_value=$(this).val();
    //     var to_length=to_value.length;
    //     if(to_length<4){
    //         $('#budget_range_to_p').html('<span>₹</span>'+ to_value)
    //         console.log(to_value);
    //     }

    //     if(to_length==4||to_length==5){
    //         if(to_value%1000!=0){
    //             var fraction=to_value/1000;
    //             $('#budget_range_to_p').html('<span>₹</span>'+ fraction.toFixed(2)+'k')
    //             console.log(fraction.toFixed(2)+'k');
    //         }else{
    //             var fraction=to_value/1000;
    //             $('#budget_range_to_p').html('<span>₹</span>'+ fraction+'k')
    //             console.log(fraction+'k');
    //         }
    //     }
    //     if(to_length==6||to_length==7){
    //         if(to_value%100000!=0){
    //             var fraction=to_value/100000;
    //             $('#budget_range_to_p').html('<span>₹</span>'+ fraction.toFixed(2)+'Lakhs')
    //             console.log(fraction.toFixed(2)+'Lakhs');
    //         }else{
    //             var fraction=to_value/100000;
    //             $('#budget_range_to_p').html('<span>₹</span>'+ fraction.toFixed(2)+'Lakhs')
    //             console.log(fraction+'Lakhs');
    //         }
    //     }
    //     if(to_length==8||to_length==9||to_length==10){

    //         if(to_value%10000000!=0){
    //             var fraction=to_value/10000000;
    //             $('#budget_range_to_p').html('<span>₹</span>'+ fraction.toFixed(2)+'Cr')
    //             console.log(fraction.toFixed(2)+'Cr');
    //         }else{
    //             var fraction=to_value/10000000;
    //             $('#budget_range_to_p').html('<span>₹</span>'+ fraction+'Cr')
    //             console.log(fraction+'Cr');
    //         }
    //     }

    // })
    $('#available_from').datepicker({
        minDate: new Date(),
    });
</script>
<script>
        function ytVidId(url) {
        var p = /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
        return (url.match(p)) ? RegExp.$1 : false;
    }

    $('#video_link').bind("change", function() {

        var url = $(this).val();
        if(url != ''){

            if (ytVidId(url) !== false) {
            $('.intro_video_error').html('');
            } else {
            $(".intro_video_error").html('Youtube Link invalid');
            $('#video_link').val('');
            }

        }
        
    });
</script>
<script>
    $('.master_property_type:radio').on('change', function(){

        var master_property_type=$(this).val();
        $('#master_prop_type').val(master_property_type);
        var master_prop_type = $('#master_prop_type').val();
        
        if(master_prop_type == 'RES'){

            $('.residential_property').show();
            $('.commercial_property').hide(); 
        }
        else if(master_prop_type == 'COM'){

            $('.residential_property').hide();
            $('.commercial_property').show(); 
        }
        if(master_prop_type == 'COM'){

            $('.area-class').slideUp();
            $('#super_area').removeClass('required');
            $('#carpet_area').removeClass('required');
            $('.new-remove-room-class').show();
            $('#no_of_room').addClass('required');
            $('.new-remove-office-class').hide();
            $('#no_of_bedrooms').removeClass('required');
            $('.commercial_construction_status').show();
            $('.pro_commercial').hide();

        }
        else if(master_prop_type == 'RES'){

            $('.area-class').slideDown();
            $('#super_area').addClass('required');
            $('#carpet_area').addClass('required');
            $('.commercial_construction_status').hide();
            $('.pro_commercial').show();
        }
       

           
    })
 
</script>
@endsection

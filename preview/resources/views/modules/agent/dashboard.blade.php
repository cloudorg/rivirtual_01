@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> RiVirtual | Dashbord </title>
@endsection




@section('header')
@include('includes.header')
@endsection


@section('content')
<!----header--->
<div class="haeder-padding"></div>

<div class="dashboard-agent">
	<div class="container">
		<div class="row">
			@include('includes.sidebar')

			<div class="col-lg-9 col-md-12 col-sm-12">
				<div class="cus-dashboard-right">
				   <h2>Dashboard</h2>
				</div>
				<div class="agent-dash-right">
                    @if(auth()->user()->approval_status==null)
                    <center><p class="alert alert-info">Complete Your Profile  <a href="{{route('agent.profile')}}"> click here</a></p></center>
                    @endif

					<div class="agent-right-body">
						<div class="prov-statis">
								<div class="row">
									<div class="col-lg-4 col-md-4 col-sm-6">
										<div class="stats-box">
											<div>
												<p>Membership Expiry on </p>
												@if(@Auth::user()->package_expiry_date)
												<h5>{{ date('jS M,Y',strtotime(@Auth::user()->package_expiry_date))}}</h5>
												@else
												<h5>NA</h5>
												@endif
											</div>
											<img src="{{ URL::to('public/frontend/images/mems.png')}}">
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-6">
										<div class="stats-box">
											<div>
												<p>Property Posted </p>
												<h5>{{ @$total_property }}</h5>
											</div>
											<img src="{{ URL::to('public/frontend/images/memsc.png')}}">
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-6">
										<div class="stats-box">
											<div>
												<p>New Messages </p>
												<h5>0</h5>
											</div>
											<img src="{{ URL::to('public/frontend/images/memsd.png')}}">
										</div>
									</div>
								</div>
							</div>


					<div>

						<div>
							<div class="user_statis mt-4">
							<em>
								<i class="icofont-ui-calendar"></i>
							</em>
							<p> Upcoming Visits : <span> {{count($visit_req)}} </span></p>
							</div>
						</div>

						<div class="custom-table ">
										<div class="new-table-mr">
											<div class="table">
												<div class="one_row1 hidden-sm-down only_shawo">
													<div class="cell1 tab_head_sheet">Property	</div>
													<div class="cell1 tab_head_sheet">Agent</div>
													<div class="cell1 tab_head_sheet">Date </div>
													<div class="cell1 tab_head_sheet">Time </div>
													<div class="cell1 tab_head_sheet">Location </div>

													<div class="cell1 tab_head_sheet">Action </div>
												</div>
												<!--row 1-->
												<!--row 1-->
												<!--row 1-->
												@if(@$visit_req)
													@foreach(@$visit_req as $val)
													<div class="one_row1 small_screen31">
														<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Property	</span>
															<p class="add_ttrr"> <a href="{{ route('search.property.details',['slug'=>@$val->propertyDetails->slug]) }}">
																@if(strlen(@$val->propertyDetails->name)>20)
																	{{substr($val->propertyDetails->name,0,20)}}...
																@else
																	{{@$val->propertyDetails->name}}
																@endif
															</a></p>
														</div>
														<div class="cell1 tab_head_sheet_1 half-boxes agents"> <span class="W55_1">Agent</span>
															<p class="add_ttrr"><a href="javascript:;">{{@$val->agentDetails->name}}</a></p>
														</div>
														<div class="cell1 tab_head_sheet_1 half-boxes dates"> <span class="W55_1">Date</span>
															<p class="add_ttrr">{{date('m.d.Y', strtotime(@$val->visit_date))}}</p>
														</div>
														<div class="cell1 tab_head_sheet_1 half-boxes dates"> <span class="W55_1">Time</span>
															<p class="add_ttrr">{{date('H:i 
																A', strtotime(@$val->visit_date))}}</p>
														</div>
														<div class="cell1 tab_head_sheet_1 dates"> <span class="W55_1">Location </span>
															<p class="add_ttrr">{{@$val->address}}</p>
														</div>
														<div class="cell1 tab_head_sheet_1 "> <span class="W55_1">Action</span>
                                                        <div class="add_ttrr actions-main text-center">
                                                            <a href="javascript:void(0);" class="action-dots" id="action{{ @$val->id }}" data-id="{{ @$val->id }}"><img src="{{ url('public/frontend/./images/action-dots.png') }}" alt=""></a>
                                                            <div class="show-actions" id="show-action{{ @$val->id }}" style="display: none;"> <span class="angle"><img src="{{ url('public/frontend/./images/angle.png') }}" alt=""></span>
                                                                <ul class="text-left">
                                                                    <li><a data-toggle="modal" data-target="#myModal1" data-id="{{@$val->id}}" class="rsch">Reschedule </a></li>
                                                                    <!-- <li><a href="javascript:;">Cancel</a></li> -->
                                                                    <li><a href="{{ route('agent.visit.request.cancel',['id'=>@$val->id]) }}" onclick="return confirm('Are you want to cancel this visit request?')">Cancel</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
													</div>
													@endforeach
												@else
												<div class="one_row1 small_screen31">
													No data found
												</div>
												@endif
												<!--row 1-->
												<!-- <div class="one_row1 small_screen31">
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Property	</span>
														<p class="add_ttrr"> <a href="property-details.html">Pacific Heights Area</a></p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes agents"> <span class="W55_1">Agent</span>
														<p class="add_ttrr"><a href="agent-public-profile.html">Aiden Benjamin</a></p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes dates"> <span class="W55_1">Date</span>
														<p class="add_ttrr">01.10.2021</p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes dates"> <span class="W55_1">Time</span>
														<p class="add_ttrr">05:15pm</p>
													</div>
													<div class="cell1 tab_head_sheet_1 dates"> <span class="W55_1">Location </span>
														<p class="add_ttrr">Kerala</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
														<div class="add_ttrr actions-main text-center">
															<a href="javascript:void(0);" class="action-dots" id="action29"><img src="{{ URL::to('public/frontend/images/action-dots.png')}}" alt=""></a>
															<div class="show-actions" id="show-action29" style="display: none;"> <span class="angle"><img src="{{ URL::to('public/frontend/images/angle.png')}}" alt=""></span>
																<ul class="text-left">
																	<li><a data-toggle="modal" data-target="#myModal">Reschedule </a></li>
																	<li><a href="#">Cancel</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
												<div class="one_row1 small_screen31">
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Property	</span>
														<p class="add_ttrr"> <a href="property-details.html">Pacific Heights Area</a></p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes agents"> <span class="W55_1">Agent</span>
														<p class="add_ttrr"><a href="agent-public-profile.html">Aiden Benjamin</a></p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes dates"> <span class="W55_1">Date</span>
														<p class="add_ttrr">01.10.2021</p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes dates"> <span class="W55_1">Time</span>
														<p class="add_ttrr">06:15pm</p>
													</div>
													<div class="cell1 tab_head_sheet_1 dates"> <span class="W55_1">Location </span>
														<p class="add_ttrr">Mumbai</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
														<div class="add_ttrr actions-main text-center">
															<a href="javascript:void(0);" class="action-dots" id="action30"><img src="{{ URL::to('public/frontend/images/action-dots.png')}}" alt=""></a>
															<div class="show-actions" id="show-action30" style="display: none;"> <span class="angle"><img src="{{ URL::to('public/frontend/images/angle.png')}}" alt=""></span>
																<ul class="text-left">
																	<li><a data-toggle="modal" data-target="#myModal">Reschedule </a></li>
																	<li><a href="#">Cancel</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
												<div class="one_row1 small_screen31">
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Property	</span>
														<p class="add_ttrr"> <a href="property-details.html">Pacific Heights Area</a></p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes agents"> <span class="W55_1">Agent</span>
														<p class="add_ttrr"><a href="agent-public-profile.html">Aiden Benjamin</a></p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes dates"> <span class="W55_1">Date</span>
														<p class="add_ttrr">01.10.2021</p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes dates"> <span class="W55_1">Time</span>
														<p class="add_ttrr">02:15pm</p>
													</div>
													<div class="cell1 tab_head_sheet_1 dates"> <span class="W55_1">Location </span>
														<p class="add_ttrr">Hyderabad</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
														<div class="add_ttrr actions-main text-center">
															<a href="javascript:void(0);" class="action-dots" id="action4"><img src="{{ URL::to('public/frontend/images/action-dots.png')}}" alt=""></a>
															<div class="show-actions" id="show-action4" style="display: none;"> <span class="angle"><img src="{{ URL::to('public/frontend/images/angle.png')}}" alt=""></span>
																<ul class="text-left">
																	<li><a data-toggle="modal" data-target="#myModal">Reschedule </a></li>
																	<li><a href="#">Cancel</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>

												<div class="one_row1 small_screen31">
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Property	</span>
														<p class="add_ttrr"> <a href="property-details.html">Pacific Heights Area</a></p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes agents"> <span class="W55_1">Agent</span>
														<p class="add_ttrr"><a href="agent-public-profile.html">Aiden Benjamin</a></p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes dates"> <span class="W55_1">Date</span>
														<p class="add_ttrr">21.09.2021</p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes dates"> <span class="W55_1">Time</span>
														<p class="add_ttrr">02:15pm</p>
													</div>
													<div class="cell1 tab_head_sheet_1 dates"> <span class="W55_1">Location </span>
														<p class="add_ttrr">Punjab</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
														<div class="add_ttrr actions-main text-center">
															<a href="javascript:void(0);" class="action-dots" id="action5"><img src="{{ URL::to('public/frontend/images/action-dots.png')}}" alt=""></a>
															<div class="show-actions" id="show-action5" style="display: none;"> <span class="angle"><img src="{{ URL::to('public/frontend/images/angle.png')}}" alt=""></span>
																<ul class="text-left">
																	<li><a data-toggle="modal" data-target="#myModal">Reschedule </a></li>
																	<li><a href="#">Cancel</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div> -->
											</div>
										</div>
									</div>
					</div>

					<div class="clearfix"></div>


					</div>
				</div>

			</div>
		</div>

	</div>
</div>
<div class="modal res-modal" id="myModal1">
        <div class="modal-dialog">
          <div class="modal-content">
          
            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title">Reschedule Property Visits</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            
            <!-- Modal body -->
            <div class="modal-body">
                <form action="{{route('agent.reschedule.availability')}}" method="POST" id="myForm">
                    @csrf
                    <input type="hidden" name="id" id="id">
                    <div class="appome_input das_input">
                        <label>Alternative Date:</label>
                            <div class="dash-d">
                                <input type="text" placeholder="Select Date" id="datepicker3" name="date" readonly="readonly">
                                <span class="over_llp1"><img src="{{ url('public/frontend/images/cala.png') }}"></span>
                            </div>
                    </div>
                    <div class="appome_input das_input">
                        <label>Select Time</label>
                            <div class="dash-d">
                                <input type="text" placeholder="Select Time" name="time" readonly="readonly">
                                <span class="over_llp1"><img src="{{ url('public/frontend/images/clock.png') }}"></span>
                            </div>
                    </div>
                                            
                
            </div>
            
            <!-- Modal footer -->
            <div class="modal-footer">
                <input type="submit" name="" value="Save" class="btn btn-danger">
              <!-- <button type="submit" class="btn btn-danger" data-dismiss="modal">Save</button> -->
            </div>
            </form>
          </div>
        </div>
    </div>

@endsection


@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection



@section('script')
@include('includes.script')
<link rel='stylesheet' href='css/jquery-clockpicker.min.css'>
<script src='https://weareoutman.github.io/clockpicker/dist/jquery-clockpicker.min.js'></script>
<script>
	$(document).on('click', function () {
		@foreach(@$visit_req as $val)
		var $target = $(event.target);
		if (!$target.closest('#action{{@$val->id}}').length && $('#show-action{{@$val->id}}').is(":visible")) {
			$('#show-action{{@$val->id}}').slideUp();
		}
		@endforeach
	});

	$(document).ready(function() {
		@foreach(@$visit_req as $val)
			$("#action{{@$val->id}}").click(function() {
				$("#show-action{{@$val->id}}").slideToggle();
			});
		@endforeach
    });
</script>
<script>
    $('.rsch').click(function(){
        var id = $(this).data('id');
        $('#id').val(id);
    });
</script>
<script>
    $("input[name=time]").clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        default: 'now',
        donetext: "Select",
        init: function() {
            console.log("colorpicker initiated");
        },
        beforeShow: function() {
            console.log("before show");
        },
        afterShow: function() {
            console.log("after show");
        },
        beforeHide: function() {
            console.log("before hide");
        },
        afterHide: function() {
            console.log("after hide");
        },
        beforeHourSelect: function() {
            console.log("before hour selected");
        },
        afterHourSelect: function() {
            console.log("after hour selected");
        },
        beforeDone: function() {
            console.log("before done");
        },
        afterDone: function() {
            console.log("after done");
        }
    });
    </script>
@endsection

@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
<link href="{{ URL::asset('public/frontend/croppie/croppie.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('public/frontend/croppie/croppie.min.css') }}" rel="stylesheet" />
@endsection

@section('title')
<title> RiVirtual | Subcription </title>
@endsection




@section('header')
@include('includes.header')
@endsection

@section('content')

<div class="haeder-padding"></div>


<div class="dashboard-agent">
	<div class="container">
		<div class="row">
        @include('includes.sidebar')
			<div class="col-lg-9 col-md-12 col-sm-12">
				<div class="cus-dashboard-right">
				   <h2>Subscription</h2> 
				</div>
				<div class="agent-dash-right">
					
					<div class="agent-right-body">
                      @include('includes.message')
                        <div class="row">
                        	<div class="col-md-12">
                        		<div class="meb_shptxt">
						            <label><img src="{{ url('public/frontend/images/member.png') }}">
						            	My Current Membership is : <span>{{ @$currentPackage->UserPackageName->package_name }}</span></label>
						        </div>
						        <div class="meb_shptxt" style="float:left !important;">
						            <label><img src="{{ url('public/frontend/images/member.png') }}">
						            	My Current Membership expiry date is : <span>{{ date('jS M,Y',strtotime(@$user->package_expiry_date))}}</span></label>
						        </div>
                        	</div>
                             
                        	<div class=" col-sm-6">
                        		<div class="member-box">
                        			<div class="ribbon2 ribbon-top-left"><strong>Popular</strong></div>
                        			<h2>{{ @$defaultPackage->package_name }}</h2>
                        			<h3>Free for all </h3>
                        			<h5><img src="{{ url('public/frontend/images/tick3.png') }}"> Lorem Ipsum is simply text </h5>
                        			<p>
										@if(strlen(@$defaultPackage->package_desc)>200)
										 {!! substr(@$defaultPackage->package_desc,0,200) . '...' !!}
										 @else
										 {!! @$defaultPackage->package_desc !!}
										 @endif
									</p>
									@if(@$currentPackage->agent_package_id == 1)
                        			<a href="javascript:;" class="member_btn mem_active">Active</a>
									@elseif(@$currentPackage->agent_package_id != 1)
									<a href="{{ route('agent.set.subscription',['id'=>1]) }}" class="member_btn " onclick="return confirm('Are you want to choose this package?')">Choose plan</a>
							
									@endif
                        		</div>
                        	</div>
                             @if(@$allSubscription)
							  @foreach(@$allSubscription as $subscript)
                        	<div class=" col-sm-6">
                        		<div class="member-box">
                        			<h2>{{ @$subscript->package_name }}</h2>
                        			<h3 class="extra-mem">
										@if(@$subscript->type == 'M')
										INR {{ @$subscript->monthly_price }} <span> /Monthly </span>
										@elseif(@$subscript->type == 'Y')
										INR {{ @$subscript->yearly_price }} <span> /Yearly </span>
										@endif
									  </h3>
                        			<h5><img src="{{ url('public/frontend/images/tick3.png') }}"> Lorem Ipsum is simply text </h5>
                        			<p>
									@if(strlen(@$subscript->package_desc)>200)
										 {!! substr(@$subscript->package_desc,0,200) . '...' !!}
										 @else
										 {!! @$subscript->package_desc !!}
										 @endif
									</p>
									 @if(@$subscript->userToPackage->agent_package_id == 2)
									 <a href="javascript:;" class="member_btn mem_active">Active</a>
									 @elseif(@$subscript->userToPackage->agent_package_id == 3)
									 <a href="javascript:;" class="member_btn mem_active">Active</a>
									 @elseif(@$subscript->userToPackage->agent_package_id == 4)
									 <a href="javascript:;" class="member_btn mem_active">Active</a>
									 @else
									<form method="POST" action="{{route('razorpay.index')}}">
									@csrf
									<input type="hidden" name="page_type" value="S">
									<input type="hidden" name="subscription_id" value="{{@$subscript->id}}">
									<button type="submit" class="member_btn member_btn1" onclick="return confirm('Are you want to choose this package?')">Choose plan</button>
                        		
                        			</form>
									@endif
                        		</div>
                        	</div>
                            @endforeach
							 @endif
                        	{{--<div class=" col-sm-6">
                        		<div class="member-box">
                        			<h2>Diamond</h2>
                        			<h3 class="extra-mem">INR 2999 <span> /Monthly </span></h3>
                        			<h5><img src="{{ url('public/frontend/images/tick3.png') }}"> Lorem Ipsum is simply text </h5>
                        			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy type and Lorem Ipsum has been the industry's standard dummy type and scrambled it to make a type specimen book.</p>
                        			<a href="#" class="member_btn ">Choose plan</a>
                        		</div>
                        	</div>

                        	<div class=" col-sm-6">
                        		<div class="member-box">
                        			<h2>Platinum</h2>
                        			<h3 class="extra-mem">INR 3999 <span> /Monthly </span></h3>
                        			<h5><img src="{{ url('public/frontend/images/tick3.png') }}"> Lorem Ipsum is simply text </h5>
                        			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy type and Lorem Ipsum has been the industry's standard dummy type and scrambled it to make a type specimen book.</p>
                        			<a href="#" class="member_btn ">Choose plan</a>
                        		</div>
                        	</div>--}}
                        </div>
 
                           
                    </div>
						
					</div>
				</div>
				
			</div>
		</div>
		
	</div>
    @endsection

    
@section('footer')
@include('includes.footer')
@endsection


@section('script')
@include('includes.script')

@endsection

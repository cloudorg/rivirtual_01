@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> RiVirtual | Client Details </title>
@endsection




@section('header')
@include('includes.header')
@endsection


@section('content')
<!----header--->
<div class="haeder-padding"></div>

<div class="dashboard-agent">
	<div class="container">
		<div class="row">
			@include('includes.sidebar')

			<div class="col-lg-9 col-md-12 col-sm-12">
				<div class="cus-dashboard-right">
				   <h2>Client Details</h2>
				</div>
				<div class="agent-dash-right">
                    

					<div class="agent-right-body">
						<div class="prov-statis">
							<div class="agent_ppcc">
								<div class="media">
									<div class="c_image">
										@if($user->profile_pic)
	                            		<img src="{{ url('storage/app/public/profile_picture/'.$user->profile_pic) }}" alt="">
	                            		@else
	                            		<img src="{{ url('public/admin/assets/images/avatar.png') }}" alt="">
	                            		@endif
									</div>
									<div class="media-body">
										<ul>
											<li>
												<label>Client Name:</label>
												<span>{{@$user->name}}</span>
											</li>
											<li>
												<label>Client Email:</label>
												<span>{{@$user->email}}</span>
											</li>
											<li>
												<label>Client Phone:</label>
												<span>{{@$user->mobile_number}}</span>
												
											</li>
										</ul>
									</div>
									
								</div>
	                            
                        	</div>
							
						</div>
					<div>

						

						

					<div class="clearfix"></div>


					</div>
				</div>

			</div>
		</div>

	</div>
</div>


@endsection


@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection



@section('script')
@include('includes.script')

@endsection

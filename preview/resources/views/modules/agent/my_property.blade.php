@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
        margin-top: 10px;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
<link href="{{ URL::asset('public/frontend/croppie/croppie.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('public/frontend/croppie/croppie.min.css') }}" rel="stylesheet" />
@endsection

@section('title')
<title> RiVirtual | My Property </title>
@endsection




@section('header')
@include('includes.header')
@endsection


@section('content')
<!----header--->
<div class="haeder-padding"></div>

<div class="dashboard-agent">
	<div class="container">
		<div class="row">
            @include('includes.sidebar')
            <div class="col-lg-9 col-md-12 col-sm-12">
				<div class="cus-dashboard-right">
				   <h2>My Property</h2>
				</div>
				<div class="agent-dash-right">
                    <div class="agent-right-body">

                        <form action="{{route('agent.my.property')}}" method="POST">
                            @csrf
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="das_input">
                                        <label>Property Name </label>
                                        <input type="text" placeholder="Enter here.." name="name" value="{{@$key['name']}}"> </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="das_input">
                                        <label>State</label>
                                        <select name="state" id="states" class="required">
                                                <option value="">Select State</option>
                                                @foreach(@$states as $state)
                                                <option value="{{@$state->id}}" @if(@$key['state'] == $state->id) selected @endif>{{@$state->name}}</option>
                                                @endforeach
                                            </select>
                                </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="das_input">
                                        <label>City</label>
                                        <select name="city" id="city" class="required">
                                                <option value="">Select City</option>
                                                @foreach(@$cites as $city)
                                                <option value="{{@$city->id}}" @if(@$key['city'] == $city->id) selected @endif>{{@$city->name}}</option>
                                                @endforeach
                                            </select>
                                </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="das_input">
                                        <label>Property For </label>
                                        <select name="property_for">
                                            <option value="">Select</option>
                                            <option value="R" @if(@$key['property_for']=='R')  selected @endif>For Rent</option>
                                            <option value="B" @if(@$key['property_for']=='B')  Selected @endif>For Buy</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="das_input">
                                        <label>Status  </label>
                                        <select name="status">
                                            <option value="">Select Status</option>
                                            <option value="S" @if(@$key['status']=='S')  selected @endif>Sold</option>
                                            <option value="A" @if(@$key['status']=='A')  selected @endif>Active</option>
                                            <option value="B" @if(@$key['status']=='B')  selected @endif>Inactive</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-4">
                                    <button class="dash-btn"> <img src="{{ URL::asset('public/frontend/images/ser.png')}}"> Search </button>
                                </div>
                                <div class="col-md-12">
                                    <div class="bodar"> </div>
                                </div>
                            </div>
                        </form>
                        @include('includes.message')
                        <div class="our_properties_panel">
                            <div class="row">
                                @foreach (@$allProperty as $property)
                                <div class="col-sm-6 col-md-4 col-lg-6 col-xl-4">

                                    <div class=" @if(@$property->aprove_by_admin=='N') approval_lists @endif">
                                        <div class="property-item">
                                            <div class="listing-image solds_imgs rib_sale">
                                                <a href="javascript:;">
                                                  @if(@$property->property_type == 'L')
                                                  <img src="{{ URL::asset('storage/app/public/property_image')}}/{{@$property->landPropertyImageMain->image}}" alt="listing property">
                                                  @else
                                                <img src="{{ URL::asset('storage/app/public/property_image')}}/{{@$property->propertyImageMain->image}}" alt="listing property">
                                                  @endif
                                                    @if(@$property->is_sold=='Y')
                                                    <div class="propety-info-top"></div>
                                                    <div class="sold_tags">Sold</div>
                                                    @endif
                                                </a>
                                                @if(@$property->property_for =='R')
                                                <div class="ribbon-img">
                                                    <div class="ribbon-vertical"><p>For Rent</p></div>
                                                </div>
                                                @elseif(@$property->property_for =='B')
                                                <div class="ribbon-img">
                                                    <div class="ribbon-vertical Sale"><p>For Sale</p></div>
                                                </div>
                                                @endif
                                                @if(@$property->is_sold=='Y')

                                                <!-- <div class="ribbon-img">
                                                    <div class="ribbon-vertical sold_in"><p>SOLD</p></div>
                                                </div> -->
                                                @endif
                                                <div class="wish-property menu-peo">
                                                    <a href="javascript:void(0);" class="action-dots" id="action{{$property->id}}" data-id="{{$property->id}}" ><img src="{{ URL::asset('public/frontend/images/action-dots.png')}}" class="hovern"><img src="{{ URL::asset('public/frontend/images/dot3.png')}}" class="hoverb"></a>

                                                    <div class="show-actionn" id="show-action{{$property->id}}" style="display: none;"> <span class="angle"><img src="{{ URL::asset('public/frontend/images/angle.png')}}" alt=""></span>
                                                            <ul class="text-left">
                                                                @if($property->status=='B'|| $property->status=='A')
                                                                @if(@$property->is_sold=='Y')
                                                                <li><a href="{{route('agent.my.property.un.sold',['id'=>$property->id])}}"  onclick="return confirm('Are you want to mark as unsold this property?')">Unsold</a></li>
                                                                <li><a href="{{route('search.property.details',['slug'=>@$property->slug])}}" target="_blank">View</a></li>
                                                                @else
                                                                <li><a href="{{route('agent.my.property.sold',['id'=>$property->id])}}"  onclick="return confirm('Are you want to mark as sold this property?')">Mark as Sold</a></li>

                                                                <li><a href="{{route('agent.add.property',['id'=>$property->id])}}" >Edit</a></li>
                                                                @if($property->status=='B')
                                                                <li><a href="{{route('agent.my.property.status.change',['id'=>$property->id])}}" onclick="return confirm('Are you want to active this property?')">Active  </a></li>
                                                                @elseif($property->status=='A')
                                                                <li><a href="{{route('agent.my.property.status.change',['id'=>$property->id])}}"  onclick="return confirm('Are you want to inactive this property?')">Inactive  </a></li>
                                                                @endif
                                                                <li><a href="{{route('agent.my.property.delete',['id'=>$property->id])}}"  onclick="return confirm('Are you want delete this property?')">Delete</a></li>
                                                                @endif
                                                                @endif


                                                            </ul>
                                                    </div>
                                                </div>
                                                <div class="location-rating">
                                                    <div class="product-img-location">
                                                        <p><img src="{{ URL::asset('public/frontend/images/loac.png')}}">
                                                         @if(strlen(@$property->localityName->locality_name)>7) 
                                                            
                                                            {{substr(@$property->localityName->locality_name,0,7)}}..

                                                        @else{{@$property->localityName->locality_name}}@endif,

                                                        @if(strlen(@$property->cityName->name)>7)
                                                            {{substr(@$property->cityName->name,0,7)}}..
                                                        @else
                                                            {{@$property->cityName->name}}
                                                        @endif,
                                                        @if(strlen(@$property->stateName->state_abbreviations)>2)
                                                            {{substr(@$property->stateName->state_abbreviations,0,2)}}..
                                                        @else
                                                        {{@$property->stateName->state_abbreviations}}
                                                        @endif
                                                    </p>
                                                    </div>
                                                    <div class="rating-pro">
                                                        <p><img src="{{ URL::asset('public/frontend/images/star.png')}}"> {{ number_format(@$property->avg_review,1, '.', ',') }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="property-detasils-info">
                                                <div class="name-price">
                                                    <a href="{{ route('search.property.details',['slug'=>@$property->slug]) }}"><h5>@if(strlen(@$property->name)> 15){{substr(@$property->name,0,11)}}...@else {{@$property->name}} @endif</h5></a>
                                                    <p>₹{{ number_format(@$property->budget_range_from, 0, '.', ',') }}</p>
                                                </div>
                                                <ul class="list-item-info @if(@$property->property_type == 'L' || @$property->property_type == 'FRMH') only_land @endif ">
                                                    @if(@$property->property_type == 'F' || @$property->property_type == 'H' || @$property->property_type == 'GTCM')
                                                    <li class="before">
                                                        <span>{{@$property->no_of_bedrooms}} <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span> Bedrooms
                                                    </li>
                                                    @endif
                                                    @if(@$property->property_type == 'WRH' || @$property->property_type == 'SHR' || @$property->property_type == 'INDT' || @$property->property_type == 'RET')
                                                    <li class="before">
                                                        <span>{{@$property->no_of_bedrooms}} <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span>Rooms
                                                    </li>
                                                    @endif
                                                    @if(@$property->property_type == 'F' || @$property->property_type == 'GTCM' || @$property->property_type == 'H' || @$property->property_type == 'R' || @$property->property_type == 'OSP' || @$property->property_type == 'WRH' || @$property->property_type == 'SHR' || @$property->property_type == 'INDT' || @$property->property_type == 'RET')
                                                    <li class="before">
                                                        <span>{{@$property->bathroom}} <img src="{{ URL::asset('public/frontend/images/bath.png')}}"></span> Bathrooms
                                                    </li>
                                                    @endif
                                                    @if(@$property->property_type == 'OSP')
                                                    <li class="before">
                                                        <span>{{@$property->office_class}} <img src="{{ URL::asset('public/frontend/images/ofc.png')}}"></span> Class
                                                    </li>
                                                    @endif
                                                    @if(@$property->property_type == 'L' || @$property->property_type == 'FRMH')
                                                    <li class="">
                                                        <span>{{@$property->area}}<img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
                                                    </li>
                                                    @else
                                                    <li class="">
                                                        <span>{{@$property->area}}<img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
                                                    </li>
                                                    @endif
                                                </ul>
                                                {{-- @if(@$property->aprove_by_admin=='N')
                                                Admin Not approved
                                                @endif --}}
                                            </div>
                                        </div>
                                        @if(@$property->aprove_by_admin=='N')<p class="approval_msg">Approval in process</p>@endif
                                    </div>
                                </div>
                                @endforeach
                                @if(@$allProperty->isEmpty())
                                <div class="col-sm-12 col-md-12">
                                    <center style="padding: 10px"><p>No Poperty Found</p></center>
                                </div>
                                @endif
                            </div>
                        </div>

                        <nav aria-label="...">
                            <ul class="pagination new-pagination">
                                {{@$allProperty->appends(request()->except(['page', '_token']))->links()}}
                                {{-- <li class="page-item"> <span class="page-link">Prev </span> </li>
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item" aria-current="page"> <span class="page-link">
                                2
                                <span class="sr-only">(current)</span> </span>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">4</a></li>
                                <li class="page-item"> <a class="page-link act" href="#">Next </a> </li> --}}
                            </ul>
                        </nav>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection


@section('footer')
@include('includes.footer')
@endsection



@section('script')
@include('includes.script')
<script>
    $(document).on('click', function () {
		@foreach(@$allProperty as $value)
		var $target = $(event.target);
		if (!$target.closest('#action{{@$value->id}}').length && $('#show-action{{@$value->id}}').is(":visible")) {
			$('#show-action{{@$value->id}}').slideUp();
		}
		@endforeach
	});

	$(document).ready(function() {
		@foreach(@$allProperty as $value)
			$("#action{{@$value->id}}").click(function() {
				$("#show-action{{@$value->id}}").slideToggle();
			});
		@endforeach
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
    //   $('#country').on('change',function(e){
    //     e.preventDefault();
    //     var id = $(this).val();

    //     $.ajax({
    //       url:'{{route('get.state')}}',
    //       type:'GET',
    //       data:{country:id,id:'{{auth()->user()->state}}'},
    //       success:function(data){
    //         console.log(data);
    //         $('#states').html(data.state);
    //       }
    //     })
    //   });

      $('#states').on('change',function(e){
        e.preventDefault();
        var id = $(this).val();
        $.ajax({
          url:'{{route('get.city')}}',
          type:'GET',
          data:{state:id,id:'{{auth()->user()->state}}'},
          success:function(data){
            console.log(data);
            $('#city').html(data.city);
          }
        })
      });


    });
  </script>
@endsection

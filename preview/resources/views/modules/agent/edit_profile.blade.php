@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
<link href="{{ URL::asset('public/frontend/croppie/croppie.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('public/frontend/croppie/croppie.min.css') }}" rel="stylesheet" />
@endsection

@section('title')
<title> RiVirtual | Edit Profile </title>
@endsection




@section('header')
@include('includes.header')
@endsection


@section('content')
<!----header--->
<div class="haeder-padding"></div>

<div class="dashboard-agent">
	<div class="container">
		<div class="row">
			@include('includes.sidebar')
            <div class="col-lg-9 col-md-12 col-sm-12">
				<div class="cus-dashboard-right">
				   <h2>Edit Profile</h2>
				</div>
				<div class="agent-dash-right">
                    @if(auth()->user()->approval_status==null)
                    <center><p class="alert alert-info">Complete Your Profile </p></center>
                    @endif
                    @include('includes.message')

					<div class="agent-right-body">
                        @php
                        $name =explode(" ",auth()->user()->name);
                        $fname =$name[0];
                        $lname = $name[1];
                        @endphp
						<form action="{{route('agent.profile.save')}}" method="POST" id="editform" autocomplete="off" enctype="multipart/form-data">
                            @csrf

							<div class="user_dashboard_form">
								<div class="row">
									<div class="col-md-4">
										<div class="das_input">
											<label>First Name</label>
											<input type="text" placeholder="Enter First Name" name="first_name" class="required" value="{{@$fname}}">
										</div>
									</div>
									<div class="col-md-4">
										<div class="das_input">
											<label>Last Name</label>
											<input type="text" placeholder="Enter Last Name" class="required" name="last_name" value="{{@$lname}}">
										</div>
									</div>
									<div class="col-md-4">
										<div class="das_input">
											<label>Email Id</label> | <a href="javascript:;" id="ch_email" style="color: blue"> Edit</a>
											<input type="email" placeholder="Enter here.." readonly value="{{auth()->user()->email}}">
                                            @if(auth()->user()->temp_email)
											<label  style="margin-top: 2px;">&nbsp;( {{auth()->user()->temp_email}} email awaiting approval)</label>
                                            @endif
										</div>
									</div>
									<div class="col-md-4">
										<div class="das_input">
											<label>Phone Number</label> | <a href="javascript:;" id="ch_mo" style="color: blue"> Edit</a>
											<input type="tel" placeholder="Enter Contact Numbere" readonly value="{{auth()->user()->mobile_number}}">
                                            @if(auth()->user()->temp_mobile)
                                            <label  style="margin-top: 2px;">&nbsp;( {{auth()->user()->temp_mobile}} mobile awaiting approval)</label>
                                            @endif
										</div>
									</div>
									<div class="col-md-4">
										<div class="das_input">
											<label>WhatsApp</label>
											<input type="tel" placeholder="Enter Whatsapp Number" name ="whatsapp_no" value="{{auth()->user()->whatsapp_no}}" maxlength="10">
										</div>
									</div>
									<div class="col-md-4">
										<div class="das_input">
											<label>Website <span class="ifs"> ( if any ) </span></label>
											<input type="url" placeholder="Enter here.." name ="website" value="{{auth()->user()->website}}">
										</div>
									</div>
									<div class="col-md-4">
										<div class="redio_bx">
											<span>Agent For :</span>
											<ul class="category-ul ul_radio agent-ra">
												<li>
													<label class="radio">
				                                        <input id="radio1" type="radio" name="agent_for" @if(auth()->user()->agent_for=='I' || auth()->user()->agent_for==null) checked @endif value="I" >
				                                        <span class="outer"><span class="inner"></span></span>Individual
				                                    </label>
												</li>
												<li>
													<label class="radio">
					                                        <input id="radio2" type="radio" name="agent_for" @if(auth()->user()->agent_for=='C' ) checked @endif value="C" >
					                                        <span class="outer"><span class="inner " ></span></span>Company
					                                </label>
												</li>
											</ul>
										</div>
									</div>
									  <div class="col-md-4 new-diif" @if(auth()->user()->agent_for=='I' || auth()->user()->agent_for==null) style="display: none;" @endif>
	                                        <div class="das_input">
												<label>Company Name</label>
												<input type="text" placeholder="Enter here.." class=" @if(auth()->user()->agent_for=='C' ) required @endif " name="company_name" value="{{auth()->user()->company_name}}" id="company_name">
											</div>
	                                  </div>
                                    <div class="col-md-4">
										<div class="das_input malti_select">
											<label>Languages Knows</label>
											<span class="autocomplete-select"></span>
                                            <label id="language-error" class="error" for="language" style="display: none;"></label>
										</div>
									</div>
									<div class="col-md-12">
										<div class="das_input">
											<h4>Address Information</h4>
										</div>
									</div>
									<div class="col-md-4">
										<div class="das_input">
											<label>Country</label>
											<select name="country" class="required" id="country">
                                                <option value="">Select Country</option>
                                                @foreach ( $allCountry as $country)
                                                <option value="{{$country->id}}" @if(auth()->user()->country==$country->id) selected @endif>{{$country->name}}</option>
                                                @endforeach
                                            </select>
										</div>
									</div>
									<div class="col-md-4">
                                        <div class="das_input">
                                            <label>State</label>
                                            {{-- <input type="text" placeholder="Enter here.." class="required" name="state" value="{{auth()->user()->state}}"> --}}
                                            <select name="state" id="states" class="required">
                                                <option value="">Select State</option>
                                                @foreach(@$states as $state)
                                                <option value="{{@$state->id}}" @if(auth()->user()->state==@$state->id)selected @endif>{{@$state->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="das_input">
                                            <label>City</label>
                                            <select name="city" id="city" class="required">
                                                <option value="">Select City</option>
                                                @foreach(@$cites as $city)
                                                <option value="{{@$city->id}}" @if(auth()->user()->city==@$city->id)selected @endif>{{@$city->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
									<div class="col-md-4">
										<div class="das_input">
											<label>Address</label>
											<input type="text" placeholder="Enter here.." class="required" name="address" value="{{auth()->user()->address}}">
										</div>
									</div>
                                    <div class="col-md-4">
                                    <div class="das_input">
                                        <label>Post Code</label>
                                        <input type="text" placeholder="Enter here.." name="post_code" class="required" value="{{ auth()->user()->postal_code?auth()->user()->postal_code: '' }}" id="post_code"  maxlength="6">
                                    </div>
                                </div>
									<div class="col-md-12">
										<div class="das_input">
											<label>About Me </label>
											<textarea placeholder="Enter here.." name="about" class="required">{!! auth()->user()->about !!}</textarea>

										</div>
									</div>
									<div class="col-md-12">
										<div class="uplodimg">
											<div class="uplodimgfil">
												<b>Gov ID (Allowed files - images, pdf, docx )</b>
												<input type="file" name="gov_id" id="gov_id" class="inputfile inputfile-1" />
												<label for="gov_id">Upload Id <img src="{{ URL::to('public/frontend/images/clickhe.png')}}" alt=""></label>
                                                <label id="gov_id-error" class="error" for="gov_id" style="display: none"></label>
											</div>
                                            <label id ="label1" style="display:none"></label>

										</div>
									</div>
									<div class="col-md-12">
										<div class="uplodimg">
											<div class="uplodimgfil">
												<b>License (Allowed files - images, pdf, docx )</b>
												<input type="file" name="license" id="license" class="inputfile inputfile-1" />
												<label for="license">Upload License <img src="{{URL::to('public/frontend/images/clickhe.png')}}" alt=""></label>
                                                <label id="license-error" class="error" for="license" style="display: none"></label>
											</div>
                                            <label id ="label2" style="display:none"></label>
										</div>
									</div>
									<div class="col-md-12">
										<div class="uplodimg">
											<div class="uplodimgfil">
												<b>Upload Photo</b>
												<input type="hidden" name="profile_picture" id="profile_picture">
                                                <input type="file" name="file" id="file" class="inputfile inputfile-1" />
                                                <label for="file">Upload profile picture <img src="{{ URL::to('public/frontend/images/clickhe.png')}}" alt=""></label>
                                                <label id="profile_picture-error" class="error" for="profile_picture" style="display: none"></label>
                                                <input type="hidden" name="old_image" id="old_image" value="{{auth()->user()->profile_pic}}">
											</div>
											<div class="uplodimg_pick uplodimgfilimg">
                                                @if(auth()->user()->profile_pic != null)
                                                <img src="{{ URL::to('storage/app/public/profile_picture')}}/{{auth()->user()->profile_pic}}" alt="" id="img2">
                                                @else
                                                <img src="{{ URL::to('public/frontend/images/avatar.png')}}" alt="" id="img2">
                                                @endif
											</div>
										</div>
									</div>


									<div class="col-md-12">
										<div class="bodar">
										</div>
									</div>
                                    <div class="col-md-12">
									<div class="das_input">
										<h4>Change Password</h4>
									</div>
								</div>
                                @if(auth()->user()->password!=null)
								<div class="col-md-4">
									<div class="das_input">
										<label>Old Password</label>
										<input type="password" placeholder="Enter here.." name="old_password" id="old_password">
									</div>
								</div>
                                @endif
								<div class="col-md-4">
									<div class="das_input">
										<label>New Password</label>
										<input type="password" placeholder="Enter here.." name="new_password" id="new_password">
									</div>
								</div>
								<div class="col-md-4">
									<div class="das_input">
										<label>Confirm Password </label>
										<input type="password" placeholder="Enter here.." name="new_password_confirmation" id="new_password_confirmation">
									</div>
								</div>
								<div class="col-md-12">
									<div class="bodar">
									</div>
								</div>
                                    <input type="hidden" name="language" id="language">
									<div class="col-md-12">
										<div class="usei_sub text-right">
											<input type="submit" value="Save all changes" class="see_cc">
										</div>
									</div>
								</div>
							</div>
						</form>


					</div>
				</div>

			</div>


		</div>

	</div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="croppie-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Crop Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="croppie-div" style="width: 100%;"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="crop-img">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="chenge-mobile-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="sign_popup_body">
                    <h4>Change Mobile Number</h4>
                    @include('includes.modal_message')
                    <form action="{{route('agent.temp.mobile.save')}}" method="POST" id="changeModal">
                        @csrf
                        <div class="sign_popup_pane">
                            <div class="input-field">
                                <input type="tel" class="login-type" name="mobile" value="{{ old('mobile') }}" id="mobile" maxlength="10">
                                <label for="mobile">Mobile Number </label>
                            </div>
                            <div class="sing_button">
                                <button class="see_cc" >Edit</button>
                                <a href="javascript:;" class="see_cc" style="display: none" id="servicelogina">Please Wait</a>
                            </div>
                        </div>
                    </form>
                    <p id="changemob" style="display: none">Enter the 6 digit code sent to you at +91 9876543210</p>
                    <p id="changemobotp" style="display: none"></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="chenge-email-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="sign_popup_body">
                    <h4>Change Email Address</h4>
                    @include('includes.modal_message')
                    <form action="{{route('agent.temp.email.save')}}" method="POST" id="changeEmailModal">
                        @csrf
                        <div class="sign_popup_pane">
                            <div class="input-field">
                                <input type="txt" class="login-type" name="email" value="{{ old('email') }}" id="email">
                                <label for="email">Email </label>
                            </div>
                            <div class="sing_button">
                                <button class="see_cc" >Edit</button>
                                <a href="javascript:;" class="see_cc" style="display: none" id="servicelogina">Please Wait</a>
                            </div>
                        </div>
                    </form>
                    <p id="changemob" style="display: none">Enter the 6 digit code sent to you at +91 9876543210</p>
                    <p id="changemobotp" style="display: none"></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('footer')
@include('includes.footer')
@endsection



@section('script')
@include('includes.script')
<script src="{{ URL::asset('public/frontend/croppie/croppie.js') }}"></script>
<script>
    function dataURLtoFile(dataurl, filename) {
        var arr = dataurl.split(','),
        mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]),
        n = bstr.length,
        u8arr = new Uint8Array(n);
        while(n--){
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new File([u8arr], filename, {type:mime});
    }
    var uploadCrop;
    $(document).ready(function(){
        $('#croppie-modal').on('hidden.bs.modal', function() {
            uploadCrop.croppie('destroy');
        });

        $('#crop-img').click(function() {
            uploadCrop.croppie('result', {
                type: 'base64',
                format: 'png'
            }).then(function(base64Str) {
                $("#croppie-modal").modal("hide");
               //  $('.lds-spinner').show();
               let file = dataURLtoFile('data:text/plain;'+base64Str+',aGVsbG8gd29ybGQ=','hello.png');
                  console.log(file.mozFullPath);
                  console.log(base64Str);
                  $('#profile_picture').val(base64Str);
               // $.each(file, function(i, f) {
                    var reader = new FileReader();
                    reader.onload = function(e){
                        $('.uplodimgfilimg').append('<em><img  src="' + e.target.result + '"><em>');
                    };
                    reader.readAsDataURL(file);

               //  });
                $('.uplodimgfilimg').show();

            });
        });
    });
    $("#file").change(function () {
            $('.uplodimgfilimg').html('');
            let files = this.files;
            console.log(files);
            let img  = new Image();
            if (files.length > 0) {
                let exts = ['image/jpeg', 'image/png', 'image/gif'];
                let valid = true;
                $.each(files, function(i, f) {
                    if (exts.indexOf(f.type) <= -1) {
                        valid = false;
                        return false;
                    }
                });
                if (! valid) {
                    alert('Please choose valid image files (jpeg, png, gif) only.');
                    $("#file").val('');
                    return false;
                }
                // img.src = window.URL.createObjectURL(event.target.files[0])
                // img.onload = function () {
                //     if(this.width > 250 || this.height >160) {
                //         flag=0;
                //         alert('Please upload proper image size less then : 250px x 160px');
                //         $("#file").val('');
                //         $('.uploadImg').hide();
                //         return false;
                //     }
                // };
                $("#croppie-modal").modal("show");
                uploadCrop = $('.croppie-div').croppie({
                    viewport: { width: 256, height: 256, type: 'square' },
                    boundary: { width: $(".croppie-div").width(), height: 400 }
                });
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.upload-demo').addClass('ready');
                    // console.log(e.target.result)
                    uploadCrop.croppie('bind', {
                        url: e.target.result
                    }).then(function(){
                        console.log('jQuery bind complete');
                    });
                }
                reader.readAsDataURL(this.files[0]);
               //  $('.uploadImg').append('<img width="100"  src="' + reader.readAsDataURL(this.files[0]) + '">');
               //  $.each(files, function(i, f) {
               //      var reader = new FileReader();
               //      reader.onload = function(e){
               //          $('.uploadImg').append('<img width="100"  src="' + e.target.result + '">');
               //      };
               //      reader.readAsDataURL(f);
               //  });
               //  $('.uploadImg').show();
            }

        });
</script>
<script>
    $(document).ready(function(){
        jQuery.validator.addMethod("validate_word", function(value, element) {
        if (/^([a-zA-Z])+$/.test(value)) {
            return true;
        } else {
            return false;
        }
    }, "Not allow special characters or numbers");
    jQuery.validator.addMethod("validate_address", function(value, element) {
        if (/^([a-zA-Z0-9./_ ,-])+$/.test(value)) {
            return true;
        } else {
            return false;
        }
    }, "Allow only (a-z, A-Z, 0-9, ./_ ,-)");
    jQuery.validator.addMethod("validate_about", function(value, element) {
        if (/^([a-zA-Z0-9./_ ,-])+$/.test(value)) {
            return true;
        } else {
            return false;
        }
    }, "Allow only (a-z, A-Z, 0-9, ./_ ,-)");
        $('#editform').validate({
            rules: {
                first_name:{
                    validate_word:true,
                },
                last_name:{
                    validate_word:true,
                },
                state:{
                    required:true,
                },
                city:{
                    required:true,
                },
    
                address:{
                    validate_address:true,
                },
                post_code:{
                    required: true,
                    digits: true ,
                    minlength:6,
                },
                about:{
                    validate_about:true,
                },
                whatsapp_no:{
                    required: true,
                    digits: true ,
                    minlength: 10,
                    maxlength: 10,
                },
                old_password: {
                    required: function(){
                        var new_pass = $('#new_password').val();
                        var old_pass_confirm = $('#new_password_confirmation').val();
                        if(new_pass!='' || old_pass_confirm !=''){
                            return true
                        }else{
                            return false
                        }
                    }
                },
                new_password: {
                    required: function(){
                        var old_pass = $('#old_password').val();
                        var old_pass_confirm = $('#new_password_confirmation').val();
                        if(old_pass!=''|| old_pass_confirm !=''){
                            return true
                        }else{
                            return false
                        }
                    },
                    minlength: 8
                },
                new_password_confirmation:{
                    required: function(){
                        var old_pass = $('#old_password').val();
                        var new_pass = $('#new_password').val();
                        if(old_pass!=''||new_pass!=''){
                            return true
                        }else{
                            return false
                        }
                    },
                    equalTo: "#new_password"
                },
                profile_picture:{
                    required: function(element){
                        var old = $('#old_image').val();
                        if(old == null || old == "")
                        return true;
                        else
                        return false;
                    },
                },
                gov_id:{
                    extension: "docx|doc|pdf|jpg|png|jpeg|doc|gif",
                },
                license:{
                    extension: "docx|doc|pdf|jpg|png|jpeg|doc|gif",
                },
            },
            messages:{
                whatsapp_no:{
                    required: 'Enter whatsApp Number',
                    digits: 'Please enter a valid number ',
                    minlength: 'Exactly only 10 digits without country code',
                    maxlength: 'Exactly only 10 digits without country code',
                },
                gov_id:{
                    extension: "Only allow  pdf, doc, docx, jpeg, gif, png",
                },
                license:{
                    extension: "Only allow pdf, doc, docx, jpeg, gif, png",
                },
                country:{
                    required:"Select a country",
                },
                state:{
                    required:"Select a state",
                },
                city:{
                    required:"Select a city",
                },
                post_code:{
                    required: 'Enter post code',
                    digits: 'post code only number ',
                    minlength:'Post code exactly 6 digits',
                },
            },
            ignore: [],
            submitHandler:function(form){
                var language = $('#language').val();
                if(language == ''){
                    $('#language-error').html('Minimum one language required');
                    $('#language-error').css('display', 'block');
                    return false;
                }else{
                    form.submit();
                }
            }
        });
        $('#ch_mo').click(function(){
        $("#chenge-mobile-modal").modal("show");
    });
    $('#ch_email').click(function(){
        $("#chenge-email-modal").modal("show");
    });
    $('#chenge-mobile-modal').on('hidden.bs.modal', function () {
        $(this).find('form').trigger('reset');
    });
    $('#chenge-email-modal').on('hidden.bs.modal', function () {
        $(this).find('form').trigger('reset');
    });
    $("#changeModal").validate({
        rules: {
            mobile:{
                required: true,
                digits: true ,
                minlength: 10,
                maxlength: 10,
                remote: {
                    url: '{{ route("check.mobile") }}',
                    dataType: 'json',
                    type:'post',
                    data: {
                        mobile: function() {
                            return $('#mobile').val();
                        },
                        _token: '{{ csrf_token() }}'
                    }
                },
            }
        },
        messages: {
            mobile:{
                required: 'Enter Mobile Number',
                digits: 'Please enter a valid number ',
                minlength: 'Exactly only 10 digits without country code',
                maxlength: 'Exactly only 10 digits without country code',
                remote:'Mobile Number already in Use'
            }
        },
    });
    jQuery.validator.addMethod("validate_email", function(value, element) {
        if (/^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,5}$/i.test(value)) {
            return true;
        } else {
            return false;
        }
    }, "Oops...! check your Email again");
    $("#changeEmailModal").validate({
        rules: {
            email:{
                required: true,
                email: true,
                validate_email:true,
                remote: {
                    url: '{{ route("check.email") }}',
                    dataType: 'json',
                    type:'post',
                    data: {
                        email: function() {
                            return $('#email').val();
                        },
                        _token: '{{ csrf_token() }}'
                    }
                },
            }
        },
        messages: {
            email:{
                required: 'Enter email',
                email:'Oops...! check your Email again',
                remote:'Email already in Use ',
            }
        },
    });
});
</script>
<script>
$('#radio1:radio').on('change', function() {
  if($(this).is(":checked")) {
    $('.new-diif').slideUp();
    $('#company_name').removeClass('required');
  } else {
    $('.new-diif').slideDown();
    $('#company_name').addClass('required');
  }
});
$('#radio2:radio').on('change', function() {
  if($(this).is(":checked")) {
    $('.new-diif').slideDown();
    $('#company_name').addClass('required');

  } else {
    $('.new-diif').slideUp();
    $('#company_name').removeClass('required');
  }
});

$('#gov_id').change(function() {
  var file = $('#gov_id')[0].files[0].name;
  console.log(file);
  if(file==null|| file==''){
    $('#label1').css('display','none');
  }else{
    $('#label1').css('display','block');
    $('#label1').css('padding-top','45px');
    $('#label1').css('padding-left','10px');
    $('#label1').text(file);
  }

});
$('#license').change(function() {
  var file = $('#license')[0].files[0].name;
  console.log(file);
  if(file==null|| file==''){
    $('#label2').css('display','none');
  }else{
    $('#label2').css('display','block');
    $('#label2').css('padding-top','45px');
    $('#label2').css('padding-left','10px');
    $('#label2').text(file);
  }

});
$(document).ready(function() {
    /*all language data push in array*/
    var language = [];
    @foreach($allLanguage as $language )
    language.push({ label : '{{ $language->name }}', value : '{{ $language->id }}'});
    @endforeach
    console.log(language);
    /*end*/

    /*user language data push in array*/
    var languagevalue= [];
    @foreach(@$allUserLanguage as $languages )
    languagevalue.push('{{ $languages->language_id }}');
    @endforeach
    console.log(languagevalue);
    /*end*/
    $('#language').val(languagevalue);




    var autocomplete = new SelectPure(".autocomplete-select", {
        options: language,
        value: languagevalue,
        multiple: true,
        autocomplete: true,
        icon: "icofont-close-line",
        onChange: value => {
            console.log(value);
            $('#language').val(value);
            if(value==''){
                $('#language-error').html('Minimum one language required');
                $('#language-error').css('display', 'block');
            }else{
                $('#language-error').css('display', 'none');
            }
        },
        classNames: {
            select: "select-pure__select",
            dropdownShown: "select-pure__select--opened",
            multiselect: "select-pure__select--multiple",
            label: "select-pure__label",
            placeholder: "select-pure__placeholder",
            dropdown: "select-pure__options",
            option: "select-pure__option",
            autocompleteInput: "select-pure__autocomplete",
            selectedLabel: "select-pure__selected-label",
            selectedOption: "select-pure__option--selected",
            placeholderHidden: "select-pure__placeholder--hidden",
            optionHidden: "select-pure__option--hidden",
        }
    });
    var resetAutocomplete = function() {
        autocomplete.reset();
    };
});
</script>
<script type="text/javascript">
    $(document).ready(function(){
      $('#country').on('change',function(e){
        e.preventDefault();
        var id = $(this).val();

        $.ajax({
          url:'{{route('get.state')}}',
          type:'GET',
          data:{country:id,id:'{{auth()->user()->state}}'},
          success:function(data){
            console.log(data);
            $('#states').html(data.state);
          }
        })
      });

      $('#states').on('change',function(e){
        e.preventDefault();
        var id = $(this).val();
        $.ajax({
          url:'{{route('get.city')}}',
          type:'GET',
          data:{state:id,id:'{{auth()->user()->state}}'},
          success:function(data){
            console.log(data);
            $('#city').html(data.city);
          }
        })
      });


    });
  </script>
@endsection

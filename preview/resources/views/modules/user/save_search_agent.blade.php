@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> RiVirtual | Saved Searches </title>
@endsection




@section('header')
@include('includes.header')
@endsection

@section('content')

<div class="haeder-padding"></div>

<div class="user_dashboard">

		<div class="container">

			<div class="user_dashboard_inr">

            @include('includes.sidebar')

				<div class="user_dashboard_right">

					<h1><img src="{{ url('public/frontend/images/save.png') }}" alt=""> Saved searches</h1>					

					@include('includes.message')

					<div class="user_das_right_inr">

						

						<div class="agent-das-tab user-search-tabs">

							<ul>

								<li><a href="{{ route('user.save.property') }}">Properties </a></li>

								<li class="active"><a href="{{ route('user.save.agent') }}">Agents  </a></li>

								<li><a href="{{ route('user.save.pro') }}">Pro  </a></li>

							</ul>

							<hr>

							

						</div>



							<div class="our_properties_panel">

								<div class="row">
                                   @if(count(@$saveAgentData)>0)
								    @foreach(@$saveAgentData as $agent)
									<div class="col-sm-6 col-md-4">

										<div class="agent-item">
											<div class="pro-wish">

												<a href="{{ route('user.save.agent.delete',['id'=>@$agent->id]) }}" onclick="return confirm('Do you want to delete this agent?')"> <img src="{{ url('public/frontend/images/delete.png') }}"></a>

											</div>
											<div class="agent-pro">
												<em>
													<img src="{{ URL::to('storage/app/public/profile_picture') }}/{{ @$agent->agentDetails->profile_pic }}">
												</em>
												<div class="agent-detailas">
													<h2>
														<a href="{{ route('agent.public.profile',['slug'=>@$agent->agentDetails->slug]) }}">{{ @$agent->agentDetails->name }}</a>
														<span><img src="{{ url('public/frontend/images/w-star.png') }}">{{ number_format(@$agent->agentDetails->owner_avg_review,1,'.',',') }}</span>
													</h2>
													<p>Private Broker</p>
												</div>
											</div>
											<p> 
											@if(strlen(@$agent->agentDetails->about) >30)
                                             {!! substr(@$agent->agentDetails->about, 0, 30) . '..' !!}
                                             @else
                                             {!!@$agent->agentDetails->about!!}
                                              @endif
											</p>

											<div class="agentpro-info">

													

													<p> <em><img src="{{ url('public/frontend/images/agent_icon1.png') }}"> </em>
													 @if(strlen(@$agent->agentDetails->address) > 25)

                                                           {{ substr(@$agent->agentDetails->address,0,25)}}...
                                                            @else 
                                                        {{@$agent->agentDetails->address}} 
                                                           @endif
													 </p>

													<p><a href="mailto:{{ @$agent->agentDetails->email }}"><em><img src="{{ url('public/frontend/images/agent_icon2.png') }}"></em> {{ @$agent->agentDetails->email }} </a> </p>

													<p><a href="tel:{{ @$agent->agentDetails->mobile_number }}"> <em><img src="{{ url('public/frontend/images/agent_icon3.png') }}"></em> +91 {{ @$agent->agentDetails->mobile_number }} </a> </p>

												</div>
											
											<hr class="prod-hr1">

											<div class="serp-budget p-0">

													<div class="bud-details1 ">

														<h5><img src="{{ url('public/frontend/images/adt.png') }}"> {{ @$agent->totalProperty->count() }} Property</h5>

													</div>

													<a href="{{ route('agent.public.profile',['slug'=>@$agent->agentDetails->slug]) }}">View profile</a>

												</div>
										</div>

									</div>
                                      @endforeach
									  @else
									  @if(@$saveAgentData->isEmpty())
                                     <div class="col-sm-12 col-md-12">
                                 <center style="padding: 10px"><p>No Agent Available</p></center>
                                      </div>
                                       @endif
									  @endif
									{{--<div class="col-sm-6 col-md-4">

										<div class="agent-item">
											<div class="pro-wish">

												<a href="#"> <img src="{{ url('public/frontend/images/wish2.png') }}"></a>

											</div>
											<div class="agent-pro">
												<em>
													<img src="{{ url('public/frontend/images/agent_publickimg.png') }}">
												</em>
												<div class="agent-detailas">
													<h2>
														<a href="agent-public-profile.html">Saikat Basu</a>
														<span><img src="{{ url('public/frontend/images/w-star.png') }}"> 4.5</span>
													</h2>
													<p>Private Broker</p>
												</div>
											</div>
											<p>Lorem Ipsum is simply dummy  type setting, remaining essentially unchanged.</p>

											<div class="agentpro-info">

													

													<p> <em><img src="{{ url('public/frontend/images/agent_icon1.png') }}"> </em> Btlanta, GA 30354</p>

													<p><a href="mailto:adamokrara94@gmail.com"><em><img src="{{ url('public/frontend/images/agent_icon2.png') }}"></em> adamokrara94@gmail.com </a> </p>

													<p><a href="tel:9876543210"> <em><img src="{{ url('public/frontend/images/agent_icon3.png') }} "></em> +91 9876543210 </a> </p>

												</div>
											
											<hr class="prod-hr1">

											<div class="serp-budget p-0">

													<div class="bud-details1 ">

														<h5><img src="{{ url('public/frontend/images/adt.png') }}"> 58 Property</h5>

													</div>

													<a href="#">View profile</a>

												</div>
										</div>

									</div>

									<div class="col-sm-6 col-md-4">

										<div class="agent-item">
											<div class="pro-wish">

												<a href="#"> <img src="{{ url('public/frontend/images/wish2.png') }}"></a>

											</div>
											<div class="agent-pro">
												<em>
													<img src="{{ url('public/frontend/images/upro2.png') }}">
												</em>
												<div class="agent-detailas">
													<h2>
														<a href="agent-public-profile.html">Saikat Basu</a>
														<span><img src="{{ url('public/frontend/images/w-star.png') }}"> 4.5</span>
													</h2>
													<p>Private Broker</p>
												</div>
											</div>
											<p>Lorem Ipsum is simply dummy  type setting, remaining essentially unchanged.</p>

											<div class="agentpro-info">

													

													<p> <em><img src="{{ url('public/frontend/images/agent_icon1.png') }}"> </em> Btlanta, GA 30354</p>

													<p><a href="mailto:adamokrara94@gmail.com"><em><img src="{{ url('public/frontend/images/agent_icon2.png') }}"></em> adamokrara94@gmail.com </a> </p>

													<p><a href="tel:9876543210"> <em><img src="{{ url('public/frontend/images/agent_icon3.png') }} "></em> +91 9876543210 </a> </p>

												</div>
											
											<hr class="prod-hr1">

											<div class="serp-budget p-0">

													<div class="bud-details1 ">

														<h5><img src="{{ url('public/frontend/images/adt.png') }}"> 58 Property</h5>

													</div>

													<a href="#">View profile</a>

												</div>
										</div>

									</div>--}}

									{{--<div class="col-sm-6 col-md-4">

										<div class="agent-item">
											<div class="pro-wish">

												<a href="#"> <img src="images/wish2.png"></a>

											</div>
											<div class="agent-pro">
												<em>
													<img src="images/agent-users.png">
												</em>
												<div class="agent-detailas">
													<h2>
														<a href="agent-public-profile.html">Saikat Basu</a>
														<span><img src="images/w-star.png"> 4.5</span>
													</h2>
													<p>Private Broker</p>
												</div>
											</div>
											<p>Lorem Ipsum is simply dummy  type setting, remaining essentially unchanged.</p>

											<div class="agentpro-info">

													

													<p> <em><img src="images/agent_icon1.png"> </em> Btlanta, GA 30354</p>

													<p><a href="mailto:adamokrara94@gmail.com"><em><img src="images/agent_icon2.png"></em> adamokrara94@gmail.com </a> </p>

													<p><a href="tel:9876543210"> <em><img src="images/agent_icon3.png "></em> +91 9876543210 </a> </p>

												</div>
											
											<hr class="prod-hr1">

											<div class="serp-budget p-0">

													<div class="bud-details1 ">

														<h5><img src="images/adt.png"> 58 Property</h5>

													</div>

													<a href="agent-public-profile.html">View profile</a>

												</div>
										</div>

									</div>

									<div class="col-sm-6 col-md-4">

										<div class="agent-item">
											<div class="pro-wish">

												<a href="#"> <img src="images/wish2.png"></a>

											</div>
											<div class="agent-pro">
												<em>
													<img src="images/ser-pro.png">
												</em>
												<div class="agent-detailas">
													<h2>
														<a href="agent-public-profile.html">Saikat Basu</a>
														<span><img src="images/w-star.png"> 4.5</span>
													</h2>
													<p>Private Broker</p>
												</div>
											</div>
											<p>Lorem Ipsum is simply dummy  type setting, remaining essentially unchanged.</p>

											<div class="agentpro-info">

													

													<p> <em><img src="images/agent_icon1.png"> </em> Btlanta, GA 30354</p>

													<p><a href="mailto:adamokrara94@gmail.com"><em><img src="images/agent_icon2.png"></em> adamokrara94@gmail.com </a> </p>

													<p><a href="tel:9876543210"> <em><img src="images/agent_icon3.png "></em> +91 9876543210 </a> </p>

												</div>
											
											<hr class="prod-hr1">

											<div class="serp-budget p-0">

													<div class="bud-details1 ">

														<h5><img src="images/adt.png"> 58 Property</h5>

													</div>

													<a href="agent-public-profile.html">View profile</a>

												</div>
										</div>

									</div>

									<div class="col-sm-6 col-md-4">

										<div class="agent-item">
											<div class="pro-wish">

												<a href="#"> <img src="images/wish2.png"></a>

											</div>
											<div class="agent-pro">
												<em>
													<img src="images/pro1.png">
												</em>
												<div class="agent-detailas">
													<h2>
														<a href="agent-public-profile.html">Saikat Basu</a>
														<span><img src="images/w-star.png"> 4.5</span>
													</h2>
													<p>Private Broker</p>
												</div>
											</div>
											<p>Lorem Ipsum is simply dummy  type setting, remaining essentially unchanged.</p>

											<div class="agentpro-info">

													

													<p> <em><img src="images/agent_icon1.png"> </em> Btlanta, GA 30354</p>

													<p><a href="mailto:adamokrara94@gmail.com"><em><img src="images/agent_icon2.png"></em> adamokrara94@gmail.com </a> </p>

													<p><a href="tel:9876543210"> <em><img src="images/agent_icon3.png "></em> +91 9876543210 </a> </p>

												</div>
											
											<hr class="prod-hr1">

											<div class="serp-budget p-0">

													<div class="bud-details1 ">

														<h5><img src="images/adt.png"> 58 Property</h5>

													</div>

													<a href="agent-public-profile.html">View profile</a>

												</div>
										</div>

									</div>--}}

									
									

								</div>							

							</div>



							<nav aria-label="...">

											<ul class="pagination new-pagination">
											{{@$saveAgentData->appends(request()->except(['page', '_token']))->links()}}
												{{--<li class="page-item"> <span class="page-link">Prev </span> </li>

												<li class="page-item active"><a class="page-link" href="#">1</a></li>

												<li class="page-item" aria-current="page"> <span class="page-link">

                                                2

                                                <span class="sr-only">(current)</span> </span>

												</li>

												<li class="page-item"><a class="page-link" href="#">3</a></li>

												<li class="page-item"><a class="page-link" href="#">4</a></li>

												<li class="page-item"> <a class="page-link act" href="#">Next </a> </li>--}}

											</ul>

										</nav>

						



					</div>

				</div>

			</div>

		</div>

	</div>
    @endsection

    @section('footer')
@include('includes.footer')
@endsection


@section('script')
@include('includes.script')


@endsection
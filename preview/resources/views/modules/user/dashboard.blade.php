@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> RiVirtual | Dashbord </title>
@endsection




@section('header')
@include('includes.header')
@endsection


@section('content')
<!----header--->
<div class="haeder-padding"></div>

<div class="user_dashboard">
	<div class="container">
		<div class="user_dashboard_inr">
			@include('includes.sidebar')
			<div class="user_dashboard_right">
				<h1><img src="{{ URL::to('public/frontend/images/dash.png')}}" alt="">Dashboard</h1>
				<div class="user_das_right_inr">
                    <div>
                        <div>
                            <div class="user_statis">
                                <em>
                                    <i class="icofont-briefcase-1"></i>
                                </em>
                                <p> Jobs In Progress : <span> {{ @$jobProgressData->count() }} </span></p>
                            </div>
                            <div class="custom-table">
                                <div class="new-table-mr">
                                    <div class="table">
                                        <div class="one_row1 hidden-sm-down only_shawo">
                                            <div class="cell1 tab_head_sheet">Job Title	</div>
                                            <div class="cell1 tab_head_sheet">Provider Name </div>
                                            <div class="cell1 tab_head_sheet">Category </div>

                                            <div class="cell1 tab_head_sheet">Date </div>
                                            <div class="cell1 tab_head_sheet">Budget</div>
                                            <div class="cell1 tab_head_sheet">&nbsp;</div>
                                            <div class="cell1 tab_head_sheet">Action </div>
                                        </div>
                                        <!--row 1-->
                                        @if(count(@$jobProgressData)>0)
                                         @foreach(@$jobProgressData as $val)
                                        <div class="one_row1 small_screen31">
                                            <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Job Title	</span>
                                                <p class="add_ttrr"> 
                                                @if(strlen(@$val->job_name)>20)
                                                              {!! substr(@$val->job_name,0,20) . '...'!!}
                                                              @else
                                                            {!! @$val->job_name !!}
                                                            @endif
                                                     </p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Provider Name</span>
                                                <p class="add_ttrr">{{ @$val->quoteDetails->proDetails->name }}</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Category</span>
                                                <p class="add_ttrr">{{ @$val->categoryName->category_name }}</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
                                                <p class="add_ttrr">{{ date('d.m.Y',strtotime(@$val->job_start_date)) }}</p>
                                            </div>

                                            <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Budget</span>
                                                <p class="add_ttrr">
                                                  @if(@$val->budget_range_from != null)
														₹{{ @$val->budget_range_from }}
														@else
                                                        --
                                                        @endif
                                                </p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1 msg_rm_01"> <span class="W55_1">Message</span>
                                                @if(@$val->proDetails->status == 'Awarded')

                                                <!--<a href="javascript:;" class="chat_btn msg_rm" data-userid="{{@Auth::user()->id}}" data-agentid="{{@$val->proDetails->user_id}}" data-name="{{@$val->proDetails->name}}" data-username="{{@Auth::user()->name}}" data-usertype="P" data-msgtyp="Pr" data-propertyid="{{@$val->proDetails->user_id}}" data-bidid="{{@$val->proDetails->id}}" data-providerid="{{@$val->proDetails->user_id}}"><strong class="msg_rm_02">-->
                                                <!--        <img src="{{ url('public/frontend/images/messg.png') }}" alt=""> Message</strong></a>-->
                                                        
                                        <a href="javascript:;" class="chat_btn msg_rm" data-userid="{{@Auth::user()->id}}" data-agentid="{{@$val->proDetails->user_id}}" data-name="{{@$val->proDetails->name}}" data-username="{{@Auth::user()->name}}" data-propertyid="0" data-providerid="{{@$val->proDetails->user_id}}"><strong class="msg_rm_02">
                                                        <img src="{{ url('public/frontend/images/messg.png') }}" alt=""> Message</strong></a>


                                                @endif

                                                
                                                        <!-- <a href="javascript:void(0);" class="msg_rm"><strong class="msg_rm_02">
                                                        <img src="{{ url('public/frontend/images/messg.png') }}" alt=""> Message</strong> </a> -->
                                            </div>
                                            <div class="cell1 tab_head_sheet_1 "> <span class="W55_1">Action</span>
                                                <div class="add_ttrr actions-main text-center">
                                                    <a href="javascript:void(0);" class="action-dots" id="action11{{ @$val->id }}"><img src="{{ URL::to('public/frontend/images/action-dots.png')}}" alt=""></a>
                                                    <div class="show-actions" id="show-action11{{ @$val->id }}" style="display: none;"> <span class="angle"><img src="{{ URL::to('public/frontend/images/angle.png')}}" alt=""></span>
                                                        <ul class="text-left">

                                                        <li><a href="{{ route('user.post.job',['id'=>@$val->id]) }}">Edit</a></li>
															<li><a href="{{ route('user.post.job.delete',['id'=>@$val->id]) }}" onclick="return confirm('Are you want to delete this job?')">Delete</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                         @endforeach
                                         @else
                                                  @if(@$jobProgressData->isEmpty())
                                                   <div class="col-sm-12 col-md-12" align="center">
                                                   <center style="padding: 10px"><p>No Data Found</p></center>
                                                   </div>
                                                   @endif
                                                   @endif
                                          
                                        <!--row 1-->
                                        <!--row 1-->
                                       {{--<div class="one_row1 small_screen31">
                                            <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Job Title	</span>
                                                <p class="add_ttrr"> Dummy title heresed do mod a type... </p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Provider Name</span>
                                                <p class="add_ttrr">Rahul Das</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Category</span>
                                                <p class="add_ttrr">Carpenter</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
                                                <p class="add_ttrr">25.09.2021</p>
                                            </div>

                                            <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Budget</span>
                                                <p class="add_ttrr">₹150/hr. </p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1 msg_rm_01"> <span class="W55_1">Message</span>


                                                        <!-- <a href="javascript:void(0);" class="msg_rm"><strong class="msg_rm_02">
                                                        <img src="{{ url('public/frontend/images/messg.png') }}" alt=""> Message</strong> </a> -->
                                            </div>
                                            <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
                                                <div class="add_ttrr actions-main text-center">
                                                    <a href="javascript:void(0);" class="action-dots" id="action15"><img src="{{ URL::to('public/frontend/images/action-dots.png')}}" alt=""></a>
                                                    <div class="show-actions" id="show-action15" style="display: none;"> <span class="angle"><img src="{{ URL::to('public/frontend/images/angle.png')}}" alt=""></span>
                                                        <ul class="text-left">

                                                            <li><a href="#">Edit</a></li>
                                                            <li><a href="#">Delete</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="one_row1 small_screen31">
                                            <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Job Title	</span>
                                                <p class="add_ttrr"> Dummy title heresed do mod a type... </p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Provider Name</span>
                                                <p class="add_ttrr">Avishek Bera</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Category</span>
                                                <p class="add_ttrr">Carpenter</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
                                                <p class="add_ttrr">10.10.2021</p>
                                            </div>

                                            <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Budget</span>
                                                <p class="add_ttrr">₹150/hr. </p>
                                            </div>
                
                                            <div class="cell1 tab_head_sheet_1 msg_rm_01"> <span class="W55_1">Message </span>
	                                                <a href="javascript:void(0);" class="msg_rm"><strong class="msg_rm_02">
                                                        <img src="{{ url('public/frontend/images/messg.png') }}" alt=""> Message</strong> </a>
                                                    </div>
                                            <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
                                                <div class="add_ttrr actions-main text-center">
                                                    <a href="javascript:void(0);" class="action-dots" id="action14"><img src="{{ URL::to('public/frontend/images/action-dots.png')}}" alt=""></a>
                                                    <div class="show-actions" id="show-action14" style="display: none;"> <span class="angle"><img src="{{ URL::to('public/frontend/images/angle.png')}}" alt=""></span>
                                                        <ul class="text-left">

                                                            <li><a href="#">Edit</a></li>
                                                            <li><a href="#">Delete</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div>
                        <div>
                            <div class="user_statis">
                                <em>
                                    <i class="icofont-ui-calendar"></i>
                                </em>
                                <p> Upcoming Visits : <span> {{ @$allVisitRequest->count() }} </span></p>
                            </div>
                        </div>
                        <div class="custom-table ">
                            <div class="new-table-mr">
                                <div class="table">
                                    <div class="one_row1 hidden-sm-down only_shawo">
                                        <div class="cell1 tab_head_sheet">Property	</div>
                                        <div class="cell1 tab_head_sheet">Agent</div>
                                        <div class="cell1 tab_head_sheet">Date </div>
                                        <div class="cell1 tab_head_sheet">Time </div>
                                        <div class="cell1 tab_head_sheet">Location </div>
                                        <div class="cell1 tab_head_sheet">&nbsp;</div>
                                        <div class="cell1 tab_head_sheet">Action </div>
                                    </div>
                                    <!--row 1-->
                                    <!--row 1-->
                                    <!--row 1-->
                                    @if(count(@$allVisitRequest)>0)
                                      @foreach($allVisitRequest as $vr)
                                    <div class="one_row1 small_screen31">
                                        <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Property	</span>
                                            <p class="add_ttrr"> <a href="{{ route('search.property.details',['slug'=>@$vr->propertyDetails->slug]) }}">
                                            @if(strlen(@$vr->propertyDetails->name)>20)
                                             {!! substr(@$vr->propertyDetails->name,0,20) . '...'!!}
                                              @else
                                             {!! @$vr->propertyDetails->name !!}
                                             @endif
                                            </a></p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1 half-boxes agents"> <span class="W55_1">Agent</span>
                                            <p class="add_ttrr"><a href="{{ route('agent.public.profile',['slug'=>@$vr->agentDetails->slug]) }}">{{ @$vr->agentDetails->name }}</a></p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1 half-boxes dates"> <span class="W55_1">Date</span>
                                            <p class="add_ttrr">{{ date('d.m.Y',strtotime(@$vr->visit_date)) }}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1 half-boxes dates"> <span class="W55_1">Time</span>
                                            <p class="add_ttrr">{{ date('h:i A',strtotime(@$vr->visit_date)) }}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1 dates"> <span class="W55_1">Location </span>
                                            <p class="add_ttrr">{{ @$vr->address }}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1 msg_rm_01"> <span class="W55_1">Message </span>

                                        <!--<a href="javascript:void(0);" class="msg_rm chat_btn" data-userid="{{@Auth::user()->id}}" data-agentid="{{@$vr->propertyDetails->propertyUser->id}}" data-name="{{@$vr->propertyDetails->propertyUser->name}}" data-username="{{@Auth::user()->name}}" data-propertyid="{{@$vr->propertyDetails->id}}"><strong class="msg_rm_02">-->
                                        <!--<img src="{{ url('public/frontend/images/messg.png') }}" alt=""> Message</strong> </a>-->
                                        
                                         <a href="javascript:void(0);" class="msg_rm chat_btn" data-userid="{{@Auth::user()->id}}" data-agentid="{{@$vr->propertyDetails->propertyUser->id}}" data-name="{{@$vr->propertyDetails->propertyUser->name}}" data-username="{{@Auth::user()->name}}" data-propertyid="0" data-providerid="{{@$vr->propertyDetails->propertyUser->id}}"><strong class="msg_rm_02">
                                        <img src="{{ url('public/frontend/images/messg.png') }}" alt=""> Message</strong> </a>
                                                        <!-- <a href="javascript:void(0);" class="msg_rm"><strong class="msg_rm_02">
                                                        <img src="{{ url('public/frontend/images/messg.png') }}" alt=""> Message</strong> </a> -->
                                        </div>
                                        <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
                                            <div class="add_ttrr actions-main text-center">
                                                <a href="javascript:void(0);" class="action-dots" id="action{{ @$vr->id }}" data-id="{{ @$vr->id }}"><img src="{{ URL::to('public/frontend/images/action-dots.png')}}" alt=""></a>
                                                <div class="show-actions" id="show-action{{ @$vr->id }}" style="display: none;"> <span class="angle"><img src="{{ URL::to('public/frontend/images/angle.png')}}" alt=""></span>
                                                    <ul class="text-left">
                                                        {{--<li><a data-toggle="modal" data-target="#myModal">Reschedule </a></li>--}}
                                                        <li><a href="{{ route('user.property.visit.request.cancel',['id'=>@$vr->id]) }}" onclick="return confirm('Are you want to cancel this visit request?')">Cancel</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                                  @else
                                                  @if(@$allVisitRequest->isEmpty())
                                                   <div class="col-sm-12 col-md-12" align="center">
                                                   <center style="padding: 10px"><p>No Data Found</p></center>
                                                   </div>
                                                   @endif
                                                   @endif
                                    <!--row 1-->
                                    {{--<div class="one_row1 small_screen31">
                                        <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Property	</span>
                                            <p class="add_ttrr"> <a href="property-details.html">Pacific Heights Area</a></p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1 half-boxes agents"> <span class="W55_1">Agent</span>
                                            <p class="add_ttrr"><a href="agent-public-profile.html">Aiden Benjamin</a></p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1 half-boxes dates"> <span class="W55_1">Date</span>
                                            <p class="add_ttrr">01.10.2021</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1 half-boxes dates"> <span class="W55_1">Time</span>
                                            <p class="add_ttrr">02:15pm</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1 dates"> <span class="W55_1">Location </span>
                                            <p class="add_ttrr">Hyderabad</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
                                            <div class="add_ttrr actions-main text-center">
                                                <a href="javascript:void(0);" class="action-dots" id="action29"><img src="{{ URL::to('public/frontend/images/action-dots.png')}}" alt=""></a>
                                                <div class="show-actions" id="show-action29" style="display: none;"> <span class="angle"><img src="{{ URL::to('public/frontend/images/angle.png')}}" alt=""></span>
                                                    <ul class="text-left">
                                                        <li><a data-toggle="modal" data-target="#myModal">Reschedule </a></li>
                                                        <li><a href="#">Cancel</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="one_row1 small_screen31">
                                        <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Property	</span>
                                            <p class="add_ttrr"> <a href="property-details.html">Pacific Heights Area</a></p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1 half-boxes agents"> <span class="W55_1">Agent</span>
                                            <p class="add_ttrr"><a href="agent-public-profile.html">Aiden Benjamin</a></p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1 half-boxes dates"> <span class="W55_1">Date</span>
                                            <p class="add_ttrr">01.10.2021</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1 half-boxes dates"> <span class="W55_1">Time</span>
                                            <p class="add_ttrr">02:15pm</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1 dates"> <span class="W55_1">Location </span>
                                            <p class="add_ttrr">Hyderabad</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
                                            <div class="add_ttrr actions-main text-center">
                                                <a href="javascript:void(0);" class="action-dots" id="action30"><img src="{{ URL::to('public/frontend/images/action-dots.png')}}" alt=""></a>
                                                <div class="show-actions" id="show-action30" style="display: none;"> <span class="angle"><img src="{{ URL::to('public/frontend/images/angle.png')}}" alt=""></span>
                                                    <ul class="text-left">
                                                        <li><a data-toggle="modal" data-target="#myModal">Reschedule </a></li>
                                                        <li><a href="#">Cancel</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>--}}
                                </div>
                            </div>
                        </div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>

@endsection


@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection



@section('script')
@include('includes.script')

<script>
         $(document).on('click', function () {
		@foreach(@$allVisitRequest as $value)
		var $target = $(event.target);
		if (!$target.closest('#action{{@$value->id}}').length && $('#show-action{{@$value->id}}').is(":visible")) {
			$('#show-action{{@$value->id}}').slideUp();
		}
		@endforeach
	});

	$(document).ready(function() {
		@foreach(@$allVisitRequest as $value)
			$("#action{{@$value->id}}").click(function() {
				$("#show-action{{@$value->id}}").slideToggle();
			});
		@endforeach
    });
    </script>

<script>
         $(document).on('click', function () {
		@foreach(@$jobProgressData as $value)
		var $target = $(event.target);
		if (!$target.closest('#action11{{@$value->id}}').length && $('#show-action11{{@$value->id}}').is(":visible")) {
			$('#show-action{{@$value->id}}').slideUp();
		}
		@endforeach
	});

	$(document).ready(function() {
		@foreach(@$jobProgressData as $value)
			$("#action11{{@$value->id}}").click(function() {
				$("#show-action11{{@$value->id}}").slideToggle();
			});
		@endforeach
    });
    </script>

@endsection

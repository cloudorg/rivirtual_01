@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
    .amount_err{
    	display: none;
    }
    .save_all_changes_btn {
    height: 48px;
    padding: 0px 10px;
    background: #338206;
    color: #fff;
    border-radius: 2px;
    font-family: 'Roboto', sans-serif;
    cursor: pointer;
    font-size: 17px;
    border: none;
    font-weight: 500;
    margin-top: 5px;
    border-radius: 8px;
    text-transform: capitalize;
}
</style>
<link href="{{ URL::asset('public/frontend/croppie/croppie.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('public/frontend/croppie/croppie.min.css') }}" rel="stylesheet" />
@endsection

@section('title')
<title> RiVirtual | Milestone </title>
@endsection




@section('header')
@include('includes.header')
@endsection

@section('content')

<div class="haeder-padding"></div>

<div class="user_dashboard">
		<div class="container">
			<div class="user_dashboard_inr">
            @include('includes.sidebar')
				<div class="user_dashboard_right">
					<h1><img src="{{ url('public/frontend/images/prop.png') }}" alt="">Milestone</h1>
                    @include('includes.message')
					<div class="proposal-list">
						<div class="proposal-de">
							<h5>
                                @if(strlen(@$jobDetails->job_name)>80)
                                 {!! substr(@$jobDetails->job_name,0,80) . '...' !!}
                                  @else
                                {!! @$jobDetails->job_name !!}
                                @endif
                              </h5>
							<ul>
								<li> <img src="{{ url('public/frontend/images/ic1.png') }}">{{ @$jobDetails->categoryName?@$jobDetails->categoryName->category_name:'' }}</li>
								<li> <img src="{{ url('public/frontend/images/ic2.png') }}">
                                    @if(@$jobDetails->budget_range_from != null)
                                   ₹{{ @$jobDetails->budget_range_from }}
                                    @else
									 --
                                   
                                    @endif
                                   </li>
								<li> <img src="{{ url('public/frontend/images/ic3.png') }}">{{ date('d.m.Y',strtotime(@$jobDetails->job_start_date)) }}</li>
								<li> <img src="{{ url('public/frontend/images/ic4.png') }}">{{ @$jobDetails->address }}</li>
								 @if(@$jobDetails->rivirtual_assistance == 'Y')
								<li>Get Rivirtual Assistance</li>
								@endif
							</ul>
						</div>
						<div class="proposal-action">
							<a href="javascript:void(0);" class="action-dots" id="action11"><img src="{{ url('public/frontend/./images/dot3.png') }}" alt="" class="hovern"> <img src="{{ url('public/frontend/images/doth.png') }}" class="hoverb"></a>
							<div class="show-actions" id="show-action11" style="display: none;"> <span class="angle"><img src="{{ url('public/frontend/./images/angle.png') }}" alt=""></span>
								<ul class="text-left">
								{{--<li><a href="#">Repost</a></li>--}}
								<li><a href="{{ route('user.post.job.delete',['id'=>@$jobDetails->id]) }}" onclick="return confirm('Are you want to delete this job?')">Delete</a></li>
								</ul>
							</div>
						</div>
					</div>
                    @if(@$jobDetails->assistance_name != null && @$jobDetails->assistance_phone != null)  
					<div class="proposal-list">
						<div class="proposal-de">
							<h5>
                               Rivirtual Assistance Details
                              </h5>
							<ul>
								<li><span>Name: &nbsp;</span> {{ @$jobDetails->assistance_name }}</li>
								<li> 
								<span>Phone: &nbsp;</span>	
								{{ @$jobDetails->assistance_phone }}
                                   </li>
								<li><span>Email: &nbsp;</span> {{ @$jobDetails->assistance_email }} </li>
							
							</ul>
						</div>
						{{--<div class="proposal-action">
							<a href="javascript:void(0);" class="action-dots" id="action11"><img src="{{ url('public/frontend/./images/dot3.png') }}" alt="" class="hovern"> <img src="{{ url('public/frontend/images/doth.png') }}" class="hoverb"></a>
							<div class="show-actions" id="show-action11" style="display: none;"> <span class="angle"><img src="{{ url('public/frontend/./images/angle.png') }}" alt=""></span>
								<ul class="text-left">
								<li><a href="#">Repost</a></li>
								<li><a href="{{ route('user.post.job.delete',['id'=>@$jobDetails->id]) }}" onclick="return confirm('Are you want to delete this job?')">Delete</a></li>
								</ul>
							</div>
						</div>--}}
					</div>
                     @endif
					<div class="user_das_right_inr">
                    
						
                        
						<div class="proposal-box">

							<div class="proposal-top">
								<div class="pro-user">
									<em>
									 	@if(@$quotes->proDetails->profile_pic != null)
                                        	<img src="{{ URL::to('storage/app/public/profile_picture') }}/{{ @$quotes->proDetails->profile_pic }}" alt="">
                                        @else
											<img src="{{ url('public/frontend/images/user1.png') }}">
                                    	@endif
                                	</em>
									<div class="pro-user-name">
										<h5><a href="{{ route('pro.profile',['slug'=>@$quotes->proDetails->slug]) }}">{{ @$quotes->proDetails->name }}</a> <span><i class="icofont-star"></i>{{ number_format(@$quotes->proDetails->avg_review,1,'.',',') }}</span></h5>
										<p>{{ @$quotes->proDetails->proToCategory->categoryName->category_name  }}</p>
									</div>
								</div>
								<div class="pro-award">
									<!-- <a href="#" class="award_btn "> <img src="images/awa.png"> Award</a>
									<a href="#" class=" ">Message</a> -->
								</div>
							</div>

							<div class="proposla-para">
								<p class="desc_bid" data-id="{{@$quotes->id}}">
								@if(strlen(@$quotes->quote_description)>100)
				
									<p class=" short_desc_bid short_desc_bid_{{@$quotes->id}}" data-desc="{{substr(@$quotes->quote_description, 0, 100)}}" data-id ="{{$quotes->id}}">
									<a href="javascript:;" class="read_more_bid" data-id="{{@$quotes->id}}"> Read more +</a>
									</p>
									<p class="full_desc_bid full_desc_bid_{{@$quotes->id}}"  data-desc="{{@$quotes->quote_description}}" style="display:none;" data-id="{{$quotes->id}}">
																	

									<a href="javascript:;" class="read_more_bid" data-id="{{@$quotes->id}}"> Read less -</a>
									</p>
									@else
									<p>{{@$quotes->description}}</p>
								@endif
														  	
							</p>
							</div>

							<div class="proposla-ul">
								<ul>
									<li><img src="{{ url('public/frontend/images/lista.png') }}"> 
                                    <span>Budget: <b>
                                        
                                        ₹{{ @$quotes->cost }}
                                        
                                        
                                       </b></span></li>
									<li><img src="{{ url('public/frontend/images/lista.png') }}"> <span>Time: <b>{{ @$quotes->timeline }} Days</b></span></li>
								</ul>
							</div>
						</div>

						<div class="proposal-box">
							<h4 class="mile_pay">
								Milestone Payments
							</h4>

							<a class="create-milestone" style="float: right;" href="javascript:;">Create Milestone</a>
                            
							<div class="clearfix"></div>

							<form style="display: none;" class="milestone_frm" method="POST" action="{{route('razorpay.index')}}">
                                @csrf
                                <input type="hidden" name="" value="{{@$max_limit}}">
                                <input type="hidden" name="bid_id" value="{{@$quotes->id}}">
                                <input type="hidden" name="post_id" value="{{@$jobDetails->id}}">
                                <input type="hidden" name="user_id" value="{{@$jobDetails->user_id}}">
                                <input type="hidden" name="pro_id" value="{{@$quotes->user_id}}">
                                <input type="hidden" name="page_type" value="M">
                                <div class="row" style="margin: 0 -9px; width: 100%;">
                                    <div class="col-lg-5 col-md-6 col-sm-12">
                                        <div class="das_input">
                                            <label class="label_input_title">Milestone Description </label>
                                            <input type="text" placeholder="Milestone Description" class="input-type-text" name="description">
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                        <div class="das_input" style="margin-bottom: 0px !important;">                                            
                                            <label class="label_input_title">Milestone Amount </label>
                                            <div class="bud_sets bud_ropi">
                                            <input type="text" placeholder="Milestone Amount" class="input-type-text amount" name="amount">
                                            <span class="budget_iconn02"><i class="fa fa-inr" aria-hidden="true"></i></span>
                                            </div>
                                            <label for="amount" generated="true" class="amount_err error">Enter Amount must be grater then 0.</label>
                                        </div>
                                        <label class="amt_conv"></label>
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-6 col-sm-12">
                                        <div class="das_input">
                                            <label class="label_input_title">&nbsp;</label>
                                            <button style="text-transform: capitalize;width: 100%;" type="submit" class="save_all_changes_btn pad-5 crt-btnn">Create Milestone</button>
                                        </div>
                                    </div>
                                </div>
                            </form>


                            <div class="main-tabel" style="margin-top:10px;">
                                <h4 class="txtcls mile_tit">Created Milestones</h4>
                                <div class="table-responsive">
                                    <div class="table">
                                        <div class="one_row1 hidden-sm-down only_shawo">
                                            <div class="cell1 tab_head_sheet">Date</div>
                                            <div style="width: 50%;" class="cell1 tab_head_sheet">Description</div>
                                            <div class="cell1 tab_head_sheet">Status</div>
                                            <div class="cell1 tab_head_sheet">Amount</div>
                                            <div class="cell1 tab_head_sheet">Action</div>
                                        </div>
                                        @foreach($createdM as $val)
                                        <div class="one_row1 small_screen31 last_row_table">
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">Date</span>
                                                <p class="add_ttrr">{{ date('d M Y',strtotime(@$val->created_at))}}</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">Description</span>
                                                <p class="add_ttrr" style=" word-break: break-all ! important;">{!! nl2br(@$val->description ? $val->description :'-') !!} 
                                                    
                                                </p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">Status</span>
                                                <p class="add_ttrr"> @if(@$val->status == 'F') Funded @elseif(@$val->status == 'R') Released @endif</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">Amount</span>
                                                <p class="add_ttrr">
                                                    {{@$val->amount}}
                                                </p>
                                            </div>
                                            
                                            
                                            
                                             @if(@$val->status != 'R' && @$val->is_requested_release == 'Y')
                                                   <div class="cell1 tab_head_sheet_1 last_padd last_padd_ac">
                                                        <span class="W55_1">Action</span>
                                                        <a href="javascript:;" class="rel-mod ml-rel" data-id="{{$val->id}}">Release</a>
                                                        <!-- <a href="javascript:void(0);" class="action-dots action_menu" data-id="{{$val->id}}" id="action-{{$val->id}}"><img src="{{asset('public/frontend/images/dot3.png')}}" alt=""></a> -->             
                                                        <!-- <div class="show-actions" id="show-action-{{$val->id}}">
                                                            <span class="angle"><img src="{{asset('public/images/angle.png') }}" alt=""></span>
                                                            <div class="add_ttrr actions-main"> 
                                                              <ul class="ac_ul">
                                                                
                                                                
                                                                <li><a href="javascript:;" class="rel-mod"  data-id="{{$val->id}}">Release</a></li>
                                                                <li><a href="javascript:;" class="cancel_milestone" data-id="{{@$val->id}}">Cancel</a></li>
                                                                
                                                              </ul>
                                                            </div>
                                                        </div> -->
                                                   </div>
                                            @elseif(@$val->status == 'R')
                                            	<div class="cell1 tab_head_sheet_1 last_padd last_padd_ac">
                                                        <span class="W55_1">Action</span>
                                                              
                                                      <span>Completed</span>
                                                </div>
                                            @else
                                            <div class="cell1 tab_head_sheet_1 last_padd last_padd_ac">
                                                        <span class="W55_1">Action</span>
                                                              
                                                      
                                                </div>
                                            @endif
                                            
                                            
                                             
                                           
                                            
                                                
                                            
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
							
						</div>
						

						
					</div>
				</div>
			</div>
		</div>
	</div>
    @endsection

    
 @section('footer')
@include('includes.footer')
@endsection


@section('script')
@include('includes.script')
<script>
		$(function() {
			$("#datepicker").datepicker();
		});
		$(function() {
			$("#datepicker2").datepicker();
		});
		$(function() {
			$("#datepicker3").datepicker();
		});
		</script>
		<script>
              $(document).on('click', function () {
		
		var $target = $(event.target);
		if (!$target.closest('#action11').length && $('#show-action11').is(":visible")) {
			$('#show-action11').slideUp();
		}
	});

	$(document).ready(function() {
			$("#action11").click(function() {
				$("#show-action11").slideToggle();
			});
		
    });
        </script>
 <script>
         $(document).ready(function(){

$('#short_by').change(function(){
          $('#searchFilter').submit();
      });

});
 </script>
 <script>
		// $("#short_desc").html($('#short_desc').data('desc'));

	$('.short_desc_bid').each(function(){

		var str1 = $(this).data('desc');
		var bid_id = $(this).data('id');
		console.log( "SHORT : "  + bid_id);
		$(this).html(str1 + "..." + `<a href="javascript:;" class="read_more_bid" data-id="${bid_id}"> Read more +</a>`);
	});
	$('.full_desc_bid').each(function(){
		var str2 = $(this).data('desc');
		var bid_id = $(this).data('id');
		console.log( "FULL : "  + bid_id);
		$(this).html(str2 + `<a href="javascript:;" class="read_more_bid" data-id="${bid_id}"> Read less -</a>`);
	});

	// $('.read_more').click(function(){
	$("body").delegate(".read_more_bid", "click", function(e) {
		var id = $(this).data('id');
		console.log(id);
		if($('.short_desc_bid_'+id).css('display') == "block"){
			$('.short_desc_bid_'+id).hide();
			$('.full_desc_bid_'+id).show();
		} else {
			$('.full_desc_bid_'+id).hide();
			$('.short_desc_bid_'+id).show();
		}
	});
	
</script>
<script>
	$('.create-milestone').click(function(){
        $('.milestone_frm').toggle();
    });

    $('.milestone_frm').validate({
            rules: {
                description: {required: true},
                amount: {required: true,number: true,min:0.0001,max:{{@$max_limit}} },
            },
            messages:{
                amount:{min: "Enter Amount must be grater then 0."}
            },
            submitHandler:function(form){
                var nmm = Number($('.amount').val());
                if(nmm>0){
                    $('.amount_err').attr("style", "display:none");
                    $('.amount').removeClass('error');
                    form.submit();
                } else {
                    $('.amount_err').attr("style", "display:block");
                    $('.amount').addClass('error');
                    return false;
                }
            },
        });

    	$('.action_menu').click(function(){
                var id = $(this).data('id');
                if($("#show-action-"+id).css('display')=="none"){
                        $('.show-actions').slideUp();
                        $("#show-action-"+id).slideDown();
                }else{
                    $('.show-actions').slideUp();
                }
         });

    	$('.cancel_milestone').click(function(){
    			var m_id = $(this).data('id');
    			if(confirm('Do you want to cancel this milestone ?')){
    				$.ajax({
                	url:'{{route('user.cancel.milestone')}}',
                	type:'GET',
                	data:{id:m_id},
                	success:function(data){
                		
                		if(data.status == 1){
                			location.reload();
                		}
                    }
                })
    		}
    	});


    	$('.rel-mod').click(function(){
    		var m_id = $(this).data('id');
    		if(confirm('Do you want to release this milestone ?')){
    				$.ajax({
                	url:'{{route('user.release.milestone')}}',
                	type:'GET',
                	data:{id:m_id},
                	success:function(data){
                		
                		if(data.status == 1){
                			location.reload();
                		}
                    }
                })
    		}
    	});
</script>
@endsection
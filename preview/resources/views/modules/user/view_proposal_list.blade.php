@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
<link href="{{ URL::asset('public/frontend/croppie/croppie.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('public/frontend/croppie/croppie.min.css') }}" rel="stylesheet" />
@endsection

@section('title')
<title> RiVirtual | Proposal List </title>
@endsection




@section('header')
@include('includes.header')
@endsection

@section('content')

<div class="haeder-padding"></div>

<div class="user_dashboard">
		<div class="container">
			<div class="user_dashboard_inr">
            @include('includes.sidebar')
				<div class="user_dashboard_right">
					<h1><img src="{{ url('public/frontend/images/prop.png') }}" alt=""> Proposal List</h1>
                    @include('includes.message')
					<div class="proposal-list">
						<div class="proposal-de">
							<h5>
                                @if(strlen(@$jobDetails->job_name)>80)
                                 {!! substr(@$jobDetails->job_name,0,80) . '...' !!}
                                  @else
                                {!! @$jobDetails->job_name !!}
                                @endif
                              </h5>
							<ul>
								<li> <img src="{{ url('public/frontend/images/ic1.png') }}">{{ @$jobDetails->categoryName?@$jobDetails->categoryName->category_name:'' }}</li>
								<li> <img src="{{ url('public/frontend/images/ic2.png') }}">
                                    @if(@$jobDetails->budget_range_from != null)
                                   ₹{{ @$jobDetails->budget_range_from }}
                                    @else
									 --
                                   
                                    @endif
                                   </li>
								<li> <img src="{{ url('public/frontend/images/ic3.png') }}">{{ date('d.m.Y',strtotime(@$jobDetails->job_start_date)) }}</li>
								<li> <img src="{{ url('public/frontend/images/ic4.png') }}">{{ @$jobDetails->address }}</li>
								 @if(@$jobDetails->rivirtual_assistance == 'Y')
								<li>Get Rivirtual Assistance</li>
								@endif
							</ul>
						</div>
						<div class="proposal-action">
							<a href="javascript:void(0);" class="action-dots" id="action11"><img src="{{ url('public/frontend/./images/dot3.png') }}" alt="" class="hovern"> <img src="{{ url('public/frontend/images/doth.png') }}" class="hoverb"></a>
							<div class="show-actions" id="show-action11" style="display: none;"> <span class="angle"><img src="{{ url('public/frontend/./images/angle.png') }}" alt=""></span>
								<ul class="text-left">
								{{--<li><a href="#">Repost</a></li>--}}
								<li><a href="{{ route('user.post.job.delete',['id'=>@$jobDetails->id]) }}" onclick="return confirm('Are you want to delete this job?')">Delete</a></li>
								</ul>
							</div>
						</div>
					</div>

				     @if(@$jobDetails->assistance_name != null && @$jobDetails->assistance_phone != null)  
					<div class="proposal-list">
						<div class="proposal-de">
							<h5>
                               Rivirtual Assistance Details
                              </h5>
							<ul>
								<li><span>Name: &nbsp;</span> {{ @$jobDetails->assistance_name }}</li>
								<li> 
								<span>Phone: &nbsp;</span>	
								{{ @$jobDetails->assistance_phone }}
                                   </li>
								<li><span>Email: &nbsp;</span> {{ @$jobDetails->assistance_email }} </li>
							
							</ul>
						</div>
						{{--<div class="proposal-action">
							<a href="javascript:void(0);" class="action-dots" id="action11"><img src="{{ url('public/frontend/./images/dot3.png') }}" alt="" class="hovern"> <img src="{{ url('public/frontend/images/doth.png') }}" class="hoverb"></a>
							<div class="show-actions" id="show-action11" style="display: none;"> <span class="angle"><img src="{{ url('public/frontend/./images/angle.png') }}" alt=""></span>
								<ul class="text-left">
								<li><a href="#">Repost</a></li>
								<li><a href="{{ route('user.post.job.delete',['id'=>@$jobDetails->id]) }}" onclick="return confirm('Are you want to delete this job?')">Delete</a></li>
								</ul>
							</div>
						</div>--}}
					</div>
                     @endif

					<div class="user_das_right_inr">
                    <form action="{{ route('user.proposal.list',['id' => @$jobDetails->id]) }}" method="post" id="searchFilter">
                        @csrf
						<div class="por-heding">
							<h4>All Proposal</h4>
							<div class="sort-filter">
								<p>Sort By : </p>
								<select class="sort-select" name="sort_by_value" id="short_by">
									<option value="L" @if(@$request['sort_by_value'] == 'L') selected @endif>Budget Low to High</option>
									<option value="H" @if(@$request['sort_by_value'] == 'H') selected @endif>Budget High to Low</option>

									<option value="N" @if(@$request['sort_by_value'] == 'N') selected @endif>New Proposals</option>
								</select>
							</div>
						</div>
                       </form>
						<div class="row">
							<div class="col-md-12">
								<div class="bodar1"> </div>
							</div>
						</div>
                        @if(count(@$allProposal)>0)
                         @foreach(@$allProposal as $proposal)
						<div class="proposal-box">

							<div class="proposal-top">
								<div class="pro-user">
									<em>
                                        @if(@$proposal->proDetails->profile_pic != null)
                                        <img src="{{ URL::to('storage/app/public/profile_picture') }}/{{ @$proposal->proDetails->profile_pic }}" alt="">
                                        @else
										<img src="{{ URL::to('public/frontend/images/avatar.png')}}" alt="" id="img2">
                                        @endif
									</em>
									<div class="pro-user-name">
										<h5><a href="{{ route('pro.profile',['slug'=>@$proposal->proDetails->slug]) }}">{{ @$proposal->proDetails->name }}</a> <span><i class="icofont-star"></i>{{ number_format(@$proposal->proDetails->avg_review,1,'.',',') }}</span></h5>
										<p>{{ @$proposal->proDetails->proToCategory->categoryName->category_name  }}</p>
									</div>
								</div>
								<div class="pro-award">
									@if(@$proposal->status == 'Bid Placed')
									 @if(@$proposal->is_recommended == 1)
									<a href="javascript:;" class="award_btn recomm_btn"> <img src="{{ url('public/frontend/images/awa.png') }}">Recommended</a>
									@endif
									<a href="{{ route('user.job.award',['id'=>@$proposal->id]) }}" class="award_btn " onclick="return confirm(' Do you want to award this job?')"> <img src="{{ url('public/frontend/images/awa.png') }}"> Award</a>
									@elseif(@$proposal->status == 'Awarded')
									<a href="javascript:;" class="ar_bt "> <img src="{{ url('public/frontend/images/awa2.png') }}">Awarded</a>
										@if(!@$proposal->jobDetails->getMilestone)
											<a href="{{ route('user.job.award',['id'=>@$proposal->id]) }}" class="award_btn revoke_btn" onclick="return confirm('Are you want to revoked this job?')"> <img src="{{ url('public/frontend/images/awa1.png') }}">Revoke</a>
										@endif
									
									<a href="{{route('user.get.milestone',@$proposal->id)}}" class="award_btn mile_btn">Milestone</a>

									@endif
									@if(@$proposal->status == 'Awarded')
									<!--<a href="javascript:;" class="chat_btn" data-userid="{{@Auth::user()->id}}" data-agentid="{{@$proposal->user_id}}" data-name="{{@$proposal->proDetails->name}}" data-username="{{@Auth::user()->name}}" data-usertype="P" data-msgtyp="Pr" data-propertyid="{{@$proposal->user_id}}" data-bidid="{{@$proposal->id}}" data-providerid="{{@$proposal->user_id}}">Message</a>-->
									<a href="javascript:;" class="chat_btn" data-userid="{{@Auth::user()->id}}" data-agentid="{{@$proposal->user_id}}" data-name="{{@$proposal->proDetails->name}}" data-username="{{@Auth::user()->name}}"  data-propertyid="0"  data-providerid="{{@$proposal->user_id}}">Message</a>

									@endif
									
								</div>
							</div>

							<div class="proposla-para">
							<!-- @if(strlen(@$proposal->quote_description)>800)
				
								<p id="short_desc" data-desc="{{substr(@$proposal->quote_description, 0, 800)}}">
														
								</p>
								<p id="full_desc" style="display:none;" data-desc="{{@$proposal->quote_description}}">
														
								</p>
							@else
								<p>{!!@$proposal->quote_description!!}</p>
							@endif -->
							<p class="desc_bid" data-id="{{@$proposal->id}}">
								@if(strlen(@$proposal->quote_description)>100)
				
									<p class=" short_desc_bid short_desc_bid_{{@$proposal->id}}" data-desc="{{substr(@$proposal->quote_description, 0, 100)}}" data-id ="{{$proposal->id}}">
									<a href="javascript:;" class="read_more_bid" data-id="{{@$proposal->id}}"> Read more +</a>
									</p>
									<p class="full_desc_bid full_desc_bid_{{@$proposal->id}}"  data-desc="{{@$proposal->quote_description}}" style="display:none;" data-id="{{$proposal->id}}">
																	

									<a href="javascript:;" class="read_more_bid" data-id="{{@$proposal->id}}"> Read less -</a>
									</p>
									@else
									<p>{{@$proposal->quote_description}}</p>
								@endif
														  	
							</p>
                            <!-- <p>
							@if(strlen(@$proposal->quote_description)>200)
							{!! substr(@$proposal->quote_description,0,200) . '...' !!}<br>
							<p class="moretext{{ @$proposal->id }}">
							{!! @$proposal->quote_description !!}
	                        </p>
                            <a class="moreless-button{{@$proposal->id }}">Read More +</a>
							@else
							
							{!! @$proposal->quote_description !!}
							@endif
						 </p> -->
								{{--<p class="moretext" style="display: none;">
		                          Blue sapphire is a very cold and extremely powerful gem stone and represents planet Saturn. It blesses with immense good luck, wealth and prosperity. 
		                        </p><a class="moreless-button"> Read More + </a>--}}
							</div>

							<div class="proposla-ul">
								<ul>
									<li><img src="{{ url('public/frontend/images/lista.png') }}"> 
                                    <span>Budget: <b>
                                        
                                        ₹{{ @$proposal->cost }}
                                        
                                        
                                       </b></span></li>
									<li><img src="{{ url('public/frontend/images/lista.png') }}"> <span>Time: <b>{{ @$proposal->timeline }} Days</b></span></li>
								</ul>
							</div>
						</div>
                        @endforeach
                         @else
                             <div class="col-md-12">
                                    <div class="product-item search_product_item" style="text-align: center">
                                        <img src="{{url('public/frontend/images/error-no-search-results.png')}}" style="padding-top: 60px; width: 300px;">
                                    </div>
                                    <h4 style="color: #ef1600; text-align: center; font-size: 20px;">No proposal available.</h4>
                                    <h6 style=" text-align: center;">Please check the spelling or try searching for something else</h6>
                                </div>
                                @endif
						{{--<div class="proposal-box">

							<div class="proposal-top">
								<div class="pro-user">
									<em>
										<img src="images/user2.png">
									</em>
									<div class="pro-user-name">
										<h5><a href="#">Rahul Manna</a> <span><i class="icofont-star"></i> 4.5</span></h5>
										<p>Housekeeper</p>
									</div>
								</div>
								<div class="pro-award">
									<a href="#" class="award_btn "> <img src="images/awa.png"> Award</a>
									<a href="#" class=" ">Message</a>
								</div>
							</div>

							<div class="proposla-para">
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and electronic typesetting, remaining essentially unchanged. </p>
								<p class="moretext" style="display: none;">
		                          Blue sapphire is a very cold and extremely powerful gem stone and represents planet Saturn. It blesses with immense good luck, wealth and prosperity. 
		                        </p><a class="moreless-button"> Read More + </a>
							</div>

							<div class="proposla-ul">
								<ul>
									<li><img src="images/lista.png"> <span>Budget: <b>₹128/hr. </b></span></li>
									<li><img src="images/lista.png"> <span>Time: <b>25 Days</b></span></li>
								</ul>
							</div>
						</div>
						<div class="proposal-box">

							<div class="proposal-top">
								<div class="pro-user">
									<em>
										<img src="images/user3.png">
									</em>
									<div class="pro-user-name">
										<h5><a href="#">Soumyadip Das</a> <span><i class="icofont-star"></i> 4.5</span></h5>
										<p>Carpenter</p>
									</div>
								</div>
								<div class="pro-award">
									<a href="#" class="award_btn "> <img src="images/awa.png"> Award</a>
									<a href="#" class=" ">Message</a>
								</div>
							</div>

							<div class="proposla-para">
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and electronic typesetting, remaining essentially unchanged. </p>
								<p class="moretext" style="display: none;">
		                          Blue sapphire is a very cold and extremely powerful gem stone and represents planet Saturn. It blesses with immense good luck, wealth and prosperity. 
		                        </p><a class="moreless-button"> Read More + </a>
							</div>

							<div class="proposla-ul">
								<ul>
									<li><img src="images/lista.png"> <span>Budget: <b>₹150/hr. </b></span></li>
									<li><img src="images/lista.png"> <span>Time: <b>25 Days</b></span></li>
								</ul>
							</div>
						</div>--}}

						<nav aria-label="...">
											<ul class="pagination new-pagination">
                                            {{$allProposal->appends(request()->except(['page', '_token']))->links()}}
												{{--<li class="page-item"> <span class="page-link">Prev </span> </li>
												<li class="page-item active"><a class="page-link" href="#">1</a></li>
												<li class="page-item" aria-current="page"> <span class="page-link">
                                                2
                                                <span class="sr-only">(current)</span> </span>
												</li>
												<li class="page-item"><a class="page-link" href="#">3</a></li>
												<li class="page-item"><a class="page-link" href="#">4</a></li>
												<li class="page-item"> <a class="page-link act" href="#">Next </a> </li>--}}
											</ul>
										</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
    @endsection

    
 @section('footer')
@include('includes.footer')
@endsection


@section('script')
@include('includes.script')
<script>
		$(function() {
			$("#datepicker").datepicker();
		});
		$(function() {
			$("#datepicker2").datepicker();
		});
		$(function() {
			$("#datepicker3").datepicker();
		});
		</script>
		<script>
              $(document).on('click', function () {
		
		var $target = $(event.target);
		if (!$target.closest('#action11').length && $('#show-action11').is(":visible")) {
			$('#show-action11').slideUp();
		}
	});

	$(document).ready(function() {
			$("#action11").click(function() {
				$("#show-action11").slideToggle();
			});
		
    });
        </script>
 <script>
         $(document).ready(function(){

$('#short_by').change(function(){
          $('#searchFilter').submit();
      });

});
 </script>
 <script>
		// $("#short_desc").html($('#short_desc').data('desc'));

	$('.short_desc_bid').each(function(){

		var str1 = $(this).data('desc');
		var bid_id = $(this).data('id');
		console.log( "SHORT : "  + bid_id);
		$(this).html(str1 + "..." + `<a href="javascript:;" class="read_more_bid" data-id="${bid_id}"> Read more +</a>`);
	});
	$('.full_desc_bid').each(function(){
		var str2 = $(this).data('desc');
		var bid_id = $(this).data('id');
		console.log( "FULL : "  + bid_id);
		$(this).html(str2 + `<a href="javascript:;" class="read_more_bid" data-id="${bid_id}"> Read less -</a>`);
	});

	// $('.read_more').click(function(){
	$("body").delegate(".read_more_bid", "click", function(e) {
		var id = $(this).data('id');
		console.log(id);
		if($('.short_desc_bid_'+id).css('display') == "block"){
			$('.short_desc_bid_'+id).hide();
			$('.full_desc_bid_'+id).show();
		} else {
			$('.full_desc_bid_'+id).hide();
			$('.short_desc_bid_'+id).show();
		}
	});
	
</script>
@endsection
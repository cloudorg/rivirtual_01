@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> RiVirtual | Saved Searches </title>
@endsection




@section('header')
@include('includes.header')
@endsection

@section('content')

<div class="haeder-padding"></div>


<div class="user_dashboard">

<div class="container">

    <div class="user_dashboard_inr">

    @include('includes.sidebar')

        <div class="user_dashboard_right">

            <h1><img src="{{ url('public/frontend/images/save.png') }}" alt=""> Saved searches</h1>					

            @include('includes.message')

            <div class="user_das_right_inr">

                

                <div class="agent-das-tab user-search-tabs">

                    <ul>

                        <li><a href="{{ route('user.save.property') }}">Properties </a></li>

                        <li><a href="{{ route('user.save.agent') }}">Agents  </a></li>

                        <li class="active"><a href="{{ route('user.save.pro') }}">Pro</a></li>

                    </ul>

                    <hr>

                </div>



                    <div class="our_pro_panel">

                        <div class="row">
                            @if(count(@$saveProData)>0)
                             @foreach(@$saveProData as $pro)
                            <div class="col-sm-6 col-md-4">

                                <div class="service-pro-info">

                                <div class="pro-wish">

                                        <a href="{{ route('user.save.pro.delete',['id'=>@$pro->id]) }}" onclick="return confirm('Do you want to delete this pro?')"> <img src="{{ url('public/frontend/images/delete.png') }}"></a>

                                    </div>

                                    <div class="ser-pro-info-top">

                                        <em>

                                            <img src="{{ URL::to('storage/app/public/profile_picture') }}/{{ @$pro->proDetails->profile_pic }}">

                                        </em>

                                        <h2><a href="{{ route('pro.profile',['slug'=>@$pro->proDetails->slug]) }}"> {{ @$pro->proDetails->name }} </a> <span> - ({{ @$pro->proDetails->experience }} Year) </span></h2>

                                        <p>
                                         @if(strlen(@$pro->proDetails->about) > 25)

                                        {{ substr(@$pro->proDetails->about,0,25)}}...
                                        @else 
                                        {{@$pro->proDetails->about}} 
                                         @endif
                                        </p>

                                        <ul>

                                        @if(@$pro->proDetails->avg_review>=0.75 && @$pro->proDetails->avg_review<=1.24)
													 @php
													 $ii = 1;
													 @endphp
													 @for($i=1; $i<=$ii; $i++)
													<li><i class="icofont-star"></i></li>
													 @endfor
													 <li class="grey-star"><i class="icofont-star"></i></li>
													<li class="grey-star"><i class="icofont-star"></i></li>
													<li class="grey-star"><i class="icofont-star"></i></li>
													<li class="grey-star"><i class="icofont-star"></i></li>
													<li><p>{{ @$ii }}<span> ({{ @$pro->proDetails->tot_review }})</span></p></li>

													@elseif(@$pro->proDetails->avg_review>=1.25 && @$pro->proDetails->avg_review<=1.74)
													 @php
													 $ii = 1.5;
													 @endphp
													 @for($i=1; $i<=1; $i++)
													<li><i class="icofont-star"></i></li>
													 @endfor
													 <li><i class="fa fa-star-half-o"></i></li>
													 <li class="grey-star"><i class="icofont-star"></i></li>
													<li class="grey-star"><i class="icofont-star"></i></li>
													<li class="grey-star"><i class="icofont-star"></i></li>
													
													<li><p>{{ @$ii }}<span> ({{ @$pro->proDetails->tot_review }})</span></p></li>
                                                    
												  @elseif(@$pro->proDetails->avg_review>=1.75 && @$pro->proDetails->avg_review<=2.24)
													 @php
													 $ii = 2;
													 @endphp
													 @for($i=1; $i<=$ii; $i++)
													<li><i class="icofont-star"></i></li>
													 @endfor
													 <li class="grey-star"><i class="icofont-star"></i></li>
													<li class="grey-star"><i class="icofont-star"></i></li>
													<li class="grey-star"><i class="icofont-star"></i></li>
													<li><p>{{ @$ii }}<span> ({{ @$pro->proDetails->tot_review }})</span></p></li>

													@elseif(@$pro->proDetails->avg_review>=2.25 && @$pro->proDetails->avg_review<=2.74)
													 @php
													 $ii = 2.5;
													 @endphp
													 @for($i=1; $i<=2; $i++)
													<li><i class="icofont-star"></i></li>
													 @endfor
													 <li><i class="fa fa-star-half-o"></i></li>
													 <li class="grey-star"><i class="icofont-star"></i></li>
													<li class="grey-star"><i class="icofont-star"></i></li>
													
													<li><p>{{ @$ii }}<span> ({{ @$pro->proDetails->tot_review }})</span></p></li>

													@elseif(@$pro->proDetails->avg_review>=2.75 && @$pro->proDetails->avg_review<=3.24)
													 @php
													 $ii = 3;
													 @endphp
													 @for($i=1; $i<=$ii; $i++)
													<li><i class="icofont-star"></i></li>
													 @endfor
													 <li class="grey-star"><i class="icofont-star"></i></li>
													<li class="grey-star"><i class="icofont-star"></i></li>
													<li><p>{{ @$ii }}<span> ({{ @$pro->proDetails->tot_review }})</span></p></li>
                                                      
													
													@elseif(@$pro->proDetails->avg_review>=3.25 && @$pro->proDetails->avg_review<=3.74)
													 @php
													 $ii = 3.5
													 @endphp
													 @for($i=1; $i<=3; $i++)
													<li><i class="icofont-star"></i></li>
													 @endfor
													 <li><i class="fa fa-star-half-o"></i></li>
													<li class="grey-star"><i class="icofont-star"></i></li>
													<li><p>{{ @$ii }}<span> ({{ @$pro->proDetails->tot_review }})</span></p></li>

													@elseif(@$pro->proDetails->avg_review>=3.75 && @$pro->proDetails->avg_review<=4.24)
													 @php
													 $ii = 4
													 @endphp
													 @for($i=1; $i<=4; $i++)
													<li><i class="icofont-star"></i></li>
													 @endfor
													 <li class="grey-star"><i class="icofont-star"></i></li>
			
													<li><p>{{ @$ii }}<span> ({{ @$pro->proDetails->tot_review }})</span></p></li>
													@elseif(@$pro->proDetails->avg_review>=4.25 && @$pro->proDetails->avg_review<=4.74)
													 @php
													 $ii = 4.5
													 @endphp
													 @for($i=1; $i<=4; $i++)
													<li><i class="icofont-star"></i></li>
													 @endfor
													 <li><i class="fa fa-star-half-o"></i></li>
													<li><p>{{ @$ii }}<span> ({{ @$pro->proDetails->tot_review }})</span></p></li>
													@elseif(@$pro->proDetails->avg_review>=4.75 && @$pro->proDetails->avg_review<=5.24)
													 @php
													 $ii = 5
													 @endphp
													 @for($i=1; $i<=5; $i++)
													<li><i class="icofont-star"></i></li>
													 @endfor
				
													<li><p>{{ @$ii }}<span> ({{ @$pro->proDetails->tot_review }})</span></p></li>
                                                      @endif

                                        </ul>

                                    </div>



                                    <div class="ser-pro-info-bottom">

                                        <div class="providers-info">

                                            <p><img src="{{ url('public/frontend/images/p1.png') }}">{{ @$pro->proToCategory?@$pro->proToCategory->categoryName->category_name:'' }}</p>

                                            <p><img src="{{ url('public/frontend/images/p2.png') }}">
                                             @if(strlen(@$pro->proDetails->address) > 25)

                                            {{ substr(@$pro->proDetails->address,0,25)}}...
                                             @else 
                                             {{@$pro->proDetails->address}} 
                                              @endif
                                             </p>

                                            <p><img src="{{ url('public/frontend/images/p3.png') }}"> 
                                            
                                            @if(!@$pro->proToLanguage->isEmpty())

													 @php
                                                      
													 $i = count($pro->proToLanguage)-1;
													 @endphp

													  @foreach($pro->proToLanguage as $key=>$language)

													  <span>{{ $language->userLanguage->name }}</span>@if($key == $i) @else ,@endif
												     @endforeach
													 @endif
                                        
                                            </p>

                                        </div>

                                        <hr class="prod-hr">

                                        <div class="serp-budget">

                                            <div class="bud-details">

                                                <h5><img src="{{ url('public/frontend/images/01.png') }}"> 
                                                @if(@$pro->proDetails->budget_type == 'H')
                                                 {{ @$pro->proDetails->budget }}/hr
                                                 @elseif(@$pro->proDetails->budget_type == 'F')
                                                 {{ @$pro->proDetails->budget }}/day
                                                  @endif
                                               </h5>

                                                <p>{{ @$pro->proDetails->total_project }} projects completed</p>

                                            </div>

                                            <a href="{{ route('pro.profile',['slug'=>@$pro->proDetails->slug]) }}">View profile</a>

                                        </div>

                                    </div>

                                    

                                </div>

                            </div>
                            @endforeach
                            @else
						      @if(@$saveProData->isEmpty())
                             <div class="col-sm-12 col-md-12">
                             <center style="padding: 10px"><p>No Pro Available</p></center>
                                 </div>
                             @endif
							 @endif
                            {{--<div class="col-sm-6 col-md-4">

                                <div class="service-pro-info">

                                    <div class="pro-wish">

                                        <a href="#"> <img src="{{ url('public/frontend/images/wish2.png') }}"></a>

                                    </div>

                                    <div class="ser-pro-info-top">

                                        <em>

                                            <img src="{{ url('public/frontend/images/upro2.png') }}">

                                        </em>

                                        <h2><a href="service-provider-profile.html"> Rabin Manna </a> <span> - (Expert) </span></h2>

                                        <p>This is a simply display name here</p>

                                        <ul>

                                            <li><i class="icofont-star"></i></li>

                                            <li><i class="icofont-star"></i></li>

                                            <li><i class="icofont-star"></i></li>

                                            <li><i class="icofont-star"></i></li>

                                            <li><i class="icofont-star"></i></li>

                                            <li><p>4.6 <span>(102)</span></p></li>

                                        </ul>

                                    </div>



                                    <div class="ser-pro-info-bottom">

                                        <div class="providers-info">

                                            <p><img src="{{ url('public/frontend/images/p1.png ') }}"> Real estate Agent</p>

                                            <p><img src="{{ url('public/frontend/images/p2.png ') }}"> Btlanta, GA 30354</p>

                                            <p><img src="{{ url('public/frontend/images/p3.png ') }}"> English, Hindi</p>

                                        </div>

                                        <hr class="prod-hr">

                                        <div class="serp-budget">

                                            <div class="bud-details">

                                                <h5><img src="{{ url('public/frontend/images/01.png ') }}"> 1025/hr</h5>

                                                <p>140 projects completed</p>

                                            </div>

                                            <a href="service-provider-profile.html">View Profile</a>

                                        </div>

                                    </div>

                                    

                                </div>

                        </div>

                        <div class="col-sm-6 col-md-4">

                                <div class="service-pro-info">

                                    <div class="pro-wish">

                                        <a href="#"> <img src="{{ url('public/frontend/images/wish2.png ') }}"></a>

                                    </div>

                                    <div class="ser-pro-info-top">

                                        <em>

                                            <img src="{{ url('public/frontend/images/pro3.png ') }}">

                                        </em>

                                        <h2><a href="service-provider-profile.html"> Saikat Basu </a> <span> - (Intermediate ) </span></h2>

                                        <p>This is a simply display name here</p>

                                        <ul>

                                            <li><i class="icofont-star"></i></li>

                                            <li><i class="icofont-star"></i></li>

                                            <li><i class="icofont-star"></i></li>

                                            <li class="grey-star"><i class="icofont-star"></i></li>

                                            <li class="grey-star"><i class="icofont-star"></i></li>

                                            <li><p>3 <span>(102)</span></p></li>

                                        </ul>

                                    </div>



                                    <div class="ser-pro-info-bottom">

                                        <div class="providers-info">

                                            <p><img src="{{ url('public/frontend/images/p1.png ') }}"> Construction project manager</p>

                                            <p><img src="{{ url('public/frontend/images/p2.png ') }}"> Btlanta, GA 30354</p>

                                            <p><img src="{{ url('public/frontend/images/p3.png ') }}"> English, Hindi</p>

                                        </div>

                                        <hr class="prod-hr">

                                        <div class="serp-budget">

                                            <div class="bud-details">

                                                <h5><img src="{{ url('public/frontend/images/01.png ') }}"> 1025/day</h5>

                                                <p>140 projects completed</p>

                                            </div>

                                            <a href="service-provider-profile.html">View Profile</a>

                                        </div>

                                    </div>

                                    

                                </div>

                        </div>--}}



                        {{--<div class="col-sm-6 col-md-4">

                                <div class="service-pro-info">

                                    <div class="pro-wish">

                                        <a href="#"> <img src="images/wish2.png"></a>

                                    </div>

                                    <div class="ser-pro-info-top">

                                        <em>

                                            <img src="images/pro4.png">

                                        </em>

                                        <h2><a href="service-provider-profile.html"> Rabin Manna </a> <span> - (Expert) </span></h2>

                                        <p>This is a simply display name here</p>

                                        <ul>

                                            <li><i class="icofont-star"></i></li>

                                            <li><i class="icofont-star"></i></li>

                                            <li><i class="icofont-star"></i></li>

                                            <li><i class="icofont-star"></i></li>

                                            <li><i class="icofont-star"></i></li>

                                            <li><p>4.6 <span>(102)</span></p></li>

                                        </ul>

                                    </div>



                                    <div class="ser-pro-info-bottom">

                                        <div class="providers-info">

                                            <p><img src="images/p1.png "> Real estate Agent</p>

                                            <p><img src="images/p2.png  "> Btlanta, GA 30354</p>

                                            <p><img src="images/p3.png  "> English, Hindi</p>

                                        </div>

                                        <hr class="prod-hr">

                                        <div class="serp-budget">

                                            <div class="bud-details">

                                                <h5><img src="images/01.png"> 1025/hr</h5>

                                                <p>140 projects completed</p>

                                            </div>

                                            <a href="service-provider-profile.html">View Profile</a>

                                        </div>

                                    </div>

                                    

                                </div>

                        </div>

                        <div class="col-sm-6 col-md-4">

                                <div class="service-pro-info">

                                    <div class="pro-wish">

                                        <a href="#"> <img src="images/wish2.png"></a>

                                    </div>

                                    <div class="ser-pro-info-top">

                                        <em>

                                            <img src="images/pro5.png">

                                        </em>

                                        <h2><a href="service-provider-profile.html"> Saikat Basu </a> <span> - (Intermediate ) </span></h2>

                                        <p>This is a simply display name here</p>

                                        <ul>

                                            <li><i class="icofont-star"></i></li>

                                            <li><i class="icofont-star"></i></li>

                                            <li><i class="icofont-star"></i></li>

                                            <li><i class="icofont-star"></i></li>

                                            <li><i class="icofont-star"></i></li>

                                            <li><p>4.6 <span>(102)</span></p></li>

                                        </ul>

                                    </div>



                                    <div class="ser-pro-info-bottom">

                                        <div class="providers-info">

                                            <p><img src="images/p1.png "> Construction project manager</p>

                                            <p><img src="images/p2.png  "> Btlanta, GA 30354</p>

                                            <p><img src="images/p3.png  "> English, Hindi</p>

                                        </div>

                                        <hr class="prod-hr">

                                        <div class="serp-budget">

                                            <div class="bud-details">

                                                <h5><img src="images/01.png"> 1025/day</h5>

                                                <p>140 projects completed</p>

                                            </div>

                                            <a href="service-provider-profile.html">View Profile</a>

                                        </div>

                                    </div>

                                    

                                </div>

                        </div>

                        <div class="col-sm-6 col-md-4">

                                <div class="service-pro-info">

                                    <div class="pro-wish">

                                        <a href="#"> <img src="images/wish2.png"></a>

                                    </div>

                                    <div class="ser-pro-info-top">

                                        <em>

                                            <img src="images/pro6.png">

                                        </em>

                                        <h2><a href="service-provider-profile.html"> Rabin Manna </a> <span> - (Expert) </span></h2>

                                        <p>This is a simply display name here</p>

                                        <ul>

                                            <li><i class="icofont-star"></i></li>

                                            <li><i class="icofont-star"></i></li>

                                            <li><i class="icofont-star"></i></li>

                                            <li><i class="icofont-star"></i></li>

                                            <li><i class="icofont-star"></i></li>

                                            <li><p>4.6 <span>(102)</span></p></li>

                                        </ul>

                                    </div>



                                    <div class="ser-pro-info-bottom">

                                        <div class="providers-info">

                                            <p><img src="images/p1.png "> Real estate Agent</p>

                                            <p><img src="images/p2.png  "> Btlanta, GA 30354</p>

                                            <p><img src="images/p3.png  "> English, Hindi</p>

                                        </div>

                                        <hr class="prod-hr">

                                        <div class="serp-budget">

                                            <div class="bud-details">

                                                <h5><img src="images/01.png"> 1025/hr</h5>

                                                <p>140 projects completed</p>

                                            </div>

                                            <a href="service-provider-profile.html">View Profile</a>

                                        </div>

                                    </div>

                                    

                                </div>

                        </div>--}}

                        </div>							

                    </div>







                    <nav aria-label="...">

                                    <ul class="pagination new-pagination">
                                    {{@$saveProData->appends(request()->except(['page', '_token']))->links()}}
                                        {{--<li class="page-item"> <span class="page-link">Prev </span> </li>

                                        <li class="page-item active"><a class="page-link" href="#">1</a></li>

                                        <li class="page-item" aria-current="page"> <span class="page-link">

                                        2

                                        <span class="sr-only">(current)</span> </span>

                                        </li>

                                        <li class="page-item"><a class="page-link" href="#">3</a></li>

                                        <li class="page-item"><a class="page-link" href="#">4</a></li>

                                        <li class="page-item"> <a class="page-link act" href="#">Next </a> </li>--}}

                                    </ul>

                                </nav>

                



            </div>

        </div>

    </div>

</div>

</div>
@endsection

@section('footer')
@include('includes.footer')
@endsection


@section('script')
@include('includes.script')


@endsection
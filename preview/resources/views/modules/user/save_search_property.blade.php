@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> RiVirtual | Saved Searches </title>
@endsection




@section('header')
@include('includes.header')
@endsection

@section('content')

<div class="haeder-padding"></div>

<div class="user_dashboard">

		<div class="container">

			<div class="user_dashboard_inr">

               @include('includes.sidebar')

				<div class="user_dashboard_right">

					<h1><img src="{{ url('public/frontend/images/save.png') }}" alt=""> Saved searches</h1>					

					@include('includes.message')

					<div class="user_das_right_inr">

						

						<div class="agent-das-tab user-search-tabs">

							<ul>

								<li class="active"><a href="{{ route('user.save.property') }}">Properties </a></li>

								<li><a href="{{ route('user.save.agent') }}">Agents  </a></li>

								<li><a href="{{ route('user.save.pro') }}">Pro </a></li>

							</ul>

							<hr>

							

						</div>



							<div class="our_properties_panel">

								<div class="row">
                                  @if(count(@$saveProperty)>0)
								   @foreach(@$saveProperty as $property)
									<div class="col-sm-6 col-md-4">

										<div class="property-item">

											<div class="listing-image">

													<a href="{{route('search.property.details',['slug'=>@$property->propertyDetails->slug])}}"><img src="{{ URL::to('storage/app/public/property_image') }}/{{ @$property->propertyDetails->propertyImageMain->image }}" alt="listing property">

													<div class="propety-info-top">									

													</div></a>
                                                     
													@if(@$property->propertyDetails->property_for =='R')
                                                <div class="ribbon-img">
                                                <div class="ribbon-vertical"><p>For Rent</p></div>
                                                   </div>
                                              @elseif(@$property->propertyDetails->property_for =='B')
                                           <div class="ribbon-img">
                                             <div class="ribbon-vertical Sale"><p>For Sale</p></div>
                                              </div>
                                                  @endif

													<div class="wish-property">

														<a href="{{ route('user.save.property.delete',['id'=>@$property->id]) }}" onclick="return confirm('Do you want to delete this property?')"><i class="fa fa-trash"></i></a>

													</div>	

													<div class="location-rating">

														<div class="product-img-location">

															<p><img src="{{ url('public/frontend/images/loac.png') }}">
															@if(strlen(@$property->propertyDetails->address) > 25)

                                                           {{ substr(@$property->propertyDetails->address,0,25)}}...
                                                            @else 
                                                        {{@$property->propertyDetails->address}} 
                                                           @endif
														</p>

														</div>

														<div class="rating-pro">

															<p><img src="{{ url('public/frontend/images/star.png') }}">{{ number_format(@$property->propertyDetails->avg_review,1,'.',',') }}</p>

														</div>

													</div>

											</div>

											<div class="property-detasils-info">

												<div class="name-price">

													<a href="{{route('search.property.details',['slug'=>@$property->propertyDetails->slug])}}"><h5>
														  @if(strlen(@$property->propertyDetails->name) > 40)
                                                       {!! substr(@$property->propertyDetails->name, 0, 40 ) . '..' !!}
                                                            @else
                                                      {!!@$property->propertyDetails->name!!}
                                                          @endif
													  </h5></a>

													<p>₹{{ number_format(@$property->propertyDetails->budget_range_from, 0, '.', ',') }} </p>

												</div>

												<ul class="list-item-info @if(@$property->propertyDetails->property_type == 'L' || @$property->propertyDetails->property_type == 'FRMH') only_land @endif">
											   		@if(@$property->propertyDetails->property_type == 'F' || @$property->propertyDetails->property_type == 'H' || @$property->propertyDetails->property_type == 'GTCM')
                                                    <li class="before">
                                                        <span>{{@$property->propertyDetails->no_of_bedrooms}} <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span> Bedrooms
                                                    </li>
                                                    @endif
                                                    @if(@$property->propertyDetails->property_type == 'WRH' || @$property->propertyDetails->property_type == 'SHR' || @$property->propertyDetails->property_type == 'INDT' || @$property->propertyDetails->property_type == 'RET')
                                                    <li class="before">
                                                        <span>{{@$property->propertyDetails->no_of_bedrooms}} <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span>Rooms
                                                    </li>
                                                    @endif
                                                    @if(@$property->propertyDetails->property_type == 'F' || @$property->propertyDetails->property_type == 'GTCM' || @$property->propertyDetails->property_type == 'H' || @$property->propertyDetails->property_type == 'R' || @$property->propertyDetails->property_type == 'OSP' || @$property->propertyDetails->property_type == 'WRH' || @$property->propertyDetails->property_type == 'SHR' || @$property->propertyDetails->property_type == 'INDT' || @$property->propertyDetails->property_type == 'RET')
                                                    <li class="before">
                                                        <span>{{@$property->propertyDetails->bathroom}} <img src="{{ URL::asset('public/frontend/images/bath.png')}}"></span> Bathrooms
                                                    </li>
                                                    @endif
                                                    @if(@$property->propertyDetails->property_type == 'OSP')
                                                    <li class="before">
                                                        <span>{{@$property->propertyDetails->office_class}} <img src="{{ URL::asset('public/frontend/images/ofc.png')}}"></span> Class
                                                    </li>
                                                    @endif
                                                    @if(@$property->propertyDetails->property_type == 'L' || @$property->propertyDetails->property_type == 'FRMH')
                                                    <li class="">
                                                        <span>{{@$property->propertyDetails->area}}<img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
                                                    </li>
                                                    @else
                                                    <li class="">
                                                        <span>{{@$property->propertyDetails->area}}<img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
                                                    </li>
                                                    @endif

												</ul>

											</div>

										</div>

									</div>
                                      @endforeach
                                       @else
									   @if(@$saveProperty->isEmpty())
                                     <div class="col-sm-12 col-md-12">
                                 <center style="padding: 10px"><p>No Poperty Available</p></center>
                                      </div>
                                       @endif
									  @endif
									{{--<div class="col-sm-6 col-md-4">

										<div class="property-item">

											<div class="listing-image">

													<a href="property-details.html"><img src="{{ url('public/frontend/images/list2.png') }}" alt="listing property">

													<div class="propety-info-top">									

													</div></a>

													<div class="ribbon-img">

														<div class="ribbon-vertical Sale"><p>For Sale</p></div>

													</div>

													<div class="wish-property">

														<a href="#"><i class="icofont-heart"></i></a>

													</div>	

													<div class="location-rating">

														<div class="product-img-location">

															<p><img src="{{ url('public/frontend/images/loac.png') }}">Karnataka</p>

														</div>

														<div class="rating-pro">

															<p><img src="{{ url('public/frontend/images/star.png') }}"> 4.7</p>

														</div>

													</div>

											</div>

											<div class="property-detasils-info">

												<div class="name-price">

													<a href="property-details.html"><h5>Blue Reef Properties</h5></a>

													<p>₹4,300 </p>

												</div>

												<ul class="list-item-info ">

													<li class="before">

														<span>3 <img src="{{ url('public/frontend/images/bed.png') }}"></span> Bedrooms

													</li>

													<li class="before">

														<span>2 <img src="{{ url('public/frontend/images/bath.png') }}"></span> Bathrooms

													</li>

													<li class="">

														<span>3450 <img src="{{ url('public/frontend/images/sqft.png') }}"></span> Square Ft

													</li>

												</ul>

											</div>

										</div>

									</div>

									<div class="col-sm-6 col-md-4">

										<div class="property-item">

											<div class="listing-image">

													<a href="property-details.html"><img src="{{ url('public/frontend/images/list2.png') }}" alt="listing property">

													<div class="propety-info-top">									

													</div></a>

													<div class="ribbon-img">

														<div class="ribbon-vertical Sale"><p>For Sale</p></div>

													</div>

													<div class="wish-property">

														<a href="#"><i class="icofont-heart"></i></a>

													</div>	

													<div class="location-rating">

														<div class="product-img-location">

															<p><img src="{{ url('public/frontend/images/loac.png') }}">Hyderabad</p>

														</div>

														<div class="rating-pro">

															<p><img src="{{ url('public/frontend/images/star.png') }}"> 4.5</p>

														</div>

													</div>

											</div>

											<div class="property-detasils-info">

												<div class="name-price">

													<a href="property-details.html"><h5>Blue Reef Properties</h5></a>

													<p>₹7,500 </p>

												</div>

												<ul class="list-item-info ">

													<li class="before">

														<span>3 <img src="{{ url('public/frontend/images/bed.png') }}"></span> Bedrooms

													</li>

													<li class="before">

														<span>2 <img src="{{ url('public/frontend/images/bath.png') }}"></span> Bathrooms

													</li>

													<li class="">

														<span>3450 <img src="{{ url('public/frontend/images/sqft.png') }}"></span> Square Ft

													</li>

												</ul>

											</div>

										</div>

									</div>--}}

									{{--<div class="col-sm-6 col-md-4">

										<div class="property-item">

											<div class="listing-image">

													<a href="property-details.html"><img src="images/list6.png" alt="listing property">

													<div class="propety-info-top">									

													</div></a>

													<div class="ribbon-img">

														<div class="ribbon-vertical Sale"><p>For Sale</p></div>

													</div>

													<div class="wish-property">

														<a href="#"><i class="icofont-heart"></i></a>

													</div>	

													<div class="location-rating">

														<div class="product-img-location">

															<p><img src="images/loac.png">Bangalore, Karnataka</p>

														</div>

														<div class="rating-pro">

															<p><img src="images/star.png"> 4.6</p>

														</div>

													</div>

											</div>

											<div class="property-detasils-info">

												<div class="name-price">

													<a href="property-details.html"><h5>Blue Reef Properties</h5></a>

													<p>₹4,800 </p>

												</div>

												<ul class="list-item-info ">

													<li class="before">

														<span>3 <img src="images/bed.png"></span> Bedrooms

													</li>

													<li class="before">

														<span>2 <img src="images/bath.png"></span> Bathrooms

													</li>

													<li class="">

														<span>3450 <img src="images/sqft.png"></span> Square Ft

													</li>

												</ul>

											</div>

										</div>

									</div>

									<div class="col-sm-6 col-md-4">

										<div class="property-item">

											<div class="listing-image">

													<a href="property-details.html"><img src="images/list1.png" alt="listing property">

													<div class="propety-info-top">									

													</div></a>

													<div class="ribbon-img">

														<div class="ribbon-vertical"><p>For Rent</p></div>

													</div>

													<div class="wish-property">

														<a href="#"><i class="icofont-heart"></i></a>

													</div>	

													<div class="location-rating">

														<div class="product-img-location">

															<p><img src="images/loac.png">USA</p>

														</div>

														<div class="rating-pro">

															<p><img src="images/star.png"> 4.9</p>

														</div>

													</div>

											</div>

											<div class="property-detasils-info">

												<div class="name-price">

													<a href="property-details.html"><h5>Blue Reef Properties</h5></a>

													<p>₹7,500 </p>

												</div>

												<ul class="list-item-info ">

													<li class="before">

														<span>3 <img src="images/bed.png"></span> Bedrooms

													</li>

													<li class="before">

														<span>2 <img src="images/bath.png"></span> Bathrooms

													</li>

													<li class="">

														<span>3450 <img src="images/sqft.png"></span> Square Ft

													</li>

												</ul>

											</div>

										</div>

									</div>

									<div class="col-sm-6 col-md-4">

										<div class="property-item">

											<div class="listing-image">

													<a href="property-details.html"><img src="images/list7.png" alt="listing property">

													<div class="propety-info-top">									

													</div></a>

													<div class="ribbon-img">

														<div class="ribbon-vertical"><p>For Rent</p></div>

													</div>

													<div class="wish-property">

														<a href="#"><i class="icofont-heart"></i></a>

													</div>	

													<div class="location-rating">

														<div class="product-img-location">

															<p><img src="images/loac.png">New York</p>

														</div>

														<div class="rating-pro">

															<p><img src="images/star.png"> 4.7</p>

														</div>

													</div>

											</div>

											<div class="property-detasils-info">

												<div class="name-price">

													<a href="property-details.html"><h5>Blue Reef Properties</h5></a>

													<p>₹10,500 </p>

												</div>

												<ul class="list-item-info ">

													<li class="before">

														<span>3 <img src="images/bed.png"></span> Bedrooms

													</li>

													<li class="before">

														<span>2 <img src="images/bath.png"></span> Bathrooms

													</li>

													<li class="">

														<span>3450 <img src="images/sqft.png"></span> Square Ft

													</li>

												</ul>

											</div>

										</div>

									</div>--}}

								</div>							

							</div>



							<nav aria-label="...">

											<ul class="pagination new-pagination">
											{{@$saveProperty->appends(request()->except(['page', '_token']))->links()}}
												{{--<li class="page-item"> <span class="page-link">Prev </span> </li>

												<li class="page-item active"><a class="page-link" href="#">1</a></li>

												<li class="page-item" aria-current="page"> <span class="page-link">

                                                2

                                                <span class="sr-only">(current)</span> </span>

												</li>

												<li class="page-item"><a class="page-link" href="#">3</a></li>

												<li class="page-item"><a class="page-link" href="#">4</a></li>

												<li class="page-item"> <a class="page-link act" href="#">Next </a> </li>--}}

											</ul>

										</nav>

						



					</div>

				</div>

			</div>

		</div>

	</div>
    @endsection

@section('footer')
@include('includes.footer')
@endsection


@section('script')
@include('includes.script')


@endsection
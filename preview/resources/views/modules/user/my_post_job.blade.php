@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
<link href="{{ URL::asset('public/frontend/croppie/croppie.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('public/frontend/croppie/croppie.min.css') }}" rel="stylesheet" />
@endsection

@section('title')
<title> RiVirtual | My Service Requirments </title>
@endsection




@section('header')
@include('includes.header')
@endsection

@section('content')

<div class="haeder-padding"></div>

<div class="user_dashboard">
		<div class="container">
			<div class="user_dashboard_inr">
            @include('includes.sidebar')
				<div class="user_dashboard_right">
					<h1><img src="{{ url('public/frontend/images/servr.png') }}" alt=""> My Service Requirements</h1>
                    @include('includes.message')
					<div class="user_das_right_inr">
						<form action="{{ route('user.my.post.job') }}" method="post">
							@csrf
							<div class="user_dashboard_form">
								<div class="row">
									<div class="col-md-10">
										<div class="row">
											<div class="col-md-4">
												<div class="das_input">
													<label>Service Category </label>
													<select name="category">
														<option value="">Select category </option>
                                                        @foreach($category as $cat)
														<option value="{{ @$cat->id }}" @if(@$key['category']==@$cat->id) selected @endif>{{ @$cat->category_name }}</option>
														@endforeach
													</select>
												</div>
											</div>
											<div class="col-md-4">
												<div class="das_input">
													<label>Jobs by Name</label>
													<input type="text" placeholder="Enter here.." name="job_title" value="{{ @$key['job_title'] }}"> </div>
											</div>
											<div class="col-md-4">
												<div class="das_input">
													<label>Date</label>
													<div class="dash-d">
														<input type="text" placeholder="From date" id="datepicker" name="start_date" value="{{ @$key['start_date'] }}" autocomplete="off"> <span class="over_llp1"><img src="{{ url('public/frontend/images/cala.png') }}" alt=""></span> </div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-2 col-sm-4">
										<button class="dash-btn" type="submit"> <img src="{{ url('public/frontend/images/ser.png') }}"> Search </button>
									</div>
									{{--<input type="hidden" name="tab" id="tab" value="{{@$key['tab']}}">--}}
									<div class="col-md-12">
										<div class="bodar"> </div>
									</div>
								</div>
							</div>
						</form>
                       
						<div class="main-tabels">
							<!-- Nav tabs -->
							<div class="tab-rii">
								<ul class="nav nav-tabs custom_tab">
									<li class="nav-item tab1"> <a class="nav-link" data-toggle="tab" data-tab="1" href="#today">Open </a> </li>
									<li class="nav-item tab2"> <a class="nav-link " data-toggle="tab" data-tab="2" href="#menu2">In Progress  </a> </li>
									<li class="nav-item tab3"> <a class="nav-link" data-toggle="tab" data-tab="3" href="#menu3">Closed </a> </li>
									<li class="nav-item tab4"> <a class="nav-link" data-toggle="tab" data-tab="4" href="#menu4">All </a> </li>
								</ul>
								
							</div>
							<div class="tab-content">
								<div class="tab-pane active" id="today">
									<div class="custom-table">
										<div class="new-table-mr">
											<div class="table">
												<div class="one_row1 hidden-sm-down only_shawo">
													<div class="cell1 tab_head_sheet">Service Title	</div>
													<div class="cell1 tab_head_sheet">Category </div>
													<div class="cell1 tab_head_sheet">Date </div>
													<div class="cell1 tab_head_sheet">Location </div>
													<div class="cell1 tab_head_sheet">Budget</div>
													<div class="cell1 tab_head_sheet">Proposal</div>
													<div class="cell1 tab_head_sheet">Action </div>
												</div>
												<!--row 1-->
											 @if(count(@$posts)>0)
												 @foreach($posts as $post)
												<div class="one_row1 small_screen31">
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Service Title	</span>
														<p class="add_ttrr"> 
														@if(strlen(@$post->job_name)>20)
                                                              {!! substr(@$post->job_name,0,20) . '...'!!}
                                                              @else
                                                            {!! @$post->job_name !!}
                                                            @endif	
													</p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Category</span>
														<p class="add_ttrr">{{ @$post->categoryName?@$post->categoryName->category_name:'' }}</p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
														<p class="add_ttrr">{{ date('d.m.Y',strtotime(@$post->job_start_date)) }}</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Location </span>
														<p class="add_ttrr">{{ @$post->address }}</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Budget</span>
														<p class="add_ttrr">
														@if(@$post->budget_range_from != null)
														₹{{ @$post->budget_range_from }}
														@else
														-- 
														
                                                        @endif
													</p>
													</div>
													<div class="cell1 tab_head_sheet_1">
														
												      <span class="W55_1">View Proposal</span>
														<p class="add_ttrr rm_colorr"><a href="{{ route('user.proposal.list',['id'=>@$post->id]) }}">View Proposal</a></p>
													</div>
													<div class="cell1 tab_head_sheet_1 "> <span class="W55_1">Action</span>
														<div class="add_ttrr actions-main text-center">
															<a href="javascript:void(0);" class="action-dots" id="action{{ @$post->id }}"><img src="{{ url('public/frontend/./images/action-dots.png') }}" alt=""></a>
															<div class="show-actions" id="show-action{{ @$post->id }}" style="display: none;"> <span class="angle"><img src="{{ url('public/frontend/./images/angle.png') }}" alt=""></span>
																<ul class="text-left">
																	{{--<li><a href="javascript:;">View Proposal</a></li>--}}
																	<li><a href="{{ route('user.post.job',['id'=>@$post->id]) }}">Edit</a></li>
																	 @if(@$post->rivirtual_assistance != 'Y')
																	<li><a href="{{ route('user.get.rivirtual.assist',['id'=>@$post->id]) }}" onclick="return confirm('Are you want to get rivirtual assistance?')">Get Rivirtual Asisstance</a></li>
																	@endif
																	<li><a href="{{route('job.details',@$post->slug)}}" target="_blank"> View Job</a></li>
																	<li><a href="{{ route('user.post.job.delete',['id'=>@$post->id]) }}" onclick="return confirm('Are you want to delete this job?')">Delete</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
												@endforeach
												@php
                                                 $req=request()->except(['page1', '_token']);
                                                    $req['tab']='1'
                                                   @endphp
												<!--row 1-->
												<!--row 1-->
												{{--<div class="one_row1 small_screen31">
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Service Title	</span>
														<p class="add_ttrr"> industry's standard dummy text ever ... </p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Category</span>
														<p class="add_ttrr">Housekeeping</p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
														<p class="add_ttrr">04.10.2021</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Location </span>
														<p class="add_ttrr">New Delhi	</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Budget</span>
														<p class="add_ttrr">₹190/hr.</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
														<div class="add_ttrr actions-main text-center">
															<a href="javascript:void(0);" class="action-dots" id="action12"><img src="./images/action-dots.png" alt=""></a>
															<div class="show-actions" id="show-action12" style="display: none;"> <span class="angle"><img src="./images/angle.png" alt=""></span>
																<ul class="text-left">
																	<li><a href="user-proposal-list.html">View Proposal</a></li>
																	<li><a href="#">Edit</a></li>
																	<li><a href="#">Delete</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
												<!--row 1-->
												<!--row 1-->
												<div class="one_row1 small_screen31">
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Service Title	</span>
														<p class="add_ttrr"> Dummy title heresed do ... </p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Category</span>
														<p class="add_ttrr">Electrician </p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
														<p class="add_ttrr">01.10.2021</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Location </span>
														<p class="add_ttrr">Hyderabad</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Budget</span>
														<p class="add_ttrr">₹96/hr. </p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
														<div class="add_ttrr actions-main text-center">
															<a href="javascript:void(0);" class="action-dots" id="action13"><img src="./images/action-dots.png" alt=""></a>
															<div class="show-actions" id="show-action13" style="display: none;"> <span class="angle"><img src="./images/angle.png" alt=""></span>
																<ul class="text-left">
																	<li><a href="user-proposal-list.html">View Proposal</a></li>
																	<li><a href="#">Edit</a></li>
																	<li><a href="#">Delete</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
												<!--row 1-->
												<div class="one_row1 small_screen31">
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Service Title	</span>
														<p class="add_ttrr"> Dummy title heresed do mod a type... </p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Category</span>
														<p class="add_ttrr">Carpenter</p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
														<p class="add_ttrr">25.09.2021</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Location </span>
														<p class="add_ttrr">Bangalore</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Budget</span>
														<p class="add_ttrr">₹150/hr. </p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
														<div class="add_ttrr actions-main text-center">
															<a href="javascript:void(0);" class="action-dots" id="action15"><img src="./images/action-dots.png" alt=""></a>
															<div class="show-actions" id="show-action15" style="display: none;"> <span class="angle"><img src="./images/angle.png" alt=""></span>
																<ul class="text-left">
																	<li><a href="user-proposal-list.html">View Proposal</a></li>
																	<li><a href="#">Edit</a></li>
																	<li><a href="#">Delete</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
												<div class="one_row1 small_screen31">
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Service Title	</span>
														<p class="add_ttrr"> Dummy title heresed do mod a type... </p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Category</span>
														<p class="add_ttrr">Carpenter</p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
														<p class="add_ttrr">10.10.2021</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Location </span>
														<p class="add_ttrr">Bangalore</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Budget</span>
														<p class="add_ttrr">₹150/hr. </p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
														<div class="add_ttrr actions-main text-center">
															<a href="javascript:void(0);" class="action-dots" id="action14"><img src="./images/action-dots.png" alt=""></a>
															<div class="show-actions" id="show-action14" style="display: none;"> <span class="angle"><img src="./images/angle.png" alt=""></span>
																<ul class="text-left">
																	<li><a href="user-proposal-list.html">View Proposal</a></li>
																	<li><a href="#">Edit</a></li>
																	<li><a href="#">Delete</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>--}}
											<nav aria-label="...">
											<ul class="pagination new-pagination">
											{{@$posts->appends($req,'page1')->links()}}
												{{--<li class="page-item"> <span class="page-link">Prev </span> </li>
												<li class="page-item active"><a class="page-link" href="#">1</a></li>
												<li class="page-item" aria-current="page"> <span class="page-link">
                                                2
                                                <span class="sr-only">(current)</span> </span>
												</li>
												<li class="page-item"><a class="page-link" href="#">3</a></li>
												<li class="page-item"><a class="page-link" href="#">4</a></li>
												<li class="page-item"> <a class="page-link act" href="#">Next </a> </li>--}}
											</ul>
										</nav>
									      	@else
                                          <div class="one_row1 small_screen31">
                                             <p>No data found</p>
                                             </div>

                                          @endif
											</div>
										</div>
										 
										

									</div>
								</div>

								<div class="tab-pane " id="menu2">
									<div class="custom-table">
										<div class="new-table-mr">
											<div class="table">
												<div class="one_row1 hidden-sm-down only_shawo">
													<div class="cell1 tab_head_sheet">Service Title	</div>
													<div class="cell1 tab_head_sheet">Category </div>
													<div class="cell1 tab_head_sheet">Date </div>
													<div class="cell1 tab_head_sheet">Location </div>
													<div class="cell1 tab_head_sheet">Budget</div>
													<div class="cell1 tab_head_sheet">Proposal</div>
													<div class="cell1 tab_head_sheet">Action </div>
												</div>
												<!--row 1-->
												@if(count(@$jobProgressData)>0)
												 @foreach(@$jobProgressData as $val)
												<div class="one_row1 small_screen31">
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Service Title	</span>
														<p class="add_ttrr">
														@if(strlen(@$val->job_name)>20)
                                                              {!! substr(@$val->job_name,0,20) . '...'!!}
                                                              @else
                                                            {!! @$val->job_name !!}
                                                            @endif
														 </p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Category</span>
														<p class="add_ttrr">{{ @$val->categoryName?@$val->categoryName->category_name:'' }}</p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
														<p class="add_ttrr">{{ date('d.m.Y',strtotime(@$val->job_start_date)) }}</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Location </span>
														<p class="add_ttrr">{{ @$val->address }}</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Budget</span>
														<p class="add_ttrr">
														@if(@$val->budget_range_from != null)
														₹{{ @$val->budget_range_from }}
														@else
														-- 
														
                                                        @endif
														</p>
													</div>
													<div class="cell1 tab_head_sheet_1">
														
												      <span class="W55_1">View Proposal</span>
														<p class="add_ttrr rm_colorr"><a href="{{ route('user.proposal.list',['id'=>@$val->id]) }}">View Proposal</a></p>
													</div>
													<div class="cell1 tab_head_sheet_1 "> <span class="W55_1">Action</span>
														<div class="add_ttrr actions-main text-center">
															<a href="javascript:void(0);" class="action-dots" id="action{{ @$val->id }}"><img src="{{ url('public/frontend/./images/action-dots.png') }}" alt=""></a>
															<div class="show-actions" id="show-action{{ @$val->id }}" style="display: none;"> <span class="angle"><img src="{{ url('public/frontend/./images/angle.png') }}" alt=""></span>
																<ul class="text-left">
																	{{--<li><a href="user-proposal-list.html">View Proposal</a></li>--}}
																	{{--<li><a href="{{ route('user.post.job',['id'=>@$val->id]) }}">Edit</a></li>--}}
																	<li><a href="{{route('job.details',@$val->slug)}}" target="_blank"> View Job</a></li>
																	<li><a href="{{ route('user.post.job.delete',['id'=>@$val->id]) }}" onclick="return confirm('Are you want to delete this job?')">Delete</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
												@endforeach
												@php
                                         $req=request()->except(['page2', '_token']);
                                         $req['tab']='2'
                                         @endphp
												<!--row 1-->
												<!--row 1-->
												{{--<div class="one_row1 small_screen31">
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Service Title	</span>
														<p class="add_ttrr"> industry's standard dummy text ever ... </p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Category</span>
														<p class="add_ttrr">Housekeeping</p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
														<p class="add_ttrr">04.10.2021</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Location </span>
														<p class="add_ttrr">New Delhi	</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Budget</span>
														<p class="add_ttrr">₹190/hr.</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
														<div class="add_ttrr actions-main text-center">
															<a href="javascript:void(0);" class="action-dots" id="action2"><img src="./images/action-dots.png" alt=""></a>
															<div class="show-actions" id="show-action2" style="display: none;"> <span class="angle"><img src="./images/angle.png" alt=""></span>
																<ul class="text-left">
																	<li><a href="user-proposal-list.html">View Proposal</a></li>
																	<li><a href="#">Edit</a></li>
																	<li><a href="#">Delete</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
												<!--row 1-->
												<!--row 1-->
												<div class="one_row1 small_screen31">
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Service Title	</span>
														<p class="add_ttrr"> Dummy title heresed do ... </p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Category</span>
														<p class="add_ttrr">Electrician </p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
														<p class="add_ttrr">01.10.2021</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Location </span>
														<p class="add_ttrr">Hyderabad</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Budget</span>
														<p class="add_ttrr">₹96/hr. </p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
														<div class="add_ttrr actions-main text-center">
															<a href="javascript:void(0);" class="action-dots" id="action3"><img src="./images/action-dots.png" alt=""></a>
															<div class="show-actions" id="show-action3" style="display: none;"> <span class="angle"><img src="./images/angle.png" alt=""></span>
																<ul class="text-left">
																	<li><a href="user-proposal-list.html">View Proposal</a></li>
																	<li><a href="#">Edit</a></li>
																	<li><a href="#">Delete</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
												<!--row 1-->
												<div class="one_row1 small_screen31">
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Service Title	</span>
														<p class="add_ttrr"> Dummy title heresed do mod a type... </p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Category</span>
														<p class="add_ttrr">Carpenter</p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
														<p class="add_ttrr">25.09.2021</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Location </span>
														<p class="add_ttrr">Bangalore</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Budget</span>
														<p class="add_ttrr">₹150/hr. </p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
														<div class="add_ttrr actions-main text-center">
															<a href="javascript:void(0);" class="action-dots" id="action5"><img src="./images/action-dots.png" alt=""></a>
															<div class="show-actions" id="show-action5" style="display: none;"> <span class="angle"><img src="./images/angle.png" alt=""></span>
																<ul class="text-left">
																	<li><a href="user-proposal-list.html">View Proposal</a></li>
																	<li><a href="#">Edit</a></li>
																	<li><a href="#">Delete</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
												<div class="one_row1 small_screen31">
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Service Title	</span>
														<p class="add_ttrr"> Dummy title heresed do mod a type... </p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Category</span>
														<p class="add_ttrr">Carpenter</p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
														<p class="add_ttrr">10.10.2021</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Location </span>
														<p class="add_ttrr">Bangalore</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Budget</span>
														<p class="add_ttrr">₹150/hr. </p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
														<div class="add_ttrr actions-main text-center">
															<a href="javascript:void(0);" class="action-dots" id="action4"><img src="./images/action-dots.png" alt=""></a>
															<div class="show-actions" id="show-action4" style="display: none;"> <span class="angle"><img src="./images/angle.png" alt=""></span>
																<ul class="text-left">
																	<li><a href="user-proposal-list.html">View Proposal</a></li>
																	<li><a href="#">Edit</a></li>
																	<li><a href="#">Delete</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>--}}
												<nav aria-label="...">
											<ul class="pagination new-pagination">
											{{@$jobProgressData->appends($req,'page2')->links()}}
												{{--<li class="page-item"> <span class="page-link">Prev </span> </li>
												<li class="page-item active"><a class="page-link" href="#">1</a></li>
												<li class="page-item" aria-current="page"> <span class="page-link">
                                                2
                                                <span class="sr-only">(current)</span> </span>
												</li>
												<li class="page-item"><a class="page-link" href="#">3</a></li>
												<li class="page-item"><a class="page-link" href="#">4</a></li>
												<li class="page-item"> <a class="page-link act" href="#">Next </a> </li>--}}
											</ul>
										</nav>
										@else
                                          <div class="one_row1 small_screen31">
                                             <p>No data found</p>
                                             </div>

                                          @endif
											</div>
										</div>
										
										
									</div>
								</div>

								<div class="tab-pane " id="menu3">
									<div class="custom-table">
										<div class="new-table-mr">
											<div class="table">
												<div class="one_row1 hidden-sm-down only_shawo">
													<div class="cell1 tab_head_sheet">Service Title	</div>
													<div class="cell1 tab_head_sheet">Category </div>
													<div class="cell1 tab_head_sheet">Date </div>
													<div class="cell1 tab_head_sheet">Location </div>
													<div class="cell1 tab_head_sheet">Budget</div>
													<div class="cell1 tab_head_sheet">Proposal</div>
													<div class="cell1 tab_head_sheet">Action </div>
												</div>
												<!--row 1-->
												@if(count(@$jobClosedData)>0)
												 @foreach(@$jobClosedData as $val2)
												<div class="one_row1 small_screen31">
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Service Title	</span>
														<p class="add_ttrr">
														@if(strlen(@$val2->job_name)>20)
                                                              {!! substr(@$val2->job_name,0,20) . '...'!!}
                                                              @else
                                                            {!! @$val2->job_name !!}
                                                            @endif
														 </p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Category</span>
														<p class="add_ttrr">{{ @$val2->categoryName?@$val2->categoryName->category_name:'' }}</p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
														<p class="add_ttrr">{{ date('d.m.Y',strtotime(@$val2->job_start_date)) }}</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Location </span>
														<p class="add_ttrr">{{ @$val2->address }}</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Budget</span>
														<p class="add_ttrr">
														@if(@$val2->budget_range_from != null)
														₹{{ @$val2->budget_range_from }}
														@else
														-- 
														
                                                        @endif
														</p>
													</div>
													<div class="cell1 tab_head_sheet_1">
														
												      <span class="W55_1">View Proposal</span>
														<p class="add_ttrr rm_colorr"><a href="{{ route('user.proposal.list',['id'=>@$val2->id]) }}">View Proposal</a></p>
													</div>
													<div class="cell1 tab_head_sheet_1 "> <span class="W55_1">Action</span>
														<div class="add_ttrr actions-main text-center">
															<a href="javascript:void(0);" class="action-dots" id="action{{ @$val2->id }}"><img src="{{ url('public/frontend/./images/action-dots.png') }}" alt=""></a>
															<div class="show-actions" id="show-action{{ @$val2->id }}" style="display: none;"> <span class="angle"><img src="{{ url('public/frontend/./images/angle.png') }}" alt=""></span>
																<ul class="text-left">
																	{{--<li><a href="user-proposal-list.html">View Proposal</a></li>--}}
																	{{--<li><a href="{{ route('user.post.job',['id'=>@$val2->id]) }}">Edit</a></li>--}}
																	<li><a href="{{route('job.details',@$val2->slug)}}" target="_blank"> View Job</a></li>
																	<li><a href="javascript:;" class="modal-open" data-toggle="modal" data-target="#exampleModal" data-jobid="{{ @$val2->id }}" 
																	 data-proid="{{ @$val2->proDetails->user_id }}" data-jobtitle="{{@$val2->job_name }}" data-desc="{{ @$val2->reviewDetails?@$val2->reviewDetails->review_text:'' }}" data-reviewpoint="{{ @$val2->reviewDetails?@$val2->reviewDetails->review_point:'' }}">
									                                  Rate job
								                                    </a></li>
																	<li><a href="{{ route('user.post.job.delete',['id'=>@$val2->id]) }}" onclick="return confirm('Are you want to delete this job?')">Delete</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
												@endforeach
												@php
                                         $req=request()->except(['page3', '_token']);
                                         $req['tab']='3'
                                         @endphp
												<!--row 1-->
												<!--row 1-->
												{{--<div class="one_row1 small_screen31">
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Service Title	</span>
														<p class="add_ttrr"> industry's standard dummy text ever ... </p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Category</span>
														<p class="add_ttrr">Housekeeping</p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
														<p class="add_ttrr">04.10.2021</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Location </span>
														<p class="add_ttrr">New Delhi	</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Budget</span>
														<p class="add_ttrr">₹190/hr.</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
														<div class="add_ttrr actions-main text-center">
															<a href="javascript:void(0);" class="action-dots" id="action7"><img src="./images/action-dots.png" alt=""></a>
															<div class="show-actions" id="show-action7" style="display: none;"> <span class="angle"><img src="./images/angle.png" alt=""></span>
																<ul class="text-left">
																	<li><a href="user-proposal-list.html">View Proposal</a></li>
																	<li><a href="#">Edit</a></li>
																	<li><a href="#">Delete</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
												<!--row 1-->
												<!--row 1-->
												<div class="one_row1 small_screen31">
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Service Title	</span>
														<p class="add_ttrr"> Dummy title heresed do ... </p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Category</span>
														<p class="add_ttrr">Electrician </p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
														<p class="add_ttrr">01.10.2021</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Location </span>
														<p class="add_ttrr">Hyderabad</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Budget</span>
														<p class="add_ttrr">₹96/hr. </p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
														<div class="add_ttrr actions-main text-center">
															<a href="javascript:void(0);" class="action-dots" id="action8"><img src="./images/action-dots.png" alt=""></a>
															<div class="show-actions" id="show-action8" style="display: none;"> <span class="angle"><img src="./images/angle.png" alt=""></span>
																<ul class="text-left">
																	<li><a href="user-proposal-list.html">View Proposal</a></li>
																	<li><a href="#">Edit</a></li>
																	<li><a href="#">Delete</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
												<!--row 1-->
												<div class="one_row1 small_screen31">
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Service Title	</span>
														<p class="add_ttrr"> Dummy title heresed do mod a type... </p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Category</span>
														<p class="add_ttrr">Carpenter</p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
														<p class="add_ttrr">25.09.2021</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Location </span>
														<p class="add_ttrr">Bangalore</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Budget</span>
														<p class="add_ttrr">₹150/hr. </p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
														<div class="add_ttrr actions-main text-center">
															<a href="javascript:void(0);" class="action-dots" id="action9"><img src="./images/action-dots.png" alt=""></a>
															<div class="show-actions" id="show-action9" style="display: none;"> <span class="angle"><img src="./images/angle.png" alt=""></span>
																<ul class="text-left">
																	<li><a href="user-proposal-list.html">View Proposal</a></li>
																	<li><a href="#">Edit</a></li>
																	<li><a href="#">Delete</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
												<div class="one_row1 small_screen31">
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Service Title	</span>
														<p class="add_ttrr"> Dummy title heresed do mod a type... </p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Category</span>
														<p class="add_ttrr">Carpenter</p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
														<p class="add_ttrr">10.10.2021</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Location </span>
														<p class="add_ttrr">Bangalore</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Budget</span>
														<p class="add_ttrr">₹150/hr. </p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
														<div class="add_ttrr actions-main text-center">
															<a href="javascript:void(0);" class="action-dots" id="action10"><img src="./images/action-dots.png" alt=""></a>
															<div class="show-actions" id="show-action10" style="display: none;"> <span class="angle"><img src="./images/angle.png" alt=""></span>
																<ul class="text-left">
																	<li><a href="user-proposal-list.html">View Proposal</a></li>
																	<li><a href="#">Edit</a></li>
																	<li><a href="#">Delete</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>--}}
												<nav aria-label="...">
											<ul class="pagination new-pagination">
											{{@$jobClosedData->appends($req,'page3')->links()}}
												{{--<li class="page-item"> <span class="page-link">Prev </span> </li>
												<li class="page-item active"><a class="page-link" href="#">1</a></li>
												<li class="page-item" aria-current="page"> <span class="page-link">
                                                2
                                                <span class="sr-only">(current)</span> </span>
												</li>
												<li class="page-item"><a class="page-link" href="#">3</a></li>
												<li class="page-item"><a class="page-link" href="#">4</a></li>
												<li class="page-item"> <a class="page-link act" href="#">Next </a> </li>--}}
											</ul>
										</nav>
										@else
                                          <div class="one_row1 small_screen31">
                                             <p>No data found</p>
                                             </div>

                                          @endif
											</div>
										</div>
									
										
									</div>
								</div>

								<div class="tab-pane " id="menu4">
									<div class="custom-table">
										<div class="new-table-mr">
											<div class="table">
												<div class="one_row1 hidden-sm-down only_shawo">
													<div class="cell1 tab_head_sheet">Service Title	</div>
													<div class="cell1 tab_head_sheet">Category </div>
													<div class="cell1 tab_head_sheet">Date </div>
													<div class="cell1 tab_head_sheet">Location </div>
													<div class="cell1 tab_head_sheet">Budget</div>
													<div class="cell1 tab_head_sheet">Proposal</div>
													<div class="cell1 tab_head_sheet">Action </div>
												</div>

												<!--row 1-->
												@if(count(@$allJobData)>0)
												 @foreach(@$allJobData as $val1)
												<div class="one_row1 small_screen31">
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Service Title	</span>
														<p class="add_ttrr">
														@if(strlen(@$val1->job_name)>20)
                                                              {!! substr(@$val1->job_name,0,20) . '...'!!}
                                                              @else
                                                            {!! @$val1->job_name !!}
                                                            @endif
														 </p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Category</span>
														<p class="add_ttrr">{{ @$val1->categoryName?@$val1->categoryName->category_name:'' }}</p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
														<p class="add_ttrr">{{ date('d.m.Y',strtotime(@$val1->job_start_date)) }}</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Location </span>
														<p class="add_ttrr">{{ @$val1->address }}</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Budget</span>
														<p class="add_ttrr">
														@if(@$val1->budget_range_from != null)
														₹{{ @$val1->budget_range_from }}
														@else
														-- 
														
                                                        @endif
														</p>
													</div>
													<div class="cell1 tab_head_sheet_1">
														
												      <span class="W55_1">View Proposal</span>
														<p class="add_ttrr rm_colorr"><a href="{{ route('user.proposal.list',['id'=>@$val1->id]) }}">View Proposal</a></p>
													</div>
													<div class="cell1 tab_head_sheet_1 "> <span class="W55_1">Action</span>
														<div class="add_ttrr actions-main text-center">
															<a href="javascript:void(0);" class="action-dots" id="action11{{ @$val1->id }}"><img src="{{ url('public/frontend/./images/action-dots.png') }}" alt=""></a>
															<div class="show-actions" id="show-action11{{ @$val1->id }}" style="display: none;"> <span class="angle"><img src="{{ url('public/frontend/./images/angle.png') }}" alt=""></span>
																<ul class="text-left">
																	{{--<li><a href="user-proposal-list.html">View Proposal</a></li>--}}
																	 @if(@$val1->job_status != 'IP')
																	<li><a href="{{ route('user.post.job',['id'=>@$val1->id]) }}">Edit</a></li>
																	@endif
																	<li><a href="{{route('job.details',@$val1->slug)}}" target="_blank"> View Job</a></li>
																	<li><a href="{{ route('user.post.job.delete',['id'=>@$val1->id]) }}" onclick="return confirm('Are you want to delete this job?')">Delete</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
												@endforeach
												@php
                                               $req=request()->except(['page4', '_token']);
                                                $req['tab']='4'
                                               @endphp
												<!--row 1-->
												<!--row 1-->
												{{--<div class="one_row1 small_screen31">
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Service Title	</span>
														<p class="add_ttrr"> industry's standard dummy text ever ... </p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Category</span>
														<p class="add_ttrr">Housekeeping</p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
														<p class="add_ttrr">04.10.2021</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Location </span>
														<p class="add_ttrr">New Delhi	</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Budget</span>
														<p class="add_ttrr">₹190/hr.</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
														<div class="add_ttrr actions-main text-center">
															<a href="javascript:void(0);" class="action-dots" id="action27"><img src="./images/action-dots.png" alt=""></a>
															<div class="show-actions" id="show-action27" style="display: none;"> <span class="angle"><img src="./images/angle.png" alt=""></span>
																<ul class="text-left">
																	<li><a href="user-proposal-list.html">View Proposal</a></li>
																	<li><a href="#">Edit</a></li>
																	<li><a href="#">Delete</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
												<!--row 1-->
												<!--row 1-->
												<div class="one_row1 small_screen31">
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Service Title	</span>
														<p class="add_ttrr"> Dummy title heresed do ... </p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Category</span>
														<p class="add_ttrr">Electrician </p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
														<p class="add_ttrr">01.10.2021</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Location </span>
														<p class="add_ttrr">Hyderabad</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Budget</span>
														<p class="add_ttrr">₹96/hr. </p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
														<div class="add_ttrr actions-main text-center">
															<a href="javascript:void(0);" class="action-dots" id="action28"><img src="./images/action-dots.png" alt=""></a>
															<div class="show-actions" id="show-action28" style="display: none;"> <span class="angle"><img src="./images/angle.png" alt=""></span>
																<ul class="text-left">
																	<li><a href="user-proposal-list.html">View Proposal</a></li>
																	<li><a href="#">Edit</a></li>
																	<li><a href="#">Delete</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
												<!--row 1-->
												<div class="one_row1 small_screen31">
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Service Title	</span>
														<p class="add_ttrr"> Dummy title heresed do mod a type... </p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Category</span>
														<p class="add_ttrr">Carpenter</p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
														<p class="add_ttrr">25.09.2021</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Location </span>
														<p class="add_ttrr">Bangalore</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Budget</span>
														<p class="add_ttrr">₹150/hr. </p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
														<div class="add_ttrr actions-main text-center">
															<a href="javascript:void(0);" class="action-dots" id="action29"><img src="./images/action-dots.png" alt=""></a>
															<div class="show-actions" id="show-action29" style="display: none;"> <span class="angle"><img src="./images/angle.png" alt=""></span>
																<ul class="text-left">
																	<li><a href="user-proposal-list.html">View Proposal</a></li>
																	<li><a href="#">Edit</a></li>
																	<li><a href="#">Delete</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
												<div class="one_row1 small_screen31">
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Service Title	</span>
														<p class="add_ttrr"> Dummy title heresed do mod a type... </p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Category</span>
														<p class="add_ttrr">Carpenter</p>
													</div>
													<div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
														<p class="add_ttrr">10.10.2021</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Location </span>
														<p class="add_ttrr">Bangalore</p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Budget</span>
														<p class="add_ttrr">₹150/hr. </p>
													</div>
													<div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
														<div class="add_ttrr actions-main text-center">
															<a href="javascript:void(0);" class="action-dots" id="action30"><img src="./images/action-dots.png" alt=""></a>
															<div class="show-actions" id="show-action30" style="display: none;"> <span class="angle"><img src="./images/angle.png" alt=""></span>
																<ul class="text-left">
																	<li><a href="user-proposal-list.html">View Proposal</a></li>
																	<li><a href="#">Edit</a></li>
																	<li><a href="#">Delete</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>--}}
												<nav aria-label="...">
											<ul class="pagination new-pagination">
											{{@$allJobData->appends($req,'page4')->links()}}
												{{--<li class="page-item"> <span class="page-link">Prev </span> </li>
												<li class="page-item active"><a class="page-link" href="#">1</a></li>
												<li class="page-item" aria-current="page"> <span class="page-link">
                                                2
                                                <span class="sr-only">(current)</span> </span>
												</li>
												<li class="page-item"><a class="page-link" href="#">3</a></li>
												<li class="page-item"><a class="page-link" href="#">4</a></li>
												<li class="page-item"> <a class="page-link act" href="#">Next </a> </li>--}}
											</ul>
										</nav>
										@else
                                          <div class="one_row1 small_screen31">
                                             <p>No data found</p>
                                             </div>

                                          @endif   
											</div>
										</div>
										
									       
										
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade modal_review_post" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		<form action="{{ route('job.rating.save') }}" method="post" id="ReviewForm">
		@csrf	
         <input type="hidden" name="job_id" id="job_id" value="">
		 <input type="hidden" name="pro_id" id="pro_id" value="">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Rate <span id="job_title"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
	  <div class="modal-body">
       	<div class="body_review_post">
       		<div class="form_box_area newmar">  

                <label>Rating</label>   

                <div>
				<div id="half-stars-example">
                        <div class="rating-group">
				           <div class="rating-group"></div>
                            <input class="rating__input rating__input--none" checked name="property_rating" id="rating2-0" value="0" type="radio">
                            <label aria-label="0 stars" class="rating__label none" for="rating2-0">&nbsp;</label>

                            <label aria-label="1 star" class="rating__label" for="rating2-10"><i class="rating__icon rating__icon--star fa fa-star"></i></label>

                            <input class="rating__input" name="property_rating" id="rating2-10" value="1" type="radio">

                            <label aria-label="2 stars" class="rating__label" for="rating2-20"><i class="rating__icon rating__icon--star fa fa-star"></i></label>

                            <input class="rating__input" name="property_rating" id="rating2-20" value="2" type="radio">

                            <label aria-label="3 stars" class="rating__label" for="rating2-30"><i class="rating__icon rating__icon--star fa fa-star"></i></label>

                            <input class="rating__input" name="property_rating" id="rating2-30" value="3" type="radio">

                            <label aria-label="4 stars" class="rating__label" for="rating2-40"><i class="rating__icon rating__icon--star fa fa-star"></i></label>

                            <input class="rating__input" name="property_rating" id="rating2-40" value="4" type="radio">

                            <label aria-label="5 stars" class="rating__label" for="rating2-50"><i class="rating__icon rating__icon--star fa fa-star"></i></label>

                            <input class="rating__input" name="property_rating" id="rating2-50" value="5" type="radio">
                          
                        </div>

                </div> 

                </div>   

            </div>

            </div>
       		<div class="das_input">
				<label>Description </label>
				<textarea placeholder="Enter here.." name="description" id="description"></textarea> 
			</div>
			<div class="revie_btn_modal">
				
                
				<input type="submit" value="Post" class="see_cc">
				<button type="button" class="see_cc cancel_foot" data-dismiss="modal">Cancel</button>
                 
				
				
				
			</div>
       	</div>
      </div>
      
	</form>
    </div>
	
  </div>
</div>
    @endsection

 @section('footer')
@include('includes.footer')
@endsection


@section('script')
@include('includes.script')
<script>
		$(function() {
			$("#datepicker").datepicker();
		});
		$(function() {
			$("#datepicker2").datepicker();
		});
		$(function() {
			$("#datepicker3").datepicker();
		});
		</script>

<script>
         $(document).on('click', function () {
		@foreach(@$posts as $value)
		var $target = $(event.target);
		if (!$target.closest('#action{{@$value->id}}').length && $('#show-action{{@$value->id}}').is(":visible")) {
			$('#show-action{{@$value->id}}').slideUp();
		}
		@endforeach
	});

	$(document).ready(function() {
		@foreach(@$posts as $value)
			$("#action{{@$value->id}}").click(function() {
				$("#show-action{{@$value->id}}").slideToggle();
			});
		@endforeach
    });
    </script>	
	
	<script>
         $(document).on('click', function () {
		@foreach(@$jobProgressData as $value)
		var $target = $(event.target);
		if (!$target.closest('#action{{@$value->id}}').length && $('#show-action{{@$value->id}}').is(":visible")) {
			$('#show-action{{@$value->id}}').slideUp();
		}
		@endforeach
	});

	$(document).ready(function() {
		@foreach(@$jobProgressData as $value)
			$("#action{{@$value->id}}").click(function() {
				$("#show-action{{@$value->id}}").slideToggle();
			});
		@endforeach
    });
    </script>
	
	<script>
         $(document).on('click', function () {
		@foreach(@$allJobData as $value)
		var $target = $(event.target);
		if (!$target.closest('#action11{{@$value->id}}').length && $('#show-action11{{@$value->id}}').is(":visible")) {
			$('#show-action11{{@$value->id}}').slideUp();
		}
		@endforeach
	});

	$(document).ready(function() {
		@foreach(@$allJobData as $value)
			$("#action11{{@$value->id}}").click(function() {
				$("#show-action11{{@$value->id}}").slideToggle();
			});
		@endforeach
    });
    </script>

		
<script>
         $(document).on('click', function () {
		@foreach(@$jobClosedData as $value)
		var $target = $(event.target);
		if (!$target.closest('#action{{@$value->id}}').length && $('#show-action{{@$value->id}}').is(":visible")) {
			$('#show-action{{@$value->id}}').slideUp();
		}
		@endforeach
	});

	$(document).ready(function() {
		@foreach(@$jobClosedData as $value)
			$("#action{{@$value->id}}").click(function() {
				$("#show-action{{@$value->id}}").slideToggle();
			});
		@endforeach
    });
    </script>
	<script>
		$(document).ready(function(){
			    
			   $('.modal-open').on('click',function(e){
				     
				     var jobId = $(e.currentTarget).attr('data-jobid');
					 var proId = $(e.currentTarget).attr('data-proid');
					 var jobTitle = $(e.currentTarget).attr('data-jobtitle');
                     var review_text = $(e.currentTarget).attr('data-desc');
					 var review_point = $(e.currentTarget).attr('data-reviewpoint');
					 $('#job_id').val(jobId);
					 $('#pro_id').val(proId);
					 $('#job_title').html(jobTitle);
                     $('#description').html(review_text);
                    
                     if(review_point != null){
                            
							 for(var i=1; i<=review_point; i++){

							    $('#rating2-'+i+'0').attr('checked',true);
						        }
					 }
					 

						
					 
			   })
		})
	</script>
	<script>
		$(document).ready(function(){

			 $('#ReviewForm').validate({

				  rules: {

					    property_rating: {

							 required: true,
						},
						description: {

							 required: true,
						},
				  }
			 })
		})
	</script>
	<script>
    $(document).ready(function () {
        var tab_value = $('#tab').val()
        if(tab_value == '')
        {
            $('.tab1').addClass('active');
            $('#today').addClass('active');
            $('#today').addClass('show');
        }
        else
        {
            $('.tab'+tab_value).addClass('active');
            if(tab_value == 1){
                $('#today').addClass('active');
                $('#today').addClass('show');
            }else{
                $('#menu'+tab_value).addClass('active');
                $('#menu'+tab_value).addClass('show');
            }
        }
        $('.nav-link').click(function(){
            $('#tab').val($(this).data('tab'))
            console.log($('#tab').val());
        });
    });
</script>
@endsection
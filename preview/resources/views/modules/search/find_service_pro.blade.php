@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> RiVirtual | Find Pro </title>
@endsection




@section('header')
@include('includes.header')
@endsection

@section('content')

<div class="haeder-padding"></div>
<!-----filter--------->
<div class="search-wrapper">
	<section class="search-pro">
		<div class="container">
			<div class="row">
				<div class="col-lg-3">

					<div class="mobile-list">
						<p><i class="fa fa-filter"></i> Show Filter</p>
					</div>
                  <form action="{{ route('search.service.pro') }}" method="post" id="searchFilter">
					  @csrf
					<div class="search-filter new-filters">
						<div class="search-section2">
							<div class="list-category-sec">
		                        <h3>Categories</h3>
		                        <div class="diiferent-sec">
		                           <select class="dropdown-list" name="category" id="category">
		                              <option value="">Select Category</option>
		                               @if(@$category)
									    @foreach($category as $cat)
		                              <option value="{{ $cat->id }}" @if(@$request['category'] == $cat->id) selected @elseif(@$catId->category_id == $cat->id) selected @endif>{{ $cat->category_name }}</option>
									  @endforeach
									  @endif
		                           </select>
		                        </div>
		                     </div>
							 <div class="list-category-sec">
		                        <h3>Sub Categories</h3>
		                        <div class="diiferent-sec">
		                           <select class="dropdown-list" name="sub_category" id="sub_category">
		                              <option value="">Select Sub Category</option>
		                               @if(@$subcategory)
									    @foreach($subcategory as $cat)
		                              <option value="{{ $cat->id }}" @if(@$request['sub_category'] == $cat->id) selected @elseif(@$subcatId->id == $cat->id) selected @endif>{{ $cat->name }}</option>
									  @endforeach
									  @endif
		                           </select>
		                        </div>
		                     </div>

		                     <div class="list-category-sec">
		                        <h3>State</h3>
		                        <div class="diiferent-sec">
								<select class="dropdown-list" name="state" id="states">
		                              <option value="">Select State</option>
		                               @if(@$states)
									    @foreach(@$states as $state)
		                              <option value="{{ $state->id }}" @if(@$request['state'] == $state->id) selected @endif>{{ @$state->name }}</option>
									  @endforeach
									  @endif
		                           </select>
		                        </div>
		                     </div>
							 <div class="list-category-sec">
		                        <h3>City</h3>
		                        <div class="diiferent-sec">
								<select class="dropdown-list" name="city" id="city">
		                              <option value="">Select City</option>
		                               @if(@$cites)
									    @foreach(@$cites as $city)
		                              <option value="{{ $city->id }}" @if(@$request['city'] == $city->id) selected @endif>{{ @$city->name }}</option>
									  @endforeach
									  @endif
		                           </select>
		                        </div>
		                     </div>

		                     <div class="list-category-sec">
		                        <h3>Experience level</h3>
		                        <div class="diiferent-sec">
		                           <ul class="prolists-ul">
		                              <li>
		                                 <label class="list_checkBox">0 year to 2 years
		                                 <input type="checkbox" name="year[]" class="checkboxname" value="2"
										 @if(!empty(@$request['year']))
										 @foreach(@$request['year'] as $rate) 
                                                @if($rate == 2) 
                                                checked="checked" 
                                                @endif 
                                                @endforeach
                                                @endif>
		                                 <span class="list_checkmark ratingcheckpart"></span>
		                                 </label>
		                              </li>
		                              <li>
		                                 <label class="list_checkBox">3 years to 5 years
		                                 <input type="checkbox" name="year[]" class="checkboxname" value="5"
										 @if(!empty(@$request['year']))
										 @foreach(@$request['year'] as $rate) 
                                                @if($rate == 5) 
                                                checked="checked" 
                                                @endif 
                                                @endforeach
                                                @endif>
		                                 <span class="list_checkmark ratingcheckpart"></span>
		                                 </label>
		                              </li>
		                              <li>
		                                 <label class="list_checkBox">6 years to 10 years
		                                 <input type="checkbox" name="year[]" class="checkboxname" value="10"
										 @if(!empty(@$request['year']))
										 @foreach(@$request['year'] as $rate) 
                                                @if($rate == 10) 
                                                checked="checked" 
                                                @endif 
                                                @endforeach
                                                @endif>
		                                 <span class="list_checkmark ratingcheckpart"></span>
		                                 </label>
		                              </li>
		                              <li>
		                                 <label class="list_checkBox">11 years and Above
		                                 <input type="checkbox" name="year[]" class="checkboxname" value="11"
										 @if(!empty(@$request['year']))
										 @foreach(@$request['year'] as $rate) 
                                                @if($rate == 11) 
                                                checked="checked" 
                                                @endif 
                                                @endforeach
                                                @endif>
		                                 <span class="list_checkmark ratingcheckpart"></span>
		                                 </label>
		                              </li>
		                              <li>
		                                 <label class="list_checkBox">All
		                                 <input type="checkbox" name="" id="checkAll">
		                                 <span class="list_checkmark"></span>
		                                 </label>
		                              </li>
		                           </ul>
		                        </div>
		                     </div>

		                     <div class="list-category-sec">
		                        <h3>Ratings</h3>
		                        <div class="diiferent-sec">
		                           <ul class="prolists-ul">
		                              <li>
		                                 <label class="list_checkBox">
		                                 <ul class="starRate_chec">
	                                       <li><i class="icofont-star"></i></li>
	                                       <li><i class="icofont-star"></i></li>
	                                       <li><i class="icofont-star"></i></li>
	                                       <li><i class="icofont-star"></i></li>
	                                       <li class="grey-star"><i class="icofont-star"></i></li>
	                                       <li>4 and Above</li>
	                                    </ul> 
										<input type="checkbox"  name="rating[]" value="4" @if(!empty(@$request['rating']))
                                                @foreach(@$request['rating'] as $rate) 
                                                @if($rate == 4) 
                                                checked="checked" 
                                                @endif 
                                                @endforeach
                                                @endif>
		                                 <span class="list_checkmark ratingcheckpart"></span>
		                                 </label>
		                              </li>

		                              <li>
		                                 <label class="list_checkBox">
		                                 <ul class="starRate_chec">
	                                       <li><i class="icofont-star"></i></li>
	                                       <li><i class="icofont-star"></i></li>
	                                       <li><i class="icofont-star"></i></li>
	                                       <li class="grey-star"><i class="icofont-star"></i></li>
	                                       <li class="grey-star"><i class="icofont-star"></i></li>
	                                       <li>3 and Above</li>
	                                    </ul>  
										<input type="checkbox"  name="rating[]" value="3" @if(!empty(@$request['rating']))
                                                @foreach(@$request['rating'] as $rate) 
                                                @if($rate == 3) 
                                                checked="checked" 
                                                @endif 
                                                @endforeach
                                                @endif>
		                                 <span class="list_checkmark ratingcheckpart"></span>
		                                 </label>
		                              </li>
		                              <li>
		                                 <label class="list_checkBox">
		                                 <ul class="starRate_chec">
	                                       <li><i class="icofont-star"></i></li>
	                                       <li><i class="icofont-star"></i></li>
	                                       <li class="grey-star"><i class="icofont-star"></i></li>
	                                       <li class="grey-star"><i class="icofont-star"></i></li>
	                                       <li class="grey-star"><i class="icofont-star"></i></li>
	                                       <li>2 and Above</li>
	                                    </ul>  
										<input type="checkbox"  name="rating[]" value="2" @if(!empty(@$request['rating']))
                                                @foreach(@$request['rating'] as $rate) 
                                                @if($rate == 2) 
                                                checked="checked" 
                                                @endif 
                                                @endforeach
                                                @endif>
		                                 <span class="list_checkmark ratingcheckpart"></span>
		                                 </label>
		                              </li>
		                              <li>
		                                 <label class="list_checkBox">
		                                 <ul class="starRate_chec">
	                                       <li><i class="icofont-star"></i></li>
	                                       <li class="grey-star"><i class="icofont-star"></i></li>
	                                       <li class="grey-star"><i class="icofont-star"></i></li>
	                                       <li class="grey-star"><i class="icofont-star"></i></li>
	                                       <li class="grey-star"><i class="icofont-star"></i></li>
	                                       <li>1 and Above</li>
	                                    </ul> 
										<input type="checkbox"  name="rating[]" value="1" @if(!empty(@$request['rating']))
                                                @foreach(@$request['rating'] as $rate) 
                                                @if($rate == 1) 
                                                checked="checked" 
                                                @endif 
                                                @endforeach
                                                @endif>
		                                 <span class="list_checkmark ratingcheckpart"></span>
		                                 </label>
		                              </li>
		                              
		                           </ul>
		                        </div>
		                     </div>

		                     <div class="list-category-sec">
		                        <h3>Budget</h3>
								
		                        <div class="diiferent-sec">
		                          <div class="slider_rnge">
									                  <div id="slider-range" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
									                    <div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 0%; width: 100%;"></div>
														 <span tabindex="0" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 3.32667%;"></span>
														  <span tabindex="0" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 66.6667%;"></span>
														   <div class="ui-slider-range ui-corner-all ui-widget-header" style="left: 3.32667%; width: 63.34%;"></div></div> 
														   <span class="range-text">
									                           
														<input type="text" class="price_numb" readonly="" id="amount">
														<input type="hidden" class="price_numb" id="amount1" name="amount1" value="{{@$request['amount1']?@$request['amount1']:0}}">
                                                        <input type="hidden" class="price_numb" id="amount2" name="amount2" value="{{@request['amount2']?@$request['amount2']:(int)@$maxPrice->budget}}">
									                           </span> 
									                </div>
		                        </div>
		                     </div>
							 <input type="hidden" class="price_numb" id="sort_by_value" name="sort_by_value" value="{{@$request['sort_by_value']?@$request['sort_by_value']:0}}">
		                     <div class="fil-can">
		                     	<button type="submit" class="filt-btn">Filter</button>
		                     	<button type="button" class="clear-btn" onclick="window.location.href='{{ route('search.service.pro') }}'">Clear</button>
		                     </div>

							
							


							
						</div>
							<div class="clearfix"></div>
					</div>
						<div class="clearfix"></div>
                 </form>
				</div>
				

				<div class="col-lg-9">
					<div class="product-list-view">
						<div class="top-total">
							<div class="page-pro">
	                        <h5>{{ !empty(@$total_pro) ? @$total_pro : 0 }} Results found - PRO</h5>
	                        <!--<p>some dummy caption how here</p>-->
	                        </div>
	                        <div class="sort-filter">
							
	                           <p>Sort by : </p>
	                           <select class="sort-select" id="short_by" name="short_by">
	                              
	                              <option value="RH" 
                                        @if(!empty(@$request['sort_by_value'])) 
                                        @if(@$request['sort_by_value'] == 'RH') 
                                        selected="" 
                                        @endif 
                                        @elseif(!empty(@request()->sort_by_value))
                                        @if(@request()->sort_by_value == 'RH') 
                                        selected="" 
                                        @endif 
                                        @endif>Review (High to Low) </option>
	                              <option value="RL"
								       @if(!empty(@$request['sort_by_value'])) 
                                        @if(@$request['sort_by_value'] == 'RL') 
                                        selected="" 
                                        @endif 
                                        @elseif(!empty(@request()->sort_by_value))
                                        @if(@request()->sort_by_value == 'RL') 
                                        selected="" 
                                        @endif 
                                        @endif >Review (Low to High)</option>
	                              <option value="H"
								      @if(!empty(@$request['sort_by_value'])) 
                                        @if(@$request['sort_by_value'] == 'H') 
                                        selected="" 
                                        @endif 
                                        @elseif(!empty(@request()->sort_by_value))
                                        @if(@request()->sort_by_value == 'H') 
                                        selected="" 
                                        @endif 
                                        @endif>Price (High to Low)</option>
	                              <option value="L"
								       @if(!empty(@$request['sort_by_value'])) 
                                        @if(@$request['sort_by_value'] == 'L') 
                                        selected="" 
                                        @endif 
                                        @elseif(!empty(@request()->sort_by_value))
                                        @if(@request()->sort_by_value == 'L') 
                                        selected="" 
                                        @endif 
                                        @endif>Price (Low to High) </option>
	                           </select>
	                        </div>
	                     </div>
	                     <div class="all-products">
	                     	<div class="row">
                                 @if(count(@$providers)>0)
                                   @foreach($providers as $pro)
								<div class="col-sm-6 col-md-4">
										<div class="service-pro-info">
											<div class="pro-wish">
												<a @if(Auth::user() && Auth::user()->user_type == 'U') href="{{  route('provider.save',['id'=>@$pro->id]) }}" @else href="javascript:;" @endif @if(!Auth::user()) onclick="alert('Please login with continue')" @endif> <img src="{{ url('public/frontend/images/wish2.png') }}"></a>

												{{--<a @if(Auth::user() && Auth::user()->user_type == 'U') href="javascript:;" @else href="javascript:;" @endif @if(!Auth::user()) onclick="alert('Please login with continue')" @endif> <img src="{{ url('public/frontend/images/wish2.png') }}"></a>--}}
											</div>
											<div class="ser-pro-info-top">
												<em>
                                                    @if($pro->profile_pic != null)
                                                    <img src="{{ URL::to('storage/app/public/profile_picture') }}/{{ $pro->profile_pic }}">
                                                    @else
													<img src="{{ url('public/frontend/images/pro1.png') }}">
                                                    @endif
												</em>
												<h2><a href="{{route('pro.profile',@$pro->slug)}}"> {{ $pro->name }} </a> <span> - ({{ @$pro->experience }} Year) </span></h2>
												<p> 
													@if(strlen(@$pro->about)>30)
													{!! substr(@$pro->about,0,30)  !!}
													@else
													 {!! @$pro->about !!}
													 @endif
												 </p>
												<ul>

												@if(@$pro->avg_review>=0.75 && @$pro->avg_review<=1.24)
													 @php
													 $ii = 1;
													 @endphp
													 @for($i=1; $i<=$ii; $i++)
													<li><i class="icofont-star"></i></li>
													 @endfor
													 <li class="grey-star"><i class="icofont-star"></i></li>
													<li class="grey-star"><i class="icofont-star"></i></li>
													<li class="grey-star"><i class="icofont-star"></i></li>
													<li class="grey-star"><i class="icofont-star"></i></li>
													<li><p>{{ @$ii }}<span> ({{ @$pro->tot_review }})</span></p></li>

													@elseif(@$pro->avg_review>=1.25 && @$pro->avg_review<=1.74)
													 @php
													 $ii = 1.5;
													 @endphp
													 @for($i=1; $i<=1; $i++)
													<li><i class="icofont-star"></i></li>
													 @endfor
													 <li><i class="fa fa-star-half-o"></i></li>
													 <li class="grey-star"><i class="icofont-star"></i></li>
													<li class="grey-star"><i class="icofont-star"></i></li>
													<li class="grey-star"><i class="icofont-star"></i></li>
													
													<li><p>{{ @$ii }}<span> ({{ @$pro->tot_review }})</span></p></li>
                                                    
												  @elseif(@$pro->avg_review>=1.75 && @$pro->avg_review<=2.24)
													 @php
													 $ii = 2;
													 @endphp
													 @for($i=1; $i<=$ii; $i++)
													<li><i class="icofont-star"></i></li>
													 @endfor
													 <li class="grey-star"><i class="icofont-star"></i></li>
													<li class="grey-star"><i class="icofont-star"></i></li>
													<li class="grey-star"><i class="icofont-star"></i></li>
													<li><p>{{ @$ii }}<span> ({{ @$pro->tot_review }})</span></p></li>

													@elseif(@$pro->avg_review>=2.25 && @$pro->avg_review<=2.74)
													 @php
													 $ii = 2.5;
													 @endphp
													 @for($i=1; $i<=2; $i++)
													<li><i class="icofont-star"></i></li>
													 @endfor
													 <li><i class="fa fa-star-half-o"></i></li>
													 <li class="grey-star"><i class="icofont-star"></i></li>
													<li class="grey-star"><i class="icofont-star"></i></li>
													
													<li><p>{{ @$ii }}<span> ({{ @$pro->tot_review }})</span></p></li>

													@elseif(@$pro->avg_review>=2.75 && @$pro->avg_review<=3.24)
													 @php
													 $ii = 3;
													 @endphp
													 @for($i=1; $i<=$ii; $i++)
													<li><i class="icofont-star"></i></li>
													 @endfor
													 <li class="grey-star"><i class="icofont-star"></i></li>
													<li class="grey-star"><i class="icofont-star"></i></li>
													<li><p>{{ @$ii }}<span> ({{ @$pro->tot_review }})</span></p></li>
                                                      
													
													@elseif(@$pro->avg_review>=3.25 && @$pro->avg_review<=3.74)
													 @php
													 $ii = 3.5
													 @endphp
													 @for($i=1; $i<=3; $i++)
													<li><i class="icofont-star"></i></li>
													 @endfor
													 <li><i class="fa fa-star-half-o"></i></li>
													<li class="grey-star"><i class="icofont-star"></i></li>
													<li><p>{{ @$ii }}<span> ({{ @$pro->tot_review }})</span></p></li>

													@elseif(@$pro->avg_review>=3.75 && @$pro->avg_review<=4.24)
													 @php
													 $ii = 4
													 @endphp
													 @for($i=1; $i<=4; $i++)
													<li><i class="icofont-star"></i></li>
													 @endfor
													 <li class="grey-star"><i class="icofont-star"></i></li>
			
													<li><p>{{ @$ii }}<span> ({{ @$pro->tot_review }})</span></p></li>
													@elseif(@$pro->avg_review>=4.25 && @$pro->avg_review<=4.74)
													 @php
													 $ii = 4.5
													 @endphp
													 @for($i=1; $i<=4; $i++)
													<li><i class="icofont-star"></i></li>
													 @endfor
													 <li><i class="fa fa-star-half-o"></i></li>
													<li><p>{{ @$ii }}<span> ({{ @$pro->tot_review }})</span></p></li>
													@elseif(@$pro->avg_review>=4.75 && @$pro->avg_review<=5.24)
													 @php
													 $ii = 5
													 @endphp
													 @for($i=1; $i<=5; $i++)
													<li><i class="icofont-star"></i></li>
													 @endfor
				
													<li><p>{{ @$ii }}<span> ({{ @$pro->tot_review }})</span></p></li>
													@else
                                                     @for($i=1; $i<=5; $i++)
                                                    <li class="grey-star"><i class="icofont-star"></i></li>
                                                    @endfor
                                                    <li><p>{{ number_format(@$pro->avg_review,1,'.',',') }}<span> ({{ @$pro->tot_review }})</span></p></li>
                                                    
                                                      @endif
													
												</ul>
											</div>

											<div class="ser-pro-info-bottom">
												<div class="providers-info">
													<p><img src="{{ url('public/frontend/images/p1.png') }}">{{ @$pro->proToCategory?@$pro->proToCategory->categoryName->category_name:'' }}</p>
													<p><img src="{{ url('public/frontend/images/p2.png') }}">@if(strlen(@$pro->address) > 17) {{substr(@$pro->address,0,17)}},@else {{@$pro->address}} ,@endif @if(@$pro->userCity->name) {{ @$pro->userCity->name }} @endif</p>
													<p><img src="{{ url('public/frontend/images/p3.png') }}">
													
													@if(!@$pro->providerToLanguage->isEmpty())

													 @php
                                                      
													 $i = count($pro->providerToLanguage)-1;
													 @endphp

													  @foreach($pro->providerToLanguage as $key=>$language)

													  <span>{{ $language->userLanguage->name }}</span>@if($key == $i) @else ,@endif
												     @endforeach
													 @endif
												</p>
												</div>
												<hr class="prod-hr">
												<div class="serp-budget">
													<div class="bud-details">
														<h5><img src="{{ url('public/frontend/images/01.png') }}"> 
                                                         @if(@$pro->budget_type == 'H')
                                                        {{ @$pro->budget }}/hr
                                                         @elseif(@$pro->budget_type == 'F')
                                                         {{ @$pro->budget }}/Fixed
														 @elseif(@$pro->budget_type == 'D')
														 {{ @$pro->budget }}/day
                                                         @endif
                                                         </h5>
														<p>{{ @$pro->total_project }} projects completed</p>
													</div>
													<a href="{{ route('pro.profile',['slug'=>@$pro->slug]) }}">View Profile</a>
												</div>
											</div>
											
										</div>
								</div>
                                @endforeach
								@else
								<div class="col-md-12">
                                    <div class="product-item search_product_item" style="text-align: center">
                                        <img src="{{url('public/frontend/images/error-no-search-results.png')}}" style="padding-top: 60px; width: 300px;">
                                    </div>
                                    <h4 style="color: #ef1600; text-align: center; font-size: 20px;">No provider available.</h4>
                                    <h6 style=" text-align: center;">Please check the spelling or try searching for something else</h6>
                                </div>
                                @endif
								{{--<div class="col-sm-6 col-md-4">
										<div class="service-pro-info">
											<div class="pro-wish">
												<a href="#"> <img src="images/wish2.png"></a>
											</div>
											<div class="ser-pro-info-top">
												<em>
													<img src="images/upro2.png">
												</em>
												<h2><a href="service-provider-profile.html"> Rabin Manna </a> <span> - (Expert) </span></h2>
												<p>This is a simply display name here</p>
												<ul>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><p>4.6 <span>(102)</span></p></li>
												</ul>
											</div>

											<div class="ser-pro-info-bottom">
												<div class="providers-info">
													<p><img src="images/p1.png "> Real estate Agent</p>
													<p><img src="images/p2.png  "> Btlanta, GA 30354</p>
													<p><img src="images/p3.png  "> English, Hindi</p>
												</div>
												<hr class="prod-hr">
												<div class="serp-budget">
													<div class="bud-details">
														<h5><img src="images/01.png"> 1025/hr</h5>
														<p>140 projects completed</p>
													</div>
													<a href="service-provider-profile.html">View Profile</a>
												</div>
											</div>
											
										</div>
								</div>
								<div class="col-sm-6 col-md-4">
										<div class="service-pro-info">
											<div class="pro-wish">
												<a href="#"> <img src="images/wish2.png"></a>
											</div>
											<div class="ser-pro-info-top">
												<em>
													<img src="images/pro3.png">
												</em>
												<h2><a href="service-provider-profile.html"> Saikat Basu </a> <span> - (Intermediate ) </span></h2>
												<p>This is a simply display name here</p>
												<ul>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li class="grey-star"><i class="icofont-star"></i></li>
													<li class="grey-star"><i class="icofont-star"></i></li>
													<li><p>3 <span>(102)</span></p></li>
												</ul>
											</div>

											<div class="ser-pro-info-bottom">
												<div class="providers-info">
													<p><img src="images/p1.png "> Construction project manager</p>
													<p><img src="images/p2.png  "> Btlanta, GA 30354</p>
													<p><img src="images/p3.png  "> English, Hindi</p>
												</div>
												<hr class="prod-hr">
												<div class="serp-budget">
													<div class="bud-details">
														<h5><img src="images/01.png"> 1025/day</h5>
														<p>140 projects completed</p>
													</div>
													<a href="service-provider-profile.html">View Profile</a>
												</div>
											</div>
											
										</div>
								</div>
								<div class="col-sm-6 col-md-4">
										<div class="service-pro-info">
											<div class="pro-wish">
												<a href="#"> <img src="images/wish2.png"></a>
											</div>
											<div class="ser-pro-info-top">
												<em>
													<img src="images/pro4.png">
												</em>
												<h2><a href="service-provider-profile.html"> Rabin Manna </a> <span> - (Expert) </span></h2>
												<p>This is a simply display name here</p>
												<ul>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><p>4.6 <span>(102)</span></p></li>
												</ul>
											</div>

											<div class="ser-pro-info-bottom">
												<div class="providers-info">
													<p><img src="images/p1.png "> Real estate Agent</p>
													<p><img src="images/p2.png  "> Btlanta, GA 30354</p>
													<p><img src="images/p3.png  "> English, Hindi</p>
												</div>
												<hr class="prod-hr">
												<div class="serp-budget">
													<div class="bud-details">
														<h5><img src="images/01.png"> 1025/hr</h5>
														<p>140 projects completed</p>
													</div>
													<a href="service-provider-profile.html">View Profile</a>
												</div>
											</div>
											
										</div>
								</div>
								<div class="col-sm-6 col-md-4">
										<div class="service-pro-info">
											<div class="pro-wish">
												<a href="#"> <img src="images/wish2.png"></a>
											</div>
											<div class="ser-pro-info-top">
												<em>
													<img src="images/pro5.png">
												</em>
												<h2><a href="service-provider-profile.html"> Saikat Basu </a> <span> - (Intermediate ) </span></h2>
												<p>This is a simply display name here</p>
												<ul>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><p>4.6 <span>(102)</span></p></li>
												</ul>
											</div>

											<div class="ser-pro-info-bottom">
												<div class="providers-info">
													<p><img src="images/p1.png "> Construction project manager</p>
													<p><img src="images/p2.png  "> Btlanta, GA 30354</p>
													<p><img src="images/p3.png  "> English, Hindi</p>
												</div>
												<hr class="prod-hr">
												<div class="serp-budget">
													<div class="bud-details">
														<h5><img src="images/01.png"> 1025/day</h5>
														<p>140 projects completed</p>
													</div>
													<a href="service-provider-profile.html">View Profile</a>
												</div>
											</div>
											
										</div>
								</div>
								<div class="col-sm-6 col-md-4">
										<div class="service-pro-info">
											<div class="pro-wish">
												<a href="#"> <img src="images/wish2.png"></a>
											</div>
											<div class="ser-pro-info-top">
												<em>
													<img src="images/pro6.png">
												</em>
												<h2><a href="service-provider-profile.html"> Rabin Manna </a> <span> - (Expert) </span></h2>
												<p>This is a simply display name here</p>
												<ul>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><p>4.6 <span>(102)</span></p></li>
												</ul>
											</div>

											<div class="ser-pro-info-bottom">
												<div class="providers-info">
													<p><img src="images/p1.png "> Real estate Agent</p>
													<p><img src="images/p2.png  "> Btlanta, GA 30354</p>
													<p><img src="images/p3.png  "> English, Hindi</p>
												</div>
												<hr class="prod-hr">
												<div class="serp-budget">
													<div class="bud-details">
														<h5><img src="images/01.png"> 1025/hr</h5>
														<p>140 projects completed</p>
													</div>
													<a href="service-provider-profile.html">View Profile</a>
												</div>
											</div>
											
										</div>
								</div>
								<div class="col-sm-6 col-md-4">
										<div class="service-pro-info">
											<div class="pro-wish">
												<a href="#"> <img src="images/wish2.png"></a>
											</div>
											<div class="ser-pro-info-top">
												<em>
													<img src="images/pro3.png">
												</em>
												<h2><a href="service-provider-profile.html"> Saikat Basu </a> <span> - (Intermediate ) </span></h2>
												<p>This is a simply display name here</p>
												<ul>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><p>4.6 <span>(102)</span></p></li>
												</ul>
											</div>

											<div class="ser-pro-info-bottom">
												<div class="providers-info">
													<p><img src="images/p1.png "> Construction project manager</p>
													<p><img src="images/p2.png  "> Btlanta, GA 30354</p>
													<p><img src="images/p3.png  "> English, Hindi</p>
												</div>
												<hr class="prod-hr">
												<div class="serp-budget">
													<div class="bud-details">
														<h5><img src="images/01.png"> 1025/day</h5>
														<p>140 projects completed</p>
													</div>
													<a href="service-provider-profile.html">View Profile</a>
												</div>
											</div>
											
										</div>
								</div>
								<div class="col-sm-6 col-md-4">
										<div class="service-pro-info">
											<div class="pro-wish">
												<a href="#"> <img src="images/wish2.png"></a>
											</div>
											<div class="ser-pro-info-top">
												<em>
													<img src="images/pro4.png">
												</em>
												<h2><a href="service-provider-profile.html"> Rabin Manna </a> <span> - (Expert) </span></h2>
												<p>This is a simply display name here</p>
												<ul>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><p>4.6 <span>(102)</span></p></li>
												</ul>
											</div>

											<div class="ser-pro-info-bottom">
												<div class="providers-info">
													<p><img src="images/p1.png "> Real estate Agent</p>
													<p><img src="images/p2.png  "> Btlanta, GA 30354</p>
													<p><img src="images/p3.png  "> English, Hindi</p>
												</div>
												<hr class="prod-hr">
												<div class="serp-budget">
													<div class="bud-details">
														<h5><img src="images/01.png"> 1025/hr</h5>
														<p>140 projects completed</p>
													</div>
													<a href="service-provider-profile.html">View Profile</a>
												</div>
											</div>
											
										</div>
								</div>
								<div class="col-sm-6 col-md-4">
										<div class="service-pro-info">
											<div class="pro-wish">
												<a href="#"> <img src="images/wish2.png"></a>
											</div>
											<div class="ser-pro-info-top">
												<em>
													<img src="images/pro3.png">
												</em>
												<h2><a href="service-provider-profile.html"> Saikat Basu </a> <span> - (Beginner) </span></h2>
												<p>This is a simply display name here</p>
												<ul>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><p>4.6 <span>(102)</span></p></li>
												</ul>
											</div>

											<div class="ser-pro-info-bottom">
												<div class="providers-info">
													<p><img src="images/p1.png "> Construction project manager</p>
													<p><img src="images/p2.png  "> Btlanta, GA 30354</p>
													<p><img src="images/p3.png  "> English, Hindi</p>
												</div>
												<hr class="prod-hr">
												<div class="serp-budget">
													<div class="bud-details">
														<h5><img src="images/01.png"> 1025/day</h5>
														<p>140 projects completed</p>
													</div>
													<a href="service-provider-profile.html">View Profile</a>
												</div>
											</div>
											
										</div>
								</div>
								<div class="col-sm-6 col-md-4">
										<div class="service-pro-info">
											<div class="pro-wish">
												<a href="#"> <img src="images/wish2.png"></a>
											</div>
											<div class="ser-pro-info-top">
												<em>
													<img src="images/pro5.png">
												</em>
												<h2><a href="service-provider-profile.html"> Rabin Manna </a> <span> - (Expert) </span></h2>
												<p>This is a simply display name here</p>
												<ul>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><p>4.6 <span>(102)</span></p></li>
												</ul>
											</div>

											<div class="ser-pro-info-bottom">
												<div class="providers-info">
													<p><img src="images/p1.png "> Real estate Agent</p>
													<p><img src="images/p2.png  "> Btlanta, GA 30354</p>
													<p><img src="images/p3.png  "> English, Hindi</p>
												</div>
												<hr class="prod-hr">
												<div class="serp-budget">
													<div class="bud-details">
														<h5><img src="images/01.png"> 1025/hr</h5>
														<p>140 projects completed</p>
													</div>
													<a href="service-provider-profile.html">View Profile</a>
												</div>
											</div>
											
										</div>
								</div>
								<div class="col-sm-6 col-md-4">
										<div class="service-pro-info">
											<div class="pro-wish">
												<a href="#"> <img src="images/wish2.png"></a>
											</div>
											<div class="ser-pro-info-top">
												<em>
													<img src="images/pro1.png">
												</em>
												<h2><a href="service-provider-profile.html"> Saikat Basu </a> <span> - (Beginner) </span></h2>
												<p>This is a simply display name here</p>
												<ul>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><p>4.6 <span>(102)</span></p></li>
												</ul>
											</div>

											<div class="ser-pro-info-bottom">
												<div class="providers-info">
													<p><img src="images/p1.png "> Construction project manager</p>
													<p><img src="images/p2.png  "> Btlanta, GA 30354</p>
													<p><img src="images/p3.png  "> English, Hindi</p>
												</div>
												<hr class="prod-hr">
												<div class="serp-budget">
													<div class="bud-details">
														<h5><img src="images/01.png"> 1025/day</h5>
														<p>140 projects completed</p>
													</div>
													<a href="service-provider-profile.html">View Profile</a>
												</div>
											</div>
											
										</div>
								</div>
								<div class="col-sm-6 col-md-4">
										<div class="service-pro-info">
											<div class="pro-wish">
												<a href="#"> <img src="images/wish2.png"></a>
											</div>
											<div class="ser-pro-info-top">
												<em>
													<img src="images/pro6.png">
												</em>
												<h2><a href="service-provider-profile.html"> Rabin Manna </a> <span> - (Expert) </span></h2>
												<p>This is a simply display name here</p>
												<ul>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><i class="icofont-star"></i></li>
													<li><p>4.6 <span>(102)</span></p></li>
												</ul>
											</div>

											<div class="ser-pro-info-bottom">
												<div class="providers-info">
													<p><img src="images/p1.png "> Real estate Agent</p>
													<p><img src="images/p2.png  "> Btlanta, GA 30354</p>
													<p><img src="images/p3.png  "> English, Hindi</p>
												</div>
												<hr class="prod-hr">
												<div class="serp-budget">
													<div class="bud-details">
														<h5><img src="images/01.png"> 1025/hr</h5>
														<p>140 projects completed</p>
													</div>
													<a href="service-provider-profile.html">View Profile</a>
												</div>
											</div>
											
										</div>
								</div>--}}
							</div>
						 </div>

						 <div class="paginationsec">
						 
								<ul>
								{{$providers->appends(request()->except(['page', '_token']))->links()}}
									{{--<li><a href="#url">
										<img src="images/pagleft.png" alt="" class="dpb">
										<img src="images/paglefthov.png" alt="" class="dpn">
										</a>
									</li>
									<li><a href="#url" class="actv">1</a></li>
									<li><a href="#url">2</a></li>
									<li><a href="#url">3</a></li>
									<li><a href="#url">4</a></li>
									<li><a href="#url">......</a></li>
									<li><a href="#url">25</a></li>
									<li><a href="#url">
										<img src="images/pagright.png" alt="" class="dpb">
										<img src="images/pagrightho.png" alt="" class="dpn">
									</a>
									</li>--}}
								</ul>
							</div>

					</div>

					
				</div>
			</div>

		</div>
	</section>
</div>
@endsection


@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection



@section('script')
@include('includes.script')
@include('includes.toaster')
<script>
 
  $(document).ready(function(){

	var allSliderAmount;
        @if(@$request['amount1']!=null && @$request['amount2']!=null)
        allSliderAmount=[];
        allSliderAmount=['{{$request['amount1']}}','{{$request['amount2']}}'];
        @else
        allSliderAmount=[0,'{{(int)@$maxPrice->budget}}'];
        @endif
        console.log(allSliderAmount);
        $( "#slider-range" ).slider({
            range: true,
            min:0 ,
            max: '{{(int)@$maxPrice->budget}}',
            values: allSliderAmount,
            slide: function( event, ui ) {
                $( "#amount" ).val( "₹" + ui.values[ 0 ] + " - ₹" + ui.values[ 1 ] );
                $( "#amount1" ).val( ui.values[ 0 ]);
                $( "#amount2" ).val( ui.values[ 1 ] );
            }
        });

$( "#amount" ).val( "₹" + $( "#slider-range" ).slider( "values", 0 ) +

    " - ₹" + $( "#slider-range" ).slider( "values", 1 ) ); 

  });

  $(document).ready(function(){

	    
  $('#checkAll').click(function(){

	    $('.checkboxname').prop('checked',$(this).prop('checked'));
  });

  $('#short_by').change(function(){
            $('#sort_by_value').val($(this).val());
            $('#searchFilter').submit();
        });

  });

</script>


<script type="text/javascript">
    $(document).ready(function(){

      $('#states').on('change',function(e){
        e.preventDefault();
        var id = $(this).val();
        $.ajax({
          url:'{{route('get.city')}}',
          type:'GET',
          data:{state:id},
          success:function(data){
            console.log(data);
            $('#city').html(data.city);
          }
        })
      });


     });
  </script>
  <script>
	   $(document).ready(function(){

$('#category').on('change',function(e){
  e.preventDefault();
  var id = $(this).val();
  $.ajax({
	url:'{{route('get.subcategory')}}',
	type:'GET',
	data:{category:id},
	success:function(data){
	  console.log(data);
	  $('#sub_category').html(data.subcategory);
	}
  })
});


});
  </script>

@endsection

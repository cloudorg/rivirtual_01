@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> Sell your home with confidence. RiVirtual:  #1 Residential Real Estate Advisor Platform in India</title>
<meta name="keywords" content="rivirtual, buy a property, rent a property, hire a pro, residential real estate, commercial real estate, flats for rent, Apartments for rent, flats for sale, apartments for sale, Properties for rent, commercial real estate for lease, commercial auctions,Properties for rent" />
<meta name="description" content="Find Residential  estate for sale, lease & auction on the # 1 real estate advisor and advertising marketplace." />
<meta name="robots" content="NOODP, NOYDIR, follow" />
<meta name="X-TrackingAnalytic" content="G-77HVYFKHNB" />
<meta property="og:title" content=" > Find perfect Residential home Buy or Rent- RiVirtual:  #1 Residential Real Estate Advisor Platform in India "/>
<meta property="og:description" content="RiVirtual - Exclusive Real Estate Intelligence Platform Lookup for Residential properties for rent,buy,sell in India Find real estate for sale, lease & auction on the # 1 real estate advisor and advertising marketplace"/>
<meta property="og:url" content="https://www.rivirtual.in"/>
<meta property="og:image" content="https://www.rivirtual.in/public/frontend/images/RiVirtusl_logosmall.jpg"/>
@endsection




@section('header')
@include('includes.header')
@endsection

@section('content')

<div class="haeder-padding"></div>

<!-- <div class="sell_hed_sec pop_sectins">
	<div class="container">
		<div class="sell_hed_inr ">
			<a href="javascript:;" class="cross"><i class="icofont-close-line"></i></a>
			<p>RIvirtual Offers is winding down, which means we are not making any new offers on homes. We're focused on helping existing customers and<br> selling our remaining inventory. If you're looking to sell, we can connect you with an expert agent in your market. If you have<br> questions about a current contract with RIvirtual Offers, get answers at <a href="#url" class="offer_cc">Offers & Closings.</a></p>
		</div>
	</div>
</div> -->
<div class="sell_ban_sec img_sell">
	<img src="{{ url('public/frontend/images/sel_ban.jpg') }}" alt="">
	<div class="sell_ban_bx">
		<div class="container">
			<div class="sell_ban_bx_inr">
				<h1>Sell your home with confidence</h1>
				<p>RiVirtual is making it simpler to sell your home and move forward.</p>
			</div>
		</div>
	</div>
</div>
<div class="traditionally_sec">
	<div class="container">
		<div class="traditionally_ir">
			<div class="traditionally_img sl_img">
				<img src="{{ url('public/frontend/images/selimg3.jpg') }}" alt="">
			</div>
			<div class="traditionally_text">
				<h2>Sell your property hassle-free.</h2>
				<p><b>RiVirtual helps you to sell your property with a minimum of your effort. The Agents provide guidance and support at every step.</b></p>
				<p style="font-size:13px"><i><b>Agents listed in the directory under ‘Find an agent’ are not licensed with RiVirtual, Inc. or any of our affiliated entities.</b></i></p>
				<!-- <a href="{{ route('find.agent') }}" class="see_cc">Find an agent</a>
				<p><a href="#" class="choose_agent">Learn how to choose an agent</a></p> -->
				<div class="row">
					<div class="col-lg-6">
						<div class="traditionally_text_bx">
							<h4>Why sell through an Agent</h4>
							<ul>
								<li><i class="fa fa-check" aria-hidden="true"></i>Best pricing according to market.</li>
								<li><i class="fa fa-check" aria-hidden="true"></i> Minimizes your leg work</li>
								<li><i class="fa fa-check" aria-hidden="true"></i> Manage all the documentation</li>
								<li><i class="fa fa-check" aria-hidden="true"></i>Takes care of price negotiation</li>
							</ul>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="traditionally_text_bx">
							<h4>How to sell through an Agent</h4>
							<p>The Agents know the locality and the market very well. We have a long list of trusted and professional real estate agents from your locality. You can just go to the <a href="{{route('search.agent')}}">list of Agents</a> and pick the one you like and that's it, just enjoy your day and wait for the best results.</p>
							<!-- <p>Learn more about the process of <a href="#">selling your house with a listing agent.</a> If this is the best route for you, interview agents and select a professional who will meet your expectations. Your agent will then guide you through the steps of <a href="#">selling your home.</a></p> -->
						</div>
					</div>
				</div>
				<!-- <p><small>Agents listed in the directory under ‘Find an agent’ are not licensed with RIvirtual, Inc. or any of our affiliated entities.</small></p> -->
			</div>
		</div>
	</div>
</div>
<div class="traditionally_sec">
	<div class="container">
		<div class="traditionally_ir">
			<div class="traditionally_img">
				<img src="{{ url('public/frontend/images/selimg4.jpg') }}" alt="">
			</div>
			<div class="traditionally_text">
				<h2>Selling with local partner agents</h2>
				<p>Selling through our trusted local Agents makes life easier*. Let them take all your stress while you can focus on other important things. Our Agent network is expanding every day.</p>
				<!-- <div class="traditionally_frm">
					<form class="banner-form" action="">
						  <div class="tab-content">
						      <div class="from-sec-banner">
						      	<div class="input-from">
						      		<div class="form-group ">
						      			<div class="input-with-icon">
											<input type="text" class="form-control" placeholder="Enter Your Address">
										</div>
						      		</div>
						      	</div>
						      	<div class="search-box">
						      		<input type="submit" name="" value="Get started">
						      	</div>
						      </div>
						  </div>
					</form>
				</div> -->
				<!-- <a href="#" class="see_cc">Get started</a> -->
				<div class="row">
					<div class="col-lg-6">
						<div class="traditionally_text_bx">
							<h4>Why sell with partner agent</h4>
							<ul>
								<li><i class="fa fa-check" aria-hidden="true"></i>Knows the market status current as well as future.</li>
								<li><i class="fa fa-check" aria-hidden="true"></i> Helps to negotiate</li>
								<li><i class="fa fa-check" aria-hidden="true"></i> Provide valuable insights.</li>
								<li><i class="fa fa-check" aria-hidden="true"></i> Professional style of listing on RiVirtual.</li>
								<li><i class="fa fa-check" aria-hidden="true"></i>  More chances of topping the list on RiVirtual.</li>
							</ul>
						</div>
					</div>
				</div>
				<p><small>* Listing compensation and terms are determined by local partner brokerages and not by RiVirtual. Subject to individual brokerage eligibility criteria. Restrictions may apply.</small></p>
			</div>
		</div>
	</div>
</div>
<div class="home_lone_sec">
	<div class="container">
		<div class="home_lone_inr">
			<div class="home_lone_logo">
				
				<span>Loans</span>
			</div>
			<div class="row">
				<div class="col-lg-5">
					<div class="homeLoane_bx">
						<h5>Extra advantage with Rivirtual Loans</h5>

						<p>Get financial advantages from <a href='{{route("loan.page")}}' class="choose_agent">RiVirtual loans</a>.
						<p>Visit our <a href='{{route("loan.page")}}' class="choose_agent">loan section</a> to know more about the</p>
						<p>benefits of <a href='{{route("loan.page")}}' class="choose_agent">RiVirtual loans</a>.</p>

						<!-- <a href="#" class="choose_agent">Start saving</a> -->
					</div>
				</div>
				<div class="col-lg-5">
					<div class="homeLoane_bx">
						<h5>Why RiVirtual Loans</h5>
						<ul>
							<li><i class="fa fa-check" aria-hidden="true"></i>Transparent process</li>
							<li><i class="fa fa-check" aria-hidden="true"></i>Quick and Easy</li>
							<li><i class="fa fa-check" aria-hidden="true"></i>Better rates</li>
							<li><i class="fa fa-check" aria-hidden="true"></i>Easy Eligibility Norms</li>

						</ul>
					</div>
				</div>
				<div class="col-lg-2">
					<div class="homeLoane_img">
						<h5>ADVERTISEMENT</h5>
						<img src="{{ url('public/frontend/images/selimg5.png') }}" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="traditionally_sec">
	<div class="container">
		<div class="traditionally_ir">
			<div class="traditionally_img">
				<img src="{{ url('public/frontend/images/selimg6.jpg') }}" alt="">
			</div>
			<div class="traditionally_text">
				<h2>FSBO (For Sale By Owner) <span class="blink_me">Coming Soon</span></h2>
				<p>Sell your home all by yourself on RiVirtual by following some easy steps without the help of agents. List your property on RiVirtual and let the potential buyers contact you.</p>
				<!-- <a href="#" class="see_cc">Post your home for sale</a> -->
				<p><a href="#" class="choose_agent">Learn more about FSBO</a></p>
				<div class="row">
					<div class="col-lg-6">
						<div class="traditionally_text_bx">
							<h4>How to sell FSBO</h4>
							<p>For FSBO you need to do staging, get a professional photographer, make the property sale-ready, research about price according to market and your locale. When you are ready,<a href="javascript:;">create a listing on RiVirtual</a>. Now the potential buyers will contact you, show them your property and negotiate the best price. You can also upgrade to make your property visible to potential buyers more frequently.</p>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="traditionally_text_bx">
							<h4>Why sell FSBO on RiVirtual</h4>
							<ul>
								<li><i class="fa fa-check" aria-hidden="true"></i>Listing on RiVirtual is free.</li>
								<li><i class="fa fa-check" aria-hidden="true"></i>Saves commission from agent</li>
								<li><i class="fa fa-check" aria-hidden="true"></i>Easy procedure</li>
								<li><i class="fa fa-check" aria-hidden="true"></i>Complete guidance from start to finish</li>

							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="successful_sale_sec">
	<div class="container">
		<div class="successful_sale_inr">
			<div class="section-heading text-center">
				<h2>Know your property value from experts</h2>
				<p>Share your address to get your free estimate</p>
			</div>
			<a href="{{route('contact.us')}}" class="see_cc">Contact Us</a>
			<!-- <div class="traditionally_frm">
				<form class="banner-form" action="">
					  <div class="tab-content">
					      <div class="from-sec-banner">
					      	<div class="input-from">
					      		<div class="form-group ">
					      			<div class="input-with-icon">
										<input type="text" class="form-control" placeholder="Enter Your Address">
									</div>
					      		</div>
					      	</div>
					      	<div class="search-box">
					      		<input type="submit" name="" value="Submit">
					      	</div>
					      </div>
					  </div>
				</form>
			</div> -->
		</div>
	</div>
</div>

<!-- <div class="acquainted_sec">
	<div class="container">
		<div class="section-heading2 text-center">
			<h2>Get acquainted with the process</h2>
			<p>As you begin the steps to selling, learn what to expect with our <a href="#url">Sellers Guide.</a> </p>
		</div>
		<div class="acquainted_sec_inr">
			<div class="faq-atbs">
				<ul class="nav nav-tabs ">
					<li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#acqu1">Preparing to sell </a></li>
					<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#acqu2">Timing your sale  </a></li>
					<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#acqu3">Pricing your home</a></li>
					<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#acqu4">Getting noticed</a></li>
					<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#acqu5">Offers, closing & moving</a></li>
				</ul>
			</div>
			<div class="tab-content">
				<div class="tab-pane active" id="acqu1">
				 	<div class="acquainted_tab_panel">
				 		<div class="row">
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list1.png" alt="">
				 					<h4><a href="#">How to Price Your Home to Sell</a></h4>
				 					<p>When selling, you definitely want to get top dollar.  Follow these nine steps to price your home competitively  for your market, sell quickly and earn maximum profit.</p>
				 				</div>
				 			</div>
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list2.png" alt="">
				 					<h4><a href="#">Best Home Improvements to Increase Value</a></h4>
				 					<p>If you're thinking about selling your home sometime in the next  few years, make sure any renovations you complete add value.  Learn more about home improvements with the best ROI, and a few to avoid.</p>
				 				</div>
				 			</div>
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list3.png" alt="">
				 					<h4><a href="#">Tips for Negotiating With Real Estate Agents</a></h4>
				 					<p>When interviewing listing agents, the majority  of sellers don't negotiate. Learn how to  successfully negotiate terms of your listing agreement with this guide.</p>
				 				</div>
				 			</div>
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list4.png" alt="">
				 					<h4><a href="#">How to Calculate Home Equity</a></h4>
				 					<p>Learn everything you need to know about your home equity:  how to calculate it, how it increases, how  much equity you need to sell and more in this helpful guide.</p>
				 				</div>
				 			</div>
				 		</div>
				 	</div>
				</div>
				<div class="tab-pane" id="acqu2">
				 	<div class="acquainted_tab_panel">
				 		<div class="row">
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list5.png" alt="">
				 					<h4><a href="#">How Soon Can I Sell My House After Purchase?</a></h4>
				 					<p>Need to sell your home sooner than expected?  Learn more about your break even timeframe, amortization , fees and possible consequences for selling soon after purchasing.</p>
				 				</div>
				 			</div>
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list6.png" alt="">
				 					<h4><a href="#">Selling a Property With Tenants</a></h4>
				 					<p>It's time to sell your rental property. But how do you  approach the topic with your tenants? Learn more  about sale timing, strategies and tenant considerations.</p>
				 				</div>
				 			</div>
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list7.png" alt="">
				 					<h4><a href="#">Can You Sell Your House Before Paying Off the Mortgage?</a></h4>
				 					<p>Selling your home before it's paid off is a common occurrence. Learn about who pays the mortgage when selling,  pricing to repay your balance and options for selling underwater homes.</p>
				 				</div>
				 			</div>
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list8.png" alt="">
				 					<h4><a href="#">What Happens When You Inherit a House?</a></h4>
				 					<p>It’s possible to inherit a property at fair market value and only pay  capital gains tax from the time of inheritance to sale. But,  existing mortgages and other stakeholders can complicate the process. Learn about your options.</p>
				 				</div>
				 			</div>
				 		</div>
				 	</div>
				</div>
				<div class="tab-pane" id="acqu3">
				 	<div class="acquainted_tab_panel">
				 		<div class="row">
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list1.png" alt="">
				 					<h4><a href="#">How to Price Your Home to Sell</a></h4>
				 					<p>When selling, you definitely want to get top dollar.  Follow these nine steps to price your home competitively  for your market, sell quickly and earn maximum profit.</p>
				 				</div>
				 			</div>
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list2.png" alt="">
				 					<h4><a href="#">Best Home Improvements to Increase Value</a></h4>
				 					<p>If you're thinking about selling your home sometime in the next  few years, make sure any renovations you complete add value.  Learn more about home improvements with the best ROI, and a few to avoid.</p>
				 				</div>
				 			</div>
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list3.png" alt="">
				 					<h4><a href="#">Tips for Negotiating With Real Estate Agents</a></h4>
				 					<p>When interviewing listing agents, the majority  of sellers don't negotiate. Learn how to  successfully negotiate terms of your listing agreement with this guide.</p>
				 				</div>
				 			</div>
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list4.png" alt="">
				 					<h4><a href="#">How to Calculate Home Equity</a></h4>
				 					<p>Learn everything you need to know about your home equity:  how to calculate it, how it increases, how  much equity you need to sell and more in this helpful guide.</p>
				 				</div>
				 			</div>
				 		</div>
				 	</div>
				</div>
				<div class="tab-pane" id="acqu4">
				 	<div class="acquainted_tab_panel">
				 		<div class="row">
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list5.png" alt="">
				 					<h4><a href="#">How Soon Can I Sell My House After Purchase?</a></h4>
				 					<p>Need to sell your home sooner than expected?  Learn more about your break even timeframe, amortization , fees and possible consequences for selling soon after purchasing.</p>
				 				</div>
				 			</div>
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list6.png" alt="">
				 					<h4><a href="#">Selling a Property With Tenants</a></h4>
				 					<p>It's time to sell your rental property. But how do you  approach the topic with your tenants? Learn more  about sale timing, strategies and tenant considerations.</p>
				 				</div>
				 			</div>
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list7.png" alt="">
				 					<h4><a href="#">Can You Sell Your House Before Paying Off the Mortgage?</a></h4>
				 					<p>Selling your home before it's paid off is a common occurrence. Learn about who pays the mortgage when selling,  pricing to repay your balance and options for selling underwater homes.</p>
				 				</div>
				 			</div>
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list8.png" alt="">
				 					<h4><a href="#">What Happens When You Inherit a House?</a></h4>
				 					<p>It’s possible to inherit a property at fair market value and only pay  capital gains tax from the time of inheritance to sale. But,  existing mortgages and other stakeholders can complicate the process. Learn about your options.</p>
				 				</div>
				 			</div>
				 		</div>
				 	</div>
				</div>
				<div class="tab-pane" id="acqu5">
				 	<div class="acquainted_tab_panel">
				 		<div class="row">
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list5.png" alt="">
				 					<h4><a href="#">How Soon Can I Sell My House After Purchase?</a></h4>
				 					<p>Need to sell your home sooner than expected?  Learn more about your break even timeframe, amortization , fees and possible consequences for selling soon after purchasing.</p>
				 				</div>
				 			</div>
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list6.png" alt="">
				 					<h4><a href="#">Selling a Property With Tenants</a></h4>
				 					<p>It's time to sell your rental property. But how do you  approach the topic with your tenants? Learn more  about sale timing, strategies and tenant considerations.</p>
				 				</div>
				 			</div>
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list7.png" alt="">
				 					<h4><a href="#">Can You Sell Your House Before Paying Off the Mortgage?</a></h4>
				 					<p>Selling your home before it's paid off is a common occurrence. Learn about who pays the mortgage when selling,  pricing to repay your balance and options for selling underwater homes.</p>
				 				</div>
				 			</div>
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list8.png" alt="">
				 					<h4><a href="#">What Happens When You Inherit a House?</a></h4>
				 					<p>It’s possible to inherit a property at fair market value and only pay  capital gains tax from the time of inheritance to sale. But,  existing mortgages and other stakeholders can complicate the process. Learn about your options.</p>
				 				</div>
				 			</div>
				 		</div>
				 	</div>
				</div>
			</div>
		</div>
	</div>
</div> -->


<div class="faqs-sec faq_sec">

		<div class="container">

			<div class="row">

				<div class="col-lg-10 col-xl-9 col-md-11 mx-auto">

					<div class="section-heading2 text-center">

						<h2>Frequently Asked Questions</h2>

						<!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit sed lorem sapien, auctor in justo id, dignissim dipiscing elit sed lorem sapien</p> -->

					</div>

				</div>



				<div class="col-lg-10 col-xl-9 col-md-11 mx-auto">

					<div class="faq-atbs">

							<ul class="nav nav-tabs ">

								<li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#today">User </a></li>

								<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#home">Agents  </a></li>

								<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#menu4">Service Pro</a></li>

								<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#menu5">Payments</a></li>

							</ul>

						</div>

						<div class="tab-content">

							<div class="tab-pane active" id="today">

								<div class="accordian-faq">

									<div class="accordion" id="faq">

												<div class="card">

													<div class="card-header" id="faqhead1">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq1" aria-expanded="true" aria-controls="faq1">

															<p><span>1. </span> What is RiVirtual?</p>

														</a>

													</div>

													<div id="faq1" class="collapse" aria-labelledby="faqhead1" data-parent="#faq">

														<div class="card-body">

															<p>RiVirtual is a privately owned global real estate intelligence company, development, and management firm in 100 cities in 5 countries and more than 100 million square feet of assets for which RiVirtual provides third-party property-level services. RiVirtual's Artificial Intelligence (AI) is based on Accessibility, Affordability, Availability, and Authenticity ( 4As) models of real estate markets, Economic Data, and Sustainability.</p>

														</div>

													</div>

												</div>





												<div class="card">

													<div class="card-header" id="faqhead2">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq2" aria-expanded="true" aria-controls="faq2">

															<p><span>2. </span> How does RiVirtual work?</p>

														</a>

													</div>

													<div id="faq2" class="collapse" aria-labelledby="faqhead2" data-parent="#faq">

														<div class="card-body">

															<p>RiVirtual is the #1 Most Trusted Real Estate Agents Platform in India, which provides a complete ecosystem of real estate providing an online marketplace, real estate agents, loans, property management, service providers, and much more.</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead3">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq3" aria-expanded="true" aria-controls="faq3">

															<p><span>3. </span>  How to Sign up as a User?</p>

														</a>

													</div>

													<div id="faq3" class="collapse" aria-labelledby="faqhead3" data-parent="#faq">

														<div class="card-body">

															<p>Follow the steps below:
																<ul>
																	<li>1.  	Click on the Sign-in button at the top right corner.</li>
																	<li>2.  	Click on Sign Up Now at the bottom of the pop-up form.</li>
																	<li>3.  	Enter your details and click Sign Up or Sign Up by your Facebook account or Google account.</li>
																</ul>
															</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead4">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq4" aria-expanded="true" aria-controls="faq4">

															<p><span>4. </span>  Is it safe to use login via Facebook & Google?</p>

														</a>

													</div>

													<div id="faq4" class="collapse" aria-labelledby="faqhead4" data-parent="#faq">

														<div class="card-body">

															<p>If you log in via Facebook or Google, your account will be automatically verified. At RiVirtual, we do not misuse your personal information, and we do not post anything from your social media for publicity. We do not ask for any extra information from your social media account.</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead5">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq5" aria-expanded="true" aria-controls="faq5">

															<p><span>5. </span> How to search for a property visit?</p>

														</a>

													</div>

													<div id="faq5" class="collapse" aria-labelledby="faqhead5" data-parent="#faq">

														<div class="card-body">

															<p>Based on the type of property you want to search, click on commercial or residential from the top menu. On the next page, enter the location where you want to explore and select the property type. You can toggle between BUY and RENT based upon your choice and click search.
															</p>
															<p>You will get a curated list of properties matching your criteria. You can further apply filters to the list.</p>

														</div>

													</div>

												</div>





												<div class="card">

													<div class="card-header" id="faqhead10">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq10" aria-expanded="true" aria-controls="faq10">

															<p><span>6. </span> How to book a property visit?</p>

														</a>

													</div>

													<div id="faq10" class="collapse" aria-labelledby="faqhead10" data-parent="#faq">

														<div class="card-body">

															<p>When you open a property details page by clicking on View More, you can fill out the short form on the right side and send a request to the agent for a property visit. You can also view the agent's profile and message him directly. (To be more secure, we recommend you to reach the agents through RiVirtual)</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead6">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq6" aria-expanded="true" aria-controls="faq6">

															<p><span>7. </span>  What is Property Management?</p>

														</a>

													</div>

													<div id="faq6" class="collapse" aria-labelledby="faqhead6" data-parent="#faq">

														<div class="card-body">

															<p>If you find it hard to manage your property because you don't have time or live far from the property, we provide a property management service to look after your property. To know all the features visit our <a href="{{route('property.manage.page')}}">Property Management</a> page and fill out the form for a free consultation.</p>

														</div>

													</div>

												</div>



												<!-- <div class="card">

													<div class="card-header" id="faqhead7">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq7" aria-expanded="true" aria-controls="faq7">

															<p><span>8. </span> Dummy questions here?</p>

														</a>

													</div>

													<div id="faq7" class="collapse" aria-labelledby="faqhead7" data-parent="#faq">

														<div class="card-body">

															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead8">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq8" aria-expanded="true" aria-controls="faq8">

															<p><span>9. </span> how  Rivirtual work?</p>

														</a>

													</div>

													<div id="faq8" class="collapse" aria-labelledby="faqhead8" data-parent="#faq">

														<div class="card-body">

															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead9">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq9" aria-expanded="true" aria-controls="faq9">

															<p><span>10. </span> Dummy questions here?</p>

														</a>

													</div>

													<div id="faq9" class="collapse" aria-labelledby="faqhead9" data-parent="#faq">

														<div class="card-body">

															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

														</div>

													</div>

												</div> -->

									</div>

								</div>

							</div>



							<div class="tab-pane " id="home">

								<div class="accordian-faq">

									<div class="accordion" id="accordionExample">

												<div class="card">

													<div class="card-header" id="faqhead11">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq11" aria-expanded="true" aria-controls="faq11">

															<p><span>1. </span> How to Sign up as an Agent?</p>

														</a>

													</div>

													<div id="faq11" class="collapse" aria-labelledby="faqhead11" data-parent="#accordionExample">

														<div class="card-body">

															<p>Follow the steps below:
																<ul>
																	<li>1.  	Click on the Sign-in button at the top right corner.</li>
																	<li>2.  	Click on Sign Up Now at the bottom of the pop-up form.</li>
																	<li>3.  	Enter your details and click Sign Up or Sign Up by your Facebook account or Google account.</li>
																</ul>
															</p>



														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead12">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq12" aria-expanded="true" aria-controls="faq12">

															<p><span>2. </span> How to hire an Agent?</p>

														</a>

													</div>

													<div id="faq12" class="collapse" aria-labelledby="faqhead12" data-parent="#accordionExample">

														<div class="card-body">

															<p>Click on the Agent from the top menu, enter your locality pin code, and click the Find an Agent button. You will get a list of top agents in your area. You can apply different filters to the list. You can view their profile and choose the best one you like.</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead13">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq13" aria-expanded="true" aria-controls="faq13">

															<p><span>3. </span> How will I know if my property is verified?</p>

														</a>

													</div>

													<div id="faq13" class="collapse" aria-labelledby="faqhead13" data-parent="#accordionExample">

														<div class="card-body">

															<p>When your property is verified on your registered email account, you will receive mail.</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead14">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq14" aria-expanded="true" aria-controls="faq14">

															<p><span>4. </span> How can I chat with customers?</p>

														</a>

													</div>

													<div id="faq14" class="collapse" aria-labelledby="faqhead14" data-parent="#accordionExample">

														<div class="card-body">

															<p>RiVirtual only activates the chatting feature when a User initiates the chatting.</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead15">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq15" aria-expanded="true" aria-controls="faq15">

															<p><span>5. </span> How to manage the availability of agents?</p>

														</a>

													</div>

													<div id="faq15" class="collapse" aria-labelledby="faqhead15" data-parent="#accordionExample">

														<div class="card-body">

															<p> When you go to the Manage Visit Availability section, you need to enter your availability hours for the week starting from Monday. This will reflect for the coming weeks. you can edit it anytime.</p>

														</div>

													</div>

												</div>





												<!-- <div class="card">

													<div class="card-header" id="faqhead16">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq16" aria-expanded="true" aria-controls="faq16">

															<p><span>6. </span> simple questions here?</p>

														</a>

													</div>

													<div id="faq16" class="collapse" aria-labelledby="faqhead16" data-parent="#accordionExample">

														<div class="card-body">

															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

														</div>

													</div>

												</div> -->

									</div>

								</div>

							</div>



							<div class="tab-pane " id="menu4">

								<div class="accordian-faq">

									<div class="accordion" id="accordionExampletwo">

												<div class="card">

													<div class="card-header" id="faqhead11">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq21" aria-expanded="true" aria-controls="faq21">

															<p><span>1. </span> How to Sign up as a Pro (Service Provider)?</p>

														</a>

													</div>

													<div id="faq21" class="collapse" aria-labelledby="faqhead21" data-parent="#accordionExampletwo">

														<div class="card-body">

															<p>Follow the steps below:
																<ul>
																	<li>1.  	Click on the Sign-in button at the top right corner.</li>
																	<li>2.  	Click on Sign Up Now at the bottom of the pop-up form.</li>
																	<li>3.  	Enter your details and click Sign Up or Sign Up by your Facebook account or Google account.</li>
																</ul>
															</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead22">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq22" aria-expanded="true" aria-controls="faq22">

															<p><span>2. </span> How to Hire a Pro?</p>

														</a>

													</div>

													<div id="faq22" class="collapse" aria-labelledby="faqhead22" data-parent="#accordionExampletwo">

														<div class="card-body">

															<p>Click on “Hire a Pro” on the top menu. From the “Hire a pro” page, you can fill the required entries in the search box and click search. You can apply additional filters to the list.</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead23">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq23" aria-expanded="true" aria-controls="faq23">

															<p><span>3. </span> How to create a Job?</p>

														</a>

													</div>

													<div id="faq23" class="collapse" aria-labelledby="faqhead23" data-parent="#accordionExampletwo">

														<div class="card-body">

															<p>Click on post a service requirement from the drop-down menu beside your profile picture. Fill out the required entries, upload the job picture, and click on the Post Service button. You can see all your Service requirements from the My service requirements section.</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead24">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq24" aria-expanded="true" aria-controls="faq24">

															<p><span>4. </span> How to apply for a Job?</p>

														</a>

													</div>

													<div id="faq24" class="collapse" aria-labelledby="faqhead24" data-parent="#accordionExampletwo">

														<div class="card-body">

															<p>From the list of jobs available for you, click on view more for the job you want. If you are interested in that job, click on the “I am interested” button on the right side of the page. In the pop-up, write the amount you want to quote, the number of days the job will take, and any description if you want to make it. Click Save, and the customer will receive your request with your quotes.</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead15">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq25" aria-expanded="true" aria-controls="faq25">

															<p><span>5. </span> How to chat with the customer?</p>

														</a>

													</div>

													<div id="faq25" class="collapse" aria-labelledby="faqhead25" data-parent="#accordionExampletwo">

														<div class="card-body">

															<p>The chatting feature will only be enabled when the customer initiates the chat.</p>

														</div>

													</div>

												</div>





												<!-- <div class="card">

													<div class="card-header" id="faqhead26">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq26" aria-expanded="true" aria-controls="faq26">

															<p><span>6. </span> simple questions here?</p>

														</a>

													</div>

													<div id="faq26" class="collapse" aria-labelledby="faqhead26" data-parent="#accordionExampletwo">

														<div class="card-body">

															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

														</div>

													</div>

												</div> -->

									</div>

								</div>

							</div>

							<div class="tab-pane " id="menu5">

								<div class="accordian-faq">

									<div class="accordion" id="accordionExamplefour">

												<div class="card">

													<div class="card-header" id="faqhead47">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq47" aria-expanded="true" aria-controls="faq47">

															<p><span>1. </span> What are the commission rates for Agents?</p>

														</a>

													</div>

													<div id="faq47" class="collapse" aria-labelledby="faqhead47" data-parent="#accordionExamplefour">

														<div class="card-body">

															<p>The market standard rate for Agents is 2%, but we recommend you discuss rates with your agent beforehand.
															</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead48">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq48" aria-expanded="true" aria-controls="faq48">

															<p><span>2. </span>  How to pay the Agents?</p>

														</a>

													</div>

													<div id="faq48" class="collapse" aria-labelledby="faqhead48" data-parent="#accordionExamplefour">

														<div class="card-body">

															<p>After the successful closing of the deal, the User can pay the pre-decided commission to the agent by any mode agreed by both of them.</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead49">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq49" aria-expanded="true" aria-controls="faq49">

															<p><span>3. </span> How does the Escrow payment method work?</p>

														</a>

													</div>

													<div id="faq49" class="collapse" aria-labelledby="faqhead49" data-parent="#accordionExamplefour">

														<div class="card-body">

															<p>After a customer awards the job to a Pro, he can create several milestones for the Job. For every milestone, he has to deposit a part of the sum in the Escrow account of RiVirtual. After compilation of the milestone, the customer will release the sum deposited for it, which will reach the Pro.</p>

														</div>

													</div>

												</div>



												<!-- <div class="card">

													<div class="card-header" id="faqhead24">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq24" aria-expanded="true" aria-controls="faq24">

															<p><span>4. </span> How to apply for a Job?</p>

														</a>

													</div>

													<div id="faq24" class="collapse" aria-labelledby="faqhead24" data-parent="#accordionExampletwo">

														<div class="card-body">

															<p>From the list of jobs available for you, click on view more for the job you want. If you are interested in that job, click on the “I am interested” button on the right side of the page. In the pop-up, write the amount you want to quote, the number of days the job will take, and any description if you want to make it. Click Save, and the customer will receive your request with your quotes.</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead15">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq25" aria-expanded="true" aria-controls="faq25">

															<p><span>5. </span> How to chat with the customer?</p>

														</a>

													</div>

													<div id="faq25" class="collapse" aria-labelledby="faqhead25" data-parent="#accordionExampletwo">

														<div class="card-body">

															<p>The chatting feature will only be enabled when the customer initiates the chat.</p>

														</div>

													</div>

												</div> -->





												<!-- <div class="card">

													<div class="card-header" id="faqhead26">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq26" aria-expanded="true" aria-controls="faq26">

															<p><span>6. </span> simple questions here?</p>

														</a>

													</div>

													<div id="faq26" class="collapse" aria-labelledby="faqhead26" data-parent="#accordionExampletwo">

														<div class="card-body">

															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

														</div>

													</div>

												</div> -->

									</div>

								</div>

							</div>

						</div>

				</div>



			</div>

		</div>

</div>
    @endsection

    @section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection

@section('script')
@include('includes.script')
@include('includes.toaster')
<script>
	$('.cross').click(function(){
		$('.sell_hed_sec').hide();
	})
</script>

@endsection
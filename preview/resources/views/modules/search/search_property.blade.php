@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
    .res_pro_type{
        display: none;
    }
</style>
@endsection

@section('title')
<title> RiVirtual | Search Property </title>
@endsection




@section('header')
@include('includes.header')
@endsection


@section('content')
<!----header--->
<div class="haeder-padding"></div>
<!-----filter--------->
<form action="{{route('search.property')}}" method="post" id="searchFilter">
    @csrf
    <section class="search-list">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 bor-r">
                    <div class="mobile-list">
                        <p><i class="fa fa-filter"></i> Show Filter</p>
                    </div>
                    <div class="search-filter">
                        <div class="search-section">
                            <div class="input-search">
                                <div class="inputsearch">
                                    <a href="#" class="user_llk"> <span>Property For</span> <img src="{{ URL::asset('public/frontend/images/caret.png')}}" class="caret-img"> </a>
                                    <div class="show01 filter-category" style="display: none;">
                                        <div class="diiferent-sec">
                                            <ul class="category-ul forbuy">
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="buy" name="property_for[]" value="B" {{@in_array('B', @$key['property_for'])?'checked':''}}>
                                                        <label for="buy">For Buy</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rent" name="property_for[]" value="R" {{@in_array('R', @$key['property_for'])?'checked':''}}>
                                                        <label for="rent">For Rent</label>

                                                    </div>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="inputsearch">
                                    <a href="#" class="user_lllk"> <span>Location</span> <img src="{{ URL::asset('public/frontend/images/caret.png')}}" class="caret-img"> </a>
                                    <div class="show02 filter-category" style="display: none;">
                                        <div class="diiferent-sec">

                                            <div class="locatiob-search">
                                                <input type="text" name="location" id="location" placeholder="Enter Location" value="{{@$key['location']}}">
                                                <ul id="serch_result" class="search_sajasation" style="display: none"></ul>
                                            </div>
                                        </div>
                                    </div>
                                </div> --}}
                                <div class="inputsearch">
                                    {{-- <input type="text" placeholder="Location" name="keyword" value="{{@$key['keyword']}}"> --}}
                                    <input type="text" name="location" id="location" placeholder="Enter Location" value="{{@$key['location']}}"  onkeyup="search_result_check(this);" autocomplete="off">
                                    <ul id="serch_result" class="search_sajasation" style="display: none"></ul>
                                </div>
                                <div class="inputsearch">
                                    <input type="text" placeholder="Keyword" name="keyword" value="{{@$key['keyword']}}">
                                </div>
                                <div class="inputsearch ">
                                    <div class="bedroom-derp user_llllllk" style="background:#fff !important">
                                        <select class="mstr_pro_type" name="master_property_type">
                                                <option value="COM" @if(@$key['master_property_type'] == 'COM') selected @endif>Commercial</option>
                                                <option value="RES" @if(@$key['master_property_type'] == 'RES') selected @endif>Residential</option>

                                                
                                        </select>
                                    </div>
                                </div>
                               
                                <div class="inputsearch res_pro_type" @if(@$key['master_property_type'] == 'RES') style="display:inline-block !important; "@endif>
                                    <a href="#" class="user_llllk"> <span>Property Type</span> <img src="{{ URL::asset('public/frontend/images/caret.png')}}" class="caret-img"> </a>
                                    <div class="show03 filter-category" style="display: none;">
                                        <div class="diiferent-sec">
                                            <ul class="category-ul">
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate1" name="property_type[]" value="F" {{@in_array('F', @$key['property_type'])?'checked':''}}>
                                                        <label for="rate1">Flat</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate2" name="property_type[]" value="H" {{@in_array('H', @$key['property_type'])?'checked':''}}>
                                                        <label for="rate2">House</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate5" name="property_type[]" value="GTCM" {{@in_array('GTCM', @$key['property_type'])?'checked':''}}>
                                                        <label for="rate5">Gated Community</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate3" name="property_type[]" value="L" {{@in_array('L', @$key['property_type'])?'checked':''}}>
                                                        <label for="rate3">Land</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate4" name="property_type[]" value="FRMH" {{@in_array('FRMH', @$key['property_type'])?'checked':''}}>
                                                        <label for="rate4">Farmhouse</label>
                                                    </div>
                                                </li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="inputsearch ofc_pro_type" @if(@$key['master_property_type'] == 'COM') style="display:inline-block !important;" @elseif(@$key['master_property_type'] == 'RES') style="display:none !important" @endif>
                                    <a href="#" class="user_llllk"> <span>Property Type</span> <img src="{{ URL::asset('public/frontend/images/caret.png')}}" class="caret-img"> </a>
                                    <div class="show03 filter-category" style="display: none;">
                                        <div class="diiferent-sec">
                                            <ul class="category-ul">
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate6" name="property_type[]" value="O" {{@in_array('OSP', @$key['property_type'])?'checked':''}}>
                                                        <label for="rate6">Office Space</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate7" name="property_type[]" value="WRH" {{@in_array('WRH', @$key['property_type'])?'checked':''}}>
                                                        <label for="rate7">Warehouse</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate8" name="property_type[]" value="SHR" {{@in_array('SHR', @$key['property_type'])?'checked':''}}>
                                                        <label for="rate8">Showrooms</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate9" name="property_type[]" value="INDT" {{@in_array('INDT', @$key['property_type'])?'checked':''}}>
                                                        <label for="rate9">Industrial</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate10" name="property_type[]" value="RET" {{@in_array('RET', @$key['property_type'])?'checked':''}}>
                                                        <label for="rate10">Retail</label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="inputsearch">
                                    <a href="#" class="user_lllllk"> <span>Area in sqft.</span> <img src="{{ URL::asset('public/frontend/images/caret.png')}}" class="caret-img"> </a>
                                    <div class="show04 filter-category" style="display: none;">
                                        <div class="diiferent-sec">
                                            <div class="forbuy">
                                                <input type="text" name="sqFtFrom" placeholder="From sqft." value="{{@$key['sqFtFrom']}}">
                                                <input type="text" name="sqFtTo" placeholder="To sqft." value="{{@$key['sqFtTo']}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="inputsearch">
                                    <!-- <a href="#" class="user_llllllk"> <span>Bedroom </span> <img src="{{ URL::asset('public/frontend/images/caret.png')}}" class="caret-img"> </a> -->
                                    <!-- <div class="show05 filter-category" style="display: none;"> -->
                                        <div class="bedroom-derp user_llllllk" style="background:#fff !important">
                                            <select class="" name="no_of_bedrooms">
                                                <option value="">Bedroom</option>
                                                @for ($i = 1; $i <= 10; $i++)
                                                <option value="{{ $i }}" @if(@$key['no_of_bedrooms']==$i) selected @endif> {{ $i }} @if($i == 1) Bedroom @else Bedrooms @endif</option>
                                                @endfor
                                                {{-- <option>2 Bedroom</option>
                                                <option>3 Bedroom</option>
                                                <option>4 Bedroom</option> --}}
                                            </select>
                                        </div>
                                    <!-- </div> -->
                                </div>
                            </div>
                            <div class="apply-btn">
                                <div class="mor-filters">
                                    <a href="#" class="user_lllllllk"> <span>More Filters</span> <img src="{{ URL::asset('public/frontend/images/gr-caret.png')}}" class="caret-img"> </a>
                                    <div class="show06 filter-category" style="display: none;">
                                            <div class="diiferent-sec">
                                                <div class="bedroom-derp">
                                                    <label>Bathroom</label>
                                                    <select name="bathroom">
                                                        <option value="">Select Bathroom</option>
                                                        @for ($i = 1; $i <= 10; $i++)
                                                        <option value="{{ $i }}" @if(@$key['bathroom']==$i) selected @endif>{{ $i }} Bathroom</option>
                                                        @endfor
                                                        {{-- <option>1 Bathroom</option>
                                                        <option>2 Bathroom</option>
                                                        <option>Attach Bathroom</option> --}}
                                                    </select>
                                                </div>
                                                <div class="bedroom-derp">
                                                    <label>Construction Status</label>
                                                    <select name="construction_status">
                                                        <option value="">Select</option>
                                                        <option value="RM" @if (@$key['construction_status']=='RM') selected @endif>Ready to Move</option>
                                                        <option value="UC" @if (@$key['construction_status']=='UC') selected @endif>Under Construction</option>
                                                        <option value="PL" @if (@$key['construction_status']=='PL') selected @endif>Pre-Launch</option>
                                                    </select>
                                                </div>
                                                <div class="bedroom-derp">
                                                    <label>Furnishing</label>
                                                    <select name="furnishing">
                                                        <option value="">select</option>
                                                        <option value="SF" @if (@$key['furnishing']=='SF') selected @endif>Semi Furnished</option>
                                                        <option value="FF" @if (@$key['furnishing']=='FF') selected @endif>Full Furnished</option>
                                                        <option value="NF" @if (@$key['furnishing']=='NF') selected @endif>Not Furnished</option>
                                                    </select>
                                                </div>
                                                <div class="bedroom-derp">
                                                    <label>Budget</label>
                                                    <div class="slider_rnge">
                                                      <div id="slider-range" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
                                                        <div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 0%; width: 100%;"></div> <span tabindex="0" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 0%;"></span> <span tabindex="0" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 100%;"></span> </div> <span class="range-text">
                                                               <input type="text" class="price_numb" readonly id="amount" name="amount">
                                                               <input type="hidden" class="price_numb" id="amount1" name="amount1" value="{{@$key['amount1']?@$key['amount1']:0}}">
                                                               <input type="hidden" class="price_numb" id="amount2" name="amount2" value="{{@$key['amount2']?@$key['amount2']:(int)@$maxPrice->budget_range_from}}">
                                                               </span>
                                                    </div>
                                                </div>
                                                <div class="filter-re">
                                                    <a href="{{route('search.property')}}" class="rest"> Reset Filters</a>
                                                    <a href="javascript:;" class="done-se submit_from"> Done</a>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" class="price_numb" id="short_by_value" name="short_by_value" value="{{@$key['short_by_value']?@$key['short_by_value']:0}}">
                                </div>

                                <input type="submit" name="" class="save-search" value="Search">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </section>
</form>

<!-----filter--------->
<section class="search-thum">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="show-result">
                    <div class="property-data">
                    {{-- <h5>Open Houses in Atlanta, GA , For Sale & Real Estate</h5> --}}
                    <h5>{{@$key['location']}}</h5>
                    <p>{{@$totalProperty}} 
                    @if(@$key['master_property_type'] == 'COM')
                    @if(@$key['property_type']){{@in_array('OSP', @$key['property_type'])?'Office Space,':''}} {{@in_array('WRH', @$key['property_type'])?'Warehouse,':''}} {{@in_array('SHR', @$key['property_type'])?'Showrooms,':''}} {{@in_array('INDT', @$key['property_type'])?'Industrial,':''}} {{@in_array('RET', @$key['property_type'])?'Retail,':''}} @else Properties @endif @elseif(@$key['master_property_type'] == 'RES') @if(@$key['property_type']){{@in_array('F', @$key['property_type'])?'Flat,':''}} {{@in_array('H', @$key['property_type'])?'House,':''}} {{@in_array('GTCM', @$key['property_type'])?'Gated Community,':''}} {{@in_array('L', @$key['property_type'])?'Land,':''}} {{@in_array('FRMH', @$key['property_type'])?'Farmhouse,':''}} @else Properties @endif 
                    @endif found on RiVirtual</p>
                    </div>
                    <div class="sort-filter">
                        <p>Sort By : </p>
                        <select class="sort-select" id="short_by" name="short_by">
                            <option value="0" @if(@$key['short_by_value']==0) selected @endif>New Listings</option>
                            <option value="1" @if(@$key['short_by_value']==1) selected @endif>Price Low to High</option>
                            <option value="2" @if(@$key['short_by_value']==2) selected @endif>Price High to Low</option>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                </div>

            </div>
        </div>
        <div class="row">
            @foreach (@$allProperty as $property)

            <div class="col-lg-4 col-sm-6">
                <div class="property-item">
                    <div class="listing-image">
                        <a href="{{route('search.property.details',['slug'=>@$property->slug])}}">
                         @if(@$property->property_type == 'L')
                         <img src="{{ URL::asset('storage/app/public/property_image')}}/{{@$property->landPropertyImageMain->image}}" alt="listing property">
                         @else
                        <img src="{{ URL::asset('storage/app/public/property_image')}}/{{@$property->propertyImageMain->image}}" alt="listing property">
                        @endif
                        <div class="propety-info-top"></div></a>
                        <div class="ribbon-img">
                            <div class="ribbon-vertical"><p>For Rent</p></div>
                        </div>
                        @if(@$property->property_for =='R')
                        <div class="ribbon-img">
                            <div class="ribbon-vertical"><p>For Rent</p></div>
                        </div>
                        @elseif(@$property->property_for =='B')
                        <div class="ribbon-img">
                            <div class="ribbon-vertical Sale"><p>For Sale</p></div>
                        </div>
                        @endif
                        @if(@$property->propertySave)
                        <div class="wish-property save-property">
                            <a @if(Auth::user() && Auth::user()->user_type == 'U') href="{{  route('property.save',['id'=>@$property->id]) }}" @else href="javascript:;" @endif @if(!Auth::user()) onclick="alert('Please login with continue')" @endif><i class="icofont-heart"></i></a>
                        </div>
                        @else
                        <div class="wish-property">
                            <a @if(Auth::user() && Auth::user()->user_type == 'U') href="{{  route('property.save',['id'=>@$property->id]) }}" @else href="javascript:;" @endif @if(!Auth::user()) onclick="alert('Please login with continue')" @endif><i class="icofont-heart"></i></a>
                        </div>
                        @endif
                        <div class="location-rating">
                            <div class="product-img-location">
                                <p><img src="{{ URL::asset('public/frontend/images/loac.png')}}">
                                @if(strlen(@$property->localityName->locality_name)>7) 
                                    
                                    {{substr(@$property->localityName->locality_name,0,7)}}..

                                @else{{@$property->localityName->locality_name}}@endif,

                                @if(strlen(@$property->cityName->name)>7)
                                    {{substr(@$property->cityName->name,0,7)}}..
                                @else
                                    {{@$property->cityName->name}}
                                @endif,
                                @if(strlen(@$property->stateName->state_abbreviations)>2)
                                    {{substr(@$property->stateName->state_abbreviations,0,2)}}..
                                @else
                                {{@$property->stateName->state_abbreviations}}
                                @endif
                                </p>
                            </div>
                            <div class="rating-pro">
                                <p><img src="{{ URL::asset('public/frontend/images/star.png')}}">{{ number_format(@$property->avg_review,1, '.', ',') }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="property-detasils-info @if(@$property->property_type == 'L' || @$property->property_type == 'FRMH') only_land1 @endif ">
                        <div class="name-price">
                            <a href="{{route('search.property.details',['slug'=>@$property->slug])}}">
                                <h5>
                                    @if(strlen(@$property->name) > 40)
                                    {!! substr(@$property->name, 0, 40 ) . '..' !!}
                                    @else
                                    {!!@$property->name!!}
                                    @endif
                                </h5>
                            </a>
                            <p>₹{{ number_format(@$property->budget_range_from, 0, '.', ',') }} </p>
                        </div>
                        <ul class="list-item-info ">
                        @if(@$property->property_type == 'F' || @$property->property_type == 'H' || @$property->property_type == 'GTCM')
                        <li class="before">
                         <span>{{@$property->no_of_bedrooms}} <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span> Bedrooms
                         </li>
                         @elseif(@$property->property_type == 'WRH' || @$property->property_type == 'INDT' || @$property->property_type == 'SHR' || @$property->property_type == 'RET' )
                         <li class="before">
                         <span>{{@$property->no_of_bedrooms}} <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span> Rooms
                         </li>
                          @endif
                          @if(@$property->property_type == 'F' || @$property->property_type == 'H' || @$property->property_type == 'GTCM' || @$property->property_type == 'OSP' || @$property->property_type == 'WRH' || @$property->property_type == 'SHR' || @$property->property_type == 'INDT' || @$property->property_type == 'RET')
                            <li class="before">
                           <span>{{@$property->bathroom}} <img src="{{ URL::asset('public/frontend/images/bath.png')}}"></span> Bathrooms
                         </li>
                           @endif
                            @if(@$property->property_type == 'OSP')
                            <li class="before">
                                <span>{{@$property->office_class}} <img src="{{ URL::asset('public/frontend/images/ofc.png')}}"></span> Class
                            </li>
                            @endif
                            <li class="">
                                <span>{{@$property->area}} <img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
                            </li>
                        </ul>
                    </div>
                    <div class="propety-agent">
                        <div class="agent-info">
                            <em>
                                @if(@$property->propertyUser->profile_pic != null)
                                <img src="{{ URL::to('storage/app/public/profile_picture')}}/{{@$property->propertyUser->profile_pic}}" alt="">
                                @else
                                <img src="{{ URL::to('public/frontend/images/agent1.png')}}" alt="">
                                @endif
                            </em>
                            <div class="agent-name">
                                <h6> @if(strlen(@$property->propertyUser->name) > 20)
                                    {!! substr(@$property->propertyUser->name, 0, 20 ) . '..' !!}
                                    @else
                                    {!!@$property->propertyUser->name!!}
                                    @endif</h6>
                                <p>{{ @$property->propertyUser->totalProperty->count() }} Properties</p>
                            </div>
                        </div>
                        <a href="{{route('search.property.details',['slug'=>@$property->slug])}}" class="vi-more">
                            View More
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
            {{-- <div class="col-lg-4 col-sm-6">
                <div class="property-item">
                    <div class="listing-image">
                            <a href="property-details.html"><<img src="images/list2.png" alt="listing property">
                            <div class="propety-info-top">
                            </div></a>
                            <div class="ribbon-img">
                                <div class="ribbon-vertical Sale"><p>For Sale</p></div>
                            </div>
                            <div class="wish-property">
                                <a href="#"><i class="icofont-heart"></i></a>
                            </div>
                            <div class="location-rating">
                                <div class="product-img-location">
                                    <p><img src="images/loac.png">New York , USA</p>
                                </div>
                                <div class="rating-pro">
                                    <p><img src="images/star.png"> 4.5</p>
                                </div>
                            </div>
                    </div>
                    <div class="property-detasils-info">
                        <div class="name-price">
                            <a href="property-details.html"><h5>Blue Reef Properties</h5></a>
                            <p>₹9,100 </p>
                        </div>
                        <ul class="list-item-info ">
                            <li class="before">
                                <span>3 <img src="images/bed.png"></span> Bedrooms
                            </li>
                            <li class="before">
                                <span>2 <img src="images/bath.png"></span> Bathrooms
                            </li>
                            <li class="">
                                <span>3450 <img src="images/sqft.png"></span> Square Ft
                            </li>
                        </ul>
                    </div>
                    <div class="propety-agent">
                        <div class="agent-info">
                            <em>
                                <img src="images/agent2.png">
                            </em>
                            <div class="agent-name">
                                <h6>Gina Mconihon</h6>
                                <p>14 Properties</p>
                            </div>
                        </div>
                        <a href="#" class="vi-more">
                            View More
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="property-item">
                    <div class="listing-image">
                            <a href="property-details.html"><<img src="images/list3.png" alt="listing property">
                            <div class="propety-info-top">
                            </div></a>
                            <div class="ribbon-img">
                                <div class="ribbon-vertical Sale"><p>For Sale</p></div>
                            </div>
                            <div class="wish-property">
                                <a href="#"><i class="icofont-heart"></i></a>
                            </div>
                            <div class="location-rating">
                                <div class="product-img-location">
                                    <p><img src="images/loac.png">Hydrabad</p>
                                </div>
                                <div class="rating-pro">
                                    <p><img src="images/star.png"> 4.8</p>
                                </div>
                            </div>
                    </div>
                    <div class="property-detasils-info">
                        <div class="name-price">
                            <a href="property-details.html"><h5>Blue Reef Properties</h5></a>
                            <p>₹4,300 </p>
                        </div>
                        <ul class="list-item-info ">
                            <li class="before">
                                <span>3 <img src="images/bed.png"></span> Bedrooms
                            </li>
                            <li class="before">
                                <span>2 <img src="images/bath.png"></span> Bathrooms
                            </li>
                            <li class="">
                                <span>3450 <img src="images/sqft.png"></span> Square Ft
                            </li>
                        </ul>
                    </div>
                    <div class="propety-agent">
                        <div class="agent-info">
                            <em>
                                <img src="images/agent1.png">
                            </em>
                            <div class="agent-name">
                                <h6>Gina Mconihon</h6>
                                <p>14 Properties</p>
                            </div>
                        </div>
                        <a href="#" class="vi-more">
                            View More
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="property-item">
                    <div class="listing-image">
                            <a href="property-details.html"><img src="images/list4.png" alt="listing property">
                            <div class="propety-info-top">
                            </div></a>
                            <div class="ribbon-img">
                                <div class="ribbon-vertical Sale"><p>For Sale</p></div>
                            </div>
                            <div class="wish-property">
                                <a href="#"><i class="icofont-heart"></i></a>
                            </div>
                            <div class="location-rating">
                                <div class="product-img-location">
                                    <p><img src="images/loac.png">Canada</p>
                                </div>
                                <div class="rating-pro">
                                    <p><img src="images/star.png"> 4.5</p>
                                </div>
                            </div>
                    </div>
                    <div class="property-detasils-info">
                        <div class="name-price">
                            <a href="property-details.html"><h5>Blue Reef Properties</h5></a>
                            <p>₹9,500 </p>
                        </div>
                        <ul class="list-item-info ">
                            <li class="before">
                                <span>3 <img src="images/bed.png"></span> Bedrooms
                            </li>
                            <li class="before">
                                <span>2 <img src="images/bath.png"></span> Bathrooms
                            </li>
                            <li class="">
                                <span>3450 <img src="images/sqft.png"></span> Square Ft
                            </li>
                        </ul>
                    </div>
                    <div class="propety-agent">
                        <div class="agent-info">
                            <em>
                                <img src="images/agent2.png">
                            </em>
                            <div class="agent-name">
                                <h6>Gina Mconihon</h6>
                                <p>14 Properties</p>
                            </div>
                        </div>
                        <a href="#" class="vi-more">
                            View More
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="property-item">
                    <div class="listing-image">
                            <a href="property-details.html"><<img src="images/list5.png" alt="listing property">
                            <div class="propety-info-top">
                            </div></a>
                            <div class="ribbon-img">
                                <div class="ribbon-vertical"><p>For Rent</p></div>
                            </div>
                            <div class="wish-property">
                                <a href="#"><i class="icofont-heart"></i></a>
                            </div>
                            <div class="location-rating">
                                <div class="product-img-location">
                                    <p><img src="images/loac.png">Canada</p>
                                </div>
                                <div class="rating-pro">
                                    <p><img src="images/star.png"> 4.5</p>
                                </div>
                            </div>
                    </div>
                    <div class="property-detasils-info">
                        <div class="name-price">
                            <a href="property-details.html"><h5>Blue Reef Properties</h5></a>
                            <p>₹7,500 </p>
                        </div>
                        <ul class="list-item-info ">
                            <li class="before">
                                <span>3 <img src="images/bed.png"></span> Bedrooms
                            </li>
                            <li class="before">
                                <span>2 <img src="images/bath.png"></span> Bathrooms
                            </li>
                            <li class="">
                                <span>3450 <img src="images/sqft.png"></span> Square Ft
                            </li>
                        </ul>
                    </div>
                    <div class="propety-agent">
                        <div class="agent-info">
                            <em>
                                <img src="images/agent1.png">
                            </em>
                            <div class="agent-name">
                                <h6>Gina Mconihon</h6>
                                <p>14 Properties</p>
                            </div>
                        </div>
                        <a href="#" class="vi-more">
                            View More
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="property-item">
                    <div class="listing-image">
                            <a href="property-details.html"><<img src="images/list6.png" alt="listing property">
                            <div class="propety-info-top">
                            </div></a>
                            <div class="ribbon-img">
                                <div class="ribbon-vertical Sale"><p>For Sale</p></div>
                            </div>
                            <div class="wish-property">
                                <a href="#"><i class="icofont-heart"></i></a>
                            </div>
                            <div class="location-rating">
                                <div class="product-img-location">
                                    <p><img src="images/loac.png">Canada</p>
                                </div>
                                <div class="rating-pro">
                                    <p><img src="images/star.png"> 4.5</p>
                                </div>
                            </div>
                    </div>
                    <div class="property-detasils-info">
                        <div class="name-price">
                            <a href="property-details.html"><h5>Blue Reef Properties</h5></a>
                            <p>₹7,300 </p>
                        </div>
                        <ul class="list-item-info ">
                            <li class="before">
                                <span>3 <img src="images/bed.png"></span> Bedrooms
                            </li>
                            <li class="before">
                                <span>2 <img src="images/bath.png"></span> Bathrooms
                            </li>
                            <li class="">
                                <span>3450 <img src="images/sqft.png"></span> Square Ft
                            </li>
                        </ul>
                    </div>
                    <div class="propety-agent">
                        <div class="agent-info">
                            <em>
                                <img src="images/agent2.png">
                            </em>
                            <div class="agent-name">
                                <h6>Gina Mconihon</h6>
                                <p>14 Properties</p>
                            </div>
                        </div>
                        <a href="#" class="vi-more">
                            View More
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="property-item">
                    <div class="listing-image">
                            <a href="property-details.html"><<img src="images/list1.png" alt="listing property">
                            <div class="propety-info-top">
                            </div></a>
                            <div class="ribbon-img">
                                <div class="ribbon-vertical Sale"><p>For Sale</p></div>
                            </div>
                            <div class="wish-property">
                                <a href="#"><i class="icofont-heart"></i></a>
                            </div>
                            <div class="location-rating">
                                <div class="product-img-location">
                                    <p><img src="images/loac.png">Canada</p>
                                </div>
                                <div class="rating-pro">
                                    <p><img src="images/star.png"> 4.5</p>
                                </div>
                            </div>
                    </div>
                    <div class="property-detasils-info">
                        <div class="name-price">
                            <a href="property-details.html"><h5>Blue Reef Properties</h5></a>
                            <p>₹4,500 </p>
                        </div>
                        <ul class="list-item-info ">
                            <li class="before">
                                <span>3 <img src="images/bed.png"></span> Bedrooms
                            </li>
                            <li class="before">
                                <span>2 <img src="images/bath.png"></span> Bathrooms
                            </li>
                            <li class="">
                                <span>3450 <img src="images/sqft.png"></span> Square Ft
                            </li>
                        </ul>
                    </div>
                    <div class="propety-agent">
                        <div class="agent-info">
                            <em>
                                <img src="images/agent1.png">
                            </em>
                            <div class="agent-name">
                                <h6>Gina Mconihon</h6>
                                <p>14 Properties</p>
                            </div>
                        </div>
                        <a href="#" class="vi-more">
                            View More
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="property-item">
                    <div class="listing-image">
                            <a href="property-details.html"><<img src="images/list4.png" alt="listing property">
                            <div class="propety-info-top">
                            </div></a>
                            <div class="ribbon-img">
                                <div class="ribbon-vertical"><p>For Rent</p></div>
                            </div>
                            <div class="wish-property">
                                <a href="#"><i class="icofont-heart"></i></a>
                            </div>
                            <div class="location-rating">
                                <div class="product-img-location">
                                    <p><img src="images/loac.png">Canada</p>
                                </div>
                                <div class="rating-pro">
                                    <p><img src="images/star.png"> 4.5</p>
                                </div>
                            </div>
                    </div>
                    <div class="property-detasils-info">
                        <div class="name-price">
                            <a href="property-details.html"><h5>Blue Reef Properties</h5></a>
                            <p>₹3,500 </p>
                        </div>
                        <ul class="list-item-info ">
                            <li class="before">
                                <span>3 <img src="images/bed.png"></span> Bedrooms
                            </li>
                            <li class="before">
                                <span>2 <img src="images/bath.png"></span> Bathrooms
                            </li>
                            <li class="">
                                <span>3450 <img src="images/sqft.png"></span> Square Ft
                            </li>
                        </ul>
                    </div>
                    <div class="propety-agent">
                        <div class="agent-info">
                            <em>
                                <img src="images/agent2.png">
                            </em>
                            <div class="agent-name">
                                <h6>Gina Mconihon</h6>
                                <p>14 Properties</p>
                            </div>
                        </div>
                        <a href="#" class="vi-more">
                            View More
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="property-item">
                    <div class="listing-image">
                            <a href="property-details.html"><<img src="images/list7.png" alt="listing property">
                            <div class="propety-info-top">
                            </div></a>
                            <div class="ribbon-img">
                                <div class="ribbon-vertical"><p>For Rent</p></div>
                            </div>
                            <div class="wish-property">
                                <a href="#"><i class="icofont-heart"></i></a>
                            </div>
                            <div class="location-rating">
                                <div class="product-img-location">
                                    <p><img src="images/loac.png">Canada</p>
                                </div>
                                <div class="rating-pro">
                                    <p><img src="images/star.png"> 4.5</p>
                                </div>
                            </div>
                    </div>
                    <div class="property-detasils-info">
                        <div class="name-price">
                            <a href="property-details.html"><h5>Blue Reef Properties</h5></a>
                            <p>₹7,500 </p>
                        </div>
                        <ul class="list-item-info ">
                            <li class="before">
                                <span>3 <img src="images/bed.png"></span> Bedrooms
                            </li>
                            <li class="before">
                                <span>2 <img src="images/bath.png"></span> Bathrooms
                            </li>
                            <li class="">
                                <span>3450 <img src="images/sqft.png"></span> Square Ft
                            </li>
                        </ul>
                    </div>
                    <div class="propety-agent">
                        <div class="agent-info">
                            <em>
                                <img src="images/agent1.png">
                            </em>
                            <div class="agent-name">
                                <h6>Gina Mconihon</h6>
                                <p>14 Properties</p>
                            </div>
                        </div>
                        <a href="#" class="vi-more">
                            View More
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="property-item">
                    <div class="listing-image">
                            <a href="property-details.html"><<img src="images/list3.png" alt="listing property">
                            <div class="propety-info-top">
                            </div></a>
                            <div class="ribbon-img">
                                <div class="ribbon-vertical"><p>For Rent</p></div>
                            </div>
                            <div class="wish-property">
                                <a href="#"><i class="icofont-heart"></i></a>
                            </div>
                            <div class="location-rating">
                                <div class="product-img-location">
                                    <p><img src="images/loac.png">Canada</p>
                                </div>
                                <div class="rating-pro">
                                    <p><img src="images/star.png"> 4.5</p>
                                </div>
                            </div>
                    </div>
                    <div class="property-detasils-info">
                        <div class="name-price">
                            <a href="property-details.html"><h5>Blue Reef Properties</h5></a>
                            <p>₹6,500 </p>
                        </div>
                        <ul class="list-item-info ">
                            <li class="before">
                                <span>3 <img src="images/bed.png"></span> Bedrooms
                            </li>
                            <li class="before">
                                <span>2 <img src="images/bath.png"></span> Bathrooms
                            </li>
                            <li class="">
                                <span>3450 <img src="images/sqft.png"></span> Square Ft
                            </li>
                        </ul>
                    </div>
                    <div class="propety-agent">
                        <div class="agent-info">
                            <em>
                                <img src="images/agent2.png">
                            </em>
                            <div class="agent-name">
                                <h6>Gina Mconihon</h6>
                                <p>14 Properties</p>
                            </div>
                        </div>
                        <a href="#" class="vi-more">
                            View More
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="property-item">
                    <div class="listing-image">
                            <a href="property-details.html"><<img src="images/list2.png" alt="listing property">
                            <div class="propety-info-top">
                            </div></a>
                            <div class="ribbon-img">
                                <div class="ribbon-vertical Sale"><p>For Sale</p></div>
                            </div>
                            <div class="wish-property">
                                <a href="#"><i class="icofont-heart"></i></a>
                            </div>
                            <div class="location-rating">
                                <div class="product-img-location">
                                    <p><img src="images/loac.png">Canada</p>
                                </div>
                                <div class="rating-pro">
                                    <p><img src="images/star.png"> 4.5</p>
                                </div>
                            </div>
                    </div>
                    <div class="property-detasils-info">
                        <div class="name-price">
                            <a href="property-details.html"><h5>Blue Reef Properties</h5></a>
                            <p>₹4,500 </p>
                        </div>
                        <ul class="list-item-info ">
                            <li class="before">
                                <span>3 <img src="images/bed.png"></span> Bedrooms
                            </li>
                            <li class="before">
                                <span>2 <img src="images/bath.png"></span> Bathrooms
                            </li>
                            <li class="">
                                <span>3450 <img src="images/sqft.png"></span> Square Ft
                            </li>
                        </ul>
                    </div>
                    <div class="propety-agent">
                        <div class="agent-info">
                            <em>
                                <img src="images/agent2.png">
                            </em>
                            <div class="agent-name">
                                <h6>Gina Mconihon</h6>
                                <p>14 Properties</p>
                            </div>
                        </div>
                        <a href="#" class="vi-more">
                            View More
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="property-item">
                    <div class="listing-image">
                            <a href="property-details.html"><<img src="images/list4.png" alt="listing property">
                            <div class="propety-info-top">
                            </div></a>
                            <div class="ribbon-img">
                                <div class="ribbon-vertical Sale"><p>For Sale</p></div>
                            </div>
                            <div class="wish-property">
                                <a href="#"><i class="icofont-heart"></i></a>
                            </div>
                            <div class="location-rating">
                                <div class="product-img-location">
                                    <p><img src="images/loac.png">Canada</p>
                                </div>
                                <div class="rating-pro">
                                    <p><img src="images/star.png"> 4.5</p>
                                </div>
                            </div>
                    </div>
                    <div class="property-detasils-info">
                        <div class="name-price">
                            <a href="property-details.html"><h5>Blue Reef Properties</h5></a>
                            <p>₹4,500 </p>
                        </div>
                        <ul class="list-item-info ">
                            <li class="before">
                                <span>3 <img src="images/bed.png"></span> Bedrooms
                            </li>
                            <li class="before">
                                <span>2 <img src="images/bath.png"></span> Bathrooms
                            </li>
                            <li class="">
                                <span>3450 <img src="images/sqft.png"></span> Square Ft
                            </li>
                        </ul>
                    </div>
                    <div class="propety-agent">
                        <div class="agent-info">
                            <em>
                                <img src="images/agent1.png">
                            </em>
                            <div class="agent-name">
                                <h6>Gina Mconihon</h6>
                                <p>14 Properties</p>
                            </div>
                        </div>
                        <a href="#" class="vi-more">
                            View More
                        </a>
                    </div>
                </div>
            </div> --}}
            @if(@$allProperty->isEmpty())
            <div class="col-sm-12 col-md-12">
                <center style="padding: 10px"><p>No Poperty Available</p></center>
            </div>
            @endif
        </div>
        <div class="pagination-section">
            <div class="row">
                <div class="col-12">
                    <nav aria-label="Page navigation example" class="list-pagination">
                        <ul class="pagination justify-content-center">
                            {{@$allProperty->appends(request()->except(['page', '_token']))->links()}}
                            {{-- <li class="page-item disabled">
                                <a class="page-link page-link-prev" href="#" aria-label="Previous" tabindex="-1" aria-disabled="true"> Prev </a>
                            </li>
                            <li class="page-item active" aria-current="page"><a class="page-link" href="#">1</a> </li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item page-item-dots"><a class="page-link" href="#">6</a></li>
                            <li class="page-item"> <a class="page-link page-link-next" href="#" aria-label="Next"></a> </li> --}}
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection


@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection



@section('script')
@include('includes.script')
@include('includes.toaster')
{{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRZMuXnvy3FntdZUehn0IHLpjQm55Tz1E&libraries=places&callback=initAutocomplete" async defer></script>
<script>
    function initAutocomplete() {
        // Create the search box and link it to the UI element.
        var input = document.getElementById('location');

        var options = {
          types: ['establishment']
        };

        var input = document.getElementById('location');
        var autocomplete = new google.maps.places.Autocomplete(input, options);

        autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);

        autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();
            console.log(place)
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            $('#lat').val(place.geometry.location.lat());
            $('#long').val(place.geometry.location.lng());
            lat = place.geometry.location.lat();
            lng = place.geometry.location.lng();
            $('.exct_btn').show();

            initMap();
        });
        initMap();
    }
</script>

<script>

    function initMap() {
        geocoder = new google.maps.Geocoder();
        var lat = $('#lat').val();
        var lng = $('#lng').val();
        var myLatLng = new google.maps.LatLng(lat, lng);
        // console.log(myLatLng);
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 16,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'Choose hotel location',
          draggable: true
        });

        google.maps.event.addListener(marker, 'dragend', function(evt,status){
        $('#lat').val(evt.latLng.lat());
        $('#long').val(evt.latLng.lng());
        var lat_1 = evt.latLng.lat();
        var lng_1 = evt.latLng.lng();
        var latlng = new google.maps.LatLng(lat_1, lng_1);
            geocoder.geocode({'latLng': latlng}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    $('#location').val(results[0].formatted_address);
                }
            });


        });
    }
    </script> --}}
<script>
    $(document).ready(function(){
        var allSliderAmount;
        @if(@$key['amount1']!=null)
        allSliderAmount=[];
        allSliderAmount=['{{$key['amount1']}}','{{$key['amount2']}}'];
        @else
        allSliderAmount=[0,'{{(int)@$maxPrice->budget_range_from}}'];
        @endif
        console.log(allSliderAmount);
        var x=allSliderAmount[ 0 ];
        var y=allSliderAmount[ 1 ];
                x=x.toString();
                y=y.toString();
                var lastThree = x.substring(x.length-3);
                var otherNumbers = x.substring(0,x.length-3);
                var lastThreey = y.substring(y.length-3);
                var otherNumbersy = y.substring(0,y.length-3);
                if(otherNumbers != '')
                lastThree = ',' + lastThree;
                if(otherNumbersy != '')
                lastThreey = ',' + lastThreey;
                var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
                var resy = otherNumbersy.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThreey;
        $( "#slider-range" ).slider({
            range: true,
            min:0 ,
            max: '{{(int)@$maxPrice->budget_range_from}}',
            values: allSliderAmount,
            slide: function( event, ui ) {
                var x=ui.values[ 1 ];
                var y=ui.values[ 0 ];
                x=x.toString();
                y=y.toString();
                var lastThree = x.substring(x.length-3);
                var otherNumbers = x.substring(0,x.length-3);
                var lastThreey = y.substring(y.length-3);
                var otherNumbersy = y.substring(0,y.length-3);
                if(otherNumbers != '')
                lastThree = ',' + lastThree;
                if(otherNumbersy != '')
                lastThreey = ',' + lastThreey;
                var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
                var resy = otherNumbersy.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThreey;
                $( "#amount" ).val( "₹" + resy + " - ₹" + res );
                $( "#amount1" ).val( ui.values[ 0 ]);
                $( "#amount2" ).val( ui.values[ 1 ] );
            }
        });
        $( "#amount" ).val( "₹" + res +" - ₹" + resy );
        $('.submit_from').click(function(){
            $('#searchFilter').submit();
        })
        $('#short_by').change(function(){
            $('#short_by_value').val($(this).val());
            $('#searchFilter').submit();
        })
    })
</script>
<script>
    function search_result_check(that) {
        var $this = that;
        var name = $($this).val();
        console.log($($this).val());
        $.ajax({
            type: "POST",
            url:"{{route('get.locality.available')}}",
            data: {
                'name': name,
                '_token':'{{@csrf_token()}}'
            },
            success: function(data) {
              if(data){
                console.log(data);
                $('#serch_result').show();
                $('#serch_result').fadeIn();
                $('#serch_result').last().html(data);

              }
              else
              {
                $('#serch_result').children().remove();
              }
            }
        });
    }
    function add_searchbar(that) {
        var $this = that;
        var name = $($this).text();
        console.log(name);
        $('#location').val(name);
        $($this).remove();
        $('#serch_result').children().remove();
        $('#serch_result').hide();
    }
</script>
<script>
    $('.mstr_pro_type').change(function(){
        var val =this.value;
        if(val == 'RES'){
            $('.res_pro_type').css('display','inline-block');
            $('.ofc_pro_type').css('display','none');
        }else if(val == 'COM'){
            $('.ofc_pro_type').css('display','inline-block');;
            $('.res_pro_type').css('display','none');
        }
    });
</script>
@endsection

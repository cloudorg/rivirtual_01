@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> Hire the perfect real estate agent in your area- RiVirtual:  #1 Residential Real Estate Advisor Platform in India</title>
<meta name="keywords" content="rivirtual, property agents, property loans agents, hire a pro, residential broker, commercial real estate loans, flats for rent, Apartments for rent, flats for sale, apartments for sale, Properties for rent, commercial real estate for lease, commercial auctions, Properties for rent" />
<meta name="description" content=" Get the free recommendation of Top real estate agents in your area that help speed up the process and with maximum profits. We analyze millions of real estate transactions to bring you the most relevant real estate agents.. " />
<meta name="robots" content="NOODP, NOYDIR, follow" />
<meta name="X-TrackingAnalytic" content="G-77HVYFKHNB" />
<meta property="og:title" content=" > The right Loan could save you Lakhs - RiVirtual:  #1 Residential Real Estate Advisor Platform in India "/>
<meta property="og:description" content="RiVirtual - Get the free recommendation of Top real estate agents in your area that help speed up the process and with maximum profits. We analyze millions of real estate transactions to bring you the most relevant real estate agents."/>
<meta property="og:url" content="https://www.rivirtual.in"/>
<meta property="og:image" content="https://www.rivirtual.in/public/frontend/images/RiVirtusl_logosmall.jpg"/>
@endsection


@section('header')
@include('includes.header')
@endsection

@section('content')

<div class="haeder-padding"></div>

<!----banner--->
<section class="agents-section">
	<div class="banner-image">
		<img src="{{ url('public/frontend/images/agent-img1.png') }}" alt="banner-image">
	</div>
	<div class="agents_texts">
		<div class="container">
			<div class="row">
				<div class="col-xl-6 col-lg-8 col-md-9 col-sm-10">
					<h1>Hire the perfect real estate agent in your area</h1>
					<p>Get the free recommendation of Top real estate agents in your area that help speed up the process and with maximum profits. We analyze millions of real estate transactions to bring you the most relevant real estate agents.</p>
					<form action="{{ route('search.agent') }}" method="post" id="searchForm">
                        @csrf
						<div class="agent_serachs">
							<div class="from-sec-banner2">
								<div class="fr_in">
									<input type="text" name="post_code" id="post_code" placeholder="Enter Pin code">
								</div>
								<div class="search-box2">							      		
							      		<input type="submit" name="" value="Find an Agent">
							      	</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<!----banner--->
<section class="agents-contact_sec">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-md-6">
				<img src="{{ url('public/frontend/images/img2.png') }}" class="img_can">
			</div>
			<div class="col-md-6">
				<div class="contact_infos">
					<h1>Voted #1 Most Trusted Real Estate Agents Platform in India</h1>
					<p>With the limited number of homes on the current India market, you need an agent with the right connections, technology, and strategies to achieve your home buying or selling vision. Whether you're feeling overwhelmed and want someone to take the wheel, or you just need a second opinion and you have it covered, you can be rest assured - RiVirtual agent is the right agent for any level of service, in any market condition.</p>
					<!-- <ul>
						<li><a href="tel:+0123-456789 "> Call: +0123-456789  </a></li>
						<li><a href="mailto:info@rivirtual.com">Email: info@rivirtual.com </a></li>
					</ul> -->
					<a href="#" class="cont_an butn_cont">Contact Us</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="property-sec py-50">
	<div class="container">
		<div class="row">
			<div class="col-lg-10 col-xl-9 col-md-11 mx-auto">
				<div class="section-heading2 text-center">
					<h2>What our Clients say.</h2>
					<p>Your views and insights are important to us. We’re delighted when you tell us we can share these more widely and are proud to be able to give you access to these testimonials.</p>
				</div>
			</div>

			<div class="col-lg-10 col-xl-11 col-md-11 mx-auto revew_sec_agent2">
				<div class="revew_panel">
							<div class="revew_item">
								<div class="revew_top">
									<div class="media">
										
										<div class="media-body">
											<h4>Rakesh Reddy</h4>
											<div class="star_bx">
											<ul>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star-half-o"></i></li>
											</ul>
											<strong><img src="{{ url('public/frontend/images/calendar2.png') }}" alt=""> 17th March, 2022</strong>
										</div>
										</div>
									</div>
								</div>
								<p>Professional, knowledgeable, and friendly Marketplace and excellent customer service</p>
							</div>

							<div class="revew_item">
								<div class="revew_top">
									<div class="media">
										
										<div class="media-body">
											<h4>Anirban Goswami</h4>
											<div class="star_bx">
											<ul>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star-half-o"></i></li>
											</ul>
											<strong><img src="{{ url('public/frontend/images/calendar2.png') }}" alt=""> 1st March, 2022</strong>
										</div>
										</div>
									</div>
								</div>
								<p>Outstanding Realestate Market place and get advice and insights.</p>
							</div>

							<div class="revew_item">
								<div class="revew_top">
									<div class="media">
										
										<div class="media-body">
											<h4>Raghavendra Kumar</h4>
											<div class="star_bx">
											<ul>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star-half-o"></i></li>
											</ul>
											<strong><img src="{{ url('public/frontend/images/calendar2.png') }}" alt=""> 15th Feb, 2022</strong>
										</div>
										</div>
									</div>
								</div>
								<p>Extremely Satisfied Customer and homeowner</p>
							</div>

							<div class="revew_item">
								<div class="revew_top">
									<div class="media">
										
										<div class="media-body">
											<h4>Sujit Kumary</h4>
											<div class="star_bx">
											<ul>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star-half-o"></i></li>
											</ul>
											<strong><img src="{{ url('public/frontend/images/calendar2.png') }}" alt=""> 5th Feb, 2022</strong>
										</div>
										</div>
									</div>
								</div>
								<p>Best Real estate advisor in India</p>
							</div>

							<div class="revew_item">
								<div class="revew_top">
									<div class="media">
										
										<div class="media-body">
											<h4>Eshwar Setineni</h4>
											<div class="star_bx">
											<ul>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star-half-o"></i></li>
											</ul>
											<strong><img src="{{ url('public/frontend/images/calendar2.png') }}" alt=""> 11th Jan, 2022</strong>
										</div>
										</div>
									</div>
								</div>
								<p>Best Real Estate advice I ever got</p>
							</div>

							<!-- <div class="revew_item">
								<div class="revew_top">
									<div class="media">
										
										<div class="media-body">
											<h4>Akashbev Roy</h4>
											<div class="star_bx">
											<ul>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star-half-o"></i></li>
											</ul>
											<strong><img src="{{ url('public/frontend/images/calendar2.png') }}" alt=""> 20th Sept, 2021</strong>
										</div>
										</div>
									</div>
								</div>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the of Letraset sheets containing  It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, Lorem Ipsum has been the of Letraset sheets containing  Lorem Ipsum passages..</p>
							</div> -->
							
						</div>
			</div>

		</div>



	</div>
</section>
@endsection

@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection

@section('script')
@include('includes.script')
@include('includes.toaster')
<script>
    $(document).ready(function(){

           $('#searchForm').validate({

                 rules: {

                    post_code:{
                    //required: true,
                    digits: true ,
                    minlength:6,
                } 
                 },
                 messages: {

                    post_code:{
                    required: 'Enter post code',
                    digits: 'post code only number ',
                    minlength:'Post code exactly 6 digits',
                },
                 }
           })
    })
</script>
@endsection
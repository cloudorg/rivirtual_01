@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> Rivirtual property management -  #1 Residential Real Estate Advisor Platform in India</title>
<meta name="keywords" content="rivirtual, property management, property loans agents, hire a pro, residential property management , commercial property management loans, flats for rent, Apartments for rent, flats for sale, apartments for sale, Properties for rent, commercial real estate for lease, commercial auctions, Properties for rent" />
<meta name="description" content=" Our property manager will visit the property from time to time, inspect the property and give the updates to the owners. If needed the manager will capture pictures or even do a video call to the owners to give the live iew of property. " />
<meta name="robots" content="NOODP, NOYDIR, follow" />
<meta name="X-TrackingAnalytic" content="G-77HVYFKHNB" />
<meta property="og:title" content=" > Rivirtual property management - RiVirtual:  #1 Residential Real Estate Advisor Platform in India "/>
<meta property="og:description" content=" Our property manager will visit the property from time to time, inspect the property and give the updates to the owners. If needed the manager will capture pictures or even do a video call to the owners to give the live iew of property "/>
<meta property="og:url" content="https://www.rivirtual.in"/>
<meta property="og:image" content="https://www.rivirtual.in/public/frontend/images/RiVirtusl_logosmall.jpg"/>
@endsection




@section('header')
@include('includes.header')
@endsection

@section('content')

<div class="haeder-padding"></div>

<div class="property_manag_ban">
	<div class="container">
		<div class="property_manag_inn">
			<div class="property_manag_left">
				<h4>RiVirtual Property Management</h4>
				<ul>
					<li><em><i class="fa fa-check"></i></em>There will be timely and live updates provided to the owners on the website. </li>
					<li><em><i class="fa fa-check"></i></em>One may hire a property manager for all or any one of the services mentioned.</li>
					<li><em><i class="fa fa-check"></i></em>The property management service will also be provided for offices, lands and vacant properties.</li>
				</ul>
			</div>
			<div class="property_manag_rig">
				<div class="property_manag_from">
					<h4> To Manage your Property</h4>
					<p>Get in touch with our Home expert </p>
					<form method="POST" action="{{route('save.property.management')}}" id="myForm">
						@csrf
						<div class="das_input">
							<input type="text" placeholder="Name" value="" name="name">
						</div>
						<div class="das_input mt-0">
							<input type="text" placeholder="Phone" value="" name="phone">
						</div>
						<div class="das_input mt-0">
							<input type="text" placeholder="Email" value="" name="email">
						</div>
						<div class="das_input mt-0">
							<select name="service_city">
								<option hidden value="">Choose Service City</option>
								<option value="Bangalore">Bangalore</option>
								<option value="Hyderabad">Hyderabad</option>
								<option value="Chennai">Chennai</option>
								<option value="Mumbai">Mumbai</option>
								<option value="Pune">Pune</option>
								<option value="Delhi">Delhi</option>
								<option value="Noida">Noida</option>
								<option value="Greater Noida">Greater Noida</option>
								<option value="Ghaziabad">Ghaziabad</option>
								<option value="Faridabad">Faridabad</option>
								<option value="Gurgaon">Gurgaon</option>
								<option value="Other City">Other City</option>
							</select>
						</div>
						<div class="das_input mt-0">
							<select name="user_type">
								<option hidden value="" >I am</option>
								<option value="Owner">Owner</option>
								<option value="Tenant">Tenant</option>
								<option value="Seller">Seller</option>
								<option value="Buyer">Buyer</option>
							</select>
						</div>
						<div class="manage_sub">
							<input type="submit" value="Manage My Home" class="see_cc">
						</div>
						<div class="manage_tx">
							<!-- <span>Call or WhatsApp us <a href="tel:918793456789">+918793456789 </a></span> -->
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="property_manag_list">
	<div class="container">
		<div class="section-heading text-center">
			<h2>Services offered</h2>
		</div>
		<div class="property_manag_list_inr">
			<div class="row">
				<div class="col-md-12">
					<div class="property_manag_box">
						<em><img src="{{ url('public/frontend/images/pr1w.png') }}" alt=""></em>
						<div class="property_text">
							<h5>Inspection of property:</h5>
							<p>Our property manager will visit the property from time to time, inspect the property and give updates to the owners through photos and videos.</p>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="property_manag_box">
						<em><img src="{{ url('public/frontend/images/pr3w.png') }}" alt=""></em>
						<div class="property_text">
							<h5>Property maintenance:</h5>
							<p>Our property manager will take care of any repairments and upgradations required after the consultation with the owners.</p>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="property_manag_box">
						<em><img src="{{ url('public/frontend/images/pr8w.png') }}" alt=""></em>
						<div class="property_text">
							<h5>Rent Management:</h5>
							<p>Our property manager will study the market and quote the best rent rate and take care of timely and hassle-free collection of the rent.</p>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="property_manag_box">
						<em><img src="{{ url('public/frontend/images/pr4w.png') }}" alt=""></em>
						<div class="property_text">
							<h5>Tenant search:</h5>
							<p>The property manager will look for the tenants for the property and show around the property to the potential tenants.</p>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="property_manag_box">
						<em><img src="{{ url('public/frontend/images/pr5w.png') }}" alt=""></em>
						<div class="property_text">
							<h5>Tenant verification:</h5>
							<p>Background verification with proper documentation of the new tenants before handing them the property.</p>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="property_manag_box">
						<em><img src="{{ url('public/frontend/images/pr2w.png') }}" alt=""></em>
						<div class="property_text">
							<h5>Property vacating:</h5>
							<p>Take care of the vacating processes of the property. We conduct a complete damage inspection of property and amenities and deal accordingly with the leaving tenants.</p>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="property_manag_box">
						<em><img src="{{ url('public/frontend/images/pr11w.png') }}" alt=""></em>
						<div class="property_text">
							<h5>Listen to the complaints:</h5>
							<p>The Property Manager will listen to the tenants’ complaints and try to resolve them at their level.</p>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="property_manag_box">
						<em><img src="{{ url('public/frontend/images/pr12w.png') }}" alt=""></em>
						<div class="property_text">
							<h5>Documentation:</h5>
							<p>The Property Manager will take care of all the documentation related to leasing the property and update the owner and tenant about the renewal of the contract or any changes.</p>
						</div>
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>
@endsection


@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection



@section('script')
@include('includes.script')
@include('includes.toaster')
<script>
     $('#myForm').validate({
        rules:{
            name:{
                required:true,
            },
            email:{
                required:true,
                email:true,
            },
            phone:{
                required:true,
                digits:true,
                maxlength:10,
                minlength:10,
            },
            service_city:{
                required:true,
            },
            user_type:{
                required:true
            }
        }

     });
</script>

@endsection
@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection


@section('title')
<title>Best Constructions in {{ @$provider->userCity->name }}, Construction services {{ @$provider->userCity->name }}  | {{ @$provider->name }} </title>
<meta name="description" content="We gives best services for any type of constructions like commercial and residential buildings - {{ @$provider->name }}" />
<meta name="keywords" content="Constructions in {{ @$provider->userCity->name }}, Best Construction services {{ @$provider->userCity->name }}, Commercial Building Constructions, Residential Building Constructions , {{ @$provider->name }}" />
<meta property="og:title" content="Best Constructions in {{ @$provider->userCity->name }} | RiVirtual - {{ @$provider->name }} " />
<meta property="og:description" content="We gives best services for any type of constructions like commercial and residential buildings.." />
<meta property="og:site_name" content="rivirtual.com" />
@endsection




@section('header')
@include('includes.header')
@endsection

@section('content')
<div class="haeder-padding"></div>

<div class="wraper">
	<section >
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="white-backgrund new_pro_divs">
						<div class="row">

							<div class="col-lg-9 col-xl-9 col-sm-12 padd-0">
								<div class="ser-pro-info">
									<div class="ser-pro-details">
										<div class="ser-pro-img">
											<em>
                                            @if($provider->profile_pic != null)
                                                    <img src="{{ URL::to('storage/app/public/profile_picture') }}/{{ $provider->profile_pic }}">
                                                    @else
													<img src="{{ url('public/frontend/images/pro1.png') }}">
                                                    @endif
												{{--<img src="images/ser-pro.png" alt="provider-image">--}}
											</em>
										</div>
										<div class="ser-pro-name">
											<div class="d-flex align-items-center"><h5>{{ $provider->name }}</h5> <span><img src="{{ url('public/frontend/images/w-star.png') }}"> 
											@if(@$provider->avg_review>=0.75 && @$provider->avg_review<=1.24)
											@php
											 $ii = 1;
											 @endphp
											{{ $ii }}
											@elseif(@$provider->avg_review>=1.25 && @$provider->avg_review<=1.74)
											@php
											 $ii = 1.5;
											 @endphp
											{{ $ii }}
											@elseif(@$provider->avg_review>=1.75 && @$provider->avg_review<=2.24)
											@php
											 $ii = 2;
											 @endphp
											{{ $ii }}
											@elseif(@$provider->avg_review>=2.25 && @$provider->avg_review<=2.74)
											@php
											 $ii = 2.5;
											 @endphp
											{{ $ii }}
											@elseif(@$provider->avg_review>=2.75 && @$provider->avg_review<=3.24)
											@php
											 $ii = 3;
											 @endphp
											{{ $ii }}
											@elseif(@$provider->avg_review>=3.25 && @$provider->avg_review<=3.74)
											@php
											 $ii = 3.5;
											 @endphp
											{{ $ii }}
											@elseif(@$provider->avg_review>=3.75 && @$provider->avg_review<=4.24)
											@php
											 $ii = 4;
											 @endphp
											{{ $ii }}
											@elseif(@$provider->avg_review>=4.25 && @$provider->avg_review<=4.74)
											@php
											 $ii = 4.5;
											 @endphp
											{{ $ii }}
											@elseif(@$provider->avg_review>=4.75 && @$provider->avg_review<=5.24)
											@php
											 $ii = 5;
											 @endphp
											{{ $ii }}
											@else
											{{ number_format(@$provider->avg_review,1,'.',',')  }}
											@endif
										    </span></div>
											<p>{{ @$provider->proToCategory?@$provider->proToCategory->categoryName->category_name:'' }}</p>
											<ul>
												<li>
													<img src="{{ url('public/frontend/images/brief.png') }}"><span> {{ $provider->experience }} years</span>
												</li>
												<li>
													<img src="{{ url('public/frontend/images/lan.png') }}">

                                                    @if(!@$provider->providerToLanguage->isEmpty())

													 @php
                                                      
													 $i = count($provider->providerToLanguage)-1;
													 @endphp

													  @foreach($provider->providerToLanguage as $key=>$language)

													  <span>{{ $language->userLanguage->name }}</span>@if($key == $i) @else ,@endif
												     @endforeach
													 @endif
												</li>
											</ul>
											@if(Auth::user() && Auth::user()->user_type == 'U')
											
											<!-- <a href="javascript:;" class="msg-me chat_btn" data-userid="{{@Auth::user()->id}}" data-agentid="{{@$propertyDetails->propertyUser->id}}" data-name="{{@$propertyDetails->propertyUser->name}}" data-username="{{@Auth::user()->name}}" data-usertype="P" data-propertyid="{{@$provider->id}}" data-providerid="{{@$provider->id}}" data-bidid=0>Message Me</a> -->

											<a href="javascript:;" class="msg-me chat_btn" data-userid="{{@Auth::user()->id}}" data-agentid="{{@$provider->id}}" data-name="{{@$provider->name}}" data-username="{{@Auth::user()->name}}"  data-propertyid="0" data-providerid="{{@$provider->id}}" data-usertype="P">Message Me</a>

											@else
											<a href="javascript:;" class="msg-me snd_msg">Message Me</a>
											@endif
											<!-- <a href="#" class="msg-me">Message Me</a> -->
										</div>
									</div>
									<div class="ser-pro-socail">
										<div class="pro-budget">
											<p>
                                                  @if(@$provider->budget_type == 'H')
                                                  ₹{{ @$provider->budget }}/hr
                                                   @elseif(@$provider->budget_type == 'F')
                                                   ₹{{ @$provider->budget }}/Fixed
												   @elseif(@$provider->budget_type == 'D')
												   ₹{{ @$provider->budget }}/day
                                                         @endif
                                            </p>
										</div>
										<div class="pro-social">
											<p>Share :</p>
											<div class="addthis_inline_share_toolbox"></div>
											<!-- <ul>
												<li>
													<a href="#"><img src="{{ url('public/frontend/images/facebook.png') }}"></a>
												</li>
												<li>
													<a href="#"><img src="{{ url('public/frontend/images/twitter2.png') }}"></a>
												</li>
												<li>
													<a href="#"><img src="{{ url('public/frontend/images/in.png') }}"></a>
												</li>
												<li>
													<a href="#"><img src="{{ url('public/frontend/images/plus.png') }}"></a>
												</li>
											</ul> -->
										</div>
									</div>
								</div>
							</div>

							<div class="col-lg-3 col-xl-3 col-sm-12">
								@if(Auth::user())
								<div class="ser-pro-contact">
								<ul>
									
										<li><img src="{{ url('public/frontend/images/map2.png') }}"> <span> 
											@if(strlen(@$provider->address)>30)
											 {!! substr(@$provider->address,0,30) . '...' !!}
											 @else

											{!! @$provider->address !!}
											@endif 
										    </span></li>

										<li><a href="javascript:;"><img src="{{ url('public/frontend/images/mail.png') }}"> <span class="view_email">View Email</span>
										 <strong class="show_email">
										{{ @$provider->email }}</strong></a>
									    </li>

										<li><a href="javascript:;"><img src="{{ url('public/frontend/images/call.png') }}"> <span class="view_noo">View Phone Number</span> 
										<strong class="show_noo">
										+91 {{ @$provider->mobile_number }}</strong></a>
									     </li>
	                                     @if(@$provider->whatsapp_no != null)
										<li><a href="javascript:;"><img src="{{ url('public/frontend/images/whats.png') }}"> <span class="view_whatsapp">View WhatsApp </span>
										 <strong class="show_whatsapp">
										+91 {{ @$provider->whatsapp_no }}</strong></a>
									    </li>
	                                      @endif
	                                
									</ul>
								</div>
								@endif
							</div>
							@if(@$proSave)
							<div class="fav-pro fav-pro-save">
								<a @if(Auth::user() && Auth::user()->user_type == 'U') href="{{  route('provider.save',['id'=>@$provider->id]) }}" @else href="javascript:;" @endif @if(!Auth::user()) onclick="alert('Please login with continue')" @endif> <i class="fa fa-solid fa-bookmark"></i></a>
							</div>
							@else
							<div class="fav-pro">
								<a @if(Auth::user() && Auth::user()->user_type == 'U') href="{{  route('provider.save',['id'=>@$provider->id]) }}" @else href="javascript:;" @endif @if(!Auth::user()) onclick="alert('Please login with continue')" @endif> <i class="fa fa-bookmark-o"></i></a>
							</div>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>	

	<section >
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="white-backgrund">
						<div class="about-sec-pro">
							<h5>About Me :</h5>
							
							<p>
							@if(strlen(@$provider->about)>150)
							{!! substr(@$provider->about,0,150) . '...' !!}<br>
							<p class="moretext">
							{!! @$provider->about !!}
	                        </p>
                            <a class="moreless-button">Read More +</a>
							@else
							
							{!! @$provider->about !!}
							@endif
						 </p>
							
						
							{{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
								 Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
								  when an unknown printer took a galley of type and electronic typesetting, 
								  remaining essentially unchanged. 
								  It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</p>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
								 Lorem Ipsum has been the of Letraset sheets containing Lorem Ipsum passages..
							</p>--}}
							
		
	                        {{--<a class="moreless-button">Read More +</a>--}}
							
	                      </div>
                      
	                      <div class="about-sec-pro">
	                      	<h5>Skills :</h5>
	                      	<ul class="skills">
								  @if(!@$provider->providerSkill->isEmpty())
								   @foreach($provider->providerSkill as $skill)
								    
	                      		<li>{{ $skill->userSkill->skill_name }}</li>
								  @endforeach
								  @endif
	                      		{{--<li>Carpenters</li>
	                      		<li>Pest Control</li>
	                      		<li>Refrigerator</li>
	                      		<li>Alarm Systems</li>
	                      		<li>Technician</li>
	                      		<li>Solar</li>
	                      		<li>Filtering Professional</li>
	                      		<li>Pest Control</li>
	                      		<li>Carpenters</li>
	                      		<li>Refrigerator</li>
	                      		<li>Pest Control</li>
	                      		<li>Filtering Professional</li>--}}
	                      	</ul>
	                      </div>

	                      {{--<div class="about-sec-pro">
	                      	<h5>Certification :</h5>
	                      	<ul class="certification">
	                      		<li><img src="{{ url('public/frontend/images/cera.png') }}"> <span>Certificate One</span></li>
	                      		<li><img src="{{ url('public/frontend/images/cera.png') }}"> <span>Certificate Two</span></li>
	                      		
	                      	</ul>
	                      </div>
	                      <hr class="dash-o">

	                      <div class="about-sec-pro">
	                      	<h5>Experience :</h5>
	                      	<ul >
	                      		<li><p>Real estate Pvt. Ltd. - Bengalore - <span>2020 - 2021</span></p></li>
	                      		<li><p>Real estate new enterprise Pvt. Ltd. - Maharashtra - <span>2018 - 2020 </span></p></li>
	                      		<li><p>Real estate new enterprise Pvt. Ltd. - Hyderabad - <span>2014 - 2018 </span></p></li>
	                      		
	                      	</ul>
	                      </div>--}}
						  {{--@if(Auth::user())
							 <div class="revBtn">
								 <a href="javascript:;" class="see_cc" data-toggle="modal" data-target="#exampleModal">
									 Rate This Pro
								 </a>
							 </div>
							 @else
							 <div class="revBtn">
								 <a href="javascript:;" class="see_cc" onclick="alert('Please login with continue')">
									 Rate This Pro
								 </a>
							 </div>
							
							 @endif--}}
						</div>
					</div>
				</div>
			
		</div>
	</section>

	<section >
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="white-backgrund">
						<div class="about-sec-pro">
							<h5>Gallery :</h5>
							<div class="progallery">
								<div class="owl-carousel" id="workimage">
									@foreach($workImages as $image)
									<div class="item">
										@foreach($image as $item)
										<div class="pro-gallery">
											<em><img src="{{ URL::to('storage/app/public/work_image') }}/{{ @$item->image }}"></em>
										</div>
										@endforeach
										<!-- <div class="pro-gallery">
											<em><img src="{{ url('public/frontend/images/ga2.png') }}"></em>
										</div> -->
									</div>
									@endforeach
									{{--<div class="item">
										<div class="pro-gallery">
											<em><img src="{{ url('public/frontend/images/ga3.png') }}"></em>
										</div>
										<div class="pro-gallery">
											<em><img src="{{ url('public/frontend/images/ga4.png') }}"></em>
										</div>
									</div>
									<div class="item">
										<div class="pro-gallery">
											<em><img src="{{ url('public/frontend/images/ga5.png') }}"></em>
										</div>
										<div class="pro-gallery">
											<em><img src="{{ url('public/frontend/images/ga6.png') }}"></em>
										</div>
									</div>--}}
					
								</div>
							</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section >
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="white-backgrund">
						<div class="about-sec-pro">
							<h5>Reviews :</h5>
						</div>
						<div class="review_person">
							<div class="row">
								@if(@$allReviews)
								 @foreach(@$allReviews as $review)
								<div class="col-md-6">
									<div class="review_per_item pt-0">
										<div class="media">
											<em>
											@if(@$review->userDetails->profile_pic != null)
											<img src="{{ URL::to('storage/app/public/profile_picture')}}/{{ @$review->userDetails->profile_pic }}" alt="">
											@else
											<img src="{{ URL::asset('public/frontend/images/revew1.png')}}" alt="">
											@endif
											   </em>
											<div class="media-body">
												<h2>{{ @$review->userDetails->name }}</h2>
												<ul>
													<li>
												     	@for($i=1; $i<=$review->review_point; $i++)
														<span><img src="{{ url('public/frontend/images/y-star.png') }}" alt=""></span>
														@endfor
														@for($j=$review->review_point+1; $j<=5; $j++)
									
														<span><img src="{{ url('public/frontend/images/g-star.png') }}" alt=""></span>
														@endfor
													</li>
													<li class="da-rat">
														<i><img src="images/calendar.png" alt=""></i>
														<strong>{{date('jS M,Y',strtotime(@$review->created_at))}}</strong>
													</li>
												</ul>
											</div>
										</div>
										<p>
										{{ @$review->review_text }}
										{{--<span class="moretext2">
				                          Blue sapphire is a very cold and extremely powerful gem stone and represents planet Saturn. It blesses with immense good luck, wealth and prosperity. 
				                        </span> <a class="moreless-button2 allread">Read More +</a>--}}

										</p>
										
									</div>
								</div>
								@endforeach
								 @endif
								{{--<div class="col-md-6">
									<div class="review_per_item pt-0">
										<div class="media">
											<em><img src="{{ url('public/frontend/images/reviewimg1.jpg') }}" alt=""></em>
											<div class="media-body">
												<h2>Akashbev Roys</h2>
												<ul>
													<li>
													<span><img src="{{ url('public/frontend/images/y-star.png') }}" alt=""></span>
														<span><img src="{{ url('public/frontend/images/y-star.png') }}" alt=""></span>
														<span><img src="{{ url('public/frontend/images/y-star.png') }}" alt=""></span>
														<span><img src="{{ url('public/frontend/images/y-star.png') }}" alt=""></span>
														<span><img src="{{ url('public/frontend/images/g-star.png') }}" alt=""></span>
													</li>
													<li class="da-rat">
														<i><img src="images/calendar.png" alt=""></i>
														<strong>20th Sept, 2021</strong>
													</li>
												</ul>
											</div>
										</div>
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the of Letraset sheets containing  It was popularIpsum passages, Lorem Ipsum... 
										<span class="moretext8">
				                          Blue sapphire is a very cold and extremely powerful gem stone and represents planet Saturn. It blesses with immense good luck, wealth and prosperity. 
				                        </span> <a class="moreless-button8 allread">Read More +</a>

										</p>
										
									</div>
								</div>
								<div class="col-md-6">
									<div class="review_per_item ">
										<div class="media">
											<em><img src="{{ url('public/frontend/images/reviewimg1.jpg') }}" alt=""></em>
											<div class="media-body">
												<h2>Akashbev Roys</h2>
												<ul>
													<li>
													<span><img src="{{ url('public/frontend/images/y-star.png') }}" alt=""></span>
														<span><img src="{{ url('public/frontend/images/y-star.png') }}" alt=""></span>
														<span><img src="{{ url('public/frontend/images/y-star.png') }}" alt=""></span>
														<span><img src="{{ url('public/frontend/images/y-star.png') }}" alt=""></span>
														<span><img src="{{ url('public/frontend/images/g-star.png') }}" alt=""></span>
													</li>
													<li class="da-rat">
														<i><img src="images/calendar.png" alt=""></i>
														<strong>20th Sept, 2021</strong>
													</li>
												</ul>
											</div>
										</div>
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the of Letraset sheets containing  It was popularIpsum passages, Lorem Ipsum... 
										<span class="moretext7">
				                          Blue sapphire is a very cold and extremely powerful gem stone and represents planet Saturn. It blesses with immense good luck, wealth and prosperity. 
				                        </span> <a class="moreless-button7 allread">Read More +</a>

										</p>
										
									</div>
								</div>
								<div class="col-md-6">
									<div class="review_per_item ">
										<div class="media">
											<em><img src="{{ url('public/frontend/images/reviewimg1.jpg') }}" alt=""></em>
											<div class="media-body">
												<h2>Akashbev Roys</h2>
												<ul>
													<li>
													<span><img src="{{ url('public/frontend/images/y-star.png') }}" alt=""></span>
														<span><img src="{{ url('public/frontend/images/y-star.png') }}" alt=""></span>
														<span><img src="{{ url('public/frontend/images/y-star.png') }}" alt=""></span>
														<span><img src="{{ url('public/frontend/images/y-star.png') }}" alt=""></span>
														<span><img src="{{ url('public/frontend/images/g-star.png') }}" alt=""></span>
													</li>
													<li class="da-rat">
														<i><img src="images/calendar.png" alt=""></i>
														<strong>20th Sept, 2021</strong>
													</li>
												</ul>
											</div>
										</div>
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the of Letraset sheets containing  It was popularIpsum passages, Lorem Ipsum... 
										<span class="moretext4">
				                          Blue sapphire is a very cold and extremely powerful gem stone and represents planet Saturn. It blesses with immense good luck, wealth and prosperity. 
				                        </span> <a class="moreless-button4 allread">Read More +</a>

										</p>
										
									</div>
								</div>
								<div class="col-md-6">
									<div class="review_per_item ">
										<div class="media">
											<em><img src="{{ url('public/frontend/images/reviewimg1.jpg') }}" alt=""></em>
											<div class="media-body">
												<h2>Akashbev Roys</h2>
												<ul>
													<li>
													<span><img src="{{ url('public/frontend/images/y-star.png') }}" alt=""></span>
														<span><img src="{{ url('public/frontend/images/y-star.png') }}" alt=""></span>
														<span><img src="{{ url('public/frontend/images/y-star.png') }}" alt=""></span>
														<span><img src="{{ url('public/frontend/images/y-star.png') }}" alt=""></span>
														<span><img src="{{ url('public/frontend/images/g-star.png') }}" alt=""></span>
													</li>
													<li class="da-rat">
														<i><img src="images/calendar.png" alt=""></i>
														<strong>20th Sept, 2021</strong>
													</li>
												</ul>
											</div>
										</div>
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the of Letraset sheets containing  It was popularIpsum passages, Lorem Ipsum... 
										<span class="moretext5">
				                          Blue sapphire is a very cold and extremely powerful gem stone and represents planet Saturn. It blesses with immense good luck, wealth and prosperity. 
				                        </span> <a class="moreless-button5 allread">Read More +</a>

										</p>
										
									</div>
								</div>
								<div class="col-md-6">
									<div class="review_per_item ">
										<div class="media">
											<em><img src="{{ url('public/frontend/images/reviewimg1.jpg') }}" alt=""></em>
											<div class="media-body">
												<h2>Akashbev Roys</h2>
												<ul>
													<li>
													<span><img src="{{ url('public/frontend/images/y-star.png') }}" alt=""></span>
														<span><img src="{{ url('public/frontend/images/y-star.png') }}" alt=""></span>
														<span><img src="{{ url('public/frontend/images/y-star.png') }}" alt=""></span>
														<span><img src="{{ url('public/frontend/images/y-star.png') }}" alt=""></span>
														<span><img src="{{ url('public/frontend/images/g-star.png') }}" alt=""></span>
													</li>
													<li class="da-rat">
														<i><img src="images/calendar.png" alt=""></i>
														<strong>20th Sept, 2021</strong>
													</li>
												</ul>
											</div>
										</div>
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the of Letraset sheets containing  It was popularIpsum passages, Lorem Ipsum... 
										<span class="moretext6">
				                          Blue sapphire is a very cold and extremely powerful gem stone and represents planet Saturn. It blesses with immense good luck, wealth and prosperity. 
				                        </span> <a class="moreless-button6 allread">Read More +</a>

										</p>
										
									</div>
								</div>--}}

					@if(count(@$hideReviews)>6)
					<div class="moretext15" style="display: none;">
						<div class="row">
						@if(@$hideReviews)
							 @foreach(@$hideReviews as $key=>$review)
							<div class="col-md-6">
									<div class="review_per_item ">
										<div class="media">
											<em>
											@if(@$review->userDetails->profile_pic != null)
											<img src="{{ URL::to('storage/app/public/profile_picture')}}/{{ @$review->userDetails->profile_pic }}" alt="">
											@else
											<img src="{{ URL::asset('public/frontend/images/revew1.png')}}" alt="">
											@endif
											 </em>
											<div class="media-body">
												<h2>{{ @$review->userDetails->name }}</h2>
												<ul>
													<li>
													@for($i=1; $i<=$review->review_point; $i++)
														<span><img src="{{ url('public/frontend/images/y-star.png') }}" alt=""></span>
														@endfor
														@for($j=$review->review_point+1; $j<=5; $j++)
									
														<span><img src="{{ url('public/frontend/images/g-star.png') }}" alt=""></span>
														@endfor
													</li>
													<li class="da-rat">
														<i><img src="images/calendar.png" alt=""></i>
														<strong>{{date('jS M,Y',strtotime(@$review->created_at))}}</strong>
													</li>
												</ul>
											</div>
										</div>
									<p>
									{{ @$review->review_text }}	
									{{--<span class="moretext14">
			                          Blue sapphire is a very cold and extremely powerful gem stone and represents planet Saturn. It blesses with immense good luck, wealth and prosperity. 
			                        </span> <a class="moreless-button14 allread">Read More +</a>--}}

									</p>
									
								</div>
							</div>
							@endforeach
							 @endif
							{{--<div class="col-md-6">
									<div class="review_per_item ">
										<div class="media">
											<em><img src="{{ url('public/frontend/images/reviewimg1.jpg') }}" alt=""></em>
											<div class="media-body">
												<h2>Akashbev Roys</h2>
												<ul>
													<li>
													<span><img src="{{ url('public/frontend/images/y-star.png') }}" alt=""></span>
														<span><img src="{{ url('public/frontend/images/y-star.png') }}" alt=""></span>
														<span><img src="{{ url('public/frontend/images/y-star.png') }}" alt=""></span>
														<span><img src="{{ url('public/frontend/images/y-star.png') }}" alt=""></span>
														<span><img src="{{ url('public/frontend/images/g-star.png') }}" alt=""></span>
													</li>
													<li class="da-rat">
														<i><img src="images/calendar.png" alt=""></i>
														<strong>20th Sept, 2021</strong>
													</li>
												</ul>
											</div>
										</div>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing eiusmod tempor incididuntl aibore dolore magna aliqua.labor consectetur adipi lorem ipsum dolor sit amet
									<span class="moretext18">
			                          Blue sapphire is a very cold and extremely powerful gem stone and represents planet Saturn. It blesses with immense good luck, wealth and prosperity. 
			                        </span> <a class="moreless-button18 allread">Read More +</a>

									</p>
								</div>
							</div>--}}
						</div>
					</div>


								<a class="moreless-button15  show_more">Show More Reviews</a>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


</div>

@endsection

@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection

@section('script')
@include('includes.script')
@include('includes.toaster')
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-613782201f0abe3d"></script>
<script>
$(document).ready(function(){
    $(".view_noo").click(function(){
        $(".show_noo").show();
    });
	$(".view_noo").click(function(){
		$(".view_noo").hide();
	});
});
</script> 

<script>
$(document).ready(function(){
    $(".view_email").click(function(){
        $(".show_email").show();
    });
	$(".view_email").click(function(){
		$(".view_email").hide();
	});
});
</script> 

<script>
$(document).ready(function(){
    $(".view_whatsapp").click(function(){
        $(".show_whatsapp").show();
    });
	$(".view_whatsapp").click(function(){
		$(".view_whatsapp").hide();
	});
});
$(document).ready(function() {
    var owl = $('.progallery .owl-carousel');
    owl.owlCarousel({
      margin: 12,
      autoplay: true,

    //    center: true,
      
    //    autoplayTimeout: 3500,
      
     // nav: true,
      
    //   autoplayHoverPause:true,
      
       loop: false,
      responsive: {
        0: {
          items: 1
        },
        480: {
          items: 1
        },
       576: {
          items: 2
        },
        991: {
          items: 2
        },
        1000: {
          items: 3
        },
        1300: {
          items: 3
        }
      }
    })
  })
</script> 
<script>
	  $('.snd_msg').click(function(){

	    
	      alert('Please Login as User');
	   

	  });
</script>


@endsection
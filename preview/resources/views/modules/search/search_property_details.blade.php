@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
.block_day{
    background: #f6f6f6;
    position: relative;
}
.block_day:before{
    position: absolute;
    content: "";
    margin-top: -4px;
    width:3px;
    height: 80%;
    background: #ec8b8b;
    -ms-transform: rotate(45deg);
    -webkit-transform: rotate(45deg);
    transform: rotate(45deg);
}
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
    .floating-label .error {
    position: inherit;
    top: 0px!important;
    display: block;
    margin-bottom: 10px;
}
</style>
<link rel="canonical" href="https://www.rivirtual.com/" />
@endsection

@if(@$propertyDetails->master_property_type == 'RES' && @$propertyDetails->property_for == 'B')
@section('title')
<title>{{@$propertyDetails->name}} - @if(@$propertyDetails->property_type == 'F') Flat @elseif(@$propertyDetails->property_type == 'H')
	House @elseif(@$propertyDetails->property_type == 'L') Land @elseif(@$propertyDetails->property_type == 'FRMH') Farmhouse
	@elseif(@$propertyDetails->property_type == 'GTCM') Gated Community @endif for sale
	in {{@$propertyDetails->cityName->name}} - {{ intval(@$propertyDetails->area) }}sqft| RiVirtual </title>
	<meta name="Description" content="Best Luxury @if(@$propertyDetails->property_type == 'F') Flat @elseif(@$propertyDetails->property_type == 'H') House @elseif(@$propertyDetails->property_type == 'L') Land @elseif(@$propertyDetails->property_type == 'FRMH') Farmhouse @elseif(@$propertyDetails->property_type == 'GTCM') Gated Community @endif  for sale in {{@$propertyDetails->cityName->name}}. We provide affordable price with best amenities. - {{@$propertyDetails->name}}" />
	<meta name="keywords" content="@if(@$propertyDetails->property_type == 'F') Flat @elseif(@$propertyDetails->property_type == 'H') House @elseif(@$propertyDetails->property_type == 'L') Land 
	 @elseif(@$propertyDetails->property_type == 'FRMH') Farmhouse @elseif(@$propertyDetails->property_type == 'GTCM') Gated Community @endif 
	 for Sale {{@$propertyDetails->cityName->name}}, Luxury @if(@$propertyDetails->property_type == 'F') Flat @elseif(@$propertyDetails->property_type == 'H') House 
	 @elseif(@$propertyDetails->property_type == 'L') Land @elseif(@$propertyDetails->property_type == 'FRMH') Farmhouse 
	 @elseif(@$propertyDetails->property_type == 'GTCM') Gated Community @endif in {{@$propertyDetails->cityName->name}}, @if(@$propertyDetails->property_type == 'F') Flat 
	 @elseif(@$propertyDetails->property_type == 'H') House @elseif(@$propertyDetails->property_type == 'L') Land @elseif(@$propertyDetails->property_type == 'FRMH') Farmhouse @elseif(@$propertyDetails->property_type == 'GTCM') Gated Community @endif nearby {{@$propertyDetails->cityName->name}}, Residential @if(@$propertyDetails->property_type == 'F') Flat @elseif(@$propertyDetails->property_type == 'H') House @elseif(@$propertyDetails->property_type == 'L') Land @elseif(@$propertyDetails->property_type == 'FRMH') Farmhouse @elseif(@$propertyDetails->property_type == 'GTCM') Gated Community @endif for Sale {{@$propertyDetails->cityName->name}} , {{@$propertyDetails->name}}" />
{{--<title> RiVirtual |  {{@$propertyDetails->name}} </title>--}}
<meta property="og:title" content="@if(@$propertyDetails->property_type == 'F') Flat @elseif(@$propertyDetails->property_type == 'H')
	House @elseif(@$propertyDetails->property_type == 'L') Land @elseif(@$propertyDetails->property_type == 'FRMH') Farmhouse
	@elseif(@$propertyDetails->property_type == 'GTCM') Gated Community @endif for Sale in {{@$propertyDetails->cityName->name}} - {{ intval(@$propertyDetails->area) }}sqft | RiVirtual - {{@$propertyDetails->name}} " />
  <meta property="og:url" content="{{route('search.property.details',['slug'=>@$propertyDetails->slug])}}" />
  <meta property="og:image" content="{{ URL::asset('storage/app/public/property_image')}}/{{@$propertyDetails->propertyImageMain->image}}" />
  <meta property="og:description" content="Best Luxury @if(@$propertyDetails->property_type == 'F') Flat @elseif(@$propertyDetails->property_type == 'H') House @elseif(@$propertyDetails->property_type == 'L') Land @elseif(@$propertyDetails->property_type == 'FRMH') Farmhouse @elseif(@$propertyDetails->property_type == 'GTCM') Gated Community @endif for sale in {{@$propertyDetails->cityName->name}}. We provide affordable price with best amenities" />
  <meta property="og:site_name" content="rivirtual.com" />
@endsection
@elseif(@$propertyDetails->master_property_type == 'RES' && @$propertyDetails->property_for == 'R')
@section('title')
<title>{{@$propertyDetails->name}} - @if(@$propertyDetails->property_type == 'F') Flat @elseif(@$propertyDetails->property_type == 'H')
	House @elseif(@$propertyDetails->property_type == 'L') Land @elseif(@$propertyDetails->property_type == 'FRMH') Farmhouse
	@elseif(@$propertyDetails->property_type == 'GTCM') Gated Community @endif for rent
	in {{@$propertyDetails->cityName->name}} Residential Lease in {{@$propertyDetails->cityName->name}} - {{ intval(@$propertyDetails->area) }}sqft| RiVirtual </title>
	<meta name="Description" content="Best Residential @if(@$propertyDetails->property_type == 'F') Flat @elseif(@$propertyDetails->property_type == 'H')
	House @elseif(@$propertyDetails->property_type == 'L') Land @elseif(@$propertyDetails->property_type == 'FRMH') Farmhouse
	@elseif(@$propertyDetails->property_type == 'GTCM') Gated Community @endif for rent in {{@$propertyDetails->cityName->name}} with best facilities - {{@$propertyDetails->name}}" />
	<meta name="keywords" content="@if(@$propertyDetails->property_type == 'F') Flat @elseif(@$propertyDetails->property_type == 'H')
	House @elseif(@$propertyDetails->property_type == 'L') Land @elseif(@$propertyDetails->property_type == 'FRMH') Farmhouse
	@elseif(@$propertyDetails->property_type == 'GTCM') Gated Community @endif for Rent in {{@$propertyDetails->cityName->name}}, Residential Lease in {{@$propertyDetails->cityName->name}}, Residential @if(@$propertyDetails->property_type == 'F') Flat @elseif(@$propertyDetails->property_type == 'H')
	House @elseif(@$propertyDetails->property_type == 'L') Land @elseif(@$propertyDetails->property_type == 'FRMH') Farmhouse
	@elseif(@$propertyDetails->property_type == 'GTCM') Gated Community @endif Rent {{@$propertyDetails->cityName->name}} , {{@$propertyDetails->name}}" />
{{--<title> RiVirtual |  {{@$propertyDetails->name}} </title>--}}
<meta property="og:title" content="@if(@$propertyDetails->property_type == 'F') Flat @elseif(@$propertyDetails->property_type == 'H')
	House @elseif(@$propertyDetails->property_type == 'L') Land @elseif(@$propertyDetails->property_type == 'FRMH') Farmhouse
	@elseif(@$propertyDetails->property_type == 'GTCM') Gated Community @endif for Rent  in {{@$propertyDetails->cityName->name}} - {{ intval(@$propertyDetails->area) }}sqft | RiVirtual - {{@$propertyDetails->name}} " />
  <meta property="og:url" content="{{route('search.property.details',['slug'=>@$propertyDetails->slug])}}" />
  <meta property="og:image" content="{{ URL::asset('storage/app/public/property_image')}}/{{@$propertyDetails->propertyImageMain->image}}" />
  <meta property="og:description" content="Best Luxury @if(@$propertyDetails->property_type == 'F') Flat @elseif(@$propertyDetails->property_type == 'H')
	House @elseif(@$propertyDetails->property_type == 'L') Land @elseif(@$propertyDetails->property_type == 'FRMH') Farmhouse
	@elseif(@$propertyDetails->property_type == 'GTCM') Gated Community @endif for Rent in {{@$propertyDetails->cityName->name}}. We provide affordable price with best amenities" />
	<meta property="og:site_name" content="rivirtual.com" />
@endsection
@elseif(@$propertyDetails->master_property_type == 'COM' && @$propertyDetails->property_for == 'B')
@section('title')
<title>{{@$propertyDetails->name}} - @if(@$propertyDetails->property_type == 'OSP') Office Space @elseif(@$propertyDetails->property_type == 'WRH')
	Warehouse @elseif(@$propertyDetails->property_type == 'SHR') Showrooms @elseif(@$propertyDetails->property_type == 'INDT') Industrial
	@elseif(@$propertyDetails->property_type == 'RET') Retail @endif for sale
	in {{@$propertyDetails->cityName->name}}, Commercial @if(@$propertyDetails->property_type == 'OSP') Office Space @elseif(@$propertyDetails->property_type == 'WRH')
	Warehouse @elseif(@$propertyDetails->property_type == 'SHR') Showrooms @elseif(@$propertyDetails->property_type == 'INDT') Industrial
	@elseif(@$propertyDetails->property_type == 'RET') Retail @endif for sale in {{@$propertyDetails->cityName->name}} - {{ intval(@$propertyDetails->area) }}sqft| RiVirtual </title>
	<meta name="Description" content="We have best commercial spaces and @if(@$propertyDetails->property_type == 'OSP') Office Space @elseif(@$propertyDetails->property_type == 'WRH')
	Warehouse @elseif(@$propertyDetails->property_type == 'SHR') Showrooms @elseif(@$propertyDetails->property_type == 'INDT') Industrial
	@elseif(@$propertyDetails->property_type == 'RET') Retail @endif for sale in {{@$propertyDetails->cityName->name}} with a best facilities - {{@$propertyDetails->name}}" />
	<meta name="keywords" content="@if(@$propertyDetails->property_type == 'OSP') Office Space @elseif(@$propertyDetails->property_type == 'WRH')
	Warehouse @elseif(@$propertyDetails->property_type == 'SHR') Showrooms @elseif(@$propertyDetails->property_type == 'INDT') Industrial
	@elseif(@$propertyDetails->property_type == 'RET') Retail @endif for sale in {{@$propertyDetails->cityName->name}}, Commercial @if(@$propertyDetails->property_type == 'OSP') Office Space @elseif(@$propertyDetails->property_type == 'WRH')
	Warehouse @elseif(@$propertyDetails->property_type == 'SHR') Showrooms @elseif(@$propertyDetails->property_type == 'INDT') Industrial
	@elseif(@$propertyDetails->property_type == 'RET') Retail @endif for sale in {{@$propertyDetails->cityName->name}}, @if(@$propertyDetails->property_type == 'OSP') Office Space @elseif(@$propertyDetails->property_type == 'WRH')
	Warehouse @elseif(@$propertyDetails->property_type == 'SHR') Showrooms @elseif(@$propertyDetails->property_type == 'INDT') Industrial
	@elseif(@$propertyDetails->property_type == 'RET') Retail @endif for sale in {{@$propertyDetails->cityName->name}} , {{@$propertyDetails->name}}" />
{{--<title> RiVirtual |  {{@$propertyDetails->name}} </title>--}}
<meta property="og:title" content="@if(@$propertyDetails->property_type == 'OSP') Office Space @elseif(@$propertyDetails->property_type == 'WRH')
	Warehouse @elseif(@$propertyDetails->property_type == 'SHR') Showrooms @elseif(@$propertyDetails->property_type == 'INDT') Industrial
	@elseif(@$propertyDetails->property_type == 'RET') Retail @endif for Sale  in {{@$propertyDetails->cityName->name}} - {{ intval(@$propertyDetails->area) }}sqft | RiVirtual - {{@$propertyDetails->name}} " />
  <meta property="og:url" content="{{route('search.property.details',['slug'=>@$propertyDetails->slug])}}" />
  <meta property="og:image" content="{{ URL::asset('storage/app/public/property_image')}}/{{@$propertyDetails->propertyImageMain->image}}" />
  <meta property="og:description" content="Best @if(@$propertyDetails->property_type == 'OSP') Office Space @elseif(@$propertyDetails->property_type == 'WRH')
	Warehouse @elseif(@$propertyDetails->property_type == 'SHR') Showrooms @elseif(@$propertyDetails->property_type == 'INDT') Industrial
	@elseif(@$propertyDetails->property_type == 'RET') Retail @endif for sale in {{@$propertyDetails->cityName->name}}. We provide affordable price with best facilities" />
	<meta property="og:site_name" content="rivirtual.com" />
@endsection
@elseif(@$propertyDetails->master_property_type == 'COM' && @$propertyDetails->property_for == 'R')
@section('title')
<title>{{@$propertyDetails->name}} - @if(@$propertyDetails->property_type == 'OSP') Office Space @elseif(@$propertyDetails->property_type == 'WRH')
	Warehouse @elseif(@$propertyDetails->property_type == 'SHR') Showrooms @elseif(@$propertyDetails->property_type == 'INDT') Industrial
	@elseif(@$propertyDetails->property_type == 'RET') Retail @endif for rent
	in {{@$propertyDetails->cityName->name}},@if(@$propertyDetails->property_type == 'OSP') Office Space @elseif(@$propertyDetails->property_type == 'WRH')
	Warehouse @elseif(@$propertyDetails->property_type == 'SHR') Showrooms @elseif(@$propertyDetails->property_type == 'INDT') Industrial
	@elseif(@$propertyDetails->property_type == 'RET') Retail @endif Lease in {{@$propertyDetails->cityName->name}} - {{ intval(@$propertyDetails->area) }}sqft| RiVirtual </title>
	<meta name="Description" content="Best Commercial spaces and @if(@$propertyDetails->property_type == 'OSP') Office Space @elseif(@$propertyDetails->property_type == 'WRH')
	Warehouse @elseif(@$propertyDetails->property_type == 'SHR') Showrooms @elseif(@$propertyDetails->property_type == 'INDT') Industrial
	@elseif(@$propertyDetails->property_type == 'RET') Retail @endif for rent in {{@$propertyDetails->cityName->name}} with best facilities - {{@$propertyDetails->name}}" />
	<meta name="keywords" content="@if(@$propertyDetails->property_type == 'OSP') Office Space @elseif(@$propertyDetails->property_type == 'WRH')
	Warehouse @elseif(@$propertyDetails->property_type == 'SHR') Showrooms @elseif(@$propertyDetails->property_type == 'INDT') Industrial
	@elseif(@$propertyDetails->property_type == 'RET') Retail @endif Rent in {{@$propertyDetails->cityName->name}}, @if(@$propertyDetails->property_type == 'OSP') Office Space @elseif(@$propertyDetails->property_type == 'WRH')
	Warehouse @elseif(@$propertyDetails->property_type == 'SHR') Showrooms @elseif(@$propertyDetails->property_type == 'INDT') Industrial
	@elseif(@$propertyDetails->property_type == 'RET') Retail @endif Lease in {{@$propertyDetails->cityName->name}}, Commercial Space Rent {{@$propertyDetails->cityName->name}} , {{@$propertyDetails->name}}" />
{{--<title> RiVirtual |  {{@$propertyDetails->name}} </title>--}}
<meta property="og:title" content="@if(@$propertyDetails->property_type == 'OSP') Office Space @elseif(@$propertyDetails->property_type == 'WRH')
	Warehouse @elseif(@$propertyDetails->property_type == 'SHR') Showrooms @elseif(@$propertyDetails->property_type == 'INDT') Industrial
	@elseif(@$propertyDetails->property_type == 'RET') Retail @endif for Rent in {{@$propertyDetails->cityName->name}} - {{ intval(@$propertyDetails->area) }}sqft | RiVirtual - {{@$propertyDetails->name}} " />
  <meta property="og:url" content="{{route('search.property.details',['slug'=>@$propertyDetails->slug])}}" />
  <meta property="og:image" content="{{ URL::asset('storage/app/public/property_image')}}/{{@$propertyDetails->propertyImageMain->image}}" />
  <meta property="og:description" content="Best @if(@$propertyDetails->property_type == 'OSP') Office Space @elseif(@$propertyDetails->property_type == 'WRH')
	Warehouse @elseif(@$propertyDetails->property_type == 'SHR') Showrooms @elseif(@$propertyDetails->property_type == 'INDT') Industrial
	@elseif(@$propertyDetails->property_type == 'RET') Retail @endif for rent in {{@$propertyDetails->cityName->name}}. We provide affordable price with best facilities." />
	<meta property="og:site_name" content="rivirtual.com" />
@endsection
@endif




@section('header')
@include('includes.header')
@endsection


@section('content')
<!----header--->
<div class="haeder-padding"></div>
<div class="property_details_pg">
	<div class="container con_det_pg">
		<div class="property_details_inr">
			<div class="property_left">
				<div class="property_top">
					<div class="property_top_le">
						<h1>{{@$propertyDetails->name}}</h1>
						<ul>
							{{--<li><b><a href="#url" class="preowned">Preowned</a></b></li>--}}
							<li><img src="{{ URL::asset('public/frontend/images/property_icon1.png')}}" alt="">{{@$propertyDetails->localityName->locality_name}}</li>
							<li><img src="{{ URL::asset('public/frontend/images/property_icon2.png')}}" alt="">	  @if(@$propertyDetails->master_property_type == 'RES')
								Residential -
								@elseif(@$propertyDetails->master_property_type == 'COM')
								Commercial -
								@endif
                                @if(@$propertyDetails->property_type=='F')
                                Flat
                                @elseif(@$propertyDetails->property_type=='H')
                                House
                                @elseif(@$propertyDetails->property_type=='GTCM')
                                Gated Community
                                @elseif(@$propertyDetails->property_type=='FRMH')
                                Farmhouse
                                @elseif(@$propertyDetails->property_type=='L')
                                Land
                                @elseif(@$propertyDetails->property_type=='OSP')
                                Office Space
                                @elseif(@$propertyDetails->property_type=='WRH')
                                Warehouse
                                @elseif(@$propertyDetails->property_type=='SHR')
                                Showrooms
                                @elseif(@$propertyDetails->property_type=='INDT')
                                Industrial
                                @elseif(@$propertyDetails->property_type=='RET')
                                Retail
                                @endif
                            </li>
							<li><img src="{{ URL::asset('public/frontend/images/property_icon3.png')}}" alt="">
                                @if(@$propertyDetails->construction_status=='RM')
                                Ready to move
                                @elseif(@$propertyDetails->construction_status=='UC')
                                Under Construction
                                @elseif(@$propertyDetails->construction_status=='PL')
                                Pre Launch
                                @endif
                            </li>
						</ul>
					</div>
					<div class="property_top_rig">
						@if(@$propertyDetails->is_sold=='Y')
						<strong class="for_sale">Sold</strong>
						@else
                        @if(@$propertyDetails->property_for=='B')
						<strong class="for_sale">For Sale</strong>
                        @elseif(@$propertyDetails->property_for=='R')
						<strong class="for_sale">For Rent</strong>
						@elseif(@$propertyDetails->is_sold=='Y')
						@endif
                        @endif
                        <span>INR <b>

						 @php
							function count_digit($number) {

							  return strlen($number);
							}

							function divider($number_of_digits) {
							    $tens="1";

							  if($number_of_digits>8)
							    return 10000000;

							  while(($number_of_digits-1)>0)
							  {
							    $tens.="0";
							    $number_of_digits--;
							  }
							  return $tens;
							}
//function call
$num = intval(@$propertyDetails->budget_range_from);
$ext="";//thousand,lac, crore
$number_of_digits = count_digit($num); //this is call :)
    if($number_of_digits>3)
{
    if($number_of_digits%2!=0){
        $divider=divider($number_of_digits-1);
        
    }else
        $divider=divider($number_of_digits);
}
else
    $divider=1;

$fraction=$num/$divider;
$fraction=number_format($fraction,2);

if($number_of_digits==4 ||$number_of_digits==5)
    $ext="k";
if($number_of_digits==6 ||$number_of_digits==7)
    $ext="L";
if($number_of_digits==8 ||$number_of_digits==9)
    $ext="Cr";
if($number_of_digits >= 13)
	$ext="Cr";
echo $fraction." ".$ext;

@endphp
</b></span>
						<!-- <span>INR <b>{{ number_format(@$propertyDetails->budget_range_from, 0, '.', ',') }} - {{ number_format(@$propertyDetails->budget_range_to, 0, '.', ',') }}</b></span> -->
						<!--<span>INR <b>{{ number_format(@$propertyDetails->budget_range_from, 0, '.', ',') }}</b></span>-->
					</div>
				</div>
				<div class="mangi_glla_sec">
					<div class="ninja_trail_remove">
						<div class="left_slider_area">
							<div id="thumbnail-slider" style="float:left;">
								<div class="inner">
									<ul>
                                        @foreach ($propertyImageExterior as $image1)
										<li><a class="thumb" href="{{ URL::asset('storage/app/public/property_image')}}/{{$image1->image}}"></a></li>
                                        @endforeach
                                        @foreach ($propertyImageInterior as $image2)
										<li><a class="thumb" href="{{ URL::asset('storage/app/public/property_image')}}/{{$image2->image}}"></a></li>
                                        @endforeach
                                        @foreach ($propertyImageFloor as $image3)
										<li><a class="thumb" href="{{ URL::asset('storage/app/public/property_image')}}/{{$image3->image}}"></a></li>
                                        @endforeach
										{{-- <li><a class="thumb" href="{{ URL::asset('public/frontend/images/property_img22.jpg')}}"></a></li>
										<li><a class="thumb" href="{{ URL::asset('public/frontend/images/property_img33.jpg')}}"></a></li>
                                        <li><a class="thumb" href="{{ URL::asset('public/frontend/images/property_img44.jpg')}}"></a></li>
										<li><a class="thumb" href="{{ URL::asset('public/frontend/images/property_img55.jpg')}}"></a></li>
										<li><a class="thumb" href="{{ URL::asset('public/frontend/images/property_img66.jpg')}}"></a></li> --}}
									</ul>
								</div>
							</div>
							<div id="ninja-slider" style="float:left;">
								<div class="slider-inner">
									<ul>
                                        @foreach ($propertyImageExterior as $image1)
										<li><a class="ns-img" href="{{ URL::asset('storage/app/public/property_image')}}/{{$image1->image}}"></a></li>
                                        @endforeach
                                        @foreach ($propertyImageInterior as $image2)
										<li><a class="ns-img" href="{{ URL::asset('storage/app/public/property_image')}}/{{$image2->image}}"></a></li>
                                        @endforeach
                                        @foreach ($propertyImageFloor as $image3)
										<li><a class="ns-img" href="{{ URL::asset('storage/app/public/property_image')}}/{{$image3->image}}"></a></li>
                                        @endforeach
										{{-- <li><a class="ns-img" href="{{ URL::asset('public/frontend/images/list3.png')}}"></a></li>
										<li><a class="ns-img" href="{{ URL::asset('public/frontend/images/list4.png')}}"></a></li>
                                        <li><a class="ns-img" href="{{ URL::asset('public/frontend/images/list5.png')}}"></a></li>
										<li><a class="ns-img" href="{{ URL::asset('public/frontend/images/list6.png')}}"></a></li>
										<li><a class="ns-img" href="{{ URL::asset('public/frontend/images/list7.png')}}"></a></li> --}}
									</ul>
									@if(@$propertyDetails->propertySave)
									<a @if(Auth::user() && Auth::user()->user_type == 'U') href="{{  route('property.save',['id'=>@$propertyDetails->id]) }}" @else href="javascript:;" @endif @if(!Auth::user()) onclick="alert('Please login with continue')" @endif class="hhert_icon save-property-that"><i class="fa fa-heart" aria-hidden="true"></i></a>
									@else
                                    <a @if(Auth::user() && Auth::user()->user_type == 'U') href="{{  route('property.save',['id'=>@$propertyDetails->id]) }}" @else href="javascript:;" @endif @if(!Auth::user()) onclick="alert('Please login with continue')" @endif class="hhert_icon"><i class="fa fa-heart" aria-hidden="true"></i></a>
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="hom_det_sec">
					<div class="hom_det_bx hom_det_bx1">
						<div class="hom_det_it">
                            <h4 class="hom_det_h4">Home details</h4>
                            <ul class="hom_det_ul">
                                  @if(@$propertyDetails->property_type == 'F' || @$propertyDetails->property_type == 'H' || @$propertyDetails->property_type == 'GTCM')
                                <li><em><img src="{{ URL::asset('public/frontend/images/hom_icon1.png')}}" alt=""></em>Carpet area : <span>{{@$propertyDetails->carpet_area}} Sf</span></li>
                                @endif
								@if(@$propertyDetails->property_type == 'F' || @$propertyDetails->property_type == 'H' || @$propertyDetails->property_type == 'GTCM')
                                <li><em><img src="{{ URL::asset('public/frontend/images/hom_icon1.png')}}" alt=""></em>Super area : <span>{{@$propertyDetails->super_area}} Sf</span></li>
                                @endif
							 	@if(@$propertyDetails->build_year)<li><em><img src="{{ URL::asset('public/frontend/images/hom_icon2.png')}}" alt=""></em>Year Built : <span>{{@$propertyDetails->build_year}}</span></li>@endif
								 @if(@$propertyDetails->property_type == 'F' || @$propertyDetails->property_type == 'H' || @$propertyDetails->property_type == 'GTCM')
							 	<li><em><img src="{{ URL::asset('public/frontend/images/hom_icon1.png')}}" alt=""></em>Bedrooms : <span>{{@$propertyDetails->no_of_bedrooms}}</span></li>
							 	@elseif(@$propertyDetails->property_type == 'WRH' || @$propertyDetails->property_type == 'SHR' || @$propertyDetails->property_type == 'INDT' || @$propertyDetails->property_type == 'RET')
							 	<li><em><img src="{{ URL::asset('public/frontend/images/hom_icon1.png')}}" alt=""></em>Rooms : <span>{{@$propertyDetails->no_of_bedrooms}}</span></li>
								 @endif
								 @if(@$propertyDetails->property_type == 'O')
								 <li><em><img src="{{ URL::asset('public/frontend/images/ofc.png')}}" alt=""></em>Cabins : <span>{{@$propertyDetails->office_class}}</span></li>
								 @endif
								 @if(@$propertyDetails->property_type == 'F' || @$propertyDetails->property_type == 'H' || @$propertyDetails->property_type == 'GTCM' || @$propertyDetails->property_type == 'O' || @$propertyDetails->property_type == 'WRH' || @$propertyDetails->property_type == 'SHR' || @$propertyDetails->property_type == 'INDT' || @$propertyDetails->property_type == 'RET')
							 	<li><em><img src="{{ URL::asset('public/frontend/images/hom_icon3.png')}}" alt=""></em>Bathrooms : <span>{{@$propertyDetails->bathroom}}</span></li>
								 @endif
							 	<li><em><img src="{{ URL::asset('public/frontend/images/hom_icon4.png')}}" alt=""></em>Sqft : <span>{{@$propertyDetails->area}}</span></li>
							 </ul>
						</div>
						<div class="facil_panel">
                            <h4 class="hom_det_h4">Facilities / Amenities </h4>
                            <ul class="facil_ad">
                                @foreach ($propertyFacilitiesAmenities as $facilitiesAmenities)
                                <li>{{@$facilitiesAmenities->facilitiesAmenitiesName->name}}</li>
                                @endforeach
							 </ul>
						</div>
						<div class="description_panel">
							 <h4 class="hom_det_h4">Description</h4>
							 <p>
							 @if(strlen(@$propertyDetails->description)>150)
							{!! substr(@$propertyDetails->description,0,150) . '...' !!}<br>
							<p> 
							 <span class="moretext1" style="display: none;">{!! nl2br(@$propertyDetails->description) !!}</span><br>
							{{--<a href="#url" class="moreless-button1 more_cc">View All +</a>--}}
						  </p>
                            <a href="#url" class="moreless-button1 more_cc">Read More +</a>
							@else
							
							{!! nl2br(@$propertyDetails->description) !!}
							@endif
							 </p>
							 @if(Auth::user())
							 <div class="revBtn">
								 <a href="javascript:;" class="see_cc" data-toggle="modal" data-target="#exampleModal">
									 Rate This Property
								 </a>
							 </div>
							 @else
							 <div class="revBtn">
								 <a href="javascript:;" class="see_cc" onclick="alert('Please login with continue')">
									 Rate This Property
								 </a>
							 </div>
							
							 @endif
						</div>
						@if(@$propertyDetails->video_link)
						<div class="locti_panel locti_youtube_video">

							<h4 class="hom_det_h4">Youtube Video Link</h4>


							<iframe width="560" height="315" src="https://www.youtube.com/embed/{{@$propertyDetails->video_link}}?rel=0&amp;autoplay=1&amp;modestbranding=1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							
						</div>
						@endif

						<div class="locti_panel">
                            <h4 class="hom_det_h4">Location</h4>
                            <input type="hidden" name="city_name" id="city_name" value="{{@$propertyDetails->countName->name}},{{@$propertyDetails->stateName->name}},{{@$propertyDetails->cityName->name}},{{@$propertyDetails->address}}">
                            {{-- <span><img src="{{ URL::asset('public/frontend/images/property_icon1.png')}}" alt="">{{@$propertyDetails->address}}</span> --}}
                            <span><img src="{{ URL::asset('public/frontend/images/property_icon1.png')}}" alt="">{{@$propertyDetails->cityName->name}} , {{@$propertyDetails->stateName->name}}</span>
                             
                             <div id="map_div">
                             	
                             </div>
                             
							 
						</div>
					</div>
					<div class="hom_det_bx revew_sec revew_sec_agent">
						<h4 class="hom_det_h4">Reviews</h4>
						<div class="revew_panel">
							@if(@$allReviews)

							 @foreach(@$allReviews as $key=>$review)

							<div class="revew_item">
								<div class="revew_top">
									<div class="media">
										<span>
											@if(@$review->userDetails->profile_pic != null)
											<img src="{{ URL::to('storage/app/public/profile_picture')}}/{{ @$review->userDetails->profile_pic }}" alt="">
											@else
											<img src="{{ URL::asset('public/frontend/images/revew1.png')}}" alt="">
											@endif
										 </span>
										<div class="media-body">
											<h4>{{ @$review->userDetails->name }}</h4>
											<div class="star_bx">
										    <ul>
											@for($i=1; $i<=$review->review_point; $i++)
											<li><i class="fa fa-star"></i></li>
											@endfor
											@for($j=$review->review_point+1; $j<=5; $j++)
											<li><i class="fa fa-star gray"></i></li>
											@endfor
										  </ul>
										<strong><img src="{{ URL::asset('public/frontend/images/calen.png')}}" alt="">{{date('jS M,Y',strtotime(@$review->created_at))}} </strong>
									</div>
											
										</div>
									</div>
								
								</div>
								<p class="desc_bid" data-id="{{@$review->id}}">
								@if(strlen(@$review->review_text)>150)
				
									<p class=" short_desc_bid short_desc_bid_{{@$review->id}}" data-desc="{{substr(@$review->review_text, 0, 150)}}" data-id ="{{$review->id}}">
									<a href="javascript:;" class="read_more_bid" data-id="{{@$proposal->id}}"> Read more +</a>
									</p>
									<p class="full_desc_bid full_desc_bid_{{@$review->id}}"  data-desc="{{@$review->review_text}}" style="display:none;" data-id="{{$review->id}}">
																	

									<a href="javascript:;" class="read_more_bid" data-id="{{@$review->id}}"> Read less -</a>
									</p>
									@else
									<p>{{@$review->review_text}}</p>
								@endif
														  	
							</p>
							</div>
                            @endforeach
							@endif
							{{--<div class="revew_item">
								<div class="revew_top">
									<div class="media">
										<span><img src="{{ URL::asset('public/frontend/images/revew2.png')}}" alt=""></span>
										<div class="media-body">
											<h4>Saikat Roy</h4>
											<h5>Reviews title text show here</h5>
										</div>
									</div>
									<div class="star_bx">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star gray"></i></li>
										</ul>
										<strong><img src="{{ URL::asset('public/frontend/images/calen.png')}}" alt=""> 20th Sept, 2021</strong>
									</div>
								</div>
								<p>Lorem ipsum dolor sit amet consectetur the adipiscing elit iaculis magna dignissim facilisis felis nisl pellentesque libero, imperdiet neque ante ut nisi sapien neque sodales lacus auctor the pretium nibhfeugiat dui. Lorem ipsum dolor sit ametconsectetur.</p>
							</div>
							<div class="revew_item">
								<div class="revew_top">
									<div class="media">
										<span><img src="{{ URL::asset('public/frontend/images/revew3.png')}}" alt=""></span>
										<div class="media-body">
											<h4>Abhijheet Manna</h4>
											<h5>Simply dummy review heading</h5>
										</div>
									</div>
									<div class="star_bx">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star gray"></i></li>
											<li><i class="fa fa-star gray"></i></li>
											<li><i class="fa fa-star gray"></i></li>
										</ul>
										<strong><img src="{{ URL::asset('public/frontend/images/calen.png')}}" alt=""> 20th Sept, 2021</strong>
									</div>
								</div>
								<p>Lorem ipsum dolor sit amet consectetur the adipiscing elit iaculis magna dignissim facilisis felis nisl pellentesque libero, imperdiet neque ante ut nisi sapien neque sodales lacus.</p>
							</div>
							<div class="revew_item">
								<div class="revew_top">
									<div class="media">
										<span><img src="{{ URL::asset('public/frontend/images/revew4.png')}}" alt=""></span>
										<div class="media-body">
											<h4>Saikat Roy</h4>
											<h5>Reviews title text show here</h5>
										</div>
									</div>
									<div class="star_bx">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star gray"></i></li>
											<li><i class="fa fa-star gray"></i></li>
											<li><i class="fa fa-star gray"></i></li>
										</ul>
										<strong><img src="{{ URL::asset('public/frontend/images/calen.png')}}" alt=""> 20th Sept, 2021</strong>
									</div>
								</div>
								<p>Lorem ipsum dolor sit amet consectetur the adipiscing elit iaculis magna dignissim facilisis felis nisl pellentesque libero, imperdiet neque ante ut nisi sapien neque sodales lacus auctor the pretium nibhfeugiat dui. Lorem ipsum dolor sit ametconsectetur adipiscing elit  iaculis, magna. <span class="moretext1 moretext_all">Lorem ipsum dolor sit amet consectetur the adipiscing elit iaculis magna dignissim facilisis felis nisl pellentesque libero, imperdiet neque ante ut nisi sapien neque sodales lacus auctor the pretium nibhfeugiat dui. Lorem ipsum dolor sit ametconsectetur adipiscing elit  iaculis, </span>  <a href="#url" class="moreless-button1 more_cc">more +</a> </p>
							</div>
							<div class="revew_item">
								<div class="revew_top">
									<div class="media">
										<span><img src="{{ URL::asset('public/frontend/images/revew5.png')}}" alt=""></span>
										<div class="media-body">
											<h4>Abhijheet Manna</h4>
											<h5>This is a simply dummy review heading</h5>
										</div>
									</div>
									<div class="star_bx">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
										</ul>
										<strong><img src="{{ URL::asset('public/frontend/images/calen.png')}}" alt=""> 20th Sept, 2021</strong>
									</div>
								</div>
								<p>Lorem ipsum dolor sit amet consectetur the adipiscing elit iaculis magna dignissim facilisis felis nisl pellentesque libero, imperdiet neque ante ut nisi sapien neque sodales lacus auctor the pretium nibhfeugiasapien neque sodales lacus auctor the pretium nibhfeugia ametconsectetur.</p>
							</div>--}}
                            @if(count(@$hideReviews)>6)
							<div class="moretext_all moretext2">
							@if(@$hideReviews)
							 @foreach(@$hideReviews as $key=>$review)
							<div class="revew_item">
								<div class="revew_top">
									<div class="media">
										<span>
											@if(@$review->userDetails->profile_pic != null)
											<img src="{{ URL::to('storage/app/public/profile_picture')}}/{{ @$review->userDetails->profile_pic }}" alt="">
											@else
											<img src="{{ URL::asset('public/frontend/images/revew1.png')}}" alt="">
											@endif
										 </span>
										<div class="media-body">
											<h4>{{ @$review->userDetails->name }}</h4>
											<div class="star_bx">
										     <ul>
											@for($i=1; $i<=$review->review_point; $i++)
											<li><i class="fa fa-star"></i></li>
											@endfor
											@for($j=$review->review_point+1; $j<=5; $j++)
											<li><i class="fa fa-star gray"></i></li>
											@endfor
										   </ul>
										<strong><img src="{{ URL::asset('public/frontend/images/calen.png')}}" alt="">{{date('jS M,Y',strtotime(@$review->created_at))}} </strong>
									</div>
										</div>
									</div>
								
								</div>
								<p class="desc_bid" data-id="{{@$review->id}}">
								@if(strlen(@$review->review_text)>150)
				
									<p class=" short_desc_bid short_desc_bid_{{@$review->id}}" data-desc="{{substr(@$review->review_text, 0, 150)}}" data-id ="{{$review->id}}">
									<a href="javascript:;" class="read_more_bid" data-id="{{@$proposal->id}}"> Read more +</a>
									</p>
									<p class="full_desc_bid full_desc_bid_{{@$review->id}}"  data-desc="{{@$review->review_text}}" style="display:none;" data-id="{{$review->id}}">
																	

									<a href="javascript:;" class="read_more_bid" data-id="{{@$review->id}}"> Read less -</a>
									</p>
									@else
									<p>{{@$review->review_text}}</p>
								@endif
														  	
							</p>
							</div>
							@endforeach
							@endif
								{{--<div class="revew_item">
									<div class="revew_top">
										<div class="media">
											<span><img src="{{ URL::asset('public/frontend/images/revew4.png')}}" alt=""></span>
											<div class="media-body">
												<h4>Saikat Roy</h4>
												<h5>Reviews title text show here</h5>
											</div>
										</div>
										<div class="star_bx">
											<ul>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star gray"></i></li>
												<li><i class="fa fa-star gray"></i></li>
												<li><i class="fa fa-star gray"></i></li>
											</ul>
											<strong><img src="{{ URL::asset('public/frontend/images/calen.png')}}" alt=""> 20th Sept, 2021</strong>
										</div>
									</div>
									<p>Lorem ipsum dolor sit amet consectetur the adipiscing elit iaculis magna dignissim facilisis felis nisl pellentesque libero, imperdiet neque ante ut nisi sapien neque sodales lacus auctor the pretium nibhfeugiat dui. Lorem ipsum dolor sit ametconsectetur adipiscing elit  iaculis, magna. <span class="moretext1 moretext_all">Lorem ipsum dolor sit amet consectetur the adipiscing elit iaculis magna dignissim facilisis felis nisl pellentesque libero, imperdiet neque ante ut nisi sapien neque sodales lacus auctor the pretium nibhfeugiat dui. Lorem ipsum dolor sit ametconsectetur adipiscing elit  iaculis, </span>  <a href="#url" class="moreless-button1 more_cc">more +</a> </p>
								</div>
								<div class="revew_item">
									<div class="revew_top">
										<div class="media">
											<span><img src="{{ URL::asset('public/frontend/images/revew5.png')}}" alt=""></span>
											<div class="media-body">
												<h4>Abhijheet Manna</h4>
												<h5>This is a simply dummy review heading</h5>
											</div>
										</div>
										<div class="star_bx">
											<ul>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
											</ul>
											<strong><img src="{{ URL::asset('public/frontend/images/calen.png')}}" alt=""> 20th Sept, 2021</strong>
										</div>
									</div>
									<p>Lorem ipsum dolor sit amet consectetur the adipiscing elit iaculis magna dignissim facilisis felis nisl pellentesque libero, imperdiet neque ante ut nisi sapien neque sodales lacus auctor the pretium nibhfeugiasapien neque sodales lacus auctor the pretium nibhfeugia ametconsectetur.</p>
								</div>--}}
							</div>
							<a href="#url" class="see_cc moreless-button2">See all review +</a>
							@endif
						</div>
					</div>
				</div>
			</div>

			<div class="property_right">
				<div class="property_ri_inr">
					<div class="share_bx">
						<span><i class="fa fa-share"></i> Share this property by using these</span>
                        <!-- ShareThis BEGIN -->
                        <ul>

                            <div class="sharethis-inline-share-buttons"></div>
                        </ul>

                        {{-- <div class="item">
                            <div class="st-custom-button modalshare" data-network="whatsapp" data-url="{{route('search.property.details',['slug'=>@$propertyDetails->slug])}}">
                                <em><i class="fa fa-whatsapp" aria-hidden="true"></i></em>
                            </div>
                        </div> --}}
                        <!-- ShareThis END -->
						{{-- <ul>
							<li><a href="#" target="_blank"><img src="{{ URL::asset('public/frontend/images/share1.png')}}" alt=""></a></li>
							<li><a href="#" target="_blank"><img src="{{ URL::asset('public/frontend/images/share2.png')}}" alt=""></a></li>
							<li><a href="#" target="_blank"><img src="{{ URL::asset('public/frontend/images/share3.png')}}" alt=""></a></li>
							<li><a href="#" target="_blank"><img src="{{ URL::asset('public/frontend/images/share4.png')}}" alt=""></a></li>
							<li><a href="#" target="_blank"><img src="{{ URL::asset('public/frontend/images/share5.png')}}" alt=""></a></li>
						</ul> --}}
					</div>
					<div class="property_ri_itm">
						<div class="media pro_media">
							<span>
                                @if(@$propertyDetails->propertyUser->profile_pic != null)
                                <img src="{{ URL::to('storage/app/public/profile_picture')}}/{{@$propertyDetails->propertyUser->profile_pic}}" alt="">
                                @else
                                <img src="{{ URL::to('public/frontend/images/agent1.png')}}" alt="">
                                @endif
                                {{-- <img src="{{ URL::asset('public/frontend/images/pro_img.jpg')}}" alt=""> --}}
                            </span>
							<div class="media-body">
								<h5>{{@$propertyDetails->propertyUser->name}}</h5>
								<b><img src="{{ URL::asset('public/frontend/images/property_icon1.png')}}" alt="">{{@$propertyDetails->propertyUser->userState->name}}, {{@$propertyDetails->propertyUser->userCountry->name}}</b>
							</div>
						</div>
						<span class="bodr_arw"></span>
						<div class="send_mess_bx">
							<ul>
								{{-- <li>
									<span><img src="{{ URL::asset('public/frontend/images/send_img1.png')}}" alt=""><a href="mailto:test-agent12@gmail.com">test-agent12@gmail.com</a></span>
								</li>
								<li class="num_2">
									<span><img src="{{ URL::asset('public/frontend/images/send_img2.png')}}" alt=""><a href="tel:919876542100">+91 9876542100</a></span>
									<span><img src="{{ URL::asset('public/frontend/images/send_img4.png')}}" alt=""><a href="tel:876542130">876542130</a></span>
								</li>
								<li>
									<span><img src="{{ URL::asset('public/frontend/images/send_img3.png')}}" alt=""><a href="#">https://www.bbbbannnnhhfg.com</a></span>
								</li> --}}
							@if(@Auth::user()->user_type == 'U')
                                <li>

									<span><img src="{{ URL::asset('public/frontend/images/send_img1.png')}}" alt="">
                                    	<a href="javascript:void(0);" class="view_email">View Email</a>
                                        <strong class="show_email">{{@$propertyDetails->propertyUser->email}}</strong>
                                    </span>

								</li>
                                @if(@$propertyDetails->propertyUser->whatsapp_no)

								<li class="num_2">

									<span><img src="{{ URL::asset('public/frontend/images/send_img2.png')}}" alt="">
                                    	<a href="javascript:void(0);" class="view_noo">View Phone Number</a>
                                        <strong class="show_noo">+91 {{@$propertyDetails->propertyUser->mobile_number}}</strong>
                                    </span>

									<span><img src="{{ URL::asset('public/frontend/images/send_img4.png')}}" alt="">
                                    	<a href="javascript:void(0);" class="view_whatsapp">View WhatsApp </a>
                                        <strong class="show_whatsapp">+91 {{@$propertyDetails->propertyUser->whatsapp_no}}</strong>
                                    </span>

								</li>
                                @endif
                                @if(@$propertyDetails->propertyUser->website)
								<li>

									<span><img src="{{ URL::asset('public/frontend/images/send_img3.png')}}" alt="">
                                    	<a href="javascript:void(0);" class="view_website">View Website</a>
                                        <strong class="show_website">{{@$propertyDetails->propertyUser->website}}</strong>
                                    </span>

								</li>
                                @endif
                            @endif
							</ul>
							<div class="view_pro_btn">
								<a href="{{route('agent.public.profile',['slug'=>@$propertyDetails->propertyUser->slug])}}" class="see_cc">View Profile</a>
								@if(Auth::user() && Auth::user()->user_type == 'U')
								
								<!-- <a href="javascript:;" class="see_cc see_cc_gr chat_btn" data-userid="{{@Auth::user()->id}}" data-agentid="{{@$propertyDetails->propertyUser->id}}" data-name="{{@$propertyDetails->propertyUser->name}}" data-username="{{@Auth::user()->name}}" data-propertyid="{{@$propertyDetails->id}}">Send Message</a> -->

								<a href="javascript:;" class="see_cc see_cc_gr chat_btn" data-userid="{{@Auth::user()->id}}" data-agentid="{{@$propertyDetails->propertyUser->id}}" data-name="{{@$propertyDetails->propertyUser->name}}" data-username="{{@Auth::user()->name}}" data-propertyid="0" data-providerid="{{@$propertyDetails->propertyUser->id}}">Send Message</a>

								@else
								<a class="see_cc see_cc_gr snd_msg">Send Message</a>
								@endif
							</div>
						</div>
					</div>
					<div class="property_ri_itm">
						<h2>Schedule a visit request</h2>
						<span class="bodr_arw"></span>
						<div class="calendar_div">
							<div class="container-calendar">
					          <h3 id="monthAndYear"></h3>
					          <div class="button-container-calendar">
					              <button id="previous" onclick="previous()">&#8249;</button>
					              <button id="next" onclick="next()">&#8250;</button>
					          </div>

					          <table class="table-calendar" id="calendar" data-lang="en">
					              <thead id="thead-month"></thead>
					              <tbody id="calendar-body"></tbody>
					          </table>

					          <div class="footer-container-calendar">
					              <label for="month">Jump To: </label>
					              <select id="month" onchange="jump()">
					                  <option value=0>Jan</option>
					                  <option value=1>Feb</option>
					                  <option value=2>Mar</option>
					                  <option value=3>Apr</option>
					                  <option value=4>May</option>
					                  <option value=5>Jun</option>
					                  <option value=6>Jul</option>
					                  <option value=7>Aug</option>
					                  <option value=8>Sep</option>
					                  <option value=9>Oct</option>
					                  <option value=10>Nov</option>
					                  <option value=11>Dec</option>
					              </select>
					              <select id="year" onchange="jump()"></select>
					          </div>
					      </div>
						</div>
						<div class="input_type_bx">
                            {{-- @include('includes.message') --}}
                            @if(auth()->user())
                            <form action="{{route('visit.request.create')}}" method="POST" id="queryform">
                            {{-- <form action="javascript:;" method="POST" id="queryform"> --}}
                                @csrf
                                <input type="hidden" name="property" value="{{@$propertyDetails->id}}">
                                <div class="input-field">
								    <input type="text" id="datepicker" required="" name="date" placeholder="MM/DD/YYYY" autocomplete="off" readonly="readonly">
								    <label for="name">Enter Date</label>
                                    {{-- <label id="name-error" class="error" for="name" style="display: none"></label> --}}
								 </div>
                                <div class="floating-label">
                                    <select class="floating-select" name="time" required="" id="time">
                                        <option value="">Select Time Slot</option>
                                    </select>
                                    {{-- <label>Choose a time</label> --}}
                                    <label id="time-error" class="error" for="time" style="display: none">Select Time</label>
							    </div>
							    <div class="input-field">
								    <input type="text" id="phonenumber" required="" name="phonenumber" @if(Auth::user()->user_type == 'U') value="{{Auth::user()->mobile_number}}" @endif maxlength="10">
								    <label for="phonenumber">Phone</label>
                                    <label id="phonenumber-error" class="error" for="phonenumber" style="display: none"></label>
								 </div>
								 <div class="input-field">
								    <input type="text" id="address"  name="address">
								    <label for="address">Search area</label>
                                    <label id="address-error" class="error" for="address" style="display: none"></label>
								 </div>
								 <div class="chec_bx">
								 	<div class="radiobx">
										<input type="checkbox" id="radios" name="radios" value="all" >
										<label for="radios">I want to talk about financing</label>
                                        <label id="radios-error" class="error" for="radios" style="display: none">Click Check box</label>
									</div>
								 </div>
								 <div class="view_pro_btn">
									<button class="see_cc">Send Request</button>
								</div>
							</form>
                            @else
                            <div class="view_pro_btn">
                                <button class="see_cc opensignin">Please login for visit request</button>
                            </div>
                            @endif
							<!--<p>Donec quis felis tincidunt, sollicitudin risus the imperdiet turpis sagittis cursus risus aliquet isurna sagittis aliquam tempus erat ia adummy ahnm ipsum facilisis tincidunt.</p>-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="similar_sec">
	<div class="container con_det_pg">
		<div class="similar_inr">
			<div class="section-heading2 text-center">
				<h2>Similar Properties You May Like</h2>
				<p></p>
			</div>
			<div class="similar_slaid">
				<div class="owl-carousel">
                    @foreach ($similarProperty as $smProperty)
                    <div class="item">
						<div class="property-item">
							<div class="listing-image">
                                <a href="#"><img src="{{ URL::asset('storage/app/public/property_image')}}/{{@$smProperty->propertyImageMain->image}}" alt="listing property">
                                    <div class="propety-info-top">

                                    </div>
                                </a>
                                @if(@$smProperty->property_for =='R')
                                <div class="ribbon-img">
                                    <div class="ribbon-vertical"><p>For Rent</p></div>
                                </div>
                                @elseif(@$smProperty->property_for =='B')
                                <div class="ribbon-img">
                                    <div class="ribbon-vertical Sale"><p>For Sale</p></div>
                                </div>
                                @endif
								@if(@$smProperty->propertySave)
                               <div class="wish-property save-property">
                                <a @if(Auth::user() && Auth::user()->user_type == 'U') href="{{  route('property.save',['id'=>@$smProperty->id]) }}" @else href="javascript:;" @endif @if(!Auth::user()) onclick="alert('Please login with continue')" @endif><i class="icofont-heart"></i></a>
                              </div>
                               @else
                              <div class="wish-property">
                               <a @if(Auth::user() && Auth::user()->user_type == 'U') href="{{  route('property.save',['id'=>@$smProperty->id]) }}" @else href="javascript:;" @endif @if(!Auth::user()) onclick="alert('Please login with continue')" @endif><i class="icofont-heart"></i></a>
                              </div>
                               @endif
                                <div class="location-rating">
                                    <div class="product-img-location">
                                        <p><img src="{{ URL::asset('public/frontend/images/loac.png')}}">{{@$smProperty->address}}</p>
                                    </div>
                                    <div class="rating-pro">
                                        <p><img src="{{ URL::asset('public/frontend/images/star.png')}}"> {{ number_format(@$smProperty->avg_review,1, '.', ',') }}</p>
                                    </div>
                                </div>

							</div>
							<div class="property-detasils-info @if(@$smProperty->property_type == 'L') only_land1 @endif ">
								<div class="name-price">
									<a href="{{route('search.property.details',['slug'=>@$smProperty->slug])}}">
                                        <h5>
                                            @if(strlen(@$smProperty->name) > 40)
                                            {!! substr(@$smProperty->name, 0, 40 ) . '..' !!}
                                            @else
                                            {!!@$smProperty->name!!}
                                            @endif
                                        </h5>
                                    </a>
                                    <p>₹{{ number_format(@$smProperty->budget_range_from, 0, '.', ',') }} </p>
								</div>
								<ul class="list-item-info ">
								@if(@$smProperty->property_type == 'F' || @$smProperty->property_type == 'H' || @$smProperty->property_type == 'GTCM')
									<li class="before">
										<span>{{@$smProperty->no_of_bedrooms}} <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span> Bedrooms
									</li>
									@endif
									@if(@$smProperty->property_type == 'WRH' || @$smProperty->property_type == 'SHR' || @$smProperty->property_type == 'INDT' || @$smProperty->property_type == 'RET')
									<li class="before">
										<span>{{@$smProperty->no_of_bedrooms}} <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span>Rooms
									</li>
									@endif
									@if(@$smProperty->property_type == 'F' || @$smProperty->property_type == 'GTCM' || @$smProperty->property_type == 'H' || @$smProperty->property_type == 'R' || @$smProperty->property_type == 'OSP' || @$smProperty->property_type == 'WRH' || @$smProperty->property_type == 'SHR' || @$smProperty->property_type == 'INDT' || @$smProperty->property_type == 'RET')
									<li class="before">
										<span>{{@$smProperty->bathroom}} <img src="{{ URL::asset('public/frontend/images/bath.png')}}"></span> Bathrooms
									</li>
									@endif
									@if(@$smProperty->property_type == 'OSP')
									<li class="before">
										<span>{{@$smProperty->office_class}} <img src="{{ URL::asset('public/frontend/images/ofc.png')}}"></span> Class
									</li>
									@endif
									@if(@$smProperty->property_type == 'L' || @$smProperty->property_type == 'FRMH')
									<li class="">
										<span>{{@$smProperty->area}}<img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
									</li>
									@else
									<li class="">
										<span>{{@$smProperty->area}}<img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
									</li>
									@endif
								</ul>
							</div>
							<div class="propety-agent">
								<div class="agent-info">
									<em>
										@if(@$smProperty->propertyUser->profile_pic != null)
                                        <img src="{{ URL::to('storage/app/public/profile_picture')}}/{{@$smProperty->propertyUser->profile_pic}}" alt="">
                                        @else
                                        <img src="{{ URL::to('public/frontend/images/agent1.png')}}" alt="">
                                        @endif
									</em>
									<div class="agent-name">
										<h6>{{@$smProperty->propertyUser->name}}</h6>
										<p>14 Properties</p>
									</div>
								</div>
								<a href="{{route('search.property.details',['slug'=>@$smProperty->slug])}}" class="vi-more">
									View More
								</a>
							</div>
						</div>
					</div>
                    @endforeach
					{{-- <div class="item">
						<div class="property-item">
							<div class="listing-image">
									<a href="#"><img src="{{ URL::asset('public/frontend/images/list1.png')}}" alt="listing property">
									<div class="propety-info-top">
									</div></a>
									<div class="ribbon-img">
										<div class="ribbon-vertical"><p>For Rent</p></div>
									</div>
									<div class="wish-property">
										<a href="#"><i class="icofont-heart"></i></a>
									</div>
									<div class="location-rating">
										<div class="product-img-location">
											<p><img src="{{ URL::asset('public/frontend/images/loac.png')}}">Canada</p>
										</div>
										<div class="rating-pro">
											<p><img src="{{ URL::asset('public/frontend/images/star.png')}}"> 4.5</p>
										</div>
									</div>
							</div>
							<div class="property-detasils-info">
								<div class="name-price">
									<a href="#"><h5>Blue Reef Properties</h5></a>
									<p>₹4,500 </p>
								</div>
								<ul class="list-item-info ">
									<li class="before">
										<span>3 <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span> Bedrooms
									</li>
									<li class="before">
										<span>2 <img src="{{ URL::asset('public/frontend/images/bath.png')}}"></span> Bathrooms
									</li>
									<li class="">
										<span>3450 <img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
									</li>
								</ul>
							</div>
							<div class="propety-agent">
								<div class="agent-info">
									<em>
										<img src="{{ URL::asset('public/frontend/images/agent1.png')}}">
									</em>
									<div class="agent-name">
										<h6>Gina Mconihon</h6>
										<p>14 Properties</p>
									</div>
								</div>
								<a href="#" class="vi-more">
									View More
								</a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="property-item">
							<div class="listing-image">
									<a href="#"><img src="{{ URL::asset('public/frontend/images/list2.png')}}" alt="listing property">
									<div class="propety-info-top">
									</div></a>
									<div class="ribbon-img">
										<div class="ribbon-vertical Sale"><p>For Sale</p></div>
									</div>
									<div class="wish-property">
										<a href="#"><i class="icofont-heart"></i></a>
									</div>
									<div class="location-rating">
										<div class="product-img-location">
											<p><img src="{{ URL::asset('public/frontend/images/loac.png')}}">New York , USA</p>
										</div>
										<div class="rating-pro">
											<p><img src="{{ URL::asset('public/frontend/images/star.png')}}"> 4.5</p>
										</div>
									</div>
							</div>
							<div class="property-detasils-info">
								<div class="name-price">
									<a href="#"><h5>Blue Reef Properties</h5></a>
									<p>₹9,100 </p>
								</div>
								<ul class="list-item-info ">
									<li class="before">
										<span>3 <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span> Bedrooms
									</li>
									<li class="before">
										<span>2 <img src="{{ URL::asset('public/frontend/images/bath.png')}}"></span> Bathrooms
									</li>
									<li class="">
										<span>3450 <img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
									</li>
								</ul>
							</div>
							<div class="propety-agent">
								<div class="agent-info">
									<em>
										<img src="{{ URL::asset('public/frontend/images/agent2.png')}}">
									</em>
									<div class="agent-name">
										<h6>Gina Mconihon</h6>
										<p>14 Properties</p>
									</div>
								</div>
								<a href="#" class="vi-more">
									View More
								</a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="property-item">
							<div class="listing-image">
									<a href="#"><img src="{{ URL::asset('public/frontend/images/list3.png')}}" alt="listing property">
									<div class="propety-info-top">
									</div></a>
									<div class="ribbon-img">
										<div class="ribbon-vertical Sale"><p>For Sale</p></div>
									</div>
									<div class="wish-property">
										<a href="#"><i class="icofont-heart"></i></a>
									</div>
									<div class="location-rating">
										<div class="product-img-location">
											<p><img src="{{ URL::asset('public/frontend/images/loac.png')}}">Hydrabad</p>
										</div>
										<div class="rating-pro">
											<p><img src="{{ URL::asset('public/frontend/images/star.png')}}"> 4.8</p>
										</div>
									</div>
							</div>
							<div class="property-detasils-info">
								<div class="name-price">
									<a href="#"><h5>Blue Reef Properties</h5></a>
									<p>₹4,300 </p>
								</div>
								<ul class="list-item-info ">
									<li class="before">
										<span>3 <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span> Bedrooms
									</li>
									<li class="before">
										<span>2 <img src="{{ URL::asset('public/frontend/images/bath.png')}}"></span> Bathrooms
									</li>
									<li class="">
										<span>3450 <img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
									</li>
								</ul>
							</div>
							<div class="propety-agent">
								<div class="agent-info">
									<em>
										<img src="{{ URL::asset('public/frontend/images/agent1.png')}}">
									</em>
									<div class="agent-name">
										<h6>Gina Mconihon</h6>
										<p>14 Properties</p>
									</div>
								</div>
								<a href="#" class="vi-more">
									View More
								</a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="property-item">
							<div class="listing-image">
									<a href="#"><img src="{{ URL::asset('public/frontend/images/list4.png')}}" alt="listing property">
									<div class="propety-info-top">
									</div></a>
									<div class="ribbon-img">
										<div class="ribbon-vertical Sale"><p>For Sale</p></div>
									</div>
									<div class="wish-property">
										<a href="#"><i class="icofont-heart"></i></a>
									</div>
									<div class="location-rating">
										<div class="product-img-location">
											<p><img src="{{ URL::asset('public/frontend/images/loac.png')}}">Canada</p>
										</div>
										<div class="rating-pro">
											<p><img src="{{ URL::asset('public/frontend/images/star.png')}}"> 4.5</p>
										</div>
									</div>
							</div>
							<div class="property-detasils-info">
								<div class="name-price">
									<a href="#"><h5>Blue Reef Properties</h5></a>
									<p>₹9,500 </p>
								</div>
								<ul class="list-item-info ">
									<li class="before">
										<span>3 <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span> Bedrooms
									</li>
									<li class="before">
										<span>2 <img src="{{ URL::asset('public/frontend/images/bath.png')}}"></span> Bathrooms
									</li>
									<li class="">
										<span>3450 <img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
									</li>
								</ul>
							</div>
							<div class="propety-agent">
								<div class="agent-info">
									<em>
										<img src="{{ URL::asset('public/frontend/images/agent2.png')}}">
									</em>
									<div class="agent-name">
										<h6>Gina Mconihon</h6>
										<p>14 Properties</p>
									</div>
								</div>
								<a href="#" class="vi-more">
									View More
								</a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="property-item">
							<div class="listing-image">
									<a href="#"><img src="{{ URL::asset('public/frontend/images/list1.png')}}" alt="listing property">
									<div class="propety-info-top">
									</div></a>
									<div class="ribbon-img">
										<div class="ribbon-vertical"><p>For Rent</p></div>
									</div>
									<div class="wish-property">
										<a href="#"><i class="icofont-heart"></i></a>
									</div>
									<div class="location-rating">
										<div class="product-img-location">
											<p><img src="{{ URL::asset('public/frontend/images/loac.png')}}">Canada</p>
										</div>
										<div class="rating-pro">
											<p><img src="{{ URL::asset('public/frontend/images/star.png')}}"> 4.5</p>
										</div>
									</div>
							</div>
							<div class="property-detasils-info">
								<div class="name-price">
									<a href="#"><h5>Blue Reef Properties</h5></a>
									<p>₹4,500 </p>
								</div>
								<ul class="list-item-info ">
									<li class="before">
										<span>3 <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span> Bedrooms
									</li>
									<li class="before">
										<span>2 <img src="{{ URL::asset('public/frontend/images/bath.png')}}"></span> Bathrooms
									</li>
									<li class="">
										<span>3450 <img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
									</li>
								</ul>
							</div>
							<div class="propety-agent">
								<div class="agent-info">
									<em>
										<img src="{{ URL::asset('public/frontend/images/agent1.png')}}">
									</em>
									<div class="agent-name">
										<h6>Gina Mconihon</h6>
										<p>14 Properties</p>
									</div>
								</div>
								<a href="#" class="vi-more">
									View More
								</a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="property-item">
							<div class="listing-image">
									<a href="#"><img src="{{ URL::asset('public/frontend/images/list2.png')}}" alt="listing property">
									<div class="propety-info-top">
									</div></a>
									<div class="ribbon-img">
										<div class="ribbon-vertical Sale"><p>For Sale</p></div>
									</div>
									<div class="wish-property">
										<a href="#"><i class="icofont-heart"></i></a>
									</div>
									<div class="location-rating">
										<div class="product-img-location">
											<p><img src="{{ URL::asset('public/frontend/images/loac.png')}}">New York , USA</p>
										</div>
										<div class="rating-pro">
											<p><img src="{{ URL::asset('public/frontend/images/star.png')}}"> 4.5</p>
										</div>
									</div>
							</div>
							<div class="property-detasils-info">
								<div class="name-price">
									<a href="#"><h5>Blue Reef Properties</h5></a>
									<p>₹9,100 </p>
								</div>
								<ul class="list-item-info ">
									<li class="before">
										<span>3 <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span> Bedrooms
									</li>
									<li class="before">
										<span>2 <img src="{{ URL::asset('public/frontend/images/bath.png')}}"></span> Bathrooms
									</li>
									<li class="">
										<span>3450 <img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
									</li>
								</ul>
							</div>
							<div class="propety-agent">
								<div class="agent-info">
									<em>
										<img src="{{ URL::asset('public/frontend/images/agent2.png')}}">
									</em>
									<div class="agent-name">
										<h6>Gina Mconihon</h6>
										<p>14 Properties</p>
									</div>
								</div>
								<a href="#" class="vi-more">
									View More
								</a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="property-item">
							<div class="listing-image">
									<a href="#"><img src="{{ URL::asset('public/frontend/images/list3.png')}}" alt="listing property">
									<div class="propety-info-top">
									</div></a>
									<div class="ribbon-img">
										<div class="ribbon-vertical Sale"><p>For Sale</p></div>
									</div>
									<div class="wish-property">
										<a href="#"><i class="icofont-heart"></i></a>
									</div>
									<div class="location-rating">
										<div class="product-img-location">
											<p><img src="{{ URL::asset('public/frontend/images/loac.png')}}">Hydrabad</p>
										</div>
										<div class="rating-pro">
											<p><img src="{{ URL::asset('public/frontend/images/star.png')}}"> 4.8</p>
										</div>
									</div>
							</div>
							<div class="property-detasils-info">
								<div class="name-price">
									<a href="#"><h5>Blue Reef Properties</h5></a>
									<p>₹4,300 </p>
								</div>
								<ul class="list-item-info ">
									<li class="before">
										<span>3 <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span> Bedrooms
									</li>
									<li class="before">
										<span>2 <img src="{{ URL::asset('public/frontend/images/bath.png')}}"></span> Bathrooms
									</li>
									<li class="">
										<span>3450 <img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
									</li>
								</ul>
							</div>
							<div class="propety-agent">
								<div class="agent-info">
									<em>
										<img src="{{ URL::asset('public/frontend/images/agent1.png')}}">
									</em>
									<div class="agent-name">
										<h6>Gina Mconihon</h6>
										<p>14 Properties</p>
									</div>
								</div>
								<a href="#" class="vi-more">
									View More
								</a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="property-item">
							<div class="listing-image">
									<a href="#"><img src="{{ URL::asset('public/frontend/images/list4.png')}}" alt="listing property">
									<div class="propety-info-top">
									</div></a>
									<div class="ribbon-img">
										<div class="ribbon-vertical Sale"><p>For Sale</p></div>
									</div>
									<div class="wish-property">
										<a href="#"><i class="icofont-heart"></i></a>
									</div>
									<div class="location-rating">
										<div class="product-img-location">
											<p><img src="{{ URL::asset('public/frontend/images/loac.png')}}">Canada</p>
										</div>
										<div class="rating-pro">
											<p><img src="{{ URL::asset('public/frontend/images/star.png')}}"> 4.5</p>
										</div>
									</div>
							</div>
							<div class="property-detasils-info">
								<div class="name-price">
									<a href="#"><h5>Blue Reef Properties</h5></a>
									<p>₹9,500 </p>
								</div>
								<ul class="list-item-info ">
									<li class="before">
										<span>3 <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span> Bedrooms
									</li>
									<li class="before">
										<span>2 <img src="{{ URL::asset('public/frontend/images/bath.png')}}"></span> Bathrooms
									</li>
									<li class="">
										<span>3450 <img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
									</li>
								</ul>
							</div>
							<div class="propety-agent">
								<div class="agent-info">
									<em>
										<img src="{{ URL::asset('public/frontend/images/agent2.png')}}">
									</em>
									<div class="agent-name">
										<h6>Gina Mconihon</h6>
										<p>14 Properties</p>
									</div>
								</div>
								<a href="#" class="vi-more">
									View More
								</a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="property-item">
							<div class="listing-image">
									<a href="#"><img src="{{ URL::asset('public/frontend/images/list1.png')}}" alt="listing property">
									<div class="propety-info-top">
									</div></a>
									<div class="ribbon-img">
										<div class="ribbon-vertical"><p>For Rent</p></div>
									</div>
									<div class="wish-property">
										<a href="#"><i class="icofont-heart"></i></a>
									</div>
									<div class="location-rating">
										<div class="product-img-location">
											<p><img src="{{ URL::asset('public/frontend/images/loac.png')}}">Canada</p>
										</div>
										<div class="rating-pro">
											<p><img src="{{ URL::asset('public/frontend/images/star.png')}}"> 4.5</p>
										</div>
									</div>
							</div>
							<div class="property-detasils-info">
								<div class="name-price">
									<a href="#"><h5>Blue Reef Properties</h5></a>
									<p>₹4,500 </p>
								</div>
								<ul class="list-item-info ">
									<li class="before">
										<span>3 <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span> Bedrooms
									</li>
									<li class="before">
										<span>2 <img src="{{ URL::asset('public/frontend/images/bath.png')}}"></span> Bathrooms
									</li>
									<li class="">
										<span>3450 <img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
									</li>
								</ul>
							</div>
							<div class="propety-agent">
								<div class="agent-info">
									<em>
										<img src="{{ URL::asset('public/frontend/images/agent1.png')}}">
									</em>
									<div class="agent-name">
										<h6>Gina Mconihon</h6>
										<p>14 Properties</p>
									</div>
								</div>
								<a href="#" class="vi-more">
									View More
								</a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="property-item">
							<div class="listing-image">
									<a href="#"><img src="{{ URL::asset('public/frontend/images/list2.png')}}" alt="listing property">
									<div class="propety-info-top">
									</div></a>
									<div class="ribbon-img">
										<div class="ribbon-vertical Sale"><p>For Sale</p></div>
									</div>
									<div class="wish-property">
										<a href="#"><i class="icofont-heart"></i></a>
									</div>
									<div class="location-rating">
										<div class="product-img-location">
											<p><img src="{{ URL::asset('public/frontend/images/loac.png')}}">New York , USA</p>
										</div>
										<div class="rating-pro">
											<p><img src="{{ URL::asset('public/frontend/images/star.png')}}"> 4.5</p>
										</div>
									</div>
							</div>
							<div class="property-detasils-info">
								<div class="name-price">
									<a href="#"><h5>Blue Reef Properties</h5></a>
									<p>₹9,100 </p>
								</div>
								<ul class="list-item-info ">
									<li class="before">
										<span>3 <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span> Bedrooms
									</li>
									<li class="before">
										<span>2 <img src="{{ URL::asset('public/frontend/images/bath.png')}}"></span> Bathrooms
									</li>
									<li class="">
										<span>3450 <img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
									</li>
								</ul>
							</div>
							<div class="propety-agent">
								<div class="agent-info">
									<em>
										<img src="{{ URL::asset('public/frontend/images/agent2.png')}}">
									</em>
									<div class="agent-name">
										<h6>Gina Mconihon</h6>
										<p>14 Properties</p>
									</div>
								</div>
								<a href="#" class="vi-more">
									View More
								</a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="property-item">
							<div class="listing-image">
									<a href="#"><img src="{{ URL::asset('public/frontend/images/list3.png')}}" alt="listing property">
									<div class="propety-info-top">
									</div></a>
									<div class="ribbon-img">
										<div class="ribbon-vertical Sale"><p>For Sale</p></div>
									</div>
									<div class="wish-property">
										<a href="#"><i class="icofont-heart"></i></a>
									</div>
									<div class="location-rating">
										<div class="product-img-location">
											<p><img src="{{ URL::asset('public/frontend/images/loac.png')}}">Hydrabad</p>
										</div>
										<div class="rating-pro">
											<p><img src="{{ URL::asset('public/frontend/images/star.png')}}"> 4.8</p>
										</div>
									</div>
							</div>
							<div class="property-detasils-info">
								<div class="name-price">
									<a href="#"><h5>Blue Reef Properties</h5></a>
									<p>₹4,300 </p>
								</div>
								<ul class="list-item-info ">
									<li class="before">
										<span>3 <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span> Bedrooms
									</li>
									<li class="before">
										<span>2 <img src="{{ URL::asset('public/frontend/images/bath.png')}}"></span> Bathrooms
									</li>
									<li class="">
										<span>3450 <img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
									</li>
								</ul>
							</div>
							<div class="propety-agent">
								<div class="agent-info">
									<em>
										<img src="{{ URL::asset('public/frontend/images/agent1.png')}}">
									</em>
									<div class="agent-name">
										<h6>Gina Mconihon</h6>
										<p>14 Properties</p>
									</div>
								</div>
								<a href="#" class="vi-more">
									View More
								</a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="property-item">
							<div class="listing-image">
									<a href="#"><img src="{{ URL::asset('public/frontend/images/list4.png')}}" alt="listing property">
									<div class="propety-info-top">
									</div></a>
									<div class="ribbon-img">
										<div class="ribbon-vertical Sale"><p>For Sale</p></div>
									</div>
									<div class="wish-property">
										<a href="#"><i class="icofont-heart"></i></a>
									</div>
									<div class="location-rating">
										<div class="product-img-location">
											<p><img src="{{ URL::asset('public/frontend/images/loac.png')}}">Canada</p>
										</div>
										<div class="rating-pro">
											<p><img src="{{ URL::asset('public/frontend/images/star.png')}}"> 4.5</p>
										</div>
									</div>
							</div>
							<div class="property-detasils-info">
								<div class="name-price">
									<a href="#"><h5>Blue Reef Properties</h5></a>
									<p>₹9,500 </p>
								</div>
								<ul class="list-item-info ">
									<li class="before">
										<span>3 <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span> Bedrooms
									</li>
									<li class="before">
										<span>2 <img src="{{ URL::asset('public/frontend/images/bath.png')}}"></span> Bathrooms
									</li>
									<li class="">
										<span>3450 <img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
									</li>
								</ul>
							</div>
							<div class="propety-agent">
								<div class="agent-info">
									<em>
										<img src="{{ URL::asset('public/frontend/images/agent2.png')}}">
									</em>
									<div class="agent-name">
										<h6>Gina Mconihon</h6>
										<p>14 Properties</p>
									</div>
								</div>
								<a href="#" class="vi-more">
									View More
								</a>
							</div>
						</div>
					</div> --}}
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade modal_review_post" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		<form action="{{ route('rating.save') }}" method="post" id="ReviewForm">
		@csrf	
         <input type="hidden" name="property_id" value="{{ @$propertyDetails->id }}">
		 <input type="hidden" name="owner_id" id="" value="{{ @$propertyDetails->propertyUser->id }}">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Rate <span>{{ @$propertyDetails->name }}</span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
	  <div class="modal-body">
       	<div class="body_review_post">
       		<div class="form_box_area newmar">  

                <label>Rating</label>   

                <div>
				<div id="half-stars-example">
                        <div class="rating-group">
				          @if(@$user_rating)
						     @for($i=1; $i<=@$user_rating->review_point; $i++)
							 <label aria-label="1 star" class="rating__label" for="rating2-{{$i}}0"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                             <input class="rating__input" name="property_rating" id="rating2-{{ $i }}0" checked value="{{ $i }}" type="radio" disabled>
							 @endfor
							 @for($j=@$user_rating->review_point+1; $j<=5; $j++)
							 <label aria-label="1 star" class="rating__label" for="rating2-{{$j}}0"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                             <input class="rating__input" name="property_rating" id="rating2-{{ $j }}0" value="{{ $j }}" type="radio" disabled>
							 @endfor
	                       @else
                            <input class="rating__input rating__input--none" checked name="property_rating" id="rating2-0" value="0" type="radio">
                            {{--<label aria-label="0 stars" class="rating__label none" for="rating2-0">&nbsp;</label>--}}

                            <label aria-label="1 star" class="rating__label" for="rating2-10"><i class="rating__icon rating__icon--star fa fa-star"></i></label>

                            <input class="rating__input" name="property_rating" id="rating2-10" value="1" type="radio">

                            <label aria-label="2 stars" class="rating__label" for="rating2-20"><i class="rating__icon rating__icon--star fa fa-star"></i></label>

                            <input class="rating__input" name="property_rating" id="rating2-20" value="2" type="radio">

                            <label aria-label="3 stars" class="rating__label" for="rating2-30"><i class="rating__icon rating__icon--star fa fa-star"></i></label>

                            <input class="rating__input" name="property_rating" id="rating2-30" value="3" type="radio">

                            <label aria-label="4 stars" class="rating__label" for="rating2-40"><i class="rating__icon rating__icon--star fa fa-star"></i></label>

                            <input class="rating__input" name="property_rating" id="rating2-40" value="4" type="radio">

                            <label aria-label="5 stars" class="rating__label" for="rating2-50"><i class="rating__icon rating__icon--star fa fa-star"></i></label>

                            <input class="rating__input" name="property_rating" id="rating2-50" value="5" type="radio">
                          @endif
                        </div>

                </div> 

                </div>   

            </div>

            </div>
       		<div class="das_input">
				<label>Description </label>
				<textarea placeholder="Enter here.." name="description"@if( @$user_rating->review_text) readonly @endif>{{ @$user_rating->review_text }}</textarea> 
			</div>
			<div class="revie_btn_modal">
				@if( @$user_rating->review_text)
				<!-- <button type="button" class="see_cc cancel_foot" data-dismiss="modal">Cancel</button> -->

				@else

				<input type="submit" value="Post" class="see_cc">
				<button type="button" class="see_cc cancel_foot" data-dismiss="modal">Cancel</button>

				@endif
				
				
			</div>
       	</div>
      </div>
      
	</form>
    </div>
	
  </div>
</div>

@endsection


@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection



@section('script')
@include('includes.script')
@include('includes.toaster')

{{--<script>
    // Initialize and add the map
function initMap() {
  // The location of Uluru
  var uluru = {lat: {{$propertyDetails->address_lat}}, lng: {{$propertyDetails->address_long}}};
  // The map, centered at Uluru
  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 10, center: uluru});
  // The marker, positioned at Uluru
  var marker = new google.maps.Marker({position: uluru, map: map});
  var mapKey ='{{env('GOOGLE_MAP_KEY')}}';
}
</script>--}}
<!--Load the API from the specified URL
    * The async attribute allows the browser to render the page while the API loads
    * The key parameter will contain your own API key (which is not needed for this tutorial)
    * The callback parameter executes the initMap() function
    -->

<script>
    $(document).ready(function(){
        $(".view_noo").click(function(){
            $(".show_noo").show();
        });
        $(".view_noo").click(function(){
            $(".view_noo").hide();
        });
    });
    </script>

    <script>
    $(document).ready(function(){
        $(".view_email").click(function(){
            $(".show_email").show();
        });
        $(".view_email").click(function(){
            $(".view_email").hide();
        });
    });
    </script>

    <script>
    $(document).ready(function(){
        $(".view_whatsapp").click(function(){
            $(".show_whatsapp").show();
        });
        $(".view_whatsapp").click(function(){
            $(".view_whatsapp").hide();
        });
    });
    </script>

    <script>
    $(document).ready(function(){
        $(".view_website").click(function(){
            $(".show_website").show();
        });
        $(".view_website").click(function(){
            $(".view_website").hide();
        });
    });
    </script>
    <script>
        $(document).ready(function(){
            $('#queryform').validate({
                rules: {
                    phonenumber:{
                        required: true,
                        number: true ,
                        minlength: 10,
                        maxlength: 10,
                    },
                    time:{
                        remote: {
                            url: '{{ route("visit.request.check") }}',
                            dataType: 'json',
                            type:'post',
                            data: {
                                time: function() {
                                    return $('#time').val();
                                },
                                _token: '{{ csrf_token() }}',
                                user_id: '{{@$propertyDetails->propertyUser->id}}',
                                date:  function() {
                                    return $('#datepicker').val();
                                },
                            }
                        }
                    }
                },
                messages:{
                    phonenumber:{
                        required: 'Enter Phone number',
                        number: 'Only Number' ,
                        minlength: 'Phone number minimum 10 digit',
                        maxlength: 'Phone number maximum 10 digit',
                    },
                    name:{
                        required:"Enter Full Name"
                    },
                    time:{
                        required:"Select Time",
                        remote: 'This time already booked ',
                    },
                    date:{
                        required:"Select Date"
                    },
                    
                },
                ignore: [],
            });
        });
        var daylist=[];
        @foreach ($dayAvailability as $dayava)
        daylist.push({{ $dayava }});
        @endforeach
        $('#datepicker').datepicker({ beforeShowDay: function(date) {
            var day=daylist;
            return [day.includes(date.getDay()) ,''];
        },minDate: 0,});
		
    </script>

    <script>

        function generate_year_range(start, end) {
            var years = "";
            for (var year = start; year <= end; year++) {
                years += "<option value='" + year + "'>" + year + "</option>";
            }
            return years;
        }
        var today = new Date();
        var currentMonth = today.getMonth();
        var currentYear = today.getFullYear();
        var selectYear = document.getElementById("year");
        var selectMonth = document.getElementById("month");
        var createYear = generate_year_range(1970, 2050);
        /** or
         * * createYear = generate_year_range( 1970, currentYear );
         * */
        document.getElementById("year").innerHTML = createYear;
        var calendar = document.getElementById("calendar");
        var lang = calendar.getAttribute('data-lang');
        var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
        var dayHeader = "<tr>";
        for (day in days) {
            dayHeader += "<th data-days='" + days[day] + "'>" + days[day] + "</th>";
        }
        dayHeader += "</tr>";
        document.getElementById("thead-month").innerHTML = dayHeader;
        monthAndYear = document.getElementById("monthAndYear");
        showCalendar(currentMonth, currentYear);
        function next() {
            currentYear = (currentMonth === 11) ? currentYear + 1 : currentYear;
            currentMonth = (currentMonth + 1) % 12;
            showCalendar(currentMonth, currentYear);
        }
        function previous() {
            currentYear = (currentMonth === 0) ? currentYear - 1 : currentYear;
            currentMonth = (currentMonth === 0) ? 11 : currentMonth - 1;
            showCalendar(currentMonth, currentYear);
        }
        function jump() {
            currentYear = parseInt(selectYear.value);
            currentMonth = parseInt(selectMonth.value);
            showCalendar(currentMonth, currentYear);
        }
        function showCalendar(month, year) {
            var firstDay = ( new Date( year, month ) ).getDay();
            tbl = document.getElementById("calendar-body");
            tbl.innerHTML = "";
            monthAndYear.innerHTML = months[month] + " " + year;selectYear.value = year;
            selectMonth.value = month;
            // creating all cells
            var date = 1;
            for ( var i = 0; i < 6; i++ ) {
                var row = document.createElement("tr");
                for ( var j = 0; j < 7; j++ ) {
                    if ( i === 0 && j < firstDay ) {
                        cell = document.createElement( "td" );
                        cellText = document.createTextNode("");
                        cell.appendChild(cellText);
                        row.appendChild(cell);
                    } else if (date > daysInMonth(month, year)) {
                        break;
                    } else {
                        cell = document.createElement("td");
                        cell.setAttribute("data-date", date);
                        cell.setAttribute("data-month", month + 1);
                        cell.setAttribute("data-year", year);
                        cell.setAttribute("data-month_name", months[month]);
                        cell.className = "date-picker";
                        cell.innerHTML = "<span>" + date + "</span>";
                        if ( date === today.getDate() && year === today.getFullYear() && month === today.getMonth() ) {
                            cell.className = "date-picker selected";
                        }
                        if(daylist.includes((j))==false){
                            cell.className = "date-picker block_day";
                        }
                        if(year === today.getFullYear()){
                            if(month === today.getMonth()){
                                if(date < today.getDate()){
                                    cell.className = "date-picker block_day";
                                }
                            }
                            else if(month < today.getMonth()){
                                cell.className = "date-picker block_day";
                            }
                        }if(year < today.getFullYear()){
                            cell.className = "date-picker block_day";
                        }
                        row.appendChild(cell);
                        date++;
                    }
                }
                tbl.appendChild(row);
            }
        }
        function daysInMonth(iMonth, iYear) {
            return 32 - new Date(iYear, iMonth, 32).getDate();
        }



    </script>
    <script>
        $('#datepicker').change(function(){
            let date=$('#datepicker').val();

            $('#time').html('');
            var reqData = {
                'jsonrpc': '2.0',
                '_token': '{{csrf_token()}}',
                'params': {
                    date: date,
                    user_id: '{{@$propertyDetails->propertyUser->id}}',
                }
            };
            $.ajax({
                url: '{{ route('get.time.slot') }}',
                type: 'post',
                dataType: 'json',
                data: reqData,
            })
            .done(function(response) {
                console.log(response);
                $('#time').html(response.result.html);

            })
            .fail(function(error) {
                $('#time').html('<option value="" selected>No time slot available</option>');
                console.log("error", error);
            })
            .always(function() {
                // console.log("complete");
            })
        })
    </script>
    <script>
	  $('.snd_msg').click(function(){

	    
	      alert('Please Login as User');
	   

	  });
	</script>
	<script>
		$(document).ready(function(){

			 $('#ReviewForm').validate({

				  rules: {

					    property_rating: {

							 required: true,
						},
						description: {

							 required: true,
						},
				  }
			 })
		})
	</script>

 <script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?libraries=geometry&key=AIzaSyAJ6xqCq4nj_VBSUUX67q34-ELENfvqFIM">
</script>
 

<script> 



var geocoder = new google.maps.Geocoder();
var address = document.getElementById("city_name").value;
geocoder.geocode( { 'address': address}, function(results, status) {
  if (status == google.maps.GeocoderStatus.OK)
  {
      // do something with the geocoded result
      //
     
      var lat =results[0].geometry.location.lat();
      var lng =results[0].geometry.location.lng();
    //   var lat = 22.5726;
    //   var lng = 88.3639;
      $('#map_div').html('<iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAJ6xqCq4nj_VBSUUX67q34-ELENfvqFIM&q='+address+'&center='+lat+','+lng+'&zoom=15" style="border:0;" allowfullscreen="" loading="lazy"></iframe>');
      
  }
});
</script>

<script>
		// $("#short_desc").html($('#short_desc').data('desc'));

	$('.short_desc_bid').each(function(){

		var str1 = $(this).data('desc');
		var bid_id = $(this).data('id');
		console.log( "SHORT : "  + bid_id);
		$(this).html(str1 + "..." + `<a href="javascript:;" class="read_more_bid" data-id="${bid_id}"> Read more +</a>`);
	});
	$('.full_desc_bid').each(function(){
		var str2 = $(this).data('desc');
		var bid_id = $(this).data('id');
		console.log( "FULL : "  + bid_id);
		$(this).html(str2 + `<a href="javascript:;" class="read_more_bid" data-id="${bid_id}"> Read less -</a>`);
	});

	// $('.read_more').click(function(){
	$("body").delegate(".read_more_bid", "click", function(e) {
		var id = $(this).data('id');
		console.log(id);
		if($('.short_desc_bid_'+id).css('display') == "block"){
			$('.short_desc_bid_'+id).hide();
			$('.full_desc_bid_'+id).show();
		} else {
			$('.full_desc_bid_'+id).hide();
			$('.short_desc_bid_'+id).show();
		}
	});
	
</script>

	
@endsection

@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> RiVirtual | Help</title>
@endsection




@section('header')
@include('includes.header')
@endsection

@section('content')

<div class="haeder-padding"></div>

<div class="about-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="breadcrumbs-inner">
						<h1 class="page-title">Help</h1>
						<div class="breadcrumbs-list">
                            <ul>
                                <li><a href="{{ route('home') }}"><span class=""><i class="icofont-home"></i></span> Home</a></li>
                                <li>Help</li>
                            </ul>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>

    <div class="faqs-sec ">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 col-xl-9 col-md-11 mx-auto">
					<div class="section-heading2 text-center">
						<h2>How can we help?</h2>
						<!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit sed lorem sapien, auctor in justo id, dignissim dipiscing elit sed lorem sapien</p>-->
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-xl-3 col-md-4 col-sm-4">
					<div class="help_left">
					  <div class="faq_tbs">
						<ul class="nav nav-tabs">
							<li>
								<a class="active" data-toggle="tab" href="#tab1">User</a>
							</li>
							<li>
								<a data-toggle="tab" href="#tab2">Agents</a>
							</li>
							<li>
								<a data-toggle="tab" href="#tab3">Service Pro</a>
							</li>
							<li>
								<a data-toggle="tab" href="#tab4">Payments</a>
							</li>
							
						</ul>
					  </div>
					</div>
						
				</div>

				<div class="col-lg-8 col-xl-9 col-md-8 col-sm-8">
					<div class="tab-content">
						<div class="tab-pane active" id="tab1">
								<div class="accordian-faq">
									<div class="accordion" id="faq">
												<div class="card">
													<div class="card-header" id="faqhead1">
														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq1" aria-expanded="true" aria-controls="faq1">
															<p><span>1. </span> What is RiVirtual?</p>
														</a>
													</div>
													<div id="faq1" class="collapse" aria-labelledby="faqhead1" data-parent="#faq">
														<div class="card-body">
															<p>RiVirtual is a privately owned global real estate intelligence company, development, and management firm in 100 cities in 5 countries and more than 100 million square feet of assets for which RiVirtual provides third-party property-level services. RiVirtual's Artificial Intelligence (AI) is based on Accessibility, Affordability, Availability, and Authenticity ( 4As) models of real estate markets, Economic Data, and Sustainability.</p>
														</div>
													</div>
												</div>


												<div class="card">
													<div class="card-header" id="faqhead2">
														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq2" aria-expanded="true" aria-controls="faq2">
															<p><span>2. </span> How does RiVirtual work?</p>
														</a>
													</div>
													<div id="faq2" class="collapse" aria-labelledby="faqhead2" data-parent="#faq">
														<div class="card-body">
															<p>RiVirtual is the #1 Most Trusted Real Estate Agents Platform in India, which provides a complete ecosystem of real estate providing an online marketplace, real estate agents, loans, property management, service providers, and much more.</p>
														</div>
													</div>
												</div>

												<div class="card">
													<div class="card-header" id="faqhead3">
														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq3" aria-expanded="true" aria-controls="faq3">
															<p><span>3. </span> How to Sign up as a User?</p>
														</a>
													</div>
													<div id="faq3" class="collapse" aria-labelledby="faqhead3" data-parent="#faq">
														<div class="card-body">
															<p>Follow the steps below:
																<ul>
																	<li>1.  	Click on the Sign-in button at the top right corner.</li>
																	<li>2.  	Click on Sign Up Now at the bottom of the pop-up form.</li>
																	<li>3.  	Enter your details and click Sign Up or Sign Up by your Facebook account or Google account.</li>
																</ul>
															</p>
														</div>
													</div>
												</div>

												<div class="card">
													<div class="card-header" id="faqhead4">
														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq4" aria-expanded="true" aria-controls="faq4">
															<p><span>4. </span> Is it safe to use login via Facebook & Google?</p>
														</a>
													</div>
													<div id="faq4" class="collapse" aria-labelledby="faqhead4" data-parent="#faq">
														<div class="card-body">
															<p>If you log in via Facebook or Google, your account will be automatically verified. At RiVirtual, we do not misuse your personal information, and we do not post anything from your social media for publicity. We do not ask for any extra information from your social media account.</p>
														</div>
													</div>
												</div>

												<div class="card">
													<div class="card-header" id="faqhead5">
														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq5" aria-expanded="true" aria-controls="faq5">
															<p><span>5. </span> How to search for a property visit?</p>
														</a>
													</div>
													<div id="faq5" class="collapse" aria-labelledby="faqhead5" data-parent="#faq">
														<div class="card-body">
															<p>Based on the type of property you want to search, click on commercial or residential from the top menu. On the next page, enter the location where you want to explore and select the property type. You can toggle between BUY and RENT based upon your choice and click search.
															</p>
															<p>You will get a curated list of properties matching your criteria. You can further apply filters to the list.</p>
														</div>
													</div>
												</div>


												<div class="card">
													<div class="card-header" id="faqhead10">
														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq10" aria-expanded="true" aria-controls="faq10">
															<p><span>6. </span> How to book a property visit?</p>
														</a>
													</div>
													<div id="faq10" class="collapse" aria-labelledby="faqhead10" data-parent="#faq">
														<div class="card-body">
															<p>When you open a property details page by clicking on View More, you can fill out the short form on the right side and send a request to the agent for a property visit. You can also view the agent's profile and message him directly. (To be more secure, we recommend you to reach the agents through RiVirtual)</p>
														</div>
													</div>
												</div>

												<div class="card">
													<div class="card-header" id="faqhead6">
														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq6" aria-expanded="true" aria-controls="faq6">
															<p><span>7. </span> What is Property Management?</p>
														</a>
													</div>
													<div id="faq6" class="collapse" aria-labelledby="faqhead6" data-parent="#faq">
														<div class="card-body">
															<p>If you find it hard to manage your property because you don't have time or live far from the property, we provide a property management service to look after your property. To know all the features visit our <a href="{{route('property.manage.page')}}">Property Management</a> page and fill out the form for a free consultation.</p>
														</div>
													</div>
												</div>

												<!-- <div class="card">
													<div class="card-header" id="faqhead7">
														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq7" aria-expanded="true" aria-controls="faq7">
															<p><span>8. </span> Dummy questions here?</p>
														</a>
													</div>
													<div id="faq7" class="collapse" aria-labelledby="faqhead7" data-parent="#faq">
														<div class="card-body">
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
														</div>
													</div>
												</div>

												<div class="card">
													<div class="card-header" id="faqhead8">
														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq8" aria-expanded="true" aria-controls="faq8">
															<p><span>9. </span> how  Rivirtual work?</p>
														</a>
													</div>
													<div id="faq8" class="collapse" aria-labelledby="faqhead8" data-parent="#faq">
														<div class="card-body">
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
														</div>
													</div>
												</div>

												<div class="card">
													<div class="card-header" id="faqhead9">
														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq9" aria-expanded="true" aria-controls="faq9">
															<p><span>10. </span> Dummy questions here?</p>
														</a>
													</div>
													<div id="faq9" class="collapse" aria-labelledby="faqhead9" data-parent="#faq">
														<div class="card-body">
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
														</div>
													</div>
												</div> -->
									</div>
								</div>
							</div>

							<div class="tab-pane " id="tab2">
								<div class="accordian-faq">
									<div class="accordion" id="accordionExamplefour">
												<div class="card">
													<div class="card-header" id="faqhead21">
														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq21" aria-expanded="true" aria-controls="faq21">
															<p><span>1. </span> How to Sign up as an Agent?</p>
														</a>
													</div>
													<div id="faq21" class="collapse" aria-labelledby="faqhead21" data-parent="#accordionExamplefour">
														<div class="card-body">
															<p>Follow the steps below:
																<ul>
																	<li>1.  	Click on the Sign-in button at the top right corner.</li>
																	<li>2.  	Click on Sign Up Now at the bottom of the pop-up form.</li>
																	<li>3.  	Enter your details and click Sign Up or Sign Up by your Facebook account or Google account.</li>
																</ul>
															</p>
														</div>
													</div>
												</div>


												<div class="card">
													<div class="card-header" id="faqhead22">
														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq22" aria-expanded="true" aria-controls="faq22">
															<p><span>2. </span> How to hire an Agent?</p>
														</a>
													</div>
													<div id="faq22" class="collapse" aria-labelledby="faqhead22" data-parent="#accordionExamplefour">
														<div class="card-body">
															<p>Click on the Agent from the top menu, enter your locality pin code, and click the Find an Agent button. You will get a list of top agents in your area. You can apply different filters to the list. You can view their profile and choose the best one you like.</p>
														</div>
													</div>
												</div>

												<div class="card">
													<div class="card-header" id="faqhead23">
														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq23" aria-expanded="true" aria-controls="faq23">
															<p><span>3. </span> How will I know if my property is verified?</p>
														</a>
													</div>
													<div id="faq23" class="collapse" aria-labelledby="faqhead23" data-parent="#accordionExamplefour">
														<div class="card-body">
															<p>When your property is verified on your registered email account, you will receive mail.</p>
														</div>
													</div>
												</div>

												<div class="card">
													<div class="card-header" id="faqhead24">
														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq24" aria-expanded="true" aria-controls="faq24">
															<p><span>4. </span> How can I chat with customers?</p>
														</a>
													</div>
													<div id="faq24" class="collapse" aria-labelledby="faqhead24" data-parent="#accordionExamplefour">
														<div class="card-body">
															<p>RiVirtual only activates the chatting feature when a User initiates the chatting.</p>
														</div>
													</div>
												</div>

												<div class="card">
													<div class="card-header" id="faqhead25">
														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq25" aria-expanded="true" aria-controls="faq25">
															<p><span>5. </span> How to manage the availability of agents?</p>
														</a>
													</div>
													<div id="faq25" class="collapse" aria-labelledby="faqhead25" data-parent="#accordionExamplefour">
														<div class="card-body">
															<p>When you go to the Manage Visit Availability section, you need to enter your availability hours for the week starting from Monday. This will reflect for the coming weeks. you can edit it anytime.</p>
														</div>
													</div>
												</div>


												
									</div>
								</div>
							</div>

							<div class="tab-pane " id="tab3">
							<div class="accordian-faq">
									<div class="accordion" id="accordionExampletwo">
												<div class="card">
													<div class="card-header" id="faqhead31">
														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq31" aria-expanded="true" aria-controls="faq31">
															<p><span>1. </span> How to Sign up as a Pro (Service Provider)?</p>
														</a>
													</div>
													<div id="faq31" class="collapse" aria-labelledby="faqhead31" data-parent="#accordionExampletwo">
														<div class="card-body">
															<p>Follow the steps below:
																<ul>
																	<li>1.  	Click on the Sign-in button at the top right corner.</li>
																	<li>2.  	Click on Sign Up Now at the bottom of the pop-up form.</li>
																	<li>3.  	Enter your details and click Sign Up or Sign Up by your Facebook account or Google account.</li>
																</ul>
															</p>
														</div>
													</div>
												</div>

												<div class="card">
													<div class="card-header" id="faqhead32">
														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq32" aria-expanded="true" aria-controls="faq32">
															<p><span>2. </span> How to Hire a Pro?</p>
														</a>
													</div>
													<div id="faq32" class="collapse" aria-labelledby="faqhead32" data-parent="#accordionExampletwo">
														<div class="card-body">
															<p>Click on “Hire a Pro” on the top menu. From the “Hire a pro” page, you can fill the required entries in the search box and click search. You can apply additional filters to the list.</p>
														</div>
													</div>
												</div>

												<div class="card">
													<div class="card-header" id="faqhead33">
														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq33" aria-expanded="true" aria-controls="faq33">
															<p><span>3. </span> How to create a Job?</p>
														</a>
													</div>
													<div id="faq33" class="collapse" aria-labelledby="faqhead33" data-parent="#accordionExampletwo">
														<div class="card-body">
															<p>Click on post a service requirement from the drop-down menu beside your profile picture. Fill out the required entries, upload the job picture, and click on the Post Service button. You can see all your Service requirements from the My service requirements section.</p>
														</div>
													</div>
												</div>

												<div class="card">
													<div class="card-header" id="faqhead34">
														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq34" aria-expanded="true" aria-controls="faq34">
															<p><span>4. </span> How to apply for a Job?</p>
														</a>
													</div>
													<div id="faq34" class="collapse" aria-labelledby="faqhead34" data-parent="#accordionExampletwo">
														<div class="card-body">
															<p>From the list of jobs available for you, click on view more for the job you want. If you are interested in that job, click on the “I am interested” button on the right side of the page. In the pop-up, write the amount you want to quote, the number of days the job will take, and any description if you want to make it. Click Save, and the customer will receive your request with your quotes.</p>
														</div>
													</div>
												</div>

												<div class="card">
													<div class="card-header" id="faqhead35">
														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq35" aria-expanded="true" aria-controls="faq35">
															<p><span>5. </span> How to chat with the customer?</p>
														</a>
													</div>
													<div id="faq35" class="collapse" aria-labelledby="faqhead35" data-parent="#accordionExampletwo">
														<div class="card-body">
															<p>The chatting feature will only be enabled when the customer initiates the chat.</p>
														</div>
													</div>
												</div>


												<!-- <div class="card">
													<div class="card-header" id="faqhead26">
														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq26" aria-expanded="true" aria-controls="faq26">
															<p><span>6. </span> simple questions here?</p>
														</a>
													</div>
													<div id="faq26" class="collapse" aria-labelledby="faqhead26" data-parent="#accordionExampletwo">
														<div class="card-body">
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
														</div>
													</div>
												</div> -->
									</div>
								</div>
						</div>

						<div class="tab-pane " id="tab4">
							<div class="accordian-faq">
									<div class="accordion" id="accordionExamplethree">
												<div class="card">
													<div class="card-header" id="faqhead51">
														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq51" aria-expanded="true" aria-controls="faq51">
															<p><span>1. </span> What are the commission rates for Agents?</p>
														</a>
													</div>
													<div id="faq51" class="collapse" aria-labelledby="faqhead51" data-parent="#accordionExamplethree">
														<div class="card-body">
															<p>The market standard rate for Agents is 2%, but we recommend you discuss rates with your agent beforehand.</p>
														</div>
													</div>
												</div>

												<div class="card">
													<div class="card-header" id="faqhead52">
														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq52" aria-expanded="true" aria-controls="faq52">
															<p><span>2. </span> How to pay the Agents?</p>
														</a>
													</div>
													<div id="faq52" class="collapse" aria-labelledby="faqhead52" data-parent="#accordionExamplethree">
														<div class="card-body">
															<p>After the successful closing of the deal, the User can pay the pre-decided commission to the agent by any mode agreed by both of them.</p>
														</div>
													</div>
												</div>

												<div class="card">
													<div class="card-header" id="faqhead53">
														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq53" aria-expanded="true" aria-controls="faq53">
															<p><span>3. </span> How does the Escrow payment method work?</p>
														</a>
													</div>
													<div id="faq53" class="collapse" aria-labelledby="faqhead53" data-parent="#accordionExamplethree">
														<div class="card-body">
															<p>After a customer awards the job to a Pro, he can create several milestones for the Job. For every milestone, he has to deposit a part of the sum in the Escrow account of RiVirtual. After compilation of the milestone, the customer will release the sum deposited for it, which will reach the Pro.</p>
														</div>
													</div>
												</div>

												<!-- <div class="card">
													<div class="card-header" id="faqhead54">
														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq54" aria-expanded="true" aria-controls="faq54">
															<p><span>4. </span> Dummy questions here?</p>
														</a>
													</div>
													<div id="faq54" class="collapse" aria-labelledby="faqhead54" data-parent="#accordionExamplethree">
														<div class="card-body">
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
														</div>
													</div>
												</div>

												<div class="card">
													<div class="card-header" id="faqhead55">
														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq55" aria-expanded="true" aria-controls="faq55">
															<p><span>5. </span>Dummy questions here go?</p>
														</a>
													</div>
													<div id="faq55" class="collapse" aria-labelledby="faqhead55" data-parent="#accordionExamplethree">
														<div class="card-body">
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
														</div>
													</div>
												</div> -->
									</div>
								</div>
						</div>



					</div>
				</div>

			</div>
		</div>
	</div>
    @endsection


@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection



@section('script')
@include('includes.script')
@include('includes.toaster')

@endsection
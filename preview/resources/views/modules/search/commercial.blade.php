@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> Find perfect Commercial Space - RiVirtual:  #1 Commercial Real Estate Advisor Platform in India</title>
<meta name="keywords" content="rivirtual, buy a property, rent a property, hire a pro, residential real estate, commercial real estate, commercial real estate marketing, commercial real estate advertising, commercial, land for sale, commercial properties, commercial real estate for lease, commercial auctions,Properties for rent" />
<meta name="description" content="Find Commercialreal estate for sale, lease & auction on the # 1 real estate advisor and advertising marketplace." />
<meta name="robots" content="NOODP, NOYDIR, follow" />
<meta name="X-TrackingAnalytic" content="G-77HVYFKHNB" />
<meta property="og:title" content=" Find perfect Commercial Space - RiVirtual:  #1 Commercial Real Estate Advisor Platform in India "/>
<meta property="og:description" content="RiVirtual - Exclusive Real Estate Intelligence Platform Lookup for Commercial properties for rent,buy,sell in India Find real estate for sale, lease & auction on the # 1 real estate advisor and advertising marketplace"/>
<meta property="og:url" content="https://www.rivirtual.in"/>
<meta property="og:image" content="https://www.rivirtual.in/public/frontend/images/RiVirtusl_logosmall.jpg"/>
@endsection




@section('header')
@include('includes.header')
@endsection

@section('content')

<div class="haeder-padding"></div>
<section class="agents-section">
	<div class="banner-image">
		<img src="{{ url('public/frontend/images/commercialt-img.jpg') }}" alt="banner-image">
	</div>
	<div class="banner-text">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 com_pad">
					<div class="text-center">
						<h1>Find  perfect commercial space</h1>

						

                        <form class="banner-form" action="{{route('search.property')}}" method="POST">
                            @csrf
                            <input type="hidden" name="master_property_type" value="COM">
						  	 <ul class="nav nav-pills" role="tablist">
							    <li class="nav-item">
                                    <a class="nav-link active" data-toggle="pill" href="javascript:;" id="buy_link">
                                      <img src="{{ URL::to('public/frontend/images/tab1h.png')}}" alt="buy" class="hovern">
                                      <img src="{{ URL::to('public/frontend/images/tab1.png')}}" alt="buy" class="hoverb">
                                      BUY
                                    </a>
                                    <div class="radiobx">
                                        <input type="radio" id="buy" name="property_for[]" value="B"  checked>
                                        {{-- <label for="buy">For Buy</label> --}}
                                    </div>
							    </li>
							    <li class="nav-item">
                                    <a class="nav-link" data-toggle="pill" href="javascript:;" id="rent_link">
                                        <img src="{{ URL::to('public/frontend/images/tab2h.png')}}" alt="rent" class="hovern">
                                        <img src="{{ URL::to('public/frontend/images/tab2.png')}}" alt="rent" class="hoverb">
                                        Rent
                                    </a>
                                    <div class="radiobx">
                                        <input type="radio" id="rent" name="property_for[]" value="R" >
                                        {{-- <label for="rent">For Rent</label> --}}

                                    </div>
							    </li>

							  </ul>
							  <div class="tab-content">
                                <div id="home" class="tab-pane active">
                                    <div class="from-sec-banner">
                                        <div class="input-from">
                                            <div class="form-group borders">
                                                <div class="input-with-icon">
                                                  <input type="text" class="form-control" placeholder="Location" name="location" id="location" onkeyup="search_result_check(this);" autocomplete="off">
                                                  <img src="{{ URL::to('public/frontend/images/map1.png')}}">
                                                </div>
                                                <ul id="serch_result" class="search_sajasation" style="display: none"></ul>
                                            </div>
                                            <div class="form-group ">
                                                <div class="input-with-icon">
                                                    <a class="user_llllk"> <span>Property Type</span> <img src="{{ url('public/frontend/images/caret.png') }}" class="caret-img"> </a>
                                                  <img src="{{ URL::to('public/frontend/images/house1.png')}}">


                                                  <div class="show03 filter-category" style="display: none;">
                                        <div class="diiferent-sec">
                                            <ul class="category-ul">
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate1" name="property_type[]" value="OSP" >
                                                        <label for="rate1">Office Space</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate2" name="property_type[]" value="WRH" >
                                                        <label for="rate2">Warehouse</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate5" name="property_type[]" value="SHR" >
                                                        <label for="rate5">Showrooms</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate3" name="property_type[]" value="INDT" >
                                                        <label for="rate3">Industrial</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate4" name="property_type[]" value="RET" >
                                                        <label for="rate4">Retail</label>
                                                    </div>
                                                </li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                              </div>


                                            </div>
                                        </div>
                                        <div class="search-box">
                                            <div class="sub2-lap">
                                                <img src="{{ URL::to('public/frontend/images/search.png')}}" alt="search">
                                            </div>
                                            <input type="submit" value="Search">
                                        </div>
                                    </div>
                                </div>
                            </div>
						</form>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<div class="about-inner pt-50">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-6 align-self-center">
					
                            
                            <div class="commerical_img">
                                <img src="{{ url('public/frontend/images/commer_img.jpg') }}" alt="video popup bg image">
                            </div>
                           
				</div>

				<div class="col-md-6 align-self-center">
					<div class="about-us-info-wrap">
						
						<h1 class="section-title mt-0">#1 Commercial Real Estate Advisor Platform in India<span>.</span></h1>
                        <p><span style="font-size:20px">India's real estate sector is expected to touch a US$ 1 trillion market size by 2030, accounting for 18-20% of India's GDP.</span> </p>
                        <p>More than 85% of commercial real estate decisions begin with an online search. Your next tenant or investor will see your property first online, only if it’s on RiVirtual. RiVirtual listings achieve greater visibility than those anywhere else on the web. In an era where the first tour happens online, give your property the winning advantage.</p>
                        <p>Our trained experts can show you how our industry-trusted solutions are driving the success of today's commercial real estate professiona</p>
                        <a href="{{ route('contact.us') }}" class="theme-btn mt-3">Contact Us</a>
					</div>
				</div>
			</div>
		</div>
	</div>

<section class="property-sec py-50">
	<div class="container">
		<div class="row">
			<div class="col-lg-10 col-xl-9 col-md-11 mx-auto">
				<div class="section-heading2 text-center">
					<h2>What our Clients say.</h2>
					<p>Your views and insights are important to us. We’re delighted when you tell us we can share these more widely and are proud to be able to give you access to these testimonials.</p>
				</div>
			</div>

			<div class="col-lg-10 col-xl-11 col-md-11 mx-auto revew_sec_agent2">
				<div class="revew_panel">
							<div class="revew_item">
								<div class="revew_top">
									<div class="media">
										
										<div class="media-body">
											<h4>Rakesh Reddy</h4>
											<div class="star_bx">
											<ul>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star-half-o"></i></li>
											</ul>
											<strong><img src="{{ url('public/frontend/images/calendar2.png') }}" alt=""> 17th March, 2022</strong>
										</div>
										</div>
									</div>
								</div>
								<p>Professional, knowledgeable, and friendly Marketplace and excellent customer service</p>
							</div>

							<div class="revew_item">
								<div class="revew_top">
									<div class="media">
										
										<div class="media-body">
											<h4>Anirban Goswami</h4>
											<div class="star_bx">
											<ul>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star-half-o"></i></li>
											</ul>
											<strong><img src="{{ url('public/frontend/images/calendar2.png') }}" alt=""> 1st March, 2022</strong>
										</div>
										</div>
									</div>
								</div>
								<p>Outstanding Realestate Market place and get advice and insights.</p>
							</div>

							<div class="revew_item">
								<div class="revew_top">
									<div class="media">
										
										<div class="media-body">
											<h4>Raghavendra Kumar</h4>
											<div class="star_bx">
											<ul>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star-half-o"></i></li>
											</ul>
											<strong><img src="{{ url('public/frontend/images/calendar2.png') }}" alt=""> 15th Feb, 2022</strong>
										</div>
										</div>
									</div>
								</div>
								<p>Extremely Satisfied Customer and homeowner</p>
							</div>

							<div class="revew_item">
								<div class="revew_top">
									<div class="media">
										
										<div class="media-body">
											<h4>Sujit Kumary</h4>
											<div class="star_bx">
											<ul>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star-half-o"></i></li>
											</ul>
											<strong><img src="{{ url('public/frontend/images/calendar2.png') }}" alt=""> 5th Feb, 2022</strong>
										</div>
										</div>
									</div>
								</div>
								<p>Best Real estate advisor in India</p>
							</div>

							<div class="revew_item">
								<div class="revew_top">
									<div class="media">
										
										<div class="media-body">
											<h4>Eshwar Setineni</h4>
											<div class="star_bx">
											<ul>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star-half-o"></i></li>
											</ul>
											<strong><img src="{{ url('public/frontend/images/calendar2.png') }}" alt=""> 11th Jan, 2022</strong>
										</div>
										</div>
									</div>
								</div>
								<p>Best Real Estate advice I ever got</p>
							</div>

							<!-- <div class="revew_item">
								<div class="revew_top">
									<div class="media">
										
										<div class="media-body">
											<h4>Akashbev Roy</h4>
											<div class="star_bx">
											<ul>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star-half-o"></i></li>
											</ul>
											<strong><img src="{{ url('public/frontend/images/calendar2.png') }}" alt=""> 20th Sept, 2021</strong>
										</div>
										</div>
									</div>
								</div>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the of Letraset sheets containing  It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, Lorem Ipsum has been the of Letraset sheets containing  Lorem Ipsum passages..</p>
							</div> -->
							
						</div>
			</div>

		</div>



	</div>
</section>
@endsection

@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection

@section('script')
@include('includes.script')
@include('includes.toaster')
<script>
	function search_result_check(that) {

        var $this = that;
        var name = $($this).val();

        console.log($($this).val());
        $.ajax({
            type: "POST",
            url:"{{route('get.locality.available')}}",
            data: {
                'name': name,
                '_token':'{{@csrf_token()}}'
            },
            success: function(data) {
              if(data){
                console.log(data);
                $('#serch_result').show();
                $('#serch_result').fadeIn();
                $('#serch_result').last().html(data);

              }
              else
              {
                $('#serch_result').children().remove();
              }
            }
        });
    }
    function add_searchbar(that) {
        var $this = that;
        var name = $($this).text();
        console.log(name);
        $('#location').val(name);
        $($this).remove();
        $('#serch_result').children().remove();
        $('#serch_result').hide();
    }
</script>

@endsection
@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> The right Loan could save you Lakhs - RiVirtual:  #1 Residential Real Estate Advisor Platform in India</title>
<meta name="keywords" content="rivirtual, buy a property, property loans, hire a pro, residential real estate loans, commercial real estate loans , flats for rent, Apartments for rent, flats for sale, apartments for sale, Properties for rent, commercial real estate for lease, commercial auctions,Properties for rent" />
<meta name="description" content=" A simpler way to buy or refinance .Whether you are a first-time home buyer or an experienced real estate investor, having a loans team you can count on is crucial. RiVirtual has excellent communication and easy accessibility when you need us. Our job is to guide you every step of the way. " />
<meta name="robots" content="NOODP, NOYDIR, follow" />
<meta name="X-TrackingAnalytic" content="G-77HVYFKHNB" />
<meta property="og:title" content=" > The right Loan could save you Lakhs - RiVirtual:  #1 Residential Real Estate Advisor Platform in India "/>
<meta property="og:description" content="RiVirtual - A simpler way to buy or refinance .Whether you are a first-time home buyer or an experienced real estate investor, having a loans team you can count on is crucial. RiVirtual has excellent communication and easy accessibility when you need us. Our job is to guide you every step of the way."/>
<meta property="og:url" content="https://www.rivirtual.in"/>
<meta property="og:image" content="https://www.rivirtual.in/public/frontend/images/RiVirtusl_logosmall.jpg"/>
@endsection




@section('header')
@include('includes.header')
@endsection

@section('content')

<div class="haeder-padding"></div>


<div class="sell_ban_sec ln_pg">
	<img src="{{ url('public/frontend/images/lon_ban.jpg') }}" alt="">
	<div class="sell_ban_bx">
		<div class="container">
			<div class="con_loan_pro">
			<div class="sell_ban_bx_inr">
				<h1> The right Loan could save you Lakhs </h1>
				
				<ul class="ex_ul_commercial">
					<li><span><i class="icofont-tick-mark"></i></span> 24/7 support</li>
					<li><span><i class="icofont-tick-mark"></i></span> 1% commissions and bank fees</li>
					<li><span><i class="icofont-tick-mark"></i></span> Simple online experience</li>
				</ul>

			</div>
			<div class="property_manag_rig">
				<div class="property_manag_from">
					<h4> RiVirtual Loans </h4>
					<p> Get in touch with our Loan expert</p>
					<form method="POST" action="{{route('save.loan.request')}}" id="myForm">
						@csrf
						<div class="das_input">
							<input type="text" placeholder="Name" value="" name="name">
						</div>
						<div class="das_input mt-0">
							<input type="text" placeholder="Phone" value="" name="phone">
						</div>
						<div class="das_input mt-0">
							<input type="text" placeholder="Email" value="" name="email">
						</div>
						<div class="das_input mt-0">
							<select name="service_city">
								<option hidden value="">Choose Service City</option>
								<option value="Bangalore">Bangalore</option>
								<option value="Hyderabad">Hyderabad</option>
								<option value="Chennai">Chennai</option>
								<option value="Mumbai">Mumbai</option>
								<option value="Pune">Pune</option>
								<option value="Delhi">Delhi</option>
								<option value="Noida">Noida</option>
								<option value="Greater Noida">Greater Noida</option>
								<option value="Ghaziabad">Ghaziabad</option>
								<option value="Faridabad">Faridabad</option>
								<option value="Gurgaon">Gurgaon</option>
								<option value="Other City">Other City</option>
							</select>
						</div>
						<div class="das_input mt-0">
							<select name="user_type">
								<option hidden value="" >I am</option>
								<option value="Owner">Owner</option>
								<option value="Tenant">Tenant</option>
								<option value="Seller">Seller</option>
								<option value="Buyer">Buyer</option>
							</select>
						</div>
						<div class="manage_sub">
							<input type="submit" value="Request a Loan" class="see_cc">
						</div>
						<div class="manage_tx">
							<!-- <span>Call or WhatsApp us <a href="tel:918793456789">+918793456789 </a></span> -->
						</div>
					</form>
				</div>
			</div>
			<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
<div class="traditionally_sec lon_sec_div">
	<div class="container">
		<div class="traditionally_ir">
			<div class="traditionally_img">
				<img src="{{ url('public/frontend/images/loneimg1.jpg') }}" alt="">
			</div>
			<div class="traditionally_text">
				<h2>A simpler way to buy or refinance</h2>
				<ul class="avil_yexts">
					<li><h2> Available 24/7</h2>
						<p>Our site is available on nights and weekends, so you can get started whenever you’re ready.
						</p>
					</li>
					<li><h2>Honest rate quotes</h2>
						<p>No bait-and-switch. No hidden fees. Just transparent, upfront pricing.</p>
					</li>
					<li><h2>Instant loan estimate</h2>
						<p>Don‘t wait three days to know what you‘ll owe. We‘ll give you a Loan Estimate in seconds.</p>
					</li>
					<li><h2>Simple, 100% online process</h2>
						<p>Log in anytime to see exactly where you are in the process and keep</p>
					</li>
				</ul>
				
				
			</div>
		</div>
		<div class="traditionally_ir">
				<div class="traditionally_img">
					<img src="{{ url('public/frontend/images/loneimg2.jpg') }}" alt="">
				</div>
				<div class="traditionally_text">
					<h2>Better Real Estate Loans</h2>
					<p>Whether you are a first-time home buyer or an experienced real estate investor, having a loans team you can count on is crucial.</p>
					<p>RiVirtual has excellent communication and easy accessibility when you need us. Our job is to guide you every step of the way.</p>
					<p>Fill out the questionnaire on this page to start discussing your home loan needs today!
					</p>
					
					
			</div>
		</div>
		<div class="traditionally_ir">
			<div class="traditionally_img">
				
			</div>
			<div class="traditionally_text">
				<h2>Our Banking Partners</h2>
			</div>
			<div class="clearfix">
			<div class="logo_bank_caros">
					<div class="bank_logo_slid">
						<div class="owl-carousel">
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn1.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn2.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn3.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn4.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn5.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn6.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn7.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn8.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn9.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn10.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn11.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn12.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn13.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn14.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn15.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn16.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn17.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn18.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn19.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn20.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn21.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn22.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn23.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn24.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn25.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn26.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn27.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn28.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn29.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn30.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn31.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn32.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn33.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn34.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn35.png')}}">
								</div>
							</div>
							<div class="item">
								<div class="bank_logos">
									<img src="{{asset('public/frontend/images/bn36.png')}}">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
@endsection


@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection



@section('script')
@include('includes.script')
@include('includes.toaster')
<script>
     $('#myForm').validate({
        rules:{
            name:{
                required:true,
            },
            email:{
                required:true,
                email:true,
            },
            phone:{
                required:true,
                digits:true,
                maxlength:10,
                minlength:10,
            },
            service_city:{
                required:true,
            },
            user_type:{
                required:true
            }
        }

     });
</script>

@endsection
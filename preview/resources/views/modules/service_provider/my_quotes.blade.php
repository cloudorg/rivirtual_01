@extends('layouts.app')
@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> RiVirtual | My Quotes </title>
@endsection
@section('header')
@include('includes.header')
@endsection


@section('content')
<!----header--->
<div class="haeder-padding"></div>

<div class="dashboard-provider">

    <div class="container">

        <div class="row">

            @include('includes.sidebar')
            <div class="col-lg-9 col-md-12 col-sm-12">

                <div class="cus-dashboard-right">

                   <h2 class="pro-heading-das">My Quotes</h2> 

                </div>
                @include('includes.message')

                <div class="prod-dash-right">

                    <div class="agent-right-body">

                        <form action="{{route('service.provider.my.quotes')}}" method="get">

                            <div class="user_dashboard_form">

                                <div class="row">

                                    <div class="col-md-10">

                                        <div class="row">

                                            <div class="col-md-4">

                                                <div class="das_input">

                                                    <label> Status</label>

                                                        <select name="status">

                                                            <option value="">Select Status  </option>

                                                            <option value="Bid Placed" @if(@$key['status'] == 'Bid Placed') selected @endif>Bid Placed</option>

                                                            <option value="Awarded" @if(@$key['status'] == 'Awarded') selected @endif>Awarded</option>

                                                            <option value="Revoked" @if(@$key['status'] == 'Revoked') selected @endif>Revoked</option>

                                                        </select>

                                                </div>

                                            </div>

                                            <div class="col-md-4">

                                                <div class="das_input">

                                                    <label>Jobs by Name</label>

                                                    <input type="text" placeholder="Enter here.." name="keyword" value="{{@$key['keyword']}}" > 
                                                </div>

                                            </div>

                                            <div class="col-md-4">

                                                <div class="das_input">

                                                    <label>Date</label>

                                                    <div class="dash-d">

                                                        <input type="text" placeholder="From date" id="datepicker" name="date" value="{{@$key['date']}}"> <span class="over_llp1"><img src="{{ asset('public/frontend/images/cala.png') }}" alt=""></span> </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="col-md-2 col-sm-4">

                                        <button class="dash-btn"> <img src="{{ asset('public/frontend/images/ser.png') }}"> Search </button>

                                    </div>

                                    <input type="hidden" name="tab" id="tab" value="{{@$key['tab']}}">

                                    <div class="col-md-12">

                                        <div class="bodar"> </div>

                                    </div>

                                </div>

                            </div>

                        </form>



                        <div class="main-tabels">

                            <!-- Nav tabs -->

                            <div class="tab-rii">
                                <ul class="nav nav-tabs custom_tab">

                                    <li class="nav-item tab1"> <a class="nav-link" data-toggle="tab" data-tab="1" href="#today">Open </a> </li>

                                    <li class="nav-item tab2"> <a class="nav-link " data-toggle="tab" data-tab="2" href="#menu2">In Progress  </a> </li>

                                    <li class="nav-item tab3"> <a class="nav-link" data-toggle="tab" data-tab="3" href="#menu3">Closed </a> </li>

                                    <li class="nav-item tab4"> <a class="nav-link"data-toggle="tab" data-tab="4" href="#menu4">All </a> </li>

                                </ul>
                            </div>

                            <div class="tab-content">

                                <div class="tab-pane fade" id="today">

                                    <div class="custom-table">

                                        <div class="new-table-mr">

                                            <div class="table">

                                                <div class="one_row1 hidden-sm-down only_shawo">

                                                    <div class="cell1 tab_head_sheet">Title </div>

                                                    <div class="cell1 tab_head_sheet">Date </div>

                                                    <div class="cell1 tab_head_sheet">Location </div>

                                                    <div class="cell1 tab_head_sheet">Amount</div>

                                                    <div class="cell1 tab_head_sheet">Status</div>

                                                    <div class="cell1 tab_head_sheet">Action </div>

                                                </div>

                                                @if(count(@$open)>0)

                                                    <!--row 1-->
                                                    @foreach(@$open as $val)
                                                    <div class="one_row1 small_screen31">

                                                        <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Title  </span>

                                                            <p class="add_ttrr"><a href="{{route('job.details',@$val->jobDetails->slug)}}">@if(strlen(@$val->jobDetails->job_name)>20) {{substr(@$val->jobDetails->job_name,0,20)}}...@else{{@$val->jobDetails->job_name}}@endif</a>  </p>

                                                        </div>

                                                        

                                                        <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>

                                                            <p class="add_ttrr">{{date('m.d.Y',strtotime(@$val->jobDetails->job_start_date))}}</p>

                                                        </div>

                                                        <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Location </span>

                                                            <p class="add_ttrr">
                                                                @if(strlen(@$val->jobDetails->address) >10)
                                                                    {{substr(@$val->jobDetails->address,0,10)}}...
                                                                @else
                                                                    {{@$val->jobDetails->address}}
                                                                @endif
                                                            </p>

                                                        </div>

                                                        <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Amount</span>

                                                            <p class="add_ttrr">
                                                                @if(@$val->jobDetails->budget_range_from != null)
                                                                    ₹{{@$val->jobDetails->budget_range_from}}
                                                                @else
                                                                   --- 
                                                                @endif
                                                            </p>

                                                        </div>

                                                        <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Status </span>

                                                            <p class="add_ttrr">
                                                               {{@$val->status}}
                                                                    
                                                            </p>

                                                        </div>

                                                        <div class="cell1 tab_head_sheet_1 "> <span class="W55_1">Action</span>

                                                            <div class="add_ttrr actions-main text-center">

                                                                <a href="javascript:void(0);" class="action-dots" id="action{{@$val->id}}"><img src="
                                                                    {{asset('public/frontend/images/action-dots.png') }}" alt=""></a>

                                                                <div class="show-actions" id="show-action{{@$val->id}}" style="display: none;"> <span class="angle"><img src="{{asset('public/frontend/images/angle.png') }}" alt=""></span>

                                                                    <ul class="text-left">

                                                                        <li><a href="{{route('service.provider.view.quote',@$val->id)}}">View Quote</a></li>

                                                                        <li><a href="{{route('job.details',@$val->jobDetails->slug)}}">Edit Quotes </a></li>

                                                                        <li><a href="{{route('service.provider.delete.quotes',@$val->id)}}" onclick="return confirm('Do you want to delete this quote?')">Delete</a></li>

                                                                    </ul>

                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div>
                                                    
                                                    @endforeach
                                                    @php
                                                        $req=request()->except(['page1', '_token']);
                                                        $req['tab']='1'
                                                    @endphp
                                                    <nav aria-label="...">

                                                        <ul class="pagination new-pagination">
                                                            {{@$open->appends($req,'page1')->links()}}
                                                           <!--  {{@$open->appends(request()->except(['page', '_token']))->links()}} -->
                                                            {{-- <li class="page-item"> <span class="page-link">Prev </span> </li>

                                                            <li class="page-item active"><a class="page-link" href="#">1</a></li>

                                                            <li class="page-item" aria-current="page"> <span class="page-link">

                                                            2

                                                            <span class="sr-only">(current)</span> </span>

                                                            </li>

                                                            <li class="page-item"><a class="page-link" href="#">3</a></li>

                                                            <li class="page-item"><a class="page-link" href="#">4</a></li>

                                                            <li class="page-item"> <a class="page-link act" href="#">Next </a> </li> --}}

                                                        </ul>
                                                    </nav>
                                                @else
                                                    <div class="one_row1 small_screen31">
                                                        <p>No data found</p>
                                                    </div>

                                                @endif

                                                <!--row 1-->

                                                

                                            </div>

                                        </div>

                                        

                                    </div>

                                </div>


                              
                                    
                                            <div class="tab-pane fade" id="menu2">

                                                <div class="custom-table">

                                                    <div class="new-table-mr">

                                                        <div class="table">

                                                            <div class="one_row1 hidden-sm-down only_shawo">

                                                                <div class="cell1 tab_head_sheet">Title </div>

                                                                

                                                                <div class="cell1 tab_head_sheet">Date </div>

                                                                <div class="cell1 tab_head_sheet">Location </div>

                                                                <div class="cell1 tab_head_sheet">Amount</div>

                                                                <div class="cell1 tab_head_sheet">Status</div>

                                                                <div class="cell1 tab_head_sheet">Action </div>

                                                            </div>
                                                            @if(count(@$awarded) >0)
                                                            @foreach(@$awarded as $val)
                                                            <!--row 1-->

                                                            <div class="one_row1 small_screen31">

                                                                <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Title  </span>

                                                                    <p class="add_ttrr"><a href="{{route('job.details',@$val->jobDetails->slug)}}">@if(strlen(@$val->jobDetails->job_name) > 20) {{substr(@$val->jobDetails->job_name,0,20)}}... @else {{@$val->jobDetails->job_name}} @endif</a></p>

                                                                </div>

                                                                

                                                                <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>

                                                                    <p class="add_ttrr">{{date('m.d.Y',strtotime(@$val->jobDetails->job_start_date))}}</p>

                                                                </div>

                                                                <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Location </span>

                                                                    <p class="add_ttrr">@if(strlen(@$val->jobDetails->address) >10)
                                                                    {{substr(@$val->jobDetails->address,0,10)}}...
                                                                @else
                                                                    {{@$val->jobDetails->address}}
                                                                @endif</p>

                                                                </div>

                                                                <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Amount</span>

                                                                    <p class="add_ttrr">
                                                                    @if(@$val->jobDetails->budget_range_from != null)
                                                                    ₹{{@$val->jobDetails->budget_range_from}}
                                                                    @else
                                                                   --- 
                                                                   @endif
                                                                    </p>

                                                                </div>

                                                                <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Status </span>

                                                                    <p class="add_ttrr">{{@$val->status}}</p>

                                                                </div>

                                                                <div class="cell1 tab_head_sheet_1 "> <span class="W55_1">Action</span>

                                                                    <div class="add_ttrr actions-main text-center">

                                                                        <a href="javascript:void(0);" class="action-dots" id="action1{{@$val->id}}"><img src="{{ asset('public/frontend/images/action-dots.png') }}" alt=""></a>

                                                                        <div class="show-actions" id="show-action1{{@$val->id}}" style="display: none;"> <span class="angle"><img src="{{ asset('public/images/angle.png') }}" alt=""></span>

                                                                            <ul class="text-left">

                                                                            <li><a href="{{route('service.provider.view.quote',@$val->id)}}">My Quote & Milestone</a></li>

                                                                            <li><a href="{{route('job.details',@$val->jobDetails->slug)}}" target="_blank"> View Job</a></li>

                                                                            </ul>

                                                                        </div>

                                                                    </div>

                                                                </div>

                                                            </div>

                                                            <!--row 1-->
                                                            @endforeach
                                                            @php
                                                            $req=request()->except(['page2', '_token']);
                                                            $req['tab']='2'
                                                            @endphp
                                                            <nav aria-label="...">

                                                                <ul class="pagination new-pagination">
                                                                    {{@$awarded->appends($req,'page2')->links()}}
                                                                    <!-- {{@$awarded->appends(request()->except(['page', '_token']))->links()}} -->
                                                                    {{-- <li class="page-item"> <span class="page-link">Prev </span> </li>

                                                                    <li class="page-item active"><a class="page-link" href="#">1</a></li>

                                                                    <li class="page-item" aria-current="page"> <span class="page-link">

                                                                    2

                                                                    <span class="sr-only">(current)</span> </span>

                                                                    </li>

                                                                    <li class="page-item"><a class="page-link" href="#">3</a></li>

                                                                    <li class="page-item"><a class="page-link" href="#">4</a></li>

                                                                    <li class="page-item"> <a class="page-link act" href="#">Next </a> </li> --}}

                                                                </ul>

                                                            </nav>

                                                            @else
                                                                <div class="one_row1 small_screen31">
                                                                    <p>No data found</p>
                                                                </div>
                                                            @endif


                                                        </div>

                                                    </div>
                                                    
                                                </div>

                                            </div>
                                    
                               

                                
                                <div class="tab-pane fade" id="menu3">

                                    <div class="custom-table">

                                        <div class="new-table-mr">

                                            <div class="table">

                                                <div class="one_row1 hidden-sm-down only_shawo">

                                                    <div class="cell1 tab_head_sheet">Title </div>

                                                    

                                                    <div class="cell1 tab_head_sheet">Date </div>

                                                    <div class="cell1 tab_head_sheet">Location </div>

                                                    <div class="cell1 tab_head_sheet">Amount</div>

                                                    <div class="cell1 tab_head_sheet">Status</div>

                                                    <div class="cell1 tab_head_sheet">Action </div>

                                                </div>
                                                @if(count(@$closed)>0)
                                                <!--row 1-->
                                                @foreach(@$closed as $val)
                                                <div class="one_row1 small_screen31">

                                                    <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Title  </span>

                                                        <p class="add_ttrr">
                                                            <a href="{{route('job.details',@$val->jobDetails->slug)}}">
                                                                @if(strlen(@$val->jobDetails->job_name) > 20) {{substr(@$val->jobDetails->job_name,0,20)}}... @else {{@$val->jobDetails->job_name}} @endif  
                                                            </a>
                                                        </p>

                                                    </div>

                                                    

                                                    <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>

                                                        <p class="add_ttrr">{{date('m.d.Y',strtotime(@$val->jobDetails->job_start_date))}}</p>

                                                    </div>

                                                    <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Location </span>

                                                        <p class="add_ttrr">
                                                            @if(strlen(@$val->jobDetails->address) >10)
                                                                    {{substr(@$val->jobDetails->address,0,10)}}...
                                                            @else
                                                                    {{@$val->jobDetails->address}}
                                                            @endif
                                                        </p>

                                                    </div>

                                                    <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Amount</span>

                                                        <p class="add_ttrr">
                                                              @if(@$val->jobDetails->budget_range_from != null)
                                                                    ₹{{@$val->jobDetails->budget_range_from}}
                                                                @else
                                                                   --- 
                                                                @endif 
                                                        </p>

                                                    </div>

                                                    <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Status </span>

                                                        <p class="add_ttrr">{{@$val->status}}</p>

                                                    </div>

                                                    <div class="cell1 tab_head_sheet_1 "> <span class="W55_1">Action</span>

                                                        <div class="add_ttrr actions-main text-center">

                                                            <a href="javascript:void(0);" class="action-dots" id="action2{{@$val->id}}"><img src="{{ asset('public/frontend/images/action-dots.png') }}" alt=""></a>

                                                            <div class="show-actions" id="show-action2{{@$val->id}}" style="display: none;"> <span class="angle"><img src="{{ asset('public/frontend/images/angle.png') }}" alt=""></span>

                                                                <ul class="text-left">

                                                                    <li><a href="{{route('service.provider.view.quote',@$val->id)}}">My Quote & Milestone</a></li>

                                                                    <li><a href="{{route('job.details',@$val->jobDetails->slug)}}" target="_blank"> View Job</a></li>
                                                                    <li><a href="{{route('service.provider.delete.quotes',@$val->id)}}" onclick="return confirm('Do you want to delete this quote?')">Delete</a></li>

                                                                </ul>

                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>
                                                @endforeach

                                                @php
                                                    $req=request()->except(['page3', '_token']);
                                                    $req['tab']='3'
                                                @endphp

                                                <nav aria-label="...">

                                                    <ul class="pagination new-pagination">
                                                                {{@$closed->appends($req,'page3')->links()}}
                                                                <!-- {{@$open->appends(request()->except(['page', '_token']))->links()}} -->
                                                                {{-- <li class="page-item"> <span class="page-link">Prev </span> </li>

                                                                <li class="page-item active"><a class="page-link" href="#">1</a></li>

                                                                <li class="page-item" aria-current="page"> <span class="page-link">

                                                                2

                                                                <span class="sr-only">(current)</span> </span>

                                                                </li>

                                                                <li class="page-item"><a class="page-link" href="#">3</a></li>

                                                                <li class="page-item"><a class="page-link" href="#">4</a></li>

                                                                <li class="page-item"> <a class="page-link act" href="#">Next </a> </li> --}}

                                                    </ul>
                                                </nav>

                                                

                                                @else
                                                <div class="one_row1 small_screen31">
                                                    <p>No data found</p>
                                                </div>
                                                @endif


                                            </div>

                                        </div>
                                        

                                    </div>

                                </div>
                                

                               
                                    <div class="tab-pane  fade" id="menu4">

                                        <div class="custom-table">

                                            <div class="new-table-mr">

                                                <div class="table">

                                                    <div class="one_row1 hidden-sm-down only_shawo">

                                                        <div class="cell1 tab_head_sheet">Title </div>

                                                        

                                                        <div class="cell1 tab_head_sheet">Date </div>

                                                        <div class="cell1 tab_head_sheet">Location </div>

                                                        <div class="cell1 tab_head_sheet">Amount</div>

                                                        <div class="cell1 tab_head_sheet">Status</div>

                                                        

                                                        <div class="cell1 tab_head_sheet">Action </div>

                                                    </div>
                                                    @if(count(@$all)>0)
                                                    <!--row 1-->
                                                    @foreach(@$all as $val)
                                                    <div class="one_row1 small_screen31">

                                                        <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Title  </span>

                                                            <p class="add_ttrr">
                                                                <a href="{{route('job.details',@$val->jobDetails->slug)}}">@if(strlen(@$val->jobDetails->job_name) > 20) {{substr(@$val->jobDetails->job_name,0,20)}}... @else {{@$val->jobDetails->job_name}} @endif
                                                                </a>
                                                            </p>

                                                        </div>

                                                        

                                                        <div class="cell1 tab_head_sheet_1 half-boxes">

                                                         <span class="W55_1">Date</span>

                                                            <p class="add_ttrr">{{date('m.d.Y',strtotime(@$val->jobDetails->job_start_date))}}</p>

                                                        </div>

                                                        <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Location </span>

                                                            <p class="add_ttrr">
                                                            @if(strlen(@$val->jobDetails->address) >10)
                                                                    {{substr(@$val->jobDetails->address,0,10)}}...
                                                            @else
                                                                    {{@$val->jobDetails->address}}
                                                            @endif
                                                            </p>

                                                        </div>

                                                        <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Amount</span>

                                                            <p class="add_ttrr">
                                                              @if(@$val->jobDetails->budget_range_from != null)
                                                                    ₹{{@$val->jobDetails->budget_range_from}}
                                                                @else
                                                                   --- 
                                                                @endif 
                                                            </p>

                                                        </div>

                                                        

                                                        <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Status </span>

                                                            <p class="add_ttrr">{{@$val->status}}</p>

                                                        </div>

                                                        <div class="cell1 tab_head_sheet_1 "> <span class="W55_1">Action</span>

                                                            <div class="add_ttrr actions-main text-center">

                                                                <a href="javascript:void(0);" class="action-dots" id="action3{{@$val->id}}"><img src="{{asset('public/frontend/images/action-dots.png') }}" alt=""></a>

                                                                <div class="show-actions" id="show-action3{{@$val->id}}" style="display: none;"> <span class="angle"><img src="{{ asset('public/frontend/images/angle.png') }}" alt=""></span>

                                                                    <ul class="text-left">
                                                                        @if(@$val->status == 'Bid Placed')
                                                                        <li><a href="{{route('service.provider.view.quote',@$val->id)}}">My Quote & Milestone</a></li>

                                                                        <li><a href="{{route('job.details',@$val->jobDetails->slug)}}">Edit Quotes </a></li>

                                                                        <li><a href="{{route('service.provider.delete.quotes',@$val->id)}}" onclick="return confirm('Do you want to delete this quote?')">Delete</a>
                                                                        </li>

                                                                        @elseif(@$val->status == 'Awarded')

                                                                        <li><a href="{{route('service.provider.view.quote',@$val->id)}}">My Quote & Milestone</a></li>

                                                                        <li><a href="{{route('job.details',@$val->jobDetails->slug)}}" target="_blank"> View Job</a></li>
                                                                        
                                                                        

                                                                        @elseif(@$val->status == 'Closed')

                                                                        <li><a href="{{route('service.provider.view.quote',@$val->id)}}">My Quote & Milestone</a></li>

                                                                        <li><a href="{{route('job.details',@$val->jobDetails->slug)}}" target="_blank"> View Job</a></li>
                                                                        <li><a href="{{route('service.provider.delete.quotes',@$val->id)}}" onclick="return confirm('Do you want to delete this quote?')">Delete</a>
                                                                        </li>

                                                                        @endif

                                                                    </ul>

                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div>
                                                    @endforeach
                                                    @php
                                                        $req=request()->except(['page4', '_token']);
                                                        $req['tab']='4'
                                                    @endphp
                                                   

                                                </div>

                                            </div>
                                            <nav aria-label="...">

                                                <ul class="pagination new-pagination">
                                                    {{@$all->appends($req,'page4')->links()}}
                                                    <!-- {{@$closed->appends(request()->except(['page', '_token']))->links()}} -->
                                                    {{-- <li class="page-item"> <span class="page-link">Prev </span> </li>

                                                    <li class="page-item active"><a class="page-link" href="#">1</a></li>

                                                    <li class="page-item" aria-current="page"> <span class="page-link">

                                                    2

                                                    <span class="sr-only">(current)</span> </span>

                                                    </li>

                                                    <li class="page-item"><a class="page-link" href="#">3</a></li>

                                                    <li class="page-item"><a class="page-link" href="#">4</a></li>

                                                    <li class="page-item"> <a class="page-link act" href="#">Next </a> </li> --}}

                                                </ul>

                                            </nav>

                                            
                                            @else
                                            <div class="tab-pane ">
                                            </div>
                                            @endif
                                        </div>

                                    </div>
                               



                                <div class="clearfix"></div>

                            </div>

                        </div>



                    </div>

                        

                    </div>

                </div>

                

            </div>

        </div>

        

    </div>

</div>

@endsection


@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection



@section('script')
@include('includes.script')
<script>
    $(document).on('click', function () {
        @foreach(@$open as $val)
        var $target = $(event.target);
        if (!$target.closest('#action{{@$val->id}}').length && $('#show-action{{@$val->id}}').is(":visible")) {
            $('#show-action{{@$val->id}}').slideUp();
        }
        @endforeach
    });

    $(document).ready(function() {
        @foreach(@$open as $val)
            $("#action{{@$val->id}}").click(function() {
                $("#show-action{{@$val->id}}").slideToggle();
            });
        @endforeach
    });
</script>

<script>
    $(document).on('click', function () {
        @foreach(@$awarded as $val)
        var $target = $(event.target);
        if (!$target.closest('#action1{{@$val->id}}').length && $('#show-action1{{@$val->id}}').is(":visible")) {
            $('#show-action1{{@$val->id}}').slideUp();
        }
        @endforeach
    });

    $(document).ready(function() {
        @foreach(@$awarded as $val)
            $("#action1{{@$val->id}}").click(function() {
                $("#show-action1{{@$val->id}}").slideToggle();
            });
        @endforeach
    });
</script>
<script>
    $(document).on('click', function () {
        @foreach(@$closed as $val)
        var $target = $(event.target);
        if (!$target.closest('#action2{{@$val->id}}').length && $('#show-action2{{@$val->id}}').is(":visible")) {
            $('#show-action2{{@$val->id}}').slideUp();
        }
        @endforeach
    });

    $(document).ready(function() {
        @foreach(@$closed as $val)
            $("#action2{{@$val->id}}").click(function() {
                $("#show-action2{{@$val->id}}").slideToggle();
            });
        @endforeach
    });
</script>
<script>
    $(document).on('click', function () {
        @foreach(@$all as $val)
        var $target = $(event.target);
        if (!$target.closest('#action3{{@$val->id}}').length && $('#show-action3{{@$val->id}}').is(":visible")) {
            $('#show-action3{{@$val->id}}').slideUp();
        }
        @endforeach
    });

    $(document).ready(function() {
        @foreach(@$all as $val)
            $("#action3{{@$val->id}}").click(function() {
                $("#show-action3{{@$val->id}}").slideToggle();
            });
        @endforeach
    });
</script>
<script>
    $('.del_quo').on('click',function(){
        var id = $(this).data('id');

        if(confirm('Do you want to delete this quote')){


        }
    });
</script>
<script>
    $(document).ready(function () {
        var tab_value = $('#tab').val()
        if(tab_value == '')
        {
            $('.tab1').addClass('active');
            $('#today').addClass('active');
            $('#today').addClass('show');
        }
        else
        {
            $('.tab'+tab_value).addClass('active');
            if(tab_value == 1){
                $('#today').addClass('active');
                $('#today').addClass('show');
            }else{
                $('#menu'+tab_value).addClass('active');
                $('#menu'+tab_value).addClass('show');
            }
        }
        $('.nav-link').click(function(){
            $('#tab').val($(this).data('tab'))
            console.log($('#tab').val());
        });
    });
</script>
@endsection

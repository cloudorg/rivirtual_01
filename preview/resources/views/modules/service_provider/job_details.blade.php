@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> RiVirtual | Dashbord </title>
@endsection




@section('header')
@include('includes.header')
@endsection


@section('content')
<!----header--->
<div class="haeder-padding"></div>

<div class="jobs-details">

    <div class="container">
        @include('includes.message')

        <div class="row">           

            <div class="col-lg-9 col-md-12 col-sm-12">

                <div class="main-dtls">

                    <div class="details-block">
                    
                        <div class="jobb_pic">
                            @if(@$job_image->image != null)
                            <img src="{{ URL::to('storage/app/public/service_image') }}/{{ @$job_image->image }}">
                            @else
                            <img src="{{ asset('public/frontend/images/ga5.png') }}">
                            @endif
                        </div>


                        <div class="jobs_infoo">
                                <h2>{{@$job_data->job_name}}</h2>

                            <div class="jobs-subject">

                                <h5> <img src="{{ asset('public/frontend/images/lists.png') }}">{{@$job_data->categoryName?@$job_data->categoryName->category_name: ''}}</h5>
                               
                                
                            </div>
                            <div class="jobs-subject">
                            <h5> <img src="{{ asset('public/frontend/images/lists.png') }}">{{@$job_data->subCategoryName?@$job_data->subCategoryName->name: ''}}</h5>
                            </div>
                        </div>
                        
                        <div class="w-100"></div>

                        <div class="hours-areas">
                            <ul>
                                <li>
                                    <div class="hour-box">
                                 <span><img src="{{ asset('public/frontend/images/mapd.png') }}"></span>
                                <h6>Location</h6>
                                 
                                    <p>
                                       
                                        {{ @$job_data->jobState->name }},
                                        {{ @$job_data->jobCity->name }}
                                    </p>

                        </div>
                            </li>
                             <li>



                               <div class="hour-box">



                                 <span><img src="{{ asset('public/frontend/images/bud4.png') }}"></span>



                                 <h6>Budget</h6>



                                 <p>
                                    @if(@$job_data->budget_range_from != null)
                                    ₹{{ @$job_data->budget_range_from }}
                                    @else
                                    --     
                                    @endif

                                 </p>



                               </div>



                             </li>



                             <li>



                               <div class="hour-box">



                                 <span><img src="{{ asset('public/frontend/images/calend.png') }}"></span>



                                 <h6>Deadline </h6>

                                @php
                                 $duration = strval(@$job_data->duration);
                                 
                                 date('Y-m-d', strtotime('+'.$duration .'days',strtotime(@$job_data->job_start_date)))

                                 
                                @endphp

                                 <p>{{date('jS M,Y', strtotime('+'.$duration .'days',strtotime(@$job_data->job_start_date)))}}</p>



                               </div>



                             </li>



                           </ul>



                        </div>



                        <p>{!! nl2br(@$job_data->description) !!}</p>



                        <div class="row">

                            <div class="col-md-12">

                                <div class="bodar4"> </div>

                            </div>

                        </div>



                        <div class="job-app">

                            <p>Total Job Applicants : <span>{{@$job_data->tot_bids}}</span> </p>

                            <div class="d-flex align-items-center">

                                <span>Share this Job:</span>
                                

                
                                
                
                                <div class="addthis_inline_share_toolbox_plbv"></div>
            
            
                                <!-- <ul>

                                    <li><a href="#" target="_blank"><img src="images/share1.png" alt=""></a></li>

                                    <li><a href="#" target="_blank"><img src="images/share2.png" alt=""></a></li>

                                    <li><a href="#" target="_blank"><img src="images/share3.png" alt=""></a></li>

                                    <li><a href="#" target="_blank"><img src="images/share4.png" alt=""></a></li>

                                    <li><a href="#" target="_blank"><img src="images/share5.png" alt=""></a></li>

                                </ul> -->

                            </div>

                        </div>

                        

                    </div>



                    

                </div>
                 <div class="right-details make_me_sticky desk_none">

                    <div class="client-box">

                        <h3>Posted By</h3>

                        <div class="below-abouts">

                            <div class="post-info">

                                <em>
                                    @if(@$job_data->getUser->profile_pic != null)

                                            <img src="{{ url('storage/app/public/profile_picture/'.@$job_data->getUser->profile_pic)}}">

                                    @else
                                            <img src="{{ asset('public/frontend/images/postimg.png') }}">
                                    @endif

                                </em>

                                <div class="postd-na">

                                    <h3><a href="javascript:;">{{@$job_data->getUser->name}}</a> </h3>

                                    <p>{{@$job_data->getUser->address}}</p>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-12">

                                    <div class="bodar4"> </div>

                                </div>

                            </div>

                            <p>Total Job Posted: <b>{{@$total_jobs}}</b></p>

                        </div>

                    </div>

                    @if(@Auth::user()->user_type == 'S')
                       @if(@$quote && @$quote->status == 'Awarded' ) 
                            <!--@if(@$chat) -->
                            <!--<a href="javascript:;" class="rm_add_btnn chat_btn" data-userid="{{@$job_data->getUser->id}}" data-agentid="{{@$quote->user_id}}" data-name="{{@$quote->proDetails->name}}" data-username="{{@$job_data->getUser->name}}" data-usertype="P" data-msgtyp="Pr" data-propertyid="{{@$quote->user_id}}" data-bidid="{{@$quote->id}}" data-providerid="{{@$quote->user_id}}"><img src="{{ asset('public/frontend/images/messg2.png') }}" alt=""> Send a Message</a>-->
                            
                               
                            <!--@endif-->
                            
                             <a href="javascript:;" class="rm_add_btnn chat_btn" data-userid="{{@$job_data->getUser->id}}" data-agentid="{{@$job_data->proDetails->user_id}}" data-name="{{@$job_data->proDetails->name}}" data-username="{{@$job_data->getUser->name}}" data-usertype="P"  data-propertyid="{{@$job_data->proDetails->user_id}}" data-bidid=0 data-providerid="{{@$job_data->proDetails->user_id}}"><img src="{{ asset('public/frontend/images/messg2.png') }}" alt=""> Send a Message</a>
                        @endif
                    @endif
                    @if(@Auth::user()->user_type == 'S')
                    

                      @if(@$quote)
                        
                        @if(@$quote->status == 'Bid Placed')

                        <a href="javascript:;" data-toggle="modal" data-target="#myModal2" data-id="{{@$job_data->id}}"  class="rm_add_btnn2">Bid Placed</a>
                        @endif
                    @else

                       <a href="javascript:;" data-toggle="modal" data-target="#myModal1" data-id="{{@$job_data->id}}"  class="rm_add_btnn2">I am interested</a> 
                    @endif
                    
                    @endif

                    <!--<div class="get_quote">

                        <h3>Send a Quote</h3>

                        <form>

                            <div class="das_input">

                                <input type="text" placeholder="Cost this job" name="">

                            </div>



                            <div class="das_input">

                                <div class="dash-d">

                                    <input type="text" placeholder="Timeline" id="datepicker"> 

                                    <span class="over_llp1"><img src="images/cala.png" alt=""></span> </div>

                             </div>

                            <div class="das_input">

                             <textarea placeholder="Description "></textarea>

                            </div>

                            <div class="sub_quote">

                                <input type="submit" value="Submit" name="">

                            </div>



                        </form>

                    </div>-->
                </div>
                
                
                @if(count(@$similar_jobs) > 0)
                <div class="jobs-details-job">

                    <h3>Similar Jobs</h3>



                    <div class="owl-carousel similar-jobs">
                        @foreach(@$similar_jobs as $val)
                        <div class="item">

                            <div class="job-lists">

                                    <h2><a href="{{route('job.details',@$val->slug)}}">@if(strlen(@$val->job_name) > 50)

                                        {{substr(@$val->job_name,0,50)}}....
                                        @else
                                        {{@$val->job_name}}
                                        @endif
                                     </a></h2>

                                    <h5> <img src="{{ asset('public/frontend/images/lists.png') }}">{{@$val->categoryName->category_name}}</h5>
                                    <h5> <img src="{{ asset('public/frontend/images/lists.png') }}">{{@$val->subCategoryName->name}}</h5>
                                    <p>{{substr(@$val->description,0,50)}}....</p>

                                    <ul>

                                        <li><img src="{{ asset('public/frontend/images/maps.png') }}">{{@$val->jobState->name}},{{@$val->jobCity->name}}</li>

                                        <li><img src="{{ asset('public/frontend/images/drwaers.png') }}">
                                            @if(@$val->budget_range_from != null) 
                                            ₹{{@$val->budget_range_from}}
                                            @else
                                            --
                                            @endif
                                        </li>

                                        <li><img src="{{asset('public/frontend/images/cals.png') }}"> {{date('jS M,Y',strtotime(@$val->job_start_date))}}</li>

                                    </ul>



                                    <div class="posted">

                                        <p>Posted By <span>{{@$val->getUser->name}}</span> on {{date('jS M,Y',strtotime(@$val->created_at))}}</p>

                                        <a href="{{route('job.details',@$val->slug)}}">View More</a>

                                    </div>

                                </div>

                        </div>
                        @endforeach

                        <!-- <div class="item">

                            <div class="job-lists">

                                    <h2><a href="job-details.html"> Dummy title heresed do eiu mod tempor.. </a></h2>

                                    <h5> <img src="images/lists.png"> Electrical Engineer</h5>

                                    <p>Lorem Ipsum is simply dummy text of the printing and type setting the type book Lorem Ipsum has been the industry's standard dummy text..</p>

                                    <ul>

                                        <li><img src="images/maps.png"> Park Street , Kolkata</li>

                                        <li><img src="images/drwaers.png"> $80/hr.</li>

                                        <li><img src="images/cals.png"> 10.10.2021</li>

                                    </ul>



                                    <div class="posted">

                                        <p>Posted By <span> Micky Holley </span> on 04.10.2021</p>

                                        <a href="job-details.html">View More</a>

                                    </div>

                                </div>

                        </div>

                        <div class="item">

                            <div class="job-lists">

                                    <h2><a href="job-details.html"> Dummy title heresed do eiu mod tempor.. </a></h2>

                                    <h5> <img src="images/lists.png"> Electrical Engineer</h5>

                                    <p>Lorem Ipsum is simply dummy text of the printing and type setting the type book Lorem Ipsum has been the industry's standard dummy text..</p>

                                    <ul>

                                        <li><img src="images/maps.png"> Park Street , Kolkata</li>

                                        <li><img src="images/drwaers.png"> $80/hr.</li>

                                        <li><img src="images/cals.png"> 10.10.2021</li>

                                    </ul>



                                    <div class="posted">

                                        <p>Posted By <span> Micky Holley </span> on 04.10.2021</p>

                                        <a href="job-details.html">View More</a>

                                    </div>

                                </div>

                        </div>

                        <div class="item">

                            <div class="job-lists">

                                    <h2><a href="job-details.html"> Dummy title heresed do eiu mod tempor.. </a></h2>

                                    <h5> <img src="images/lists.png"> Electrical Engineer</h5>

                                    <p>Lorem Ipsum is simply dummy text of the printing and type setting the type book Lorem Ipsum has been the industry's standard dummy text..</p>

                                    <ul>

                                        <li><img src="images/maps.png"> Park Street , Kolkata</li>

                                        <li><img src="images/drwaers.png"> $80/hr.</li>

                                        <li><img src="images/cals.png"> 10.10.2021</li>

                                    </ul>



                                    <div class="posted">

                                        <p>Posted By <span> Micky Holley </span> on 04.10.2021</p>

                                        <a href="job-details.html">View More</a>

                                    </div>

                                </div>

                        </div>

                        <div class="item">

                            <div class="job-lists">

                                    <h2><a href="job-details.html"> Dummy title heresed do eiu mod tempor.. </a></h2>

                                    <h5> <img src="images/lists.png"> Electrical Engineer</h5>

                                    <p>Lorem Ipsum is simply dummy text of the printing and type setting the type book Lorem Ipsum has been the industry's standard dummy text..</p>

                                    <ul>

                                        <li><img src="images/maps.png"> Park Street , Kolkata</li>

                                        <li><img src="images/drwaers.png"> $80/hr.</li>

                                        <li><img src="images/cals.png"> 10.10.2021</li>

                                    </ul>



                                    <div class="posted">

                                        <p>Posted By <span> Micky Holley </span> on 04.10.2021</p>

                                        <a href="job-details.html">View More</a>

                                    </div>

                                </div>

                        </div> -->

                    </div>

                </div>
                @endif



            </div>



            <div class="col-lg-3 col-md-12 col-sm-12">

                <div class="right-details make_me_sticky mob_none">

                    <div class="client-box">

                        <h3>Posted By</h3>

                        <div class="below-abouts">

                            <div class="post-info">

                                <em>
                                    @if(@$job_data->getUser->profile_pic != null)

                                            <img src="{{ url('storage/app/public/profile_picture/'.@$job_data->getUser->profile_pic)}}">

                                    @else
                                            <img src="{{ asset('public/frontend/images/postimg.png') }}">
                                    @endif

                                </em>

                                <div class="postd-na">

                                    <h3><a href="javascript:;">{{@$job_data->getUser->name}}</a> </h3>

                                    <p>{{@$job_data->getUser->address}}</p>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-12">

                                    <div class="bodar4"> </div>

                                </div>

                            </div>

                            <p>Total Job Posted: <b>{{@$total_jobs}}</b></p>

                        </div>

                    </div>

                    @if(@Auth::user()->user_type == 'S')
                       @if(@$quote && @$quote->status == 'Awarded' ) 
                            <!--@if(@$chat) -->
                            <!--<a href="javascript:;" class="rm_add_btnn chat_btn" data-userid="{{@$job_data->getUser->id}}" data-agentid="{{@$quote->user_id}}" data-name="{{@$quote->proDetails->name}}" data-username="{{@$job_data->getUser->name}}" data-usertype="P" data-msgtyp="Pr" data-propertyid="{{@$quote->user_id}}" data-bidid="{{@$quote->id}}" data-providerid="{{@$quote->user_id}}"><img src="{{ asset('public/frontend/images/messg2.png') }}" alt=""> Send a Message</a>-->
                            
                               
                            <!--@endif-->
                            
                             <a href="javascript:;" class="rm_add_btnn chat_btn" data-userid="{{@$job_data->getUser->id}}" data-agentid="{{@$job_data->proDetails->user_id}}" data-name="{{@$job_data->proDetails->name}}" data-username="{{@$job_data->getUser->name}}" data-usertype="P"  data-propertyid="{{@$job_data->proDetails->user_id}}" data-bidid=0 data-providerid="{{@$job_data->proDetails->user_id}}"><img src="{{ asset('public/frontend/images/messg2.png') }}" alt=""> Send a Message</a>
                        @endif
                    @endif
                    @if(@Auth::user()->user_type == 'S')
                    

                      @if(@$quote)
                        
                        @if(@$quote->status == 'Bid Placed')

                        <a href="javascript:;" data-toggle="modal" data-target="#myModal2" data-id="{{@$job_data->id}}"  class="rm_add_btnn2">Bid Placed</a>
                        @endif
                    @else

                       <a href="javascript:;" data-toggle="modal" data-target="#myModal1" data-id="{{@$job_data->id}}"  class="rm_add_btnn2">I am interested</a> 
                    @endif
                    
                    @endif

                    <!--<div class="get_quote">

                        <h3>Send a Quote</h3>

                        <form>

                            <div class="das_input">

                                <input type="text" placeholder="Cost this job" name="">

                            </div>



                            <div class="das_input">

                                <div class="dash-d">

                                    <input type="text" placeholder="Timeline" id="datepicker"> 

                                    <span class="over_llp1"><img src="images/cala.png" alt=""></span> </div>

                             </div>

                            <div class="das_input">

                             <textarea placeholder="Description "></textarea>

                            </div>

                            <div class="sub_quote">

                                <input type="submit" value="Submit" name="">

                            </div>



                        </form>

                    </div>-->
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal res-modal" id="myModal1">
        <div class="modal-dialog">
          <div class="modal-content">
          
            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title">Quotes</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            
            <!-- Modal body -->
            <div class="modal-body">
                <form action="{{route('service.provider.save.quotes')}}" method="POST" id="myForm">
                    @csrf
                    <input type="hidden" name="id" value="{{@$job_data->id}}">
                    <input type="hidden" name="user_id" value="{{@Auth::user()->id}}">
                    <input type="hidden" name="" id="budget_range_from" value="0">
                    <div class="appome_input das_input">
                        <label>Quote:</label>
                            <div class="dash-d">
                                <input type="text" placeholder="Your Estimate"  name="cost" maxlength="10">
                            </div>
                    </div>
                    <div class="appome_input das_input">
                        <label>Duration/(Days):</label>
                            <div class="dash-d">
                                <input type="text" placeholder="Days"  name="timeline" maxlength="4">
                                
                            </div>
                    </div>

                    <div class="appome_input das_input qute_wid des_wid">
                        <label>Bid Description:</label>
                            <div class="dash-d">
                                <textarea name="description" id="description"></textarea>
                                
                            </div>
                    </div>
                                            
                
            </div>
            
            <!-- Modal footer -->
            <div class="modal-footer">
                <input type="submit" name="" value="Save" class="btn btn-danger">
              <!-- <button type="submit" class="btn btn-danger" data-dismiss="modal">Save</button> -->
            </div>
            </form>
          </div>
        </div>
</div>

<div class="modal res-modal" id="myModal2">
        <div class="modal-dialog">
          <div class="modal-content">
          
            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title">Quotes</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            
            <!-- Modal body -->
            <div class="modal-body">
                <form action="{{route('service.provider.save.quotes')}}" method="POST" id="myForm1">
                    @csrf
                    <input type="hidden" name="id" value="{{@$job_data->id}}">
                    <input type="hidden" name="user_id" value="{{@Auth::user()->id}}">
                    <input type="hidden" name="" id="budget_range_from" value="0">
                    <input type="hidden" name="quote_id" id="quote_id" value="{{@$quote->id}}">
                    <div class="appome_input das_input">
                        <label>Quote:</label>
                            <div class="dash-d">
                                <input type="text" placeholder="Your Estimate"  name="cost" value="{{@$quote->cost}}" maxlength="10">
                            </div>
                    </div>
                    <div class="appome_input das_input">
                        <label>Duration/(Days):</label>
                            <div class="dash-d">
                                <input type="text" placeholder="Days"  name="timeline" value="{{@$quote->timeline}}" maxlength="4">
                                
                            </div>
                    </div>

                    <div class="appome_input das_input qute_wid des_wid">
                        <label>Bid Description:</label>
                            <div class="dash-d">
                                <textarea name="description" id="description">{{@$quote->quote_description}}</textarea>
                                
                            </div>
                    </div>
                                            
                
            </div>
            
            <!-- Modal footer -->
            <div class="modal-footer">
                <input type="submit" name="" value="Save" class="btn btn-danger">
              <!-- <button type="submit" class="btn btn-danger" data-dismiss="modal">Save</button> -->
            </div>
            </form>
          </div>
        </div>
</div>



@endsection


@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection



@section('script')
@include('includes.script')
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-613782201f0abe3d"></script>

<script>
    $(function() {
            $("#datepicker3").datepicker();
        });
</script>
<script>
    
</script>
<script>
    $(document).ready(function(){
        $.validator.addMethod('greaterThan', function (value, element, param) {
            return this.optional(element) || parseInt(value) > parseInt($(param).val());
        }, 'Please put a valid amount');
        $('#myForm').validate({
            rules: {
                    timeline:{
                        digits:true,
                        required:true,
                    },
                    cost:{
                        required:true,
                        digits: true ,
                        greaterThan: '#budget_range_from'
                    }
                    
                },
        })


    });
</script>

<script>
    $(document).ready(function(){
        $.validator.addMethod('greaterThan', function (value, element, param) {
            return this.optional(element) || parseInt(value) > parseInt($(param).val());
        }, 'Please put a valid amount');
        $('#myForm1').validate({
            rules: {
                    timeline:{
                        digits:true,
                        required:true,
                    },
                    cost:{
                        required:true,
                        digits: true ,
                        greaterThan: '#budget_range_from'
                    }
                    
                },
        })


    });
</script>
@endsection

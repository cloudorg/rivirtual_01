@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
<link href="{{ URL::asset('public/frontend/croppie/croppie.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('public/frontend/croppie/croppie.min.css') }}" rel="stylesheet" />
@endsection

@section('title')
<title> RiVirtual | Review and Rating </title>
@endsection




@section('header')
@include('includes.header')
@endsection

@section('content')

<div class="haeder-padding"></div>

<div class="dashboard-provider">
	<div class="container">
		<div class="row">
          @include('includes.sidebar')
			<div class="col-lg-9 col-md-12 col-sm-12">
				<div class="cus-dashboard-right">
				   <h2 class="pro-heading-das">Reviews & Rating</h2> 
				</div>
                <div class="average_ratingg">
                    	<span>Collective average rating </span>
                        @if(@$proDetails->avg_review>=0.75 && @$proDetails->avg_review<=1.24)
                        @php
						 $ii = 1;
						 @endphp
                         @for($i=1; $i<=$ii; $i++)
                        <i class="fa fa-star"></i>
                        @endfor
                        <i class="fa fa-star grey-star"></i>
                        <i class="fa fa-star grey-star"></i>
                        <i class="fa fa-star grey-star"></i>
                        <i class="fa fa-star grey-star"></i>
                        <strong>{{ $ii }}</strong>
                        @elseif(@$proDetails->avg_review>=1.25 && @$proDetails->avg_review<=1.74)
                        @php
                        $ii = 1.5;
                        @endphp
                        @for($i=1; $i<=1; $i++)
                        <i class="fa fa-star"></i>
                        @endfor
                        <i class="fa fa-star-half-o"></i>
                        <i class="fa fa-star grey-star"></i>
                        <i class="fa fa-star grey-star"></i>
                        <i class="fa fa-star grey-star"></i>
                        <strong>{{ $ii }}</strong>
                        @elseif(@$proDetails->avg_review>=1.75 && @$proDetails->avg_review<=2.24)
                        @php
                        $ii = 2;
                        @endphp
                        @for($i=1; $i<=$ii; $i++)
                        <i class="fa fa-star"></i>
                        @endfor
            
                        <i class="fa fa-star grey-star"></i>
                        <i class="fa fa-star grey-star"></i>
                        <i class="fa fa-star grey-star"></i>
                        <strong>{{ $ii }}</strong>
                        @elseif(@$proDetails->avg_review>=2.25 && @$proDetails->avg_review<=2.74)
                        @php
                        $ii = 2.5;
                        @endphp
                        @for($i=1; $i<=2; $i++)
                        <i class="fa fa-star"></i>
                        @endfor
                        <i class="fa fa-star-half-o"></i>
                        <i class="fa fa-star grey-star"></i>
                        <i class="fa fa-star grey-star"></i>
                        <strong>{{ $ii }}</strong>
                        @elseif(@$proDetails->avg_review>=2.75 && @$proDetails->avg_review<=3.24)
                        @php
                        $ii = 3;
                        @endphp
                        @for($i=1; $i<=$ii; $i++)
                        <i class="fa fa-star"></i>
                        @endfor
                       
                        <i class="fa fa-star grey-star"></i>
                        <i class="fa fa-star grey-star"></i>
                        <strong>{{ $ii }}</strong>
                        @elseif(@$proDetails->avg_review>=3.25 && @$proDetails->avg_review<=3.74)
                        @php
                        $ii = 3.5;
                        @endphp
                        @for($i=1; $i<=3; $i++)
                        <i class="fa fa-star"></i>
                        @endfor
                        <i class="fa fa-star-half-o"></i>
                        <i class="fa fa-star grey-star"></i>
                        
                        <strong>{{ $ii }}</strong>
                        @elseif(@$proDetails->avg_review>=3.75 && @$proDetails->avg_review<=4.24)
                        @php
                        $ii = 4;
                        @endphp
                        @for($i=1; $i<=$ii; $i++)
                        <i class="fa fa-star"></i>
                        @endfor
                        <i class="fa fa-star grey-star"></i>
                        <strong>{{ $ii }}</strong>
                        @elseif(@$proDetails->avg_review>=4.25 && @$proDetails->avg_review<=4.74)
                        @php
                        $ii = 4.5;
                        @endphp
                        @for($i=1; $i<=4; $i++)
                        <i class="fa fa-star"></i>
                        @endfor
                        <i class="fa fa-star-half-o"></i>
                        <strong>{{ $ii }}</strong>
                        @elseif(@$proDetails->avg_review>=4.75 && @$proDetails->avg_review<=5.24)
                        @php
                        $ii = 5;
                        @endphp
                        @for($i=1; $i<=$ii; $i++)
                        <i class="fa fa-star"></i>
                        @endfor

                        <strong>{{ $ii }}</strong>
                        @endif
                        <p>({{ @$proDetails->tot_review }} reviews)</p>
                    </div>
				   <div class="prod-dash-right">
					
					<div class="agent-right-body rm_xmtop">
                        <div class="revew_panel">
                            @if(count(@$allReviews)>0)
                             @foreach(@$allReviews as $review)
							<div class="revew_item">
										<p><b><a href="{{route('job.details',['slug'=>@$review->jobDetails->slug])}}" target="_blank">{{@$review->jobDetails->job_name}}</a></b></p>
										<div class="revew_top">

											<div class="star_bx">
												<ul>
	                                                @for($i=1; $i<=$review->review_point; $i++ )
													<li><i class="fa fa-star"></i></li>
	                                                @endfor
													@for($j=$review->review_point+1; $j<=5; $j++)
												<li><i class="fa fa-star gray"></i></li>
												@endfor
												</ul>
												<strong><img src="{{ url('public/frontend/images/calendar2.png') }}" alt="">{{date('jS M,Y',strtotime(@$review->created_at))}}</strong>
											</div>
										</div>

								<p>
                                  {{ @$review->review_text }}  
                                {{--<span class="moretextew" style="display: none;">Lorem ipsum dolor sit amet consectetur the adipiscing elit iaculis magna dignissim facilisis felis nisl pellentesque libero, imperdiet neque ante ut nisi sapien neque sodales lacus auctor the pretium nibhfeugiat dui. Lorem ipsum dolor sit ametconsectetur adipiscing elit  iaculis, </span> <a href="#url" class="moreless-button10 more_cc">Read More +</a>--}} 
                               </p>
							</div>
                            @endforeach
                            @else
                            @if(@$allReviews->isEmpty())
                                <div class="col-sm-12 col-md-12">
                                    <center style="padding: 10px"><p>No Data Found</p></center>
                                </div>
                                @endif
                             @endif
							{{--<div class="revew_item">
								<div class="revew_top">
									
										
											<div class="star_bx">
											<ul>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star-half-o"></i></li>
											</ul>
											<strong><img src="images/calendar2.png" alt=""> 20th Sept, 2021</strong>
										
									</div>
								</div>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the of Letraset sheets containing  It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, Lorem Ipsum has been the of Letraset sheets containing  Lorem Ipsum passages..<span class="moretext21" style="display: none;">Lorem ipsum dolor sit amet consectetur the adipiscing elit iaculis magna dignissim facilisis felis nisl pellentesque libero, imperdiet neque ante ut nisi sapien neque sodales lacus auctor the pretium nibhfeugiat dui. Lorem ipsum dolor sit ametconsectetur adipiscing elit  iaculis, </span> <a href="#url" class="moreless-button21 more_cc">Read More +</a> </p>
							</div>

							<div class="revew_item">
								<div class="revew_top">
									
										
											<div class="star_bx">
											<ul>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star-half-o"></i></li>
											</ul>
											<strong><img src="images/calendar2.png" alt=""> 20th Sept, 2021</strong>
										
									</div>
								</div>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the of Letraset sheets containing  It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, Lorem Ipsum has been the of Letraset sheets containing  Lorem Ipsum passages..<span class="moretext30" style="display: none;">Lorem ipsum dolor sit amet consectetur the adipiscing elit iaculis magna dignissim facilisis felis nisl pellentesque libero, imperdiet neque ante ut nisi sapien neque sodales lacus auctor the pretium nibhfeugiat dui. Lorem ipsum dolor sit ametconsectetur adipiscing elit  iaculis, </span> <a href="#url" class="moreless-button30 more_cc">Read More +</a> </p>
							</div>

							<div class="revew_item">
								<div class="revew_top">
									
										
											<div class="star_bx">
											<ul>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star-half-o"></i></li>
											</ul>
											<strong><img src="images/calendar2.png" alt=""> 20th Sept, 2021</strong>
										
									</div>
								</div>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the of Letraset sheets containing  It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, Lorem Ipsum has been the of Letraset sheets containing  Lorem Ipsum passages..<span class="moretext40" style="display: none;">Lorem ipsum dolor sit amet consectetur the adipiscing elit iaculis magna dignissim facilisis felis nisl pellentesque libero, imperdiet neque ante ut nisi sapien neque sodales lacus auctor the pretium nibhfeugiat dui. Lorem ipsum dolor sit ametconsectetur adipiscing elit  iaculis, </span> <a href="#url" class="moreless-button40 more_cc">Read More +</a> </p>
							</div>

							<div class="revew_item">
								<div class="revew_top">
									
										
											<div class="star_bx">
											<ul>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star-half-o"></i></li>
											</ul>
											<strong><img src="images/calendar2.png" alt=""> 20th Sept, 2021</strong>
										
									</div>
								</div>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the of Letraset sheets containing  It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, Lorem Ipsum has been the of Letraset sheets containing  Lorem Ipsum passages..<span class="moretex50" style="display: none;">Lorem ipsum dolor sit amet consectetur the adipiscing elit iaculis magna dignissim facilisis felis nisl pellentesque libero, imperdiet neque ante ut nisi sapien neque sodales lacus auctor the pretium nibhfeugiat dui. Lorem ipsum dolor sit ametconsectetur adipiscing elit  iaculis, </span> <a href="#url" class="moreless-button50 more_cc">Read More +</a> </p>
							</div>--}}
							
							
						</div>

						<nav aria-label="...">
                                            <ul class="pagination new-pagination">
                                            {{@$allReviews->appends(request()->except(['page', '_token']))->links()}}
                                                {{--<li class="page-item"> <span class="page-link">Prev </span> </li>
                                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                                <li class="page-item" aria-current="page"> <span class="page-link">
                                                2
                                                <span class="sr-only">(current)</span> </span>
                                                </li>
                                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                <li class="page-item"><a class="page-link" href="#">4</a></li>
                                                <li class="page-item"> <a class="page-link act" href="#">Next </a> </li>--}}
                                            </ul>
                                        </nav>  
                           
                    </div>
						
					</div>
				</div>
				
			</div>
		</div>
		
	</div>
@endsection
        
@section('footer')
@include('includes.footer')
@endsection


@section('script')
@include('includes.script')

@endsection
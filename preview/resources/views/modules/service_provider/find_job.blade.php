@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
<link href="{{ URL::asset('public/frontend/croppie/croppie.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('public/frontend/croppie/croppie.min.css') }}" rel="stylesheet" />
@endsection

@section('title')
<title> RiVirtual | Find Job</title>
@endsection




@section('header')
@include('includes.header')
@endsection

@section('content')

<div class="haeder-padding"></div>


<div class="jobs-finder">
	<div class="container">
		<div class="row no-gutters rows-bor">
			<div class="col-lg-3 col-md-12 col-sm-12 find-jobs">
				<div class="mobile-list">
						<p><i class="fa fa-filter"></i> Show Filter</p>
					</div>
                    <form action="{{ route('search.job') }}" method="post" id="searchFilter">
                        @csrf

				<div class="search-filter find-job-se">
					<h3 class="filters"> Filters <img src="{{ url('public/frontend/images/filter-rights.png') }}" alt=""></h3>
						<div class="search-section2">
							<div class="list-category-sec">
		                        <h3>Category</h3>
		                        <div class="diiferent-sec">
		                           <select class="dropdown-list" id="category" name="category">
		                              <option value="">Select Category</option>
		                              @if(@$category)
									    @foreach($category as $cat)
		                              <option value="{{ $cat->id }}" @if(@$request['category'] == $cat->id) selected @elseif(@$catId->category_id == $cat->id) selected  @endif>{{ $cat->category_name }}</option>
									  @endforeach
									  @endif
		                           </select>
		                        </div>
		                     </div>

		                     <div class="list-category-sec">
		                        <h3>Sub-category</h3>
		                        <div class="diiferent-sec">
		                           <select class="dropdown-list" id="sub_category" name="sub_category"> 
		                              <option value="">Select Sub Category</option>
		                              @if(@$subcategory)
									    @foreach($subcategory as $cat)
		                              <option value="{{ $cat->id }}" @if(@$request['sub_category'] == $cat->id) selected  selected @endif>{{ $cat->name }}</option>
									  @endforeach
									  @endif
		                           </select>
		                        </div>
		                     </div>

		                     {{--<div class="list-category-sec">
		                        <h3>Job Type</h3>
		                        <div class="diiferent-sec">
		                           <select class="dropdown-list" name="job_type">
                                       <option value="">Select Job Type</option>
		                              <option value="FT" @if(@$request['job_type'] == 'FT') selected @endif>Full Time </option>
		                              <option value="PT" @if(@$request['job_type'] == 'PT') selected @endif>Part Time</option>
		                              <option value="F" @if(@$request['job_type'] == 'F') selected @endif>Fixed</option>
		                           </select>
		                        </div>
		                     </div>--}}

		                     {{--<div class="list-category-sec">
		                        <h3>Rate</h3>
		                        <div class="diiferent-sec">
		                           <select class="dropdown-list" name="rate_type">
                                       <option value="">Select Rate</option>
		                              <option value="F" @if(@$request['rate_type'] == 'F') selected @endif>Fixed Cost</option>
		                              <option value="H" @if(@$request['rate_type'] == 'H') selected @endif>Hourly</option>
		                           </select>
		                        </div>
		                     </div>--}}

		                     <div class="list-category-sec">
		                        <h3>Budget</h3>
		                        <div class="diiferent-sec">
		                          <div class="slider_rnge">
									 <div id="slider-range" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
									  <div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 0%; width: 100%;"></div> <span tabindex="0" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 3.32667%;"></span> <span tabindex="0" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 66.6667%;"></span> <div class="ui-slider-range ui-corner-all ui-widget-header" style="left: 3.32667%; width: 63.34%;"></div></div> <span class="range-text">
									      <input type="text" class="price_numb" readonly="" id="amount">
                                          <input type="hidden" class="price_numb" id="amount1" name="amount1" value="{{@$request['amount1']?@$request['amount1']:0}}">
                                          <input type="hidden" class="price_numb" id="amount2" name="amount2" value="{{@request['amount2']?@$request['amount2']:(int)@$maxPrice->budget_range_from}}">
									         </span> 
									 </div>
		                        </div>
		                     </div>

		                     <div class="list-category-sec">
		                        <h3>State</h3>
		                        <div class="diiferent-sec">
								<select class="dropdown-list" name="state" id="states">
		                              <option value="">Select State</option>
		                               @if(@$states)
									    @foreach(@$states as $state)
		                              <option value="{{ $state->id }}" @if(@$request['state'] == $state->id) selected @endif>{{ @$state->name }}</option>
									  @endforeach
									  @endif
		                           </select>
		                        </div>
		                     </div>
							 <div class="list-category-sec">
		                        <h3>City</h3>
		                        <div class="diiferent-sec">
								<select class="dropdown-list" name="city" id="city">
		                              <option value="">Select City</option>
		                               @if(@$cites)
									    @foreach(@$cites as $city)
		                              <option value="{{ $city->id }}" @if(@$request['city'] == $city->id) selected @endif>{{ @$city->name }}</option>
									  @endforeach
									  @endif
		                           </select>
		                        </div>
		                     </div>

                             <input type="hidden" class="price_numb" id="sort_by_value" name="sort_by_value" value="{{@$request['sort_by_value']?@$request['sort_by_value']:0}}">
		                     <div class="fil-can">
		                     	<button type="submit" class="findfil-btn">Filter</button>
		                     	<button type="button"  class="findfil-btn clearex" onclick="window.location.href='{{ route('search.job') }}'">Clear</button>
		                     </div>

							
							


							
						</div>
							<div class="clearfix"></div>
					</div>
						<div class="clearfix"></div>

             </form>        
			</div>

			<div class="col-lg-9 col-md-12 col-sm-12">
				<div class="job-list-view">
					<div class="jobs-total">
							<div class="page-jobs">
	                        <h5>Showing {{ @$allJobs->count() }} of {{ @$totaljobs }} results for Jobs</h5>
	                        </div>
	                        <div class="sort-filter">
	                           <p>Sort by : </p>
	                           <select class="sort-select" id="short_by" name="short_by">
	                              <option value=""> New Listings</option>
	                              {{--<option value="RH" 
                                        @if(!empty(@$request['sort_by_value'])) 
                                        @if(@$request['sort_by_value'] == 'RH') 
                                        selected="" 
                                        @endif 
                                        @elseif(!empty(@request()->sort_by_value))
                                        @if(@request()->sort_by_value == 'RH') 
                                        selected="" 
                                        @endif 
                                        @endif>Review (High to Low) </option>
	                              <option value="RL"
								       @if(!empty(@$request['sort_by_value'])) 
                                        @if(@$request['sort_by_value'] == 'RL') 
                                        selected="" 
                                        @endif 
                                        @elseif(!empty(@request()->sort_by_value))
                                        @if(@request()->sort_by_value == 'RL') 
                                        selected="" 
                                        @endif 
                                        @endif >Review (Low to High)</option>--}}
	                              <option value="H"
								      @if(!empty(@$request['sort_by_value'])) 
                                        @if(@$request['sort_by_value'] == 'H') 
                                        selected="" 
                                        @endif 
                                        @elseif(!empty(@request()->sort_by_value))
                                        @if(@request()->sort_by_value == 'H') 
                                        selected="" 
                                        @endif 
                                        @endif>Price (High to Low)</option>
	                              <option value="L"
								       @if(!empty(@$request['sort_by_value'])) 
                                        @if(@$request['sort_by_value'] == 'L') 
                                        selected="" 
                                        @endif 
                                        @elseif(!empty(@request()->sort_by_value))
                                        @if(@request()->sort_by_value == 'L') 
                                        selected="" 
                                        @endif 
                                        @endif>Price (Low to High) </option>
	                           </select>
	                        </div>
	                     </div>

	                <div class="all-jobs">
	                	<div class="row">
                            @if(count(@$allJobs)>0)
                             @foreach(@$allJobs as $job)
	                		<div class="col-md-6 col-sm-6 col-12">
	                			<div class="job-lists">
	                				<h2><a href="{{route('job.details',['slug' =>  @$job->slug])}}"> 
                                       @if(strlen(@$job->job_name)>47) 
                                        {!! substr(@$job->job_name,0,47) . '...'  !!}
                                        @else
                                        {!! @$job->job_name !!}
                                        @endif
                                   </a></h2>
	                				<h5> <img src="{{ url('public/frontend/images/lists.png') }}">{{ @$job->categoryName?@$job->categoryName->category_name: '' }}</h5>
									<h5> <img src="{{ url('public/frontend/images/lists.png') }}">{{ @$job->subCategoryName?@$job->subCategoryName->name: '' }}</h5>
	                				<p>
                                      @if(strlen(@$job->description)>50)
                                      {!! substr(@$job->description,0,50) . '...' !!}
                                      @else
                                      {!! nl2br(@$job->description) !!}
                                      @endif
                                    </p>
	                				<ul>
	                					<li><img src="{{ url('public/frontend/images/maps.png') }}">
                                        <span>{{ @$job->jobState->name }}</span>,<span>{{ @$job->jobCity->name }}</span>
										
									   </li>
	                					<li><img src="{{ url('public/frontend/images/drwaers.png') }}"> 
                                        @if(@$job->budget_range_from != null)
										₹{{ @$job->budget_range_from }}
                                         @else
										 --
                                        @endif
                                      </li>
	                					<li><img src="{{ url('public/frontend/images/cals.png') }}">{{ date('d.m.Y',strtotime(@$job->job_start_date)) }}</li>
	                				</ul>

	                				<div class="posted">
	                					<p>Posted By <span> {{ @$job->userDetails->name }} </span> on {{ date('d.m.Y',strtotime(@$job->created_at)) }}</p>
	                					<a href="{{ route('job.details',['slug'=>@$job->slug]) }}">View More</a>
	                				</div>
	                			</div>
	                		</div>
                             @endforeach
                             @else
                             <div class="col-md-12">
                                    <div class="product-item search_product_item" style="text-align: center">
                                        <img src="{{url('public/frontend/images/error-no-search-results.png')}}" style="padding-top: 60px; width: 300px;">
                                    </div>
                                    <h4 style="color: #ef1600; text-align: center; font-size: 20px;">No Jobs available.</h4>
                                    <h6 style=" text-align: center;">Please check the spelling or try searching for something else</h6>
                                </div>
                                @endif
                              
	                		{{--<div class="col-md-6 col-sm-6 col-12">
	                			<div class="job-lists">
	                				<h2><a href="job-details.html"> Dummy title heresed do eiu mod tempor.. </a></h2>
	                				<h5> <img src="images/lists.png"> Electrical Engineer</h5>
	                				<p>Lorem Ipsum is simply dummy text of the printing and type setting the type book Lorem Ipsum has been the industry's standard dummy text..</p>
	                				<ul>
	                					<li><img src="images/maps.png"> Park Street , Kolkata</li>
	                					<li><img src="images/drwaers.png"> $80/hr.</li>
	                					<li><img src="images/cals.png"> 10.10.2021</li>
	                				</ul>

	                				<div class="posted">
	                					<p>Posted By <span> Micky Holley </span> on 04.10.2021</p>
	                					<a href="job-details.html">View More</a>
	                				</div>
	                			</div>
	                		</div>

	                		<div class="col-md-6 col-sm-6 col-12">
	                			<div class="job-lists">
	                				<h2><a href="job-details.html"> Dummy title heresed do eiu mod tempor.. </a></h2>
	                				<h5> <img src="images/lists.png"> Electrical Engineer</h5>
	                				<p>Lorem Ipsum is simply dummy text of the printing and type setting the type book Lorem Ipsum has been the industry's standard dummy text..</p>
	                				<ul>
	                					<li><img src="images/maps.png"> Park Street , Kolkata</li>
	                					<li><img src="images/drwaers.png"> $80/hr.</li>
	                					<li><img src="images/cals.png"> 10.10.2021</li>
	                				</ul>

	                				<div class="posted">
	                					<p>Posted By <span> Micky Holley </span> on 04.10.2021</p>
	                					<a href="job-details.html">View More</a>
	                				</div>
	                			</div>
	                		</div>

	                		<div class="col-md-6 col-sm-6 col-12">
	                			<div class="job-lists">
	                				<h2><a href="job-details.html"> Dummy title heresed do eiu mod tempor.. </a></h2>
	                				<h5> <img src="images/lists.png"> Electrical Engineer</h5>
	                				<p>Lorem Ipsum is simply dummy text of the printing and type setting the type book Lorem Ipsum has been the industry's standard dummy text..</p>
	                				<ul>
	                					<li><img src="images/maps.png"> Park Street , Kolkata</li>
	                					<li><img src="images/drwaers.png"> $80/hr.</li>
	                					<li><img src="images/cals.png"> 10.10.2021</li>
	                				</ul>

	                				<div class="posted">
	                					<p>Posted By <span> Micky Holley </span> on 04.10.2021</p>
	                					<a href="job-details.html">View More</a>
	                				</div>
	                			</div>
	                		</div>

	                		<div class="col-md-6 col-sm-6 col-12">
	                			<div class="job-lists">
	                				<h2><a href="job-details.html"> Dummy title heresed do eiu mod tempor.. </a></h2>
	                				<h5> <img src="images/lists.png"> Electrical Engineer</h5>
	                				<p>Lorem Ipsum is simply dummy text of the printing and type setting the type book Lorem Ipsum has been the industry's standard dummy text..</p>
	                				<ul>
	                					<li><img src="images/maps.png"> Park Street , Kolkata</li>
	                					<li><img src="images/drwaers.png"> $80/hr.</li>
	                					<li><img src="images/cals.png"> 10.10.2021</li>
	                				</ul>

	                				<div class="posted">
	                					<p>Posted By <span> Micky Holley </span> on 04.10.2021</p>
	                					<a href="job-details.html">View More</a>
	                				</div>
	                			</div>
	                		</div>

	                		<div class="col-md-6 col-sm-6 col-12">
	                			<div class="job-lists">
	                				<h2><a href="job-details.html"> Dummy title heresed do eiu mod tempor.. </a></h2>
	                				<h5> <img src="images/lists.png"> Electrical Engineer</h5>
	                				<p>Lorem Ipsum is simply dummy text of the printing and type setting the type book Lorem Ipsum has been the industry's standard dummy text..</p>
	                				<ul>
	                					<li><img src="images/maps.png"> Park Street , Kolkata</li>
	                					<li><img src="images/drwaers.png"> $80/hr.</li>
	                					<li><img src="images/cals.png"> 10.10.2021</li>
	                				</ul>

	                				<div class="posted">
	                					<p>Posted By <span> Micky Holley </span> on 04.10.2021</p>
	                					<a href="job-details.html">View More</a>
	                				</div>
	                			</div>
	                		</div>

	                		<div class="col-md-6 col-sm-6 col-12">
	                			<div class="job-lists">
	                				<h2><a href="job-details.html"> Dummy title heresed do eiu mod tempor.. </a></h2>
	                				<h5> <img src="images/lists.png"> Electrical Engineer</h5>
	                				<p>Lorem Ipsum is simply dummy text of the printing and type setting the type book Lorem Ipsum has been the industry's standard dummy text..</p>
	                				<ul>
	                					<li><img src="images/maps.png"> Park Street , Kolkata</li>
	                					<li><img src="images/drwaers.png"> $80/hr.</li>
	                					<li><img src="images/cals.png"> 10.10.2021</li>
	                				</ul>

	                				<div class="posted">
	                					<p>Posted By <span> Micky Holley </span> on 04.10.2021</p>
	                					<a href="job-details.html">View More</a>
	                				</div>
	                			</div>
	                		</div>

	                		<div class="col-md-6 col-sm-6 col-12">
	                			<div class="job-lists">
	                				<h2><a href="job-details.html"> Dummy title heresed do eiu mod tempor.. </a></h2>
	                				<h5> <img src="images/lists.png"> Electrical Engineer</h5>
	                				<p>Lorem Ipsum is simply dummy text of the printing and type setting the type book Lorem Ipsum has been the industry's standard dummy text..</p>
	                				<ul>
	                					<li><img src="images/maps.png"> Park Street , Kolkata</li>
	                					<li><img src="images/drwaers.png"> $80/hr.</li>
	                					<li><img src="images/cals.png"> 10.10.2021</li>
	                				</ul>

	                				<div class="posted">
	                					<p>Posted By <span> Micky Holley </span> on 04.10.2021</p>
	                					<a href="job-details.html">View More</a>
	                				</div>
	                			</div>
	                		</div>

	                		<div class="col-md-6 col-sm-6 col-12">
	                			<div class="job-lists">
	                				<h2><a href="job-details.html"> Dummy title heresed do eiu mod tempor.. </a></h2>
	                				<h5> <img src="images/lists.png"> Electrical Engineer</h5>
	                				<p>Lorem Ipsum is simply dummy text of the printing and type setting the type book Lorem Ipsum has been the industry's standard dummy text..</p>
	                				<ul>
	                					<li><img src="images/maps.png"> Park Street , Kolkata</li>
	                					<li><img src="images/drwaers.png"> $80/hr.</li>
	                					<li><img src="images/cals.png"> 10.10.2021</li>
	                				</ul>

	                				<div class="posted">
	                					<p>Posted By <span> Micky Holley </span> on 04.10.2021</p>
	                					<a href="job-details.html">View More</a>
	                				</div>
	                			</div>
	                		</div>

	                		<div class="col-md-6 col-sm-6 col-12">
	                			<div class="job-lists">
	                				<h2><a href="job-details.html"> Dummy title heresed do eiu mod tempor.. </a></h2>
	                				<h5> <img src="images/lists.png"> Electrical Engineer</h5>
	                				<p>Lorem Ipsum is simply dummy text of the printing and type setting the type book Lorem Ipsum has been the industry's standard dummy text..</p>
	                				<ul>
	                					<li><img src="images/maps.png"> Park Street , Kolkata</li>
	                					<li><img src="images/drwaers.png"> $80/hr.</li>
	                					<li><img src="images/cals.png"> 10.10.2021</li>
	                				</ul>

	                				<div class="posted">
	                					<p>Posted By <span> Micky Holley </span> on 04.10.2021</p>
	                					<a href="job-details.html">View More</a>
	                				</div>
	                			</div>
	                		</div>--}}



	                	</div>
	                </div>

				</div>
			</div>

			
				
		</div>

		<div class="row">
			<div class="col-lg-9 offset-lg-3">
				<nav aria-label="Page navigation example" class="list-pagination">
							<ul class="pagination justify-content-center">
                            {{$allJobs->appends(request()->except(['page', '_token']))->links()}}
								{{--<li class="page-item disabled">
									<a class="page-link page-link-prev" href="#" aria-label="Previous" tabindex="-1" aria-disabled="true"> Prev </a>
								</li>
								<li class="page-item active" aria-current="page"><a class="page-link" href="#">1</a> </li>
								<li class="page-item"><a class="page-link" href="#">2</a></li>
								<li class="page-item"><a class="page-link" href="#">3</a></li>
								<li class="page-item page-item-dots"><a class="page-link" href="#">6</a></li>
								<li class="page-item"> <a class="page-link page-link-next" href="#" aria-label="Next">
	                          Next
	                        </a> </li>--}}
							</ul>
						</nav>
			</div>
		</div>
	</div>
		
</div>
@endsection


@section('footer')
@include('includes.footer')
@endsection



@section('script')
@include('includes.script')


<script>
 
  $(document).ready(function(){

	var allSliderAmount;
        @if(@$request['amount1']!=null && @$request['amount2']!=null)
        allSliderAmount=[];
        allSliderAmount=['{{$request['amount1']}}','{{$request['amount2']}}'];
        @else
        allSliderAmount=[0,'{{(int)@$maxPrice->budget_range_from}}'];
        @endif
        console.log(allSliderAmount);
        $( "#slider-range" ).slider({
            range: true,
            min:0 ,
            max: '{{(int)@$maxPrice->budget_range_from}}',
            values: allSliderAmount,
            slide: function( event, ui ) {
                $( "#amount" ).val( "₹" + ui.values[ 0 ] + " - ₹" + ui.values[ 1 ] );
                $( "#amount1" ).val( ui.values[ 0 ]);
                $( "#amount2" ).val( ui.values[ 1 ] );
            }
        });

$( "#amount" ).val( "₹" + $( "#slider-range" ).slider( "values", 0 ) +

    " - ₹" + $( "#slider-range" ).slider( "values", 1 ) ); 

  });

  $(document).ready(function(){

  $('#short_by').change(function(){
            $('#sort_by_value').val($(this).val());
            $('#searchFilter').submit();
        });

  });

</script>
<script>
 $(document).ready(function(){

$('#category').on('change',function(e){
  e.preventDefault();
  var id = $(this).val();
  $.ajax({
	url:'{{route('get.subcategory')}}',
	type:'GET',
	data:{category:id},
	success:function(data){
	  console.log(data);
	  $('#sub_category').html(data.subcategory);
	}
  })
});

$('#states').on('change',function(e){
        e.preventDefault();
        var id = $(this).val();
        $.ajax({
          url:'{{route('get.city')}}',
          type:'GET',
          data:{state:id,id:'{{auth()->user()->state}}'},
          success:function(data){
            console.log(data);
            $('#city').html(data.city);
          }
        })
      });

});
  </script>
@endsection
@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> RiVirtual | Memberships </title>
@endsection




@section('header')
@include('includes.header')
@endsection


@section('content')
<!----header--->
<div class="haeder-padding"></div>



<div class="dashboard-provider">

    <div class="container">

        <div class="row">

            @include('includes.sidebar')



            <div class="col-lg-9 col-md-12 col-sm-12">

                <div class="cus-dashboard-right">

                   <h2 class="pro-heading-das">Memberships</h2> 

                </div>
                @include('includes.message')
                <div class="prod-dash-right">

                    

                    <div class="agent-right-body">

                            



                            <div class="row">

                            <div class="col-md-12">

                                <div class="meb_shptxt">

                                    <label><img src="{{ asset('public/frontend/images/member.png') }}">

                                        My Current Membership is : <span>{{@$package->getPackage->package_name}}</span></label>

                                </div>
                                 <div class="meb_shptxt">

                                    <label><img src="{{ asset('public/frontend/images/member.png') }}">

                                        My Current Membership expiry date is : <span>@if(@$user->package_expiry_date){{ date('jS M,Y',strtotime(@$user->package_expiry_date))}}@endif</span></label>

                                </div>

                            </div>
                            @if(count(@$pro_package) > 0)
                            @foreach(@$pro_package as $val)

                            <div class="col-sm-6">

                                <div class="member-box ">

                                    
                                    @if(@$val->package_name == 'PLATINUM')<div class="ribbon2 ribbon-top-left"><strong>Popular</strong></div>@endif

                                    <h2>@if(@$val->package_name == 'BRONZE')FREE @else <span @if(@$val->package_name == 'GOLD') class="gold_color" @elseif(@$val->package_name == 'PLATINUM') class="platinum_color" @endif>{{@$val->package_name}}</span> @endif</h2>
                                    @if(@$val->package_name == "BRONZE")
                                    <h3>Free for all </h3>
                                    @else
                                    @if(@$val->type == 'Y')
                                        <h3 class="extra-mem">INR {{@$val->yearly_price}} <span> /Yearly </span></h3>
                                        @else
                                        <h3 class="extra-mem">INR {{@$val->monthly_price}} <span> /Monthly </span></h3>
                                        @endif
                                    @endif

                                    <h5><img src="{{ asset('public/frontend/images/tick3.png') }}"> {{@$val->package_name}} </h5>

                                    <p>{{@$val->package_desc}}</p>
                                    
                                    @if(@$package->pro_package_id == @$val->id)
                                    <a href="javascript:;" class="member_btn mem_active">Active</a>
                                    @else

                                    @if(@$val->package_name == 'BRONZE')
                                        <a href="javascript:;" class="member_btn choose_plan" data-id="{{@$val->id}}">Choose plan</a>
                                    @else
                                        <form  method="POST" action="{{route('razorpay.index')}}">
                                        @csrf
                                        <input type="hidden" name="page_type" value="S">
                                        <input type="hidden" name="mem_type" value="Pro">
                                        <input type="hidden" name="subscription_id" value="{{@$val->id}}">
                                        <button type="submit" class="member_btn"  onclick="return confirm('Do you want to choose this package?')">Choose Plan</button>
                                        </form>
                                    @endif
                                    
                                    @endif

                                </div>

                            </div>

                            @endforeach

                            @endif

                            {{-- @if(@$free)
                            <div class=" col-sm-6">

                                <div class="member-box">

                                    

                                    <h2>@if(@$free->package_name == 'BRONZE')FREE @endif</h2>

                                    <h3>Free for all </h3>

                                    <h5><img src="{{ asset('public/frontend/images/tick3.png') }}"> {{@$free->package_name}} </h5>

                                    <p>{{@$free->package_desc}}</p>
                                    
                                    @if(@$package->pro_package_id == @$free->id)
                                    <a href="javascript:;" class="member_btn mem_active">Active</a>
                                    @else
                                    <a href="javascript:;" class="member_btn choose_plan" data-id="{{@$free->id}}">Choose plan</a>
                                    @endif

                                </div>

                            </div>
                            @endif --}}


                            {{-- @if(@$premium)
                            <div class=" col-sm-6">

                                <div class="member-box">

                                    <div class="ribbon2 ribbon-top-left"><strong>Popular</strong></div>

                                    <h2>{{@$premium->package_name}}</h2>
                                    @if(@$premium->type == 'Y')
                                    <h3 class="extra-mem">INR {{@$premium->yearly_price}} <span> /Yearly </span></h3>
                                    @else
                                    <h3 class="extra-mem">INR {{@$premium->monthly_price}} <span> /Monthly </span></h3>
                                    @endif

                                    <h5><img src="{{ asset('public/frontend/images/tick3.png') }}"> {{@$premium->package_name}}</h5>

                                    <p>{{@$premium->package_desc}}</p>

                                    @if(@$package->pro_package_id == @$premium->id)
                                    <a href="javascript:;" class="member_btn mem_active">Active</a>
                                    @else
                                    <a href="javascript:;" class="member_btn choose_plan" data-id="{{@$premium->id}}">Choose plan</a>
                                    @endif

                                    

                                </div>

                            </div>
                            @endif --}}



                        </div>



                    </div>

                        

                    </div>

                </div>

                

            </div>

        </div>

        

    </div>

</div>


@endsection


@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection



@section('script')
@include('includes.script')
<script>
    
</script>
<script>
    $('.choose_plan').on('click',function(){
        var id = $(this).data('id');

        
        if(confirm('Do you want to choose this plan')){
            $.ajax({
                url:'{{route('service.provider.update.package')}}',
                type:'GET',
                data:{id:id},
                success:function(data){
                    console.log(data.status);
                    if(data.status == 'success'){
                        location.reload();
                    }
                    
                }
            })
        }
    });
</script>
@endsection

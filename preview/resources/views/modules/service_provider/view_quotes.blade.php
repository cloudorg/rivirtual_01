@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> RiVirtual | Dashbord </title>
@endsection




@section('header')
@include('includes.header')
@endsection


@section('content')
<!----header--->
<div class="haeder-padding"></div>

<div class="dashboard-provider">
	<div class="container">
		<div class="row">
			@include('includes.sidebar')
            <div class="col-lg-9 col-md-12 col-sm-12 pro_view_quote">
            	<div class="cus-dashboard-right">
				   <h2 class="pro-heading-das">View Quote</h2>
				</div>
				<div class="user_dashboard_right pro_quote">

					<h1><img src="{{ asset('public/frontend/images/prop.png') }}" alt=""> Job Details</h1>



					<div class="proposal-list">

						<div class="proposal-de">

							<h5>{{@$quote->jobDetails->job_name}}</h5>

							<ul>

								<li> <img src="{{ asset('public/frontend/images/ic1.png') }}"> {{ @$quote->jobDetails->categoryName?@$quote->jobDetails->categoryName->category_name: '--' }}</li>

								<li> <img src="{{ asset('public/frontend/images/ic2.png') }}"> {{ @$quote->jobDetails->budget_range_from }} </li>

								<li> <img src="{{ asset('public/frontend/images/ic3.png') }}">{{ date('d.m.Y',strtotime(@$quote->jobDetails->job_start_date)) }}</li>

								<li> <img src="{{ asset('public/frontend/images/ic4.png') }}">{{ @$quote->jobDetails->address }}</li>

							</ul>

						</div>

						

					</div>

					@if(count(@$milestones) > 0)

						<div class="dash_top_inr">
							<div class="main-tabel">
                                <h4 class="txtcls mile_tit">Created Milestones</h4>
                                <div class="table-responsive">
                                    <div class="table">
                                        <div class="one_row1 hidden-sm-down only_shawo">
                                            <div class="cell1 tab_head_sheet">Date</div>
                                            <div style="width: 50%;" class="cell1 tab_head_sheet">Description</div>
                                            <div class="cell1 tab_head_sheet">Status</div>
                                            <div class="cell1 tab_head_sheet">Amount</div>
                                             <div class="cell1 tab_head_sheet">Action</div> 
                                        </div>
                                        @foreach($milestones as $val)
                                        <div class="one_row1 small_screen31 last_row_table">
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">Date</span>
                                                <p class="add_ttrr">{{ date('d M Y',strtotime(@$val->created_at))}}</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">Description</span>
                                                <p class="add_ttrr" style=" word-break: break-all ! important;">{!! nl2br(@$val->description ? $val->description :'-') !!} 
                                                    
                                                </p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">Status</span>
                                                <p class="add_ttrr"> @if(@$val->status == 'F') Funded @elseif(@$val->status == 'R') Released @endif</p>
                                            </div>
                                            <div class="cell1 tab_head_sheet_1">
                                                <span class="W55_1">Amount</span>
                                                <p class="add_ttrr">
                                                    {{@$val->amount}}
                                                </p>
                                            </div>
                                            
                                            
                                            
                                            @if(@$val->status == 'F' && @$val->is_requested_release != 'Y')
                                                   <div class="cell1 tab_head_sheet_1 last_padd last_padd_ac">
                                                        
                                                                
                                                                
                                                                <a href="javascript:;" class="rel-mod ml-rel" data-id="{{$val->id}}">Request Release</a>
                                                                
                                                                
                                                                
                                                               
                                                   </div>
                                            @else
                                            		<div class="cell1 tab_head_sheet_1 last_padd last_padd_ac">
                                            		</div>
                                            @endif
                                            
                                            
                                             
                                           
                                            
                                                
                                            
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
						</div>

						@endif



					<div class="user_das_right_inr">

						<div class="por-heding">

							<h4>Proposal</h4>

							

						</div>



						<div class="row">

							<div class="col-md-12">

								<div class="bodar1"> </div>

							</div>

						</div>


						@if(@$quote)
						<div class="proposal-box">



							<div class="proposal-top">

								<div class="pro-user">

									<em>

										@if(Auth::user()->profile_pic)
										<img src="{{ URL::to('storage/app/public/profile_picture')}}/{{auth()->user()->profile_pic}}">
										@else
										<img src="{{ URL::to('public/frontend/images/avatar.png')}}" alt="" id="img2">
										@endif

									</em>

									<div class="pro-user-name">

										<h5><a href="#">{{Auth::user()->name}}</a> <span><i class="icofont-star"></i>{{ number_format(@Auth::user()->avg_review,1,'.',',') }}</span></h5>

										<p>{{ @$quote->proDetails->proToCategory->categoryName->category_name }}</p>

									</div>

								</div>

								<div class="pro-award">
								@if(@$quote->status == 'Bid Placed')
									<a href="javascript:;" class="award_btn "> Bid Placed</a>
								@endif
								@if(@$quote->status == 'Awarded')
									<a href="javascript:;" class="ar_bt"> <img src="{{ asset('public/frontend/images/awa2.png') }}"> Awarded</a>
								@endif
								@if(@$quote->status == 'Closed')
									<a href="javascript:;" class="award_btn ">Closed</a>
								@endif

                       			@if(@$quote->status == 'Awarded' ) 
		                            @if(@$chat) 
		                            <a href="javascript:;" class="chat_btn" data-userid="{{@$quote->jobDetails->user_id}}" data-agentid="{{@$quote->user_id}}" data-name="{{@$quote->proDetails->name}}" data-username="{{@$job_data->getUser->name}}" data-usertype="P" data-msgtyp="Pr" data-propertyid="{{@$quote->user_id}}" data-bidid="{{@$quote->id}}" data-providerid="{{@$quote->user_id}}">Message</a>
		                            @endif
                        		@endif
                    			
									<!-- <a href="javascript:;" class=" ">Message</a> -->

								</div>

							</div>



							<div class="proposla-para">

								<p>{{@$quote->quote_description}}</p>

								<!-- <p class="moretext" style="display: none;">

		                          Blue sapphire is a very cold and extremely powerful gem stone and represents planet Saturn. It blesses with immense good luck, wealth and prosperity. 

		                        </p><a class="moreless-button"> Read More + </a> -->

							</div>



							<div class="proposla-ul">

								<ul>

									<li><img src="{{ asset('public/frontend/images/lista.png') }}"> <span>Budget: <b>@if(@$quote->jobDetails->budget_range_from != null) ₹{{@$quote->cost}} @else --- @endif </b></span></li>

									<li><img src="{{ asset('public/frontend/images/lista.png') }}"> <span>Time: <b>{{@$quote->timeline}} Days</b></span></li>

								</ul>

							</div>

						</div>
						@endif

						
						



										{{-- <nav aria-label="...">

											<ul class="pagination new-pagination">

												<li class="page-item"> <span class="page-link">Prev </span> </li>

												<li class="page-item active"><a class="page-link" href="#">1</a></li>

												<li class="page-item" aria-current="page"> <span class="page-link">

                                                2

                                                <span class="sr-only">(current)</span> </span>

												</li>

												<li class="page-item"><a class="page-link" href="#">3</a></li>

												<li class="page-item"><a class="page-link" href="#">4</a></li>

												<li class="page-item"> <a class="page-link act" href="#">Next </a> </li>

											</ul>

										</nav> --}}

					</div>

				</div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection



@section('script')
@include('includes.script')
<script>
$('.rel-mod').click(function(){
    		var m_id = $(this).data('id');
    		if(confirm('Do you want to place a request to release this milestone ?')){
    				$.ajax({
                	url:'{{route('release.milestone.request')}}',
                	type:'GET',
                	data:{id:m_id},
                	success:function(data){
                		
                		if(data.status == 1){
                			location.reload();
                		}
                    }
                })
    		}
    	});
	$('.action_menu').click(function(){
                var id = $(this).data('id');
                if($("#show-action-"+id).css('display')=="none"){
                        $('.show-actions').slideUp();
                        $("#show-action-"+id).slideDown();
                }else{
                    $('.show-actions').slideUp();
                }
         });
</script>
@endsection

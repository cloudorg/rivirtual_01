@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> RiVirtual | Contact Us </title>
@endsection




@section('header')
@include('includes.header')
@endsection


@section('content')
<div class="haeder-padding"></div>

    <!-----filter--------->

    <div class="priv-sec">

        <div class="container">

            <div class="row">

                

                    <div class="col-lg-10 col-xl-9 col-md-11 mx-auto">

                        <div class="section-heading2 text-center">

                            <h2>Terms and Condition</h2>

                            <p>This website is the property of RiVirtual limited C-13 Cyber Towers  HI-TECH City  Hyderabad Telangana 500 081. By visiting this website, you agree that you consent to and are bound by the following terms and conditions:</p>

                        </div>

                    </div>

                <div class="col-12">

                    <div class="privacy-content mb-60">

                    <h3>1. Copyright and Trade Marks</h3>

                    <p>All content and design of this website is protected by copyright, trademarks and other intellectual property rights and is the property of RiVirtual or issued under license from third party copyright owners. RiVirtual Limited. You may print or download such material in electronic form on your local hard disk for your personal, non-commercial use. You may not alter or otherwise make any changes to any material that you print or download, including, without limitation, removing any copyright or proprietary notices. All other uses are prohibited including, without limitation, distributing, reproducing, modifying or copying or using for commercial purposes any of the materials or contents of this site. The license to copy also does not permit incorporation of the content or any part of the website in any other work or publication in any form whatsoever. If you wish to use the material in this website for any other purpose, please email legal@rivirtual.com</p>

                    <h3>2. Disclaimer</h3>

                    <p>RiVirtual takes all reasonable care to ensure that the information contained on this website is accurate, however, we cannot guarantee its accuracy and we reserve the right to change the information on this website (including these terms and conditions) at any time. You must therefore check these terms and conditions for any such changes each time you visit this website.</p>
                    <p>RiVirtual makes no representations or warranties of any kind with respect to this website or the content contained on it, including any text, graphics,advertisements, links or other items. Furthermore, neither RiVirtual nor any other contributor to this website make any representation or gives any warranty, condition, undertaking or term either expressed or implied as to the condition, quality, performance, accuracy, fitness for purpose, completeness or freedom from viruses of the content contained on this website or that such content will be accurate, up to date, uninterrupted or error free. Nothing on this website shall be regarded or taken as financial advice.
</p>

                    <h3>3. Law</h3>

                    <p> The user confirms that the terms and conditions and use of this website shall be governed by the laws of the Republic of India and that any and all disputes arising there from shall be subject to the exclusive jurisdiction of Indian courts.</p>
                    <p>RiVirtual respect and value the security and privacy of those of its contacts. RiVirtual follows strict security procedures in the storage and disclosure of personal information in order to prevent unauthorized access. Personal Data is held on our contacts and administration database either because of work we have undertaken or are currently engaged in, or because we believe that clients may be interested in receiving material from us about our business and services. As such we hold client contact details and the history of our client relationship. This allows us to manage our client relationships effectively and target items of interest so that you do not receive unwanted material through the post.</p>
                    <p>In accordance with your rights under the Act, you may request the amendment of the personal information held or to cease receiving direct marketing materials. RiVirtual does not sell, rent or deal in the personal information we hold. Any information you provide will be treated in confidence and used only for the purpose for which you provided it.</p>

                    <h3>4. Privacy policy</h3>

                    <p>RiVirtual is committed to safeguarding your privacy online. The information we collect from you is only that required by us, in order for us to provide you with the information you have requested. We do not and will not supply your personal information to any other person unless you have expressly authorised us to do so.</p>

                    <h3>5. How to contact RiVirtual</h3>

                    <p> We welcome your views about our website and our Privacy Policy. If you would like to contact us with any queries or comments please send an e-mail to hello@rivirtual.com</p>

                    

                    <h3>6. RiVirtual Property Details</h3>

                    <p>All prices are subject to contract. Property details are based upon the instruction of the Seller and do not constitute or form any part of any offer or contract. Legal title is subject to verification from the Solicitors involved.</p>
                    <p>All floor plans and photographs are for illustration purposes only. Internal measurements are approximate.</p>


                    <h3>7. Liability</h3>

                    <p>Due to the nature of the software and the Internet, no guarantee is given of uninterrupted or error-free access to or running of this website or the information transactions conducted through it and RiVirtual shall not be liable for any failure in processing your details due to software or Internet errors or unavailability, or circumstances beyond its reasonable control.</p>
                    <p>RiVirtual makes no warranty that this site is free from errors, defects or viruses. Whilst RiVirtual have no obligation to update this site, RiVirtual makes all reasonable efforts to ensure that the information published on this website is complete, accurate and up-to-date, no representations or warranties are made (expressed or implied) as to the accuracy or completeness of such information.</p>
                    <p>RiVirtual cannot accept any responsibility (to the extent permitted by law) for any loss arising directly or indirectly from the use of, or any action taken in reliance on, any information appearing on this website or any other website to which it may be linked. Nothing on this website shall be deemed to constitute financial advice and in the event that you wish to have any such advice, you should contact a financial advisor.</p>

                  </div>

                </div>

            </div>

        </div>

    </div>

@endsection


@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection



@section('script')
@include('includes.script')
@include('includes.toaster')
<script>
    $(document).ready(function(){
        $('#rent_link').click(function(){
            $('#rent').click();
        })
        $('#buy_link').click(function(){
            $('#buy').click();
        })
    })
</script>
<script>
    function search_result_check(that) {
        var $this = that;
        var name = $($this).val();
        console.log($($this).val());
        $.ajax({
            type: "POST",
            url:"{{route('get.locality.available')}}",
            data: {
                'name': name,
                '_token':'{{@csrf_token()}}'
            },
            success: function(data) {
              if(data){
                console.log(data);
                $('#serch_result').show();
                $('#serch_result').fadeIn();
                $('#serch_result').last().html(data);

              }
              else
              {
                $('#serch_result').children().remove();
              }
            }
        });
    }
    function add_searchbar(that) {
        var $this = that;
        var name = $($this).text();
        console.log(name);
        $('#location').val(name);
        $($this).remove();
        $('#serch_result').children().remove();
        $('#serch_result').hide();
    }
</script>
@endsection

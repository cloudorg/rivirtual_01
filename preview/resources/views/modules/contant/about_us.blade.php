@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> RiVirtual | Home </title>
@endsection




@section('header')
@include('includes.header')
@endsection


@section('content')
<div class="haeder-padding"></div>

    <!-----filter--------->



    <div class="about-section">

        <div class="container">

            <div class="row">

                <div class="col-lg-12">

                    <div class="breadcrumbs-inner">

                        <h1 class="page-title">About Us</h1>

                        <div class="breadcrumbs-list">

                            <ul>

                                <li><a href="index.html"><span class=""><i class="icofont-home"></i></span> Home</a></li>

                                <li>About Us</li>

                            </ul>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    

    <div class="about-inner">

        <div class="container">

            <div class="row">

                <div class="col-md-6 align-self-center">

                    <div class="about-us-img-wrap about-img-left">

                        <img src="{{ asset('public/frontend/images/about1.png') }}" alt="About Us Image">

                        <div class="about-us-img-info about-us-img-info-2 about-us-img-info-3">

                            

                            <div class="">

                                <img src="{{ asset('public/frontend/images/about2.png') }}" alt="video popup bg image">

                                

                            </div>

                        </div>

                    </div>

                </div>



                <div class="col-md-6 align-self-center">

                    <div class="about-us-info-wrap">

                        

                        <h1 class="section-title">#1 Commercial Real Estate Advisor Platform in India<span>.</span></h1>

                        <p>RiVirtual is a privately owned global real estate intelligence company, development, and management firm with a presence in 100 cities in 5 countries and more than 100 million square feet of assets for which RiVirtual provides third-party property-level services. RiVirtual's Artificial Intelligence ( AI) is based on Accessibility, Affordability, Availability, and Authenticity ( 4As) models of real estate markets, Economic Data, and Sustainability.</p>
                        <p>India's real estate sector is expected to touch a US$ 1 trillion market size by 2030, accounting for 18-20% of India's GDP. More than 85% of commercial real estate decisions begin with an online search. Your next tenant or investor will see your property first online, only if it’s on RiVirtual. RiVirtual listings achieve greater visibility than those anywhere else on the web. In an era where the first tour happens online, give your property the winning advantage.</p>

                        



                        <ul class="list-item-half clearfix">

                            <li>

                                <i class="icofont-building-alt"></i>

                                Commercial Real Estate

                            </li>

                            <li>

                                <i class="icofont-beach"></i>

                                Vacation Homes

                            </li>

                            <li>

                                <i class="icofont-cycling-alt"></i>

                                Sustainable Properties

                            </li>

                            <li>

                                <i class="icofont-cop-badge"></i>

                                Gated Communities

                            </li>

                        </ul>



                        <a href="{{route('contact.us')}}" class="theme-btn">Contact Us</a>

                    </div>

                </div>

            </div>

        </div>

    </div>



<section class="property-sec py-50">

    <div class="container">

        <div class="row">

            <div class="col-lg-10 col-xl-9 col-md-11 mx-auto">

                <div class="section-heading2 text-center">

                    <h2>Our Vision And Mission</h2>

                    <p>#1 Real Estate Advisor Platform in India</p>

                </div>

            </div>

            <div class="col-sm-6 col-lg-4">

                <div class="misson-box">

                    <img src="{{ asset('public/frontend/images/miss.png') }}">

                    <h2>Our Mission</h2>

                    <p>We’re making homeownership simpler, affordable, and more accessible for all Indians.</p>

                </div>

            </div>

            <div class="col-sm-6 col-lg-4">

                <div class="misson-box">

                    <img src="{{ asset('public/frontend/images/visa.png') }}">

                    <h2>Our Vision</h2>

                    <p>RiVirtual is a Real Estate Intelligence Platform for real estate entrepreneurs, investors, homeowners, and the common man.</p>

                </div>

            </div>

            <div class="col-sm-6 col-lg-4">

                <div class="misson-box">

                    <img src="{{ asset('public/frontend/images/vis.png') }}">

                    <h2>Our Goal</h2>

                    <p>Outstanding agents. Outstanding results. Excellent pros. Outstanding outcomes. We strive to be the best. Make a real difference around the globe. One World. One RiVirtual. Business for, but not by, yourself.</p>

                </div>

            </div>

        </div>

    </div>



    

</section>

@endsection


@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection



@section('script')
@include('includes.script')
@include('includes.toaster')
<script>
    $(document).ready(function(){
        $('#rent_link').click(function(){
            $('#rent').click();
        })
        $('#buy_link').click(function(){
            $('#buy').click();
        })
    })
</script>
<script>
    function search_result_check(that) {
        var $this = that;
        var name = $($this).val();
        console.log($($this).val());
        $.ajax({
            type: "POST",
            url:"{{route('get.locality.available')}}",
            data: {
                'name': name,
                '_token':'{{@csrf_token()}}'
            },
            success: function(data) {
              if(data){
                console.log(data);
                $('#serch_result').show();
                $('#serch_result').fadeIn();
                $('#serch_result').last().html(data);

              }
              else
              {
                $('#serch_result').children().remove();
              }
            }
        });
    }
    function add_searchbar(that) {
        var $this = that;
        var name = $($this).text();
        console.log(name);
        $('#location').val(name);
        $($this).remove();
        $('#serch_result').children().remove();
        $('#serch_result').hide();
    }
</script>
@endsection

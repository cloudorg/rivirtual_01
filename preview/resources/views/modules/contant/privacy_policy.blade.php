@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> RiVirtual | Contact Us </title>
@endsection




@section('header')
@include('includes.header')
@endsection


@section('content')
<div class="haeder-padding"></div>

    <!-----filter--------->

    <div class="priv-sec">

        <div class="container">

            <div class="row">

                

                    <div class="col-lg-10 col-xl-9 col-md-11 mx-auto">

                        <div class="section-heading2 text-center">

                            <h2>Privacy Policy</h2>

                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit sed lorem sapien, auctor in justo id, dignissim dipiscing elit sed lorem sapien</p>

                        </div>

                    </div>

                <div class="col-12">

                    <div class="privacy-content mb-60">

                    <h3>1. What’s included in this privacy notice?</h3>

                    <p>RiVirtual is a Real Intelligence marketplace based in India. You can find out more information about us from the information found on our website.</p>
                    <p>This document (our “privacy notice”) sets out information relating to how we use personal information relating to individuals we have dealings with, including people looking to sell a property, those looking to buy property, landlords and tenants. It also sets out information about what rights individuals have in relation to their personal information and various other matters required under data protection law.</p>
                    <p>In particular, this privacy notice provides information to individuals about how they can object to our use of their personal information, how they can withdraw any permissions they have given to us to enable us to process their personal information and how they can make a complaint.</p>

                    <h3>2. Who does this privacy notice apply to?</h3>
                        <p>This privacy notice applies to:</p>
                        <ul>
                           <li>our Customers:
                            <ul class="disc_vli">
                                <li>Vendors;</li>
                                <li>Landlords;</li>
                                <li>Purchasers; and</li>
                                <li>Tenants;</li>

                            </ul>
                           </li> 
                           <li>individuals who contact us with enquiries;</li>
                           <li>individuals who use our website;</li>
                           <li>individuals who register for our mailing list;</li>
                           <li>individuals we engage to provide services on our behalf, for example builders, plumbers etc. ; and</li>
                           <li>individuals who engage with us on social media .</li>

                        </ul>
                    <p>In the sections below, when referring to the individuals listed above, we use the terms “you” or “your”.</p>
                    <p>One or more of the above categories may apply to you, for example purchasers will often have contacted us with an enquiry when searching for a property to purchase and will often have registered for our mailing list and/or used our website.</p>

                    <h3>3. What’s our approach to privacy?</h3>

                    <p> We take your privacy extremely seriously and want you to feel confident that your personal information is safe in our hands. </p>
                    <p>We will only use your personal information in accordance with data protection law applicable to India from time to time.</p>
                    <p>Under data protection law, when we use your personal information, we will be acting as a data controller. Essentially, this means that we will be making decisions about how we want to use your personal information and why.</p>
                    <p class="para_borders">We must be upfront about how we intend to use your personal information and must use your personal information fairly. Providing privacy information to individuals (such as in this privacy notice) is one aspect of using personal information fairly.</p>
                    <div  class="para_borders  bp0">
                    <p>We must only use your personal information if we have a legal basis to do so under data protection law. These legal bases include:</p>
                    <ul>
                        <li>That we need to use your personal information to perform a contract between us (or to take steps at your request prior to entering into a contract);</li>
                        <li>That we (or someone else) has a legitimate reason for needing to use your personal information and those legitimate interests are not outweighed by your rights or interests. We must balance our respective rights and interests before we can rely upon this legal basis;</li>
                        <li>That you have consented to our use of your personal information; and/or</li>
                        <li>We need to use your personal information to comply with laws and regulations we are subject to.</li>
                        

                    </ul>
                    </div>
                    <div  class="para_borders bp0">
                    <p class="mb-0">We are only permitted to share your personal information with others in certain circumstances and if we take steps to ensure that your personal information will be secure.</p>
                </div>
                <div  class="para_borders bp0">
                    <p class="mb-0">Generally speaking, we must only use your personal information for the specific purposes we have told you about. If we want to use your personal information for other purposes, we need to contact you again to tell you about this.</p>
                </div>
                <div  class="para_borders bp0">
                    <p class="mb-0">We must not hold more personal information than we need for the purposes we have told you about and must not retain your personal information for longer than is necessary for those purposes (this is known as the “retention period”). We must also dispose of any information that we no longer need securely.</p>
                </div>
                <div  class="para_borders bp0">
                    <p class="mb-0">We must ensure that we have appropriate security measures in place to protect your personal information.</p>
                </div>
                <div  class="para_borders bp0">
                    <p class="mb-0">We must act in accordance with your rights under data protection law.</p>
                </div>
                <div  class="para_borders">
                    <p class="mb-0">We must not transfer your personal information outside the Indian Economic Area unless certain safeguards are in place.</p>
                </div>
                    <p class="mt-2">Below, we summarise the main rules that apply to us under data protection law when we use your personal information:</p>
                    


                    <h3>4. How will we use your personal information?</h3>

                    <p>How we will use your personal information, the legal bases we will rely upon, how long we will keep your personal information and other details will depend upon who you are and why we need your personal information in the first place.</p>
                    <p>In this section, we provide specific privacy information relating to the different categories of individuals that this privacy notice applies to.</p>

                    <p>Our customers</p>
                    <p>Vendors</p>

                    <div  class="para_borders dflexsnt bp0">
                    <p>What personal information we will use</p>
                    <ul>
                        <li>Your name;</li>
                        <li>Your address;</li>
                        <li>Your email address;</li>
                        <li>Your telephone number;</li>
                        <li>Photographic ID;</li>
                        <li>Proof of address i.e. bank statements, utility bills etc.</li>
                        

                    </ul>
                    </div>
                    <div  class="para_borders dflexsnt bp0">
                    <p>How we will obtain the personal information</p>
                    <ul>
                        <li>Provided by you when you ask us to market your property.</li>
                        

                    </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>What purposes we will use the personal information for</p>
                    <ul>
                        <li>We will use your name, address and other contact details to contact you about your property and progress with selling it and facilitate the sale;</li>
                        <li>We will keep a record of the information listed above for our internal administrative purposes;</li>
                        <li>We will insert your name and address on the memorandum of sale, which is shared with the Purchaser and the Purchaser’s  solicitors;</li>
                        <li>We will also use the above information to comply with the Money Laundering Regulations.</li>
                        

                    </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>The legal bases for processing we rely upon</p>
                    <ul>
                        <li>Our use of your personal information, in order to progress and facilitate the sale of your property, is in connection with the supply of services to you and is necessary for the performance of the contract between us;</li>
                        <li>Our use of your personal information, for our internal administrative purposes, is based on our legitimate interests in ensuring that our business is run properly and efficiently;</li>
                        <li>Our use of your personal information, for legal or regulatory purposes, is necessary to enable us to comply with our legal and regulatory obligations in relation to anti-money laundering regulations etc. and/or to enable us to bring, defend or deal with legal claims.</li>
                        
                    </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>How long we retain the personal information and why</p>
                    <ul>
                        <li>We usually keep records relating to any property sale for 7 years after the services have completed, in case of any contractual disputes.</li>
                        
                    </ul>
                </div>
                <div  class="para_borders dflexsnt">
                    <p>Consequences of not providing/permitting us to obtain personal information</p>
                    <ul>
                        <li>Without your name, contact details and identification documentation we will be unable to supply services to you.</li>
                    </ul>
                </div>

                    <h4>Landlords</h4>
                    <div  class="para_borders dflexsnt bp0">
                    <p>What personal information we will use</p>
                    <ul>
                        <li>Your name;</li>
                        <li>Your address;</li>
                        <li>Your email address;</li>
                        <li>Your telephone number;</li>
                        <li>Bank details;</li>
                        <li>Photographic ID;</li>
                        <li>Proof of address i.e. bank statements, utility bills etc;</li>
                        <li>Rent Agreement; and</li>
                        <li>Deposit Protection Service ID number (if applicable).</li>

                    </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>How we will obtain the personal information</p>
                    <ul>
                        <li>Provided by you when you ask us to market your property.</li>
                    </ul>
                     </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>What purposes we will use the personal information for</p>
                    <ul>
                        <li>We will use your name, address and other contact details to contact you about your property and progress with renting it and to facilitate the rental;</li>
                        <li>We will insert your name and address on the tenancy agreement which is shared with the tenants;</li>
                        <li>Where we are managing a rental property for you, we will use your name, address and other contact details to inform you of any issues in relation to the tenancy and to keep you updated generally in relation to the property;</li>
                        <li>We will use your bank details to make payment of any rent collected on your behalf and transfer any bond payments to you;</li>
                        <li>Other than your bank details, we will keep a record of the information listed above for our internal administrative purposes;</li>
                        <li>If you are an overseas landlord, we shall provide your name, address and other contact details to HMRC in order to comply with laws and regulations relating to overseas landlords;</li>
                        <li>We may be required to provide your name, address and other contact details to utility companies to ensure the seamless supply of services to your properties and to ensure that you are billed accurately;</li>
                        <li>If we are not managing your rental property on your behalf, we will provide your contact details and bank details to any tenant(s) so that they have a point of contact and can make rental payments to you directly;</li>
                        <li>We will also use the above information (other than your bank details), for legal and regulatory purposes (such as checking compliance with Section 17 in the Registration Act 1908 regulations (if applicable) and Money Laundering Regulations etc.</li>
                        

                    </ul>
                     </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>The legal bases for processing we rely upon</p>
                    <ul>
                        <li>Our use of your personal information, in connection with the supply of services to you and making payments to you, is necessary for the performance of the contract between us;</li>
                        <li>Our use of your personal information for our internal administrative purposes is based on our legitimate interests in ensuring that our business is run properly and efficiently;</li>
                        <li>Our use of your personal information for legal or regulatory purposes is necessary to enable us to comply with our legal and regulatory obligations (such as   checking compliance As per Section 17 in the Registration Act 1908, it is quintessential to register for leases and rent of immovable property from year-to-year or for any term beyond one year (if applicable) and Money Laundering Regulations etc.) and/or to enable us to bring, defend or deal with legal claims.</li>
                        

                    </ul>
                     </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>How long we retain the personal information and why</p>
                    <ul>
                        <li>We only use your bank details for the purposes of making payment(s). We will store your bank details for the duration of our contract for services or until all payments under the contract have been made, whichever is later;</li>
                        <li>We usually keep records relating to any property rental for 7 years after the services have completed, in case of any contractual disputes.</li>
                        

                    </ul>
                     </div>
                <div  class="para_borders dflexsnt ">
                    <p>Consequences of not providing/permitting us to obtain personal information</p>
                    <ul>
                        <li>Without your personal information we will be unable to supply services to you.</li>
                        
                    </ul>
                </div>
                    <h4>Purchasers</h4>
                    <div  class="para_borders dflexsnt bp0">
                    <p>What personal information we will use</p>
                    <ul>
                        <li>Your name;</li>
                        <li>Your address;</li>
                        <li>Your email address;</li>
                        <li>Your telephone number;</li>
                        <li>Photographic ID;</li>
                        <li>Proof of your current address e.g. utility bill or bank statement;</li>
                        <li>Proof of funding for the purchase (where this is by way of gift we will also need photographic ID and proof of address of the person gifting the money).</li>
                    </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>How we will obtain the personal information</p>
                    <ul>
                        <li>Provided by you when you make an offer to purchase a property being marketed by us.</li>
                        

                    </ul>
                    </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>What purposes we will use the personal information for</p>
                    <ul>
                        <li>We will use your name, address and other contact details to contact you about properties you are interested in buying;</li>
                        <li>We will use proof of funding information to ensure you have adequate funding in place to proceed with a purchase;</li>
                        <li>We will insert your name and address on the memorandum of sale which is shared with the Vendor and the Vendor’s solicitors;</li>
                        <li>We will keep a record of the information listed above for our internal administrative purposes;</li>
                        <li>We will also use the above information, for legal and regulatory purposes (such as compliance with the Money Laundering Regulations).</li>
                        

                    </ul>
                    </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>The legal bases for processing we rely upon</p>
                    <ul>
                        <li>Our use of your personal information to contact you about properties, to check your funding arrangements and for our internal administrative purposes is based on our legitimate interests in ensuring that our business is run properly and efficiently and that we provide both Vendors and Purchasers with an effective service;</li>
                        <li>Our use of your personal information for legal or regulatory purposes is necessary to enable us to comply with our legal and regulatory obligations (such as compliance with Money Laundering Regulations and/or to enable us to bring, defend or deal with legal claims.</li>
                        

                    </ul>
                    </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>How long we retain the personal information and why</p>
                    <ul>
                        <li>We usually keep records relating to any property sale for 7 years after the services have completed, in case of any contractual disputes.</li>
                        
                    </ul>
                    </div>
                <div  class="para_borders dflexsnt">
                    <p>Consequences of not providing/permitting us to obtain personal information</p>
                    <ul>
                        <li>Without your personal information we will be unable to proceed with a purchase.</li>
                        
                    </ul>
                    </div>
                    <h4>Tenants</h4>
                    <div  class="para_borders dflexsnt bp0">
                    <p>What personal information we will use</p>
                    <ul>
                        <li>Your name;</li>
                        <li>Your address;</li>
                        <li>Your email address;</li>
                        <li>Your telephone number;</li>
                        <li>Your date of birth;</li>
                        <li>Your gender;</li>
                        <li>Your employment status and details of your employer;</li>
                        <li>Your marital status and family details;</li>
                        <li>Information about your income and benefits;</li>
                        <li>Details of your previous landlord;</li>
                        <li>References;</li>
                        <li>Credit reference information;</li>
                        <li>Bank details;</li>
                        <li>Photographic ID;</li>
                        <li>Evidence of your right to remain in the India i.e. a work permit and/or visa;</li>
                        <li>Whether you are a smoker or non-smoker and whether you have any pets.</li>
                        



                    </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>How we will obtain the personal information</p>
                    <ul>
                        <li>Provided by you when you ask to rent a property marketed by us;</li>
                        <li>Provided by referees when asked to provide a reference in relation to you;</li>
                        <li>Provided by credit reference agencies when asked to carry out a credit check in relation to you.</li>
                    </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>What purposes we will use the personal information for</p>
                    <ul>
                        <li>We will use your name, address and other contact details to contact you about properties you are interested in renting and for internal administrative purposes;</li>
                        <li>Your name and details of your previous landlord/employers will be used to request a reference;</li>
                        <li>Certain personal information will be used to obtain a credit reference for you;</li>
                        <li>Income and benefit details, references and credit reference information received will be used to assess your suitability as a tenant, including whether the property is affordable</li>
                        <li>Your status as a smoker/non-smoker and information as to whether you own any pets will be used to check whether you are able to rent the property based on the landlord’s specified requirements;</li>
                        <li>We will provide your name and contact details to any contractors we engage to maintain or repair the property;</li>
                        <li>We will provide your name, address and other contact details to utility companies supplying the property and to the local authority for council tax purposes</li>
                        <li>If we are not managing the property on the landlord’s behalf, we shall provide your name and contact details to the landlord and/or the landlord’s managing agent;</li>
                        <li>We will use your bank details to return any overpayment of fees or monies due to you;</li>
                        <li>We will also use your personal information, for legal and regulatory purposes such as to comply with the Money Laundering Regulations.</li>
                        


                    </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>The legal bases for processing we rely upon</p>
                    <ul>
                        <li>Our use of your personal information, to contact you about properties and for our internal administrative purposes, is based on our legitimate interests in ensuring that our business is run properly and efficiently and that we provide our customers with an effective service;</li>
                        <li>Our use of your personal information, for credit referencing and to obtain references, is based on our legitimate interests and the legitimate interests of our Landlord customers in ensuring that we provide suitable tenants for rental properties.</li>
                        <li>Our use of your personal information, for legal or regulatory purposes, is necessary to enable us to comply with our legal and regulatory obligations and/or to enable us to bring, defend or deal with legal claims.</li>
                        

                    </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>How long we retain the personal information and why</p>
                    <ul>
                        <li>Any bank details provided to us, for the purpose of making a payment to you, shall not be stored, as they shall only be used for any payments due to you, and shall subsequently be destroyed;</li>
                        <li>We usually keep records relating to any property rental for 7 years after the services have completed, in case of any contractual disputes.</li>
                        

                    </ul>
                </div>
                <div  class="para_borders dflexsnt ">
                    <p>Consequences of not providing/permitting us to obtain personal information</p>
                    <ul>
                        <li>Without your name and contact details we will be unable to correspond with you and provide you with any services;</li>
                        <li>Without the other personal information requested, we will be unable to arrange a credit reference or other reference in relation to you, which may mean that the Landlord is not prepared to enter into a tenancy agreement with you.</li>
                        
                    </ul>
                </div>
                   <h4>Guarantors</h4>

                <div  class="para_borders dflexsnt bp0">
                   <p>What personal information we will use</p>
                   <ul>
                       <li>Your name;</li>
                       <li>Your address;</li>
                       <li>Your email address;</li>
                       <li>Your telephone number;</li>
                       <li>Your date of birth;</li>
                       <li>Your gender;</li>
                       <li>Your employment status and details of your employer;</li>
                       <li>Information about your income and benefits;</li>
                       <li>References;</li>
                       <li>Credit reference information.</li>
                       


                   </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                   <p>How we will obtain the personal information</p>
                   <ul>
                       <li>Provided by the tenant who wants to rent a property and/or you;</li>
                       <li>Provided by referees when asked to provide a reference in relation to you;</li>
                       <li>Provided by credit reference agencies when asked to carry out a credit check in relation to you.</li>
                       

                   </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                   <p>What purposes we will use the personal information for</p>
                   <ul>
                       <li>Your personal information will be used to request a reference;</li>
                       <li>Certain personal information will be used to obtain a credit reference for you;</li>
                       <li>Income and benefit details, references and credit reference information received will be used to assess your financial ability to act as a suitable guarantor for the tenant;</li>
                       <li>If we are not managing the property on the landlord’s behalf, we shall provide your name and contact details to the landlord and/or the landlord’s managing agent.</li>
                       

                   </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                   <p>The legal bases for processing we rely upon</p>
                   <ul>
                       <li>Our use of your personal information, for credit referencing and to obtain references, is based on our legitimate interests and the legitimate interests of our Landlord customers in ensuring that the rent for the property can be paid.</li>
                   </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                   <p>How long we retain the personal information and why</p>
                   <ul>
                       <li>We usually keep records relating to any property rental for 7 years after the services have completed, in case of any contractual disputes.</li>
                   </ul>
                </div>
                <div  class="para_borders dflexsnt ">
                   <p>Consequences of not providing/permitting us to obtain personal information</p>
                   <ul>
                       <li>Without your personal information, we will be unable to arrange a credit reference or other reference in relation to you, which may mean that the Landlord is not prepared to enter into a tenancy agreement with the tenant in question.</li>
                   </ul>
                </div>
                   <h4>Individuals who contact us with inquiries</h4>

                <div  class="para_borders dflexsnt bp0">
                   <p>What personal information we will use</p>
                   <ul>
                       <li>Your name;</li>
                       <li>Your contact details (such as your telephone number, address or email address);</li>
                       <li>Details of your enquiry, which may include details of your property or your search criteria for a property to buy or rent.</li>

                   </ul>

                </div>
                <div  class="para_borders dflexsnt bp0">
                   <p>How we will obtain the personal information</p>
                   <ul>
                       <li>Provided by you when you contact us directly (e.g. by making a phone call or emailing us) or make an enquiry at our premises;</li>
                       <li>Provided to us by a third-party website, such as Rightmove or Zoopla, when you raise an inquiry with us via the third-party website.</li>

                   </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                   <p>What purposes we will use the personal information for</p>
                   <ul>
                       <li>We will use the personal information to deal with your enquiry;</li>
                       <li>We will also make a record of your enquiry for internal administrative purposes.</li>
                       

                   </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                   <p>The legal bases we rely upon</p>
                   <ul>
                       <li>Our use of your personal information, to deal with your enquiry, is based on your consent and our legitimate interests in ensuring our business is run efficiently and effectively;</li>
                       <li>Our use of your personal information for record keeping purposes is based on our legitimate interests in ensuring our business is run efficiently and effectively.</li>
                       

                   </ul>
                </div>
                <div  class="para_borders dflexsnt ">
                   <p>How long we retain the personal information and why</p>
                   <ul>
                       <li>We will retain your personal information until your enquiry is dealt with and/or until the property you have enquired about is sold or let, whichever is longer. </li>
                   </ul>
                </div>
                   <h4>Individuals who use our website</h4>
                <div  class="para_borders dflexsnt bp0">
                   <p>What personal information we will use</p>
                   <ul>
                       <li>We use Google Analytics in relation to our website;</li>
                       <li>Google Analytics mainly uses first-party cookies to report on the numbers of individuals who use our website. We are not provided with any information which identifies you personally;</li>
                       <li>For further information about what information is processed by Google Analytics please click on this link  <a href="https://support.google.com/analytics/answer/6004245">https://support.google.com/analytics/answer/6004245.</a></li>
                       

                   </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                   <p>How we will obtain the personal information</p>
                   <ul>
                       <li>The information is obtained via Google Analytics when you access our website.</li>
                   </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>What purposes we will use the personal information for</p>
                    <ul>
                        <li>The above information is used by us to help us to continually improve our website.</li>
                        

                    </ul>
                </div>
                <div  class="para_borders dflexsnt ">
                    <p>The legal grounds we rely upon</p>
                    <ul><li>Our collection and use of the above information is based on our legitimate interests in ensuring that our website is user-friendly and appeals to our customers.</li></ul>
                </div>
                    <h4>Individuals who register for our mailing list</h4>
                <div  class="para_borders dflexsnt bp0">
                    <p>What personal information we will use</p>
                    <ul>
                        <li>Your name, email address, telephone number and postal address;</li>
                        <li>Your buying/letting position;</li>
                        <li>Your relevant financial position i.e.whether you are a cash buyer or have a mortgage;</li>
                        <li>Your property preferences.</li>
                        
                    </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>How we will obtain the personal information</p>
                    <ul>
                        <li>Provided by you when you subscribe to our mailing list;</li>
                        <li>Provided by third party websites, provided you have selected the provision of such details to third parties (us) as a preference on their website.</li>

                    </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>What purposes we will use the personal information for</p>
                    <ul>
                        <li>To provide you with the updates you have requested;</li>
                        <li>To provide you with related information that we think may be of interest to you.</li>
                    </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>The legal grounds we rely upon</p>
                    <ul>
                        <li>We will rely on your consent to provide you with the updates you have requested and to retain your details on our marketing database;</li>
                        <li>We will rely on your consent to provide you with other information that may be of interest to you.</li>
                    </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>How long we retain the personal information and why</p>
                    <ul>
                        <li>We will retain your personal information until you notify us that you unsubscribe from our mailing list.</li>
                    </ul>
                </div>
                <div  class="para_borders dflexsnt ">
                    <p>Consequences of not providing/permitting us to obtain personal information</p>
                    <ul>
                        <li>Without your contact details, we will not be able to provide you with updates;</li>
                        <li>You can opt-out of receiving related information at the time you subscribe to updates and each subsequent time we contact you.</li>
                    </ul>
                </div>
                    <h4>Individuals we engage to provide services to us</h4>
                <div  class="para_borders dflexsnt bp0">
                    <p>What personal information we will use</p>
                    <ul>
                        <li>Your name and contact details;</li>
                        <li>Your bank account details.</li>
                        

                    </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>How we will obtain the personal information</p>
                    <ul>
                    <li>Provided by you when you agree to provide us with services.</li>
                        
                    </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>What purposes we will use the personal information for</p>
                    <ul>
                        <li>To enter into an agreement with you, to contact you, to administer the agreement for services and to pay you</li>
                    </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>The legal grounds we rely upon</p>
                    <ul>
                        <li>The use of your personal information, to enter into an agreement for services, for correspondence in relation to the services and associated matters and to make payment for services provided, will be necessary for the purposes of taking steps prior to entering into a contract with you and for the performance of the contract between us.</li>
                    </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>How long we retain the personal information and why</p>
                    <ul>
                        <li>We will retain your personal information for the duration of the provision of services and for 7 years thereafter in case there should be any contractual dispute.</li>
                    </ul>
                </div>
                <div  class="para_borders dflexsnt ">
                    <p>Consequences of not providing/permitting us to obtain personal information</p>
                    <ul>
                        <li>Without your personal information, we will not be able to engage you to provide us with services nor will we be able to pay you.</li>
                    </ul>
                </div>

                    <h4>CCTV</h4>
                <div  class="para_borders dflexsnt bp0">
                    <p>What personal information we will use</p>
                    <ul>
                        <li>Your image captured on the CCTV cameras</li>
                    </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>What purposes we will use the personal information for</p>
                    <ul>
                        <li>to prevent crime and protect buildings and assets from damage, disruption, vandalism and other crime;</li>
                        <li>for the personal safety of staff, visitors and other members of the public, and to act as a deterrent against crime;</li>
                        <li>assisting in the maintenance of public order;</li>
                        <li>to support law enforcement bodies in the prevention, detection, investigation and prosecution of crime;</li>
                        <li>to provide assistance with civil claims;</li>
                        <li>to assist in day-to-day management, including ensuring the health and safety of staff and others.</li>
                    </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>The legal basis for processing we rely upon</p>
                    <ul>
                        <li>we will rely on our legitimate interest in protecting our buildings, assets, staff and visitors and preventing crime.</li>
                        <li>To the extent that the CCTV images include special category personal data we will rely on the substantial public interest in preventing and detecting unlawful acts.</li>
                    </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>How long we retain the personal information and why</p>
                    <ul>
                        <li>Data recorded by the CCTV system will be digitally recorded and stored securely on hard drives. CCTV images will be deleted after 90 days.</li>
                        <li>Where an incident has occurred or where footage has been requested by the police, prosecution agencies, legal representatives or people whose images have been recorded and retained the usual retention period may be overridden and the footage will be retained until the matter is resolved.</li>
                    </ul>
                </div>
                <div  class="para_borders dflexsnt ">
                    <p>Who we share the personal information with</p>
                    <ul>
                        <li>the police;</li>
                        <li>prosecution agencies;</li>
                        <li>relevant legal representatives;</li>
                        <li>people whose images have been recorded and retained.</li>
                        <li>Only when our Data Privacy Manager deems it appropriate to do so having considered all relevant circumstances</li>
                    </ul>
                </div>
                    <h3>5. Individuals who engage with us on social media</h3>
                    <p>Any social media posts or comments you send to us (on our Facebook page, for instance) will be shared under the terms of the relevant social media platform (e.g. Facebook or Twitter) on which they’re written and could be made public. Other people, not us, control these platforms. We are not responsible for this kind of sharing. So, before you make any remarks or observations about anything, you should review the terms and conditions and privacy policies of the social media platforms you use. That way, you’ll understand how they will use your information, what information relating to you they will place in the public domain, and how you can stop them from doing so if you’re unhappy about it.</p>
                    <h3>6. When will we use your personal information for Direct marketing?</h3>
                    <p>In addition to data protection law, if we use your personal information for direct marketing purposes, we may also be subject to additional rules that regulate direct marketing. The term “direct marketing” essentially means directing marketing material or advertising at a particular individual.</p>
                    <p>To ensure compliance with both data protection laws and the specific rules relating to direct marketing, we will only use your personal information to tell you about our latest offers or to inform you of products and services which we think may be of interest to you in the circumstances outlined below:</p>

                <div  class="para_borders dflexsnt bp0">
                    <p>Direct marketing by telephone, post, email, text or other forms of electronic communication for potential buyers or tenants</p>
                    <ul>
                        <li>We will only contact you in these ways if you ask us to do so.</li>
                    </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>The legal grounds we rely upon</p>
                    <ul>
                        <li>Our legal bases for such processing under data protection law will either be your consent or legitimate interests. We consider it is in your legitimate interests to receive information about properties of a type you have expressed an interest in, and it is in our legitimate interests to provide you with this information and develop our business.</li>
                    </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>How long we retain the personal information and why</p>
                    <ul>
                        <li>We will retain your personal information unless and until you inform us that you no longer wish to receive direct marketing information from us. You can ask us to stop sending direct marketing to you at any time by contacting us using the details set out here [include link to contact details] or going to the “unsubscribe” section of our website. There will be an unsubscribe option in every electronic marketing message we send to you.</li>
                    </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>Consequences of not providing/permitting us to obtain personal information</p>
                    <ul>
                        <li>Without your contact details, we will not be able to provide you with updates you wish to receive;</li>
                        <li>You can opt-out of receiving related information at the time you subscribe to updates and each subsequent time we contact you.</li>
                    </ul>
                </div>
                <div  class="para_borders dflexsnt ">
                    <p>Direct marketing by post for potential sellers</p>
                    <ul>
                        <li>Where a property is advertised for sale with another estate agent we will sometimes write to the homeowner(s) to inform them of our services and to enquire whether they are interested in our services;</li>
                        <li>In such circumstances, we will rely on our legitimate interests in developing our business and the homeowner’s legitimate interests in achieving a quick sale of their property, for processing the personal data.</li>
                    </ul>
                </div>

                    <h3>7. Information about how and when we use automated decision making</h3>
                    <p>Automated decision making takes place when an electronic system uses personal information to make a decision without human intervention.</p>
                    <p>We do not make decisions about you using automated means. However, some of the services provided by third parties we use may involve an automated decision and/or credit scoring to determine whether we are able to provide a service or product. </p>
                    <p>Using third parties to undertake searches and collate information on our behalf helps make fair and responsible decisions.</p>

                    <h3>8. When will we share your personal information with others?</h3>
                    <p>Sometimes, we will need to share your personal information with others. This section sets out details of who we will share your personal information with and why. It also tells you about our legal basis for doing so under data protection law and steps we will take to protect your personal information.</p>
                    <p>We will never sell your personal information to third parties.</p>
                    <p>Landlord/Tenant and Vendor/Purchaser situations</p>
                    <p>We have already explained in the specific sections relating to our customers that there will be occasions where their names, addresses and contact details need to be shared, for example:</p>
                    <ul>
                        <li>tenant personal data will need to be shared with the landlord of a property, particularly where we are not managing the property on the landlord’s behalf; and</li>
                        <li>in any sale/purchase of property, the name and address of the parties will be included in the memorandum of sale, which will be provided to each party and their respective solicitors.</li>
                        

                    </ul>
                    <p>The legal basis for sharing this information is that it is in the legitimate interests of all parties in such circumstances, to have each other’s contact details to facilitate the management of a rental property or the sale/purchase of a property.</p>
                    <p>Set out below are other situations where we will need to share personal information.</p>

                    <h3>Our service partners</h3>

                <div  class="para_borders dflexsnt bp0">
                    <p>Who are our service partners?</p>
                    <ul>
                        <li>Our service partners include:
                            <ul class="disc_vli">
                                <li>Contractors who carry out repairs on rental properties;</li>
                                <li>Contractors we engage to provide Energy Performance Certificates, gas safety certificates etc;</li>
                                <li>Utility companies, to notify changes of occupiers of properties;</li>
                                <li>Storage companies, such as the Maltings, to store archived files;</li>
                                <li>Credit references agencies, such as CreditSafe, who carry out credit referencing on our behalf;</li>
                                <li>Referencing Agents, such as Goodlord.</li>
                            </ul>
                        </li>
                        <li>We haven’t included the names of our service partners in this privacy notice because their identity will change from time to time. However, if you would like further information about any of our current service providers, please contact us using the details set out here [include link to contact details].</li>
                    </ul>

                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>Why we need to share your personal information with them and our legal bases for doing so</p>
                    <ul>
                        <li>We use the contractors described above to enable us to perform our contracts with you.</li>
                        <li>We share personal information with utility companies, as it is in the legitimate interests of our customers to have a seamless supply of services to properties they occupy and to ensure that they are billed accurately.</li>
                        <li>We use storage companies to securely store archived files, as it is in our legitimate interests and those of our customers to ensure archived files are securely stored.</li>
                        <li>We use credit reference agencies and agents, as it is in our legitimate interests and the legitimate interests of our Landlord customers to ensure that we provide suitable tenants for rental properties.</li>
                    </ul>
                </div>
                <div  class="para_borders dflexsnt ">
                    <p>What precautions do we take?</p>
                    <ul>
                        <li>We enter into contracts with our service providers, which require them to put appropriate security measures in place and which restrict their use of your personal information.</li>
                    </ul>
                </div>
                    <h3>Our marketing partners</h3>
                <div  class="para_borders dflexsnt bp0">
                    <p>Who are our marketing partners</p>
                    <ul>
                        <li>Our marketing partners are marketing agencies that we use to create and/or deliver advertising and other promotional material on our behalf.</li>
                        <li>We haven’t included the names of our marketing partners in this privacy notice because their identity will change from time to time. However, if you would like further information about any of our current marketing partners, please contact us using the details set out here [include link to contact details].</li>
                    </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>Why we need to share your personal information with them</p>
                    <ul>
                        <li>We may need to share your personal information with our marketing partners if we ask them to create marketing materials addressed to you, or to contact you with direct marketing on our behalf.</li>
                    </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>The legal bases we rely upon when sharing your personal information</p>
                    <ul>
                        <li>We will either be relying on your express consent or our legitimate interests in developing and expanding our business.</li>
                    </ul>
                </div>
                <div  class="para_borders dflexsnt ">
                    <p>What precautions do we take?</p>
                    <ul>
                        <li>We enter into contracts with our marketing partners providers which require them to put appropriate security measures in place and which restrict their use of your personal information.</li>
                    </ul>
                </div>
                    <h4>Providers of information technology services</h4>
                <div  class="para_borders dflexsnt bp0">
                    <p>Who will we be sharing your personal information with?</p>
                    <ul>
                        <li>Suppliers of information technology products and services, such as our CRM system, cloud storage, web analytics etc.</li>
                        <li>We haven’t included the names of our IT providers in this privacy notice because their identity will change from time to time. However, if you would like further information about any of our current IT providers, please contact us using the details set out here [include link to contact details].
                        </li>
                    </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>Why we need to share your personal information with such providers</p>
                    <ul>
                        <li>We use suppliers of information technology products and services in connection with the supply, maintenance and/or improvement of our IT network and the creation, development, hosting and maintenance of our website;</li>
                        <li>We use analytics and search engine providers to assist us to improve our website;</li>
                        <li>We use a CRM system to help us manage our customers effectively and provide them with an efficient service.</li>
                    </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>The legal bases we rely upon when sharing your personal information</p>
                    <ul>
                        <li>We rely upon our legitimate interests in ensuring that our business can function properly and efficiently and that our IT network is secure;</li>
                        <li>The sharing of your personal data with analytics and search engine providers is based on our legitimate interests in having an efficient and user-friendly website;</li>
                        <li>The sharing of your personal data with our CRM system providers is based on our legitimate interests in managing our business efficiently and providing our customers with a tailored and effective service.</li>
                    </ul>
                </div>
                <div  class="para_borders dflexsnt ">
                    <p>What precautions do we take?</p>
                    <ul>
                        <li>We enter into contracts with our IT providers, which require them to put appropriate security measures in place and which restrict their use of your personal information.</li>
                    </ul>
                </div>
                    <h3>OTHER THIRD PARTIES</h3>
                    <p>We may also need to share your personal information with others in the following circumstances:</p>
                <div  class="para_borders dflexsnt bp0">
                    <p>If we sell, transfer or merge parts of our business or our assets</p>
                    <ul class="new_ra">
                        <li>
                            <p>As we continue to develop our business, we may choose to sell, transfer or merge parts of our business or our assets.  Alternatively, we may seek to acquire other businesses or merge with them. During any such process, we may need to disclose your personal information to other parties (such as potential purchasers or investors). Where we do so, we will be relying upon our legitimate business interests</p>
                            <p>However, we will only share your personal information in this way if the third parties in question agree to keep your personal information safe and private.</p>
                            <p>Also, if, for example, a merger happens, the purchaser will only be able to use your personal information in the ways set out in this privacy notice.</p>
                        </li>
                        
                    </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>Legal or regulatory requirements</p>
                    <ul>
                        <li>On occasion, we may be required to disclose your personal information to organisations such as the courts, or the police to comply with legal obligations we are subject to and/or to prevent fraud or crime.</li>
                    </ul>
                </div>
                <div  class="para_borders dflexsnt bp0">
                    <p>Protecting our business</p>
                    <ul>
                        <li>From time to time we may need to disclose your personal information in connection with steps we need to take to protect our business interests or property.</li>
                    </ul>
                </div>
                <div  class="para_borders dflexsnt ">
                    <p>Professional advice and legal action</p>
                    <ul>
                        <li>We may need to disclose your personal information to our professional advisers (for example, our lawyers and accountants) in connection with the provision by them of professional advice and/or the establishment or defence of legal claims.</li>
                    </ul>

                </div>
                    <p class="mt-2">Circumstances in which we will send your personal information outside the Indian Economic Area</p>
                    <p>This document is published in accordance with the provisions of the Information Technology Act, 2000 and the Company is compliant with the laws of India. If any users use or access the Website from outside India, they do so at their own risk and are responsible for compliance with the laws of such jurisdiction and the Company shall not have any responsibility for or liability arising from, such compliance with local laws. These Privacy Policy does not constitute, nor may it be used for or in connection with, any promotional activities or solicitation by anyone in any jurisdiction in which such promotional activities or solicitation is not authorized or to any person to whom it is unlawful to promote or solicit.</p>

                    <div  class="para_borders">
                    <table>
                        
                        <tr>
                        <th>Your rights</th>
                        <th>What this involves</th>
                        <th>What our obligations are</th>
                        </tr>
                        <tr>
                            <td><p>A right of access</p></td>
                            <td><p>This is a right to obtain access to your personal data and various supplementary information.</p></td>
                            <td><ul>
                                <li>We must provide you with a copy of your personal information and the other supplementary information without undue delay, and in any event within 1 month of receipt of your request;</li>
                                <li>We cannot charge you for the provision of this data and information, save in specific circumstances (such as where you request further copies of your personal information).</li>
                            </ul></td>

                        </tr>
                       <tr>
                           <td>A right to have personal data rectified</td>
                           <td><ul><li>This is a right to have your personal information rectified, if it is inaccurate or incomplete.</li></ul></td>
                           <td><ul>
                            <li>We must rectify any inaccurate or incomplete information without undue delay, and in any event within 1 month of receipt of your request;</li>
                            <li>If we have disclosed your personal information to others, we must, subject to certain exceptions, contact the recipients to inform them, that your personal information requires rectification.</li>
                            </ul></td>
                       </tr>
                       <tr>
                           <td>A right to erasure</td>
                           <td><ul>
                               <li>This is a right to have your personal information deleted or removed;</li>
                               <li>This right only applies in certain circumstances (such as where we no longer need the personal information for the purposes for which it was collected);</li>
                               <li>We have the right to refuse to delete or remove your personal data in certain circumstances.</li>
                           </ul></td>
                           <td>
                               <ul>
                                   <li>If this right applies, we must delete or remove your personal information without undue delay, and in any event within 1 month of receipt of your request;</li>
                                   <li>If we have disclosed your personal information to others, we must, subject to certain exceptions, contact the recipients to inform them that your personal information must be erased.</li>
                               </ul>
                           </td>
                       </tr>
                       <tr>
                           <td>A right to data portability</td>
                           <td>
                               <ul>
                                   <li>
                                       This is a right to obtain and re-use your personal information for your own purposes;
                                   </li>
                                   <li>It includes a right to ask that your personal information is transferred to another organisation (where technically feasible);</li>
                                   <li>This right only applies in certain limited circumstances</li>
                               </ul>
                           </td>
                           <td>
                               <ul>
                                   <li>
                                       If this right applies, we must provide your personal information to you in a structured, commonly used and machine reasonable form;
                                   </li>
                                   <li>Again, we must act without undue delay, and in any event within 1 month of receipt of your request;</li>
                                   <li>We cannot charge you for this service.</li>
                               </ul>
                           </td>
                       </tr>
                        <tr>
                            <td>A right to object</td>
                            <td>
                                <ul>
                                    <li>This is a right to object to the use of your personal information;</li>
                                    <li>The right applies in certain specific circumstances only;</li>
                                    <li>You can use this right to challenge our use of your personal information based on our legitimate interests;</li>
                                    <li>You can also use this right to object to use of your personal information for direct marketing.</li>
                                </ul>
                            </td>
                            <td>
                                <ul>
                                    <li>If you object to us using your personal information for direct marketing, we must stop using your personal information in this way as soon as we receive your request.</li>
                                    <li>If you object to other uses of your personal information, whether we have to stop using your personal information will depend on the particular circumstances.</li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>A right to object to automated decision making</td>
                            <td><ul><li>
                                This is a right not to be subject to a decision which is made solely on the basis of automated processing of your personal information, where the decision in question will have a legal impact on you or a similarly significant effect.
                                
                            </li></ul></td>
                            <td><ul>
                                <li>
                                    Where such a decision is made, you must be informed of that fact as soon as reasonably practicable;
                                </li>
                                <li>You then have 21 days from receipt of the notification to request that the decision is reconsidered, or that a decision is made that is not based solely on automated processing;</li>
                                <li>Your request must  be complied with within 21 days.</li>
                            </ul></td>
                        </tr>
                        <tr>
                            <td>A right to restrict processing</td>
                            <td>
                                <ul>
                                    <li>
                                        This is a right to ‘block’ or suppress processing of your personal information.
                                    </li>
                                    <li>This right applies in various circumstances, including where you contest the accuracy of your information.</li>
                                </ul>
                            </td>
                            <td>
                                <ul>
                                    <li>
                                        If we are required to restrict our processing of your personal information, we will be able to store it but not otherwise use it.
                                    </li>
                                    <li>We may only retain enough information about you to ensure that the restriction is respected in future.</li>
                                    <li>If we have disclosed your personal information to others, we must, subject to certain exceptions, contact them to tell them about the restriction on use.</li>
                                </ul>
                            </td>
                        </tr>
                    </table>
                    </div>
                    <h3>How do we keep your personal information secure?</h3>
                    <p>We take various steps to protect your personal information while it is in our possession, including:</p>
                    <ul>
                        <li>Implementation of appropriate security measures to protect our IT infrastructure; and</li>
                        <li>Training for members of staff in relation to data protection.</li>
                    </ul>
                    <p>You can help us to protect your personal information by adhering to the following security measures:</p>
                    <ul>
                        <li>Do not confirm your bank or credit card details in an email. We will not ask for such personal information in this way, so any email claiming to be from us that does so is likely to be fake. Please ignore it;</li>
                        <li>Keep your passwords private;</li>
                        <li>When creating a password, do not use words such as your name, date of birth or other personal data; and</li>
                        <li>Change your password regularly.</li>

                    </ul>
                    <h3>What rights do you have under Data Protection Law?</h3>
                    <p>Under data protection law, you have a number of different rights relating to the use of your personal information. The table below contains a summary of those rights and our obligations.</p>
                    <p>If you wish to exercise any of your rights, you can make a request by contacting us using the details set out here [include link to contact details].</p>
                    <p>If you request the exercise of any of your rights, we are entitled to ask you to provide us with any information that may be necessary to confirm your identity.</p>

                    <h3>Your right to withdraw consent</h3>
                    <p>If you have given us your consent to use any of your personal information, you can withdraw your consent at any time. To do so, please contact us using the details set out here:</p>

                    <h3>How can you get in touch with us and who is our data privacy manager?</h3>
                    <p>You can get in touch with us in the following ways:</p>
                    <div  class="para_borders dflexsnt bp0">
                    <p>Postal address</p> <p class="wid65s">RiVirtual, limited C-13 Cyber Towers  HI-TECH City 
                        Hyderabad Telangana 500 081</p>
                    </div>
                    <div  class="para_borders dflexsnt bp0">
                    <p>Email address</p> <p class="wid65s">hello@rivirtual.com</p>
                </div>
                <div  class="para_borders dflexsnt ">
                    <p>Phone number</p> <p class="wid65s">+ 91 9353 05 9999</p>
                </div>

                    <p class="mt-3">We have appointed a Data Privacy Manager to oversee our compliance with data protection law and this privacy notice. Her details are as follows – Raj Varma, Director. If you have any questions about this privacy notice, how we handle your personal information or if you wish to make a complaint, please contact our Data Privacy Manager.</p>
                    <p>Right to complain to the information commissioner’s officer</p>
                    <p>If we are unable to deal with a complaint to your satisfaction, or if you are unhappy with the way we are using your personal data, you also have the right to make a complaint at any time to India's supervisory authority for data protection issues, the Information Commissioner’s Office.</p>
                    <h3>Changes to our privacy notice</h3>
                    <p>We may update this privacy notice from time to time. If we make any substantial updates, we will provide you with a new privacy notice. We may also notify you in other ways from time to time about the processing of your personal information.</p>
                  </div>

                </div>

            </div>

        </div>

    </div>

@endsection


@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection



@section('script')
@include('includes.script')
@include('includes.toaster')
<script>
    $(document).ready(function(){
        $('#rent_link').click(function(){
            $('#rent').click();
        })
        $('#buy_link').click(function(){
            $('#buy').click();
        })
    })
</script>
<script>
    function search_result_check(that) {
        var $this = that;
        var name = $($this).val();
        console.log($($this).val());
        $.ajax({
            type: "POST",
            url:"{{route('get.locality.available')}}",
            data: {
                'name': name,
                '_token':'{{@csrf_token()}}'
            },
            success: function(data) {
              if(data){
                console.log(data);
                $('#serch_result').show();
                $('#serch_result').fadeIn();
                $('#serch_result').last().html(data);

              }
              else
              {
                $('#serch_result').children().remove();
              }
            }
        });
    }
    function add_searchbar(that) {
        var $this = that;
        var name = $($this).text();
        console.log(name);
        $('#location').val(name);
        $($this).remove();
        $('#serch_result').children().remove();
        $('#serch_result').hide();
    }
</script>
@endsection

@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> RiVirtual | Contact Us </title>
@endsection




@section('header')
@include('includes.header')
@endsection


@section('content')
<div class="haeder-padding"></div>

    <!-----filter--------->



    <div class="about-section">

        <div class="container">

            <div class="row">

                <div class="col-lg-12">

                    <div class="breadcrumbs-inner">

                        <h1 class="page-title">Contact Us</h1>

                        <div class="breadcrumbs-list">

                            <ul>

                                <li><a href="index.html"><span class=""><i class="icofont-home"></i></span> Home</a></li>

                                <li>Contact Us</li>

                            </ul>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    

    <div class="contact-add">

        <div class="container">

            <div class="row">

                <div class="col-md-4 col-sm-6">

                    <div class="contact-address-item">

                        <img src="{{ asset('public/frontend/images/10.png') }}" alt="Icon Image">

                        <h3 class="">Email Address</h3>

                        <p>

                            <a href="mailto:hello@rivirtual.com"> hello@rivirtual.com </a><br>

                            <!-- <a href="mailto:example@rivirtual.com"> example@rivirtual.com </a> -->

                        </p>

                    </div>

                </div>

                <div class="col-md-4 col-sm-6">

                    <div class="contact-address-item">

                        <img src="{{ asset('public/frontend/images/11.png') }}" alt="Icon Image">

                        <h3 class="">Phone Number</h3>

                        <p>

                            <a href="tel:85091-79999"> 85091-79999  </a><br>

                            <!-- <a href="tel: +987-6543210"> +987-6543210 </a> -->

                        </p>

                    </div>

                </div>

                <div class="col-md-4 col-sm-6">

                    <div class="contact-address-item">

                        <img src="{{ asset('public/frontend/images/13.png') }}" alt="Icon Image">

                        <h3 class="">Office Address</h3>
                        <p>C-13 Cyber Towers <br>
                           HI-TECH City <br>
                           Hyderabad <br>
                           Telangana 500 081 India
                        </p>

                        

                    </div>

                </div>

            </div>

        </div>

    </div>


    <form action="{{route('save.contact.us')}}" method="POST" id="myForm">
        @csrf
    <div class="contact-from">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <div class="contact-form-box">

                        <h4 class="title-2">Contact Us</h4>

                        <div class="row">

                            <div class="col-sm-6">

                                <div class="input-item name-item">

                                    <input type="text" name="name" placeholder="Enter your name">

                                </div>

                            </div>

                            <div class="col-sm-6">

                                <div class="input-item mail-item">

                                    <input type="email" name="email" placeholder="Enter your email">

                                </div>

                            </div>

                            <div class="col-sm-6">

                                <div class="input-item tel-item">

                                    <input type="text" name="phone" placeholder="Enter phone number">

                                </div>

                            </div>

                            <div class="col-sm-6">

                                <div class="input-item ser-item">

                                    <select name="subject">

                                        <option value="">Select Subject</option>

                                        <option value="Buy House">Buy House</option>

                                        <option value="Rent House">Rent House</option>

                                        <option value="Agent Related">Agent Related</option>

                                    </select>

                                </div>

                            </div>

                            <div class="col-sm-12">

                                <div class="input-item text-item">

                                    <textarea name="message" placeholder="Enter message"></textarea>

                                </div>

                            </div>

                            <div class="col-sm-12">
                                <input type="submit" name="" value="Submit" class="theme-btn mt-1"> 
                                <!-- <a href="#" class="theme-btn mt-1">Submit</a> -->

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>
    </form>
@endsection


@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection



@section('script')
@include('includes.script')
@include('includes.toaster')
<script>
     $('#myForm').validate({
        rules:{
            name:{
                required:true,
            },
            email:{
                required:true,
                email:true,
            },
            phone:{
                required:true,
                digits:true,
                maxlength:10,
                minlength:10,
            },
            subject:{
                required:true,
            },
            message:{
                required:true
            }
        }

     });
</script>
<script>
    $(document).ready(function(){
        $('#rent_link').click(function(){
            $('#rent').click();
        })
        $('#buy_link').click(function(){
            $('#buy').click();
        })
    })
</script>
<script>
    function search_result_check(that) {
        var $this = that;
        var name = $($this).val();
        console.log($($this).val());
        $.ajax({
            type: "POST",
            url:"{{route('get.locality.available')}}",
            data: {
                'name': name,
                '_token':'{{@csrf_token()}}'
            },
            success: function(data) {
              if(data){
                console.log(data);
                $('#serch_result').show();
                $('#serch_result').fadeIn();
                $('#serch_result').last().html(data);

              }
              else
              {
                $('#serch_result').children().remove();
              }
            }
        });
    }
    function add_searchbar(that) {
        var $this = that;
        var name = $($this).text();
        console.log(name);
        $('#location').val(name);
        $($this).remove();
        $('#serch_result').children().remove();
        $('#serch_result').hide();
    }
</script>
@endsection

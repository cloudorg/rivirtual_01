@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
        margin-top: 10px;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
<link href="{{ URL::asset('public/frontend/croppie/croppie.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('public/frontend/croppie/croppie.min.css') }}" rel="stylesheet" />
@endsection

@section('title')
<title> RiVirtual | Razorpay</title>
@endsection




@section('header')
@include('includes.header')
@endsection


@section('content')
<div class="haeder-padding"></div>
    <div id="app">
        <main class="py-4">
            <div class="container">
                <div class="pay_pg">
                    <div class="rz_pay">
  
                        <!-- @if($message = Session::get('error'))
                            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <strong>Error!</strong> {{ $message }}
                            </div>
                            Session::flush();
                        @endif -->
  
                        <!-- @if($message = Session::get('success'))
                            <div class="alert alert-success alert-dismissible fade {{ Session::has('success') ? 'show' : 'in' }}" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <strong>Success!</strong> {{ $message }}
                            </div>
                           {{Session::forget('success')}} 
                        @endif -->
  
                        <div class="card card-default">
                            <div class="card-header">
                                <span>
                                    Subscription for - {{@$package->package_name}}
                                </span>
                            </div>
  
                            <div class="card-body">
                                <form action="{{ route('razorpay.payment.store') }}" method="POST" >
                                    @csrf
                                    
                                    
                                    <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                                    <input type="hidden" name="subscription_id" value="{{@$package->id}}">
                                    <input type="hidden" name="page_type" value="S">
                                    <input type="hidden" name="mem_type" value="{{@$mem_type}}">
                                    <input type="hidden" name="amount" value="{{@$amt1}}">
                                    <input type="hidden" name="payment_id" value="{{@$payment_id}}">
                                    
                                    <script src="https://checkout.razorpay.com/v1/checkout.js"
                                            data-key="{{ env('RAZORPAY_KEY') }}"
                                            data-amount="{{@$amt1*100}}"
                                            data-buttontext="Pay {{@$amt1}} INR"
                                            data-name="Rivirtual.net"
                                            data-description="{{@$package->package_name}}"
                                            data-image="{{asset('public/frontend/images/logo.png')}}"
                                            data-prefill.name="name"
                                            data-prefill.email="email"
                                            data-theme.color="#338206">
                                    </script>
                                </form>
                            </div>
                        </div>
  
                    </div>
                </div>
            </div>
        </main>
    </div>





@endsection


@section('footer')
@include('includes.footer')
@endsection



@section('script')
@include('includes.script')
 {{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRZMuXnvy3FntdZUehn0IHLpjQm55Tz1E&libraries=places&callback=initAutocomplete" async defer></script>
<script>
    function initAutocomplete() {
        // Create the search box and link it to the UI element.
        var input = document.getElementById('address');

        var options = {
          types: ['establishment']
        };

        var input = document.getElementById('address');
        var autocomplete = new google.maps.places.Autocomplete(input, options);

        autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);

        autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();
            console.log(place)
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            $('#lat').val(place.geometry.location.lat());
            $('#long').val(place.geometry.location.lng());
            lat = place.geometry.location.lat();
            lng = place.geometry.location.lng();
            $('.exct_btn').show();

            initMap();
        });
        initMap();
    }
</script>

<script>

    function initMap() {
        geocoder = new google.maps.Geocoder();
        var lat = $('#lat').val();
        var lng = $('#long').val();
        var myLatLng = new google.maps.LatLng(lat, lng);
        // console.log(myLatLng);
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 16,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'Choose hotel location',
          draggable: true
        });

        google.maps.event.addListener(marker, 'dragend', function(evt,status){
        $('#lat').val(evt.latLng.lat());
        $('#long').val(evt.latLng.lng());
        var lat_1 = evt.latLng.lat();
        var lng_1 = evt.latLng.lng();
        var latlng = new google.maps.LatLng(lat_1, lng_1);
            geocoder.geocode({'latLng': latlng}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    $('#address').val(results[0].formatted_address);
                }
            });


        });
    }
    </script>--}} 
    {{-- <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC1A0Zjdpb5eWY6MCTp_8ZOVAlDkUB4MTY&callback=initMap">
    </script> --}}

<script>
    $(document).ready(function(){
        jQuery.validator.addMethod("validate_name", function(value, element) {
            if (/^([a-zA-Z0-9 ])+$/.test(value)) {
                 return true;
            } else {
                 return false;
            }
        }, "Allow only (a-z, A-Z, 0-9, )");
        jQuery.validator.addMethod("validate_address", function(value, element) {
            if (/^([a-zA-Z0-9./_ ,-])+$/.test(value)) {
                 return true;
            } else {
                 return false;
            }
        }, "Allow only (a-z, A-Z, 0-9, ./_ ,-)");
        jQuery.validator.addMethod("validate_location", function(value, element) {
            if (/^([a-zA-Z0-9./_ ,-])+$/.test(value)) {
                 return true;
            } else {
                 return false;
            }
        }, "Allow only (a-z, A-Z, 0-9, ./_ ,-)");
        jQuery.validator.addMethod("validate_description", function(value, element) {
            if (/^([a-zA-Z0-9./'_ ,-])+$/.test(value)) {
                return true;
            } else {
                return false;
            }
        }, "Allow only (a-z, A-Z, 0-9, ./'_ ,-)");
        $.validator.addMethod('greaterThan', function (value, element, param) {
            return this.optional(element) || parseInt(value) >= parseInt($(param).val());
        }, 'Budget to not small budget from');
        jQuery.validator.addMethod("validate_area", function(value, element) {
            var testEmail =   /^[0-9]{0,5}.[0-9]{0,2}$/
            if (testEmail.test(value)) {
                return true;
            } else {
                return false;
            }
        }, "Area maximum 5 digits and 2 decimal places.E.g. 12345.67");
        jQuery.validator.addMethod("validate_super_area", function(value, element) {
            var testEmail =   /^[0-9]{0,5}.[0-9]{0,2}$/
            if (testEmail.test(value)) {
                return true;
            } else {
                return false;
            }
        }, "Super Area maximum  5 digits and 2 decimal places.E.g. 12345.67");
        jQuery.validator.addMethod("validate_carpet_area", function(value, element) {
            var testEmail =   /^([0-9]{0,5}).[0-9]{0,2}$/
            if (testEmail.test(value)) {
                return true;
            } else {
                return false;
            }
        }, "Carpet Area maximum  5 digits and 2 decimal places.E.g. 12345.67");
        
        $('#addPropertyForm').validate({
            rules: {

                property_type:{
                    required:true
                },
                property_for:{
                    required:true
                },
                property_name:{
                    required: true,
                    validate_name:true
                },
                no_of_bedrooms:{
                    // required: true,
                    digits: true ,
                    min:0,
                    max:99,
                },
                bathroom:{
                    // required: true,
                    digits: true ,
                    min:0,
                    max:99,
                },
                floor:{
                    // required: true,
                    digits: true ,
                    // min:0,
                    max:99,
                },
                post_code:{
                    required: true,
                    digits: true ,
                    minlength:6,
                },
                budget_range_from:{
                    // required: true,
                    digits: true ,
                    min:1,
                    // max:function(){
                    //     if($('#budget_range_to').val()!=''){
                    //         return $('#budget_range_to').val();
                    //     }
                    // },
                },
                
                maintenance_charge:{
                    digits: true ,
                },
                deposit_amount:{
                    digits: true ,
                },
                area:{
                    // required: true,
                    number: true ,

                    // validate_number: true,
                   // digits: true,

                    // validate_area:true,
                    // min:0,
                },
                super_area:{
                    // required: true,
                    number: true ,

                    // validate_number: true,

                   // digits: true,

                    // validate_super_area:true,
                    // min:0,
                },
                carpet_area:{
                    // required: true,
                    number: true ,

                    // validate_number: true,

                    //digits: true,

                    // validate_carpet_area:true,
                    // min:0,
                },land_area:{
                    number:true,
                },
                address:{
                    required: true,
                    validate_address:true
                },
                description:{
                    required: true,
                   // validate_description:true,
                },
                location:{
                    required: true,
                    validate_location:true,
                },
            },
            messages: {
                construction_status:{
                    required: 'Select property construction Status',
                },
                furnishing:{
                    required: 'Select property furnishing Status',
                },
                property_type:{
                    required: 'Select property Type',
                },
                property_for:{
                    required: 'Select property For',
                },
                property_name:{
                    required: 'Enter property name',
                },
                no_of_bedrooms:{
                    required: 'Enter number of bedrooms',
                    digits: 'Bedroom only number ',
                    min:'Minimum bedroom 0 ',
                    max:'Maximum bedroom 99',
                },
                bathroom:{
                    required: 'Enter number of bathrooms',
                    digits: 'Bathroom only number ',
                    min:'Minimum bathroom 0 ',
                    max:'Maximum bathroom 99',
                },
                floor:{
                    required: 'Enter number of floor',
                    digits: 'Floor only number ',
                    // min:'Minimum floor 0 ',
                    max:'Maximum floor 99',
                },
                post_code:{
                    required: 'Enter post code',
                    digits: 'post code only number ',
                    minlength:'Post code exactly 6 digits',
                },
                budget_range_from:{
                    required: 'Enter budget from ',
                    digits: 'Budget from only number ',
                    min: 'Budget from start from 1',
                    max: 'Budget from not gater budget to',
                },
                budget_range_to:{
                    required: 'Enter budget to',
                    digits: 'Budget to only number ',
                    min: 'Budget to not small budget from',
                },
                deposit_amount:{
                    required: 'Enter deposit amount',
                    digits: 'Deposit amount only number ',
                },
                maintenance_charge:{
                    required: 'Enter maintenance charge',
                    digits: 'Maintenance charge only number ',
                },
                area:{
                    required: 'Enter area ',
                    number: 'Area only number ',
                    min:'Minimum area 0 ',
                    
                },
                super_area:{
                    required: 'Enter super area ',
                    number: 'Super area number ',
                    min:'Minimum super area 0 ',
                },
                carpet_area:{
                    required: 'Enter carpet area ',
                    number: 'Carpet area only number ',
                    min:'Minimum carpet area 0 ',
                },
                country:{
                    required:'Select Country',
                },
                build_year:{
                    required:'Select build year',
                },
                state:{
                    required:'Enter state',
                },
                city:{
                    required:'Enter city',
                },
                address:{
                    required:'Enter address',
                },
                description:{
                    required:'Enter property description',
                },
                locality:{
                    required:'Enter locality',
                },
            },
            ignore: [],
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
    //   $('#country').on('change',function(e){
    //     e.preventDefault();
    //     var id = $(this).val();

    //     $.ajax({
    //       url:'{{route('get.state')}}',
    //       type:'GET',
    //       data:{country:id,id:'{{auth()->user()->state}}'},
    //       success:function(data){
    //         console.log(data);
    //         $('#states').html(data.state);
    //       }
    //     })
    //   });

      $('#states').on('change',function(e){
        e.preventDefault();
        var id = $(this).val();
        $.ajax({
          url:'{{route('get.city')}}',
          type:'GET',
          data:{state:id,id:'{{auth()->user()->state}}'},
          success:function(data){
            console.log(data);
            $('#city').html(data.city);
          }
        })
      });
      $('#city').on('change',function(e){
        $('#locality-error').text('');
        $('#locality-error').css('display','none');
      })


    });
  </script>
  <script>
    function search_result_check(that) {
        var city =$('#city').val();
        if(city==null||city==''){
            $('#locality-error').text('Please select state & city first');
            $('#locality-error').css('display','block');
            $('#locality').val('');
        }else{
            $('#locality-error').text('');
            $('#locality-error').css('display','none');
            var $this = that;
            var name = $($this).val();
            console.log($($this).val());
        $.ajax({
            type: "POST",
            url:"{{route('get.locality')}}",
            data: {
                'name': name,
                '_token':'{{@csrf_token()}}'
            },
            success: function(data) {
              if(data){
                console.log(data);
                $('#serch_result').show();
                $('#serch_result').fadeIn();
                $('#serch_result').last().html(data);

              }
              else
              {
                $('#serch_result').children().remove();
              }
            }
        });
        }

    }
    function add_searchbar(that) {
        var $this = that;
        var name = $($this).text();
        console.log(name);
        $('#locality').val(name);
        $($this).remove();
        $('#serch_result').children().remove();
        $('#serch_result').hide();
    }
</script>
<script>
    $('.property_type:radio').on('change', function() {
        var property_type=$(this).val();
        $('#prop_type').val(property_type);
        var prop_type = $('#prop_type').val();
        var prop_for = $('#prop_for').val();

        if(prop_type == 'L' && prop_for == 'R'){
            $('.new-remove-office-class').hide();
            $('.new-office-class').hide();
            $('#office_class').removeClass('required');
            $('#no_of_bedrooms').removeClass('required');
            $('#bathroom').removeClass('required');
            $('.property_type_land').hide();
            $('input[name=furnishing]').removeClass('required');
            $('input[name=construction_status]').removeClass('required');
            $('input[name=bathroom]').removeClass('required');
            $('input[name=floor]').removeClass('required');
            $('.new-rent-class').slideUp();
            $('#rent_payment').removeClass('required');
            $('#preference').removeClass('required');
            $('#available_from').removeClass('required');
            $('#deposit_amount').removeClass('required');
            $('#maintenance_charge').removeClass('required');
            $('.area-class').slideUp();
            $('#super_area').removeClass('required');
            $('#carpet_area').removeClass('required');
            $('.build-class').slideUp();
            $('#build_year').removeClass('required');
            $('.land-area-class').slideDown();
            $('#land_area').addClass('required');
            $('#land_area_unit').addClass('required');
            $('.t-area-class').slideUp();
            $('#area').removeClass('required');
            // $('#budget_range_from').removeClass('required');
            // $('#budget_range_from').slideUp();
            // $('.price_tab').hide();
        }else if(prop_type == 'O' && prop_for == 'R'){
            $('.new-remove-office-class').hide();
            $('.new-office-class').slideDown();
            $('#office_class').addClass('required');
            $('.new-rent-class').slideUp();
            $('#rent_payment').removeClass('required');
            $('#preference').removeClass('required');
            $('#available_from').removeClass('required');
            $('#deposit_amount').removeClass('required');
            $('#maintenance_charge').removeClass('required');
            $('.build-class').slideDown();
            $('#build_year').addClass('required');
            $('.land-area-class').slideUp();
            $('#land_area').removeClass('required');
            $('#land_area_unit').removeClass('required');
            $('.t-area-class').slideDown();
            $('#area').addClass('required');
            $('#area').attr('maxlength','10');
            // $('#budget_range_from').addClass('required');
            // $('#budget_range_from').slideDown();
            // $('.price_tab').show();
            $('#no_of_bedrooms').removeClass('required');
        }else if((prop_type == 'H' && prop_for == 'R') || (prop_type == 'F' && prop_for == 'R') || (prop_type == 'R' && prop_for == 'R')){
            $('.new-rent-class').slideDown();
            $('#rent_payment').addClass('required');
            $('#preference').addClass('required');
            $('#available_from').addClass('required');
            $('#deposit_amount').addClass('required');
            $('#maintenance_charge').addClass('required');
            $('.area-class').slideDown();
            $('#super_area').addClass('required');
            $('#carpet_area').addClass('required'); 
            $('.new-remove-office-class').show();
            $('.new-remove-office-class').slideDown();
            $('.property_type_land').slideDown();
            $('#no_of_bedrooms').addClass('required');
            $('.new-office-class').slideUp();
            $('#office_class').removeClass('required');
            $('#bathroom').addClass('required');
            $('.build-class').slideDown();
            $('#build_year').addClass('required');
            $('.land-area-class').slideUp();
            $('#land_area').removeClass('required');
            $('#land_area_unit').removeClass('required');
            $('.t-area-class').slideDown();
            $('#area').addClass('required');
            $('#area').attr('maxlength','10');
            // $('#budget_range_from').removeClass('required');
            // $('#budget_range_from').slideUp();
            // $('.price_tab').hide();

        }

        else if(property_type=='O'){
            $('.new-remove-office-class').hide();
            $('.new-office-class').slideDown();
            $('#office_class').addClass('required');
            $('.property_type_land').slideDown();
            $('#no_of_bedrooms').removeClass('required');
            // $('#bathroom').removeClass('required');
            $('#bathroom').addClass('required');
            $('input[name=furnishing]').addClass('required');
            $('input[name=construction_status]').addClass('required');
            $('input[name=floor]').addClass('required');
            $('.new-rent-class').slideUp();
            $('#rent_payment').removeClass('required');
            $('#preference').removeClass('required');
            $('#available_from').removeClass('required');
            $('#deposit_amount').removeClass('required');
            $('#maintenance_charge').removeClass('required');
            $('.build-class').slideDown();
            $('#build_year').addClass('required');
            $('.land-area-class').slideUp();
            $('#land_area').removeClass('required');
            $('#land_area_unit').removeClass('required');
            $('.t-area-class').slideDown();
            $('#area').addClass('required');
            $('#area').attr('maxlength','10');

            $('#budget_range_from').addClass('required');
            $('#budget_range_from').slideDown();
            $('.price_tab').show();

        }else if(property_type=='L')
        {
            $('.new-remove-office-class').hide();
            $('.new-office-class').hide();
            $('#office_class').removeClass('required');
            $('#no_of_bedrooms').removeClass('required');
            $('#bathroom').removeClass('required');
            $('.property_type_land').hide();
            $('input[name=furnishing]').removeClass('required');
            $('input[name=construction_status]').removeClass('required');
            $('input[name=bathroom]').removeClass('required');
            $('input[name=floor]').removeClass('required');
            $('.new-rent-class').slideUp();
            $('#rent_payment').removeClass('required');
            $('#preference').removeClass('required');
            $('#available_from').removeClass('required');
            $('#deposit_amount').removeClass('required');
            $('#maintenance_charge').removeClass('required');
            $('.area-class').slideUp();
            $('#super_area').removeClass('required');
            $('#carpet_area').removeClass('required');
            $('.build-class').slideUp();
            $('#build_year').removeClass('required');
            $('.land-area-class').slideDown();
            $('#land_area').addClass('required');
            $('#land_area_unit').addClass('required');
            $('.t-area-class').slideUp();
            $('#area').removeClass('required');
            $('#area').attr('maxlength','100');

            $('#budget_range_from').addClass('required');
            $('#budget_range_from').slideDown();
            $('.price_tab').show();
            

        }
        else{
            $('.new-office-class').hide();
            $('.new-remove-office-class').slideDown();
            $('.property_type_land').slideDown();
            $('#office_class').removeClass('required');
            $('#no_of_bedrooms').addClass('required');
            $('#bathroom').addClass('required');
            $('input[name=furnishing]').addClass('required');
            $('input[name=construction_status]').addClass('required');
            $('input[name=floor]').addClass('required');
            $('.area-class').slideDown();
            $('#super_area').addClass('required');
            $('#carpet_area').addClass('required');
            $('.build-class').slideDown();
            $('#build_year').addClass('required');
            $('.land-area-class').slideUp();
            $('#land_area').removeClass('required');
            $('#land_area_unit').removeClass('required');
            $('.t-area-class').slideDown();
            $('#area').addClass('required');
            $('#area').attr('maxlength','10');
            $('#budget_range_from').addClass('required');
            $('#budget_range_from').slideDown();
            $('.price_tab').show();
            
        }
    });
    $('.property_for:radio').on('change', function() {
        var property_for=$(this).val();
        $('#prop_for').val(property_for);
        var prop_type = $('#prop_type').val();
        var prop_for = $('#prop_for').val();
        
        
        if(prop_type == 'L' && prop_for == 'R'){
            $('.new-rent-class').slideUp();
            $('#rent_payment').removeClass('required');
            $('#preference').removeClass('required');
            $('#available_from').removeClass('required');
            $('#deposit_amount').removeClass('required');
            $('#maintenance_charge').removeClass('required');
            $('.build-class').slideUp();
            $('#build_year').removeClass('required');
            $('.land-area-class').slideDown();
            $('#land_area').addClass('required');
            $('#land_area_unit').addClass('required');
            $('.t-area-class').slideUp();
            $('#area').removeClass('required');
            // $('#budget_range_from').removeClass('required');
            // $('#budget_range_from').slideUp();
            // $('.price_tab').hide();

        }else if(prop_type == 'O' && prop_for == 'R'){
            $('.new-rent-class').slideUp();
            $('#rent_payment').removeClass('required');
            $('#preference').removeClass('required');
            $('#available_from').removeClass('required');
            $('#deposit_amount').removeClass('required');
            $('#maintenance_charge').removeClass('required');
            $('.build-class').slideDown();
            $('#build_year').addClass('required');
            $('.land-area-class').slideUp();
            $('#land_area').removeClass('required');
            $('#land_area_unit').removeClass('required');
            $('.t-area-class').slideDown();
            $('#area').addClass('required');
            $('#area').attr('maxlength','10');
            $('#office_class').addClass('required');
            // $('#budget_range_from').addClass('required');
            // $('#budget_range_from').slideDown();
            // $('.price_tab').show();
            $('#no_of_bedrooms').removeClass('required');
        }

        else if(property_for=='R'){
            $('.new-rent-class').slideDown();
            $('#rent_payment').addClass('required');
            $('#preference').addClass('required');
            $('#available_from').addClass('required');
            $('#deposit_amount').addClass('required');
            $('#maintenance_charge').addClass('required');
            $('#no_of_bedrooms').addClass('required');
            $('#area').attr('maxlength','10');
            // $('#budget_range_from').removeClass('required');
            // $('#budget_range_from').slideUp();
            // $('.price_tab').hide();

        }else{
            $('.new-rent-class').slideUp();
            $('#rent_payment').removeClass('required');
            $('#preference').removeClass('required');
            $('#available_from').removeClass('required');
            $('#deposit_amount').removeClass('required');
            $('#maintenance_charge').removeClass('required');
            $('#budget_range_from').addClass('required');
            $('#budget_range_from').slideDown();
            $('.price_tab').show();
        }
    });
    function addCommas(numberString) {
        numberString += '';
  var x = numberString.split('.'),
      x1 = x[0],
      x2 = x.length > 1 ? '.' + x[1] : '',
      rgxp = /(\d+)(\d{3})/;

  while (rgxp.test(x1)) {
    x1 = x1.replace(rgxp, '$1' + ',' + '$2');
  }

  return x1 + x2;
}

function divider1(number_of_digits) {
    var tens="1";
    if(number_of_digits>8)
    return 10000000;
    while((number_of_digits-1)>0)
    {
        tens+="0";
        number_of_digits--;
    }
    return tens;
}

    $('#budget_range_from').keyup(function(){
        var from_value=$(this).val();
        var from_length=from_value.length;
        if(from_length<4){
            $('#budget_range_from_p').html('<span>₹</span>'+ from_value)
            console.log(from_value);
        }

        if(from_length==4||from_length==5){
            if(from_value%1000!=0){
                var fraction=from_value/1000;
                $('#budget_range_from_p').html('<span>₹</span>'+ fraction.toFixed(2)+'k')
                console.log(fraction.toFixed(2)+'k');
            }else{
                var fraction=from_value/1000;
                $('#budget_range_from_p').html('<span>₹</span>'+ fraction+'k')
                console.log(fraction+'k');
            }
        }
        if(from_length==6||from_length==7){
            if(from_value%100000!=0){
                var fraction=from_value/100000;
                $('#budget_range_from_p').html('<span>₹</span>'+ fraction.toFixed(2)+'Lakhs')
                console.log(fraction.toFixed(2)+'Lakhs');
            }else{
                var fraction=from_value/100000;
                $('#budget_range_from_p').html('<span>₹</span>'+ fraction.toFixed(2)+'Lakhs')
                console.log(fraction+'Lakhs');
            }
        }
        if(from_length==8||from_length==9||from_length==10){

            if(from_value%10000000!=0){
                var fraction=from_value/10000000;
                $('#budget_range_from_p').html('<span>₹</span>'+ fraction.toFixed(2)+'Cr')
                console.log(fraction.toFixed(2)+'Cr');
            }else{
                var fraction=from_value/10000000;
                $('#budget_range_from_p').html('<span>₹</span>'+ fraction+'Cr')
                console.log(fraction+'Cr');
            }
        }

    })
    // $('#budget_range_to').keyup(function(){
    //     var to_value=$(this).val();
    //     var to_length=to_value.length;
    //     if(to_length<4){
    //         $('#budget_range_to_p').html('<span>₹</span>'+ to_value)
    //         console.log(to_value);
    //     }

    //     if(to_length==4||to_length==5){
    //         if(to_value%1000!=0){
    //             var fraction=to_value/1000;
    //             $('#budget_range_to_p').html('<span>₹</span>'+ fraction.toFixed(2)+'k')
    //             console.log(fraction.toFixed(2)+'k');
    //         }else{
    //             var fraction=to_value/1000;
    //             $('#budget_range_to_p').html('<span>₹</span>'+ fraction+'k')
    //             console.log(fraction+'k');
    //         }
    //     }
    //     if(to_length==6||to_length==7){
    //         if(to_value%100000!=0){
    //             var fraction=to_value/100000;
    //             $('#budget_range_to_p').html('<span>₹</span>'+ fraction.toFixed(2)+'Lakhs')
    //             console.log(fraction.toFixed(2)+'Lakhs');
    //         }else{
    //             var fraction=to_value/100000;
    //             $('#budget_range_to_p').html('<span>₹</span>'+ fraction.toFixed(2)+'Lakhs')
    //             console.log(fraction+'Lakhs');
    //         }
    //     }
    //     if(to_length==8||to_length==9||to_length==10){

    //         if(to_value%10000000!=0){
    //             var fraction=to_value/10000000;
    //             $('#budget_range_to_p').html('<span>₹</span>'+ fraction.toFixed(2)+'Cr')
    //             console.log(fraction.toFixed(2)+'Cr');
    //         }else{
    //             var fraction=to_value/10000000;
    //             $('#budget_range_to_p').html('<span>₹</span>'+ fraction+'Cr')
    //             console.log(fraction+'Cr');
    //         }
    //     }

    // })
    $('#available_from').datepicker({
        minDate: new Date(),
    });
</script>
<script>
        function ytVidId(url) {
        var p = /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
        return (url.match(p)) ? RegExp.$1 : false;
    }

    $('#video_link').bind("change", function() {

        var url = $(this).val();
        if(url != ''){

            if (ytVidId(url) !== false) {
            $('.intro_video_error').html('');
            } else {
            $(".intro_video_error").html('Youtube Link invalid');
            $('#video_link').val('');
            }

        }
        
    });
</script>
@endsection

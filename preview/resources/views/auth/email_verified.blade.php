@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> RiVirtual | Home </title>
@endsection




@section('header')
@include('includes.header')
@endsection


@section('content')
<!----header--->
<div class="haeder-padding"></div>

<!----service--->
<section class="service-section">
	<div class="container">
		<div class="row">
			<div class="mesg-cls success-cls">
                <span class="img-span"><img width="100" src="{{asset('public/frontend/images/success.png')}}" alt=""></span>
                <h2 class="thankyou">Congratulations !!</h2>
                <p>You have successfully verified your email address.</p>
                <div class="sign-up-box">
                    <a href="javascript:;" class="btn btn-primary btn-lg opensignin">Go to Login</a>
                </div>
            </div>
		</div>
	</div>
</section>


@endsection


@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection



@section('script')
@include('includes.script')

@endsection

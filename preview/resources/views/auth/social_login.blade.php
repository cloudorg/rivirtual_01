@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> RiVirtual | Social Registration</title>
@endsection




@section('header')
@include('includes.header')
@endsection


@section('content')
<!----header--->
<div class="haeder-padding"></div>


<div class="property_manag_ban">
	<div class="container">
		<div class="property_manag_inn">
			<div class="property_manag_left">
				{{-- <h4>Rivirtual Manages Clients’ property and collect information using this box </h4>
				<ul>
					<li><em><i class="fa fa-check"></i></em>There will be timely and live updates provided to the owners on the website. </li>
					<li><em><i class="fa fa-check"></i></em>One may hire a property manager for all or any one of the services mentioned.</li>
					<li><em><i class="fa fa-check"></i></em>The property managers will also provide for lands and vacant properties. </li>
				</ul> --}}
			</div>
			<div class="property_manag_rig">
				<div class="property_manag_from">
					<h4> Complete Your Sign Up</h4>
					<p>Please fill up following information to complete the sign up process as @if(@$user_type=='agent') agent @elseif(@$user_type=='user') user @elseif(@$user_type=='service') pro @endif</p>
					<form action="{{route('login.social.register')}}" method="POST" id="social_from">
                        @csrf
                        <input type="hidden" value="{{@$user->email}}" name="social_user_email">
                        <input type="hidden" value="{{@$user->name}}" name="social_user_name">
                        <input type="hidden" value="{{@$user->id}}" name="social_user_id">
                        <input type="hidden" value="{{@$user_type}}" name="user_type">
                        <input type="hidden" value="{{@$provider_type}}" name="provider_type">
						{{-- <div class="das_input">
							<input type="text" placeholder="Name" value="">
						</div> --}}
						<div class="das_input">
							<input type="text" placeholder="Enter Phone number" class="required" name="phone" id="phone">
						</div>
						{{-- <div class="das_input">
							<input type="text" placeholder="Email" class="required">
						</div> --}}
						<div class="manage_sub">
							<input type="submit" value="Complete SignUp" class="see_cc">
						</div>
						{{-- <div class="manage_tx">
							<span>Call or WhatsApp us <a href="tel:918793456789">+918793456789 </a></span>
						</div> --}}
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection



@section('script')
@include('includes.script')
<script>
     $("#social_from").validate({
            rules: {
                phone:{
                    required: true,
                    number: true ,
                    minlength: 10,
                    maxlength: 10,
                    remote: {
                        url: '{{ route("check.mobile") }}',
                        dataType: 'json',
                        type:'post',
                        data: {
                            mobile: function() {
                                return $('#phone').val();
                            },
                            _token: '{{ csrf_token() }}'
                        }
                    },
                },
            },
            messages: {

                phone:{
                    remote:'Unique mobile number required'
                },

            }
        });
</script>
@endsection

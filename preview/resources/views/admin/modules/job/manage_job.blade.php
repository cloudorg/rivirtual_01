@extends('admin.layouts.app')

@section('title')
RiVirtual | Admin | Manage Jobs
@endsection

@section('content')

@section('links')

@include('admin.includes.links')

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65046120-1', 'auto');
  ga('send', 'pageview');
</script>
<style>
  label.error{

       color:red !important;
  }
</style>
@endsection

@section('header')

@include('admin.includes.header')

@endsection

@section('sidebar')

@include('admin.includes.sidebar')

@endsection

<div class="content-page">
    <!-- Start content -->
    <div class="content">
      <div class="container">

        <!-- Page-Title -->
        <div class="row">
          <div class="col-sm-12">
            <h4 class="pull-left page-title">Manage Jobs</h4>
            <!--<a href="add_category.html" class="rm_new04"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Category </a>
            <ol class="breadcrumb pull-right">
              <li><a href="#">Aariv School</a></li>
              <li><a href="#">Data Tables</a></li>
              <li class="active">Basic Tables</li>
            </ol>-->
          </div>
        </div>
        @include('admin.includes.message')
        <div class="row">
          <div class="col-md-12">

          <div class="panel panel-default">
          <div class="panel-heading rm02 rm04 rm_new01">
                <form role="form" action="{{ route('admin.manage.job') }}" method="get" id="searchForm">

					 <div class="form-group">
                    <label for="">Keyword</label>
                    <input type="text" name="keyword" value="{{ Request::get('keyword') }}" id="" class="form-control" placeholder="Job/User Name"> 
					</div> 

                   {{-- <div class="form-group">
                    <label for="">Name</label>
                    <input type="text" name="name" value="{{ Request::get('name') }}" id="" class="form-control" placeholder="Enter here">
					</div>

                    <div class="form-group">
                    <label for="">Email</label>
                    <input type="text" name="email" value="{{ Request::get('email') }}" id="" class="form-control" placeholder="Enter here">
					</div> 

					<div class="form-group">
                    <label for="">Phone</label>
                    <input type="text" name="phone" value="{{ Request::get('phone') }}" id="" class="form-control" placeholder="Enter here">
					</div>--}}
                <div class="form-group">
                    <label for="">Location</label>
                    <input type="text" name="location" value="{{ Request::get('location') }}" id="" class="form-control" placeholder="Enter here..">
					</div> 

                    <div class="form-group">
                        <label for="">Category</label>
                        <select class="form-control rm06" name="category" id="category">
                            <option value="">Select Category</option>
                            @foreach($category as $cnt)
                            <option value="{{ $cnt->id }}" @if(@$cnt->id == Request::get('category') ) selected @endif>{{ $cnt->category_name }}</option>
                            @endforeach
                        </select>
					</div>
                     <div class="form-group">
                        <label for="state">State</label>
                        <select class="form-control rm06" name="state" id="state">
                            <option value="">Select state</option>
                            @foreach($states as $state)
                            <option value="{{ $state->id }}" @if(@$state->id == Request::get('state') ) selected @endif>{{ $state->name }}</option>
                            @endforeach
                        </select>
					</div> 
                     <div class="form-group">
                        <label for="city">City</label>
                        <select class="form-control rm06" name="city" id="city">
                            <option value="">Select city</option>
                            @foreach($cites as $city)
                            <option value="{{ $city->id }}" @if(@$city->id == Request::get('city') ) selected @endif>{{ $city->name }}</option>
                            @endforeach
                        </select>
					</div> 

                    <div class="form-group">
                    <label for="">Status</label>
                    <select class="form-control rm06" name="status">
                        <option value="">Select</option>
                        
                        <option value="A" @if(Request::get('status') == 'A') selected @endif>Active</option>
                        <option value="I" @if(Request::get('status') == 'I') selected @endif>Inactive</option>
                        {{--<option value="D" @if(Request::get('status') == 'U') selected @endif>Deleted</option>--}}
                    </select>
                   </div>
                   <div class="form-group">
                    <label for="">Rivirtual Assistance</label>
                    <select class="form-control rm06" name="rivirtual_assist">
                        <option value="">Select</option>
                        
                        <option value="Y" @if(Request::get('rivirtual_assist') == 'Y') selected @endif>Yes</option>
                        <option value="N" @if(Request::get('rivirtual_assist') == 'N') selected @endif>No</option>
                        {{--<option value="D" @if(Request::get('status') == 'U') selected @endif>Deleted</option>--}}
                    </select>
                   </div>

                   <div class="form-group rm_new14">
                    <label for="">From Date </label>
                    <input type="text" name="from_date" value="{{ Request::get('from_date') }}" id="datepicker1" class=" form-control calander_icn " placeholder="Select" autocomplete="off">
              
                  </div>
                  <div class="form-group rm_new14">
                    <label for="">To Date</label>
                    <input type="text" name="to_date" value="{{ Request::get('to_date') }}"  id="datepicker"  class=" form-control calander_icn" placeholder="Select" autocomplete="off">
                    
                  </div>

                  {{--<div class="form-group">
                    <label for="">Budget Type</label>
                    <select class="form-control rm06" name="budget_type">
                        <option value="">Select</option>
                        <option value="H" @if(Request::get('budget_type') == 'H') selected @endif>Hourly</option>
                        <option value="F" @if(Request::get('budget_type') == 'Y') selected @endif>Fixed</option>
                        
                    </select>
                   </div>--}}
                   <div class="form-group">
                     <div class="bedroom-derp">

													<label>Budget</label>

													<div class="slider_rnge">

									                  <div id="slider-range" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">

									                    <div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 0%; width: 100%;"></div> <span tabindex="0" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 0%;"></span> <span tabindex="0" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 100%;"></span> </div> <span class="range-text">

									                           <input type="text" class="price_numb" readonly id="amount">
                                             <input type="hidden" class="price_numb" id="amount1" name="amount1" value="{{@$key['amount1']?@$key['amount1']:0}}">
                                              <input type="hidden" class="price_numb" id="amount2" name="amount2" value="{{@$key['amount2']?@$key['amount2']:(int)@$maxPrice->budget_range_from}}">
									                           </span>

									                </div>
                     </div>
                   </div> 
                  {{-- <div class="form-group">
                    <label for="">Sign Up Using</label>
                    <select class="form-control rm06" name="signup_from">
                        <option value="">Select</option>
                        <option value="E" @if(Request::get('signup_from') == 'E') selected @endif>Email</option>
                        <option value="G" @if(Request::get('signup_from') == 'G') selected @endif>Google</option>
                        <option value="F" @if(Request::get('signup_from') == 'F') selected @endif>Facebook</option>
                    </select>
                   </div> --}}
                   {{-- <div class="form-group">
                    <label for="">Approval Required</label>
                    <select class="form-control rm06" name="approval_status">
                        <option value="">Select</option>
                        <option value="E" @if(Request::get('approval_status') == 'E') selected @endif>Email Change</option>
                        <option value="M" @if(Request::get('approval_status') == 'M') selected @endif>Mobile Change</option>
                        <option value="B" @if(Request::get('approval_status') == 'B') selected @endif>Both</option>
                    </select>
                   </div> --}}
                  <!--
                <div class="form-group">
                    <label for="">Subject</label>
                    <select class="form-control rm06">
                        <option>Select</option>
                        <option>History</option>
                        <option>Geography</option>
                        <option>Mathematics</option>
                        <option>Life Science</option>
                        <option>Physical Science</option>
                        <option>English</option>
                    </select>
                </div>-->

                  <div class="rm05">
                    <button class="btn btn-primary waves-effect waves-light w-md" type="submit">Search</button>
                    <a href="{{ route('admin.manage.job') }}" class="btn btn-success waves-effect waves-light w-md">Reset</a>
                  </div>
                </form>
              </div>
          </div>


            <div class="panel panel-default">


              <div class="panel-body">
                <div class="tab-panel">
                  <form action="{{ route('admin.select.job.delete') }}" method="post" id="deleteForm">
                    @csrf
                    <input type="hidden" name="status" id="select_block">
                    
                    @if(count(@$jobs)>0)
                    <input type="submit" name="" id="" class="btn btn-sm btn-success" value="Delete Selected Job">
                    <input type="submit" name="" id="" class="btn btn-sm btn-success" onclick="selectedfun()" value="Inactive Selected Job">
                    
                    <br>
                    <label id="allid[]-error" class="error" for="allid[]" style="display: none"></label>
                    @endif
                    
            
                  <div class="tab-manage-property">
                    <div class="table-responsive">
                      <table class="table">
                        <thead>
                          <tr>
                            <th></th>
                            <th>Job Name</th>
                            <th>User Name</th>
                            <th>Category</th>
                            <th>Job Start Date</th>
                            <th>Job Start Time</th>
                            
                            <th>Budget</th>
                            <th>Location</th>
                            <th>Status</th>
                            <th>Rivirtual Assistance</th>
                            <th>Approval Status</th>
                            <th class="rm07">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        @if(count(@$jobs) > 0) 
                          @foreach(@$jobs as $val)
                            <tr>
                            <td>
                            <div class="">
							             <input type="checkbox" id="deleteId{{ @$val->id }}"  value="{{@$val->id}}" name="allid[]">
					                	</div>
                            </td>
                                <td>
                                  @if(strlen(@$val->job_name)>30)
                                   {!! substr(@$val->job_name,0,30) . '...' !!}
                                   @else
                                   {!! @$val->job_name  !!}
                                  @endif
                                  </td>
                                <td>{{@$val->getUser->name}}</td>
                                <td>{{ @$val->categoryName->category_name }}</td>
                                <td>{{date('m/d/Y',strtotime(@$val->job_start_date))}}</td>
                                <td>{{date('H:i A',strtotime(@$val->time))}}</td>
                                {{--<td>@if(@$val->budget_type == 'H') Hourly @else Fixed @endif</td>--}}
                                <td>{{@$val->budget_range_from}}</td>
                                <td>{{ @$val->address }}</td>
                                <td class="status{{ @$val->id }}">@if(@$val->status == 'A') Active @elseif(@$val->status == 'I') Inactive @elseif(@$val->status == 'D') Deleted @endif</td>
                                 <td>
                                   @if(@$val->rivirtual_assistance == 'Y')
                                    Yes
                                    @else
                                     No
                                     @endif
                                 </td>
                                 <td class="approval_status{{ @$val->id }}">
                               @if(@$val->approval_by_admin == 'Y')

                                Yes

                                @elseif(@$val->approval_by_admin == 'N')

                                 No
                                @endif
                            </td>
                                <td class="rm07">
                                    <a href="javascript:void(0);" class="action-dots" id="action1" onclick="fun({{ @$val->id }})"><img src="{{ url('public/admin/assets/images/action-dots.png') }}" alt=""></a>
                                <div class="show-actions" id="show-action{{ @$val->id }}" style="display: none;">
                                <span class="angle"><img src="{{ url('public/admin/assets//images/angle.png') }}" alt=""></span>
                                <ul>
                                    <li><a href="{{ route('admin.job.details',['id'=>@$val->id]) }}">View</a></li>
                                    @if(@$val->job_status == 'IP')
                                     @else
                                    <li><a href="{{route('admin.edit.job',@$val->id)}}">Edit</a></li>
                                    @endif
                                    <li>
                                        @if($val->status == 'A')
                                        <a href="javascript:void(0)" class="status_change" data-id="{{ $val->id }}"><span class="text_change{{ $val->id }}">Make Inactive</span></a>
                                        @elseif($val->status == 'I')
                                        <a href="javascript:void(0)" class="status_change" data-id="{{ $val->id }}"><span class="text_change{{ $val->id }}">Make Active</span></a>
                                        @endif
                                    </li>
                                    @if($val->approval_by_admin == 'N')
                                    <li>
                                        <a href="javascript:void(0)" class="approval_status_change" data-id="{{ $val->id }}" onclick="return confirm('Do you want to approve this job?')"><span class="approve_text_change{{ @$val->id }}">Approve Job</span></a> 
                                    </li>
                                    @endif 
                                     @if(@$val->rivirtual_assistance == 'Y')
                                    <li><a href="javascript:;" data-toggle="modal" data-target="#exampleModal" class="assist_modal_open" data-id="{{ @$val->id }}">Add Assistance</a></li>
                                     @endif 
                                    @if(@$val->job_status == 'IP')
                                    <li><a href="{{route('admin.view.milestone',@$val->id)}}">View Milestone</a></li>
                                    @endif
                                    <li><a href="{{ route('admin.delete.job',['id'=>@$val->id]) }}" onclick="return confirm('Are you want to delete this job?')">Delete</a></li>
                                </ul>
                              </div>
                            </td>
                            </tr>
                            @endforeach
                            @else
                            <tr role="row" class="odd">
                                        <td colspan="10" style="text-align:center;">
                                            No Data Found
                                        </td>
                                    </tr>
                            @endif

                        {{-- <tr>
                            <td>Rabin Das</td>
                            <td>test123@gmail.com</td>
                            <td>+91 1234567890</td>
                            <td>14.10.2021</td>
                            <td>Block</td>
                            <td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action3"><img src="assets/images/action-dots.png" alt=""></a>
                            <div class="show-actions" id="show-action3" style="display: none;">
                                <span class="angle"><img src="assets//images/angle.png" alt=""></span>
                                <ul>
                                    <li><a href="#">View</a></li>
                                    <li><a href="#">Edit</a></li>
                                    <li>

                                        <a href="#">Unblock</a>
                                     </li>
                                </ul>
                              </div>
                            </td>
                          </tr>

                          <tr>
                            <td>Abhijeet Roy</td>
                            <td>sample-test123@gmail.com</td>
                            <td>+91 9876543210</td>
                            <td>10.11.2021</td>
                            <td>Unblock</td>
                            <td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action4"><img src="assets/images/action-dots.png" alt=""></a>
                            <div class="show-actions" id="show-action4" style="display: none;">
                                <span class="angle"><img src="assets//images/angle.png" alt=""></span>
                                <ul>
                                    <li><a href="#">View</a></li>
                                    <li><a href="#">Edit</a></li>
                                    <li><a href="#">Block</a></li>
                                </ul>
                              </div>
                            </td>
                          </tr> --}}


                        </tbody>
                      </table>
                    </div>
                    <div style="float: right;">{{@$jobs->appends(request()->except(['page', '_token']))->links()}}</div>

                    <!-- <ul class="pagination">
                      <li class="paginate_button previous disabled"><a href="#">Previous</a></li>
                      <li class="paginate_button active"><a href="#">1</a></li>
                      <li class="paginate_button"><a href="#">2</a></li>
                      <li class="paginate_button"><a href="#">3</a></li>
                      <li class="paginate_button"><a href="#">4</a></li>
                      <li class="paginate_button"><a href="#">5</a></li>
                      <li class="paginate_button"><a href="#">6</a></li>
                      <li class="paginate_button next"><a href="#">Next</a></li>
                    </ul> -->


                  </div>
                  </form>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- End row -->

      </div>
      <!-- container -->

    </div>
    <!-- content -->
     
    @include('admin.includes.footer')
    <div class="modal fade assist_modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <form action="{{ route('admin.save.rivirtual.assist') }}" method="post" id="addForm">
         @csrf
         <input type="hidden" name="postId" id="postId">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">      
          <div class="row assist_body">
            <div class="col-sm-6">
              <div class="form-group">
                  <label for="">Name</label>
                  <input type="text" name="assist_name" value="" id="" class="form-control" placeholder="Enter here">
              </div> 
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                  <label for="">Email</label>
                  <input type="text" name="assist_email" value="" id="" class="form-control" placeholder="Enter here">
              </div> 
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                  <label for="">phone</label>
                  <input type="text" name="assist_phone" value="" id="" class="form-control" placeholder="Enter here">
              </div> 
            </div>
            <div class="col-sm-12">
              <div class="assist_btn">
               <input type="submit" name="" id="" value="Save" class="btn btn-primary">       
              </div>
             </div>
          </div>
        
      </div>
     </form>
    </div>

  </div>
</div>
  </div>

  @section('scripts')

  @include('admin.includes.scripts')
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script> -->
        <script>
    $("#datepicker1").datepicker({
      numberOfMonths: 1,
      onSelect: function (selected) {
        var dt = new Date(selected);
        dt.setDate(dt.getDate() + 1);
        $("#datepicker").datepicker("option", "minDate", dt);
      },
      maxDate: new Date("{{ date('m/d/Y') }}")
    });
    $("#datepicker").datepicker({
      numberOfMonths: 1,
      onSelect: function (selected) {
        var dt = new Date(selected);
        dt.setDate(dt.getDate() - 1);
        $("#datepicker1").datepicker("option", "maxDate", dt);
      },
    });

  </script>

  <script>
      function fun(id){

             $('.show-actions').slideUp();
              $("#show-action"+id).show();
      }
      $(document).on('click', function () {
      var $target = $(event.target);
      if (!$target.closest('.action-dots').length && $('.show-actions').is(":visible")) {
          $('.show-actions').slideUp();
        }
    });

 $(document).on('click','.status_change', function(e){

var id = $(e.currentTarget).attr('data-id');

  if(confirm('Are you want to change this status?')){

       $.ajax({

            url: '{{ route('job.status.update') }}',
            method: 'GET',
            data: 'id='+id,

            success: function(resp){
                 $('.text_change'+id).html('');
                  if(resp != 0){

                      var str = "";
                     if(resp.status == 'A'){
                         str = "Active"
                        $(".success_msg").html("Success! Status is changed to Active!");
                        $('.text_change'+id).html('Make Inactive');

                     }else if(resp.status == 'I'){
                        str = "Inactive";
                      $(".success_msg").html("Success! Status is changed to Inactive!");
                      $('.text_change'+id).html('Make Active');

                     }
                     $('.status'+id).html(str);
                      $(".success_msg_div").show();

                     $(".error_msg_div").hide();


                  }else{

                    $(".error_msg_div").show();

                      $(".success_msg_div").hide();

                      $(".error_msg").html("Something went wrong!");
                  }


            },
            error: function(error){

               consolse.log(error);
            }
       });
  }


});


$(document).on('click','.approval_status_change', function(e){

var id = $(e.currentTarget).attr('data-id');
      
       $.ajax({

            url: '{{ route('job.approval.status.update') }}',
            method: 'GET',
            data: 'id='+id,

            success: function(resp){
              console.log(resp);
                 $('.approve_text_change'+id).html('');
                 var str = "";
                  if(resp.data.message){

                      
                     
                         str = "Yes"
                        $(".success_msg").html(resp.data.message);
                        $('.approve_text_change'+id).css('display','none');

                        $('.approval_status'+id).html(str);
                      $(".success_msg_div").show();

                     $(".error_msg_div").hide();


                     

                  }else{

                    $(".error_msg_div").show();

                      $(".success_msg_div").hide();

                      $(".error_msg").html("Something went wrong!");
                  }


            },
            error: function(error){

               consolse.log(error);
            }
       });

});


 </script>



<script>
  $(document).ready(function(){

var allSliderAmount;
      @if(@$key['amount1']!=null && @$key['amount2']!=null)
      allSliderAmount=[];
      allSliderAmount=['{{$key['amount1']}}','{{$key['amount2']}}'];
      @else
      allSliderAmount=[0,'{{(int)@$maxPrice->budget_range_from}}'];
      @endif
      console.log(allSliderAmount);
      $( "#slider-range" ).slider({
          range: true,
          min:0 ,
          max: '{{(int)@$maxPrice->budget_range_from}}',
          values: allSliderAmount,
          slide: function( event, ui ) {
              $( "#amount" ).val( "₹" + ui.values[ 0 ] + " - ₹" + ui.values[ 1 ] );
              $( "#amount1" ).val( ui.values[ 0 ]);
              $( "#amount2" ).val( ui.values[ 1 ] );
          }
      });

$( "#amount" ).val( "₹" + $( "#slider-range" ).slider( "values", 0 ) +

  " - ₹" + $( "#slider-range" ).slider( "values", 1 ) ); 

});
</script>

<script>

$(document).ready(function(){

     $('#deleteForm').validate({

           rules: {

                'allid[]': {

                    required: true,
                },
           },
           messages: {

               'allid[]': {

                   required: "Please select at least one job",
               },
           }
     });
});


// $(function () {
// $(".datepicker").datepicker({
// autoclose: true,
// todayHighlight: true
// });
// });

function selectedfun(){

     $('#select_block').val('I');
     $('#deleteForm').submit();
}

</script>
<script type="text/javascript">
    $(document).ready(function(){
      $('#country').on('change',function(e){
        e.preventDefault();
        var id = $(this).val();

        $.ajax({
          url:'{{route('get.state')}}',
          type:'GET',
          data:{country:id,id:'{{@$user->state}}'},
          success:function(data){
            console.log(data);
            $('#state').html(data.state);
          }
        })
      });

      $('#state').on('change',function(e){
        e.preventDefault();
        var id = $(this).val();
        $.ajax({
          url:'{{route('get.city')}}',
          type:'GET',
          data:{state:id,id:'{{@$user->city}}'},
          success:function(data){
            console.log(data);
            $('#city').html(data.city);
          }
        })
      });

    });
</script>
<script>
  $(document).ready(function(){

    jQuery.validator.addMethod("validate_email", function(value, element) {
        if (/^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,5}$/i.test(value)) {
            return true;
        } else {
            return false;
        }
    }, "Oops...! check your Email again");

    $('#addForm').validate({

         rules: {

              assist_name: {

                   required: true,
              },
              assist_email: {

                   required: true,
                   validate_email: true,
              },
              assist_phone: {

                required: true,
                digits: true ,
                minlength: 10,
                maxlength: 10,
              },
         },
         messages: {

              assist_email: {

                required: 'Enter email',
                email:'Oops...! check your Email again',
              },
              assist_phone: {

                required: 'Enter Mobile Number',
                digits: 'Please enter a valid number ',
                minlength: 'Exactly only 10 digits without country code',
                maxlength: 'Exactly only 10 digits without country code',
              }
         }
    })

  })
</script>
<script>
  $(document).ready(function(){

        $('.assist_modal_open').click(function(e){

               var post_id = $(e.currentTarget).attr('data-id');
               $('#postId').val(post_id);
        })
  })
</script>
@endsection

  @endsection

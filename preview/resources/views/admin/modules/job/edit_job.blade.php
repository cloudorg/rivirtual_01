@extends('admin.layouts.app')

@section('title')
RiVirtual | Admin | Edit Property
@endsection

@section('content')

@section('links')

@include('admin.includes.links')

<style>
    label.error{

         color:red;
    }

    .uplodimg_pick img {
    width: 50px;
    height: 50px;
    object-fit: cover;
    border-radius: 100%;
}
</style>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65046120-1', 'auto');
  ga('send', 'pageview');
</script>
<link href="{{ URL::asset('public/frontend/croppie/croppie.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('public/frontend/croppie/croppie.min.css') }}" rel="stylesheet" />
@endsection

@section('header')

@include('admin.includes.header')

@endsection

@section('sidebar')

@include('admin.includes.sidebar')

@endsection

<div class="content-page">
    <!-- Start content -->
    <div class="content">
      <div class="container">

        <!-- Page-Title -->
        <div class="row">
          <div class="col-sm-12">
            <h4 class="pull-left page-title">Edit Job</h4>
            <a href="{{ route('admin.manage.job') }}" class="rm_new04"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
            <!--<ol class="breadcrumb pull-right">
              <li><a href="#">Aariv School</a></li>
              <li><a href="#">Data Tables</a></li>
              <li class="active">Basic Tables</li>
            </ol>-->
          </div>
        </div>
        @include('admin.includes.message')
        <div class="row">
          <div class="col-md-12">


          <div class="tab_stylee">
            
          </div>


            <div class="panel panel-default">


              <div class="">
            <form role="form" action="{{route('admin.save.job',@$postDetails->id)}}" method="post"  id="addForm" enctype="multipart/form-data">
                @csrf
                

                <div class="agent-right-body new-upcon">
                        <div class="row">
                            
                           
                        </div>
                        <div class="row">
                            
                            <div class="col-md-8 col-lg-6">
                                    <div class="das_input">
                                        <label>Job Title</label>
                                        <input type="text" class="required" placeholder="Enter here.." name="job_title" value="{{ @$postDetails->job_name }}">
                                    </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-4 col-lg-3 new-rent-class" >
                                <div class="das_input rent_pay">
                                    <label>Service Category</label>
                                    <select name="category" id="category">
                                        <option value="">Select category </option>
                                        @foreach(@$category as $cat)       
                                        <option value="{{ @$cat->id }}" @if(old('category')==@$cat->id || @$postDetails->category==@$cat->id) selected @endif>{{ @$cat->category_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4 col-lg-3 new-rent-class" >
                              <div class="das_input rent_pay">  
                            <label>Sub Category </label>
                           <select name="sub_category" id="sub_category">

								<option value="">Select Sub Category </option>
                                  @foreach(@$allSubCategory as $cat)       
								<option value="{{ @$cat->id }}" @if(old('sub_category')==@$cat->id || @$postDetails->sub_category==@$cat->id) selected @endif>{{ @$cat->name }}</option>

							   @endforeach

							</select>
                            </div>
                            </div>
                            
                            <div class="col-md-4 col-lg-3  remove_rooms" >
                                    <div class="das_input">
                                        <label>Duration/(Days)</label>
                                        <input type="text" name="day" placeholder="Enter here.." value="{{ old('day',@$postDetails->duration) }}">
                                    </div>
                            </div>
                            <div class="col-md-4 col-lg-3  remove_rooms" >
                                <div class="das_input">
                                    <label>Start Date</label>

                                        <div class="dash-d">

                                                <input type="text" name="start_date" placeholder="From date" id="datepicker" value="{{ @$postDetails? date('m/d/Y',strtotime(@$postDetails->job_start_date)): old('start_date') }}" autocomplete="off">

                                                <span class="over_llp1"><img src="{{ url('public/frontend/images/cala.png') }}" alt=""></span>

                                        </div>
                                </div>
                            </div>
                
                            <div class="col-md-4 col-lg-3  remove_rooms" >
                                <div class="das_input">
                                    <label>Start Time </label>

                                        <div class="dash-d">

                                                <input type="text" name="time" placeholder="Enter here.. " class="position-relative" value="{{@$postDetails? date('h:i A',strtotime(@$postDetails->time)): old('time') }}"> <span class="over_llp1"><img src="{{ url('public/frontend/images/clock.png') }}" alt=""></span>

                                        </div>
                                </div>
                            </div>
                             
                            <div class="col-md-6 col-lg-6">
                                    <div class="das_input">
                                        <label>Budget Range</label>
                                        <div class="bud-input">
                                            <div class="bud_sets">
                                                <input class="required" type="text" placeholder="From" value="{{@$postDetails->budget_range_from?(int)$postDetails->budget_range_from:''}}" name="budget_range_from" id="budget_range_from" maxlength="10">
                                                <span class="budget_iconn02"><i class="fa fa-inr" aria-hidden="true"></i></span>
                                               {{--<p id="budget_range_from_p">@if(@$postDetails->budget_range_from)<span>₹</span> {{numberchange((int)$postDetails->budget_range_from)}} @endif</p>--}}
                                            </div>
                                            <div class="bud_sets pad_le">
                                                <input class="required" type="text" placeholder="To" value="{{@$postDetails->budget_range_to?(int)$postDetails->budget_range_to:''}}" name="budget_range_to" id="budget_range_to" maxlength="10">
                                                <span class="budget_iconn02"><i class="fa fa-inr" aria-hidden="true"></i></span>
                                                {{--<p id="budget_range_to_p">@if(@$postDetails->budget_range_to)<span>₹</span> {{numberchange((int)$postDetails->budget_range_to)}}@endif</p>--}}
                                            </div>


                                        </div>
                                        <div class="bud-input" >
                                            <div style="width: 46.5%;">

                                                <label id="budget_range_from-error" class="error" for="budget_range_from" style="display:none;"></label>
                                            </div>
                                            <div style="width: 46.5%;">

                                                <label id="budget_range_to-error" class="error" for="budget_range_to" style="display:none;"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            {{--<div class="col-md-4  col-lg-3 ">

                                    <div class="das_input">

                                        <label>Budget </label>

                                        <div class="d-flex align-items-center">

                                            <div class="wid45">
                                            <select name="budget_type">

                                            <option value="F" @if(old('budget_type')=='F' || @$postDetails->budget_type == 'F') selected @endif>Fixed </option>

                                            <option value="H" @if(old('budget_type')=='H' || @$postDetails->budget_type == 'H') selected @endif>Hourly Rate</option>

                                        </select>
                                           </div>

                                            <div class="wid55"><input type="text" name="budget" id="budget" placeholder="Enter here.." value="{{ old('budget',intval(@$postDetails->budget)) }}"></div>

                                        </div>
                                        <label id="budget-error" class="error" for="budget" style="display: none"></label>
                                    </div>

                                </div>--}}
                            
                                 <div class="col-md-6  col-lg-3">
                                   <div class="das_input rent_pay">  
                                 <label>Country</label>
                                        <select name="country" id="country" class="form-control rm06 required">
                                            <option value="">Select Country</option>
                                             <option value="101" selected>India</option>
                                             
                                        </select>
                                 </div> 
                                 </div>
                                 

                                <div class="col-md-12  col-lg-4 ">
                                  <div class="das_input rent_pay">
                                <label>State</label>
                              <select name="state" id="states" class="form-control rm06 required">
                                <option value="">Select State</option>
                                @foreach(@$states as $state)
                                <option value="{{@$state->id}}" @if(@$postDetails->state==@$state->id)selected @endif>{{@$state->name}}</option>
                                 @endforeach
                                 </select>
                                </div>
                                </div>
                                <div class="col-md-6  col-lg-4 ">
                                  <div class="das_input rent_pay">  
                                <label>City</label>
                           <select name="city" id="city" class="form-control rm06 required">
                                <option value="">Select City</option>

                               @foreach(@$cites as $city)
                              <option value="{{@$city->id}}" @if(@$postDetails->city==@$city->id)selected @endif>{{@$city->name}}</option>
                                @endforeach
                                 </select>
                                </div>
                                </div>
                                <div class="col-md-6  col-lg-4">

                                    <div class="das_input">

                                        <label>Address</label>

                                        <input type="text" name="address" placeholder="Enter here.." value="{{ old('address',@$postDetails->address) }}">



                                    </div>

                                </div>

                                <div class="col-md-12">

                                    <div class="das_input">

                                        <label>Description </label>

                                        <textarea placeholder="Enter here.." name="description">{{ old('description',@$postDetails->description) }}</textarea>  



                                    </div>

                                </div>

                                <div class="col-md-12">

                                    <div class="uplodimg">                                      

                                        <div class="uplodimgfil">

                                           
											<b>Upload Photo</b>
                                             {{--<input type="hidden" name="service_image" id="service_image">--}}
											<input type="file" name="file-1[]" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" onchange="fun1()" accept="image/*" multiple />

											<label for="file-1">Upload Job Photo <img src="{{ url('public/frontend/images/clickhe.png') }}" alt=""></label>
                                            <label id="file-1-error" class="error" for="file-1" style="display: none"></label>
                                             {{--<input type="hidden" name="old_image" id="old_image" value="{{@$postDetails->image}}">--}}
                                        </div>  

                                        {{--<div class="uplodimg_pick uplodimgfilimg">

                                             @if(@$postDetails->image != null)
                                                <img src="{{ URL::to('storage/app/public/service_image')}}/{{@$postDetails->image}}" alt="" id="img2">
                                                @else
                                                <img src="{{ url('public/frontend/images/ga4.png') }}" alt="">
                                                @endif

                                            

                                        </div>--}}

                                    </div>
                                    <div class="dash-list-agent" id="file-1-upoade">
                                        <ul class="edit-gallery" id="file-1-ul">

                                            <div class="clearfix"></div>
                                        </ul>
                                    </div>
                                    @if(@$jobImages)
                                    <div class="dash-list-agent">

                                        <ul class="edit-gallery">
                                            @foreach ($jobImages as $image)
                                            <li>
                                                <div class="upimg">
                                                    <img src="{{ URL::to('storage/app/public/service_image')}}/{{$image->image}}">
                                                    <a href="{{route('admin.add.job.image.remove',['id'=>$image->id])}}"><img src="{{ URL::to('public/frontend/images/w-cross.png')}}"></a>
                                                </div>
                                            </li>
                                            @endforeach

                                            <div class="clearfix"></div>
                                        </ul>
                                    </div>
                                    @endif
                                </div>
                        </div>
                            
                            
                            
                        
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="bodar">
                                    </div>
                                </div>
                                
                                <div class="col-md-12">
                                    <div class="usei_sub">
                                    <button class="btn btn-primary waves-effect waves-light w-md rm_new15" type="submit">Post Job</button>
                                    </div>
                                </div>
                        </div>
                    </div>



            </form>
            </div>
            </div>

          </div>
        </div>
        <!-- End row -->

      </div>
      <!-- container -->

    </div>
    <!-- content -->
    <div class="modal" tabindex="-1" role="dialog" id="croppie-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Crop Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="croppie-div" style="width: 100%;"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="crop-img">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

    @include('admin.includes.footer')
  </div>

  @section('scripts')

  @include('admin.includes.scripts')
<script src="{{ URL::asset('public/frontend/croppie/croppie.js') }}"></script>
<script src='https://weareoutman.github.io/clockpicker/dist/jquery-clockpicker.min.js'></script>
{{--<script>
    function dataURLtoFile(dataurl, filename) {
        var arr = dataurl.split(','),
        mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]),
        n = bstr.length,
        u8arr = new Uint8Array(n);
        while(n--){
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new File([u8arr], filename, {type:mime});
    }
    var uploadCrop;
    $(document).ready(function(){
        $('#croppie-modal').on('hidden.bs.modal', function() {
            uploadCrop.croppie('destroy');
        });

        $('#crop-img').click(function() {
            uploadCrop.croppie('result', {
                type: 'base64',
                format: 'png'
            }).then(function(base64Str) {
                $("#croppie-modal").modal("hide");
               //  $('.lds-spinner').show();
               let file = dataURLtoFile('data:text/plain;'+base64Str+',aGVsbG8gd29ybGQ=','hello.png');
                  console.log(file.mozFullPath);
                  console.log(base64Str);
                  $('#service_image').val(base64Str);
               // $.each(file, function(i, f) {
                    var reader = new FileReader();
                    reader.onload = function(e){
                        $('.uplodimgfilimg').append('<em><img  src="' + e.target.result + '"><em>');
                    };
                    reader.readAsDataURL(file);

               //  });
                $('.uplodimgfilimg').show();

            });
        });
    });
    $("#file-1").change(function () {
            $('.uplodimgfilimg').html('');
            let files = this.files;
            console.log(files);
            let img  = new Image();
            if (files.length > 0) {
                let exts = ['image/jpeg', 'image/png', 'image/gif'];
                let valid = true;
                $.each(files, function(i, f) {
                    if (exts.indexOf(f.type) <= -1) {
                        valid = false;
                        return false;
                    }
                });
                if (! valid) {
                    alert('Please choose valid image files (jpeg, png, gif) only.');
                    $("#file-1").val('');
                    return false;
                }
                // img.src = window.URL.createObjectURL(event.target.files[0])
                // img.onload = function () {
                //     if(this.width > 250 || this.height >160) {
                //         flag=0;
                //         alert('Please upload proper image size less then : 250px x 160px');
                //         $("#file").val('');
                //         $('.uploadImg').hide();
                //         return false;
                //     }
                // };
                $("#croppie-modal").modal("show");
                uploadCrop = $('.croppie-div').croppie({
                    viewport: { width: 256, height: 256, type: 'square' },
                    boundary: { width: $(".croppie-div").width(), height: 400 }
                });
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.upload-demo').addClass('ready');
                    // console.log(e.target.result)
                    uploadCrop.croppie('bind', {
                        url: e.target.result
                    }).then(function(){
                        console.log('jQuery bind complete');
                    });
                }
                reader.readAsDataURL(this.files[0]);
               //  $('.uploadImg').append('<img width="100"  src="' + reader.readAsDataURL(this.files[0]) + '">');
               //  $.each(files, function(i, f) {
               //      var reader = new FileReader();
               //      reader.onload = function(e){
               //          $('.uploadImg').append('<img width="100"  src="' + e.target.result + '">');
               //      };
               //      reader.readAsDataURL(f);
               //  });
               //  $('.uploadImg').show();
            }

        });
</script>--}}

<script>
  jQuery.validator.addMethod("validate_name", function(value, element) {
            if (/^([a-zA-Z0-9 ])+$/.test(value)) {
                 return true;
            } else {
                 return false;
            }
        }, "Allow only (a-z, A-Z, 0-9, )");
    $(document).ready(function(){
        $('#addForm').validate({
            rules: {
                job_title:{
                    
                    required: true,
                    validate_name: true,
                },
                category:{
                    required:true,
                },
                sub_category: {

                     required:true,
                },
                country: {

                     required:true,
                },
                state: {

                     required:true,
                },
                city: {

                     required:true,
                },
                day:{
                    required:true,
                    digits: true ,
                    minlength: 2,
                    maxlength: 2,
                },
                start_date:{
                    required:true,
                },
                time: {
                   
                   required: true,
                },
                budget_range_from:{
                    required: true,
                    digits: true ,
                    min:1,
                    max:function(){
                        if($('#budget_range_to').val()!=''){
                            return $('#budget_range_to').val();
                        }
                    },
                }, 
                address: {

                     required: true,
                },
                description: {

                     required: true,
                },
                'file-1[]':{
                   extension: "png|jpe?g",
                },
                
            },
            messages:{
               
                day:{
                    
                    digits: 'Please enter a valid number ',
                },
                'file-1[]':{
                    required:'job Photos required',
                    extension: "File must be JPG or PNG",
                },
                budget_range_from:{
                    required: 'Enter budget from ',
                    digits: 'Budget from only number ',
                    min: 'Budget from start from 1',
                    max: 'Budget from not gater budget to',
                },
                budget_range_to:{
                    required: 'Enter budget to',
                    digits: 'Budget to only number ',
                    min: 'Budget to not small budget from',
                },
                
            }

        });
    
});
</script>
<script>
    function fun1(){
        console.log(document.getElementById('file-1').files);
        var html='';
        for (i = 0; i < document.getElementById('file-1').files.length; ++i) {
            console.log(i)
            var ii=document.getElementById('file-1').files[i];
            var b=URL.createObjectURL(ii);
            html = html+`<li><div class="upimg"><img src="`+b+`"></div></li>`;
        }
        $('#file-1-ul').html(html);
        console.log(html)
    }
</script>
<script>
    $("#datepicker").datepicker(
       {
         minDate: new Date(),
       } 
    );
</script>
<script>

    // $("input[name=time]").clockpicker({

    //     placement: 'bottom',

    //     align: 'left',

    //     autoclose: true,

    //     default: 'now',

    //     donetext: "Select",

    //     init: function() {

    //         console.log("colorpicker initiated");

    //     },

    //     beforeShow: function() {

    //         console.log("before show");

    //     },

    //     afterShow: function() {

    //         console.log("after show");

    //     },

    //     beforeHide: function() {

    //         console.log("before hide");

    //     },

    //     afterHide: function() {

    //         console.log("after hide");

    //     },

    //     beforeHourSelect: function() {

    //         console.log("before hour selected");

    //     },

    //     afterHourSelect: function() {

    //         console.log("after hour selected");

    //     },

    //     beforeDone: function() {

    //         console.log("before done");

    //     },

    //     afterDone: function() {

    //         console.log("after done");

    //     }

    // });

</script>




<script>
    $(document).ready(function(){ 
        $("input[name=time]").clockpicker({

        placement: 'bottom',

        align: 'left',

        autoclose: true,

        default: 'now',

        donetext: "Select",

        init: function() {

            console.log("colorpicker initiated");

        },

        beforeShow: function() {

            console.log("before show");

        },

        afterShow: function() {

            console.log("after show");

        },

        beforeHide: function() {

            console.log("before hide");

        },

        afterHide: function() {

            console.log("after hide");

        },

        beforeHourSelect: function() {

            console.log("before hour selected");

        },

        afterHourSelect: function() {

            console.log("after hour selected");

        },

        beforeDone: function() {

            console.log("before done");

        },

        afterDone: function() {

            console.log("after done");

        }

    });
       
});
</script>


<script type="text/javascript">
    $(document).ready(function(){

      $('#states').on('change',function(e){
        e.preventDefault();
        var id = $(this).val();
        $.ajax({
          url:'{{route('get.city')}}',
          type:'GET',
          data:{state:id,id:'{{@$postDetails->state}}'},
          success:function(data){
            console.log(data);
            $('#city').html(data.city);
          }
        })
      });

    $('#category').on('change',function(e){
  e.preventDefault();
  var id = $(this).val();
  $.ajax({
	url:'{{route('get.subcategory')}}',
	type:'GET',
	data:{category:id},
	success:function(data){
	  console.log(data);
	  $('#sub_category').html(data.subcategory);
	}
  })
});
      


    });
  </script>

@endsection
@endsection

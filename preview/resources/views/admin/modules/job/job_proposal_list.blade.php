@extends('admin.layouts.app')

@section('title')
RiVirtual | Admin |Job Proposal List
@endsection

@section('content')

@section('links')

@include('admin.includes.links')

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65046120-1', 'auto');
  ga('send', 'pageview');
</script>

@endsection

@section('header')

@include('admin.includes.header')

@endsection

@section('sidebar')

@include('admin.includes.sidebar')

@endsection

<div class="content-page"> 
    <!-- Start content -->
    <div class="content">
      <div class="container"> 
        
        <!-- Page-Title -->
        <div class="row">
          <div class="col-sm-12">
            <h4 class="pull-left page-title"> Manage Jobs <i class="fa fa-chevron-right" aria-hidden="true"></i>{{ @$job_name->job_name }}</h4>
            <a href="{{ route('admin.manage.job') }}" class="rm_new04"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
            <!--<a href="add_class_links.html" class="rm_new04"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Class Links</a>-->
            <!--<ol class="breadcrumb pull-right">
              <li><a href="#">Aariv School</a></li>
              <li><a href="#">Data Tables</a></li>
              <li class="active">Basic Tables</li>
            </ol>-->
          </div>
        </div>
        @include('admin.includes.message')
        <div class="row">
          <div class="col-md-12">
          
          
          <div class="tab_stylee">
          	<a href="{{ route('admin.job.details',['id'=>@$jobId]) }}">Job Details</a>
            
            <a href="{{ route('admin.job.proposal',['id'=>@$jobId]) }}" class="tab_active">View Proposal</a>
          </div>
          <div class="panel panel-default">
          <div class="panel-heading rm02 rm04 rm_new01">
                {{--<form role="form" action="{{ route('admin.job.proposal',['id'=>@$jobId]) }}" method="post">
                    @csrf
                  
					<div class="form-group">
                    <label for="">Client Name</label>
                    <input type="text" name="client_name" value="{{ @$key['client_name'] }}" id="" class="form-control" placeholder="Enter Name...">
					</div>
                    <div class="form-group">
                    <label for="">Location</label>
                    <input type="text" name="location" value="{{ @$key['location'] }}" id="" class="form-control" placeholder="Enter Location...">
					</div>
          <div class="form-group">
                    <label for="">From Date </label>
                    <input type="text" name="from_date" value="{{ @$key['from_date'] }}" id="datepicker1" class="datepicker form-control calander_icn " placeholder="Select" autocomplete="off">
                  </div>
                  <div class="form-group">
                    <label for="">To Date</label>
                    <input type="text" name="to_date" value="{{ @$key['to_date'] }}" id="datepicker"  class="datepicker form-control calander_icn " placeholder="Select" autocomplete="off">
                  </div>
                    <div class="form-group">
                    <label for="">Status</label>
                    <select class="form-control rm06" name="status">
                        <option value="">Select</option>
                        <option value="S" @if(Request::get('status') == 'S') selected @endif>Sold</option>
                        <option value="A" @if(Request::get('status') == 'A') selected @endif>Active</option>
                        <option value="B" @if(Request::get('status') == 'B') selected @endif>Inactive</option>
                    </select>
                   </div>
                   
                  <!--
                <div class="form-group">
                    <label for="">Subject</label>
                    <select class="form-control rm06">
                        <option>Select</option>
                        <option>History</option>
                        <option>Geography</option>
                        <option>Mathematics</option>
                        <option>Life Science</option>
                        <option>Physical Science</option>
                        <option>English</option>
                    </select>
                </div>-->
                  
                  <div class="rm05">
                    <button class="btn btn-primary waves-effect waves-light w-md" type="submit">Search</button>
                    <a href="{{ route('admin.job.proposal',['id'=>@$jobId]) }}" class="btn btn-success waves-effect waves-light w-md">Reset</a>
                  </div>
                </form>--}}
              </div>
          </div>
            <div class="panel panel-default panel-fill">
            
                    <div class="panel-body"> 
                        
                        <div class="table-responsive">
                      <table class="table">
                        <thead>
                          <tr>
                            <th>Profile Image</th>
                            <th>Date</th>
                            <th>Timeline(Days)</th>
                            <th>Quote Description</th>
                            <th>Awarded</th>
                          
                            {{--<th>Status</th>--}}
                            <th class="rm07">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          @if(count(@$allProposal)>0)
                           @foreach($allProposal as $proposal)
                          <tr>
                            <td>
                            @if($proposal->proDetails->profile_pic != null)
                            <img src="{{ url('storage/app/public/profile_picture/'.@$proposal->proDetails->profile_pic) }}" alt="" width="45px;">
                            @else
                            <img src="{{ url('public/admin/assets/images/avatar.png') }}" alt="">
                            @endif<br>
                            {{ @$proposal->proDetails->name }}
                            </td>
                            <td>{{ date('m/d/Y',strtotime(@$proposal->created_at)) }}</td>
                            <td>{{ @$proposal->timeline }}<br>
                                ₹{{ @$proposal->cost }}
                             </td>
                            <td>
                            <p class="desc_bid" data-id="{{@$proposal->id}}">
							             	@if(strlen(@$proposal->quote_description)>60)
				
								           	<p class=" short_desc_bid short_desc_bid_{{@$proposal->id}}" data-desc="{{substr(@$proposal->quote_description, 0, 100)}}" data-id ="{{$proposal->id}}">
								           	<a href="javascript:;" class="read_more_bid" data-id="{{@$proposal->id}}"> Read more +</a>
								          	</p>
								          	<p class="full_desc_bid full_desc_bid_{{@$proposal->id}}"  data-desc="{{@$proposal->quote_description}}" style="display:none;" data-id="{{$proposal->id}}">
																	

								            	<a href="javascript:;" class="read_more_bid" data-id="{{@$proposal->id}}"> Read less -</a>
								          	</p>
								          	@else
								       	<p>{!! @$proposal->quote_description !!}</p>
							          	@endif
														  	
						                	</p>
                                
                              </td>
                              <td>
                                @if(@$proposal->status == 'Awarded')
                                 Yes
                                 @else
                                 No 
                                 @endif
                              </td>
                              @if(@$proposal->jobDetails->job_status != 'IP')
                              
                            <td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action" onclick="fun({{ @$proposal->id }})"><img src="{{ url('public/admin/assets/images/action-dots.png') }}" alt=""></a>
                            <div class="show-actions" id="show-action{{ @$proposal->id }}" style="display: none;">
                                <span class="angle"><img src="{{ url('public/admin/assets//images/angle.png') }}" alt=""></span>
                                <ul>
                                   
                                    <li>
                                     @if(@$proposal->is_recommended == 0) 
                                    <a href="{{ route('admin.bid.recommended',['id'=>@$proposal]) }}" onclick="return confirm('Do you want to recommended this bid?')">Recommend</a>
                                     @else
                                     <a href="{{ route('admin.bid.recommended',['id'=>@$proposal]) }}" onclick="return confirm('Do not  you want to recommended this bid?')">Remove Recommendation</a>
                                    
                                    </li>
                                    @endif
                                    {{--<li><a href="#">Unblock</a></li>--}}
                                </ul>
                              </div>
                            </td>
                            @else
                            <td></td>
                            @endif
                          </tr>
                          @endforeach
                          @else
                        
                           <tr role="row" class="odd">
                                        <td colspan="8" style="text-align:center;"> 
                                            No Data Found
                                        </td>
                                    </tr>
                             @endif       
                        
                          {{--<tr>
                            <td>Rabin Manna</td>
                            <td>Abhijeet Roy</td>
                            <td>18.10.2021</td>
                            <td>11:30 am</td>
                            <td>Mumbai</td>
                            <td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action2"><img src="{{ url('public/admin/assets/images/action-dots.png') }}" alt=""></a>
                            <div class="show-actions" id="show-action2" style="display: none;">
                                <span class="angle"><img src="{{ url('public/admin/assets//images/angle.png') }}" alt=""></span>
                                <ul>
                                    <li><a href="agent_details.html">Reschedule</a></li> 
                                    <li><a href="agent_details.html">Cancel</a></li>
                                    <!--<li><a href="#">Edit</a></li>
                                    <li><a href="#">Unblock</a></li>-->
                                </ul>
                              </div>
                            </td>
                          </tr>
                          
                          
                          <tr>
                            <td>Rabin Das</td>
                            <td>Abhijeet Roy</td>
                            <td>14.10.2021</td>
                            <td>10:20 am</td>
                            <td>Kolkata</td>
                            <td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action3"><img src="assets/images/action-dots.png" alt=""></a>
                            <div class="show-actions" id="show-action3" style="display: none;">
                                <span class="angle"><img src="assets//images/angle.png" alt=""></span>
                                <ul>
                                    <li><a href="agent_details.html">Cancel</a></li>
                                    <!--<li><a href="#">Edit</a></li>
                                    <li><a href="#">Unblock</a></li>-->
                                </ul>
                              </div>
                            </td>
                          </tr>
                          
                          <tr>
                            <td>Rabin Manna</td>
                            <td>Abhijeet Roy</td>
                            <td>18.10.2021</td>
                            <td>11:30 am</td>
                            <td>Mumbai</td>
                            <td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action4"><img src="assets/images/action-dots.png" alt=""></a>
                            <div class="show-actions" id="show-action4" style="display: none;">
                                <span class="angle"><img src="assets//images/angle.png" alt=""></span>
                                <ul>
                                    <li><a href="agent_details.html">Reschedule</a></li>
                                    <li><a href="agent_details.html">Cancel</a></li>
                                    <!--<li><a href="#">Edit</a></li>
                                    <li><a href="#">Unblock</a></li>-->
                                </ul>
                              </div>
                            </td>
                          </tr>--}}
                        
                      
                        </tbody>
                      </table>
                    </div>
                    
                    <div style="float: right;"></div>
                    
                        
                    </div> 
                </div>
          
            
            
          </div>
        </div>
        <!-- End row --> 
        
      </div>
      <!-- container --> 
      
    </div>
    <!-- content -->
    
    @include('admin.includes.footer')
  </div>
  {{--<div class="modal res-modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Reschedule Property Visits</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <form>
                                        <div class="appome_input das_input">
                                            <label>Alternative Date:</label>
                                            <div class="dash-d">
                                            <input type="text" placeholder="Select Date" id="datepicker3">
                                            <span class="over_llp1"><img src="{{ url('public/admin/assets/images/cala.png') }}"></span>
                                            </div>
                                        </div>
                                        <div class="appome_input das_input">
                                            <label>Select Time</label>
                                            <div class="dash-d">
                                            <input type="text" placeholder="Select Time" name="time">
                                            <span class="over_llp1"><img src="{{ url('public/admin/assets/images/clock.png') }}"></span>
                                            </div>
                                        </div>
                                        
                                    </form>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Save</button>
        </div>
        
      </div>
    </div>
  </div>--}}
  @section('scripts')

  @include('admin.includes.scripts')
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script> -->

  <script>
  function fun(id){

$('.show-actions').slideUp();
 $("#show-action"+id).show();
}
</script>

<script>

$(function () {
$("#datepicker1").datepicker({ 

numberOfMonths: 1,
onSelect: function (selected) {
  var dt = new Date(selected);
   dt.setDate(dt.getDate() + 1);
   $("#datepicker").datepicker("option", "minDate", dt);
   },
   maxDate: new Date("{{ date('m/d/Y') }}")
});
$("#datepicker").datepicker({
      numberOfMonths: 1,
      onSelect: function (selected) {
        var dt = new Date(selected);
        dt.setDate(dt.getDate() - 1);
        $("#datepicker1").datepicker("option", "maxDate", dt);
      },
    });
});
</script>
<script>
		// $("#short_desc").html($('#short_desc').data('desc'));

	$('.short_desc_bid').each(function(){

		var str1 = $(this).data('desc');
		var bid_id = $(this).data('id');
		console.log( "SHORT : "  + bid_id);
		$(this).html(str1 + "..." + `<a href="javascript:;" class="read_more_bid" data-id="${bid_id}"> Read more +</a>`);
	});
	$('.full_desc_bid').each(function(){
		var str2 = $(this).data('desc');
		var bid_id = $(this).data('id');
		console.log( "FULL : "  + bid_id);
		$(this).html(str2 + `<a href="javascript:;" class="read_more_bid" data-id="${bid_id}"> Read less -</a>`);
	});

	// $('.read_more').click(function(){
	$("body").delegate(".read_more_bid", "click", function(e) {
		var id = $(this).data('id');
		console.log(id);
		if($('.short_desc_bid_'+id).css('display') == "block"){
			$('.short_desc_bid_'+id).hide();
			$('.full_desc_bid_'+id).show();
		} else {
			$('.full_desc_bid_'+id).hide();
			$('.short_desc_bid_'+id).show();
		}
	});
	
</script>
  @endsection
  @endsection
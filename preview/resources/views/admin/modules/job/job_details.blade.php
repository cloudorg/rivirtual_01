@extends('admin.layouts.app')

@section('title')
RiVirtual | Admin | Job Details
@endsection

@section('content')

@section('links')

@include('admin.includes.links')

<style>
  #profileimage img{

width:200px;
}
  .star_bx {

width: 60%;

}
.star_bx li {

display: inline-block;

color: #e9a107;

margin-left: 1px;

}



.star_bx .gray {

color: #9d9d9d;

}
</style>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65046120-1', 'auto');
  ga('send', 'pageview');
</script>

@endsection

@section('header')

@include('admin.includes.header')

@endsection

@section('sidebar')

@include('admin.includes.sidebar')

@endsection

<div class="content-page">
    <!-- Start content -->
    <div class="content">
      <div class="container">

        <!-- Page-Title -->
        <div class="row">
          <div class="col-sm-12">
            <h4 class="pull-left page-title"> Manage Jobs <i class="fa fa-chevron-right" aria-hidden="true"></i></h4>
            <a href="{{ route('admin.manage.job') }}" class="rm_new04"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
            <!--<a href="add_class_links.html" class="rm_new04"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Class Links</a>-->
            <!--<ol class="breadcrumb pull-right">
              <li><a href="#">Aariv School</a></li>
              <li><a href="#">Data Tables</a></li>
              <li class="active">Basic Tables</li>
            </ol>-->
          </div>
        </div>
        @include('admin.includes.message')
        <div class="row">
          <div class="col-md-12">


         <div class="tab_stylee">
          	<a href="{{ route('admin.job.details',['id'=>@$jobDetails->id]) }}" class="tab_btnn01 tab_active">Job Details</a>
            <a href="{{ route('admin.job.proposal',['id'=>@$jobDetails->id]) }}">View Proposal</a>
            {{--<a href="javascript:;">View Availability</a>
            <a href="javascript:;">View Visit Requests</a>--}}
          </div>

            <div class="panel panel-default panel-fill">

                    <div class="panel-body">

                        <!--<div class="about-info-p rm_new10">
                            <strong>Full Name :</strong>
                            <p class="text-muted">Mark Dicus</p>
                        </div>
                        <div class="about-info-p rm_new10">
                            <strong>User Name :</strong>
                            <p class="text-muted">Abc Username</p>
                        </div>
                        <div class="about-info-p rm_new10">
                            <strong>Email Address :</strong>
                            <p class="text-muted">testemail123@gmail.com</p>
                        </div>-->

                        <div class="agent_ppcc">
                            @if(@$job_image->image!= null)
                            <img src="{{ url('storage/app/public/service_image/'.@$job_image->image) }}" alt="">
                            @else
                            <img src="{{ url('public/admin/assets/images/avatar.png') }}" alt="">
                            @endif
                        </div>


                        <div class="boxx_002">

                        <div class="about-info-p rm_new08">
                            <strong>Job Name</strong>
                            <p class="text-muted"> : {{ @$jobDetails->job_name }}</p>
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>Job Start Date</strong>
                            <p class="text-muted"> : {{ date('m.d.Y',strtotime(@$jobDetails->job_start_date)) }}</p>
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>Job Start Time</strong>
                            <p class="text-muted"> : {{ date('H:i A',strtotime(@$jobDetails->time)) }} </p>
                        </div>
                        
                        <div class="about-info-p rm_new08">
                            <strong>Status</strong>
                             @if(@$jobDetails->status == 'A')
                            <p class="text-muted"> : Active</p>
                            @elseif(@$jobDetails->status == 'I')
                            <p class="text-muted"> : Inactive</p>
                            @endif
                        </div>



                        </div>

                        <div class="boxx_002">

                        <div class="about-info-p rm_new08">
                            <strong>Category</strong>
                            <p class="text-muted"> : {{ @$jobDetails->categoryName->category_name }}</p>
                        </div>
                        
                        <div class="about-info-p rm_new08">
                            <strong>Sub Category</strong>
                            <p class="text-muted"> : {{ @$jobDetails->subCategoryName->name }}</p>
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>Budget</strong>
                            <p class="text-muted"> : 
                            @if(@$jobDetails->budget_range_from != null)
                            ₹{{ @$jobDetails->budget_range_from }}
                              @else
                               --
                               @endif
                            </p>
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>Location</strong>
                            <p class="text-muted"> : {{ @$jobDetails->address }}</p>
                        </div>


                        {{--<div class="about-info-p rm_new08">
                            <strong>Gob Id</strong>
                            <p class="text-muted"> :

                             @if($provider->gov_id_image)

                             <img src="{{ url('storage/app/public/provider/gov_id_pics/'.$provider->gov_id_image) }}" alt="">

                             @else
                             <img src="{{ url('public/admin/assets/images/avatar-1.jpg') }}" alt="">
                             @endif

                            </p>
                        </div>--}}

                        {{--<div class="about-info-p rm_new08">
                            <strong>Certifications</strong>
                            <p class="text-muted"> :

                              @if($provider->certificate_image)

                             <img src="{{ url('storage/app/public/provider/certificate_pics/'.$provider->certificate_image) }}" alt="">

                             @else
                             <img src="{{ url('public/admin/assets/images/avatar-1.jpg') }}" alt="">
                             @endif

                            </p>
                        </div>--}}

                        {{--<div class="about-info-p rm_new08">
                            <strong>Experience</strong>
                            <p class="text-muted"> : {{ $provider->experience }}</p>
                        </div>--}}

                        {{--<div class="about-info-p rm_new08">
                            <strong>Website </strong>
                            <p class="text-muted"> : {{ $provider->website }}</p>
                        </div>--}}

                        </div>
                        <div class="">
                  <div class="about-info-p rm_new08">
                            <strong>Description</strong>
                            <p class="text-muted">:{!! @$jobDetails->description !!}</p>
                        </div>
                    </div>
                    </div>
                  
                </div>



          </div>
        </div>
        <!-- End row -->
        @if(@$review != null)
        <div class="row">
         <div class="col-md-12">
          <div class="panel panel-default panel-fill">
              <div class="panel-body">
              <div class="boxx_002">
                      <h3>Review Details</h3>
                      
                      <div class="agent_ppcc">
                            @if(@$review->userDetails->profile_pic != null)
                            <img src="{{ url('storage/app/public/profile_picture/'.@$review->userDetails->profile_pic) }}" alt="">
                            @else
                            <img src="{{ url('public/admin/assets/images/avatar.png') }}" alt="">
                            @endif
                        </div>
                       <div class="about-info-p rm_new08">
                            <strong>User Name</strong>
                            <p class="text-muted"> : {{ @$review->userDetails->name }}</p>
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>Review point</strong>
                            {{--<p class="text-muted">--}}

                            <div class="star_bx">
                                <ul>
                                  
                                @for($i=1; $i<=@$review->review_point; $i++)
                                 <li><i class="fa fa-star"></i></li>
                                 @endfor
                                @for($j=@$review->review_point+1; $j<=5; $j++)
                              <li><i class="fa fa-star gray"></i></li>
                              @endfor
                            
                              </ul>
		
									        	</div>
                            {{--</p>--}}
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>Review Date</strong>
                            <p class="text-muted"> : {{ date('m.d.Y',strtotime(@$review->created_at)) }}</p>
                        </div>
                     </div>
                     <div>
                     <div class="about-info-p rm_new08">
                            <strong>Description</strong>
                            <p class="text-muted">:{!! @$review->review_text !!}</p>
                        </div>
                    </div>
              </div>
          </div>
         </div>
        </div>
        @endif
        <!-- End row -->
      
        <div class="row">
         <div class="col-md-12">
          <div class="panel panel-default panel-fill">
              <div class="panel-body">
               @if(@$jobDetails->assistance_name != null && @$jobDetails->assistance_email)   
              <div class="boxx_002">
                      <h3>Rivirtual Assistance Details</h3>
                      
                    
                       <div class="about-info-p rm_new08">
                            <strong> Name</strong>
                            <p class="text-muted"> : {{ @$jobDetails->assistance_name }}</p>
                        </div>
                          
                        <div class="about-info-p rm_new08">
                            <strong>Email</strong>
                            <p class="text-muted"> : {{ @$jobDetails->assistance_email }}</p>
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>Phone</strong>
                            <p class="text-muted"> : {{ @$jobDetails->assistance_phone }}</p>
                        </div>
                     </div>
                     @else
                     <h3>User requested RiVirtual Assistance</h3>
                      @endif

                     <div>
                     
                    </div>
              </div>
          </div>
         </div>
        </div>
        <!-- End row -->

      </div>
      <!-- container -->

    </div>
    <!-- content -->

    @include('admin.includes.footer')
  </div>

  @section('scripts')

  @include('admin.includes.scripts')

  @endsection

  @endsection
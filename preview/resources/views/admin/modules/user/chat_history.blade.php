@extends('admin.layouts.app')

@section('title')
RiVirtual | Admin | Chat History
@endsection

@section('content')

@section('links')

@include('admin.includes.links')

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65046120-1', 'auto');
  ga('send', 'pageview');
</script>

@endsection

@section('header')

@include('admin.includes.header')

@endsection

@section('sidebar')

@include('admin.includes.sidebar')

@endsection

<div class="content-page"> 
    <!-- Start content -->
    <div class="content">
      <div class="container"> 
        
        <!-- Page-Title -->
        <div class="row">
          <div class="col-sm-12">
            <h4 class="pull-left page-title">Chat History<i class="fa fa-chevron-right" aria-hidden="true"></i>{{ @$user_name->name }}</h4>
            <a href="{{ route('admin.manage.user') }}" class="rm_new04"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
            <!--<a href="add_category.html" class="rm_new04"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Category </a>
            <ol class="breadcrumb pull-right">
              <li><a href="#">Aariv School</a></li>
              <li><a href="#">Data Tables</a></li>
              <li class="active">Basic Tables</li>
            </ol>-->
          </div>
        </div>
        @include('admin.includes.message')
        <div class="row">
          <div class="col-md-12">
          
          <div class="panel panel-default">
          {{--<div class="panel-heading rm02 rm04 rm_new01">
               <form role="form" action="{{ route('admin.manage.visit.request') }}" method="post">
                    @csrf
                    <div class="form-group">
                    <label for="">Keyword</label>
                    <input type="text" name="keyword" value="{{ @$key['keyword'] }}" id="" class="form-control" placeholder="client/agent name">
					        </div>
					<div class="form-group">
                    <label for="">Property Name</label>
                    <input type="text" name="property_name" value="{{ @$key['property_name'] }}" id="" class="form-control" placeholder="Enter name...">
					</div>
                    
                    <div class="form-group">
                    <label for="">Location</label>
                    <input type="text" name="location" value="{{ @$key['location'] }}" id="" class="form-control" placeholder="Enter location...">
					</div>
                    
                    <div class="form-group">
                    <label for="">Status</label>
                    <select class="form-control rm06" name="status">
                        <option value="">Select</option>
                        <option value="U" @if(Request::get('status') == 'U') selected @endif>Unverified</option>
                        <option value="A" @if(Request::get('status') == 'A') selected @endif>Active</option>
                        <option value="I" @if(Request::get('status') == 'I') selected @endif>Inactive</option>
                    </select>
                   </div>
                   
                   <div class="form-group rm_new14">
                    <label for="">From Date </label>
                    <input type="text" name="from_date" value="{{ @$key['from_date'] }}" id="datepicker1" class="datepicker form-control calander_icn" placeholder="Select" autocomplete="off">
                  </div>
                  <div class="form-group rm_new14">
                    <label for="">To Date</label>
                    <input type="text" name="to_date" value="{{ @$key['to_date'] }}" id="datepicker"  class="datepicker form-control calander_icn " placeholder="Select" autocomplete="off">
                  </div>

                  
                   
                  <!--
                <div class="form-group">
                    <label for="">Subject</label>
                    <select class="form-control rm06">
                        <option>Select</option>
                        <option>History</option>
                        <option>Geography</option>
                        <option>Mathematics</option>
                        <option>Life Science</option>
                        <option>Physical Science</option>
                        <option>English</option>
                    </select>
                </div>-->
                  
                  <div class="rm05">
                    <button class="btn btn-primary waves-effect waves-light w-md" type="submit">Search</button>
                    <a href="{{ route('admin.manage.visit.request') }}" class="btn btn-success waves-effect waves-light w-md">Reset</a>
                  </div>
                </form>
              </div>--}}
          </div>
          
          
            <div class="panel panel-default">
              
              
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="table-responsive">
                    <table class="table">
                        <thead>
                          <tr>
                            <th>User Name</th>
                           
                            {{--<th>Status</th>--}}
                            <th class="rm07">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                         @if(count(@$allUsers)>0) 
                         @foreach(@$allUsers as $user)
                          <tr>
                           <td>{{ @$user->getProvider->name }}</td>
                            
                            {{--<td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action" onclick="fun({{ @$vr->id }})"><img src="{{ url('public/admin/assets/images/action-dots.png') }}" alt=""></a>
                            <div class="show-actions" id="show-action{{ @$vr->id }}" style="display: none;">
                                <span class="angle"><img src="{{ url('public/admin/assets//images/angle.png') }}" alt=""></span>
                                <ul>
                                  <li><a data-toggle="modal" data-target="#myModal">Reschedule </a></li>
                                  
                                    <!--<li><a href="#">Edit</a></li>
                                    <li><a href="#">Unblock</a></li>-->
                                </ul>
                              </div>
                            </td>--}}
                            <td><a href="{{ route('admin.show.chat',['id'=>@$user->id]) }}">View Chat</a></td>
                          </tr>
                           @endforeach
                            @else
                          <tr role="row" class="odd">
                                        <td colspan="8" style="text-align:center;"> 
                                            No Data Found
                                        </td>
                                    </tr>
                        @endif
                          {{--<tr>
                            <td>Rabin Manna</td>
                            <td>Abhijeet Roy</td>
                            <td>18.10.2021</td>
                            <td>11:30 am</td>
                            <td>Mumbai</td>
                            <td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action2"><img src="{{ url('public/admin/assets/images/action-dots.png') }}" alt=""></a>
                            <div class="show-actions" id="show-action2" style="display: none;">
                                <span class="angle"><img src="{{ url('public/admin/assets//images/angle.png') }}" alt=""></span>
                                <ul>
                                    <li><a href="agent_details.html">Reschedule</a></li> 
                                    <li><a href="agent_details.html">Cancel</a></li>
                                    <!--<li><a href="#">Edit</a></li>
                                    <li><a href="#">Unblock</a></li>-->
                                </ul>
                              </div>
                            </td>
                          </tr>
                          
                          
                          <tr>
                            <td>Rabin Das</td>
                            <td>Abhijeet Roy</td>
                            <td>14.10.2021</td>
                            <td>10:20 am</td>
                            <td>Kolkata</td>
                            <td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action3"><img src="assets/images/action-dots.png" alt=""></a>
                            <div class="show-actions" id="show-action3" style="display: none;">
                                <span class="angle"><img src="assets//images/angle.png" alt=""></span>
                                <ul>
                                    <li><a href="agent_details.html">Cancel</a></li>
                                    <!--<li><a href="#">Edit</a></li>
                                    <li><a href="#">Unblock</a></li>-->
                                </ul>
                              </div>
                            </td>
                          </tr>
                          
                          <tr>
                            <td>Rabin Manna</td>
                            <td>Abhijeet Roy</td>
                            <td>18.10.2021</td>
                            <td>11:30 am</td>
                            <td>Mumbai</td>
                            <td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action4"><img src="assets/images/action-dots.png" alt=""></a>
                            <div class="show-actions" id="show-action4" style="display: none;">
                                <span class="angle"><img src="assets//images/angle.png" alt=""></span>
                                <ul>
                                    <li><a href="agent_details.html">Reschedule</a></li>
                                    <li><a href="agent_details.html">Cancel</a></li>
                                    <!--<li><a href="#">Edit</a></li>
                                    <li><a href="#">Unblock</a></li>-->
                                </ul>
                              </div>
                            </td>
                          </tr>--}}
                        
                      
                        </tbody>
                      </table>
                    </div>
                    {{--<div style="float: right;">{{@$visitRequest->appends(request()->except(['page', '_token']))->links()}}</div>--}}
                    
                    <!-- <ul class="pagination">
                      <li class="paginate_button previous disabled"><a href="#">Previous</a></li>
                      <li class="paginate_button active"><a href="#">1</a></li>
                      <li class="paginate_button"><a href="#">2</a></li>
                      <li class="paginate_button"><a href="#">3</a></li>
                      <li class="paginate_button"><a href="#">4</a></li>
                      <li class="paginate_button"><a href="#">5</a></li>
                      <li class="paginate_button"><a href="#">6</a></li>
                      <li class="paginate_button next"><a href="#">Next</a></li>
                    </ul> -->
                    
                    
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
        <!-- End row --> 
        
      </div>
      <!-- container --> 
      
    </div>
    <!-- content -->
    
    @include('admin.includes.footer')
  </div>

  @section('scripts')

  @include('admin.includes.scripts')
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script> -->
 
  @endsection

  @endsection
@extends('admin.layouts.app')

@section('title')
RiVirtual | Admin | User Details
@endsection

@section('content')

@section('links')

@include('admin.includes.links')

<style>

  #profileimage img{

       width:200px;
  }

</style>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65046120-1', 'auto');
  ga('send', 'pageview');
</script>

@endsection

@section('header')

@include('admin.includes.header')

@endsection

@section('sidebar')

@include('admin.includes.sidebar')

@endsection

<div class="content-page">
    <!-- Start content -->
    <div class="content">
      <div class="container">

        <!-- Page-Title -->
        <div class="row">
          <div class="col-sm-12">
            <h4 class="pull-left page-title"> Manage Users <i class="fa fa-chevron-right" aria-hidden="true"></i> {{ $user->name }} [{{ $user->user_id }}]</h4>
            <a href="{{ route('admin.manage.user') }}" class="rm_new04"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
            <!--<a href="add_class_links.html" class="rm_new04"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Class Links</a>-->
            <!--<ol class="breadcrumb pull-right">
              <li><a href="#">Aariv School</a></li>
              <li><a href="#">Data Tables</a></li>
              <li class="active">Basic Tables</li>
            </ol>-->
          </div>
        </div>
        @include('admin.includes.message')
        <div class="row">
          <div class="col-md-12">


         {{--<div class="tab_stylee">
          	<a href="javascript:;" class="tab_btnn01 tab_active">Profile</a>
            <a href="#">View Property</a>
            <a href="javascript:;">View Availability</a>
            <a href="javascript:;">View Visit Requests</a>
          </div>--}}

            <div class="panel panel-default panel-fill">

                    <div class="panel-body">

                        <!--<div class="about-info-p rm_new10">
                            <strong>Full Name :</strong>
                            <p class="text-muted">Mark Dicus</p>
                        </div>
                        <div class="about-info-p rm_new10">
                            <strong>User Name :</strong>
                            <p class="text-muted">Abc Username</p>
                        </div>
                        <div class="about-info-p rm_new10">
                            <strong>Email Address :</strong>
                            <p class="text-muted">testemail123@gmail.com</p>
                        </div>-->

                        <div class="agent_ppcc">
                            @if($user->profile_pic)
                            <img src="{{ url('storage/app/public/profile_picture/'.$user->profile_pic) }}" alt="">
                            @else
                            <img src="{{ url('public/admin/assets/images/avatar.png') }}" alt="">
                            @endif
                        </div>
                         <div class="about-info-p rm_new08">

                            <p class="text-muted">
                              @if(@$user->email_mob_change != null)
                              <a href="{{ route('user.email.mob.aproval',['id'=>@$user->id]) }}" class="btn btn-primary">Approved @if(@$user->email_mob_change == 'E') Email @elseif(@$user->email_mob_change == 'M') Mobile @elseif(@$user->email_mob_change == 'B') Email and Mobile @endif</a>
                               @endif
                            </p>
                        </div>


                        <div class="boxx_002">

                        <div class="about-info-p rm_new08">
                            <strong>Full Name</strong>
                            <p class="text-muted"> : {{ $user->name }}</p>
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>Email Address</strong>
                            <p class="text-muted"> : {{ $user->email }}</p>
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>Modified Email</strong>
                            <p class="text-muted"> :

                              @if(@$user->temp_email != null)
                                {{ @$user->temp_email }}
                                @endif
                            </p>
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>Phone</strong>
                            <p class="text-muted"> : +91 {{ $user->mobile_number }}</p>
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>Modified Phone</strong>
                            <p class="text-muted"> :

                              @if(@$user->temp_mobile != null)
                                {{ @$user->temp_mobile }}
                                @endif
                            </p>
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>Gender</strong>
                            <p class="text-muted"> :
                              @if($user->gender == 'M')
                              Male
                              @elseif($user->gender == 'F')
                              Female
                              @elseif($user->gender == 'O')
                              Others
                              @endif
                            </p>
                        </div>


                        {{--<div class="about-info-p rm_new08">
                            <strong>WhatsApp </strong>
                            <p class="text-muted"> : +91 {{ $user->whatsapp_no }}</p>
                        </div>--}}

                        <div class="about-info-p rm_new08">
                            <strong>Status</strong>
                             @if($user->status == 'A')
                            <p class="text-muted"> : Active</p>
                            @elseif($user->status == 'I')
                            <p class="text-muted"> : Inactive</p>
                            @elseif($user->status == 'U')
                            <p class="text-muted"> : Unverified</p>
                            @endif
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>Sign Up Date</strong>
                            <p class="text-muted"> : {{ date('m.d.Y',strtotime($user->created_at)) }}</p>
                        </div>


                        </div>

                        <div class="boxx_002">

                        <div class="about-info-p rm_new08">
                            <strong>Country</strong>
                            <p class="text-muted"> : {{ $country }}</p>
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>State</strong>
                            <p class="text-muted"> : {{ @$user->userState->name ? $user->userState->name: '--'}}</p>
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>City</strong>
                            <p class="text-muted"> : {{ @$user->userCity->name?$user->userCity->name:'--' }}</p>
                        </div>

                        {{--<div class="about-info-p rm_new08">
                            <strong>Address</strong>
                            <p class="text-muted"> : {{ $user->address }}</p>
                        </div>--}}


                        {{--<div class="about-info-p rm_new08">
                            <strong>Gob Id</strong>
                            <p class="text-muted"> :

                             @if($provider->gov_id_image)

                             <img src="{{ url('storage/app/public/provider/gov_id_pics/'.$provider->gov_id_image) }}" alt="">

                             @else
                             <img src="{{ url('public/admin/assets/images/avatar-1.jpg') }}" alt="">
                             @endif

                            </p>
                        </div>--}}

                        {{--<div class="about-info-p rm_new08">
                            <strong>Certifications</strong>
                            <p class="text-muted"> :

                              @if($provider->certificate_image)

                             <img src="{{ url('storage/app/public/provider/certificate_pics/'.$provider->certificate_image) }}" alt="">

                             @else
                             <img src="{{ url('public/admin/assets/images/avatar-1.jpg') }}" alt="">
                             @endif

                            </p>
                        </div>--}}

                        {{--<div class="about-info-p rm_new08">
                            <strong>Experience</strong>
                            <p class="text-muted"> : {{ $provider->experience }}</p>
                        </div>--}}

                        {{--<div class="about-info-p rm_new08">
                            <strong>Website </strong>
                            <p class="text-muted"> : {{ $provider->website }}</p>
                        </div>--}}

                        </div>


                        {{--<div class="about-info-p rm_new08">
                            <strong>About Me</strong>
                            <p class="text-muted">:{{ $provider->about }}</p>
                        </div>--}}

                    </div>
                </div>



          </div>
        </div>
        <!-- End row -->

      </div>
      <!-- container -->

    </div>
    <!-- content -->

    @include('admin.includes.footer')
  </div>

  @section('scripts')

  @include('admin.includes.scripts')

  @endsection

  @endsection

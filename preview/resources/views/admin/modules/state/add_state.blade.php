@extends('admin.layouts.app')

@section('title')
RiVirtual | Admin | Add State
@endsection

@section('content')

@section('links')

@include('admin.includes.links')

<style>
    label.error{

         color:red;
    }
</style>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65046120-1', 'auto');
  ga('send', 'pageview');
</script>

@endsection

@section('header')

@include('admin.includes.header')

@endsection

@section('sidebar')

@include('admin.includes.sidebar')

@endsection

<div class="content-page"> 
    <!-- Start content -->
    <div class="content">
      <div class="container"> 
        
        <!-- Page-Title -->
        <div class="row">
          <div class="col-sm-12">
            <h4 class="pull-left page-title">Add State</h4>
            <a href="{{ route('admin.manage.state') }}" class="rm_new04"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
            <!--<ol class="breadcrumb pull-right">
              <li><a href="#">Aariv School</a></li>
              <li><a href="#">Data Tables</a></li>
              <li class="active">Basic Tables</li>
            </ol>-->
          </div>
        </div>
        @include('admin.includes.message')
        <div class="row">
          <div class="col-md-12">
          
          
          
          
            <div class="panel panel-default">
              
              
              <div class="panel-body rm02 rm04"> 
            <form role="form" action="{{ route('admin.add.state') }}" method="post" id="addForm">
                @csrf
                <div class="col-md-6 m-b-15">
                <label for="">Country</label>
                    <select class="form-control rm06" name="country">
                    <option value="">Select Country</option>
                     @foreach($allCountry as $cnt)
                     <option value="{{ $cnt->id }}">{{ $cnt->name }}</option>
                    @endforeach
                  </select>
                </div>
                 <div class="col-md-6 m-b-15">
                    <label for="">State</label>
                    <input type="text" name="state" value="{{ old('state') }}" id="state" placeholder="Enter here" class="form-control">
                  </div>
                
                <div class="clearfix"></div>
                <!--<div class="form-group">
                    <label for="">Class</label>
                    <select class="form-control rm06">
                        <option>Select</option>
                        <option>5</option>
                        <option>6</option>
                        <option>7</option>
                        <option>8</option>
                        <option>9</option>
                        <option>10</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Subject</label>
                    <select class="form-control rm06">
                        <option>Select</option>
                        <option>History</option>
                        <option>Geography</option>
                        <option>Mathematics</option>
                        <option>Life Science</option>
                        <option>Physical Science</option>
                        <option>English</option>
                    </select>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <label for="">Deadline</label>
                    <input type="text" placeholder="Select" id="datepicker" class="form-control calander_icn">
                </div>
                
                <div class="clearfix"></div>
               <div class="form-group">
                   <label for="">Assignments File Upload (pdf)</label>
                    <div class="fileUpload btn btn-primary cust_file clearfix">
                        <span class="upld_txt"><i class="fa fa-upload upld-icon" aria-hidden="true"></i>Click here to Upload File</span>
                        <input type="file" class="upload">
                    </div> 
                </div>
                -->
                
                
                
                
                <div class="clearfix"></div>
                <div class="col-lg-12"> <button class="btn btn-primary waves-effect waves-light w-md rm_new15" type="submit">Save State</button></div>
            </form>
			</div>
            </div>
            
          </div>
        </div>
        <!-- End row --> 
        
      </div>
      <!-- container --> 
      
    </div>
    <!-- content -->
    
    @include('admin.includes.footer')
  </div>

  @section('scripts')

  @include('admin.includes.scripts')

  <script>
      $(document).ready(function(){

           $('#addForm').validate({

                 rules: {

                      state: {

                            required: true,
                            

                      },
                      country: {

                           required: true,
                      }
                 },

           });  
      });
  </script>

  @endsection
  @endsection
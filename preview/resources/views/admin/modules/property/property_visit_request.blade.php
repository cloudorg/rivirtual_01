@extends('admin.layouts.app')

@section('title')
RiVirtual | Admin |Property Visit Request
@endsection

@section('content')

@section('links')

@include('admin.includes.links')

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65046120-1', 'auto');
  ga('send', 'pageview');
</script>

@endsection

@section('header')

@include('admin.includes.header')

@endsection

@section('sidebar')

@include('admin.includes.sidebar')

@endsection

<div class="content-page"> 
    <!-- Start content -->
    <div class="content">
      <div class="container"> 
        
        <!-- Page-Title -->
        <div class="row">
          <div class="col-sm-12">
            <h4 class="pull-left page-title"> Property Details <i class="fa fa-chevron-right" aria-hidden="true"></i></h4>
            <a href="{{ route('admin.manage.property') }}" class="rm_new04"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
            <!--<a href="add_class_links.html" class="rm_new04"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Class Links</a>-->
            <!--<ol class="breadcrumb pull-right">
              <li><a href="#">Aariv School</a></li>
              <li><a href="#">Data Tables</a></li>
              <li class="active">Basic Tables</li>
            </ol>-->
          </div>
        </div>
        
        <div class="row">
          <div class="col-md-12">
          
          
          <div class="tab_stylee">
          	<a href="{{ route('admin.property.details',['id'=>@$propertyId]) }}">Property Details</a>
            
            <a href="{{ route('admin.property.visit.request',['id'=>@$propertyId]) }}" class="tab_active">View Visit Requests</a>
            <a href="{{ route('admin.property.review',['id'=>@$propertyId]) }}">View Rating</a>
          </div>
          <div class="panel panel-default">
          <div class="panel-heading rm02 rm04 rm_new01">
                <form role="form" action="{{ route('admin.property.visit.request',['id'=>@$propertyId]) }}" method="post">
                    @csrf
                  
					<div class="form-group">
                    <label for="">Client Name</label>
                    <input type="text" name="client_name" value="{{ @$key['client_name'] }}" id="" class="form-control" placeholder="Enter Name...">
					</div>
                    <div class="form-group">
                    <label for="">Location</label>
                    <input type="text" name="location" value="{{ @$key['location'] }}" id="" class="form-control" placeholder="Enter Location...">
					</div>
          <div class="form-group">
                    <label for="">From Date </label>
                    <input type="text" name="from_date" value="{{ @$key['from_date'] }}" id="datepicker3" class="datepicker form-control calander_icn hasDatepicker" placeholder="Select" autocomplete="off">
                  </div>
                  <div class="form-group">
                    <label for="">To Date</label>
                    <input type="text" name="to_date" value="{{ @$key['to_date'] }}"  class="datepicker form-control calander_icn hasDatepicker" placeholder="Select" autocomplete="off">
                  </div>
                    {{--<div class="form-group">
                    <label for="">Status</label>
                    <select class="form-control rm06" name="status">
                        <option value="">Select</option>
                        <option value="S" @if(Request::get('status') == 'S') selected @endif>Sold</option>
                        <option value="A" @if(Request::get('status') == 'A') selected @endif>Active</option>
                        <option value="B" @if(Request::get('status') == 'B') selected @endif>Inactive</option>
                    </select>
                   </div>--}}
                   
                  <!--
                <div class="form-group">
                    <label for="">Subject</label>
                    <select class="form-control rm06">
                        <option>Select</option>
                        <option>History</option>
                        <option>Geography</option>
                        <option>Mathematics</option>
                        <option>Life Science</option>
                        <option>Physical Science</option>
                        <option>English</option>
                    </select>
                </div>-->
                  
                  <div class="rm05">
                    <button class="btn btn-primary waves-effect waves-light w-md" type="submit">Search</button>
                    <a href="{{ route('admin.property.visit.request',['id'=>@$propertyId]) }}" class="btn btn-success waves-effect waves-light w-md">Reset</a>
                  </div>
                </form>
              </div>
          </div>
            <div class="panel panel-default panel-fill">
            
                    <div class="panel-body"> 
                        
                        <div class="table-responsive">
                      <table class="table">
                        <thead>
                          <tr>
                            <th>Property Name</th>
                            <th>Client's Name</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Location</th>
                            {{--<th>Status</th>--}}
                            {{--<th class="rm07">Action</th>--}}
                          </tr>
                        </thead>
                        <tbody>
                          @if(count(@$visitRequest)>0)
                           @foreach($visitRequest as $vr)
                          <tr>
                            <td>{{ @$vr->propertyDetails->name }}</td>
                            <td>{{ @$vr->userDetails->name }}</td>
                            <td>{{ date('m/d/Y',strtotime($vr->visit_date)) }}</td>
                            <td>{{ date('h:i A',strtotime(@$vr->visit_date)) }}</td>
                            <td>{{ @$vr->address }}</td>
                            
                            {{--<td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action" onclick="fun({{ @$vr->id }})"><img src="{{ url('public/admin/assets/images/action-dots.png') }}" alt=""></a>
                            <div class="show-actions" id="show-action{{ @$vr->id }}" style="display: none;">
                                <span class="angle"><img src="{{ url('public/admin/assets//images/angle.png') }}" alt=""></span>
                                <ul>
                                  <li><a data-toggle="modal" data-target="#myModal">Reschedule </a></li>
                                  
                                    <!--<li><a href="#">Edit</a></li>
                                    <li><a href="#">Unblock</a></li>-->
                                </ul>
                              </div>
                            </td>--}}
                          </tr>
                          @endforeach
                           @else
                           <tr role="row" class="odd">
                                        <td colspan="8" style="text-align:center;"> 
                                            No Data Found
                                        </td>
                                    </tr>
                            @endif
                          {{--<tr>
                            <td>Rabin Manna</td>
                            <td>Abhijeet Roy</td>
                            <td>18.10.2021</td>
                            <td>11:30 am</td>
                            <td>Mumbai</td>
                            <td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action2"><img src="{{ url('public/admin/assets/images/action-dots.png') }}" alt=""></a>
                            <div class="show-actions" id="show-action2" style="display: none;">
                                <span class="angle"><img src="{{ url('public/admin/assets//images/angle.png') }}" alt=""></span>
                                <ul>
                                    <li><a href="agent_details.html">Reschedule</a></li> 
                                    <li><a href="agent_details.html">Cancel</a></li>
                                    <!--<li><a href="#">Edit</a></li>
                                    <li><a href="#">Unblock</a></li>-->
                                </ul>
                              </div>
                            </td>
                          </tr>
                          
                          
                          <tr>
                            <td>Rabin Das</td>
                            <td>Abhijeet Roy</td>
                            <td>14.10.2021</td>
                            <td>10:20 am</td>
                            <td>Kolkata</td>
                            <td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action3"><img src="assets/images/action-dots.png" alt=""></a>
                            <div class="show-actions" id="show-action3" style="display: none;">
                                <span class="angle"><img src="assets//images/angle.png" alt=""></span>
                                <ul>
                                    <li><a href="agent_details.html">Cancel</a></li>
                                    <!--<li><a href="#">Edit</a></li>
                                    <li><a href="#">Unblock</a></li>-->
                                </ul>
                              </div>
                            </td>
                          </tr>
                          
                          <tr>
                            <td>Rabin Manna</td>
                            <td>Abhijeet Roy</td>
                            <td>18.10.2021</td>
                            <td>11:30 am</td>
                            <td>Mumbai</td>
                            <td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action4"><img src="assets/images/action-dots.png" alt=""></a>
                            <div class="show-actions" id="show-action4" style="display: none;">
                                <span class="angle"><img src="assets//images/angle.png" alt=""></span>
                                <ul>
                                    <li><a href="agent_details.html">Reschedule</a></li>
                                    <li><a href="agent_details.html">Cancel</a></li>
                                    <!--<li><a href="#">Edit</a></li>
                                    <li><a href="#">Unblock</a></li>-->
                                </ul>
                              </div>
                            </td>
                          </tr>--}}
                        
                      
                        </tbody>
                      </table>
                    </div>
                    
                    <div style="float: right;">{{$visitRequest->links()}}</div>
                    
                        
                    </div> 
                </div>
          
            
            
          </div>
        </div>
        <!-- End row --> 
        
      </div>
      <!-- container --> 
      
    </div>
    <!-- content -->
    
    @include('admin.includes.footer')
  </div>
  <div class="modal res-modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Reschedule Property Visits</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <form>
                                        <div class="appome_input das_input">
                                            <label>Alternative Date:</label>
                                            <div class="dash-d">
                                            <input type="text" placeholder="Select Date" id="datepicker3">
                                            <span class="over_llp1"><img src="{{ url('public/admin/assets/images/cala.png') }}"></span>
                                            </div>
                                        </div>
                                        <div class="appome_input das_input">
                                            <label>Select Time</label>
                                            <div class="dash-d">
                                            <input type="text" placeholder="Select Time" name="time">
                                            <span class="over_llp1"><img src="{{ url('public/admin/assets/images/clock.png') }}"></span>
                                            </div>
                                        </div>
                                        
                                    </form>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Save</button>
        </div>
        
      </div>
    </div>
  </div>
  @section('scripts')

  @include('admin.includes.scripts')

  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

  <script>
  function fun(id){

$('.show-actions').slideUp();
 $("#show-action"+id).show();
}
</script>

<script>

$(function () {
$(".datepicker").datepicker({ 
autoclose: true, 
todayHighlight: true
});
});


</script>
  @endsection
  @endsection
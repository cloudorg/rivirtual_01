@extends('admin.layouts.app')

@section('title')
RiVirtual | Admin | Edit Property
@endsection

@section('content')

@section('links')

@include('admin.includes.links')

<style>
    label.error{

         color:red;
    }

    .uplodimg_pick img {
    width: 50px;
    height: 50px;
    object-fit: cover;
    border-radius: 100%;
}
.dash-list-agent .upimg {
    display: inline-block;
    position: relative;
    width: 118px;
    height: 111px;
    border-radius: 3px;
    background: #cccccc42;
    margin-right: 20px;
}
</style>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65046120-1', 'auto');
  ga('send', 'pageview');
</script>
<link href="{{ URL::asset('public/frontend/croppie/croppie.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('public/frontend/croppie/croppie.min.css') }}" rel="stylesheet" />
@endsection

@section('header')

@include('admin.includes.header')

@endsection

@section('sidebar')

@include('admin.includes.sidebar')

@endsection


<div class="content-page"> 
    <!-- Start content -->
    <div class="content">
      <div class="container"> 
        
        <!-- Page-Title -->
        <div class="row">
          <div class="col-sm-12">
            <h4 class="pull-left page-title">Edit Property</h4>
            <a href="{{ route('admin.manage.property') }}" class="rm_new04"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
            <!--<ol class="breadcrumb pull-right">
              <li><a href="#">Aariv School</a></li>
              <li><a href="#">Data Tables</a></li>
              <li class="active">Basic Tables</li>
            </ol>-->
          </div>
        </div>
        @include('admin.includes.message')
        <div class="row">
          <div class="col-md-12">
          
           
          <div class="tab_stylee">
          	<a @if($propertyDetails->status=='I') href="javascript:;" @else href="{{route('admin.edit.property',['id'=>$propertyDetails->id])}}" @endif >Property Details</a>
            <a class="tab_btnn01 tab_active" @if($propertyDetails->status=='I') href="{{route('admin.edit.property.image',['id'=>$propertyDetails->id])}}" @else href="{{route('agent.add.property.image',['id'=>$propertyDetails->id])}}" @endif>Property Images</a>
          </div>
          
          
            <div class="panel panel-default">
              
              
              <div class="panel-body rm02 rm04"> 
            <form role="form" action="{{ route('admin.edit.property.image.save') }}" method="post" enctype="multipart/form-data" id="addPropertyForm">
                @csrf
                <input type="hidden" name="propertyId" id="" value="{{ @$propertyDetails->id }}">
                           
                <div class="agent-right-body">
				<div class="row">
					@if(@$propertyDetails->property_type!='L' && @$propertyDetails->property_type!='FRMH')
					<div class="col-md-12">
						<div class="redio_bx prop">
								<span>Facilities / Amenities  :</span>
								<div class="diiferent-sec">
								<ul class="category-ul new-aget-pro">
								@foreach ($allFacilitiesAmenities as $data)
								<li>
									<div class="radiobx">
										<input type="checkbox" id="rate{{@$data->id}}" name="facilities_amenities[]" value="{{@$data->id}}" {{@in_array(@$data->id, @$facilitiesAmenities) ?'checked':''}}>
										<label for="rate{{@$data->id}}">{{@$data->name}}</label>
									</div>
								</li>
								@endforeach
									{{--<li>
										<div class="radiobx">
											<input type="checkbox" id="rate2" name="radios" value="all">
											<label for="rate2">Amenities/Facilities1</label>
										</div>
									</li>
									<li>
										<div class="radiobx">
											<input type="checkbox" id="rate3" name="radios" value="all">
											<label for="rate3">Available Car Parking Area</label>
										</div>
									</li>
									<li>
										<div class="radiobx">
											<input type="checkbox" id="rate4" name="radios" value="all">
											<label for="rate4">Open balcony</label>
										</div>
									</li>
									<li>
										<div class="radiobx">
											<input type="checkbox" id="rate5" name="radios" value="all">
											<label for="rate5">Air Conditioning</label>
										</div>
									</li>
									<li>
										<div class="radiobx">
											<input type="checkbox" id="rate6" name="radios" value="all">
											<label for="rate6">Car Parking Area</label>
										</div>
									</li>
									<li>
										<div class="radiobx">
											<input type="checkbox" id="rate7" name="radios" value="all">
											<label for="rate7">Air Conditioning</label>
										</div>
									</li>
									<li>
										<div class="radiobx">
											<input type="checkbox" id="rate8" name="radios" value="all">
											<label for="rate8">Gym Center</label>
										</div>
									</li>
									<li>
										<div class="radiobx">
											<input type="checkbox" id="rate9" name="radios" value="all">
											<label for="rate9">Alarm</label>
										</div>

									</li>
									<li>
										<div class="radiobx">
											<input type="checkbox" id="rate10" name="radios" value="all">
											<label for="rate10">Car Parking Area</label>
										</div>
									</li>
									<li>
										<div class="radiobx">
											<input type="checkbox" id="rate11" name="radios" value="all">
											<label for="rate11">Central Heating</label>
										</div>
									</li>
									<li>
										<div class="radiobx">
											<input type="checkbox" id="rate12" name="radios" value="all">
											<label for="rate12">Spa & Massage</label>
										</div>
									</li>
									<li>
										<div class="radiobx">
											<input type="checkbox" id="rate13" name="radios" value="all">
											<label for="rate13">Window Covering</label>
										</div>
									</li>
									<li>
										<div class="radiobx">
											<input type="checkbox" id="rate14" name="radios" value="all">
											<label for="rate14">Air Conditioning</label>
										</div>
									</li>--}}
								</ul>
							</div>
							<label id="facilities_amenities[]-error" class="error" for="facilities_amenities[]" style="display: none"></label>
						</div>
					</div>

					<div class="col-md-12">
						<div class="bodar"></div>
					</div>
					@endif
					@if(@$propertyDetails->property_type!='L' && @$propertyDetails->property_type!='FRMH')
					<div class="col-sm-12">
						<div class="uplodimg uplodimg_cc1">
							<div class="uplodimgfil">
								<b>Exterior Photos</b>
								<input type="file" name="file-1[]" id="file-1" class="inputfile inputfile-1 @if($propertyDetails->status=='I') required @endif" data-multiple-caption="{count} files selected" onchange="fun1()" multiple="" accept="image/*">
								<label for="file-1">Upload Photos <img src="{{ url('public/admin/assets/images/clickhe.png') }}" alt=""></label>
								<label id="file-1-error" class="error" for="file-1" style="display: none"></label>
							</div>	
						</div>
						<div class="dash-list-agent" id="file-1-upoade">
						<ul class="edit-gallery" id="file-1-ul">

							<div class="clearfix"></div>
						</ul>
					      </div>
						@if($propertyDetails->status!='I')
						<div class="dash-list-agent">

							<ul class="edit-gallery">
								@foreach ($propertyImageExterior as $imageExterior)
								<li>
									<div class="upimg">
										<img src="{{ URL::to('storage/app/public/property_image')}}/{{$imageExterior->image}}">
										<a href="{{route('admin.edit.property.image.remove',['id'=>$imageExterior->id])}}"><img src="{{ url('public/admin/assets/images/w-cross.png') }}"></a>
									</div>
								</li>
								@endforeach

								<div class="clearfix"></div>
							</ul>
						</div>
						@endif
							
						</div>
						<div class="col-md-12">
						<div class="bodar"></div>
					</div>
					@endif
					@if(@$propertyDetails->property_type!='L' && @$propertyDetails->property_type!='FRMH')
					<div class="col-sm-12">
							<div class="uplodimg uplodimg_cc1">	
								<div class="uplodimgfil">
									<b>Interior photos</b>
									<input type="file" name="file-2[]" id="file-2" class="inputfile inputfile-1 @if($propertyDetails->status=='I') required @endif" data-multiple-caption="{count} files selected" onchange="fun2()" multiple="" accept="image/*">
									<label for="file-2">Upload Photos <img src="{{ url('public/admin/assets/images/clickhe.png') }}" alt=""></label>
									<label id="file-2-error" class="error" for="file-2" style="display: none"></label>
								</div>
							</div>
							<div class="dash-list-agent" id="file-2-upoade">
								<ul class="edit-gallery" id="file-2-ul">

									<div class="clearfix"></div>
								</ul>
							</div>

							@if($propertyDetails->status!='I')
							<div class="dash-list-agent">

								<ul class="edit-gallery">
									@foreach ($propertyImageInterior as $imageInterior)
									<li>
										<div class="upimg">
											<img src="{{ URL::to('storage/app/public/property_image')}}/{{$imageInterior->image}}">
											<a href="{{route('admin.edit.property.image.remove',['id'=>$imageInterior->id])}}"><img src="{{ url('public/admin/assets/images/w-cross.png') }}"></a>
										</div>
									</li>
									@endforeach
									<div class="clearfix"></div>
								</ul>
							</div>
							@endif
						</div>
						<div class="col-md-12">
						<div class="bodar"></div>
					</div>
					@endif
					<div class="col-sm-12">
							<div class="uplodimg uplodimg_cc1">
								<div class="uplodimgfil">
									<b>Images of floor plan </b>
									<input type="file" name="file-3[]" id="file-3" class="inputfile inputfile-1 @if($propertyDetails->property_type=='L') required @endif" data-multiple-caption="{count} files selected" onchange="fun3()" multiple="" accept="image/*">
									<label for="file-3">Upload Photos <img src="{{ url('public/admin/assets/images/clickhe.png') }}" alt=""></label>
									<label id="file-3-error" class="error" for="file-3" style="display: none"></label>
								</div>	
							</div>
							<div class="dash-list-agent" id="file-3-upoade">
								<ul class="edit-gallery" id="file-3-ul">

									<div class="clearfix"></div>
								</ul>
							</div>

							@if($propertyDetails->status!='I')
							<div class="dash-list-agent">

								<ul class="edit-gallery">
									@foreach ($propertyImageFloor as $imageFloor)
									<li>
										<div class="upimg">
											<img src="{{ URL::to('storage/app/public/property_image')}}/{{$imageFloor->image}}">
											<a href="{{route('admin.edit.property.image.remove',['id'=>$imageFloor->id])}}"><img src="{{ url('public/admin/assets/images/w-cross.png') }}"></a>
										</div>
									</li>
									@endforeach

									<div class="clearfix"></div>
								</ul>
							</div>
							@endif
						</div>	

						<div class="col-md-12">
							<div class="bodar">
							</div>
						</div>
						<div class="col-md-12">
							<div class="usei_sub text-right">
			                <button class="btn btn-primary waves-effect waves-light w-md rm_new15" type="submit">Save & Continue</button>
							</div>
						</div>
				</div>
</div>


        
            </form>
			</div>
            </div>
            
          </div>
        </div>
        <!-- End row --> 
        
      </div>
      <!-- container --> 
      
    </div>
    <!-- content -->

    <!-- <div class="modal" tabindex="-1" role="dialog" id="croppie-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Crop Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="croppie-div" style="width: 100%;"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="crop-img">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> -->
    
    @include('admin.includes.footer')
  </div>

  @section('scripts')

  @include('admin.includes.scripts')

  <script>
    function fun3(){
        console.log(document.getElementById('file-3').files);
        var html='';
        for (i = 0; i < document.getElementById('file-3').files.length; ++i) {
            console.log(i)
            var ii=document.getElementById('file-3').files[i];
            var b=URL.createObjectURL(ii);
            html = html+`<li><div class="upimg"><img src="`+b+`"></div></li>`;
        }
        $('#file-3-ul').html(html);
        console.log(html)
    }
    function fun2(){
        console.log(document.getElementById('file-2').files);
        var html='';
        for (i = 0; i < document.getElementById('file-2').files.length; ++i) {
            console.log(i)
            var ii=document.getElementById('file-2').files[i];
            var b=URL.createObjectURL(ii);
            html = html+`<li><div class="upimg"><img src="`+b+`"></div></li>`;
        }
        $('#file-2-ul').html(html);
        console.log(html)
    }
    function fun1(){
        console.log(document.getElementById('file-1').files);
        var html='';
        for (i = 0; i < document.getElementById('file-1').files.length; ++i) {
            console.log(i)
            var ii=document.getElementById('file-1').files[i];
            var b=URL.createObjectURL(ii);
            html = html+`<li><div class="upimg"><img src="`+b+`"></div></li>`;
        }
        $('#file-1-ul').html(html);
        console.log(html)
    }
</script>
<script>
    $(document).ready(function(){
        $('#addPropertyForm').validate({
            rules: {
               'facilities_amenities[]':{
                    required:true,
                },
                'file-3[]':{
                    extension: "png|jpe?g",
                },
               'file-2[]':{
                   extension: "png|jpe?g",
                },
               'file-1[]':{
                   extension: "png|jpe?g",
                },
            },
            messages: {
                'facilities_amenities[]':{
                    required:'Minimun one facilities or amenities required',
                },
                'file-3[]':{
                    required:'Images of floor plan required',
                    extension: "File must be JPG or PNG",
                },
               'file-2[]':{
                    required:'Interior photos required',
                    extension: "File must be JPG or PNG",
                },
               'file-1[]':{
                    required:'Exterior Photos required',
                    extension: "File must be JPG or PNG",
                },
            },
            ignore: [],
        });
    });
</script>

  @endsection
  @endsection
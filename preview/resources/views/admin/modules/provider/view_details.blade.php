@extends('admin.layouts.app')

@section('title')
RiVirtual | Admin | Pro Details
@endsection

@section('content')

@section('links')

@include('admin.includes.links')

<style>

  #profileimage img{

       width:200px;
  }

</style>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65046120-1', 'auto');
  ga('send', 'pageview');
</script>

@endsection

@section('header')

@include('admin.includes.header')

@endsection

@section('sidebar')

@include('admin.includes.sidebar')

@endsection

<div class="content-page">
    <!-- Start content -->
    <div class="content">
      <div class="container">

        <!-- Page-Title -->
        <div class="row">
          <div class="col-sm-12">
            <h4 class="pull-left page-title"> Manage Pro <i class="fa fa-chevron-right" aria-hidden="true"></i> {{ $provider->name }} [{{ $provider->user_id }}]</h4>
            <a href="{{ route('admin.manage.provider') }}" class="rm_new04"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
            <!--<a href="add_class_links.html" class="rm_new04"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Class Links</a>-->
            <!--<ol class="breadcrumb pull-right">
              <li><a href="#">Aariv School</a></li>
              <li><a href="#">Data Tables</a></li>
              <li class="active">Basic Tables</li>
            </ol>-->
          </div>
        </div>
        @include('admin.includes.message')
        <div class="row">
          <div class="col-md-12">


         {{--<div class="tab_stylee">
          	<a href="javascript:;" class="tab_btnn01 tab_active">Profile</a>
            <a href="#">View Property</a>
            <a href="javascript:;">View Availability</a>
            <a href="javascript:;">View Visit Requests</a>
          </div>--}}

            <div class="panel panel-default panel-fill">

                    <div class="panel-body">

                        <!--<div class="about-info-p rm_new10">
                            <strong>Full Name :</strong>
                            <p class="text-muted">Mark Dicus</p>
                        </div>
                        <div class="about-info-p rm_new10">
                            <strong>User Name :</strong>
                            <p class="text-muted">Abc Username</p>
                        </div>
                        <div class="about-info-p rm_new10">
                            <strong>Email Address :</strong>
                            <p class="text-muted">testemail123@gmail.com</p>
                        </div>-->

                        <div class="agent_ppcc">
                            @if($provider->profile_pic)
                            <img src="{{ url('storage/app/public/profile_picture/'.$provider->profile_pic) }}" alt="">
                            @else
                            <img src="{{ url('public/admin/assets/images/avatar.png') }}" alt="">
                            @endif
                        </div>
                         <div class="about-info-p rm_new08">

                            <p class="text-muted">
                              @if(@$provider->email_mob_change != null)
                              <a href="{{ route('user.email.mob.aproval',['id'=>@$provider->id]) }}" class="btn btn-primary">Approved @if(@$provider->email_mob_change == 'E') Email @elseif(@$provider->email_mob_change == 'M') Mobile @elseif(@$provider->email_mob_change == 'B') Email and Mobile @endif</a>
                               @endif
                            </p>
                        </div>

                        <div class="boxx_002">

                        <div class="about-info-p rm_new08">
                            <strong>Full Name</strong>
                            <p class="text-muted"> : {{ $provider->name }}</p>
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>Email Address</strong>
                            <p class="text-muted"> : {{ $provider->email }}</p>
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>Modified Email</strong>
                            <p class="text-muted"> :

                              @if(@$provider->temp_email != null)
                                {{ @$provider->temp_email }}
                                @endif
                            </p>
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>Phone</strong>
                            <p class="text-muted"> : +91 {{ $provider->mobile_number }}</p>
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>Modified Phone</strong>
                            <p class="text-muted"> :

                              @if(@$provider->temp_mobile != null)
                                {{ @$provider->temp_mobile }}
                                @endif
                            </p>
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>WhatsApp </strong>
                            <p class="text-muted"> :
                             @if($provider->whatsapp_no != null)
                            +91 {{ $provider->whatsapp_no }}
                            @else
                            @endif
                          </p>
                        </div>


                        <div class="about-info-p rm_new08">
                            <strong>Status</strong>
                             @if($provider->status == 'A')
                            <p class="text-muted"> : Active</p>
                            @elseif($provider->status == 'I')
                            <p class="text-muted"> : Inactive</p>
                            @elseif($provider->status == 'U')
                            <p class="text-muted"> : Unverified</p>
                            @endif
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>Sign Up Date</strong>
                            <p class="text-muted"> : {{ date('m.d.Y',strtotime($provider->singup_date)) }}</p>
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>Language Known</strong>
                             @if($language)
                                @php
                                 $i = count($language)-1;
                                @endphp
                            <p class="text-muted"> :
                            @foreach($language as $key=>$lan)

                            <span>{{ $lan->userLanguage->name }}</span>@if($i == $key) @else , @endif
                              @endforeach
                            </p>
                            @endif
                        </div>

                        </div>

                        <div class="boxx_002">

                        <div class="about-info-p rm_new08">
                            <strong>Country</strong>
                            <p class="text-muted"> : {{ $country }}</p>
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>State</strong>
                            <p class="text-muted"> : {{@$provider->userState->name? $provider->userState->name:'--' }}</p>
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>City</strong>
                            <p class="text-muted"> : {{ @$provider->userCity->name?$provider->userCity->name:'--' }}</p>
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>Address</strong>
                            <p class="text-muted"> : {{ $provider->address }}</p>
                        </div>


                        <div class="about-info-p rm_new08">
                            <strong>Experience (Year)</strong>
                            <p class="text-muted"> : {{ $provider->experience }}</p>
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>Skills</strong>
                             @if($skills)
                                @php
                                 $i = count($skills)-1;
                                @endphp
                            <p class="text-muted"> :
                            @foreach($skills as $key=>$skill)

                            <span>{{ $skill->userSkill->skill_name }}</span>@if($i == $key) @else , @endif
                              @endforeach
                            </p>
                            @endif
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>Website </strong>
                            <p class="text-muted"> : {{ $provider->website }}</p>
                        </div>
                        
                        <div class="about-info-p rm_new08">
                            <strong>Category</strong>
                            <p class="text-muted"> : {{ @$provider->proToCategory?@$provider->proToCategory->categoryName->category_name:'--' }}</p>
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>Sub Category</strong>
                            @if($mySubCategory)
                                @php
                                 $i = count($mySubCategory)-1;
                                @endphp
                            <p class="text-muted"> :
                            @foreach($mySubCategory as $key=>$subcat)

                            <span>{{ $subcat->subCategoryName->name }}</span>@if($i == $key) @else , @endif
                              @endforeach
                            </p>
                            @endif
                        </div>

                        </div>


                        <div class="about-info-p rm_new08">
                            <strong>About Me</strong>
                            <p class="text-muted">:{{ $provider->about }}</p>
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>Gov Id</strong>
                            <p class="text-muted"> :

                             @if($provider->gov_id_image)

                             <a href="{{ url('storage/app/public/gov_id_image/'.$provider->gov_id_image) }}"
                             style="color: #95bb95; font-size: 17px;" target="_blank" title="view gov ID">
                             <i class="fa fa-eye "></i>
                            </a>

                             @else
                             <span style="color:red;">No gov ID uploaded here</span>
                             @endif

                            </p>
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>Certification</strong>
                            <p class="text-muted"> :

                              @if($provider->certificate_image)

                              <a href="{{ url('storage/app/public/certificate_image/'.$provider->certificate_image) }}"
                             style="color: #95bb95; font-size: 17px;" target="_blank" title="view certificate">
                             <i class="fa fa-eye "></i>
                            </a>

                             @else
                             <span style="color:red;">No Certificate file uploaded here</span>
                             @endif

                            </p>
                        </div>

                    </div>
                    <div class="boxx_002">
                      <h3>Work Images</h3>
                      <div class="dash-listnew">
                                    <ul class="edit-gallerynew">
                                    @foreach ($images as $image)
                                     <li>
                                    <div class="upimg">

                                        <img src="{{ URL::to('storage/app/public/work_image')}}/{{$image->image}}" alt="">

                                    </div>
                                    </li>
                                @endforeach

                                       <div class="clearfix"></div>
                                    </ul>
                                 </div>
                    </div>
                </div>



          </div>
        </div>
        <!-- End row -->

      </div>
      <!-- container -->

    </div>
    <!-- content -->

    @include('admin.includes.footer')
  </div>

  @section('scripts')

  @include('admin.includes.scripts')

  @endsection

  @endsection

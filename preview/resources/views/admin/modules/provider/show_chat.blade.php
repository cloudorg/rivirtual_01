@extends('admin.layouts.app')

@section('title')
RiVirtual | Admin | Show Chat
@endsection

@section('content')

@section('links')

@include('admin.includes.links')

<style>

  #profileimage img{

       width:200px;
  }

</style>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65046120-1', 'auto');
  ga('send', 'pageview');
</script>

@endsection

@section('header')

@include('admin.includes.header')

@endsection

@section('sidebar')

@include('admin.includes.sidebar')

@endsection

<div class="content-page">
    <!-- Start content -->
    <div class="content">
      <div class="container">

        <!-- Page-Title -->
        <div class="row">
          <div class="col-sm-12">
            <h4 class="pull-left page-title">Show Messages<i class="fa fa-chevron-right" aria-hidden="true"></i></h4>
           <a href="{{ route('admin.pro.chat.history',['id'=>@$userId->provider_id]) }}" class="rm_new04"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
            <!--<a href="add_class_links.html" class="rm_new04"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Class Links</a>-->
            <!--<ol class="breadcrumb pull-right">
              <li><a href="#">Aariv School</a></li>
              <li><a href="#">Data Tables</a></li>
              <li class="active">Basic Tables</li>
            </ol>-->
          </div>
        </div>
        @include('admin.includes.message')
        <div class="row">
          <div class="col-md-12">


         {{--<div class="tab_stylee">
          	<a href="javascript:;" class="tab_btnn01 tab_active">Profile</a>
            <a href="#">View Property</a>
            <a href="javascript:;">View Availability</a>
            <a href="javascript:;">View Visit Requests</a>
          </div>--}}

            <div class="panel panel-default panel-fill">

                    <div class="panel-body">

                        <!--<div class="about-info-p rm_new10">
                            <strong>Full Name :</strong>
                            <p class="text-muted">Mark Dicus</p>
                        </div>
                        <div class="about-info-p rm_new10">
                            <strong>User Name :</strong>
                            <p class="text-muted">Abc Username</p>
                        </div>
                        <div class="about-info-p rm_new10">
                            <strong>Email Address :</strong>
                            <p class="text-muted">testemail123@gmail.com</p>
                        </div>-->


            <div class="chat_rig_body">
            @if(@$allMessages)
              @foreach(@$allMessages as $message)
						<div class="chat_mass_itm">
             
							<div class="media">
								<em>
                  @if(@$message->getUser->profile_pic != null)
                  <img src="{{ url('storage/app/public/profile_picture/'.@$message->getUser->profile_pic) }}" alt="">
                  @else
                  <img src="{{ url('public/frontend/images/chat_img1.jpg') }}" alt="">
                  @endif
                 </em>
								<div class="media-body">
									<h5>{{ @$message->getUser->name }}</h5>
									<div class="chat_mass_bx">
                  @if(@$message->file)<br><a href="{{url('storage/app/public/message_files/')}}/{{@$message->file}}"  target=_blank>{{substr(@$message->file,strpos(@$message->file,'_')+1)}}</a> @endif
										<p>{{  nl2br(@$message->message) }}</p>
									</div>
									<span>{{date('d-m-Y')==date('d-m-Y',strtotime($message->created_at))?date('H:i A',strtotime($message->created_at)):date('d-F-Y H:i A',strtotime($message->created_at))}}</span>
								</div>
							</div>
						</div>
            @endforeach
             @endif
						{{--<div class="chat_mass_itm chat_mass_itm_rig">
							<div class="media">
								<em><img src="images/chat_img5.jpg" alt=""></em>
								<div class="media-body">
									<div class="chat_mass_bx">
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text. simply dummy text of the print and typesetting industry. Lorem Ipsum has been the industry's standard dummy text. </p>
									</div>
									<span>10mins ago</span>
								</div>
							</div>
						</div>--}}
						{{--<div class="chat_mass_itm">
							<div class="media">
								<em><img src="images/chat_img1.jpg" alt=""></em>
								<div class="media-body">
									<h5>Mark Dacascos</h5>
									<div class="chat_mass_bx">
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</p>
									</div>
									<span>10mins ago</span>
								</div>
							</div>
						</div>
						<div class="chat_mass_itm chat_mass_itm_rig">
							<div class="media">
								<em><img src="images/chat_img5.jpg" alt=""></em>
								<div class="media-body">
									<div class="chat_mass_bx">
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text. simply dummy text of the print and typesetting industry. Lorem Ipsum has been the industry's standard dummy text. </p>
									</div>
									<span>10mins ago</span>
								</div>
							</div>
						</div>
						<div class="chat_mass_itm">
							<div class="media">
								<em><img src="images/chat_img1.jpg" alt=""></em>
								<div class="media-body">
									<h5>Mark Dacascos</h5>
									<div class="chat_mass_bx">
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</p>
									</div>
									<span>10mins ago</span>
								</div>
							</div>
						</div>--}}
					</div>
                      

                    </div>
                </div>



          </div>
        </div>
        <!-- End row -->

      </div>
      <!-- container -->

    </div>
    <!-- content -->

    @include('admin.includes.footer')
  </div>

  @section('scripts')

  @include('admin.includes.scripts')

  @endsection

  @endsection
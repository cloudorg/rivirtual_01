@extends('admin.layouts.app')

@section('title')
RiVirtual | Admin | Manage City
@endsection

@section('content')

@section('links')

@include('admin.includes.links')

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65046120-1', 'auto');
  ga('send', 'pageview');
</script>

@endsection

@section('header')

@include('admin.includes.header')

@endsection

@section('sidebar')

@include('admin.includes.sidebar')

@endsection

<div class="content-page">
    <!-- Start content -->
    <div class="content">
      <div class="container">

        <!-- Page-Title -->
        <div class="row">
          <div class="col-sm-12">
            <h4 class="pull-left page-title">Manage City</h4>
            <a href="{{ route('admin.add.city') }}" class="rm_new04"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add City </a>
            <!--<ol class="breadcrumb pull-right">
              <li><a href="#">Aariv School</a></li>
              <li><a href="#">Data Tables</a></li>
              <li class="active">Basic Tables</li>
            </ol>-->
          </div>
        </div>
        @include('admin.includes.message')
        <div class="row">
          <div class="col-md-12">

          <div class="panel panel-default">
          <div class="panel-heading rm02 rm04 rm_new01">
                <form role="form" action="{{ route('admin.manage.city') }}" method="get">

                  <!--<div class="form-group ">
                    <label for="">Category</label>
                    <select class="form-control rm06">
                        <option>Select</option>
                        <option>Option 1</option>
                        <option>Option 2</option>
                        <option>Option 3</option>
                        <option>Option 4</option>
                    </select>
                </div>-->
                <div class="form-group">
                    <label for="">City</label>
                    <input type="text" name="city" value="{{ Request::get('city') }}" id="" class="form-control" placeholder="Enter here">
                  </div>
                  <div class="form-group">
                  <label for="">State</label>
                    <select class="form-control rm06" name="state">
                    <option value="">Select State</option>
                     @foreach($allState as $st)
                     <option value="{{ $st->id }}" @if(Request::get('state') == $st->id) selected @endif>{{ $st->name }}</option>
                    @endforeach
                  </select>
                  </div>
                <!--<div class="form-group">
                    <label for="">Subject</label>
                    <select class="form-control rm06">
                        <option>Select</option>
                        <option>History</option>
                        <option>Geography</option>
                        <option>Mathematics</option>
                        <option>Life Science</option>
                        <option>Physical Science</option>
                        <option>English</option>
                    </select>
                </div>
                  <div class="form-group">
                    <label for="">Deadline</label>
                    <input type="text" value="" id="datepicker1" class="form-control calander_icn " placeholder="Select">
                  </div>-->
                  <div class="rm05">
                    <button class="btn btn-primary waves-effect waves-light w-md" type="submit">Search</button>
                    <a href="{{ route('admin.manage.city') }}" class="btn btn-success waves-effect waves-light w-md">Reset</a>
                  </div>
                </form>
              </div>
          </div>


            <div class="panel panel-default">


              <div class="panel-body">
                <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="table-responsive">
                      <table class="table">
                        <thead>
                          <tr>
                            <th>City</th>
                            <th>State</th>
                            <!--<th>Name</th>
                            <th>Email</th>-->
                            <th class="rm07">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          @if(count($city)>0)
                           @foreach($city as $ct)
                          <tr>
                            <td>{{ $ct->name }}</td>
                            <td>{{ @$ct->stateName->name }}</td>
                           <!-- <td>10</td>
                            <td>History</td>-->
                            <td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action{{ @$ct->id }}" onclick="fun({{ @$ct->id }})"><img src="{{ url('public/admin/assets/images/action-dots.png') }}" alt=""></a>
                            <div class="show-actions" id="show-action{{ @$ct->id }}" style="display: none;">
                                <span class="angle"><img src="{{ url('public/admin/assets/images/angle.png') }}" alt=""></span>
                                <ul>
                                    <li><a href="{{ route('admin.edit.city',@$ct->id) }}">Edit</a></li>
                                    <li><a href="{{ route('admin.delete.city',@$ct->id) }}" onclick="return confirm('Are you want to delete this city?')">Delete</a></li>
                                </ul>
                              </div>
                            </td>
                          </tr>
                          @endforeach
                          @else
                          <tr role="row" style="text-align:center">
                              <td colspan="3">No Data Found</td>
                          </tr>
                          @endif


                        </tbody>
                      </table>
                    </div>
                    <div style="float: right;">{{@$city->appends(request()->except(['page', '_token']))->links()}}</div>

                    <!-- <ul class="pagination">
                      <li class="paginate_button previous disabled"><a href="#">Previous</a></li>
                      <li class="paginate_button active"><a href="#">1</a></li>
                      <li class="paginate_button"><a href="#">2</a></li>
                      <li class="paginate_button"><a href="#">3</a></li>
                      <li class="paginate_button"><a href="#">4</a></li>
                      <li class="paginate_button"><a href="#">5</a></li>
                      <li class="paginate_button"><a href="#">6</a></li>
                      <li class="paginate_button next"><a href="#">Next</a></li>
                    </ul> -->


                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- End row -->

      </div>
      <!-- container -->

    </div>
    <!-- content -->

    @include('admin.includes.footer')
  </div>
  @section('scripts')

  @include('admin.includes.scripts')

   <script>

  function fun(id){
    $('.show-actions').slideUp();
    $("#show-action"+id).show();
  }
  $(document).on('click', function () {
      var $target = $(event.target);
      if (!$target.closest('.action-dots').length && $('.show-actions').is(":visible")) {
          $('.show-actions').slideUp();
        }
    });
</script>

  @endsection
  @endsection

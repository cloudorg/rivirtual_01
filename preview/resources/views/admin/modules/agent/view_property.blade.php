@extends('admin.layouts.app')

@section('title')
RiVirtual | Admin | View Property
@endsection

@section('content')

@section('links')

@include('admin.includes.links')

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65046120-1', 'auto');
  ga('send', 'pageview');
</script>

@endsection

@section('header')

@include('admin.includes.header')

@endsection

@section('sidebar')

@include('admin.includes.sidebar')

@endsection

<div class="content-page"> 
    <!-- Start content -->
    <div class="content">
      <div class="container"> 
        
        <!-- Page-Title -->
        <div class="row">
          <div class="col-sm-12">
            <h4 class="pull-left page-title"> Manage Agents <i class="fa fa-chevron-right" aria-hidden="true"></i> {{ @$userData->name }} [{{ @$userData->user_id }}]</h4>
            <a href="{{ route('admin.manage.agent') }}" class="rm_new04"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
            <!--<a href="add_class_links.html" class="rm_new04"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Class Links</a>-->
            <!--<ol class="breadcrumb pull-right">
              <li><a href="#">Aariv School</a></li>
              <li><a href="#">Data Tables</a></li>
              <li class="active">Basic Tables</li>
            </ol>-->
          </div>
        </div>
        @include('admin.includes.message')
        <div class="row">
          <div class="col-md-12">
          
          
          <div class="tab_stylee">
          	<a href="{{ route('admin.agent.details',['id'=>@$userId]) }}" class="tab_btnn01">Profile</a>
            <a href="{{ route('admin.agent.view.property',['id'=>@$userId]) }}" class="tab_active">View Property</a>
            <a href="{{ route('admin.agent.availability',['id'=>@$userId]) }}">View Availability</a>
            <a href="{{ route('admin.agent.visit.request',['id'=>@$userId]) }}" >View Visit Requests</a>
          </div>
          <div class="panel panel-default">
          <div class="panel-heading rm02 rm04 rm_new01">
                <form role="form" action="{{ route('admin.agent.view.property',['id'=>@$userId]) }}" method="get">
                  
					<div class="form-group">
                    <label for="">Property Name</label>
                    <input type="text" name="property_name" value="{{ Request::get('property_name') }}" id="" class="form-control" placeholder="Enter Name...">
					</div>
                    <div class="form-group">
                    <label for="">Location</label>
                    <input type="text" name="location" value="{{ Request::get('location') }}" id="" class="form-control" placeholder="Enter Location...">
					</div>
                    <div class="form-group">
                    <label for="">Status</label>
                    <select class="form-control rm06" name="status">
                        <option value="">Select</option>
                        <option value="S" @if(Request::get('status') == 'S') selected @endif>Sold</option>
                        <option value="A" @if(Request::get('status') == 'A') selected @endif>Active</option>
                        <option value="B" @if(Request::get('status') == 'B') selected @endif>Inactive</option>
                    </select>
                   </div>
                   <div class="form-group">
                    <label for="">Property For</label>
                    <select class="form-control rm06" name="property_for">
                        <option value="">Select</option>
                        <option value="B" @if(Request::get('property_for') == 'B') selected @endif>For Buy</option>
                        <option value="R" @if(Request::get('property_for') == 'R') selected @endif>For Rent</option>
                        
                    </select>
                   </div>
                  <!--
                <div class="form-group">
                    <label for="">Subject</label>
                    <select class="form-control rm06">
                        <option>Select</option>
                        <option>History</option>
                        <option>Geography</option>
                        <option>Mathematics</option>
                        <option>Life Science</option>
                        <option>Physical Science</option>
                        <option>English</option>
                    </select>
                </div>-->
                  
                  <div class="rm05">
                    <button class="btn btn-primary waves-effect waves-light w-md" type="submit">Search</button>
                    <a href="{{ route('admin.agent.view.property',['id'=>@$userId]) }}" class="btn btn-success waves-effect waves-light w-md">Reset</a>
                  </div>
                </form>
              </div>
          </div>
            <div class="panel panel-default panel-fill">
            
                    <div class="panel-body"> 
                        
                        <div class="table-responsive">
                      <table class="table">
                        <thead>
                          <tr>
                            <th>Property Image</th>  
                            <th>Property Name</th>
                            <th>Location</th>
                            <th>Area(sqft)</th>
                            <th>Price</th>
                            <th>Total Bedroom</th>
                            <th>Total Bathroom</th>
                            <th>Status</th>
                            <th class="rm07">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          @if(count(@$propertyDetails)>0)
                           @foreach(@$propertyDetails as $pro)
                          <tr>
                            <td>
                               @if(@$pro->propertyImageMain != null)
                                <img src="{{ URL::to('storage/app/public/property_image') }}/{{ @$pro->propertyImageMain->image }}" alt="" width="45px">  
                                @endif
                            </td>
                            <td>{{ @$pro->name }}</td>
                            <td>{!! $pro->address !!}</td>
                            <td>{{ @$pro->area }}</td>
                            <td>{{ number_format(@$pro->budget_range_from,0,'.',',') }}</td>
                            <td>{{ @$pro->no_of_bedrooms }}</td>
                            <td>{{ @$pro->bathroom }}</td>
                            <td>
                              @if(@$pro->status == 'A')
                               Active
                                @elseif(@$pro->status == 'B')
                                 Inactive
                                 @elseif(@$pro->status == 'S')
                                  Sold
                                  @endif 

                            </td>
                            <td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action" onclick="fun({{ @$pro->id }})"><img src="{{ url('public/admin/assets/images/action-dots.png') }}" alt=""></a>
                            <div class="show-actions" id="show-action{{ @$pro->id }}" style="display: none;">
                                <span class="angle"><img src="{{ url('public/admin/assets//images/angle.png') }}" alt=""></span>
                                <ul>
                                    <li><a href="{{ route('admin.property.details',['id'=>@$pro->id]) }}">View</a></li>
                                    <li><a href="{{ route('admin.edit.property',['id'=>@$pro->id]) }}">Edit</a></li>
                                    <li>
                                      @if(@$pro->status == 'A')
                                      <a href="{{ route('admin.agent.property.status.change',['id'=>@$pro->id]) }}" onclick="return confirm('Are you want to change this status?')">Make Inactive</a>
                                      @elseif(@$pro->status == 'B')
                                      <a href="{{ route('admin.agent.property.status.change',['id'=>@$pro->id]) }}" onclick="return confirm('Are you want to change this status?')">Make Active</a>
                                      @endif
                                     </li>
                                    <!--<li><a href="#">Edit</a></li>
                                    <li><a href="#">Unblock</a></li>-->
                                </ul>
                              </div>
                            </td>
                          </tr>
                          @endforeach
                          @else
                           <tr role="row" class="odd">
                                        <td colspan="10" style="text-align:center;"> 
                                            No Data Found
                                        </td>
                                    </tr>
                           @endif
                          {{--<tr>
                            <td>Rabin Manna</td>
                            <td>Abhijeet Roy</td>
                            <td>18.10.2021</td>
                            <td>11:30 am</td>
                            <td>Mumbai</td>
                            <td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action2"><img src="{{ url('public/admin/assets/images/action-dots.png') }}" alt=""></a>
                            <div class="show-actions" id="show-action2" style="display: none;">
                                <span class="angle"><img src="{{ url('public/admin/assets//images/angle.png') }}" alt=""></span>
                                <ul>
                                    <li><a href="agent_details.html">Cancel</a></li>
                                    <!--<li><a href="#">Edit</a></li>
                                    <li><a href="#">Unblock</a></li>-->
                                </ul>
                              </div>
                            </td>
                          </tr>
                          
                          
                          <tr>
                            <td>Rabin Das</td>
                            <td>Abhijeet Roy</td>
                            <td>14.10.2021</td>
                            <td>10:20 am</td>
                            <td>Kolkata</td>
                            <td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action3"><img src="assets/images/action-dots.png" alt=""></a>
                            <div class="show-actions" id="show-action3" style="display: none;">
                                <span class="angle"><img src="assets//images/angle.png" alt=""></span>
                                <ul>
                                    <li><a href="agent_details.html">Cancel</a></li>
                                    <!--<li><a href="#">Edit</a></li>
                                    <li><a href="#">Unblock</a></li>-->
                                </ul>
                              </div>
                            </td>
                          </tr>
                          
                          <tr>
                            <td>Rabin Manna</td>
                            <td>Abhijeet Roy</td>
                            <td>18.10.2021</td>
                            <td>11:30 am</td>
                            <td>Mumbai</td>
                            <td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action4"><img src="{{ url('public/admin/assets/images/action-dots.png') }}" alt=""></a>
                            <div class="show-actions" id="show-action4" style="display: none;">
                                <span class="angle"><img src="{{ url('public/admin/assets//images/angle.png') }}" alt=""></span>
                                <ul>
                                    <li><a href="agent_details.html"></a></li>
                                    <!--<li><a href="#">Edit</a></li>
                                    <li><a href="#">Unblock</a></li>-->
                                </ul>
                              </div>
                            </td>
                          </tr>--}}
                        
                      
                        </tbody>
                      </table>
                    </div>
                    
                    
                    
                        
                    </div> 
                </div>
          
            
            
          </div>
        </div>
        <!-- End row --> 
        
      </div>
      <!-- container --> 
      
    </div>
    <!-- content -->
    
    @include('admin.includes.footer')
  </div>

  @section('scripts')

  @include('admin.includes.scripts')

  <script>
    function fun(id){

$('.show-actions').slideUp();
 $("#show-action"+id).show();
}
</script>



  @endsection
  @endsection
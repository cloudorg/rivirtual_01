@extends('admin.layouts.app')

@section('title')
RiVirtual | Admin | Visit Request
@endsection

@section('content')

@section('links')

@include('admin.includes.links')

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65046120-1', 'auto');
  ga('send', 'pageview');
</script>

@endsection

@section('header')

@include('admin.includes.header')

@endsection

@section('sidebar')

@include('admin.includes.sidebar')

@endsection

<div class="content-page"> 
    <!-- Start content -->
    <div class="content">
      <div class="container"> 
        
        <!-- Page-Title -->
        <div class="row">
          <div class="col-sm-12">
            <h4 class="pull-left page-title"> Manage Agents <i class="fa fa-chevron-right" aria-hidden="true"></i> Rabin Manna [A12345678]</h4>
            <a href="{{ route('admin.manage.agent') }}" class="rm_new04"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
            <!--<a href="add_class_links.html" class="rm_new04"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Class Links</a>-->
            <!--<ol class="breadcrumb pull-right">
              <li><a href="#">Aariv School</a></li>
              <li><a href="#">Data Tables</a></li>
              <li class="active">Basic Tables</li>
            </ol>-->
          </div>
        </div>
        
        <div class="row">
          <div class="col-md-12">
          
          
          <div class="tab_stylee">
          <a href="{{ route('admin.agent.details',['id'=>@$userId]) }}" class="tab_btnn01">Profile</a>
            <a href="{{ route('admin.agent.view.property',['id'=>@$userId]) }}">View Property</a>
            <a href="{{ route('admin.agent.availability',['id'=>@$userId]) }}">View Availability</a>
            <a href="{{ route('admin.agent.visit.request',['id'=>@$userId]) }}" class="tab_active">View Visit Requests</a>
          </div>
          
            <div class="panel panel-default panel-fill">
            
                    <div class="panel-body"> 
                        
                        <div class="table-responsive">
                      <table class="table">
                        <thead>
                          <tr>
                            <th>Property Name</th>
                            <th>Client's Name</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Location</th>
                            <th class="rm07">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        
                          <tr>
                            <td>Rabin Das</td>
                            <td>Abhijeet Roy</td>
                            <td>14.10.2021</td>
                            <td>10:20 am</td>
                            <td>Kolkata</td>
                            <td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action1"><img src="{{ url('public/admin/assets/images/action-dots.png') }}" alt=""></a>
                            <div class="show-actions" id="show-action1" style="display: none;">
                                <span class="angle"><img src="{{ url('public/admin/assets//images/angle.png') }}" alt=""></span>
                                <ul>
                                    <li><a href="agent_details.html">Cancel</a></li>
                                    <!--<li><a href="#">Edit</a></li>
                                    <li><a href="#">Unblock</a></li>-->
                                </ul>
                              </div>
                            </td>
                          </tr>
                          
                          <tr>
                            <td>Rabin Manna</td>
                            <td>Abhijeet Roy</td>
                            <td>18.10.2021</td>
                            <td>11:30 am</td>
                            <td>Mumbai</td>
                            <td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action2"><img src="{{ url('public/admin/assets/images/action-dots.png') }}" alt=""></a>
                            <div class="show-actions" id="show-action2" style="display: none;">
                                <span class="angle"><img src="{{ url('public/admin/assets//images/angle.png') }}" alt=""></span>
                                <ul>
                                    <li><a href="agent_details.html">Cancel</a></li>
                                    <!--<li><a href="#">Edit</a></li>
                                    <li><a href="#">Unblock</a></li>-->
                                </ul>
                              </div>
                            </td>
                          </tr>
                          
                          
                          {{--<tr>
                            <td>Rabin Das</td>
                            <td>Abhijeet Roy</td>
                            <td>14.10.2021</td>
                            <td>10:20 am</td>
                            <td>Kolkata</td>
                            <td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action3"><img src="assets/images/action-dots.png" alt=""></a>
                            <div class="show-actions" id="show-action3" style="display: none;">
                                <span class="angle"><img src="assets//images/angle.png" alt=""></span>
                                <ul>
                                    <li><a href="agent_details.html">Cancel</a></li>
                                    <!--<li><a href="#">Edit</a></li>
                                    <li><a href="#">Unblock</a></li>-->
                                </ul>
                              </div>
                            </td>
                          </tr>
                          
                          <tr>
                            <td>Rabin Manna</td>
                            <td>Abhijeet Roy</td>
                            <td>18.10.2021</td>
                            <td>11:30 am</td>
                            <td>Mumbai</td>
                            <td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action4"><img src="assets/images/action-dots.png" alt=""></a>
                            <div class="show-actions" id="show-action4" style="display: none;">
                                <span class="angle"><img src="assets//images/angle.png" alt=""></span>
                                <ul>
                                    <li><a href="agent_details.html">Cancel</a></li>
                                    <!--<li><a href="#">Edit</a></li>
                                    <li><a href="#">Unblock</a></li>-->
                                </ul>
                              </div>
                            </td>
                          </tr>--}}
                        
                      
                        </tbody>
                      </table>
                    </div>
                    
                    
                    
                        
                    </div> 
                </div>
          
            
            
          </div>
        </div>
        <!-- End row --> 
        
      </div>
      <!-- container --> 
      
    </div>
    <!-- content -->
    
    @include('admin.includes.footer')
  </div>

  @section('scripts')

  @include('admin.includes.scripts')

  <script>
$(document).ready(function(){
    $("#action1").click(function(){
        $("#show-action1").slideToggle();
    });
});
</script>

<script>
$(document).ready(function(){
    $("#action2").click(function(){
        $("#show-action2").slideToggle();
    });
});
</script>

  @endsection
  @endsection
@extends('admin.layouts.app')

@section('title')
RiVirtual | Admin | View Availability
@endsection

@section('content')

@section('links')

@include('admin.includes.links')

<style>
    .error{
        color: red !important;
    }
</style>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65046120-1', 'auto');
  ga('send', 'pageview');
</script>

@endsection

@section('header')

@include('admin.includes.header')

@endsection

@section('sidebar')

@include('admin.includes.sidebar')

@endsection

<div class="content-page"> 
    <!-- Start content -->
    <div class="content">
      <div class="container"> 
        
        <!-- Page-Title -->
        <div class="row">
          <div class="col-sm-12">
            <h4 class="pull-left page-title"> Manage Agent <i class="fa fa-chevron-right" aria-hidden="true"></i> {{ @$userData->name }} [{{ @$userData->user_id }}]</h4>
            <a href="{{ route('admin.manage.agent') }}" class="rm_new04"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
            <!--<a href="add_class_links.html" class="rm_new04"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Class Links</a>-->
            <!--<ol class="breadcrumb pull-right">
              <li><a href="#">Aariv School</a></li>
              <li><a href="#">Data Tables</a></li>
              <li class="active">Basic Tables</li>
            </ol>-->
          </div>
        </div>
        @include('admin.includes.message')
        <div class="row">
          <div class="col-md-12">
          
          
          <div class="tab_stylee">
            <a href="{{ route('admin.agent.details',['id'=>@$userId]) }}" class="tab_btnn01">Profile</a>
          	<a href="{{ route('admin.agent.view.property',['id'=>@$userId]) }}" class="">View Property</a>
            <a href="{{ route('admin.agent.availability',['id'=>@$userId]) }}" class="tab_active">View Availability</a>
            <a href="{{ route('admin.agent.visit.request',['id'=>@$userId]) }}">View Visit Requests</a>
          </div>
          
            <div class="panel panel-default panel-fill">
            
                    <div class="panel-body"> 
                        
                    <div class="agent-right-body">
                    <form action="{{route('admin.agent.manage.availability.save')}}" method="POST" id="mondayform">
                      @csrf
                      <input type="hidden" name="day" value="1">
                      <input type="hidden" name="userId" value="{{ @$userId }}">
                    <div class="row new_rows no-gutters align-items-center">                                  
                                  <div class="col-md-2 col-sm-12">
                                                  <div class="check-box new-av-check">
                                                      {{--<input type="checkbox" id="checkbox1" name="checkbox1" value="all">--}}
                                                      <label for="checkbox1" class="checklabeel">Monday : </label>                       
                                                  </div>
                                  </div>
                                  <div class="col-md-4 col-sm-6">
                                                  <div class="form_box_area das_input">  
                                                      <label>From</label>
                                                      <div class="dash-d">
                                  <input type="text" class="demo1" placeholder="Enter here.. " class="position-relative" name="fromtime" id="fromtime1" autocomplete="off"> <span class="over_llp1"><img src="{{ url('public/admin/assets/images/clocks.png') }}" alt=""></span>
                                </div>
                                                  </div>
                                  </div>
                                  <div class="col-md-4 col-sm-6">
                                                  <div class="form_box_area das_input">  
                                                      <label>To</label>
                                                      <div class="dash-d">
                                  <input type="text" class="demo1" placeholder="Enter here.. " class="position-relative" name="totime" autocomplete="off"> <span class="over_llp1"><img src="{{ url('public/admin/assets/images/clocks.png') }}" alt=""></span>
                                </div>
                                                  </div>
                                  </div>
                                  <div class="col-md-2 col-sm-12">
                                                  <div class="form_box_area">
                                                      <label></label>
                                                      {{--<a href="#" class="adds_btn nesave">Add</a>--}}
                                                      <input type="submit"class="adds_btn nesave" value="Add">
                                                  </div>
                                  </div>
                                  <div class="col-md-10 offset-md-2">
                                    <ul class="list-showag">
                                        @foreach ($monday as $item1)
                                        <li>{{date('h:i A',strtotime(@$item1->from_time))}} to {{date('h:i A',strtotime(@$item1->to_time))}}<span><a href="{{route('admin.agent.manage.availability.remove',$item1->id)}}" onclick="return confirm('Are you want to remove this availability ?')"><img src="{{ URL::to('public/admin/assets/images/cross4.png')}}"></a></span></li>
                                        @endforeach
                                        {{-- <li>12:00 am to 12:30 pm  <span><a href="#"><img src="{{ URL::to('public/frontend/images/cross4.png')}}"></a></span></li>
                                        <li>2:00 am to 2:30 pm  <span><a href="#"><img src="{{ URL::to('public/frontend/images/cross4.png')}}"></a></span></li> --}}
                                    </ul>
                                </div>
                               </div>
                             </form>
                             <form action="{{route('admin.agent.manage.availability.save')}}" method="POST" id="tuesdayform">
                               @csrf
                               <input type="hidden" name="day" value="2">
                               <input type="hidden" name="userId" value="{{ @$userId }}">
                              <div class="row new_rows no-gutters align-items-center">                                  
                                  <div class="col-md-2 col-sm-12">
                                                  <div class="check-box new-av-check">
                                                      {{--<input type="checkbox" id="checkbox2" name="checkbox1" value="all">--}}
                                                      <label for="checkbox2" class="checklabeel">Tuesday : </label>                       
                                                  </div>
                                  </div>
                                  <div class="col-md-4 col-sm-6">
                                                  <div class="form_box_area das_input">  
                                                      <label>From</label>
                                                      <div class="dash-d">
                                  <input type="text" class="demo1" placeholder="Enter here.. " class="position-relative" name="fromtime" id="fromtime2" autocomplete="off"> <span class="over_llp1"><img src="{{ url('public/admin/assets/images/clocks.png') }}" alt=""></span>
                                </div>
                                </div>
                                  </div>
                                  <div class="col-md-4 col-sm-6">
                                                  <div class="form_box_area das_input">  
                                                      <label>To</label>
                                                      <div class="dash-d">
                                  <input type="text" class="demo1" placeholder="Enter here.. " class="position-relative" name="totime" autocomplete="off"> <span class="over_llp1"><img src="{{ url('public/admin/assets/images/clocks.png') }}" alt=""></span>
                                </div>
                                                  </div>
                                  </div>
                                  <div class="col-md-2 col-sm-12">
                                                  <div class="form_box_area">
                                                      <label></label>
                                                      {{--<a href="#" class="adds_btn nesave">Add</a>--}}
                                                      <input type="submit"class="adds_btn nesave" value="Add">
                                                  </div>
                                  </div>
                                  <div class="col-md-10 offset-md-2">
                                    <ul class="list-showag">
                                        @foreach ($tuesday as $item2)
                                        <li>{{date('h:i A',strtotime(@$item2->from_time))}} to {{date('h:i A',strtotime(@$item2->to_time))}}<span><a href="{{route('admin.agent.manage.availability.remove',$item2->id)}}" onclick="return confirm('Are you want to remove this availability ?')"><img src="{{ URL::to('public/admin/assets/images/cross4.png')}}"></a></span></li>
                                        @endforeach
                                        {{-- <li>12:00 am to 12:30 pm  <span><a href="#"><img src="{{ URL::to('public/frontend/images/cross4.png')}}"></a></span></li>
                                        <li>2:00 am to 2:30 pm  <span><a href="#"><img src="{{ URL::to('public/frontend/images/cross4.png')}}"></a></span></li> --}}
                                    </ul>
                                </div>
                              </div>
                              </form>
                              <form action="{{route('admin.agent.manage.availability.save')}}" method="POST" id="wednesdayform">
                                @csrf
                                <input type="hidden" name="day" value="3">
                                <input type="hidden" name="userId" value="{{ @$userId }}">
                              <div class="row new_rows no-gutters align-items-center">                                  
                                  <div class="col-md-2 col-sm-12">
                                                  <div class="check-box new-av-check">
                                                      {{--<input type="checkbox" id="checkbox3" name="checkbox1" value="all">--}}
                                                      <label for="checkbox3" class="checklabeel">Wednesday : </label>                       
                                                  </div>
                                  </div>
                                  <div class="col-md-4 col-sm-6">
                                                  <div class="form_box_area das_input">  
                                                      <label>From</label>
                                                      <div class="dash-d">
                                  <input type="text" class="demo1" placeholder="Enter here.. " class="position-relative" name="fromtime" id="fromtime3" autocomplete="off"> <span class="over_llp1"><img src="{{ url('public/admin/assets/images/clocks.png') }}" alt=""></span>
                                </div>
                                                  </div>
                                  </div>
                                  <div class="col-md-4 col-sm-6">
                                                  <div class="form_box_area das_input">  
                                                      <label>To</label>
                                                      <div class="dash-d">
                                  <input type="text" class="demo1" placeholder="Enter here.. " class="position-relative" name="totime" autocomplete="off"> <span class="over_llp1"><img src="{{ url('public/admin/assets/images/clocks.png') }}" alt=""></span>
                                </div>
                                                  </div>
                                  </div>
                                  <div class="col-md-2 col-sm-12">
                                                  <div class="form_box_area">
                                                      <label></label>
                                                      {{--<a href="#" class="adds_btn nesave">Add</a>--}}
                                                      <input type="submit"class="adds_btn nesave" value="Add">
                                                  </div>
                                  </div>
                                  <div class="col-md-10 offset-md-2">
                                    <ul class="list-showag">
                                        @foreach ($wednesday as $item3)
                                        <li>{{date('h:i A',strtotime(@$item3->from_time))}} to {{date('h:i A',strtotime(@$item3->to_time))}}<span><a href="{{route('admin.agent.manage.availability.remove',$item3->id)}}" onclick="return confirm('Are you want to remove this availability ?')"><img src="{{ URL::to('public/admin/assets/images/cross4.png')}}"></a></span></li>
                                        @endforeach
                                    </ul>
                                </div>
                              </div>
                             </form>
                             <form action="{{route('admin.agent.manage.availability.save')}}" method="POST" id="thursdayform">
                               @csrf
                               <input type="hidden" name="day" value="4">
                               <input type="hidden" name="userId" value="{{ @$userId }}">
                                <div class="row new_rows no-gutters align-items-center">                                  
                                  <div class="col-md-2 col-sm-12">
                                                  <div class="check-box new-av-check">
                                                      {{--<input type="checkbox" id="checkbox4" name="checkbox1" value="all">--}}
                                                      <label for="checkbox4" class="checklabeel">Thursday : </label>                       
                                                  </div>
                                  </div>
                                  <div class="col-md-4 col-sm-6">
                                                  <div class="form_box_area das_input">  
                                                      <label>From</label>
                                                      <div class="dash-d">
                                  <input type="text" class="demo1" placeholder="Enter here.. " class="position-relative" name="fromtime" id="fromtime4" autocomplete="off"> <span class="over_llp1"><img src="{{ url('public/admin/assets/images/clocks.png') }}" alt=""></span>
                                </div>
                                                  </div>
                                  </div>
                                  <div class="col-md-4 col-sm-6">
                                                  <div class="form_box_area das_input">  
                                                      <label>To</label>
                                                      <div class="dash-d">
                                  <input type="text" class="demo1" placeholder="Enter here.. " class="position-relative" name="totime" autocomplete="off"> <span class="over_llp1"><img src="{{ url('public/admin/assets/images/clocks.png') }}" alt=""></span>
                                </div>
                                                  </div>
                                  </div>
                                  <div class="col-md-2 col-sm-12">
                                                  <div class="form_box_area">
                                                      <label></label>
                                                      {{--<a href="#" class="adds_btn nesave">Add</a>--}}
                                                      <input type="submit"class="adds_btn nesave" value="Add">
                                                  </div>
                                  </div>
                                  <div class="col-md-10 offset-md-2">
                                    <ul class="list-showag">
                                        @foreach ($thursday as $item4)
                                        <li>{{date('h:i A',strtotime(@$item4->from_time))}} to {{date('h:i A',strtotime(@$item4->to_time))}}<span><a href="{{route('admin.agent.manage.availability.remove',$item4->id)}}" onclick="return confirm('Are you want to remove this availability ?')"><img src="{{ URL::to('public/admin/assets/images/cross4.png')}}"></a></span></li>
                                        @endforeach
                                    </ul>
                                </div>
                              </div>
                             </form>
                             <form action="{{route('admin.agent.manage.availability.save')}}" method="POST" id="fridayform">
                               @csrf
                               <input type="hidden" name="day" value="5">
                               <input type="hidden" name="userId" value="{{ @$userId }}">
                              <div class="row new_rows no-gutters align-items-center">                                  
                                  <div class="col-md-2 col-sm-12">
                                                  <div class="check-box new-av-check">
                                                      {{--<input type="checkbox" id="checkbox5" name="checkbox1" value="all">--}}
                                                      <label for="checkbox5" class="checklabeel">Friday : </label>                       
                                                  </div>
                                  </div>
                                  <div class="col-md-4 col-sm-6">
                                                  <div class="form_box_area das_input">  
                                                      <label>From</label>
                                                      <div class="dash-d">
                                  <input type="text" class="demo1" placeholder="Enter here.. " class="position-relative" name="fromtime" id="fromtime5" autocomplete="off"> <span class="over_llp1"><img src="{{ url('public/admin/assets/images/clocks.png') }}" alt=""></span>
                                </div>
                                                  </div>
                                  </div>
                                  <div class="col-md-4 col-sm-6">
                                                  <div class="form_box_area das_input">  
                                                      <label>To</label>
                                                      <div class="dash-d">
                                  <input type="text" class="demo1" placeholder="Enter here.. " class="position-relative" name="totime" autocomplete="off"> <span class="over_llp1"><img src="{{ url('public/admin/assets/images/clocks.png') }}" alt=""></span>
                                </div>
                                                  </div>
                                  </div>
                                  <div class="col-md-2 col-sm-12">
                                                  <div class="form_box_area">
                                                      <label></label>
                                                      {{--<a href="#" class="adds_btn nesave">Add</a>--}}
                                                      <input type="submit"class="adds_btn nesave" value="Add">
                                                  </div>
                                  </div>
                                  <div class="col-md-10 offset-md-2">
                                    <ul class="list-showag">
                                        @foreach ($friday as $item5)
                                        <li>{{date('h:i A',strtotime(@$item5->from_time))}} to {{date('h:i A',strtotime(@$item5->to_time))}}<span><a href="{{route('admin.agent.manage.availability.remove',$item5->id)}}" onclick="return confirm('Are you want to remove this availability ?')"><img src="{{ URL::to('public/admin/assets/images/cross4.png')}}"></a></span></li>
                                        @endforeach
                                    </ul>
                                </div>
                              </div>
                              </form>
                              <form action="{{route('admin.agent.manage.availability.save')}}" method="POST" id="saturdayform">
                                @csrf
                                <input type="hidden" name="day" value="6">
                                <input type="hidden" name="userId" value="{{ @$userId }}">
                              <div class="row new_rows no-gutters align-items-center">                                  
                                  <div class="col-md-2 col-sm-12">
                                                  <div class="check-box new-av-check">
                                                      {{--<input type="checkbox" id="checkbox6" name="checkbox1" value="all">--}}
                                                      <label for="checkbox6" class="checklabeel">Saturday : </label>                       
                                                  </div>
                                  </div>
                                  <div class="col-md-4 col-sm-6">
                                                  <div class="form_box_area das_input">  
                                                      <label>From</label>
                                                      <div class="dash-d">
                                  <input type="text" class="demo1" placeholder="Enter here.. " class="position-relative" name="fromtime" id="fromtime6" autocomplete="off"> <span class="over_llp1"><img src="{{ url('public/admin/assets/images/clocks.png') }}" alt=""></span>
                                </div>
                                                  </div>
                                  </div>
                                  <div class="col-md-4 col-sm-6">
                                                  <div class="form_box_area das_input">  
                                                      <label>To</label>
                                                      <div class="dash-d">
                                  <input type="text" class="demo1" placeholder="Enter here.. " class="position-relative" name="totime" autocomplete="off"> <span class="over_llp1"><img src="{{ url('public/admin/assets/images/clocks.png') }}" alt=""></span>
                                </div>
                                                  </div>
                                  </div>
                                  <div class="col-md-2 col-sm-12">
                                                  <div class="form_box_area">
                                                      <label></label>
                                                      {{--<a href="#" class="adds_btn nesave">Add</a>--}}
                                                      <input type="submit"class="adds_btn nesave" value="Add">
                                                  </div>
                                  </div>
                                  <div class="col-md-10 offset-md-2">
                                    <ul class="list-showag">
                                        @foreach ($saturday as $item6)
                                        <li>{{date('h:i A',strtotime(@$item6->from_time))}} to {{date('h:i A',strtotime(@$item6->to_time))}}<span><a href="{{route('admin.agent.manage.availability.remove',$item6->id)}}" onclick="return confirm('Are you want to remove this availability ?')"><img src="{{ URL::to('public/admin/assets/images/cross4.png')}}"></a></span></li>
                                        @endforeach
                                    </ul>
                                </div>
                              </div>
                             </form>
                             <form action="{{route('admin.agent.manage.availability.save')}}" method="POST" id="sundayform">
                               @csrf
                               <input type="hidden" name="day" value="7">
                               <input type="hidden" name="userId" value="{{ @$userId }}">
                              <div class="row new_rows no-gutters align-items-center">                                  
                                  <div class="col-md-2 col-sm-12">
                                                  <div class="check-box new-av-check">
                                                      {{--<input type="checkbox" id="checkbox7" name="checkbox7" value="all">--}}
                                                      <label for="checkbox5" class="checklabeel">Sunday : </label>                       
                                                  </div>
                                  </div>
                                  <div class="col-md-4 col-sm-6">
                                                  <div class="form_box_area das_input">  
                                                      <label>From</label>
                                                      <div class="dash-d">
                                  <input type="text" class="demo1" placeholder="Enter here.. " class="position-relative" name="fromtime" id="fromtime7" autocomplete="off"> <span class="over_llp1"><img src="{{ url('public/admin/assets/images/clocks.png') }}" alt=""></span>
                                </div>
                                                  </div>
                                  </div>
                                  <div class="col-md-4 col-sm-6">
                                                  <div class="form_box_area das_input">  
                                                      <label>To</label>
                                                      <div class="dash-d">
                                  <input type="text" class="demo1" placeholder="Enter here.. " class="position-relative" name="totime" autocomplete="off"> <span class="over_llp1"><img src="{{ url('public/admin/assets/images/clocks.png') }}" alt=""></span>
                                </div>
                                                  </div>
                                  </div>
                                  <div class="col-md-2 col-sm-12">
                                                  <div class="form_box_area">
                                                      <label></label>
                                                      {{--<a href="#" class="adds_btn nesave">Add</a>--}}
                                                      <input type="submit"class="adds_btn nesave" value="Add">
                                                  </div>
                                  </div>
                                  <div class="col-md-10 offset-md-2">
                                    <ul class="list-showag">
                                        @foreach ($sunday as $item7)
                                        <li>{{date('h:i A',strtotime(@$item7->from_time))}} to {{date('h:i A',strtotime(@$item7->to_time))}}<span><a href="{{route('admin.agent.manage.availability.remove',$item7->id)}}" onclick="return confirm('Are you want to remove this availability ?')"><img src="{{ URL::to('public/admin/assets/images/cross4.png')}}"></a></span></li>
                                        @endforeach
                                    </ul>
                                </div>
                              </div>
                         </form>
                    </div>   
                    
                    
                    
                        
                    </div> 
                </div>
          
            
            
          </div>
        </div>
        <!-- End row --> 
        
      </div>
      <!-- container --> 
      
    </div>
    <!-- content -->
    
  @include('admin.includes.footer')
  </div>

  @section('scripts')

  @include('admin.includes.scripts')
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
  <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
  <script>
	// $('#demo1').timepicker({
	// timeFormat: 'h:mm:ss p'
	// });
    var choices = ["00","15","30","45"];
$('.demo1').timepicker({
      timeFormat: 'HH:mm',
        interval: 15,
        minTime: '0',
        startTime: '00:00',
        dynamic: false,
        dropdown: true,
        scrollbar: false
});


</script>

<script>
    $(document).ready(function(){
        $.validator.addMethod("timemin", function(value, element, min) {
            // var valuestart = $("select[name='timestart']").val();
            // var valuestop = $("select[name='timestop']").val();

            //create date format
            var timeStart = new Date("01/01/2007 " + min);
            var timeEnd = new Date("01/01/2007 " + value);

            var difference = timeEnd - timeStart;
            console.log(value);

            difference = difference / 60 / 60 / 1000;
            if(value>min && difference*60>=30){
                return true;
            }
            else if(value==''){
                return true;
            }
        }, "To Time 30 min geter from time");

        $.validator.addMethod("timeduration", function(value, element, min) {
            // var valuestart = $("select[name='timestart']").val();
            // var valuestop = $("select[name='timestop']").val();

            //create date format
            var timeStart = new Date("01/01/2007 " + min);
            var timeEnd = new Date("01/01/2007 " + value);

            var difference = timeEnd - timeStart;
            difference = difference / 60 / 1000;
            console.log(difference);
            if(difference % 2 === 0 ){
                return true;
            }
        }, "To Time duration multification 30 min");

        $('#mondayform').validate({
            rules: {
                fromtime:{
                    required:true
                },
                totime:{
                    required:true,
                    timemin:function(){
                        return $("#fromtime1").val();
                    },
                    timeduration:function(){
                        return $("#fromtime2").val();
                    }
                },
            },
            messages:{
                fromtime:{
                    required:"Select from time"
                },
                totime:{
                    required:"Select to time",
                },
            },
            ignore: [],
        });
        $('#tuesdayform').validate({
            rules: {
                fromtime:{
                    required:true
                },
                totime:{
                    required:true,
                    timemin:function(){
                        return $("#fromtime2").val();
                    },
                    timeduration:function(){
                        return $("#fromtime2").val();
                    }
                },
            },
            messages:{
                fromtime:{
                    required:"Select from time"
                },
                totime:{
                    required:"Select to time",
                },
            },
            ignore: [],
        });
        $('#wednesdayform').validate({
            rules: {
                fromtime:{
                    required:true
                },
                totime:{
                    required:true,
                    timemin:function(){
                        return $("#fromtime3").val();
                    },
                    timeduration:function(){
                        return $("#fromtime2").val();
                    }
                },
            },
            messages:{
                fromtime:{
                    required:"Select from time"
                },
                totime:{
                    required:"Select to time",
                },
            },
            ignore: [],
        });
        $('#thursdayform').validate({
            rules: {
                fromtime:{
                    required:true
                },
                totime:{
                    required:true,
                    timemin:function(){
                        return $("#fromtime4").val();
                    },
                    timeduration:function(){
                        return $("#fromtime2").val();
                    }
                },
            },
            messages:{
                fromtime:{
                    required:"Select from time"
                },
                totime:{
                    required:"Select to time",
                },
            },
            ignore: [],
        });
        $('#fridayform').validate({
            rules: {
                fromtime:{
                    required:true
                },
                totime:{
                    required:true,
                    timemin:function(){
                        return $("#fromtime5").val();
                    },
                    timeduration:function(){
                        return $("#fromtime2").val();
                    }
                },
            },
            messages:{
                fromtime:{
                    required:"Select from time"
                },
                totime:{
                    required:"Select to time",
                },
            },
            ignore: [],
        });
        $('#saturdayform').validate({
            rules: {
                fromtime:{
                    required:true
                },
                totime:{
                    required:true,
                    timemin:function(){
                        return $("#fromtime6").val();
                    },
                    timeduration:function(){
                        return $("#fromtime2").val();
                    }
                },
            },
            messages:{
                fromtime:{
                    required:"Select from time"
                },
                totime:{
                    required:"Select to time",
                },
            },
            ignore: [],
        });
        $('#sundayform').validate({
            rules: {
                fromtime:{
                    required:true
                },
                totime:{
                    required:true,
                    timemin:function(){
                        return $("#fromtime7").val();
                    },
                    timeduration:function(){
                        return $("#fromtime2").val();
                    }
                },
            },
            messages:{
                fromtime:{
                    required:"Select from time"
                },
                totime:{
                    required:"Select to time",
                },
            },
            ignore: [],
        });
    });
</script>

  @endsection
  @endsection
@extends('admin.layouts.app')

@section('title')
RiVirtual | Admin | Manage State
@endsection

@section('content')

@section('links')

@include('admin.includes.links')

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65046120-1', 'auto');
  ga('send', 'pageview');
</script>

@endsection

@section('header')

@include('admin.includes.header')

@endsection

@section('sidebar')

@include('admin.includes.sidebar')

@endsection

<div class="content-page">
    <!-- Start content -->
    <div class="content">
      <div class="container">

        <!-- Page-Title -->
        <div class="row">
          <div class="col-sm-12">
            <h4 class="pull-left page-title">Manage Subscription</h4>
            
            <!--<ol class="breadcrumb pull-right">
              <li><a href="#">Aariv School</a></li>
              <li><a href="#">Data Tables</a></li>
              <li class="active">Basic Tables</li>
            </ol>-->
          </div>
        </div>
        @include('admin.includes.message')
        <div class="row">
          <div class="col-md-12">

          <div class="panel panel-default">
          <div class="panel-heading rm02 rm04 rm_new01">
                <form role="form" action="{{ route('admin.manage.subscription') }}" method="get">

                  <!--<div class="form-group ">
                    <label for="">Category</label>
                    <select class="form-control rm06">
                        <option>Select</option>
                        <option>Option 1</option>
                        <option>Option 2</option>
                        <option>Option 3</option>
                        <option>Option 4</option>
                    </select>
                </div>-->
                <div class="form-group">
                    <label for="">Keyword</label>
                    <input type="text" name="keyword" value="{{ @$key['keyword'] }}" id="" class="form-control" placeholder="Enter here">
                  </div>
                  
                
                  <div class="rm05">
                    <button class="btn btn-primary waves-effect waves-light w-md" type="submit">Search</button>
                    <a href="{{ route('admin.manage.subscription') }}" class="btn btn-success waves-effect waves-light w-md">Reset</a>
                  </div>
                </form>
              </div>
          </div>


            <div class="panel panel-default">


              <div class="panel-body">
                <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="table-responsive">
                      <table class="table">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Monthly Price</th>
                            <th>Yearly Price</th>
                            <th>Status</th>
                            <th>Type</th>
                            <!-- <th>Email</th> -->
                            <th class="rm07">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          @if(count($subscription)>0)
                           @foreach($subscription as $st)
                          <tr>
                            <td>{{@$st->package_name}}</td>
                            <td>{{@$st->monthly_price}}</td>
                            <td>{{@$st->yearly_price}}</td>
                            <td class="status{{ @$st->id }}">@if(@$st->status == 'A') Active @else Inactive @endif</td>
                            <td class="type{{ @$st->id }}">@if(@$st->type == 'M' ) Monthly @else Yearly @endif</td>
                            
                            <td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action{{ @$st->id }}" onclick="fun({{ @$st->id }})"><img src="{{ url('public/admin/assets/images/action-dots.png') }}" alt=""></a>
                            <div class="show-actions" id="show-action{{ @$st->id }}" style="display: none;">
                                <span class="angle"><img src="{{ url('public/admin/assets/images/angle.png') }}" alt=""></span>
                                <ul>
                                    @if(@$st->status == 'A')
                                    <li><a href="javascript:void(0)" class="status_change" data-id="{{ $st->id }}"><span class="text_change{{ $st->id }}">Make Inactive</span></a>
                                    </li>
                                    @else
                                    <li><a href="javascript:void(0)" class="status_change" data-id="{{ $st->id }}"><span class="text_change{{ $st->id }}">Make Active</span></a></li>
                                    @endif
                                    <li><a href="{{route('admin.edit.subscription',@$st->id)}}">Edit</a></li>
                                    @if(@$st->type == 'Y')
                                    <li><a href="javascript:void(0)" class="status_type" data-id="{{ $st->id }}"><span class="text_type{{ $st->id }}">Change Type</span></a>
                                    </li>
                                    @else
                                    <li><a href="javascript:void(0)" class="status_type" data-id="{{ $st->id }}"><span class="text_type{{ $st->id }}">Change Type</span></a></li>
                                    @endif
                                </ul>
                              </div>
                            </td>
                          </tr>
                          @endforeach
                          @else
                          <tr role="row" style="text-align:center">
                              <td colspan="3">No Data Found</td>
                          </tr>
                          @endif


                        </tbody>
                      </table>
                    </div>
                    

                    <!-- <ul class="pagination">
                      <li class="paginate_button previous disabled"><a href="#">Previous</a></li>
                      <li class="paginate_button active"><a href="#">1</a></li>
                      <li class="paginate_button"><a href="#">2</a></li>
                      <li class="paginate_button"><a href="#">3</a></li>
                      <li class="paginate_button"><a href="#">4</a></li>
                      <li class="paginate_button"><a href="#">5</a></li>
                      <li class="paginate_button"><a href="#">6</a></li>
                      <li class="paginate_button next"><a href="#">Next</a></li>
                    </ul> -->


                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- End row -->

      </div>
      <!-- container -->

    </div>
    <!-- content -->

    @include('admin.includes.footer')
  </div>
  @section('scripts')

  @include('admin.includes.scripts')

   <script>
    $(document).ready(function(){

         function action(id){

           $('#show-action'+id).slideToggle();
    }
    });

   </script>
   <script>

  function fun(id){
    $('.show-actions').slideUp();
    $("#show-action"+id).show();
  }
  $(document).on('click', function () {
      var $target = $(event.target);
      if (!$target.closest('.action-dots').length && $('.show-actions').is(":visible")) {
          $('.show-actions').slideUp();
        }
    });
</script>
<script>
  $(document).on('click','.status_change', function(e){

var id = $(e.currentTarget).attr('data-id');

  if(confirm('Are you want to change this status?')){

       $.ajax({

            url: '{{ route('admin.change.sub.status') }}',
            method: 'GET',
            data: 'id='+id,

            success: function(resp){
                 $('.text_change'+id).html('');
                  if(resp != 0){

                      var str = "";
                     if(resp.status == 'A'){
                         str = "Active"
                        $(".success_msg").html("Success! Status is changed to Active!");
                        $('.text_change'+id).html('Make Inactive');

                     }else if(resp.status == 'I'){
                        str = "Inactive";
                      $(".success_msg").html("Success! Status is changed to Inactive!");
                      $('.text_change'+id).html('Make Active');

                     }
                     $('.status'+id).html(str);
                      $(".success_msg_div").show();

                     $(".error_msg_div").hide();


                  }else{

                    $(".error_msg_div").show();

                      $(".success_msg_div").hide();

                      $(".error_msg").html("Something went wrong!");
                  }


            },
            error: function(error){

               consolse.log(error);
            }
       });
  }


});
</script>
<script>
  $(document).on('click','.status_type', function(e){

  var id = $(e.currentTarget).attr('data-id');

  if(confirm('Are you want to change this type?')){

       $.ajax({

            url: '{{ route('admin.change.sub.type') }}',
            method: 'GET',
            data: 'id='+id,

            success: function(resp){
                 $('.text_type'+id).html('');
                  if(resp != 0){

                      var str = "";
                     if(resp.type == 'Y'){
                         str = "Yearly"
                        $(".success_msg").html("Success! Type is changed to Yearly!");
                        $('.text_type'+id).html('Change Type');

                     }else if(resp.type == 'M'){
                        str = "Monthly";
                      $(".success_msg").html("Success! Type is changed to Monthly!");
                      $('.text_type'+id).html('Change Type');

                     }
                     $('.type'+id).html(str);
                      $(".success_msg_div").show();

                     $(".error_msg_div").hide();


                  }else{

                    $(".error_msg_div").show();

                      $(".success_msg_div").hide();

                      $(".error_msg").html("Something went wrong!");
                  }


            },
            error: function(error){

               consolse.log(error);
            }
       });
  }


});
</script>

  @endsection
  @endsection

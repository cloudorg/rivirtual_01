<div class="topbar">
                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center">
                        <a href="#" class="logo its_for_mob ri01"><img src="{{ url('public/admin/assets/images/logo.png') }}" alt="logo image"></a>
                    </div>
                </div>
                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="">
                            <div class="pull-left">
                                <button class="button-menu-mobile open-left">
                                    <i class="fa fa-bars"></i>
                                </button>
                                <span class="clearfix"></span>
                            </div>
                            <!--<form class="navbar-form pull-left" role="search">
                                <div class="form-group">
                                    <input type="text" class="form-control search-bar" placeholder="Type here for search...">
                                </div>
                                <button type="submit" class="btn btn-search"><i class="fa fa-search"></i></button>
                            </form>-->

                            <ul class="nav navbar-nav navbar-right pull-right">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="true">
                                       @if(Auth::guard('admin')->user()->image) 
                                           
                                       <img src="{{ url('storage/app/public/admin/profile_pics/'.Auth::guard('admin')->user()->image) }}" 
                                       alt="user-img" class="img-circle">
                                      
                                      @else
                                      <img src="{{ url('public/admin/assets/images/users/avatar-1.jpg
                                    ') }}" alt="user-img" class="img-circle"> 

                                      @endif
                                      </a>
                                    <ul class="dropdown-menu">
                                        <!--<li><a href="javascript:void(0)"><i class="fas fa-user-circle"></i> Profile</a></li>-->
                                       {{-- <li><a href="{{ route('admin.profile.update') }}"><i class="fas fa-user-circle"></i>Profile</a></li>--}}
                                        <li><a href="#"><i class="fas fa-cog"></i>  Change Password</a></li>
                                        <li>
                                        <a href="{{ url('/admin/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        <i class="fas fa-power-off"></i> Logout</a></li>
                                        <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST" style="display: none;">

                                           {{ csrf_field() }}

                                        </form>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </div>
            </div>
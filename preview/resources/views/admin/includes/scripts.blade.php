
<script>
    var resizefunc = [];
</script>

 <script src="{{ url('public/admin/assets/js/jquery.min.js') }}"></script>

<script src="{{ url('public/admin/assets/js/bootstrap.min.js') }}"></script>

<script src="{{ url('public/admin/assets/js/detect.js') }}"></script>

<script src="{{ url('public/admin/assets/js/fastclick.js') }}"></script>

<script src="{{ url('public/admin/assets/js/jquery.slimscroll.js') }}"></script>

<script src="{{ url('public/admin/assets/js/jquery.blockUI.js') }}"></script>

<script src="{{ url('public/admin/assets/js/waves.js') }}"></script>

<script src="{{ url('public/admin/assets/js/wow.min.js') }}"></script>

<script src="{{ url('public/admin/assets/js/jquery.nicescroll.js') }}"></script>

<script src="{{ url('public/admin/assets/js/jquery.scrollTo.min.js') }}"></script>

<script src="{{ url('public/admin/assets/js/jquery.app.js') }}"></script>
<script src="{{ url('public/admin/assets/js/jquery-ui.js') }}"></script>
<script src="{{ url('public/admin/assets/js/jquery.timepicker.js') }}"></script>

<!-- <script src="{{ url('public/admin/assets/js/tinymce.min.js') }}"></script> -->

<!-- <script src="{{ url('public/admin/assets/js/custom.js') }}"></script> -->

<!-- jQuery  -->
<script src="{{ url('public/admin/assets/plugins/moment/moment.js') }}"></script>
        
        <!-- jQuery  -->
        <script src="{{ url('public/admin/assets/plugins/waypoints/lib/jquery.waypoints.js') }}"></script>
        <script src="{{ url('public/admin/assets/plugins/counterup/jquery.counterup.min.js') }}"></script>
        
        <!-- jQuery  -->
        <script src="{{ url('public/admin/assets/plugins/sweetalert/dist/sweetalert.min.js') }}"></script>
        
        
        <!-- flot Chart -->
        <!-- <script src="{{ url('public/admin/assets/plugins/flot-chart/jquery.flot.js') }}"></script>
        <script src="{{ url('public/admin/assets/plugins/flot-chart/jquery.flot.time.js') }}"></script>
        <script src="{{ url('public/admin/assets/plugins/flot-chart/jquery.flot.tooltip.min.js') }}"></script>
        <script src="{{ url('public/admin/assets/plugins/flot-chart/jquery.flot.resize.js') }}"></script>
        <script src="{{ url('public/admin/assets/plugins/flot-chart/jquery.flot.pie.js') }}"></script>
        <script src="{{ url('public/admin/assets/plugins/flot-chart/jquery.flot.selection.js') }}"></script>
        <script src="{{ url('public/admin/assets/plugins/flot-chart/jquery.flot.stack.js') }}"></script>
        <script src="{{ url('public/admin/assets/plugins/flot-chart/jquery.flot.crosshair.js') }}"></script> -->

        <!-- jQuery  -->
        <script src="{{ url('public/admin/assets/pages/jquery.todo.js') }}"></script>
        
        <!-- jQuery  -->
        <script src="{{ url('public/admin/assets/pages/jquery.chat.js') }}"></script>
        
        <!-- jQuery  -->
        <!--<script src="{{ url('public/admin/assets/pages/jquery.dashboard.js') }}"></script>-->
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
        <script src="https://kit.fontawesome.com/446e463225.js" crossorigin="anonymous"></script>
        <script src="{{ url('public/admin/assets/js/bundle.min.js') }}"></script>
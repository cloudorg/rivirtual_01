@extends('admin.layouts.app')
@section('title')
RiVirtual | Admin | Login
@endsection
@section('content')
@section('links')
@include('admin.includes.links')

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65046120-1', 'auto');
  ga('send', 'pageview');

</script>
@endsection

<div class="wrapper-page">
            <div class="panel panel-color panel-primary panel-pages">
                <div class="panel-heading rm08">
                    <img src="{{ url('public/admin/assets/images/logo.png') }}" alt="">
                </div>

                <div class="panel-body">
                @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>Success!</strong> {{ session('success') }}
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>Error!</strong> {{ session('error') }}
            </div>
            @endif
                <form class="form-horizontal m-t-20"  action="{{ route('admin.login') }}" id="loginForm" method="post" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control input-lg required {{ $errors->has('email') ? 'is-invalid' : '' }} " type="text"
                            name="email" id="email" placeholder="Email" autocomplete="off" value="{{ @Cookie::get('admin_email') == null ? old('email'): @Cookie::get('admin_email')}}">
                            @if ($errors->has('email'))
                           <span class=" text-danger" role="alert">
                           <strong style="font-size: 14px;">{{ $errors->first('email') }}</strong>
                         </span>
                           @endif
                        </div>
                    </div>

                    <div class="form-group password-input">
                        <div class="col-xs-12 ">
                            <input class="form-control input-lg required {{ $errors->has('password') ? 'is-invalid' : '' }}" type="password"
                            placeholder="Password" name="password" id="password" value="{{ @Cookie::get('admin_password') }}">
                            <span toggle="#password" class="fa fa-fw field-icon toggle-password password-show-icon fa-eye-slash"></span>
                            @if ($errors->has('password'))
                         <span class="invalid-feedback" role="alert">
                         <strong style="font-size: 14px;">{{ $errors->first('password') }}</strong>
                        </span>
                          @endif
                        </div>

                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="checkbox checkbox-primary">
                                <input id="checkbox-signup" type="checkbox" name="remember" id="remember" @if(@Cookie::get('admin_email') != null) checked @endif>
                                <label for="checkbox-signup">
                                    Remember me
                                </label>
                            </div>

                        </div>
                    </div>

                    <div class="form-group text-center m-t-40">
                        <div class="col-xs-12">
                        <button class="btn btn-primary btn-lg w-lg waves-effect waves-light rm01" type="submit">Log In</button>
                        </div>
                    </div>

                    <div class="form-group m-t-30">
                        <div class="col-sm-12">
                            <a href="{{ route('admin.password.request') }}" class="rm01"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>
                        </div>
                        <!--<div class="col-sm-5 text-right">
                            <a href="register.html">Create an account</a>
                        </div>-->
                    </div>
                </form>
                </div>

            </div>
        </div>

@section('scripts')

@include('admin.includes.scripts')
<script>
    $(document).ready(function(){
        $("#loginForm" ).validate({

             rules: {

                   email: {

                        required: true,
                        email: true,
                   },

                   password: {

                         required: true,
                   }

             },
             messages: {

                  email: {

                        required: 'Please Enter your Email',
                        email: 'please Enter valid Email'
                  },
                  password: {

                        required: 'Please Enter your Password'
                  }
             }

        });
    });
</script>
<script>
    $(".toggle-password").click(function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>
@endsection
@endsection


@extends('admin.layouts.app')
@section('title')
Codebusted | Admin | Reset Password
@endsection
@section('content')
@section('links')

@include('admin.includes.links')

@endsection

<div class="wrapper-page">
    <div class="panel panel-color panel-primary panel-pages">
        <div class="panel-heading rm08"> 
            <a href="{{ route('admin.login') }}"><img src="{{ url('public/admin/assets/images/Logo.png') }}" alt="" style="width: 100px;"></a>                    
        </div> 
        <div class="panel-body">
            <form class="form-horizontal m-t-20" method="POST" action="{{ route('admin.password.request') }}" aria-label="{{ __('Login') }}" id="loginForm2">    
                @csrf
                <input type="hidden" name="email" value="{{$email}}"> 
                               
                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control input-lg" required="" id="password" name="password" type="password" placeholder="Password" autocomplete="off">
                        @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong style="font-size: 14px;">{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control input-lg" required="" id="password_confirmation" name="password_confirmation" type="password" placeholder="Comfirm Password" autocomplete="off">
                        @if ($errors->has('password_confirmation'))
                        <span class="invalid-feedback" role="alert">
                            <strong style="font-size: 14px;">{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group text-center m-t-40">
                    <div class="col-xs-12">
                        <button class="btn btn-primary btn-lg w-lg waves-effect waves-light rm01" type="submit">{{ __('Reset Password') }}</button>
                    </div>
                </div>
            </form> 
        </div>                                 

    </div>
</div>
@section('scripts')

@include('admin.includes.scripts')

<script>
    $(document).ready(function(){ 
        $('#loginForm2').validate({ 
          rules: {
              password: {
                  required: true,
                  minlength:6
              },
              password_confirmation: {
                  required: true,
                  minlength:6,
                  equalTo: "#password"
              },
          },
      });
    });
</script>

@endsection
@endsection
@extends('admin.layouts.app')
@section('title')
Codebusted | Admin | Reset Link To Email
@endsection
@section('content')
@section('links')
@include('admin.includes.links')

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65046120-1', 'auto');
  ga('send', 'pageview');

</script>

@endsection

<div class="wrapper-page">
            <div class="panel panel-color panel-primary panel-pages">

                
                
                
                <div class="panel-heading rm08"> 
                    <img src="{{ url('public/admin/assets/images/logo.png') }}" alt="logo image">
                </div>

                <div class="panel-body">
                @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>Success!</strong> {{ session('success') }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>Error!</strong> {{ session('error') }} 
            </div>
            @endif
                 <form method="post" action="{{ route('admin.password.email') }}" role="form" class="text-center" id="resetForm">
                     {{ csrf_field() }} 
                    <div class="alert alert-info alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        Enter your <b>Email</b> and instructions will be sent to you!
                    </div>
                    <div class="form-group m-b-0"> 
                        <div class="input-group"> 
                            <input type="email" name="email" class="form-control input-lg {{ $errors->has('email') ? ' is-invalid' : '' }}" 
                            placeholder="Enter Email" value="{{ old('email') }}">
                            @if ($errors->has('email'))
                           <span class="text-danger">
                           {{ $errors->first('email') }}
                         </span>
                           @endif
                            <span class="input-group-btn"> 
                            <button type="submit" class="btn btn-lg btn-primary waves-effect waves-light">Reset</button>
                           </span>
                        
                        </div> 
        
                    </div> 
                </form>

                </div>                                 
                
            </div>
        </div>
     @section('scripts')
     @include('admin.includes.scripts')
     <script>
       $(document).ready(function(){
     
           $('#resetForm').validate({

                  rules: {
                       
                       email: {

                             required: true,
                             email: true,
                       }
                         
                  },
                  messages: {

                        email: {

                             required: 'please Enter Email',
                             email: 'please Enter valid Email'
                        }
                  }
           });
              
       });

     </script>

     @endsection
     @endsection
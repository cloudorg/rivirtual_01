<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <meta name="twitter:site" content="@RiVirtual">
    <meta name="twitter:title" content="RiVirtual: #1 Real Estate Advisor Platform in India">
    <meta name="twitter:description" content="Find real estate for sale, lease & auction on the # 1 real estate advisor  and advertising marketplace."> 
    <link rel="icon" type="image/png" sizes="32x32" href="{{ url('public/admin/assets/images/favicon.ico')}}">

    @yield('title','RiVirtual')

    @yield('style')
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-77HVYFKHNB"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
gtag('config', 'G-77HVYFKHNB');
</script>

<!-- Default Statcounter code for Real Estate  -->
<script type="text/javascript">
var sc_project=12735833; 
var sc_invisible=1; 
var sc_security="5eafa64c"; 
</script>
<script type="text/javascript"
src="https://www.statcounter.com/counter/counter.js" async></script>
<noscript><div class="statcounter"><a title="Web Analytics"
href="https://statcounter.com/" target="_blank"><img class="statcounter"
src="https://c.statcounter.com/12735833/0/5eafa64c/1/" alt="Web Analytics"
referrerPolicy="no-referrer-when-downgrade"></a></div></noscript>
<!-- End of Statcounter Code -->

<script>(function(){var js,fs,d=document,id="tars-widget-script",b="https://tars-file-upload.s3.amazonaws.com/bulb/";if(!d.getElementById(id)){js=d.createElement("script");js.id=id;js.type="text/javascript";js.src=b+"js/widget.js";fs=d.getElementsByTagName("script")[0];fs.parentNode.insertBefore(js,fs)}})();window.tarsSettings = {"convid":"hyIzp0"};</script>

</head>
<body>
    @yield('header')
    @yield('content')
    @yield('footer')


    @yield('script')
</body>
</html>

@if(Auth::user()->user_type=='U')

<div class="user_dashboard_left">
    <div class="mobile_menu"> <i class="fa fa-bars" aria-hidden="true"></i>
        <span>Show Menu</span>
    </div>
    <div class="user_dash_lf_inr" id="mobile_menu_dv">
        <div class="user_dash_pro">
            <em>
            @if(auth()->user()->profile_pic != null)
            <img src="{{ URL::to('storage/app/public/profile_picture')}}/{{auth()->user()->profile_pic}}" alt="">
            @else
            <img src="{{ URL::to('public/frontend/images/avatar.png')}}" alt="" id="img2">
            @endif
            </em>
            <h4>{{auth()->user()->name}}</h4>
            <h6>{{auth()->user()->email}}</h6>
        </div>
        <div class="user_dash_menu">
            <ul>
                <li><a href="{{route('user.dashboard')}}" @if(Request::segment(1)=="dashboard") class="actv" @endif><em><img src="{{ URL::to('public/frontend/images/dasicon1.png')}}" alt=""></em>Dashboard</a></li>
                <li><a href="{{route('user.profile')}}" @if(Request::segment(1)=="profile" ) class="actv" @endif><em><img src="{{ URL::to('public/frontend/images/dasicon2.png')}}" alt=""></em>Edit profile</a></li>
                <li><a href="{{ route('search.service.pro') }}"><em><img src="{{ URL::to('public/frontend/images/dasicon3.png')}}" alt=""></em>Hire a PRO</a></li>
                <li><a href="{{route('user.post.job')}}" @if(Request::segment(1)=="user-post-service-requirments") class="actv" @endif><em><img src="{{ URL::to('public/frontend/images/dasicon4.png')}}" alt=""></em>Post Service Requirements</a></li>
                <li><a href="{{ route('user.my.post.job') }}" @if(Request::segment(1)=="user-my-service-requirments" || Request::segment(1)=="user-proposal-list") class="actv" @endif><em><img src="{{ URL::to('public/frontend/images/dasicon5.png')}}" alt=""></em>My Service Requirements </a></li>
                <li><a href="{{ route('user.property.visit.request') }}" @if(Request::segment(1)=="user-upcoming-property-visit") class="actv" @endif><em><img src="{{ URL::to('public/frontend/images/dasicon7.png')}}" alt=""></em>Upcoming Property Visits</a></li>
                <li><a href="{{ route('user.save.property') }}" @if(Request::segment(1)=="user-save-search-property" || Request::segment(1)=="user-save-search-agent" || Request::segment(1)=="user-save-search-pro") class="actv" @endif><em><img src="{{ URL::to('public/frontend/images/dasicon8.png')}}" alt=""></em>Saved Searches</a></li>
                <li><a href="{{ route('show.message') }}"><em><img src="{{ URL::to('public/frontend/images/dasicon9.png')}}" alt=""></em>Chat History</a></li>
                <li><a href="{{route('user.logout')}}"><em><img src="{{ URL::to('public/frontend/images/dasicon10.png')}}" alt=""></em>Logout</a></li>
            </ul>
        </div>
    </div>
</div>
@endif
@if(Auth::user()->user_type=='A')
<div class="col-lg-3 col-md-12 col-sm-12">
    <div class="cusdashb-left">
        <div class="mobile_menu"> <i class="fa fa-bars" aria-hidden="true"></i>
            <span>Show Menu</span>
        </div>
        <div class="user_dash_lf_inr left-profle-agent" id="mobile_menu_dv">
        <div class="user_dash_pro">
            {{-- <em><img src="{{ URL::to('public/frontend/images/agent-users.png')}}" alt=""></em> --}}
            <em>
                @if(auth()->user()->profile_pic != null)
                <img src="{{ URL::to('storage/app/public/profile_picture')}}/{{auth()->user()->profile_pic}}" alt="">
                @else
                <img src="{{ URL::to('public/frontend/images/avatar.png')}}" alt="" id="img2">
                @endif
                </em>
            <h4>{{auth()->user()->name}}</h4>
            <h6>{{auth()->user()->email}}</h6>
        </div>
        <div class="user_dash_menu">
            <ul>
                <li><a href="{{route('agent.dashboard')}}"  @if(Request::segment(2)=="dashboard") class="actv" @endif><em><img src="{{ URL::to('public/frontend/images/ad1.png')}}" alt="" class="hovern"><img src="{{ URL::to('public/frontend/images/ad1h.png')}}" alt="" class="hoverb"></em>Dashboard</a></li>
                <li><a href="{{route('agent.profile')}}"  @if(Request::segment(2)=="profile") class="actv" @endif><em><img src="{{ URL::to('public/frontend/images/ad2.png')}}" alt="" class="hovern"><img src="{{ URL::to('public/frontend/images/ad2h.png')}}" alt="" class="hoverb"></em>Edit profile</a></li>
                <li><a href="{{ route('agent.subcription') }}" @if(Request::segment(2)=="agent-subcription") class="actv" @endif><em><img src="{{ URL::to('public/frontend/images/d3.png')}}" alt="" class="hovern"><img src="{{ URL::to('public/frontend/images/d3h.png')}}" alt="" class="hoverb"></em>Subscription</a></li>
                <li><a href="{{route('agent.add.property')}}" @if(Request::segment(2)=="add-property-details" || Request::segment(2)=="add-property-image") class="actv" @endif><em><img src="{{ URL::to('public/frontend/images/ad4.png')}}" alt="" class="hovern"><img src="{{ URL::to('public/frontend/images/ad4h.png')}}" alt="" class="hoverb"></em>Add Property</a></li>
                <li><a href="{{route('agent.my.property')}}" @if(Request::segment(2)=="my-property") class="actv" @endif><em><img src="{{ URL::to('public/frontend/images/ad5.png')}}" alt="" class="hovern"><img src="{{ URL::to('public/frontend/images/d5h.png')}}" alt="" class="hoverb"></em>My Property </a></li>
                <li><a href="{{route('agent.manage.availability')}}" @if(Request::segment(2)=="manage-availability") class="actv" @endif><em><img src="{{ URL::to('public/frontend/images/ad6.png')}}" alt="" class="hovern"><img src="{{ URL::to('public/frontend/images/d6h.png')}}" alt="" class="hoverb"></em>Manage Visit Availability </a></li>
                <li><a href="{{ route('agent.visit.request') }}" @if(Request::segment(2)=="visit-request"||Request::segment(2)=="view-profile") class="actv" @endif><em><img src="{{ URL::to('public/frontend/images/ad7.png')}}" alt="" class="hovern"><img src="{{ URL::to('public/frontend/images/ad7h.png')}}" alt="" class="hoverb"></em>View Visit Requests </a></li>
                <li><a href="{{ route('show.message') }}"><em><img src="{{ URL::to('public/frontend/images/ad8.png')}}" alt="" class="hovern"><img src="{{ URL::to('public/frontend/images/ad8h.png')}}" alt="" class="hoverb"></em>Chat History </a></li>
                <li><a href="{{ route('agent.rating.review') }}"  @if(Request::segment(2)=="agent-rating-review") class="actv" @endif><em><img src="{{ URL::to('public/frontend/images/ad9.png')}}" alt="" class="hovern"><img src="{{ URL::to('public/frontend/images/adh.png')}}" alt="" class="hoverb"></em>View Reviews & Rating</a></li>
                <li><a href="{{route('user.logout')}}"><em><img src="{{ URL::to('public/frontend/images/ad10.png')}}" alt="" class="hovern"><img src="{{ URL::to('public/frontend/images/ad10h.png')}}" alt="" class="hoverb"></em>Logout</a></li>
            </ul>
        </div>
    </div>
    </div>
</div>
@endif
@if(Auth::user()->user_type=='S')
<div class="col-lg-3 col-md-12 col-sm-12">
    <div class="cusdashb-left dasbordsLeftsec">
        <div class="mobile_menu"> <i class="fa fa-bars" aria-hidden="true"></i>
            <span>Show Menu</span>
        </div>
        <div class="user_dash_lf_inr left-profle-provider" id="mobile_menu_dv">
        <div class="user_dash_pro pro-dash-top">
            <em>
                @if(auth()->user()->profile_pic != null)
                <img src="{{ URL::to('storage/app/public/profile_picture')}}/{{auth()->user()->profile_pic}}" alt="">
                @else
                <img src="{{ URL::to('public/frontend/images/avatar.png')}}" alt="" id="img2">
                @endif
            </em>
            <div class="d-flex align-items-center justify-content-center pro-n-ra">
                <h4>{{auth()->user()->name}}</h4>
                <span><i class="fa fa-star"></i>{{ number_format(auth()->user()->avg_review,1,'.',',') }}</span>
            </div>
            <h6>{{auth()->user()->email}}</h6>
        </div>
        <div class="user_dash_menu">
            <ul>
                <li><a href="{{route('service.provider.dashboard')}}"  @if(Request::segment(2)=="dashboard") class="actv" @endif><em><img src="{{ URL::to('public/frontend/images/ad1.png')}}" alt="" class="hovern"><img src="{{ URL::to('public/frontend/images/ad1h.png')}}" alt="" class="hoverb"></em>Dashboard</a></li>
                <li><a href="{{route('service.provider.profile')}}"  @if(Request::segment(2)=="profile" || Request::segment(2)=="work-images") class="actv" @endif><em><img src="{{ URL::to('public/frontend/images/ad2.png')}}" alt="" class="hovern"><img src="{{ URL::to('public/frontend/images/ad2h.png')}}" alt="" class="hoverb"></em>Edit profile</a></li>
                <li><a href="{{ route('search.job') }}" @if(Request::segment(2)=="find-jobs") class="actv" @endif><em><img src="{{ URL::to('public/frontend/images/ad11.png')}}" alt="" class="hovern"><img src="{{ URL::to('public/frontend/images/ad11h.png')}}" alt="" class="hoverb"></em>Find a Job </a></li>

                <li><a href="{{route('service.provider.my.quotes')}}" @if(Request::segment(2)=="my-quotes" || Request::segment(2)=="view-quote") class="actv" @endif ><em><img src="{{ URL::to('public/frontend/images/ad14.png')}}" alt="" class="hovern"><img src="{{ URL::to('public/frontend/images/ad14h.png')}}" alt="" class="hoverb"></em>My Quotes</a></li>
                <li><a href="{{ route('pro.rating.review') }}"  @if(Request::segment(2)=="pro-rating-review") class="actv" @endif><em><img src="{{ URL::to('public/frontend/images/ad9.png')}}" alt="" class="hovern"><img src="{{ URL::to('public/frontend/images/adh.png')}}" alt="" class="hoverb"></em>View Reviews & Rating </a></li>
                <li><a href="{{route('service.provider.membership')}}" @if(Request::segment(2)=="membership") class="actv" @endif><em><img src="{{ URL::to('public/frontend/images/ad15.png')}}" alt="" class="hovern"><img src="{{ URL::to('public/frontend/images/ad15h.png')}}" alt="" class="hoverb"></em>Memberships  </a></li>
                <li><a href="javascript:;"><em><img src="{{ URL::to('public/frontend/images/ad16.png')}}" alt="" class="hovern"><img src="{{ URL::to('public/frontend/images/ad16h.png')}}" alt="" class="hoverb"></em>Payments </a></li>
                <li><a href="{{ route('show.message') }}"><em><img src="{{ URL::to('public/frontend/images/ad8.png')}}" alt="" class="hovern"><img src="{{ URL::to('public/frontend/images/ad8h.png')}}" alt="" class="hoverb"></em>Chat History</a></li>
                <li><a href="{{route('user.logout')}}"><em><img src="{{ URL::to('public/frontend/images/ad10.png')}}" alt="" class="hovern"><img src="{{ URL::to('public/frontend/images/ad10h.png')}}" alt="" class="hoverb"></em>Logout</a></li>
            </ul>
        </div>
    </div>
    </div>
</div>
@endif

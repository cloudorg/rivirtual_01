
<link href="{{ URL::to('public/frontend/css/bootstrap.css')}}" rel="stylesheet">
<link href="{{ URL::to('public/frontend/css/style.css')}}" rel="stylesheet">
<link href="{{ URL::to('public/frontend/css/responsive.css')}}" rel="stylesheet">
<link href="{{ URL::to('public/frontend/css/owl.carousel.css')}}" rel="stylesheet">
<link href="{{ URL::to('public/frontend/css/jquery-ui.css')}}" rel="stylesheet">
<link href="{{ URL::to('public/frontend/css/font-awesome.css')}}" rel="stylesheet">
<link href="{{ URL::to('public/frontend/icofont/icofont.min.css')}}" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,200;1,300;1,400" rel="stylesheet">
<link href="{{ URL::to('public/frontend/css/cale1.css')}}" rel="stylesheet">
<link rel='stylesheet' href="{{ URL::to('public/frontend/css/jquery-clockpicker.min.css')}}">
<link rel="stylesheet" href="{{ URL::to('public/frontend/css/jquery-ui1.css')}}">
<link href="{{ URL::to('public/frontend/css/ninja-slider.css')}}" rel="stylesheet" type="text/css">
<link href="{{ URL::to('public/frontend/css/thumbnail-slider.css')}}" rel="stylesheet" type="text/css">
<link href="{{ URL::to('public/frontend/css/calendar.css')}}" rel="stylesheet" type="text/css">
<link href="{{ URL::to('public/frontend/icofont/icofont.min.css')}}" rel="stylesheet">
<style>
    .chec_bx label .error{
        color: red !important;
    }
    .social_btn{
        display: inline-block !important;
        color: #fff !important;
    }
    .sign_popup_body .radiobx .error{
        color: red !important;
    }
</style>

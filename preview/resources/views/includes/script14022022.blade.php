<script src="{{ URL::to('public/frontend/js/jquerymin.js')}}"></script>
<script src="{{ URL::to('public/frontend/js/bootstrap.js')}}"></script>
<script src="{{ URL::to('public/frontend/js/carouselscript.js')}}"></script>
<script src="{{ URL::to('public/frontend/js/owl.carousel.js')}}"></script>
 <script src="{{ URL::to('public/frontend/js/custom-file-input.js')}}"></script>
<script src="{{ URL::to('public/frontend/js/calendar1.js')}}"></script>

<script src="{{ URL::to('public/frontend/js/bundle.min.js')}}"></script>
<script src="{{ URL::to('public/frontend/js/jquery-ui.js')}}"></script>
<script src="{{ URL::to('public/frontend/js/pinterest_grid.js')}}"></script>
<script src="{{ URL::to('public/frontend/js/ninja-slider.js')}}"></script>
<script src="{{ URL::to('public/frontend/js/thumbnail-slider.js')}}"></script>
{{-- <script src="{{ URL::to('public/frontend/js/calendar.js')}}"></script> --}}
<script src="{{ URL::to('public/frontend/js/custom.js')}}"></script>
{{-- jquery-validator --}}
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

<script>
    $('body').on('click','.resetcode',function(){
     var id = $('#u_id').val();
     
     console.log("ID: " + id);
     
     var reqData = {
        "jsonrpc":"2.0",
        "_token":"{{ csrf_token() }}",
        "data":{ 
              id:id,
            }
    };
    $.ajax({
      'url':"{{route('resend.code')}}",
      'type':"post",
      'data': reqData,
        success:function(response){
            console.log(response)
            $('#otptext').text('Enter the 6 digit code sent to you at +91' +response.result.data.mobile_number);
            $('#otp').text(response.result.data.otp_mobile);
            $('#u_id').val(response.result.data.id);
            $('#user_id').val(response.result.data.id);
            $('#otpopup .alert-danger').css('display','none');
            $('#otpopup .alert-success').css('display','block');
            $('#otpopup .alert-success .success-message ').text('A verification link has been sent to your email account. Please verify.');
            $('.openotp').click();
        }
      })
     
    });
</script>


<script>
    $(".toggle-password").click(function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>
{{-- Model Open Script --}}
<script>
$(document).ready(function(){
    $(".opensignin").click(function(){
        $('#modalsignin2').modal('hide');
        $('#forgotpass').modal('hide');
        $('#otpopup').modal('hide');
        $('#modalagentsignin').modal('hide');
        $('#modalproignin').modal('hide');
        $('#modalagentsignup').modal('hide');
        $('#modalproignup').modal('hide');
        $('#forgototpopup').modal('hide');
        $('#modalchangepassword').modal('hide');
        $('#modalsignin').modal('show');
        $('#userloginbtn').show();
        $('#userlogina').hide();
        $('#modalsignin .alert-success').css('display','none');
        $('#modalsignin .alert-danger').css('display','none');
    });
    $(".opensignup").click(function(){
        $('#modalsignin').modal('hide');
        $('#forgotpass').modal('hide');
        $('#otpopup').modal('hide');
        $('#modalagentsignin').modal('hide');
        $('#modalproignin').modal('hide');
        $('#modalagentsignup').modal('hide');
         $('#modalproignup').modal('hide');
         $('#forgototpopup').modal('hide');
         $('#modalchangepassword').modal('hide');
        $('#modalsignin2').modal('show');
        $('#userbtn').show();
        $('#usera').hide();
        $('#modalsignin2 .alert-success').css('display','none');
        ('#modalsignin2 .alert-danger').css('display','none');
    });
    $(".openpass").click(function(){
        $('#modalsignin').modal('hide');
        $('#otpopup').modal('hide');
        $('#modalsignin2').modal('hide');
        $('#modalagentsignin').modal('hide');
        $('#modalproignin').modal('hide');
        $('#modalagentsignup').modal('hide');
        $('#modalproignup').modal('hide');
        $('#forgototpopup').modal('hide');
        $('#modalchangepassword').modal('hide');
        $('#forgotpass').modal('show');
        $('#forgotpass .alert-success').css('display','none');
        $('#forgotpass .alert-danger').css('display','none');
    });
    $(".openotp").click(function(){
        $('#modalsignin').modal('hide');
        $('#modalsignin2').modal('hide');
        $('#forgotpass').modal('hide');
        $('#modalagentsignin').modal('hide');
        $('#modalproignin').modal('hide');
        $('#modalagentsignup').modal('hide');
         $('#modalproignup').modal('hide');
         $('#forgototpopup').modal('hide');
         $('#modalchangepassword').modal('hide');
        $('#otpopup').modal('show');
    });
    $(".agentlogin").click(function(){
        $('#modalsignin').modal('hide');
        $('#modalsignin2').modal('hide');
        $('#forgotpass').modal('hide');
        $('#otpopup').modal('hide');
        $('#modalproignin').modal('hide');
        $('#modalagentsignup').modal('hide');
        $('#modalproignup').modal('hide');
        $('#forgototpopup').modal('hide');
        $('#modalchangepassword').modal('hide');
        $('#modalagentsignin').modal('show');
        $('#agentloginbtn').show();
        $('#agentlogina').hide();
        $('#modalagentsignin .alert-success').css('display','none');
        $('#modalagentsignin .alert-danger').css('display','none');
    });
    $(".prologin").click(function(){
        $('#modalsignin').modal('hide');
        $('#modalsignin2').modal('hide');
        $('#forgotpass').modal('hide');
        $('#otpopup').modal('hide');
        $('#modalagentsignin').modal('hide');
        $('#modalagentsignup').modal('hide');
        $('#modalproignup').modal('hide');
        $('#forgototpopup').modal('hide');
        $('#modalchangepassword').modal('hide');
        $('#modalproignin').modal('show');
        $('#serviceloginbtn').show();
        $('#servicelogina').hide();
        $('#modalproignin .alert-success').css('display','none');
        $('#modalproignin .alert-danger').css('display','none');
    });
    $(".openagentsignup").click(function(){
        $('#modalsignin').modal('hide');
        $('#modalsignin2').modal('hide');
        $('#forgotpass').modal('hide');
        $('#otpopup').modal('hide');
        $('#modalagentsignin').modal('hide');
        $('#modalproignin').modal('hide');
        $('#modalproignup').modal('hide');
        $('#modalagentsignup').modal('show');
        $('#forgototpopup').modal('hide');
        $('#modalchangepassword').modal('hide');
        $('#agentbtn').show();
        $('#agenta').hide();
        $('#modalagentsignup .alert-success').css('display','none');
        $('#modalagentsignup .alert-danger').css('display','none');
    });
     $(".openprosignup").click(function(){
        $('#modalsignin').modal('hide');
        $('#modalsignin2').modal('hide');
        $('#forgotpass').modal('hide');
        $('#otpopup').modal('hide');
        $('#modalagentsignin').modal('hide');
        $('#modalproignin').modal('hide');
        $('#modalagentsignup').modal('hide');
        $('#modalproignup').modal('show');
        $('#forgototpopup').modal('hide');
        $('#modalchangepassword').modal('hide');
        $('#modalchangepassword').modal('hide');
        $('#servicebtn').show();
        $('#servicea').hide();
        $('#modalproignup .alert-success').css('display','none');
        $('#modalproignup .alert-danger').css('display','none');
    });
    $(".forgototp").click(function(){
        $('#modalsignin').modal('hide');
        $('#modalsignin2').modal('hide');
        $('#forgotpass').modal('hide');
        $('#modalagentsignin').modal('hide');
        $('#modalproignin').modal('hide');
        $('#modalagentsignup').modal('hide');
        $('#modalproignup').modal('hide');
        $('#otpopup').modal('hide');
        $('#modalchangepassword').modal('hide');
        $('#forgototpopup').modal('show');
        $('#forgototpopup .alert-success').css('display','none');
        $('#forgototpopup .alert-danger').css('display','none');
    });
    $(".changepasswordclick").click(function(){
        $('#modalsignin').modal('hide');
        $('#modalsignin2').modal('hide');
        $('#forgotpass').modal('hide');
        $('#modalagentsignin').modal('hide');
        $('#modalproignin').modal('hide');
        $('#modalagentsignup').modal('hide');
        $('#modalproignup').modal('hide');
        $('#otpopup').modal('hide');
        $('#forgototpopup').modal('hide');
        $('#modalchangepassword').modal('show');
        $('#modalchangepassword .alert-success').css('display','none');
        $('#modalchangepassword .alert-danger').css('display','none');
    });

    // $('.user_google').click(function(){
    //     var url= '{{route('home')}}'+'/login/user/google';
    //     window.location = url;
    // })
    // $('.user_facebook').click(function(){
    //     var url= '{{route('home')}}'+'/login/user/facebook';
    //     window.location = url;
    // })
});
</script>
{{-- Model Open Script --}}

{{-- Singup Script --}}
<script>
    $(document).ready(function(){
        jQuery.validator.addMethod("validate_word", function(value, element) {
        if (/^([a-zA-Z ])+$/.test(value)) {
            return true;
        } else {
            return false;
        }
    }, "Not allow special characters or numbers");

        jQuery.validator.addMethod("validate_email", function(value, element) {
            // var testEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z]{2,4})+$/;
            var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,5}$/i;
            if (testEmail.test(value)) {
                return true;
            } else {
                return false;
            }
        }, "Oops...! check your Email again ");

        $("#usersingupform").validate({
            rules: {
                userfname:{
                    validate_word:true,
                },
                userlname:{
                    validate_word:true,
                },
                useremail: {
                    required: true,
                    // email: true,
                    validate_email:true,
                    remote: {
                        url: '{{ route("check.email") }}',
                        dataType: 'json',
                        type:'post',
                        data: {
                            email: function() {
                                return $('#useremail').val();
                            },
                            _token: '{{ csrf_token() }}'
                        }
                    },
                },
                userpassword:{
                    required: true,
                    minlength: 8
                },
                usercpassword:{
                    required: true,
                    minlength: 8,
                    equalTo : "#userpassword"
                },
                usermob:{
                    required: true,
                    digits: true,
                    minlength: 10,
                    maxlength: 10,
                    remote: {
                        url: '{{ route("check.mobile") }}',
                        dataType: 'json',
                        type:'post',
                        data: {
                            mobile: function() {
                                return $('#usermob').val();
                            },
                            _token: '{{ csrf_token() }}'
                        }
                    },
                },
                accept_user_terms:{
                    required:true,
                },
            },
            messages: {
                useremail:{
                    remote:'Email already in Use ',
                    email:'Oops...! check your Email again '
                },
                usermob:{
                    remote:'Mobile Number already in Use ',
                    digits:"Please enter a valid number",
                },
                usercpassword:{
                    equalTo : "Confirm password not match "
                },
                accept_user_terms:{
                    required:'Please accept terms and conditions',
                },
            },
            submitHandler: function (form) {
                if($('#accept_user_terms').is(":checked")){
                    console.log('test');
                    $('#userbtn').hide();
                    $('#usera').show();
                    $.post("{{ route('new.register') }}",{
                        jsonrpc: 2.0,
                        _token: "{{ csrf_token() }}",
                        params: {
                            fname: $('#userfname').val(),
                            lname: $('#userlname').val(),
                            email: $('#useremail').val(),
                            mobile: $('#usermob').val(),
                            password: $('#userpassword').val(),
                            cpassword: $('#usercpassword').val(),
                            userType: 'U'
                        }
                    })
                    .done(response => {
                        console.log(response);
                        if (response.error && response.error.message) {
                            $('#userbtn').show();
                            $('#usera').hide();
                            $('#modalsignin2 .alert-success').css('display','none');
                            $('#modalsignin2 .alert-danger').css('display','block');
                            $('#modalsignin2 .alert-danger .error-message').text(response.error.message);
                        }else{
                            $('#otptext').text('Enter the 6 digit code sent to you at +91' +response.result.data.mobile_number);
                            $('#otp').text(response.result.data.otp_mobile);
                            $('#u_id').val(response.result.data.id);
                            $('#user_id').val(response.result.data.id);
                            $('#otpopup .alert-danger').css('display','none');
                            $('#otpopup .alert-success').css('display','block');
                            $('#otpopup .alert-success .success-message ').text('A verification link has been sent to your email account. Please verify.');
                            $('.openotp').click();
                        }
                    })
                    .fail(error => {

                    })

                }else{
                    $('#accept_user_terms-error').html('Please accept terms and conditions');
                    $('#accept_user_terms-error').css('display', 'block');
                    return false;
                }

            }
        });

        $("#agentingupform").validate({
            rules: {
                agentfname:{
                    validate_word:true,
                },
                agentlname:{
                    validate_word:true,
                },
                agentemail: {
                    required: true,
                    email: true,
                    validate_email:true,
                    remote: {
                        url: '{{ route("check.email") }}',
                        dataType: 'json',
                        type:'post',
                        data: {
                            email: function() {
                                return $('#agentemail').val();
                            },
                            _token: '{{ csrf_token() }}'
                        }
                    },
                },
                agentpassword:{
                    required: true,
                    minlength: 8
                },
                agentcpassword:{
                    required: true,
                    minlength: 8,
                    equalTo : "#agentpassword"
                },
                agentmob:{
                    required: true,
                    digits: true ,
                    minlength: 10,
                    maxlength: 10,
                    remote: {
                        url: '{{ route("check.mobile") }}',
                        dataType: 'json',
                        type:'post',
                        data: {
                            mobile: function() {
                                return $('#agentmob').val();
                            },
                            _token: '{{ csrf_token() }}'
                        }
                    },
                }
            },
            messages: {
                agentemail:{
                    remote:'Email already in Use ',
                    email:'Oops...! check your Email again '
                },
                agentmob:{
                    remote:'Mobile Number already in Use ',
                    digits:"Please enter a valid number",
                },
                agentcpassword:{
                    equalTo : "Confirm password not match "
                },
            },
            submitHandler: function (form) {
                if($('#accept_agent_terms').is(":checked")){
                    console.log('test');
                    $('#agentbtn').hide();
                    $('#agenta').show();
                    $.post("{{ route('new.register') }}",{
                        jsonrpc: 2.0,
                        _token: "{{ csrf_token() }}",
                        params: {
                            fname: $('#agentfname').val(),
                            lname: $('#agentlname').val(),
                            email: $('#agentemail').val(),
                            mobile: $('#agentmob').val(),
                            password: $('#agentpassword').val(),
                            cpassword: $('#agentcpassword').val(),
                            userType: 'A'
                        }
                    })
                    .done(response => {
                        console.log(response);
                        if (response.error && response.error.message) {
                            $('#agentbtn').show();
                            $('#agenta').hide();
                            $('#modalagentsignup .alert-success').css('display','none');
                            $('#modalagentsignup .alert-danger').css('display','block');
                            $('#modalagentsignup .alert-danger .error-message').text(response.error.message);
                        }else{
                            $('#otptext').text('Enter the 6 digit code sent to you at +91' +response.result.data.mobile_number);
                            $('#otp').text(response.result.data.otp_mobile);
                            $('#user_id').val(response.result.data.id);
                            $('.openotp').click();
                        }
                    })
                    .fail(error => {

                    })
                }else{
                    $('#accept_agent_terms-error').html('Please accept terms and conditions');
                    $('#accept_agent_terms-error').css('display', 'block');
                    return false;
                }

            }
        });


        $("#serviceingupform").validate({
            rules: {
                servicefname:{
                    validate_word:true,
                },
                servicelname:{
                    validate_word:true,
                },
                serviceemail: {
                    required: true,
                    email: true,
                    validate_email:true,
                    remote: {
                        url: '{{ route("check.email") }}',
                        dataType: 'json',
                        type:'post',
                        data: {
                            email: function() {
                                return $('#serviceemail').val();
                            },
                            _token: '{{ csrf_token() }}'
                        }
                    },
                },
                servicepassword:{
                    required: true,
                    minlength: 8
                },
                servicecpassword:{
                    required: true,
                    minlength: 8,
                    equalTo : "#servicepassword"
                },
                servicemob:{
                    required: true,
                    digits: true ,
                    minlength: 10,
                    maxlength: 10,
                    remote: {
                        url: '{{ route("check.mobile") }}',
                        dataType: 'json',
                        type:'post',
                        data: {
                            mobile: function() {
                                return $('#servicemob').val();
                            },
                            _token: '{{ csrf_token() }}'
                        }
                    },
                }
            },
            messages: {
                serviceemail:{
                    remote:'Email already in Use ',
                    email:'Oops...! check your Email again '
                },
                servicemob:{
                    remote:'Mobile Number already in Use ',
                    digits:"Please enter a valid number",
                },
                servicecpassword:{
                    equalTo : "Confirm password not match "
                },
            },
            submitHandler: function (form) {
                if($('#accept_service_terms').is(":checked")){
                    console.log('test');
                    $('#servicebtn').hide();
                    $('#servicea').show();
                    $.post("{{ route('new.register') }}",{
                        jsonrpc: 2.0,
                        _token: "{{ csrf_token() }}",
                        params: {
                            fname: $('#servicefname').val(),
                            lname: $('#servicelname').val(),
                            email: $('#serviceemail').val(),
                            mobile: $('#servicemob').val(),
                            password: $('#servicepassword').val(),
                            cpassword: $('#servicecpassword').val(),
                            userType: 'S'
                        }
                    })
                    .done(response => {
                        console.log(response);
                        if (response.error && response.error.message) {
                            $('#servicebtn').show();
                            $('#servicea').hide();
                            $('#modalproignup .alert-success').css('display','none');
                            $('#modalproignup .alert-danger').css('display','block');
                            $('#modalproignup .alert-danger .error-message').text(response.error.message);
                        }else{
                            $('#otptext').text('Enter the 6 digit code sent to you at +91' +response.result.data.mobile_number);
                            $('#otp').text(response.result.data.otp_mobile);
                            $('#user_id').val(response.result.data.id);
                            $('.openotp').click();
                        }
                    })
                    .fail(error => {

                    })
                }else{
                    $('#accept_service_terms-error').html('Please accept terms and conditions');
                    $('#accept_service_terms-error').css('display', 'block');
                    return false;
                }

            }
        });


        $("#otp_form").validate({
            rules: {
                codeBox6:{
                    required: true,
                },
            },
            messages: {
                codeBox6:{
                    required: 'OTP Required',
                },
            },
            submitHandler: function (form) {
                console.log('test');
                $.post("{{ route('check.mobile.verify') }}",{
                jsonrpc: 2.0,
                _token: "{{ csrf_token() }}",
                params: {
                    codeBox1: $('#codeBox1').val(),
                    codeBox2: $('#codeBox2').val(),
                    codeBox3: $('#codeBox3').val(),
                    codeBox4: $('#codeBox4').val(),
                    codeBox5: $('#codeBox5').val(),
                    codeBox6: $('#codeBox6').val(),
                    id: $('#user_id').val()
                }
            })
            .done(response => {
                console.log(response);
                if (response.error && response.error.message) {
                    $('#otpopup .alert-success').css('display','none');
                    $('#otpopup .alert-danger').css('display','block');
                    $('#otpopup .alert-danger .error-message').text(response.error.message);

                }else{
                    if(response.result.data.user_type=='U'){
                        $('#modalsignin .alert-danger').css('display','none');
                        $('#modalsignin .alert-success').css('display','block');
                        $('#modalsignin .alert-success .success-message ').text(response.result.message);
                        $(".opensignin").click();
                    }
                    else if (response.result.data.user_type=='A'){
                        $('#modalagentsignin .alert-danger').css('display','none');
                        $('#modalagentsignin .alert-success').css('display','block');
                        $('#modalagentsignin .alert-success .success-message ').text(response.result.message);
                        $(".agentlogin").click();

                    }else{
                        $('#modalproignin .alert-danger').css('display','none');
                        $('#modalproignin .alert-success').css('display','block');
                        $('#modalproignin .alert-success .success-message ').text(response.result.message);
                        $(".prologin").click();
                    }
                }
            })
            .fail(error => {
            })
            }
        });

        $('#modalproignup').on('hidden.bs.modal', function () {
            $(this).find('form').trigger('reset');
        });
        $('#modalagentsignup').on('hidden.bs.modal', function () {
            $(this).find('form').trigger('reset');
        });
        $('#modalsignin2').on('hidden.bs.modal', function () {
            $(this).find('form').trigger('reset');
        });
        $('#otpopup').on('hidden.bs.modal', function () {
            $(this).find('form').trigger('reset');
        });
        $('#modalsignin').on('hidden.bs.modal', function () {
            $(this).find('form').trigger('reset');
        });
        $('#modalagentsignin').on('hidden.bs.modal', function () {
            $(this).find('form').trigger('reset');
        });
        $('#modalproignin').on('hidden.bs.modal', function () {
            $(this).find('form').trigger('reset');
        });
        $('#forgotpass').on('hidden.bs.modal', function () {
            $(this).find('form').trigger('reset');
        });
        $('#forgototpopup').on('hidden.bs.modal', function () {
            $(this).find('form').trigger('reset');
        });
        $('#modalchangepassword').on('hidden.bs.modal', function () {
            $(this).find('form').trigger('reset');
        });


        $("#usersininform").validate({
            rules: {
                username:{
                    validate_email: function(){
                        var userName = $('#username').val();
                        if (/^([0-9.])+$/.test(userName)) {
                            false;
                        } else {
                            return true;
                        }
                    },
                    minlength: function(){
                        var userName = $('#username').val();
                        if (/^([0-9.])+$/.test(userName)) {
                            return 10;
                        } else {
                            false;
                        }
                    },
                    maxlength: function(){
                        var userName = $('#username').val();
                        if (/^([0-9.])+$/.test(userName)) {
                            return 10;
                        } else {
                            false;
                        }
                    },
                    digits: function(){
                        var userName = $('#username').val();
                        if (/^([0-9.])+$/.test(userName)) {
                            return true;
                        } else {
                            false;
                        }
                    },
                },
            },
            messages: {
                username:{
                    required: 'Enter Email or mobile',
                    minlength: 'Mobile number exactly only 10 digits without country code',
                    maxlength: 'Mobile number exactly only 10 digits without country code',
                    digits: 'Please enter a valid number ',
                }
            },
            submitHandler: function (form) {
                console.log('test');
                $('#userloginbtn').hide();
                $('#userlogina').show();
                $.post("{{ route('user.login') }}",{
                jsonrpc: 2.0,
                _token: "{{ csrf_token() }}",
                params: {
                    username: $('#username').val(),
                    password: $('#userloginpassword').val(),
                    userType: 'U'
                }
            })
            .done(response => {
                console.log(response);
                if (response.error && response.error.message) {
                    if(response.error.otp){
                        $('#otptext').text('Enter the 6 digit code sent to you at +91' +response.error.data.mobile_number);
                        $('#otp').text(response.error.otp);
                        $('#user_id').val(response.error.data.id);
                        $('#otpopup .alert-success').css('display','none');
                        $('#otpopup .alert-danger').css('display','block');
                        $('#modalsignin .alert-success').css('display','none');
                        $('#modalsignin .alert-danger').css('display','none');
                        $('#otpopup .alert-danger .error-message').text(response.error.message);
                        $('.openotp').click();
                    }else{
                        $('#userloginbtn').show();
                        $('#userlogina').hide();
                        $('#modalsignin .alert-success').css('display','none');
                        $('#modalsignin .alert-danger').css('display','block');
                        $('#modalsignin .alert-danger .error-message').text(response.error.message);
                    }
                }else{
                    // $('#otptext').text('Enter the 6 digit code sent to you at +91' +response.result.data.mobile_number);
                    // $('#otp').text(response.result.data.otp_mobile);
                    // $('#user_id').val(response.result.data.id);
                    // $('.openotp').click();
                    // window.location.href = "{{route('user.dashboard')}}";
                    location.reload();
                }
            })
            .fail(error => {
            })
            }
        });

        $("#agentsininform").validate({
            rules: {
                agentusername:{
                    validate_email: function(){
                        var userName = $('#agentusername').val();
                        if (/^([0-9.])+$/.test(userName)) {
                            false;
                        } else {
                            return true;
                        }
                    },
                    minlength: function(){
                        var userName = $('#agentusername').val();
                        if (/^([0-9])+$/.test(userName)) {
                            return 10;
                        } else {
                            false;
                        }
                    },
                    maxlength: function(){
                        var userName = $('#agentusername').val();
                        if (/^([0-9.])+$/.test(userName)) {
                            return 10;
                        } else {
                            false;
                        }
                    },
                    digits: function(){
                        var userName = $('#agentusername').val();
                        if (/^([0-9.])+$/.test(userName)) {
                            return true;
                        } else {
                            false;
                        }
                    },
                },
            },
            messages: {
                agentusername:{
                    required: 'Enter Email or mobile',
                    minlength: 'Mobile number exactly only 10 digits without country code',
                    maxlength: 'Mobile number exactly only 10 digits without country code',
                    digits: 'Please enter a valid number ',
                }
            },
            submitHandler: function (form) {
                console.log('test');
                $('#agentloginbtn').hide();
                $('#agentlogina').show();
                $.post("{{ route('user.login') }}",{
                jsonrpc: 2.0,
                _token: "{{ csrf_token() }}",
                params: {
                    username: $('#agentusername').val(),
                    password: $('#agentloginpassword').val(),
                    userType: 'A'
                }
            })
            .done(response => {
                console.log(response);
                if (response.error && response.error.message) {
                    if(response.error.otp){
                        $('#otptext').text('Enter the 6 digit code sent to you at +91' +response.error.data.mobile_number);
                        $('#otp').text(response.error.otp);
                        $('#user_id').val(response.error.data.id);
                        $('#otpopup .alert-success').css('display','none');
                        $('#otpopup .alert-danger').css('display','block');
                        $('#modalsignin .alert-success').css('display','none');
                        $('#modalsignin .alert-danger').css('display','none');
                        $('#otpopup .alert-danger .error-message').text(response.error.message);
                        $('.openotp').click();
                    }else{
                        $('#agentloginbtn').show();
                        $('#agentlogina').hide();
                        $('#modalagentsignin .alert-success').css('display','none');
                        $('#modalagentsignin .alert-danger').css('display','block');
                        $('#modalagentsignin .alert-danger .error-message').text(response.error.message);
                    }
                }else{
                    // $('#otptext').text('Enter the 6 digit code sent to you at +91' +response.result.data.mobile_number);
                    // $('#otp').text(response.result.data.otp_mobile);
                    // $('#user_id').val(response.result.data.id);
                    // $('.openotp').click();
                    window.location.href = "{{route('agent.dashboard')}}";
                    // location.reload();
                }
            })
            .fail(error => {
            })
            }
        });

        $("#servicesininform").validate({
            rules: {
                serviceusername:{
                    validate_email: function(){
                        var userName = $('#serviceusername').val();
                        if (/^([0-9.])+$/.test(userName)) {
                            false;
                        } else {
                            return true;
                        }
                    },
                    minlength: function(){
                        var userName = $('#serviceusername').val();
                        if (/^([0-9.])+$/.test(userName)) {
                            return 10;
                        } else {
                            false;
                        }
                    },
                    maxlength: function(){
                        var userName = $('#serviceusername').val();
                        if (/^([0-9.])+$/.test(userName)) {
                            return 10;
                        } else {
                            false;
                        }
                    },
                    digits: function(){
                        var userName = $('#serviceusername').val();
                        if (/^([0-9.])+$/.test(userName)) {
                            return true;
                        } else {
                            false;
                        }
                    },
                },
            },
            messages: {
                serviceusername:{
                    required: 'Enter Email or mobile',
                    minlength: 'Mobile number exactly only 10 digits without country code',
                    maxlength: 'Mobile number exactly only 10 digits without country code',
                    digits: 'Please enter a valid number ',
                }
            },
            submitHandler: function (form) {
                console.log('test');
                $('#serviceloginbtn').hide();
                $('#servicelogina').show();
                $.post("{{ route('user.login') }}",{
                jsonrpc: 2.0,
                _token: "{{ csrf_token() }}",
                params: {
                    username: $('#serviceusername').val(),
                    password: $('#serviceloginpassword').val(),
                    userType: 'S'
                }
            })
            .done(response => {
                console.log(response);
                if (response.error && response.error.message) {
                    if(response.error.otp){
                        $('#otptext').text('Enter the 6 digit code sent to you at +91' +response.error.data.mobile_number);
                        $('#otp').text(response.error.otp);
                        $('#user_id').val(response.error.data.id);
                        $('#otpopup .alert-success').css('display','none');
                        $('#otpopup .alert-danger').css('display','block');
                        $('#modalproignin .alert-success').css('display','none');
                        $('#modalproignin .alert-danger').css('display','none');
                        $('#otpopup .alert-danger .error-message').text(response.error.message);
                        $('.openotp').click();
                    }else{
                        $('#serviceloginbtn').show();
                        $('#servicelogina').hide();
                        $('#modalproignin .alert-success').css('display','none');
                        $('#modalproignin .alert-danger').css('display','block');
                        $('#modalproignin .alert-danger .error-message').text(response.error.message);
                    }
                }else{
                    // $('#otptext').text('Enter the 6 digit code sent to you at +91' +response.result.data.mobile_number);
                    // $('#otp').text(response.result.data.otp_mobile);
                    // $('#user_id').val(response.result.data.id);
                    // $('.openotp').click();
                    // location.reload();
                    window.location.href = "{{route('service.provider.dashboard')}}";
                }
            })
            .fail(error => {
            })
            }
        });

        $("#forgotpasswordform").validate({
            rules: {
                forgotusername:{
                    validate_email: function(){
                        var userName = $('#forgotusername').val();
                        if (/^([0-9.])+$/.test(userName)) {
                            false;
                        } else {
                            return true;
                        }
                    },
                    minlength: function(){
                        var userName = $('#forgotusername').val();
                        if (/^([0-9.])+$/.test(userName)) {
                            return 10;
                        } else {
                            false;
                        }
                    },
                    maxlength: function(){
                        var userName = $('#forgotusername').val();
                        if (/^([0-9.])+$/.test(userName)) {
                            return 10;
                        } else {
                            false;
                        }
                    },
                    digits: function(){
                        var userName = $('#forgotusername').val();
                        if (/^([0-9.])+$/.test(userName)) {
                            return true;
                        } else {
                            false;
                        }
                    },
                },
            },
            messages: {
                forgotusername:{
                    required: 'Enter Email or mobile',
                    minlength: 'Mobile number exactly only 10 digits without country code',
                    maxlength: 'Mobile number exactly only 10 digits without country code',
                    digits: 'Please enter a valid number ',
                }
            },
            submitHandler: function (form) {
                console.log('test');
                $('#forgotbtn').hide();
                $('#forgota').show();
                $.post("{{ route('user.forgot.password') }}",{
                jsonrpc: 2.0,
                _token: "{{ csrf_token() }}",
                params: {
                    username: $('#forgotusername').val(),
                }
            })
            .done(response => {
                console.log(response);
                if (response.error && response.error.message) {
                    $('#forgotbtn').show();
                    $('#forgota').hide();
                    $('#forgotpass .alert-success').css('display','none');
                    $('#forgotpass .alert-danger').css('display','block');
                    $('#forgotpass .alert-danger .error-message').text(response.error.message);

                }else{
                    if(response.result.otp){
                        $('#forgototptext').text('Enter the 6 digit code sent to you at +91' +response.result.data.mobile_number);
                        $('#forgototp').text(response.result.otp);
                        $('#forgot_user_id').val(response.result.data.id);
                        $('.forgototp').click();

                    }else{
                        $('#forgotbtn').show();
                        $('#forgota').hide();
                        $('#forgotpass .alert-success').css('display','block');
                        $('#forgotpass .alert-danger').css('display','none');
                        $('#forgotpass .alert-success .success-message').text(response.result.message);
                    }
                }
            })
            .fail(error => {
            })
            }
        });


        $("#changepasswordform").validate({
            rules: {
                newpassword:{
                    required: true,
                    minlength: 8
                },
                newcpassword:{
                    required: true,
                    minlength: 8,
                    equalTo : "#newpassword"
                },
            },
            messages: {
                codeBoxs6:{
                    required: 'OTP Required',
                },
            },
            submitHandler: function (form) {
                console.log('test');
                $.post("{{ route('change.password') }}",{
                jsonrpc: 2.0,
                _token: "{{ csrf_token() }}",
                params: {
                    newpassword: $('#newpassword').val(),
                    newcpassword: $('#newcpassword').val(),
                    id: $('#change_user_id').val()
                }
            })
            .done(response => {
                console.log(response);
                if (response.error && response.error.message) {
                    $('#modalchangepassword .alert-success').css('display','none');
                    $('#modalchangepassword .alert-danger').css('display','block');
                    $('#modalchangepassword .alert-danger .error-message').text(response.error.message);

                }else{
                    if(response.result.data.user_type=='U'){
                        $(".opensignin").click();
                        $('#modalsignin .alert-danger').css('display','none');
                        $('#modalsignin .alert-success').css('display','block');
                        $('#modalsignin .alert-success .success-message ').text(response.result.message);
                    }
                    else if (response.result.data.user_type=='A'){
                        $(".agentlogin").click();
                        $('#modalagentsignin .alert-danger').css('display','none');
                        $('#modalagentsignin .alert-success').css('display','block');
                        $('#modalagentsignin .alert-success .success-message ').text(response.result.message);

                    }else{
                        $(".prologin").click();
                        $('#modalproignin .alert-danger').css('display','none');
                        $('#modalproignin .alert-success').css('display','block');
                        $('#modalproignin .alert-success .success-message ').text(response.result.message);
                    }
                }
            })
            .fail(error => {
            })
            }
        });

        $("#forgototp_form").validate({
            rules: {
                codeBoxs6:{
                    required: true,
                },
            },
            messages: {
                codeBoxs6:{
                    required: 'OTP Required',
                },
            },
            submitHandler: function (form) {
                console.log('test');
                $.post("{{ route('user.forgot.password.otp.verify') }}",{
                jsonrpc: 2.0,
                _token: "{{ csrf_token() }}",
                params: {
                    codeBox1: $('#codeBoxs1').val(),
                    codeBox2: $('#codeBoxs2').val(),
                    codeBox3: $('#codeBoxs3').val(),
                    codeBox4: $('#codeBoxs4').val(),
                    codeBox5: $('#codeBoxs5').val(),
                    codeBox6: $('#codeBoxs6').val(),
                    id: $('#forgot_user_id').val()
                }
            })
            .done(response => {
                console.log(response);
                if (response.error && response.error.message) {
                    $('#forgototpopup.alert-success').css('display','none');
                    $('#forgototpopup .alert-danger').css('display','block');
                    $('#forgototpopup .alert-danger .error-message').text(response.error.message);

                }else{
                    $(".changepasswordclick").click();
                    $('#modalproignin .alert-danger').css('display','none');
                    $('#modalproignin .alert-success').css('display','block');
                    $('#change_user_id').val(response.result.data.id)
                    $('#modalproignin .alert-success .success-message ').text(response.result.message);
                }
            })
            .fail(error => {
            })
            }
        });



    });

</script>
{{-- Singup Script --}}

{{-- OTP Script --}}
<script>
    function getCodeBoxElement(index) {
        return document.getElementById('codeBox' + index);
    }
    function getCodeBoxElement1(index) {
        return document.getElementById('codeBoxs' + index);
    }
      function onKeyUpEvent(index, event) {
        const eventCode = event.which || event.keyCode;
        if (getCodeBoxElement(index).value.length === 1) {
          if (index !== 6) {
            getCodeBoxElement(index+ 1).focus();
          } else {
            getCodeBoxElement(index).blur();
            // Submit code
            console.log('submit code ');
          }
        }
        if (eventCode === 8 && index !== 1) {
          getCodeBoxElement(index - 1).focus();
        }
      }
      function onKeyUpEvent1(index, event) {
        const eventCode = event.which || event.keyCode;
        if (getCodeBoxElement1(index).value.length === 1) {
          if (index !== 6) {
            getCodeBoxElement1(index+ 1).focus();
          } else {
            getCodeBoxElement1(index).blur();
            // Submit code
            console.log('submit code ');
          }
        }
        if (eventCode === 8 && index !== 1) {
          getCodeBoxElement1(index - 1).focus();
        }
      }
      function onFocusEvent(index) {
        for (item = 1; item < index; item++) {
          const currentElement = getCodeBoxElement(item);
          if (!currentElement.value) {
              currentElement.focus();
              break;
          }
        }
      }
      function onFocusEvent1(index) {
        for (item = 1; item < index; item++) {
          const currentElement = getCodeBoxElement1(item);
          if (!currentElement.value) {
              currentElement.focus();
              break;
          }
        }
      }
</script>
{{-- OTP Script --}}
<script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=6195ee4013b8f60014cbfb4f&product=image-share-buttons" async="async"></script>

{{-- Pusher --}}
<script src="https://js.pusher.com/6.0/pusher.min.js"></script>
<script>
$(function() {
    $('#toggle-one').toggle({
        on: 'Online',
        off: 'Away'
    });
})
@if(@Auth::user())
    var my_id = {{@Auth::user()->id}}; 
@else
    var my_id = 0; 
@endif
var user_arr=[];
var user_minm_arr=[];
var user_str_min_arr='';
var current_mob_chat_id=0;
Pusher.logToConsole = false;
var pusher = new Pusher('{{ config("services.pusher.pusher_app_key") }}', {
    cluster: '{{ config("services.pusher.pusher_app_cluster") }}'
});
var socketId = null;
pusher.connection.bind('connected', function() {
    socketId = pusher.connection.socket_id;
console.log(socketId);
});
var channel = pusher.subscribe('rivirtual');
console.log(pusher)
channel.bind('receive-event', function(data) {
   
    if(data.type=='Y'){
        $('.online-'+data.slug).css('color','#24c624');
        // $('.online-status-'+data.slug).html('Online');
    }else if(data.type=='N'){
        $('.online-'+data.slug).css('color','red');
        // $('.online-status-'+data.slug).html('Away');
    }
    if(data.reciver_id==my_id){

        var curroute = "{{Route::is('show.message')}}";
        if(curroute==1){
            if(data.type=="B" && (data.message_master_id==$('.message_master_id').val())){
                $('.attachedd-type').hide();
                $('.message-box-div').append('<h2 style="text-align: center" class="block-msg">'+data.sender_name+' have block the conversation</h2>');
            }else if(data.type=='U' && (data.message_master_id==$('.message_master_id').val())){
                $('.attachedd-type').show();
                $('.block-msg').remove();
            }else if(data.message_master_id==$('.message_master_id').val()){
            newmsg = data.message;
                if(data.file !== null){
                    file = "{{url('storage/app/public/message_files/')}}";
                    file = file+'/'+data.file;
                    filename=data.file.substr(data.file.lastIndexOf("_")+1);
                    attach_file=`<br><a href="${file}"  target=_blank>${filename}</a>`;
                }else{
                    attach_file=``; 
                }
                var rmsgg = `<div class="chat_mass_itm sender-div">
                                        <div class="media">
                                            <em>
                                                <img src="${data.sender_image}" alt="">
                                            </em>
                                            <div class="media-body">
                                                <h5>${data.sender_name}</h5>
                                                    <div class="chat_mass_bx">
                                                        <p>${nl2br(data.message)}${attach_file}</p>
                                                    </div>
                                                <span>${getTimeDiff(new Date())}</span>
                                            </div>
                                        </div>
                                    </div>`;
                // var rmsgg = `<div class="media w-100 mb-3 sender-div">
                //             <img src="${data.sender_image}" alt="" width="32" class="rounded-circle  user-round-circle">
                //             <div class="media-body ml-3">
                //                 <h5>${data.sender_name}</h5>
                //                 <div class="bg-light rounded py-2 px-3 mb-1">
                //                         <p class="text-small mb-0 text-muted">${nl2br(data.message)}${attach_file}</p>
                //                     </div>
                //                     <p class="small">${getTimeDiff(new Date())}</p>
                //                 </div>
                //             </div>`;
                $('.chat-box').append(rmsgg);
                scrollChat();  
            }
            refreshListing();
        }if(data.type=='B' && jQuery.inArray(data.message_master_id, user_arr)!== -1){
            $('.block-popup-'+data.message_master_id).hide();
            $('.block-msg--new-popup-'+data.message_master_id).append('<h6 style="text-align: center" class="block-msg-pop-'+data.message_master_id+'">'+data.sender_name+' have block the conversation</h6>')
        }else if(data.type=='U' && jQuery.inArray(data.message_master_id, user_arr)!== -1){
            $('.block-popup-'+data.message_master_id).show();
            $('.block-msg-pop-'+data.message_master_id).remove();
        }else if(jQuery.inArray(data.message_master_id, user_arr)!== -1){
            newmsg = data.message;
            if(data.file !== null){
                file = "{{url('storage/app/public/message_files/')}}";
                file = file+'/'+data.file;
                filename=data.file.substr(data.file.lastIndexOf("_")+1);
                attach_file=`<br><a href="${file}"  target=_blank>${filename}</a>`;
                // attach_file=`hi`; 
            }else{
                attach_file=``; 
            }         
            var msgg = `<div class="chat chat-left">
                    <div class="chat-avatar">
                        <a class="avatar avatar-online recive_img" href="javascript:;">
                            <img src="${data.sender_image}" alt="">
                        </a>        
                    </div>
                    <div class="chat-body">
                        <span class="username">${data.sender_name}</span>
                        <div class="chat-content">
                            <p class="recive_msg">${nl2br(data.message)}${attach_file}</p>
                        </div>
                        <time class="chat-time">${getTimeDiff(new Date())}</time>
                    </div>
                </div>`;
            $('.chat-bodys-'+data.message_master_id).append(msgg);
            $('.chat-bodys-'+data.message_master_id).scrollTop(100000000000000); 
            if($('.main-chat-box-'+data.message_master_id).css("margin-bottom")==-322+"px"){
                $('.main-chat-box-'+data.message_master_id).css("margin-bottom",0+"px");
                index=user_minm_arr.indexOf(data.message_master_id);
                user_minm_arr.splice(index, 1);
                localStorage.setItem("user_minm_arr", user_minm_arr);
            }
            if (window.matchMedia('(max-width: 767px)').matches) {
                if(current_mob_chat_id!=data.message_master_id){
                    make_molile_popup(data.message_master_id,data.title,data.s_slug,data.online_status,data.sender_name,data.sender_slug,data.sender_image,'',data.message_master_id);
                }
            }
        }else{
            makePopup(data.message_master_id,data.title,data.s_slug,data.online_status,data.sender_name,data.sender_slug,data.sender_image,'',data.message_master_id,data.user_type);
        }
    }
});

$('body').on('keyup','.type-msgs',function(){
    message_master_id = $('.frm_msg').data('id');
    $('.message_master_id').val(message_master_id);
    // console.log(message_master_id)
});
var textarea = $('.message');
var typingStatus = $('.typing');
var lastTypedTime = new Date(0); // it's 01/01/1970
var typingDelayMillis = 500; // how long user can "think about his spelling" before we show "No one is typing -blank space." message
var isTyping = false;
var curroute = "{{Route::is('show.message')}}";
console.log(curroute)
if(curroute){
    function refreshTypingStatus() {
        if (!textarea.is(':focus') || textarea.val() == '' || new Date().getTime() - lastTypedTime.getTime() > typingDelayMillis) {
            if (isTyping) { 
                startStopTypingAJAX('end');
                isTyping = false;
                console.log(isTyping);
            }
        } else {
            if (!isTyping) { 
                startStopTypingAJAX('start')
                isTyping = true;
                console.log(isTyping);
            }
        }
    }
}
else{
    function refreshTypingStatus() {
        var textarea = $('.chat-msg-0');
        if(typeof message_master_id !== 'undefined' && message_master_id!=''){
            textarea = $('.chat-msg-'+message_master_id);
        } 
        if (!textarea.is(':focus') || textarea.val() == '') {
            if (isTyping) { 
                startStopTypingAJAX('end');
                isTyping = false;
                console.log(isTyping);
            }
        } else {
            if (!isTyping) { 
                startStopTypingAJAX('start')
                isTyping = true;
                console.log(isTyping);
            }
        }
    }            
}
function updateLastTypedTime() {
    lastTypedTime = new Date();
}
setInterval(refreshTypingStatus, 2000);
textarea.keypress(updateLastTypedTime);
textarea.blur(refreshTypingStatus);

channel.bind('start-end-typing', function(data) {
    if (data.typing == 'end') {
        $('.typing').html('').hide(100);
    } else {
        if(my_id== data.reciver_id && data.message_master_id==$('.message_master_id').val())
        $('.typing').html(`<p><b>${data.from}</b> is typing...</p>`).show(100);
    }
    if (data.typing == 'end') {
        $('.typpppp').remove();
    } else {
        if(my_id== data.reciver_id){
            var msgg = `<div class="chat chat-left typing-${data.message_master_id}"> 
                <div class="chat-body">
                    <div class="chat-content typpppp">
                        <p class="recive_msg"><b>${data.from}</b> is typing...</p>
                    </div>
                </div>
            </div>`;
        }
        $('.chat-bodys-'+data.message_master_id).append(msgg);
        $('.chat-bodys-'+data.message_master_id).scrollTop(100000000000000); 
    }
});

function startStopTypingAJAX(typing) {
    message_master_id = $('.message_master_id').val();
    $.ajax({
        url: '{{ route("typing.ajax") }}',
        type: 'POST',
        dataType: 'JSON',
        data: {
            message_master_id:message_master_id,
            typing: typing,
            _token: '{{ csrf_token() }}',
            socket_id: socketId,
        }
    });
}

function scrollChat(){
  var indexxx = $('.all-conversation').find('.scrollbar-path-vertical').height();
  indexxx -= (indexxx*30)/100;
  $('.scrollbar-handle').animate({top: indexxx});
  var content_height = (-1) * parseInt($('.all-conversation').find('.scrollbar-content').height() - $('.all-conversation').height());
  $('.all-conversation').find('.scrollbar-content').animate({top: content_height},0);
  console.log('Scrolled');
}
function getTimeDiff(datetime) {
  var date =  new Date(datetime);
  var cur=new Date();
  if(date.getDate()==cur.getDate()){
      var hours = date.getHours();
      var minutes = date.getMinutes();
      var ampm = hours >= 12 ? 'pm' : 'am';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0'+minutes : minutes;
      var strTime = hours + ':' + minutes + ' ' + ampm;
  }else{
      var hours = date.getHours();
      var minutes = date.getMinutes();
      var ampm = hours >= 12 ? 'pm' : 'am';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0'+minutes : minutes;
      var dd = date. getDate();
      var mm = date.toLocaleString('default', { month: 'short' });;
      var yyyy = date. getFullYear();
      var strTime = dd+'-'+mm+'-'+yyyy+' '+hours + ':' + minutes + ' ' + ampm;  
  }
  return strTime;
}
function makePopup(message_master_id,title,s_slug,online_status,u_name,u_slug,u_image,type,current_mob_chat_id,user_type,bid_id){
    
    
    console.log(message_master_id,title,s_slug,online_status,u_name,u_slug,u_image,type,current_mob_chat_id,user_type,bid_id);
    if(!message_master_id){
        return;
    }
    if(title.length>40){
        title=title.substr(0,40)+".."
    }
    style='style="color:rgb(36, 198, 36);"'; 
    if(online_status=='N'){
        style='style="color:red;"';
    }
    msgFun="sendMsg";
    msgType="";
    if(type=='B'){
        msgFun="sendMsg1";
        msgType='data-type="B"';
    }
    if(bid_id == undefined){
        bid_id = 0;
    }
    

    mobile_html=`<li class="mobile-main-chat-${message_master_id}"><a href="javascript:;"><strong  class="mobile-box-${message_master_id} mobile-chat-person-li" data-id="${message_master_id}">
                            <span><img src="${u_image}" alt=""></span><i aria-hidden="true" class="fa fa-circle online-${u_slug}" ${style} style="font-size: 10px;"></i> ${u_name}
                        </strong><strong class="chat-action" data-id="${message_master_id}" style="margin-left: 5px; "><i aria-hidden="true" class="fa fa-times" style="float:right;margin: 6px;"></i></strong></a>
                        </li>`;
    if(user_type == 'P'){
        html=`<div class="main-chat-box main-chat-box-${message_master_id}" >
                <div class="chat-heads minimise-chat-box"  data-id="${message_master_id}">
                    <div class="chat-title ">
                        <span><i aria-hidden="true" class="fa fa-circle online-${u_slug}" ${style}></i> <b class="username-${message_master_id} ">${u_name}</b></span>
                    </div>
                    <div class="chat-action-des"  data-id="${message_master_id}">
                        <a href="javascript:;" class="minimise-chat-box-icon" data-id="${message_master_id}"><i aria-hidden="true" class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="chat-action" data-id="${message_master_id}" style="margin-left: 5px;"><i aria-hidden="true" class="fa fa-times"></i></a>
                    </div>
                </div>
                <div class="buildme">
                <p><a href="{{url('service-provider-profile')}}/${s_slug}" target="_blank">${u_name}</a></p>
                </div>
                 <input type="hidden" class="user_type" value="${user_type}">
                 <input type="hidden" class="bid_id" value="${bid_id}">
                <div class="chat-bodys chat-bodys-${message_master_id}"></div>
                <form class="frm_msg" onsubmit="return ${msgFun}(${message_master_id})" data-id="${message_master_id}">
                    <div class="chat-fots block-msg--new-popup-${message_master_id}">
                        <span class="upload-image-pgrs upload-pgrs-${message_master_id}" style="display:none"><i class="fa fa-upload" aria-hidden="true"></i> Uploading...</span>
                        <textarea  class="type-msgs block-popup-${message_master_id} chat-msg-${message_master_id}"  ${msgType} data-id="${message_master_id}" placeholder="Type your message.."></textarea>
                        <div class="upload_box block-popup-${message_master_id}">
                            <input type="file" id="file${message_master_id}" data-id="${message_master_id}" class="file-upload-chat file-${message_master_id}">
                            <label for="file${message_master_id}" class="btn-2"></label>
                        
                        </div>

                        <button type="submit" class="send-msg-pop-btn block-popup-${message_master_id}"><img src="{{asset("public/frontend/images/sendbtn.png")}}"></button>
                        <p class="file-name-foots block-popup-${message_master_id} file-name-${message_master_id}"></p>
                    </div>
                </form>
            </div>`;

    }else{
        html=`<div class="main-chat-box main-chat-box-${message_master_id}" >
                <div class="chat-heads minimise-chat-box"  data-id="${message_master_id}">
                    <div class="chat-title ">
                        <span><i aria-hidden="true" class="fa fa-circle online-${u_slug}" ${style}></i> <b class="username-${message_master_id} ">${u_name}</b></span>
                    </div>
                    <div class="chat-action-des"  data-id="${message_master_id}">
                        <a href="javascript:;" class="minimise-chat-box-icon" data-id="${message_master_id}"><i aria-hidden="true" class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="chat-action" data-id="${message_master_id}" style="margin-left: 5px;"><i aria-hidden="true" class="fa fa-times"></i></a>
                    </div>
                </div>

                <div class="buildme">
                <p><a href="{{url('property')}}/${s_slug}" target="_blank">${title}</a></p>
                </div>
                <div class="chat-bodys chat-bodys-${message_master_id}"></div>
                <form class="frm_msg" onsubmit="return ${msgFun}(${message_master_id})" data-id="${message_master_id}">
                    <div class="chat-fots block-msg--new-popup-${message_master_id}">
                        <span class="upload-image-pgrs upload-pgrs-${message_master_id}" style="display:none"><i class="fa fa-upload" aria-hidden="true"></i> Uploading...</span>
                        <textarea  class="type-msgs block-popup-${message_master_id} chat-msg-${message_master_id}"  ${msgType} data-id="${message_master_id}" placeholder="Type your message.."></textarea>
                        <div class="upload_box block-popup-${message_master_id}">
                            <input type="file" id="file${message_master_id}" data-id="${message_master_id}" class="file-upload-chat file-${message_master_id}">
                            <label for="file${message_master_id}" class="btn-2"></label>
                        
                        </div>
                        <button type="submit" class="send-msg-pop-btn block-popup-${message_master_id}"><img src="{{asset("public/frontend/images/sendbtn.png")}}"></button>
                        <p class="file-name-foots block-popup-${message_master_id} file-name-${message_master_id}"></p>
                    </div>
                </form>
            </div>`;
    }
    $('.cht-popup').append(html);
    $margin_right=`${user_arr.length * 300}px`;
    $('.main-chat-box-'+message_master_id).css('margin-right',$margin_right);
    user_arr.push(message_master_id);
    localStorage.setItem("user_arr", user_arr);
    if(user_str_min_arr!== null && Number(user_str_min_arr)!=0 && user_str_min_arr!=''){
        user_minm_arr=user_str_min_arr.split(",");
        console.log(user_minm_arr);
        if(user_minm_arr.includes(message_master_id.toString())){
            $('.main-chat-box-'+message_master_id).css('margin-bottom','-322px');
        }
    }
    if(current_mob_chat_id !=''){
        $('.cht-popup-moblie').html(html);
        localStorage.setItem("current_mob_chat_id", current_mob_chat_id);
    }else if(localStorage.getItem("current_mob_chat_id")==message_master_id){
        $('.cht-popup-moblie').html(html);
        current_mob_chat_id=Number(localStorage.getItem("current_mob_chat_id"));
    }   
    style='';   
    $('.mobile-chat-user-list').append(mobile_html);
    if (window.matchMedia('(max-width: 767px)').matches) {
        $('.mobile-chat-show').show();
    }
    html='';
    if(type!='B'){
        $.ajax({
            url: '{{ route("user.message") }}',
            type: 'POST',
            dataType: 'JSON',
            async: false,
            data: {
                message_master_id:message_master_id,
                _token: '{{ csrf_token() }}',
            }
        })
        .done(result => {
            if(result.msgcnt)
                    $('.msgcnt').html(result.msgcnt);
            if (result.result) {
                result.result.forEach(function(item, index){
                    var imgg = "{{url('public/frontend/images/blank.png')}}";
                    var file='';
                    attach_file=``; 
                    if(item.get_user.profile_pic){
                        imgg = "{{url('storage/app/public/profile_picture/')}}";
                        imgg = imgg+'/'+item.get_user.profile_pic;
                    }
                    if(item.file !== null){
                        file = "{{url('storage/app/public/message_files/')}}";
                        file = file+'/'+item.file;
                        filename=item.file.substr(item.file.lastIndexOf("_")+1);
                        if(item.user_id==my_id){
                            attach_file=`<br><a href="${file}" class="msg-link-color" target=_blank>${filename}</a>`;
                        }else{
                            attach_file=`<br><a href="${file}" target=_blank>${filename}</a>`;
                        }
                    }
                    var img_div1 = `<a class="avatar avatar-online"><img src="${imgg}" alt=""><i></i></a>`;
                    var img_div2 = `<a class="avatar avatar-online recive_img" href="javascript:;"><img src="${imgg}" alt=""></a>`;
                    var username_div = `<span class="username">${item.get_user.name}</span>`; 
                    if(item.user_id==my_id){
                        var msgg = `<div class="chat">
                            <div class="chat-avatar">${img_div1}</div>
                            <div class="chat-body">
                                <div class="chat-content">
                                    <p class="send_msgg">${nl2br(item.message)}${attach_file}</p>
                                </div>
                                <time class="chat-time">${getTimeDiff(item.created_at)}</time>
                            </div>
                        </div>`;
                    }else{
                        var msgg = `<div class="chat chat-left">
                            <div class="chat-avatar">${img_div2}</div>
                            <div class="chat-body">
                                ${username_div}
                                <div class="chat-content">
                                    <p class="recive_msg">${nl2br(item.message)}${attach_file}</p>
                                </div>
                                <time class="chat-time">${getTimeDiff(item.created_at)}</time>
                            </div>
                        </div>`;
                    }
                    $('.chat-bodys-'+message_master_id).append(msgg);
                });
                 $('.chat-bodys-'+message_master_id).scrollTop(100000000000000); 
            } 
        });
    }
}
function make_molile_popup(message_master_id,title,s_slug,online_status,u_name,u_slug,u_image,type,current_mob_chat_id,user_type){
    if(!message_master_id){
        return;
    }
    current_mob_chat_id=message_master_id;
    localStorage.setItem("current_mob_chat_id", current_mob_chat_id);
    if(title.length>40){
        title=title.substr(0,40)+".."
    }
    style='style="color:rgb(36, 198, 36);"'; 
    if(online_status=='N'){
        style='style="color:red;"';
    }
    msgFun="sendMsg";
    msgType="";
    if(type=='B'){
        msgFun="sendMsg1";
        msgType='data-type="B"';
    }
    mobile_html=`<li class="mobile-main-chat-${message_master_id}"><a href="javascript:;"><strong  class="mobile-box-${message_master_id} mobile-chat-person-li" data-id="${message_master_id}">
                            <span><img src="${u_image}" alt=""></span><i aria-hidden="true" class="fa fa-circle online-${u_slug}" ${style} style="font-size: 10px;"></i> ${u_name}
                        </strong><strong class="chat-action" data-id="${message_master_id}" style="margin-left: 5px; "><i aria-hidden="true" class="fa fa-times" style="float:right;margin: 6px;"></i></strong></a>
                        </li>`;    
    html=`<div class="main-chat-box main-chat-box-${message_master_id}" >
                <div class="chat-heads minimise-chat-box"  data-id="${message_master_id}">
                    <div class="chat-title ">
                        <span><i aria-hidden="true" class="fa fa-circle online-${u_slug}" ${style}></i> <b class="username-${message_master_id} ">${u_name}</b></span>
                    </div>
                    <div class="chat-action-des"  data-id="${message_master_id}">
                        <a href="javascript:;" class="minimise-chat-box-icon" data-id="${message_master_id}"><i aria-hidden="true" class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="chat-action" data-id="${message_master_id}" style="margin-left: 5px;"><i aria-hidden="true" class="fa fa-times"></i></a>
                    </div>
                </div>
                <div class="buildme">
                <p><a href="{{url('property')}}/${s_slug}" target="_blank">${title}</a></p>
            </div>
                <div class="chat-bodys chat-bodys-${message_master_id}"></div>
                <form class="frm_msg" onsubmit="return ${msgFun}(${message_master_id})" data-id="${message_master_id}">
                    <div class="chat-fots block-msg--new-popup-${message_master_id}">
                        <span class="upload-image-pgrs upload-pgrs-${message_master_id}" style="display:none"><i class="fa fa-upload" aria-hidden="true"></i> Uploading...</span>
                        <textarea  class="type-msgs block-popup-${message_master_id} chat-msg-${message_master_id}"  ${msgType} data-id="${message_master_id}" placeholder="Type your message.."></textarea>
                        <div class="upload_box block-popup-${message_master_id}">
                            <input type="file" id="file${message_master_id}" data-id="${message_master_id}" class="file-upload-chat file-${message_master_id}">
                            <label for="file${message_master_id}" class="btn-2"></label>
                            
                        </div>
                        <button type="submit" class="send-msg-pop-btn block-popup-${message_master_id}"><img src="{{asset("public/frontend/images/sendbtn.png")}}"></button>
                        <p class="file-name-foots block-popup-${message_master_id} file-name-${message_master_id}"></p>
                    </div>
                </form>
            </div>`;
    $('.cht-popup-moblie').html(html);
    $margin_right=`${user_arr.length * 300}px`;  
    html='';
    if(type!='B'){
        $.ajax({
            url: '{{ route("user.message") }}',
            type: 'POST',
            dataType: 'JSON',
            async: false,
            data: {
                message_master_id:message_master_id,
                _token: '{{ csrf_token() }}',
            }
        })
        .done(result => {
            if(result.msgcnt)
                    $('.msgcnt').html(result.msgcnt);
            if (result.result) {
                result.result.forEach(function(item, index){
                    var imgg = "{{url('public/frontend/images/blank.png')}}";
                    var file='';
                    attach_file=``; 
                    if(item.get_user.profile_pic){
                        imgg = "{{url('storage/app/public/profile_picture/')}}";
                        imgg = imgg+'/'+item.get_user.profile_pic;
                    }
                    if(item.file !== null){
                        file = "{{url('storage/app/public/message_files/')}}";
                        file = file+'/'+item.file;
                        filename=item.file.substr(item.file.lastIndexOf("_")+1);
                        if(item.user_id==my_id){
                            attach_file=`<br><a href="${file}" class="msg-link-color" target=_blank>${filename}</a>`;
                        }else{
                            attach_file=`<br><a href="${file}" target=_blank>${filename}</a>`;
                        }
                    }
                    var img_div1 = `<a class="avatar avatar-online"><img src="${imgg}" alt=""><i></i></a>`;
                    var img_div2 = `<a class="avatar avatar-online recive_img" href="javascript:;"><img src="${imgg}" alt=""></a>`;
                    var username_div = `<span class="username">${item.get_user.name}</span>`; 
                    if(item.user_id==my_id){
                        var msgg = `<div class="chat">
                            <div class="chat-avatar">${img_div1}</div>
                            <div class="chat-body">
                                <div class="chat-content">
                                    <p class="send_msgg">${nl2br(item.message)}${attach_file}</p>
                                </div>
                                <time class="chat-time">${getTimeDiff(item.created_at)}</time>
                            </div>
                        </div>`;
                    }else{
                        var msgg = `<div class="chat chat-left">
                            <div class="chat-avatar">${img_div2}</div>
                            <div class="chat-body">
                                ${username_div}
                                <div class="chat-content">
                                    <p class="recive_msg">${nl2br(item.message)}${attach_file}</p>
                                </div>
                                <time class="chat-time">${getTimeDiff(item.created_at)}</time>
                            </div>
                        </div>`;
                    }
                    $('.chat-bodys-'+message_master_id).append(msgg);
                });
                 $('.chat-bodys-'+message_master_id).scrollTop(100000000000000); 
            } 
        });
    }
}
function nl2br (str, is_xhtml) {
  if (typeof str === 'undefined' || str === null) {
      return '';
  }
  var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
  return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}
function sendMsg(id){
    var us_type = $('.user_type').val();
    console.log(us_type);
    if(us_type == 'A'){
        var u_type = 'A';
    }
    if(us_type == undefined){
        var u_type = 'P';
    }
    if(us_type == 'P'){
        var u_type = 'P';
    }

    
    var replymsg = $('.chat-msg-'+id).val().trim();
    var files = $('.file-'+id).prop('files');
    data = new FormData();
            data.append('_token', "{{ csrf_token() }}");
            data.append('message_master_id',id);
            data.append('socket_id',socketId);
            data.append('message',replymsg);
            data.append('u_type',u_type);
            $.each(files, function(k,file){
                data.append('file', file);
            });
    var filecnt = Object.keys(files).length;
    if (replymsg || filecnt>0) {
        if(filecnt>0){
        $('.upload-pgrs-'+id).show();
        }
        $('.chat-msg-'+id).val("");
        $('.file-'+id).val('');
        $('.file-name-'+id).html('');
        $.ajax({
            url: '{{ route("send.ajax") }}',
            type: 'POST',
            dataType: 'JSON',
            data: data,
            enctype: 'multipart/form-data',
            processData: false,  
            contentType: false
        })
        .done(result => {
            if (result.result) {
                if(result.result.file !== null){
                    file = "{{url('storage/app/public/message_files/')}}";
                    file = file+'/'+result.result.file;
                    filename=result.result.file.substr(result.result.file.lastIndexOf("_")+1)
                    attach_file=`<br><a href="${file}" class="msg-link-color" target=_blank>${filename}</a>`
                    // attach_file=`hi`; 
                }else{
                    attach_file=``; 
                }
                if(result.result.message !== null){
                    replymsg=result.result.message;
                }else{
                    replymsg=``; 
                }
                var msgg2 = `<div class="chat">
                                <div class="chat-avatar">
                                    <a class="avatar avatar-online">
                                    <img src="{{ @Auth::user()->profile_pic ? url('storage/app/public/profile_picture/'.@Auth::user()->profile_pic) : url('public/frontend/images/blank.png') }}" alt="">
                                    <i></i>
                                    </a>
                                </div>
                                <div class="chat-body">
                                    <div class="chat-content">
                                        <p class="send_msgg">${nl2br(replymsg)}${attach_file}</p>
                                    </div>
                                    <time class="chat-time">${getTimeDiff(new Date())}</time>
                                </div>
                            </div>`;
                $('.chat-bodys-'+id).append(msgg2);
                $('.chat-msg-'+id).val(""); 
                $('.chat-bodys-'+id).scrollTop(100000000000000); 
                $('.upload-pgrs-'+id).hide();
            } else {
                alert(result.error.message);
            }
        });
    } else{
        alert("Please type some message or insert file.");
    }
    $('.chat-msg-'+id).val('');
    $('.file-'+id).val('');
    return false;
}
function sendMsg1(id){
    var replymsg = $('.chat-msg-'+id).val().trim();
    var u_type = $('.user_type').val();
    var bid_id = $('.bid_id').val();
    console.log(u_type);
    if(u_type != 'P'){
        u_type = 'A';
    }
    var files = $('.file-'+id).prop('files');
    data = new FormData();
    data.append('_token', "{{ csrf_token() }}");
    data.append('property_id',id);
    data.append('socket_id',socketId);
    data.append('message',replymsg);
    data.append('u_type',u_type);
    data.append('bid_id',bid_id);
    $.each(files, function(k,file){
        data.append('file', file);
    });
    var filecnt = Object.keys(files).length;
    if (replymsg || filecnt>0) {
        $.ajax({
            url: '{{ route("send.ajax") }}',
            type: 'POST',
            dataType: 'JSON',
            data: data,
            enctype: 'multipart/form-data',
            processData: false,  
            contentType: false
        })
        .done(result => {

            if (result.message_master_id) {
                if(result.user_type == 'P'){
                    var user_type = result.user_type;
                }else{
                    var user_type = 'A';
                }
                index=user_arr.indexOf(id);
                message_master_id=result.message_master_id;
                user_arr[index]=message_master_id;
                localStorage.setItem("user_arr", user_arr);
                $('.main-chat-box-'+id).remove();
                $('.mobile-main-chat-'+id).remove();
                title=result.title;
                var style='';
                if(result.online_status=='N'){
                    style='style="color:red"';
                }else{
                    style='style="color:rgb(36, 198, 36)"';
                }
                if(title.length>40){
                    title=title.substr(0,40)+".."
                }
                if(user_type == 'P'){
                    html=`<div class="main-chat-box main-chat-box-${message_master_id}" >
                <div class="chat-heads minimise-chat-box"  data-id="${message_master_id}">
                    <div class="chat-title ">
                        <span><i aria-hidden="true" class="fa fa-circle online-${result.u_slug}" ${style}></i> <b class="username-${message_master_id} ">${result.u_name}</b></span>
                    </div>
                    <div class="chat-action-des"  data-id="${message_master_id}">
                        <a href="javascript:;" class="minimise-chat-box-icon" data-id="${message_master_id}"><i aria-hidden="true" class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="chat-action" data-id="${message_master_id}" style="margin-left: 5px;"><i aria-hidden="true" class="fa fa-times"></i></a>
                    </div>
                </div>
                <div class="buildme">
                <p><a href="{{url('service-provider-profile')}}/${result.s_slug}" target="_blank">${title}</a>
                </p>
                </div>
                 <input type="hidden" class="user_type" value="${result.user_type}">
                <div class="chat-bodys chat-bodys-${message_master_id}"></div>
                <form class="frm_msg" onsubmit="return sendMsg(${message_master_id})" data-id="${message_master_id}">
                    <div class="chat-fots block-msg--new-popup-${message_master_id}">
                        <span class="upload-image-pgrs upload-pgrs-${message_master_id}" style="display:none"><i class="fa fa-upload" aria-hidden="true"></i> Uploading...</span>
                        <textarea  class="type-msgs block-popup-${message_master_id} chat-msg-${message_master_id}" data-id="${message_master_id}" placeholder="Type your message.."></textarea>
                        <div class="upload_box block-popup-${message_master_id}">
                            <input type="file" id="file${message_master_id}" data-id="${message_master_id}" class="file-upload-chat file-${message_master_id}">
                            <label for="file${message_master_id}" class="btn-2"></label>
                            
                        </div>
                        <button type="submit" class="send-msg-pop-btn block-popup-${message_master_id}"><img src="{{asset("public/frontend/images/sendbtn.png")}}"></button>
                        <p class="file-name-foots block-popup-${message_master_id} file-name-${message_master_id}"></p>
                    </div>
                </form>
            </div>`;
                }
                else{
                html=`<div class="main-chat-box main-chat-box-${message_master_id}" >
                <div class="chat-heads minimise-chat-box"  data-id="${message_master_id}">
                    <div class="chat-title ">
                        <span><i aria-hidden="true" class="fa fa-circle online-${result.u_slug}" ${style}></i> <b class="username-${message_master_id} ">${result.u_name}</b></span>
                    </div>
                    <div class="chat-action-des"  data-id="${message_master_id}">
                        <a href="javascript:;" class="minimise-chat-box-icon" data-id="${message_master_id}"><i aria-hidden="true" class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="chat-action" data-id="${message_master_id}" style="margin-left: 5px;"><i aria-hidden="true" class="fa fa-times"></i></a>
                    </div>
                </div>
                <div class="buildme">
                <p><a href="{{url('property')}}/${result.s_slug}" target="_blank">${title}</a></p>
                </div>
                <input type="hidden" class="user_type" value="${result.user_type}">
                <div class="chat-bodys chat-bodys-${message_master_id}"></div>
                <form class="frm_msg" onsubmit="return sendMsg(${message_master_id})" data-id="${message_master_id}">
                    <div class="chat-fots block-msg--new-popup-${message_master_id}">
                        <span class="upload-image-pgrs upload-pgrs-${message_master_id}" style="display:none"><i class="fa fa-upload" aria-hidden="true"></i> Uploading...</span>
                        <textarea  class="type-msgs block-popup-${message_master_id} chat-msg-${message_master_id}" data-id="${message_master_id}" placeholder="Type your message.."></textarea>
                        <div class="upload_box block-popup-${message_master_id}">
                            <input type="file" id="file${message_master_id}" data-id="${message_master_id}" class="file-upload-chat file-${message_master_id}">
                            <label for="file${message_master_id}" class="btn-2"></label>
                            
                        </div>
                        <button type="submit" class="send-msg-pop-btn block-popup-${message_master_id}"><img src="{{asset("public/frontend/images/sendbtn.png")}}"></button>
                        <p class="file-name-foots block-popup-${message_master_id} file-name-${message_master_id}"></p>
                    </div>
                </form>
            </div>`;
            }
            $('.cht-popup').append(html);
            $margin_right=`${index * 300}px`;
            $('.main-chat-box-'+message_master_id).css('margin-right',$margin_right);
                mobile_html=`<li class="mobile-main-chat-${message_master_id}"><a href="javascript:;"><strong  class="mobile-box-${message_master_id} mobile-chat-person-li" data-id="${message_master_id}">
                                <span><img src="${result.u_image}" alt=""></span><i aria-hidden="true" class="fa fa-circle online-${result.u_slug}" ${style}  style="font-size: 10px;"></i> ${result.u_name}
                            </strong><strong class="chat-action" data-id="${message_master_id}" style="margin-left: 5px; "><i aria-hidden="true" class="fa fa-times" style="float:right;margin: 6px;"></i></strong></a>
                            </li>`; 
                style='';   
                current_mob_chat_id=message_master_id;
                $('.mobile-chat-user-list').append(mobile_html);
                if (window.matchMedia('(max-width: 767px)').matches) {
                    $('.mobile-chat-show').show();
                }
                $('.cht-popup-moblie').html(html);
                localStorage.setItem("current_mob_chat_id", current_mob_chat_id);
                $.ajax({
                    url: '{{ route("user.message") }}',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        message_master_id:message_master_id,
                        _token: '{{ csrf_token() }}',
                    }
                })
                .done(result => {
                    if (result.result) {
                        result.result.forEach(function(item, index){
                            var imgg = "{{url('public/frontend/images/blank.png')}}";
                            var file='';
                            attach_file=``; 
                            if(item.get_user.profile_pic){
                                imgg = "{{url('storage/app/public/profile_picture/')}}";
                                imgg = imgg+'/'+item.get_user.profile_pic;
                            }
                            if(item.file !== null){
                                file = "{{url('storage/app/public/message_files/')}}";
                                file = file+'/'+item.file;
                                filename=item.file.substr(item.file.lastIndexOf("_")+1);
                                if(item.user_id==my_id){
                                    attach_file=`<br><a href="${file}" class="msg-link-color" target=_blank>${filename}</a>`;
                                }else{
                                    attach_file=`<br><a href="${file}" target=_blank>${filename}</a>`;
                                }
                            }
                            var img_div1 = `<a class="avatar avatar-online"><img src="${imgg}" alt=""><i></i></a>`;
                            var img_div2 = `<a class="avatar avatar-online recive_img" href="javascript:;"><img src="${imgg}" alt=""></a>`;
                            var username_div = `<span class="username">${item.get_user.name}</span>`; 
                            if(item.user_id==my_id){
                                var msgg = `<div class="chat">
                                    <div class="chat-avatar">${img_div1}</div>
                                    <div class="chat-body">
                                        <div class="chat-content">
                                            <p class="send_msgg">${nl2br(item.message)}${attach_file}</p>
                                        </div>
                                        <time class="chat-time">${getTimeDiff(item.created_at)}</time>
                                    </div>
                                </div>`;
                            }else{
                                var msgg = `<div class="chat chat-left">
                                    <div class="chat-avatar">${img_div2}</div>
                                    <div class="chat-body">
                                        ${username_div}
                                        <div class="chat-content">
                                            <p class="recive_msg">${nl2br(item.message)}${attach_file}</p>
                                        </div>
                                        <time class="chat-time">${getTimeDiff(item.created_at)}</time>
                                    </div>
                                </div>`;
                            }
                            $('.chat-bodys-'+message_master_id).append(msgg);
                        });
                        $('.chat-bodys-'+message_master_id).scrollTop(100000000000000); 
                    } 
                });
            } else {
                alert(result.error.message)
            }
        });
    } else{
      alert('Please type some message or insert file.');
    }
    // alert(id);
    return false;
}
function refreshListing(){
    var keyword = $('.hasDatepicker').val();
    if($('.achrive-message-list').hasClass("active")){
        var reqData = {
        'jsonrpc' : '2.0',                
        '_token'  : '{{csrf_token()}}',
        'data'    : {
            'keyword'    : keyword,
            'type'    : 'A',
            }
        };
    }else{
        var reqData = {
        'jsonrpc' : '2.0',                
        '_token'  : '{{csrf_token()}}',
        'data'    : {
            'keyword'    : keyword
            }
        };
    }
    console.log(reqData);
    $.ajax({
        url: '{{ route('message.search') }}',
        dataType: 'json',
        data: reqData,
        type: 'post',
        success: function(response){
            if(response.msgcnt)
                    $('.msgcnt').html(response.msgcnt);
            console.log(response);
            var html='';
            response.message.forEach(function(item, index){
                if(item.seeker_id==my_id)
                    user=item.get_provider;
                else
                    user=item.get_seeker;
                if(my_id== item.get_service.user_id && item.is_block=='Y'){
                    data_block="Y";
                }else if(my_id== item.get_service.user_id && item.is_block=='N'){
                    data_block="N";
                }else if(item.is_block=='Y'){
                    data_block="BL";
                }else{
                    data_block="A";
                } 
                if(item.get_service.request_title.length>25){
                    title=item.get_service.request_title.substr(0,25)+".."
                }else{
                    title=item.get_service.request_title
                }
                var imgg = "{{asset('public/frontend/images/blank.png') }}";
                var file='';
                if(user.profile_pic){
                    imgg = "{{url('storage/app/public/profile_picture/')}}";
                    imgg = imgg+'/'+user.profile_pic;
                }
                html+=`<div class="chat-holders ${item.id==$('.message_master_id').val()?'chat-list-active':''}" data-id="${item.id}" data-block=${data_block}  data-name="${user.name}"  data-online_status="${user.online_status}" data-slug="${user.slug}" data-title="${title}"> 
                    <span class="holder-image">
                        <img src="${imgg}">
                        <span class="user-online-span u-${user.slug}"><i aria-hidden="true" class="fa fa-circle online-${user.slug}" style="color:${user.online_status=='Y'?'#24c624':'red'}"></i></span>
                    </span>
                    <h4>${user.name}</h4>
                    <h5>${getTimeDiff(item.get_message_detail_last_message.created_at)}</h5>
                    <div class="d-flex align-items-center mess_count_info">
                    <p>${title}</p>
                    <span class="mes_co msgdtlcnt-${item.id}">${item.get_unread_message_count}</span>
                    </div>
                </div>`;
            });
            if(!(Array.isArray(response.message) && response.message.length)){
                html='<p class="err_mesg_msg_page">You do not have any messages in inbox</p>';
            }
            $('.all-chat-lists  .scrollbar-content').html(html);
            $('.all-chat-lists').find('.scrollbar-content').animate({top: 0},0); 
        },
        error:function(error) 
        {
            console.log(error.responseText);
        }
    });
}
$(document).ready(function(){
    if($(window).innerWidth() <= 751) {
        $('.cht-popup').remove();
    }else{
        $('.cht-popup-moblie').remove();
    }
    @if(!@Auth::user())
    $('.mobile-chat-show').hide();
    localStorage.removeItem("user_arr");
    localStorage.removeItem("user_minm_arr");
    @endif
    user_str_arr=localStorage.getItem("user_arr");
    if(user_str_arr!== null && Number(user_str_arr)!=0){
        nuser_arr=user_str_arr.split(",");
        user_str_min_arr=localStorage.getItem("user_minm_arr");
        if (window.matchMedia('(max-width: 767px)').matches) {
            $('.mobile-chat-show').show();
        }
        nuser_arr.forEach(function(m_id, len){
            message_master_id=Number(m_id);
            $.ajax({
                url: '{{ route("get.message.master") }}',
                type: 'POST',
                dataType: 'JSON',
                async: false,
                data: {
                    message_master_id:message_master_id,
                    _token: '{{ csrf_token() }}',
                }
            })
            .done(result => {
                // alert(result.bid_id+'1');
                console.log(result.message_master_id,result.title,result.s_slug,result.online_status,result.u_name,result.u_slug,result.u_image,result.type,result.message_master_id,result.user_type);
                makePopup(result.message_master_id,result.title,result.s_slug,result.online_status,result.u_name,result.u_slug,result.u_image,result.type,result.message_master_id,result.user_type);
                if(result.is_block=="Y"){
                    $('.block-popup-'+result.message_master_id).hide();
                    $('.block-msg--new-popup-'+result.message_master_id).append('<h6 style="text-align: center" class="block-msg-pop-'+result.message_master_id+'">'+result.block_by+' have block the conversation</h6>')
                }
            });
        });   
    }
    $(".show-btnns").click(function(){
        $(".all-users-show").slideToggle();
    });
    $('body').on('click','.chat-action',function(){
        id=$(this).data('id');
        index=user_arr.indexOf(id);
        if(index>-1){
            if(index!=(user_arr.length-1)){
                for (let i = index+1; i < user_arr.length; i++) {
                    var elem = user_arr[i];
                    $('.main-chat-box-'+elem).css("margin-right", ((i-1)*300)+"px");  
                }
            }
            user_arr.splice(index, 1);
        }
        $('.main-chat-box-'+id).remove();
        $('.mobile-main-chat-'+id).remove();
        current_mob_chat_id=0;
        localStorage.setItem("user_arr", user_arr);
        if(user_arr.length==0){
            $('.mobile-chat-show').hide();
            localStorage.removeItem("user_arr");
            localStorage.removeItem("user_minm_arr");
        }
    });
    $('body').on('change','.file-upload-chat',function(){
       var id=$(this).data('id');
       console.log(id+"hhih");
       var filename = $('.file-'+id).val().split('\\').pop();
        $('.file-name-'+id).html(filename);
    })
    $('body').on('click','.minimise-chat-box',function(){
        id=$(this).data('id');
        if($('.main-chat-box-'+id).css("margin-bottom")==-322+"px"){
            $('.main-chat-box-'+id).css("margin-bottom",0+"px");
            index=user_minm_arr.indexOf(id);
            user_minm_arr.splice(index, 1);
            localStorage.setItem("user_minm_arr", user_minm_arr);
        }else{
                $('.main-chat-box-'+id).css("margin-bottom",-322+"px");
            user_minm_arr.push(id);
            localStorage.setItem("user_minm_arr", user_minm_arr);
        }
        current_mob_chat_id=0;
        localStorage.setItem("current_mob_chat_id", current_mob_chat_id);
        $('.cht-popup-moblie').html("");
    });
    $('.chat_btn').click(function(){

        var usertype = $(this).data('usertype');
        var msgtyp = $(this).data('msgtyp');
        console.log(msgtyp);
        var bid_id = $(this).data('bidid');
        if(usertype == 'P'){
            usertype = 'P';
        }else if(usertype == 'undefined'){
            usertype = 'A';
        }
        $.ajax({
            url: '{{ route("get.message.master") }}',
            type: 'POST',
            dataType: 'JSON',
            data: {
                usertype:usertype,
                property_id:$(this).data("propertyid"),
                msgtyp:msgtyp,
                bid_id:bid_id,
                _token: '{{ csrf_token() }}',
            }
        })
        .done(result => {
             // alert(result.bid_id+'2');
            if (result.message_master_id) {
                message_master_id=result.message_master_id;
                
                if(!(jQuery.inArray(message_master_id, user_arr) !== -1)){
                    makePopup(result.message_master_id,result.title,result.s_slug,result.online_status,result.u_name,result.u_slug,result.u_image,result.type,result.message_master_id,result.user_type,result.bid_id);
                }else if(current_mob_chat_id!=message_master_id){ 
                    if (window.matchMedia('(max-width: 767px)').matches) {    
                    make_molile_popup(result.message_master_id,result.title,result.s_slug,result.online_status,result.u_name,result.u_slug,result.u_image,result.type,result.message_master_id,result.user_type);  
                    }                    
                }
            }
        });               
    });


    $('body').on('keydown','.type-msgs',function(e){
        if (e.key == 'Enter' && e.ctrlKey){
            test=$(this).val()+"\r\n";
            $(this).val(test);
            e.preventDefault();
        }else if(e.key == 'Enter'){
            if($(this).data('type')=="B"){
                sendMsg1($(this).data('id'));
            }else{
                sendMsg($(this).data('id'));
            }
        }
    })
    $('body').on('click', '.chat-holders', function() {
        $('.chat-holders').removeClass('chat-list-active');
        $(this).addClass('chat-list-active');
        message_master_id = $(this).data('id');
        $('.msgdtlcnt-'+message_master_id).html(0);
        $('.message_master_id').val(message_master_id);
        style='red';
        if($(this).data("online_status")=='Y')
            style='#24c624';
        $(".msg-user-details").html($(this).data("name")+'<span class="name-status"><i aria-hidden="true" class="fa fa-circle online-'+$(this).data("slug")+'" style="color:'+style+'"></i></span>');
        $('.msg-service-title').html($(this).data('title'));
        $('.archive-flag').show();
        if($('.achrive-message-list').hasClass("active")){
            $('.archive-flag').attr("title","Unarchive");
            $('.archive-flag').attr("data-type","U");
        }else{
            $('.archive-flag').attr("title","Archive");
            $('.archive-flag').attr("data-type","A");
        }
        if($(this).data('block')=='Y'){
            $('.block-btn').show();
            // $('.block-btn').html('Unblock');
             $('.block-btn').html('<i class="fa fa-ban" aria-hidden="true" ></i>');
             $('.block-btn').attr("title","Unblock");
            $('.block-btn').addClass('unblock');
            $('.block-btn').removeClass('block');
            $('.attachedd-type').hide();
            $('.block-msg').remove();
            $('.message-box-div').append('<h2 class="block-msg">You have block the conversation</h2>');
        }else if($(this).data('block')=='N'){
            $('.block-btn').show();
            // $('.block-btn').html('Block');
             $('.block-btn').html('<i class="fa fa-ban" aria-hidden="true" ></i>');
              $('.block-btn').attr("title","Block");
            $('.block-btn').removeClass('unblock');
            $('.block-btn').addClass('block');
            $('.attachedd-type').show();
            $('.block-msg').remove();
        }else if($(this).data('block')=='BL'){
            $('.block-btn').hide();
            $('.block-btn').removeClass('unblock');
            $('.block-btn').removeClass('block');
            $('.block-btn').html('');
            $('.attachedd-type').hide();
            $('.block-msg').remove();
            $('.message-box-div').append('<h2 class="block-msg">'+$(this).data("name")+' have block the conversation</h2>');
        }else{
            $('.block-btn').hide();
            $('.block-btn').removeClass('unblock');
            $('.block-btn').removeClass('block');
            $('.block-btn').html('');
            $('.attachedd-type').show();
            $('.block-msg').remove();
        }
        $.ajax({
            url: '{{ route("user.message") }}',
            type: 'POST',
            dataType: 'JSON',
            data: {
                message_master_id:message_master_id,
                _token: '{{ csrf_token() }}',
            }
        })
            .done(result => {
                $('.chat-box').html('');
                if(result.msgcnt)
                    $('.msgcnt').html(result.msgcnt);
                if(result.result) {
                    result.result.forEach(function(item, index){
                        var imgg = "{{url('public/frontend/images/blank.png')}}";
                        var file='';
                        attach_file=``; 
                        if(item.get_user.profile_pic){
                            imgg = "{{url('storage/app/public/profile_picture/')}}";
                            imgg = imgg+'/'+item.get_user.profile_pic;
                        }
                        if(item.file !== null){
                            file = "{{url('storage/app/public/message_files/')}}";
                            file = file+'/'+item.file;
                            filename=item.file.substr(item.file.lastIndexOf("_")+1);
                            if(item.user_id==my_id){
                                attach_file=`<br><a href="${file}" class="msg-link-color" target=_blank>${filename}</a>`;
                            }else{
                                attach_file=`<br><a href="${file}" target=_blank>${filename}</a>`;
                            }
                            // attach_file=`hi`; 
                        }else{
                            attach_file=``; 
                        }
                    if(item.user_id==my_id){
                    var msgg =`<div class="chat_mass_itm chat_mass_itm_rig reciever-div">
                                <div class="media">
                                    <em>
                                        <img src="${imgg}" alt="">           
                                    </em>
                                                            
                                                        
                                    <div class="media-body">
                                        <div class="chat_mass_bx">
                                            <p>${nl2br(item.message)}${attach_file}</p>
                                        </div>
                                    <span>${getTimeDiff(item.created_at)}</span>
                                    </div>
                                </div>
                            </div>`;
                    // var msgg = `<div class="media w-100 ml-auto mb-3 reciever-div">
                    //                     <div class="media-body mr-3">
                    //                         <div class="bg-primary rounded py-2 px-3 mb-1">
                    //                             <p class="text-small mb-0 text-white">${nl2br(item.message)}${attach_file}
                    //                             </p>
                    //                         </div>
                    //                         <div class="w-100"></div>
                    //                         <p class="small">${getTimeDiff(item.created_at)}</p>
                    //                     </div>
                    //                     <img width="30" class="rounded-circle" src="${imgg}" alt="">
                    //                 </div>`;
                    }else{

                        var msgg = `<div class="chat_mass_itm sender-div">
                                        <div class="media">
                                            <em>
                                                <img src="${imgg}" alt="">
                                            </em>
                                            <div class="media-body">
                                                <h5>${item.get_user.name}</h5>
                                                    <div class="chat_mass_bx">
                                                        <p>${nl2br(item.message)}${attach_file}</p>
                                                    </div>
                                                <span>${getTimeDiff(item.created_at)}</span>
                                            </div>
                                        </div>
                                    </div>`;
                    //     var msgg = `<div class="media w-100 mb-3 sender-div">
                    //     <img src="${imgg}" alt="" width="32" class="rounded-circle user-round-circle">
                    //     <div class="media-body ml-3">
                    //         <h5>${item.get_user.name}</h5>
                    //         <div class="bg-light rounded py-2 px-3 mb-1">
                    //             <p class="text-small mb-0 text-muted">${nl2br(item.message)}${attach_file}</p>
                    //         </div>
                    //         <p class="small">${getTimeDiff(item.created_at)}</p>
                    //     </div>
                    // </div>`;
                    }
                    $('.chat-box').append(msgg);  
                });
                scrollChat();    
            } 
        });
    });
    $('.online_away').change(function() {
        var online_status=false;
        var slug='{{@Auth::user()->slug}}';
        $('.online-status-'+slug).html('Away');
        console.log(slug);
        if ($('.online_away').is(':checked')) {
            online_status=true;
            $('.online-status-'+slug).html('Online');
        }
        $.ajax({
            url: '{{ route('online.away') }}',
            type: 'POST',
            data: {
                    jsonrpc: 2.0,
                    _token: '{{ csrf_token() }}',
                    'socket_id' :socketId,
                    online_status:online_status
                }
        });
    });
    $('body').on('click','.mobile-chat-person-li',function(){
        $(".all-users-show").slideToggle();
        message_master_id=Number($(this).data('id'));
        $.ajax({
            url: '{{ route("get.message.master") }}',
            type: 'POST',
            dataType: 'JSON',
            async: false,
            data: {
                message_master_id:message_master_id,
                _token: '{{ csrf_token() }}',
            }
        })
        .done(result => {
             // alert(result.user_type+'3');
            make_molile_popup(result.message_master_id,result.title,result.s_slug,result.online_status,result.u_name,result.u_slug,result.u_image,result.type,result.message_master_id);
            if(result.is_block=="Y"){
                $('.block-popup-'+result.message_master_id).hide();
                $('.block-msg--new-popup-'+result.message_master_id).append('<h6 style="text-align: center" class="block-msg-pop-'+result.message_master_id+'">'+result.block_by+' have block the conversation</h6>')
            }
        });
    })
})
   

</script>

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::post('/register', 'Auth\RegisterController@customRegister')->name('new.register');

Route::get('email-verify/{id}/{vcode}', 'Auth\RegisterController@verifyEmail')->name('email.verify');
Route::post('verify-mobile', 'Auth\RegisterController@verifyMobile')->name('check.mobile.verify');

Route::get('email-verified','Auth\RegisterController@emailVerifiedMsg')->name('user.email.verified.msg');
Route::get('email-verified-failed','Auth\RegisterController@emailVerifiedFailed')->name('user.email.verified.failed');

Route::post('check-email', 'HomeController@checkEmail')->name('check.email');
Route::post('check-mobile', 'HomeController@checkMobile')->name('check.mobile');
Route::post('resend-code','Auth\RegisterController@resendCode')->name('resend.code');

Route::post('/login', 'Auth\LoginController@customLogin')->name('user.login');
Route::get('/logout', 'Auth\LoginController@logout')->name('user.logout');
Route::post('/forgot-password', 'Auth\LoginController@forgotPassword')->name('user.forgot.password');
Route::get('/password-reset/{id}/{vcode}', 'Auth\ResetPasswordController@showResetForm')->name('user.reset.password');
Route::post('/user-password-reset', 'Auth\ResetPasswordController@reset')->name('user.password.request');
Route::post('/forgot-password-otp-verify', 'Auth\LoginController@verifyOtp')->name('user.forgot.password.otp.verify');
Route::post('/change-password', 'Auth\LoginController@changePassword')->name('change.password');


Route::get('login/{user_type}/{provider_type}', 'Auth\LoginController@redirectToProvider')->name('login.social');
Route::get('login/{user_type}/{provider_type}/callback', 'Auth\LoginController@handleProviderCallback')->name('login.social.callback');
Route::post('social_login', 'Auth\LoginController@socialRegistation')->name('login.social.register');

Route::get('get-state','HomeController@getState')->name('get.state');
Route::get('get-city','HomeController@getCity')->name('get.city');
Route::get('get-subcategory','HomeController@getSubcategoryUser')->name('get.sub.category');
Route::post('get-locality','HomeController@searchLocality')->name('get.locality');
Route::post('get-available-locality','HomeController@searchLocalityAvailable')->name('get.locality.available');
Route::post('save-rating','Modules\Search\PropertySearchController@saveReting')->name('rating.save');

Route::get('get-sub-category','HomeController@getSubCategory')->name('get.subcategory');

// Razorpay

Route::post('razorpay-payment-index','Modules\Razorpay\RazorpayController@index')->name('razorpay.index');
Route::post('razorpay-payment','Modules\Razorpay\RazorpayController@store')->name('razorpay.payment.store');

// Loan Request
Route::post('save-loan-request', 'Modules\Search\SearchController@saveLoanRequest')->name('save.loan.request');
//Property Management
Route::post('save-property-management','Modules\Search\SearchController@savePropertyManagement')->name('save.property.management');

Route::get('/dashboard', 'Modules\User\ProfileController@dashboard')->name('user.dashboard');
Route::get('/profile', 'Modules\User\ProfileController@editProfile')->name('user.profile');
Route::post('/profile-save', 'Modules\User\ProfileController@editProfileSave')->name('user.profile.save');
Route::post('/temp-email-save', 'Modules\User\ProfileController@changeEmail')->name('user.temp.email.save');
Route::post('/temp-mobile-save', 'Modules\User\ProfileController@changeMobile')->name('user.temp.mobile.save');

Route::post('/visit-request-create', 'Modules\User\PropertyVisitController@visitRequestStore')->name('visit.request.create');
Route::post('/visit-request-check', 'Modules\User\PropertyVisitController@visitRequestCheck')->name('visit.request.check');

Route::get('/job-details/{slug?}','HomeController@jobDetails')->name('job.details');

Route::any('/user-upcoming-property-visit', 'Modules\User\PropertyVisitController@userVisitRequest')->name('user.property.visit.request');
Route::get('/user-upcoming-property-visit-cancel/{id}', 'Modules\User\PropertyVisitController@userVisitRequestCancel')->name('user.property.visit.request.cancel');

Route::get('/user-update-visit','Modules\User\PropertyVisitController@updateVisit')->name('user.update.visit');
Route::get('/user-update-reschedule','Modules\User\PropertyVisitController@updateReschedule')->name('user.update.reschedule');

Route::get('/user-post-service-requirments/{id?}', 'Modules\User\PostServiceController@addPostJob')->name('user.post.job');
Route::post('/user-post-service-requirments-save/{id?}', 'Modules\User\PostServiceController@addPostJobSave')->name('user.post.job.save');
Route::any('/user-my-service-requirments', 'Modules\User\PostServiceController@myPostJob')->name('user.my.post.job');
Route::get('/user-post-job-delete/{id}', 'Modules\User\PostServiceController@myPostJobDelete')->name('user.post.job.delete');
Route::any('/user-proposal-list/{id?}', 'Modules\User\PostServiceController@proposalList')->name('user.proposal.list');
Route::get('/jobs-award-status/{id?}', 'Modules\User\PostServiceController@jobAward')->name('user.job.award');

//Milestone

Route::get('get-milestone/{id?}','Modules\User\MilestoneController@getMilestone')->name('user.get.milestone');
Route::post('make-payment','Modules\User\MilestoneController@makePayment')->name('make.payment');

Route::get('cancel-milestone','Modules\User\MilestoneController@cancelMilestone')->name('user.cancel.milestone');

Route::get('release-milestone','Modules\User\MilestoneController@releaseMilestone')->name('user.release.milestone');
Route::get('release-milestone-request','Modules\User\MilestoneController@releaseMilestoneRequest')->name('release.milestone.request');

Route::get('/user-post-service-requirments-image-remove/{id?}', 'Modules\User\PostServiceController@jobImageRemove')->name('user.add.job.image.remove');

Route::get('/user-save-search-property', 'Modules\User\SaveSearchController@userSaveProperty')->name('user.save.property');
Route::get('/user-save-search-agent', 'Modules\User\SaveSearchController@userSaveAgent')->name('user.save.agent');
Route::get('/user-save-search-pro', 'Modules\User\SaveSearchController@userSavePro')->name('user.save.pro');
Route::get('/user-save-search-property-delete/{id}', 'Modules\User\SaveSearchController@userSavePropertyDelete')->name('user.save.property.delete');
Route::get('/user-save-search-agent-delete/{id}', 'Modules\User\SaveSearchController@userSaveAgentDelete')->name('user.save.agent.delete');
Route::get('/user-save-search-pro-delete/{id}', 'Modules\User\SaveSearchController@userSaveProDelete')->name('user.save.pro.delete');

Route::get('/delete-profile', 'Modules\User\ProfileController@userDeleteProfile')->name('user.delete.profile');
Route::post('/jobs-rating-save', 'Modules\User\PostServiceController@jobRatingSave')->name('job.rating.save');
Route::get('/get-rivirtual-assistance/{id?}', 'Modules\User\PostServiceController@getRivirtualAssist')->name('user.get.rivirtual.assist');
/* Agent */
Route::get('/agent/dashboard', 'Modules\Agent\ProfileController@dashboard')->name('agent.dashboard');
Route::get('/agent/profile', 'Modules\Agent\ProfileController@editProfile')->name('agent.profile');
Route::post('/agent/profile-save', 'Modules\Agent\ProfileController@editProfileSave')->name('agent.profile.save');

Route::post('/agent/temp-email-save', 'Modules\Agent\ProfileController@changeEmail')->name('agent.temp.email.save');
Route::post('/agent/temp-mobile-save', 'Modules\Agent\ProfileController@changeMobile')->name('agent.temp.mobile.save');

Route::get('/agent/add-property-details/{id?}', 'Modules\Agent\PropertyController@addProperty')->name('agent.add.property');
Route::post('/agent/add-property-details-save/{id?}', 'Modules\Agent\PropertyController@propertySave')->name('agent.add.property.save');
Route::get('/agent/add-property-image/{id}', 'Modules\Agent\PropertyController@addPropertyImage')->name('agent.add.property.image');
Route::post('/agent/add-property-image-save/{id}', 'Modules\Agent\PropertyController@propertyImageSave')->name('agent.add.property.image.save');
Route::get('/agent/add-property-image-remove/{id}', 'Modules\Agent\PropertyController@propertyImageRemove')->name('agent.add.property.image.remove');
Route::any('/agent/my-property', 'Modules\Agent\PropertyController@myProperty')->name('agent.my.property');
Route::any('/agent/my-property-status-change/{id}', 'Modules\Agent\PropertyController@myPropertyStatusChange')->name('agent.my.property.status.change');
Route::any('/agent/my-property-sold/{id}', 'Modules\Agent\PropertyController@myPropertySold')->name('agent.my.property.sold');
Route::any('/agent/my-property-unsold/{id}', 'Modules\Agent\PropertyController@myPropertyUnSold')->name('agent.my.property.un.sold');
Route::any('/agent/my-property-delete/{id}', 'Modules\Agent\PropertyController@myPropertyDelete')->name('agent.my.property.delete');

Route::get('/agent/property-image-set-default/{id}', 'Modules\Agent\PropertyController@propertyImageSetDefault')->name('agent.image.set.default');

Route::get('/agent/manage-availability', 'Modules\Agent\ProfileController@manageAvailability')->name('agent.manage.availability');
Route::any('/agent/manage-availability-save', 'Modules\Agent\ProfileController@manageAvailabilitySave')->name('agent.manage.availability.save');
Route::any('/agent/manage-availability-remove/{id}', 'Modules\Agent\ProfileController@removeAvailability')->name('agent.manage.availability.remove');
Route::post('/agent/reschedule','Modules\Agent\ProfileController@rescheduleAvailability')->name('agent.reschedule.availability');
Route::get('/agent/update-visit','Modules\Agent\ProfileController@updateVisit')->name('agent.update.visit');

Route::any('/agent/visit-request', 'Modules\Agent\VisitRequestController@visitRequest')->name('agent.visit.request');
Route::get('/agent/visit-request-cancel/{id}', 'Modules\Agent\VisitRequestController@visitRequestCancel')->name('agent.visit.request.cancel');

Route::get('/agent/view-profile/{v_id?}/{u_id?}','Modules\Agent\VisitRequestController@viewProfile')->name('agent.view.profile');

Route::get('/agent/agent-subcription', 'Modules\Agent\SubcriptionController@agentSubcription')->name('agent.subcription');
Route::get('/agent/agent-set-subscription/{id?}', 'Modules\Agent\SubcriptionController@agentSetSubscription')->name('agent.set.subscription');
/*search-property*/
Route::any('/search-property', 'Modules\Search\PropertySearchController@searchProperty')->name('search.property');
Route::any('/property/{slug}', 'Modules\Search\PropertySearchController@searchPropertyDetails')->name('search.property.details');
// Route::any('/search-property-details/{slug}', 'Modules\Search\PropertySearchController@searchPropertyDetails')->name('search.property.details');

Route::any('/get-time-slot', 'Modules\Search\PropertySearchController@getTimeSlot')->name('get.time.slot');

Route::any('/save-property/{id}', 'Modules\Search\PropertySearchController@saveProperty')->name('property.save');
Route::post('/add-rating', 'Modules\Search\PropertySearchController@saveReting')->name('rating.save');

Route::get('/agent/agent-rating-review', 'Modules\Agent\PropertyController@agentRatingReview')->name('agent.rating.review');

//pusher
	Route::post('send-ajax', 'Modules\Message\MessageController@sendAjax')->name('send.ajax');
	Route::post('add-user', 'Modules\Message\MessageController@addUser')->name('add.user');
	Route::post('remove-user', 'Modules\Message\MessageController@removeUser')->name('remove.user');
	Route::post('message-search', 'Modules\Message\MessageController@searchAjax')->name('message.search');
	Route::post('user-message', 'Modules\Message\MessageController@userMessage')->name('user.message');
	Route::post('typing-ajax', 'Modules\Message\MessageController@typingAjax')->name('typing.ajax');
	Route::get('receive', 'Modules\Message\MessageController@sendApi')->name('receive');
	Route::post('get-message-master', 'Modules\Message\MessageController@getMessageMaster')->name('get.message.master');
	Route::get('message-archive/{id?}','Modules\Message\MessageController@achive')->name('message.archive');
	Route::post('block-message/{id?}','Modules\Message\MessageController@blockMessage')->name('block.message');
	Route::post('mail-send-to-user/{id?}','Modules\Message\MessageController@mailSendToUser')->name('mail.send.to.user');
	Route::post('get-user-message-project', 'Modules\Message\MessageController@getUserMessageProject')->name('get.user.message.project');
	Route::post('online-away','HomeController@onlineAway')->name('online.away');
//Message
	Route::get('message','Modules\Message\MessageController@showMessage')->name('show.message');
	Route::post('message','Modules\Message\MessageController@showMessage')->name('show.message');


/* ServiceProvider */
Route::get('/service-provider/dashboard', 'Modules\ServiceProvider\ProfileController@dashboard')->name('service.provider.dashboard');
Route::get('/service-provider/profile', 'Modules\ServiceProvider\ProfileController@editProfile')->name('service.provider.profile');
Route::post('/service-provider/profile-save', 'Modules\ServiceProvider\ProfileController@editProfileSave')->name('service.provider.profile.save');
Route::get('/service-provider/work-images', 'Modules\ServiceProvider\ProfileController@editWorkImage')->name('service.provider.work.image');
Route::get('/service-provider/work-image-remove/{id}', 'Modules\ServiceProvider\ProfileController@removeWorkImage')->name('service.provider.remove.work.image');
Route::post('/service-provider/work-images-save', 'Modules\ServiceProvider\ProfileController@editWorkImageSave')->name('service.provider.work.image.save');

Route::post('/service-provider/temp-email-save', 'Modules\ServiceProvider\ProfileController@changeEmail')->name('service.provider.temp.email.save');
Route::post('/service-provider/temp-mobile-save', 'Modules\ServiceProvider\ProfileController@changeMobile')->name('service.provider.temp.mobile.save');

Route::post('/service-provider/save-quotes','Modules\ServiceProvider\ProfileController@saveQuotes')->name('service.provider.save.quotes');

Route::get('/service-provider/my-quotes','Modules\ServiceProvider\ProfileController@myQuotes')->name('service.provider.my.quotes');

Route::get('/service-provider/view-quote/{id?}','Modules\ServiceProvider\ProfileController@viewQuote')->name('service.provider.view.quote');

Route::get('/service-provider/delete-quotes/{id?}','Modules\ServiceProvider\ProfileController@deleteQuotes')->name('service.provider.delete.quotes');

Route::get('/service-provider/membership','Modules\ServiceProvider\ProfileController@getMembership')->name('service.provider.membership');

Route::get('/service-provider/update-package','Modules\ServiceProvider\ProfileController@updatePackage')->name('service.provider.update.package');

Route::get('/service-provider/delete-job/{id}','Modules\ServiceProvider\ProfileController@proDeleteJob')->name('service.provider.delete.job');

Route::get('/service-provider/pro-rating-review', 'Modules\ServiceProvider\ProfileController@proRatingReview')->name('pro.rating.review');


//search pro and agent

Route::any('/find-service-provider/{slug?}', 'Modules\Search\SearchController@index')->name('search.service.pro');
Route::get('/service-provider-profile/{id}', 'Modules\Search\SearchController@proProfileShow')->name('pro.profile');
Route::get('/agent-public-profile/{slug?}', 'Modules\Search\SearchController@agentProfileShow')->name('agent.public.profile');
//Route::get('/property-details/{slug?}', 'Modules\Search\SearchController@agentPropertyDetails')->name('agent.property.details');
Route::get('/agent-public-profile/{slug}', 'Modules\Search\SearchController@agentProfileShow')->name('agent.public.profile');

Route::get('/hire-a-pro', 'Modules\Search\SearchController@hirePro')->name('hire.pro');
Route::get('/sell', 'Modules\Search\SearchController@sellPage')->name('sell.page');

Route::get('/save-agent/{id}', 'Modules\Search\SearchController@agentSave')->name('agent.save');
Route::get('/save-pro/{id}', 'Modules\Search\SearchController@proSave')->name('provider.save');
Route::get('/property-save/{id}', 'Modules\Search\SearchController@saveProperty')->name('agent.property.save');
//Route::post('/add-rating', 'Modules\Search\SearchController@saveReting')->name('provider.rating.save');

Route::any('/find-agent', 'Modules\Search\SearchController@findAgent')->name('search.agent');
Route::get('/nri-property-management', 'Modules\Search\SearchController@propertyManagement')->name('property.manage.page');
Route::get('/loans', 'Modules\Search\SearchController@loanPage')->name('loan.page');
Route::get('/agents', 'Modules\Search\SearchController@searchAgent')->name('find.agent');
Route::get('/help', 'Modules\Search\SearchController@helpPage')->name('help.page');
Route::get('/commercial', 'Modules\Search\SearchController@commercialPage')->name('commercial.page');
Route::get('/residential', 'Modules\Search\SearchController@residentialPage')->name('residential.page');
//search job

Route::any('/service-provider/find-jobs', 'Modules\Search\JobSearchController@index')->name('search.job');

// Static pages

Route::get('about-us','Modules\Content\ContentController@aboutUs')->name('about.us');
Route::get('contact-us','Modules\Content\ContentController@contactUs')->name('contact.us');
Route::post('save-contact-us','Modules\Content\ContentController@saveContactUs')->name('save.contact.us');
Route::get('faq','Modules\Content\ContentController@faq')->name('faq');
Route::get('terms-and-condition','Modules\Content\ContentController@termsCondition')->name('terms.condition');
Route::get('privacy-policy','Modules\Content\ContentController@privacyPolicy')->name('privacy.policy');


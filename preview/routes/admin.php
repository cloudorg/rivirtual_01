<?php

Route::group(['namespace' => 'Admin'], function() {

    Route::get('/', 'HomeController@index')->name('admin.dashboard');

    // Login
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('admin.login');
    Route::post('login', 'Auth\LoginController@login')->name('admin.login');
    Route::post('logout', 'Auth\LoginController@logout')->name('admin.logout');

    // Register
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('admin.register');
    Route::post('register', 'Auth\RegisterController@register');

    // Passwords
    //Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    //Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('admin.save.password');
    //Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    //Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('admin.password.reset');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('admin.password.request');
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::get('password/reset/{email?}/{token?}', 'Auth\ResetPasswordController@showResetForm')->name('admin.password.reset');
    // Verify
    // Route::get('email/resend', 'Auth\VerificationController@resend')->name('admin.verification.resend');
    // Route::get('email/verify', 'Auth\VerificationController@show')->name('admin.verification.notice');
    // Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('admin.verification.verify');

    //manage category
    
    Route::get('manage-category', 'Modules\Category\CategoryController@index')->name('admin.manage.category');
    Route::get('add-category', 'Modules\Category\CategoryController@addCategory')->name('admin.add.category');
    Route::post('add-category', 'Modules\Category\CategoryController@addCategory')->name('admin.add.category');
    Route::get('edit-category/{id}', 'Modules\Category\CategoryController@editCategory')->name('admin.edit.category');
    Route::post('edit-category/{id}', 'Modules\Category\CategoryController@editCategory')->name('admin.edit.category');
    Route::get('delete-category/{id}','Modules\Category\CategoryController@deleteCategory')->name('admin.delete.category');
    Route::post('check-category','Modules\Category\CategoryController@checkCategory')->name('check.category');

   // Route::get('popular-category/{id}','Modules\Category\CategoryController@makePopular')->name('admin.make.popular');
    //manage language
     
    Route::get('manage-language', 'Modules\Language\LanguageController@index')->name('admin.manage.language');
    Route::get('add-language', 'Modules\Language\LanguageController@addLanguage')->name('admin.add.language');
    Route::post('add-language', 'Modules\Language\LanguageController@addLanguage')->name('admin.add.language');
    Route::get('edit-language/{id}', 'Modules\Language\LanguageController@editLanguage')->name('admin.edit.language');
    Route::post('edit-language/{id}', 'Modules\Language\LanguageController@editLanguage')->name('admin.edit.language');
    Route::get('delete-language/{id}','Modules\Language\LanguageController@deleteLanguage')->name('admin.delete.language');
    Route::post('check-language','Modules\Language\LanguageController@checkLanguage')->name('check.language');

    // manage skill

    Route::get('manage-skill', 'Modules\Skill\SkillController@index')->name('admin.manage.skill');
    Route::get('add-skill', 'Modules\Skill\SkillController@addSkill')->name('admin.add.skill');
    Route::post('add-skill', 'Modules\Skill\SkillController@addSkill')->name('admin.add.skill');
    Route::get('edit-skill/{id}', 'Modules\Skill\SkillController@editSkill')->name('admin.edit.skill');
    Route::post('edit-skill/{id}', 'Modules\Skill\SkillController@editSkill')->name('admin.edit.skill');
    Route::get('delete-skill/{id}','Modules\Skill\SkillController@deleteSkill')->name('admin.delete.skill');
    Route::post('check-skill','Modules\Skill\SkillController@checkSkill')->name('check.skill');

    //manage user
    Route::get('manage-user', 'Modules\User\UserController@index')->name('admin.manage.user');
    Route::get('user-status-update', 'Modules\User\UserController@userStatusUpdate')->name('user.status.update');
    Route::get('user-profile/{id}', 'Modules\User\UserController@userDetails')->name('admin.user.details');
    Route::get('edit-user/{id}', 'Modules\User\UserController@userEditSave')->name('admin.edit.user');
    Route::post('edit-user/{id}', 'Modules\User\UserController@userEditSave')->name('admin.edit.user');
    Route::get('user-delete-profile/{id}', 'Modules\User\UserController@userDeleteProfile')->name('admin.delete.user.profile');
    Route::post('user-delete-profile-all', 'Modules\User\UserController@usertDeleteAll')->name('admin.select.user.delete');

    Route::get('user-chat-history/{id}', 'Modules\User\UserController@userChatHistory')->name('admin.user.chat.history');
    Route::get('user-show-chat/{id}', 'Modules\User\UserController@userShowChat')->name('admin.show.chat');
    //manage agent
    Route::get('manage-agent', 'Modules\Agent\AgentController@index')->name('admin.manage.agent');
    Route::get('agent-status-update', 'Modules\Agent\AgentController@agentStatusUpdate')->name('agent.status.update');
    Route::get('agent-profile/{id}', 'Modules\Agent\AgentController@agentDetails')->name('admin.agent.details');
    Route::get('edit-agent/{id}', 'Modules\Agent\AgentController@agentEditProfile')->name('admin.edit.agent.profile');
    Route::post('edit-agent-save', 'Modules\Agent\AgentController@agentEditProfileSave')->name('admin.edit.agent.profile.save');
    Route::get('visit-request/{id}', 'Modules\Agent\AgentController@agentVisitRequest')->name('admin.agent.visit.request');
   // Route::get('visit-request-cancel/{id}', 'Modules\Agent\AgentController@agentVisitRequestCancel')->name('admin.cancel.visit.request');
    Route::get('view-availability/{id}', 'Modules\Agent\AgentController@agentViewAvailability')->name('admin.agent.availability');
    Route::post('view-availability-save', 'Modules\Agent\AgentController@agentViewAvailabilitySave')->name('admin.agent.manage.availability.save');
    Route::get('view-availability-remove/{id}', 'Modules\Agent\AgentController@agentViewAvailabilityRemove')->name('admin.agent.manage.availability.remove');
    Route::get('view-property/{id}', 'Modules\Agent\AgentController@agentViewProperty')->name('admin.agent.view.property');
    Route::get('agent-approval-status-update', 'Modules\Agent\AgentController@agentApprovalStatusUpdate')->name('agent.approval.status.update');
    Route::get('agent-property-status-change/{id}', 'Modules\Agent\AgentController@agentPropertyStatusChange')->name('admin.agent.property.status.change');
    Route::get('agent-delete-profile/{id}', 'Modules\Agent\AgentController@agentDeleteProfile')->name('admin.delete.agent.profile');
    Route::post('agent-delete-profile-all', 'Modules\Agent\AgentController@agentDeleteAll')->name('admin.select.agent.delete');

    Route::get('agent-chat-history/{id}', 'Modules\Agent\AgentController@agentChatHistory')->name('admin.agent.chat.history');
    Route::get('agent-show-chat/{id}', 'Modules\Agent\AgentController@agentShowChat')->name('admin.agent.show.chat');
    //manage provider
    Route::get('manage-provider', 'Modules\Provider\ProviderController@index')->name('admin.manage.provider');
    Route::get('provider-status-update', 'Modules\Provider\ProviderController@providerStatusUpdate')->name('provider.status.update');
    Route::get('provider-profile/{id}', 'Modules\Provider\ProviderController@providerDetails')->name('admin.provider.details');
    Route::get('edit-provider/{id}', 'Modules\Provider\ProviderController@providerEditProfile')->name('admin.edit.provider.profile');
    Route::post('edit-provider-save', 'Modules\Provider\ProviderController@providerEditProfileSave')->name('admin.edit.provider.profile.save');
    Route::get('service-provider/work-image-remove/{id}/{proid?}', 'Modules\Provider\ProviderController@providerWorkImageRemove')->name('admin.provider.remove.work.image');
    Route::post('service-provider/work-images-save/{id}', 'Modules\Provider\ProviderController@providerWorkImageSave')->name('admin.provider.work.image.save');
    Route::get('provider-approval-status-update', 'Modules\Provider\ProviderController@providerApprovalStatusUpdate')->name('provider.approval.status.update');
    Route::get('provider-delete-profile/{id}', 'Modules\Provider\ProviderController@proDeleteProfile')->name('admin.delete.pro.profile');
    Route::post('provider-delete-profile-all', 'Modules\Provider\ProviderController@protDeleteAll')->name('admin.select.pro.delete');

    Route::get('pro-chat-history/{id}', 'Modules\Provider\ProviderController@proChatHistory')->name('admin.pro.chat.history');
    Route::get('pro-show-chat/{id}', 'Modules\Provider\ProviderController@proShowChat')->name('admin.pro.show.chat');
    //manage approval
     
    Route::get('admin-manage-approval-status-update/{id}', 'Modules\Others\AprovalController@userApprovalStatusUpdate')->name('user.email.mob.aproval');

    //manage facilities

    Route::get('manage-facilities-amenities', 'Modules\Facilities\FacilitiesController@index')->name('admin.manage.facilities');
    Route::get('add-facilities-amenities', 'Modules\Facilities\FacilitiesController@addFacilitiesAmenities')->name('admin.add.facilities');
    Route::post('add-facilities-amenities', 'Modules\Facilities\FacilitiesController@addFacilitiesAmenities')->name('admin.add.facilities');
    Route::get('edit-facilities-amenities/{id}', 'Modules\Facilities\FacilitiesController@editFacilitiesAmenities')->name('admin.edit.facilities');
    Route::post('edit-facilities-amenities/{id}', 'Modules\Facilities\FacilitiesController@editFacilitiesAmenities')->name('admin.edit.facilities');
    Route::get('delete-facilities-amenities/{id}','Modules\Facilities\FacilitiesController@deleteFacilitiesAmenities')->name('admin.delete.facilities');
    Route::post('check-facilities-amenities','Modules\Facilities\FacilitiesController@checkFacilitiesAmenities')->name('check.facilities');

    //manage property

    Route::get('manage-property', 'Modules\Property\PropertyController@index')->name('admin.manage.property');
    Route::get('property-status-update', 'Modules\Property\PropertyController@propertyStatusUpdate')->name('property.status.update');
    Route::get('property-details/{id}', 'Modules\Property\PropertyController@propertyDetails')->name('admin.property.details');
    Route::get('edit-property/{id}', 'Modules\Property\PropertyController@editProperty')->name('admin.edit.property');
    Route::post('edit-property-save', 'Modules\Property\PropertyController@editPropertySave')->name('admin.edit.property.save');
    Route::get('edit-property-image/{id}', 'Modules\Property\PropertyController@editPropertyImage')->name('admin.edit.property.image');
    Route::post('edit-property-image-save', 'Modules\Property\PropertyController@editPropertyImageSave')->name('admin.edit.property.image.save');
    Route::get('edit-property-image-remove/{id}', 'Modules\Property\PropertyController@editPropertyImageRemove')->name('admin.edit.property.image.remove');
    Route::get('property-approval-status-update', 'Modules\Property\PropertyController@propertyApprovalStatusUpd')->name('property.approval.status.update');
    Route::get('delete-property/{id}','Modules\Property\PropertyController@deleteProperty')->name('admin.delete.property');
    Route::get('home-property-status-update', 'Modules\Property\PropertyController@propertyStatusUpdateHome')->name('home.property.status.update');
    Route::any('property-visit-request/{id}', 'Modules\Property\PropertyController@propertyVisitRequest')->name('admin.property.visit.request');
    Route::post('property-delete-all', 'Modules\Property\PropertyController@propertytDeleteAll')->name('admin.select.property.delete');

    Route::get('property-review/{id}', 'Modules\Property\PropertyController@propertyReview')->name('admin.property.review');
    Route::get('property-review-delete/{id}', 'Modules\Property\PropertyController@propertyReviewDelete')->name('admin.delete.property.review');
    //manage visit request

    Route::any('admin-manage-visit-request', 'Modules\Others\VisitRequestController@index')->name('admin.manage.visit.request');

    //manage state

    Route::get('manage-state', 'Modules\State\StateController@index')->name('admin.manage.state');
    Route::get('add-state', 'Modules\State\StateController@addState')->name('admin.add.state');
    Route::post('add-state', 'Modules\State\StateController@addState')->name('admin.add.state');
    Route::get('edit-state/{id}', 'Modules\State\StateController@editState')->name('admin.edit.state');
    Route::post('edit-state/{id}', 'Modules\State\StateController@editState')->name('admin.edit.state');
    Route::get('delete-state/{id}','Modules\State\StateController@deleteState')->name('admin.delete.state');
    Route::post('check-state','Modules\State\StateController@checkState')->name('check.state');

    //manage city

    Route::get('manage-city', 'Modules\City\CityController@index')->name('admin.manage.city');
    Route::get('add-city', 'Modules\City\CityController@addCity')->name('admin.add.city');
    Route::post('add-city', 'Modules\City\CityController@addCity')->name('admin.add.city');
    Route::get('edit-city/{id}', 'Modules\City\CityController@editCity')->name('admin.edit.city');
    Route::post('edit-city/{id}', 'Modules\City\CityController@editCity')->name('admin.edit.city');
    Route::get('delete-city/{id}','Modules\City\CityController@deleteCity')->name('admin.delete.city');
    Route::post('check-city','Modules\City\CityController@checkCity')->name('check.city');
    Route::post('user-state-dropdown','Modules\City\CityController@stateDropdownResult')->name('user.userstate');

    //manage sub category

    Route::get('manage-sub-category', 'Modules\SubCategory\SubCategoryController@index')->name('admin.manage.sub.category');
    Route::get('add-sub-category', 'Modules\SubCategory\SubCategoryController@addSubCategory')->name('admin.add.sub.category');
    Route::post('add-sub-category', 'Modules\SubCategory\SubCategoryController@addSubCategory')->name('admin.add.sub.category');
    Route::get('edit-sub-category/{id}', 'Modules\SubCategory\SubCategoryController@editSubCategory')->name('admin.edit.sub.category');
    Route::post('edit-sub-category/{id}', 'Modules\SubCategory\SubCategoryController@editSubCategory')->name('admin.edit.sub.category');
    Route::get('delete-sub-category/{id}','Modules\SubCategory\SubCategoryController@deleteSubCategory')->name('admin.delete.sub.category');
    Route::post('check-sub-category','Modules\SubCategory\SubCategoryController@checkSubCategory')->name('check.sub.category');

    Route::get('popular-category/{id}','Modules\SubCategory\SubCategoryController@makePopular')->name('admin.make.popular');
    // Manage Job

    Route::get('manage-job','Modules\Job\JobController@manageJob')->name('admin.manage.job');
    Route::get('job-details/{id}', 'Modules\Job\JobController@jobDetails')->name('admin.job.details');
    Route::get('job-status-update', 'Modules\Job\JobController@jobStatusUpdate')->name('job.status.update');
    Route::get('delete-job/{id}','Modules\Job\JobController@deleteJob')->name('admin.delete.job');
    Route::any('job-proposal/{id}', 'Modules\Job\JobController@jobProposalList')->name('admin.job.proposal');
    Route::get('edit-job/{id}','Modules\Job\JobController@editJob')->name('admin.edit.job');
    Route::post('save-job/{id}', 'Modules\Job\JobController@addPostJobSave')->name('admin.save.job');
    
    Route::get('job-image-remove/{id}','Modules\Job\JobController@jobImageRemove')->name('admin.add.job.image.remove');
    Route::post('save-assistance', 'Modules\Job\JobController@saveRivirtualAssist')->name('admin.save.rivirtual.assist');

    Route::get('bid-recommended/{id}','Modules\Job\JobController@jobBidRecommended')->name('admin.bid.recommended');

    Route::get('view-milestone/{id}','Modules\Job\JobController@viewMilestone')->name('admin.view.milestone');
     Route::post('job-delete-all', 'Modules\Job\JobController@jobDeleteAll')->name('admin.select.job.delete');
     
     Route::get('job-approval-status-update','Modules\Job\JobController@jobApproval')->name('job.approval.status.update');
    // Manage Subscription

    Route::get('manage-subscription','Modules\Subscription\SubscriptionController@manageSubscription')->name('admin.manage.subscription');
    Route::get('change-status','Modules\Subscription\SubscriptionController@changeStatus')->name('admin.change.status');
    Route::get('edit-subscription/{id?}','Modules\Subscription\SubscriptionController@editSubscription')->name('admin.edit.subscription');
    Route::post('save-subscription','Modules\Subscription\SubscriptionController@saveSubscription')->name('admin.save.subscription');

    Route::get('manage-provider-subscription','Modules\Subscription\SubscriptionController@manageProviderSubscription')->name('admin.manage.provider.subscription');
    Route::get('change-package-status','Modules\Subscription\SubscriptionController@changePackageStatus')->name('admin.change.package.status');
    Route::get('change-package-type','Modules\Subscription\SubscriptionController@changeTypeStatus')->name('admin.change.package.type');
    Route::get('change-sub-status','Modules\Subscription\SubscriptionController@changeSubStatus')->name('admin.change.sub.status');
    Route::get('change-sub-type','Modules\Subscription\SubscriptionController@changeSubTypeStatus')->name('admin.change.sub.type');
    Route::get('edit-provider-subscription/{id?}','Modules\Subscription\SubscriptionController@editProviderSubscription')->name('admin.edit.provider.subscription');
    Route::post('save-provider-subscription','Modules\Subscription\SubscriptionController@saveProviderSubscription')->name('admin.save.provider.subscription');
    
     // manage payment
   Route::get('pro-payment','Modules\Payment\PaymentController@index')->name('admin.manage.pro.payment');
   Route::post('pro-save-payment','Modules\Payment\PaymentController@proSavePayment')->name('admin.save.pro.payment');

    //manage faq

    Route::get('manage-faq', 'Modules\Faq\FaqController@index')->name('admin.manage.faq');
    Route::get('add-faq', 'Modules\Faq\FaqController@addFaq')->name('admin.add.faq');
    Route::post('add-faq', 'Modules\Faq\FaqController@addFaq')->name('admin.add.faq');
    Route::get('edit-faq/{id}', 'Modules\Faq\FaqController@editFaq')->name('admin.edit.faq');
    Route::post('edit-faq/{id}', 'Modules\Faq\FaqController@editFaq')->name('admin.edit.faq');
    Route::get('delete-faq/{id}','Modules\Faq\FaqController@deleteFaq')->name('admin.delete.faq');
    Route::post('check-faq','Modules\Faq\FaqController@checkFaq')->name('check.faq');
    Route::get('faq-status-update', 'Modules\Faq\FaqController@faqStatusUpdate')->name('faq.status.update');
});